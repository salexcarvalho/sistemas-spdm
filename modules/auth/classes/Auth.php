<?php defined('SYSPATH') OR die('No direct access allowed.');

abstract class Auth extends Kohana_Auth 
{

    /**
     * Define os usuários permitidos, normalmente um array retornado do banco de dados.
     * @author Lucas Henrique <lucas.henrique@spdm.org.br>
     */
    public function set_users($users)
    {
        $this->_users = $users;        
    }
    /**
     * Retorna os usuários permitidos da variável local de usuários.
     * @author Lucas Henrique <lucas.henrique@spdm.org.br>
     */
    public function get_users()
    {
        return $this->_users;
    }

}
<?php defined('SYSPATH') OR die('No direct script access.');

return array(
	
	':field must contain only letters'                              => ':field precisa conter somente letras',
	':field must contain only letters and spaces'                   => ':field precisa conter somente letras e espaços',
	':field must contain only numbers, letters and dashes'          => ':field precisa conter somente números, letras e traços',
	':field must contain only numbers, letters, dashes and spaces'  => ':field precisa conter somente números, letras, traços e espaços',
	':field must contain only letters and numbers'                  => ':field precisa conter somente letras e números',
	':field must be a color'                                        => ':field precisa ser uma cor',
	':field must be a credit card number'                           => ':field precisa ser um número de cartão de crédito',
	':field must be a date'                                         => ':field precisa ser uma data',
	':field must be a decimal with :param2 places'                  => ':field precisa ser um número com :param2 casas decimais',
	':field must be a digit'                                        => ':field precisa ser um dígito',
	':field must be an email address'                               => ':field precisa ser um endereço de email',
	':field must contain a valid email domain'                      => ':field precisa conter um domínio válido de email',
	':field must equal :param2'                                     => ':field precisa ser iguail a :param2',
	':field must be exactly :param2 characters long'                => ':field precisa ter exatamente :param2 caracteres',
	':field must be one of the available options'                   => ':field precisa ser uma opção válida',
	':field must be an ip address'                                  => ':field precisa ser um endereço de ip',
	':field must be the same as :param3'                            => ':field precisa ser o mesmo que :param3',
	':field must be at least :param2 characters long'               => ':field precisa ter pelo menos :param2 caracteres',
	':field must not exceed :param2 characters long'                => ':field não pode exceder :param2 caracteres',
	':field must not be empty'                                      => ':field não pode estar vazio.',
	':field must be numeric'                                        => ':field precisa ser um número',
	':field must be a phone number'                                 => ':field precisa ser um número de telefone',
	':field must be within the range of :param2 to :param3'         => ':field precisa estar entre :param2 e :param3',
	':field does not match the required format'                     => ':field não tem um formato aceito',
	':field must be a url'                                          => ':field precisa ser uma URL(link)',

);

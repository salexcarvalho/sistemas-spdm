<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - Gerenciamento de Cursos</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
   
    <style>
       .table {
           width: 98%;
           margin-left: 15px;
           margin-right: 15px;
           margin-bottom: 3px;
       }
       .table > thead > tr > th,
           .table > tbody > tr > th,
           .table > tfoot > tr > th,
           .table > thead > tr > td,
           .table > tbody > tr > td,
           .table > tfoot > tr > td {
             padding: 8px;
             padding-bottom: 6px;
             line-height: 1.42857143;
             vertical-align: bottom;
             border-bottom: 1px solid #ddd;
             border-top: 0px solid #ddd;
           }
       </style> 
    </head>
    <body>
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <?php if ($_SESSION['AcLiberaBtnVizCur'] == true) { ?>
                            <h1 class="page-header"> Visualizar Curso</h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-file-pdf-o"></i> Gerenciamento de Cursos - <?php $data = new DateTime(); echo $data->format("H:i:s")." - ".date("H:i:s"); ?></li>
                                <li><a href="home"><i class="fa fa-list"></i> Lista de cursos</a></li>
                                <li class="active"><i class="glyphicon glyphicon-file"></i> Visualizar curso</li>
                            </ol>
                            <!-- Inicio Informações Curso-->                           
                                <div class="group">    
                                    <div class="col-md-10">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <span>Dados do Curso</span>
                                            </div>
                                            <div class="panel-body"> 

                                                <table class="table table-condensed">
                                                    <tbody>
                                                    <tr>
                                                        <td colspan="2"><label>Nome:</label>&nbsp;<?=$curso[0]['nome']?></td>
                                                   </tr>
                                                   <tr>
                                                        <td> <label>Carga Horária: </label>&nbsp;<?=$curso[0]['cargaHoraria']?></td>                                                  
                                                        <td> <label class="control-label">Tipo Curso: </label>&nbsp;<?=$tipoCurso[0]['nome'];?></td>
                                                   </tr> 
                                                   <tr>
                                                        <td><label>Carga Teórica: </label>&nbsp;<?=$curso[0]['cargaTeorica']?>%</td>                                                  
                                                        <td><label>Carga Prática: </label>&nbsp;<?=$curso[0]['cargaPratica']?>%</td>
                                                   </tr>
                                                   <tr>
                                                        <td> <label>Numero Mínimo de Alunos:* </label>&nbsp;<?=$curso[0]['minAlunos']?></span></td>                                                 
                                                        <td><label>Numero Máximo de Alunos: </label>&nbsp;<?=$curso[0]['maxAlunos']?></td>
                                                   </tr>
                                                   <tr>
                                                       <td colspan="2"><label>Modalidade: </label>&nbsp;<?= str_replace(',', '/', $curso[0]['modalidade']);?></td>
                                                   </tr>
                                                   </tbody>
                                               </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">    
                                    <div class="col-md-5">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <span>Areas do Conhecimento da Saúde:</span>
                                            </div>
                                            <div class="panel-body">
                                            <table class="table table-condensed">                                                      
                                                        <tbody>
                                                            <?php foreach ($areas as $area): ?>  
                                                            <tr>
                                                                <td><?= $area?></td>
                                                            </tr> 
                                                                <?php endforeach;?>
                                                        </tbody>
                                            </table> 
                                            </div>                                                
                                        </div> 
                                    </div>
                                    <div class="col-md-5">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <span>Alunado:</span>
                                            </div>
                                            <div class="panel-body">
                                                <table class="table table-condensed">                                                      
                                                        <tbody>
                                                            <?php foreach ($forma as $form): ?>  
                                                            <tr>
                                                                <td><?= $form?></td>
                                                            </tr> 
                                                                <?php endforeach;?>
                                                        </tbody>
                                                </table>                                                
                                            </div>
                                        </div> 
                                    </div>
                                    <div class="col-md-10">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <span>Informações Complementares:</span>
                                            </div>
                                            <div class="panel-body">
                                                <table class="table table-condensed">                                                      
                                                        <tbody>
                                                            <tr>
                                                                <td><label >Períodos:</label>&nbsp;
                                                            <?php
                                                            $cont = count($periodos);
                                                            $p="";                                                            
                                                            foreach ($periodos as $periodo): 
                                                                if($cont > 1):
                                                                  $p .= $periodo.", ";
                                                                else:
                                                                  $p .= $periodo; 
                                                                endif;
                                                                $cont--;
                                                            endforeach; 
                                                            echo $p;
                                                            ?>
                                                                    
                                                                </td> 
                                                            </tr> 
                                                        </tbody>
                                                </table>
                                               <table class="table table-condensed">                                                      
                                                        <tbody>
                                                             
                                                            <tr>
                                                                <td><label >Ano(s) de Oferta:</label>&nbsp;<?=$curso[0]['oferta']?></td>
                                                           
                                                                <td><label >Semestre(s):</label>&nbsp;<?=$curso[0]['semestres']?></td>
                                                            </tr> 
                                                             <tr>
                                                                <td><label >Inicio:</label>&nbsp;<?=$curso[0]['semestres']?></td>
                                                            
                                                                <td><label >Encerramento:</label>&nbsp;<?=$curso[0]['semestres']?></td>
                                                            </tr>
                                                             <tr>
                                                                <td><label >Ativo:</label>&nbsp;<?=($curso[0]['status']==1)?"ativo": "inativo";?></td>
                                                          
                                                                <td><label >Aprovado:</label>&nbsp;<?=($curso[0]['aprovado']==1)?"aprovado": "não aprovado";?></td>
                                                            </tr>
                                                             <tr>
                                                                <td colspan="2">* Refere-se ao número de inscritos abaixo do qual o curso será cancelado. Até ultrapassar o número mínimo de inscritos, os candidatos interessados serão considerados pré-inscritos e uma vez atingindo o número, os mesmos serão notificados pela SPDM Educação para efetivarem suas inscrições</td>
                                                            </tr>
                                                        </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                     <div class="col-md-10">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <span>Coordenação:</span>
                                            </div>
                                            <div class="panel-body">
                                                <?php 
                                                $model_formacao = new Model_Grc_Formacao('default');
                                               
                                                foreach ($coordenacao as $coord):                                                  
                                                ?>
                                                <table class="table table-condensed">                                                      
                                                        <tbody>
                                                             
                                                            <tr>
                                                                <td colspan="2"><label >Nome Coordenador:</label>&nbsp;<?=$coord['nome']?></td>   
                                                            </tr> 
                                                             <tr>
                                                                <td><label >RG:</label>&nbsp;<?=$coord['RG']?></td>                                                            
                                                                <td><label >CPF:</label>&nbsp;<?=$coord['CPF']?></td>
                                                            </tr>
                                                             <tr>
                                                                 <td colspan="2"><label>Titulação:</label>&nbsp;
                                                                    <?php  
                                                                    $idForm = explode(",", $coord['id_formacao']);
                                                                    for($i=0;$i<count($idForm);$i++):
                                                                       $formacao = $model_formacao->select_formacao($idForm[$i]);
                                                                       echo $formacao[0]['nome'];
                                                                    endfor;
                                                                    
                                                                            ?>
                                                                     
                                                                     </td>
                                                            </tr>
                                                            <tr>
                                                                 <td colspan="2"><label>Endereço para correspondência:</label>&nbsp;</td>
                                                            </tr>
                                                             <tr>
                                                                 <td><label >Email:</label>&nbsp;</td>
                                                                 <td><label >Lattes:</label>&nbsp;</td>
                                                            </tr>
                                                            <tr>     
                                                                 <td><label >Telefone:</label>&nbsp;</td>
                                                                 <td><label >Celular:</label>&nbsp;</td>
                                                            </tr> 
                                                            <tr>
                                                                 <td colspan="2"><label>Breve descrição da experiência acadêmica e profissional na área de atuação:</label></td>
                                                            </tr>
                                                            <tr>
                                                                 <td colspan="2">&nbsp;</td>
                                                            </tr
                                                        </tbody>
                                                </table>
                                                <?php endforeach;?>
                                            </div>
                                        </div>
                                        </div>
                                     <div class="col-md-10">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <span>Inscrição e Seleção:</span>
                                            </div>
                                            <div class="panel-body">
                                                <table class="table table-condensed">                                                      
                                                        <tbody>
                                                             
                                                            <tr>
                                                                <td><label >Período de Inscrição:</label><i>(não colocar em finais de semana e/ou feriados)</i>&nbsp; de __/__/____ a __/__/____</td>                                                               
                                                            </tr> 
                                                             <tr>
                                                                <td><label >Documentos para inscrição:</label>&nbsp;Total de beneficiados 0</td>
                                                             </tr>                                                             
                                                            <tr>     
                                                                 <td><label >Valor da Inscrição:</label>&nbsp;R$: 0,00</td>                                                              
                                                            </tr> 
                                                            <tr>
                                                                  <td><label >Isenção de Inscrição:</label>&nbsp;Total de beneficiados 0</td>
                                                            </tr>                                                          
                                                            <tr>
                                                                  <td><label >Processo Seletivo:</label>&nbsp;</td>
                                                            </tr>
                                                        </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        </div>
                                     <div class="col-md-10">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <span>Matrícula</span>
                                            </div>
                                            <div class="panel-body">
                                                  <table class="table table-condensed">                                                      
                                                        <tbody>
                                                             
                                                            <tr>
                                                                <td><label >Período de Matrícula:</label><i>(não colocar em finais de semana e/ou feriados)</i>&nbsp; de __/__/____ a __/__/____</td>                                                               
                                                            </tr>                                                                                                                         
                                                           
                                                            <tr>
                                                                  <td><label >Valor da Matrícula:</label>&nbsp;R$: 0,00</td>
                                                            </tr>
                                                            <tr>
                                                                  <td><label >Isenção de Matrícula:</label>&nbsp;Total de beneficiados 0</td>
                                                            </tr>
                                                            <tr>
                                                                  <td><label >Previsão de Inicío do Curso:</label>&nbsp; __/__/____</td>
                                                            </tr>
                                                            <tr>
                                                                  <td><label >Previsão de Término do Curso:</label>&nbsp; __/__/____</td>
                                                            </tr>
                                                        </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        </div>
                                     <div class="col-md-10">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <span>Financiamento:</span>                                                
                                            </div>
                                            <div class="panel-body">
                                                <table class="table table-condensed">
                                                        <thead>
                                                            <tr>
                                                                <th colspan="2" style="text-align:center;"><label >Investimento Por aluno</label></th>                                                               
                                                            </tr>
                                                        </thead>                                                        
                                                        <tbody>                                                             
                                                            <tr>
                                                                <td colspan="2"><label >Valor Total do Curso:</label>&nbsp;R$: 0,00</td>                                                               
                                                            </tr>
                                                            <tr>
                                                                  <td><label >Número de Parcelas:</label>&nbsp; 0</td>                                                           
                                                                  <td><label >Valor da Parcela:</label>&nbsp; R$: 0,00</td>
                                                            </tr>
                                                            <tr>
                                                                  <td><label >Outras Taxas:</label>&nbsp;</td>
                                                                  <td><label >Valor:</label>&nbsp; R$: 0,00</td>
                                                            </tr>
                                                            <tr>
                                                                  <td colspan="2"><label >Periodicidade:</label>&nbsp;</td>
                                                            </tr>
                                                        </tbody>
                                                </table>
                                                <table class="table table-condensed">
                                                        <thead>
                                                            <tr>
                                                                <th colspan="4" style="text-align:center;"><label >Fontes Externas de Financiamento</label></th>                                                               
                                                            </tr>
                                                        </thead>                                                        
                                                        <tbody>                                                             
                                                            <tr>
                                                                <td colspan="4"><label >Fonte:</label>&nbsp;R$: 0,00</td>                                                               
                                                            </tr>
                                                            <tr>
                                                                  <td colspan="4"><label >Valor Total:</label>&nbsp; R$: 0,00</td>
                                                            </tr>
                                                            <tr>
                                                                  <th colspan="4" style="text-align:center;"><label >Inscrição</label></td>
                                                            </tr>
                                                            <tr>      
                                                                  <td><label >Valor Total:</label>&nbsp; R$: 0,00</td>
                                                                  <td><label >Sem Isenção:</label>&nbsp; 0 Aluno(s)</td>
                                                                  <td><label >Isenção Total:</label>&nbsp; 0 Aluno(s)</td>
                                                                  <td><label >Isenção Parcial:</label>&nbsp;20% 0 Aluno(s) - 40% 0 Aluno(s) - 60% 0 Aluno(s) - 80% 0 Aluno(s)</td>
                                                            </tr>
                                                           <tr>
                                                                  <th colspan="4" style="text-align:center;"><label >Matrícula</label></td>
                                                            </tr>
                                                            <tr>      
                                                                  <td><label >Valor Total:</label>&nbsp; R$: 0,00</td>
                                                                  <td><label >Sem Isenção:</label>&nbsp; 0 Aluno(s)</td>
                                                                  <td><label >Isenção Total:</label>&nbsp; 0 Aluno(s)</td>
                                                                  <td><label >Isenção Parcial:</label>&nbsp;20% 0 Aluno(s) - 40% 0 Aluno(s) - 60% 0 Aluno(s) - 80% 0 Aluno(s)</td>
                                                            </tr>
                                                            <tr>
                                                                  <th colspan="4" style="text-align:center;"><label >Parcelas</label></td>
                                                            </tr>
                                                            <tr>      
                                                                  <td><label >Valor Total:</label>&nbsp; R$: 0,00</td>
                                                                  <td><label >Sem Isenção:</label>&nbsp; 0 Aluno(s)</td>
                                                                  <td><label >Isenção Total:</label>&nbsp; 0 Aluno(s)</td>
                                                                  <td><label >Isenção Parcial:</label>&nbsp;20% 0 Aluno(s) - 40% 0 Aluno(s) - 60% 0 Aluno(s) - 80% 0 Aluno(s)</td>
                                                            </tr>
                                                            <tr>
                                                                  <th colspan="4" style="text-align:center;"><label >Outras Taxas</label></td>
                                                            </tr>
                                                            <tr>      
                                                                  <td><label >Valor Total:</label>&nbsp; R$: 0,00</td>
                                                                  <td><label >Sem Isenção:</label>&nbsp; 0 Aluno(s)</td>
                                                                  <td><label >Isenção Total:</label>&nbsp; 0 Aluno(s)</td>
                                                                  <td><label >Isenção Parcial:</label>&nbsp;20% 0 Aluno(s) - 40% 0 Aluno(s) - 60% 0 Aluno(s) - 80% 0 Aluno(s)</td>
                                                            </tr>
                                                        </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        </div>
                                     <div class="col-md-10">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <span>Anexo I: <label>Informações Pedagógicas</label></span>
                                            </div>
                                            <div class="panel-body">
                                                <table class="table table-condensed">
                                                        <tbody>                                                             
                                                            <tr>
                                                                <td><label >Nome do Curso:</label>&nbsp;</td>                                                               
                                                            </tr>
                                                             <tr>
                                                                <td><label >Ementa do Curso:</label>&nbsp;</td>                                                               
                                                            </tr>
                                                             <tr>
                                                                <td><label >Justificativa:</label>&nbsp;</td>                                                               
                                                            </tr>
                                                            <tr>
                                                                <td><label >Objetivos(geral e especifico):</label>&nbsp;</td>                                                               
                                                            </tr>
                                                             <tr>
                                                                <td><label >Concepção do Curso:</label>&nbsp;</td>                                                               
                                                            </tr>
                                                            <tr>
                                                                <td><label >Publico Alvo:</label>&nbsp;</td>                                                               
                                                            </tr>
                                                            <tr>
                                                                <td><label >Pré-requisito:</label>&nbsp;</td>                                                               
                                                            </tr>
                                                            <tr>
                                                                <td><label >Metodologia:</label>&nbsp;</td>                                                               
                                                            </tr>
                                                            <tr>
                                                                <td><label >Interdiciplinaridade:</label>&nbsp;</td>                                                               
                                                            </tr>
                                                            <tr>
                                                                <td><label >Atividades Complementares:</label>&nbsp;</td>                                                               
                                                            </tr>
                                                            <tr>
                                                                <td><label >Tecnologia:</label>&nbsp;</td>                                                               
                                                            </tr>
                                                            <tr>
                                                                <td><label >Infraestrutura Física:</label>&nbsp;</td>                                                               
                                                            </tr>
                                                            <tr>
                                                                <td><label >Sistema de Avaliação:</label>&nbsp;</td>                                                               
                                                            </tr>
                                                            <tr>
                                                                <td><label >Certificação:</label>&nbsp;</td>                                                               
                                                            </tr>
                                                        </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        </div>
                                    <div class="col-md-10">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <span>Diciplinas do Curso</span>
                                            </div>
                                            <div class="panel-body">
                                                <?php $diciplinas=1;?>
                                                <table class="table table-condensed">
                                                         <tbody>  
                                                            <tr>
                                                                <td colspan="2" style="text-align:center;"><label >Numero de Diciplinas do Curso:</label>&nbsp; <?=count($diciplinas);?></td>                                                               
                                                            </tr>
                                                        <tbody>  
                                                </table>
                                                <?php for($i=count($diciplinas);$i>0;$i--):?>
                                                <table class="table table-condensed">                                                        
                                                        <tbody>                                                             
                                                            <tr>
                                                                <td><label ><?=$i?> - Nome da Disciplina:</label>&nbsp;</td>                                                               
                                                            </tr>
                                                             <tr>
                                                                <td><?=$i?>.1 - <br />
                                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label >Carga Horária Total da disciplina:</label>&nbsp; 0 horas <br />
                                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label >Total de horas teóricas:</label>&nbsp; 0 horas <br />
                                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label >Total de horas práticas:</label>&nbsp; 0 (se for o caso)<br />
                                                                </td>                                                               
                                                            </tr>
                                                             <tr>
                                                                <td><label ><?=$i?>.2 - Informações sobre o professor responsável pela disciplina:</label>&nbsp;</td>                                                               
                                                            </tr>
                                                            <tr>
                                                                <td><label >Nome:</label>&nbsp;</td>                                                               
                                                            </tr>
                                                             <tr>
                                                                <td><label >Titulação:</label>&nbsp;</td>                                                               
                                                            </tr>
                                                            <tr>
                                                                <td><label >Especificar</label></td>                                                               
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>                                                               
                                                            </tr>
                                                            <tr>
                                                                <td><label ><?=$i?>.3 - Ementa da disciplina</label></td>                                                               
                                                            </tr>
                                                             <tr>
                                                                <td>&nbsp;</td>                                                               
                                                            </tr>
                                                            <tr>
                                                                <td><label ><?=$i?>.4 - Bibliografia(até 3 obras)</label></td>                                                               
                                                            </tr>
                                                             <tr>
                                                                <td>&nbsp;</td>                                                               
                                                            </tr>                                                            
                                                        </tbody>
                                                </table>
                                                 <?php endfor;?>
                                            </div>
                                        </div>
                                        </div>
                                        <div class="col-md-10">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <span>Ocorrência das Aulas</span>
                                            </div>
                                            <div class="panel-body">
                                                <table class="table table-condensed">                                                        
                                                        <tbody>                                                             
                                                            <tr>
                                                                <td colspan="2"><label >Integral:</label> (Manhã e Tarde)&nbsp;</td>                                                               
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2"><label >Parcial:</label> (manhã/tarde/noite):&nbsp;</td>                                                               
                                                            </tr>
                                                             <tr>
                                                                <td><label >Ensino a Distância:</label>&nbsp;</td>
                                                                <td><label >Encontros Presenciais:</label>&nbsp; 0</td>  
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2"><label >Horários por dia da semana que ocorrerão as aulas</label></td>                                                               
                                                            </tr>
                                                             <tr>
                                                                <td><label >2ª Feira:</label>&nbsp; das __:__ as __:__</td> 
                                                                <td><label >3ª Feira:</label>&nbsp; das __:__ as __:__</td>
                                                            </tr>
                                                             <tr>    
                                                                <td><label >4ª Feira:</label>&nbsp; das __:__ as __:__</td> 
                                                                <td><label >5ª Feira:</label>&nbsp; das __:__ as __:__</td> 
                                                            </tr>
                                                             <tr>   
                                                                <td><label >6ª Feira:</label>&nbsp; das __:__ as __:__</td>
                                                                <td><label >Sabado:</label>&nbsp; das __:__ as __:__</td>
                                                             </tr>
                                                             <tr> 
                                                                <td colspan="2"><label >Domingo:</label>&nbsp; das __:__ as __:__</td> 
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2"><label >Local de Realização das Aulas:</label>&nbsp;</td>                                                               
                                                            </tr>
                                                            
                                                </table>
                                                 
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                <div class="form-group">
                                    <div class="col-md-7">
                                       <?php if($curso[0]['aprovado']==0):?>
                                        <button type="submit" class="btn btn-primary btn-sm">Aprovar</button>
                                       <?php endif;?> 
                                        <a href="home" class="btn btn-danger">Voltar</a>
                                    </div>
                                </div>                                                       
                        <?php } else { ?>
                            <h1 class="page-header"> Voce não possue acesso a está pagina, consulte o administrador.</h1>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>
        <script>
            jQuery(document).ready(function () {
                // Remove seleção de ativo no menu.
                $('.nav li').removeClass('active');
                $('#side-menu li').removeClass('active');
                // Ativa botão no menu.
                $('#li-gerenciamento ul').addClass('collapse in');
                $('#li-cad-cursos a').addClass('active');
                $('#li-cursos').addClass('active');
                $('#li-cursos a').addClass('collapse in');

});
        </script>        
    </body>
</html>
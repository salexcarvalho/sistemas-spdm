<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - GERENCIAMENTO DE CURSOS</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <?php $busca = (isset($busca))? $busca:"";?>
       <!-- Modal -->
        <div class="modal fade" id="modal_excluir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Confirmar Exclusão</h4>
                    </div>
                    <div class="modal-body" id="modal_delete_participante">Deseja excluir este documento?</div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        <button type="button" class="btn btn-primary btn-sm" id="modal_btn-excluir">Excluir</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal -->

        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            
            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Matrícula</h1>
                        <ol class="breadcrumb">
                            <li><i class="fa fa-file-pdf-o"></i> Documentos</li>
                            <li class="active"><i class="fa fa-list"></i> Lista de documentos</li>
                        </ol>

                        <?php if (isset($warning)) : ?>
                            <div id="msg" class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong><?= $warning; ?></strong>
                            </div>
                        <?php endif ?>                          

                        <div class="row" style="margin-bottom:10px; margin-top: -10px;">
                            <div class="col-lg-12">
                                <?php if ($_SESSION['AcLiberaBtnAddDoc'] == true) { ?>
                                    <a href="cadastro_docs">
                                        <button type="submit" id="btn_novo_curso" class="pull-right btn btn-primary btn-sm" name="btn_novo_documento"><i class="glyphicon glyphicon-plus-sign"></i>&nbsp;Adicionar</button>
                                    </a>
                                <?php } ?>
                            </div>
                        </div>      

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <span>Documentos </span>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">

                                    <table class="table table-striped table-bordered table-hover" id="tipo">
                                        <thead>
                                            <tr>
                                                <th width="20%" style="text-align: center">ID</th>
                                                <th width="20%">Nome</th>
                                                <th width="20%">Alias</th>
                                                <th width="20%" style="text-align: center">Ações Cursos</th>
                                                <th width="20%" style="text-align: center">Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php include kohana::find_file('views', 'adm/grc/lista_docs') ?>
                                        </tbody>    
                                    </table>
                                    <?php include(kohana::find_file('views/paginator', 'paginacao', 'php')) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>

        <script>
            jQuery(document).ready(function () {
                // Remove seleção de ativo no menu.
                $('.nav li').removeClass('active');
                $('#side-menu li').removeClass('active');
                // Ativa botão no menu.
                $('#li-gerenciamento ul').addClass('collapse in');
                $('#li-cad-inscr-doc a').addClass('active');
                $('#li-cursos').addClass('active');
                $('#li-cursos a').addClass('collapse in');
                           
               $('#tipo').dataTable(
                        {
                            "aoColumnDefs": [{"bSortable": false, "aTargets": [0, 1, 2, 3]}],
                            "paging": false,
                            "info": false,
                            "aaSorting": [[0, 'desc']],
                            "bFilter": false,
                            "oLanguage":
                                    {
                                        "sSearch": " Buscar: "
                                    }
                        });

                var idDocs;
                var nome;
                var page;

                $("body").on("click", 'button[name=btnExcluir]', function ()
                {
                    idDocs = $(this).attr('idDocs');
                    nome = $(this).attr('nome');
                    $('#modal_delete_documento').html("Deseja excluir o Documento <b>\"" + nome + "\"</b> ?");
                });

                $("body").on("click", 'button[name=btn_filtrar]', function ()
                {
                    var Busca = $(this).attr('Busca');
                    window.location.href = "home?Busca=" + Busca;
                });

                $('#modal_excluir').on('hidden.bs.modal', function ()
                {
                    window.location.replace("home");
                });


                $('#modal_btn-excluir').on("click", function ()
                {
                    var btn = $(this);
                    var excluir, txt;
                    btn.text('loading');
                    btn.removeClass('btn-primary');
                    btn.removeClass('btn-danger');
                    btn.removeClass('btn-success');
                    btn.addClass('btn-primary');
                    excluir = 'delete_documento';
                    txt = 'Documento excluído com sucesso';
                    $('#modal_btn-excluir').prop('disabled', true);
                    $.ajax(excluir,
                            {
                                type: "POST",
                                data: {idDocs: idDocs}
                            })
                            .done(function (data)
                            {
                                $('#modal_delete_documento').html(txt);

                                btn.removeClass('btn-primary');
                                btn.addClass('btn-success');
                            })
                            .fail(function ()
                            {
                                $('#modal_delete_documento').html('Erro: ' + data);
                                btn.removeClass('btn-primary');
                                btn.addClass('btn-danger');
                            })
                            .always(function ()
                            {
                                btn.text('Excluído');
                            });
                });

                $("body").on("click", "button[name=btnEditar]", function () {
                    idDocs = $(this).attr('idDocs');
                    page = $(this).attr('page');
                    window.location.href = "editar_docs?idDocs=" + idDocs + "&page=" + page;
                });

                $("body").on("click", "button[name=btnAtivar]", function () {
                    idDocs = $(this).attr('idDocs');
                    $.get('update_ativar_docs?idDocs=' + idDocs, function (data) {
                        if (data === "1") {
                            $('#btnAtivar_' + idDocs).addClass('btn btn-success btn-xs');
                            $('#btnAtivar_' + idDocs).removeClass('btn-danger');
                            $('#activ_' + idDocs).removeClass('glyphicon-ban-circle');
                            $('#activ_' + idDocs).addClass('glyphicon-ok');
                        } else {
                            $('#btnAtivar_' + idDocs).addClass('btn btn-danger btn-xs');
                            $('#btnAtivar_' + idDocs).removeClass('btn-success');
                            $('#activ_' + idDocs).addClass('glyphicon-ok');
                            $('#activ_' + idDocs).addClass('glyphicon-ban-circle');
                        }
                    });
                });


                $('#btn_novo_documento').on("click", function ()
                {
                    window.location.href = "cad_documento";
                });


                $("body").on("click", "button[name=btnVisualizar]", function ()
                {
                    idDocs = $(this).attr('idDocs');
                    page = $(this).attr('page');
                    window.location.href = "visualizar_docs?idDocs=" + idDocs + "&page=" + page;
                });

                $('.dataTables_filter label').addClass('pull-right');
            });
        </script>         
    </body>
</html>
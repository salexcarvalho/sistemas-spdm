<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - GERENCIAMENTO DE CURSOS</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <?php $busca = (isset($busca))? $busca:"";?>

        <!-- Modal -->
        <div class="modal fade" id="modal_excluir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Confirmar Exclusão</h4>
                    </div>
                    <div class="modal-body" id="modal_delete_AreasCurso">Deseja excluir esta Area?</div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        <button type="button" class="btn btn-primary btn-sm" id="modal_btn-excluir">Excluir</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal -->

        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            
            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Areas do Conhecimento</h1>
                        <ol class="breadcrumb">
                            <li><i class="fa fa-file-pdf-o"></i> Ciências da Saúde</li>
                            <li class="active"><i class="fa fa-list"></i> Lista de Subáreas</li>
                        </ol>

                        <?php if (isset($warning)) : ?>
                            <div id="msg" class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong><?= $warning; ?></strong>
                            </div>
                        <?php endif ?>                          

                        <div class="row" style="margin-left: 10px;margin-bottom:10px; margin-top: -10px;">
                            <div class="col-lg-12">
                                <?php if ($_SESSION['AcLiberaBtnAddAre'] == true) { ?>
                                    <a href="cadastro_area">
                                        <button type="submit" id="btn_novo_areacurso" class="pull-right btn btn-primary btn-sm" name="btn_novo_areacurso"><i class="glyphicon glyphicon-plus-sign"></i>&nbsp;Adicionar</button>
                                    </a>
                                <?php } ?>
                            </div>
                        </div>      

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <span>Subáreas </span>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">

                                    <table class="table table-striped table-bordered table-hover" id="areas">
                                        <thead>
                                            <tr>
                                                <th width="25%" style="text-align: center">ID</th>
                                                <th width="25%" style="text-align: center">Area</th>                                                                                          
                                                <th width="25%" style="text-align: center">Ações Area</th>
                                                <th width="25%" style="text-align: center">Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php include kohana::find_file('views', 'adm/grc/area_lista') ?>
                                        </tbody>    
                                    </table>
                                    <?php include(kohana::find_file('views/paginator', 'paginacao', 'php')) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>

        <script>
            jQuery(document).ready(function () {
                // Remove seleção de ativo no menu.
                $('.nav li').removeClass('active');
                $('#side-menu li').removeClass('active');
                // Ativa botão no menu.
                $('#li-gerenciamento ul').addClass('collapse in');
                $('#li-cad-areas-curso a').addClass('active');
                $('#li-cursos').addClass('active');
                $('#li-cursos a').addClass('collapse in');
                
               $('#areas').dataTable(
                        {
                            "aoColumnDefs": [{"bSortable": false, "aTargets": [0, 1, 2, 3]}],
                            "paging": false,
                            "info": false,
                            "aaSorting": [[0, 'desc']],
                            "bFilter": false,
                            "oLanguage":
                                    {
                                        "sSearch": " Buscar: "
                                    }
                        });

                var idAreasCurso;
                var nmArea;
                var page;

                $("body").on("click", 'button[name=btnExcluir]', function ()
                {
                    idAreasCurso = $(this).attr('idAreasCurso');
                    nmArea = $(this).attr('nmArea');
                    $('#modal_delete_AreasCurso').html("Deseja excluir a Area <b>\"" + nmArea + "\"</b> ?");
                });

                $("body").on("click", 'button[name=btn_filtrar]', function ()
                {
                    var Busca = $(this).attr('Busca');
                    window.location.href = "home?Busca=" + Busca;
                });

                $('#modal_excluir').on('hidden.bs.modal', function ()
                {
                    window.location.replace("home");
                });


                $('#modal_btn-excluir').on("click", function ()
                {
                    var btn = $(this);
                    var excluir, txt;
                    btn.text('loading');
                    btn.removeClass('btn-primary');
                    btn.removeClass('btn-danger');
                    btn.removeClass('btn-success');
                    btn.addClass('btn-primary');
                    excluir = 'delete_area';
                    txt = 'Area excluída com sucesso';
                    $('#modal_btn-excluir').prop('disabled', true);
                    $.ajax(excluir,
                            {
                                type: "POST",
                                data: {idAreasCurso: idAreasCurso}
                            })
                            .done(function (data)
                            {
                                $('#modal_delete_AreasCurso').html(txt);

                                btn.removeClass('btn-primary');
                                btn.addClass('btn-success');
                            })
                            .fail(function ()
                            {
                                $('#modal_delete_AreasCurso').html('Erro: ' + data);
                                btn.removeClass('btn-primary');
                                btn.addClass('btn-danger');
                            })
                            .always(function ()
                            {
                                btn.text('Excluído');
                            });
                });

                $("body").on("click", "button[name=btnEditar]", function () {
                    idAreasCurso = $(this).attr('idAreasCurso');
                    page = $(this).attr('page');
                    window.location.href = "editar_area?idAreasCurso=" + idAreasCurso + "&page=" + page;
                });

                $("body").on("click", "button[name=btnAtivar]", function () {
                    idAreasCurso = $(this).attr('idAreasCurso');
                    $.get('update_ativar_area?idAreasCurso=' + idAreasCurso, function (data) {
                        if (data === "1") {
                            $('#btnAtivar_' + idAreasCurso).addClass('btn btn-success btn-xs');
                            $('#btnAtivar_' + idAreasCurso).removeClass('btn-danger');
                            $('#activ_' + idAreasCurso).removeClass('glyphicon-ban-circle');
                            $('#activ_' + idAreasCurso).addClass('glyphicon-ok');
                        } else {
                            $('#btnAtivar_' + idAreasCurso).addClass('btn btn-danger btn-xs');
                            $('#btnAtivar_' + idAreasCurso).removeClass('btn-success');
                            $('#activ_' + idAreasCurso).addClass('glyphicon-ok');
                            $('#activ_' + idAreasCurso).addClass('glyphicon-ban-circle');
                        }
                    });
                });


                $('#btn_novo_curso').on("click", function ()
                {
                    window.location.href = "cadastro_area";
                });


                $("body").on("click", "button[name=btnVisualizar]", function ()
                {
                    idAreasCurso = $(this).attr('idAreasCurso');
                    page = $(this).attr('page');
                    window.location.href = "visualizar_area?idAreasCurso=" + idAreasCurso + "&page=" + page;
                });

                $('.dataTables_filter label').addClass('pull-right');
            });
     </script>
    </body>
</html>
<?php
foreach ($data as $processo): 
    ?>
    <tr>
        <td style="text-align: center;vertical-align: middle;"><?= $processo['idProcesso']; ?></td>
        
        <td style="vertical-align: middle;"><?= $processo['nome']; ?></td>    
        
        <td style="vertical-align: middle;"><?= $processo['alias']; ?></td>

        <td style="text-align: center;vertical-align: middle;">
       
            <button id="btnVisualizar" class="btn btn-default btn-xs" name="btnVisualizar" page="<?= $PaginaAtual; ?>" idProcesso="<?= $processo['idProcesso']; ?>" title="Visualizar Processo" <?php if ($_SESSION['AcLiberaBtnVizPro'] == NULL) { ?>disabled<?php } ?>>
                <i class="glyphicon glyphicon-eye-open"></i>
            </button> 
            <button id="btnEditar" class="btn btn-default btn-xs" name="btnEditar" page="<?= $PaginaAtual; ?>" idProcesso="<?= $processo['idProcesso']; ?>" title="Editar Processo" <?php if ($_SESSION['AcLiberaBtnEdiPro'] == NULL) { ?>disabled<?php } ?>>
                <i class="glyphicon glyphicon-edit"></i>
            </button>

            <button id="btnExcluir" class="btn btn-default btn-xs" name="btnExcluir" page="<?= $PaginaAtual; ?>" idProcesso="<?= $processo['idProcesso']; ?>" nome="<?= $processo['nome']; ?>" <?php if ($_SESSION['AcLiberaBtnExcPro'] == NULL) { ?>disabled<?php } ?> data-toggle="modal" data-target="#modal_excluir" data-placement="bottom" title="Excluir Processo" data-delay="1">
                <i class="glyphicon glyphicon-trash"></i>
            </button>       
        </td>
        <td style="text-align: center;vertical-align: middle;">       
    <?php if ($processo['status'] == "0"): ?>
                <button id="btnAtivar_<?= $processo['idProcesso']; ?>" class="btn btn-danger btn-xs" idProcesso="<?= $processo['idProcesso']; ?>" name="btnAtivar" title="Ativar Processo" <?php if ($_SESSION['AcLiberaBtnAtivPro'] == NULL) { ?>disabled<?php } ?>>
                    <i id="activ_<?= $processo['idProcesso']; ?>" class="glyphicon glyphicon-ban-circle"></i>
                </button>
    <?php else: ?>
                <button id="btnAtivar_<?= $processo['idProcesso']; ?>" class="btn btn-success btn-xs" idProcesso="<?= $processo['idProcesso']; ?>" name="btnAtivar" title="Destivar Processo" <?php if ($_SESSION['AcLiberaBtnAtivPro'] == NULL) { ?>disabled<?php } ?>>
                    <i id="activ_<?= $processo['idProcesso']; ?>" class="glyphicon glyphicon-ok"></i>
                </button>
    <?php endif; ?>
        </td>
    </tr>
<?php endforeach; ?>
                                        

<?php
$model_tipoCurso = new Model_Grc_TipoCursos('default');
$model_formacao = new Model_Grc_Formacao('default');
$model_complemento = new Model_Grc_Complemento('default');
$model_coordenador = new Model_Grc_Coordenacao('default');
$coordenacao = $model_coordenador->select_coordenadores(); 
foreach ($data as $curso):  
    $tipoCurso = $model_tipoCurso->select_tipoCurso($curso['idTipoCurso']);
       ?>
    <tr>
        <td style="text-align: center;vertical-align: middle;"><?= $curso['idCursos']; ?></td>
        <td style="vertical-align: middle;"><?= $curso['nome']; ?></td>
        <td style="text-align: center;vertical-align: middle;"><?= $tipoCurso[0]['nome']; ?></td>
        <td style="text-align: center;vertical-align: middle;"><?= $curso['modalidade']; ?></td>
        <td style="text-align: center;vertical-align: middle;"><?= $curso['cargaHoraria']; ?> Horas</td>
        <td style="text-align: center;vertical-align: middle;">
            <?php
            
            for($i=0;$i<count($coordenacao);$i++):                
                $comp_coord = $model_complemento->select_complemento($curso['idCursos'],"coordenacao",$coordenacao[$i]['idCoordenacao']);
               
                if($comp_coord[0]['idReferencia']==null || $comp_coord[0]['idReferencia']==""){
                    continue; 
                }
                if(is_int($coordenacao[$i]['id_formacao'])){
                    $formacao = array($coordenacao[$i]['id_formacao']);
                }else{
                    $formacao = explode(",", $coordenacao[$i]['id_formacao']);
                }                
                $formac = "";
                $cont = count($formacao);
                foreach ($formacao as $value):
                    $fomacoes = $model_formacao->select_formacao($value);
                    $formac .= $fomacoes[0]['nome'];
                    if ($cont > 1):
                         $formac .= ", "; 
                     endif;                      
                  
                  $cont--;
                  endforeach;         
                echo $formac." ".$coordenacao[$i]['nome']."<br />";
            endfor;
       
            ?>
        </td>
        <td style="text-align: center;vertical-align: middle;">
       
            <button id="btnVisualizar" class="btn btn-default btn-xs" name="btnVisualizar" busca="<?= $busca; ?>" page="<?= $PaginaAtual; ?>" idCursos="<?= $curso['idCursos']; ?>" title="Visualizar Curso" <?php if ($_SESSION['AcLiberaBtnVizCur'] == NULL) { ?>disabled<?php } ?>>
                <i class="glyphicon glyphicon-eye-open"></i>
            </button> 
            <button id="btnVisualizar" class="btn btn-default btn-xs" name="btnCoordenacao" busca="<?= $busca; ?>" page="<?= $PaginaAtual; ?>" idCursos="<?= $curso['idCursos']; ?>" title="Adicionar Coordenadores" <?php if ($_SESSION['AcLiberaBtnVizCur'] == NULL) { ?>disabled<?php } ?>>
                <i class="glyphicon glyphicon-user"></i>
            </button> 
             <button id="btnVisualizar" class="btn btn-default btn-xs" name="btnInscricao" busca="<?= $busca; ?>" page="<?= $PaginaAtual; ?>" idCursos="<?= $curso['idCursos']; ?>" title="Inscrição e Seleção" <?php if ($_SESSION['AcLiberaBtnVizCur'] == NULL) { ?>disabled<?php } ?>>
                <i class="glyphicon glyphicon-book"></i>
            </button>
            <button id="btnVisualizar" class="btn btn-default btn-xs" name="btnMatricula" busca="<?= $busca; ?>" page="<?= $PaginaAtual; ?>" idCursos="<?= $curso['idCursos']; ?>" title="Matrícula" <?php if ($_SESSION['AcLiberaBtnVizCur'] == NULL) { ?>disabled<?php } ?>>
                <i class="glyphicon glyphicon-pencil"></i>
            </button>
            <button id="btnVisualizar" class="btn btn-default btn-xs" name="btnFinanciamento" busca="<?= $busca; ?>" page="<?= $PaginaAtual; ?>" idCursos="<?= $curso['idCursos']; ?>" title="Financiamento" <?php if ($_SESSION['AcLiberaBtnVizCur'] == NULL) { ?>disabled<?php } ?>>
                <i class="glyphicon glyphicon-usd"></i>
            </button>
            <button id="btnVisualizar" class="btn btn-default btn-xs" name="btnPedagogico" busca="<?= $busca; ?>" page="<?= $PaginaAtual; ?>" idCursos="<?= $curso['idCursos']; ?>" title="Informações Pedagogicas" <?php if ($_SESSION['AcLiberaBtnVizCur'] == NULL) { ?>disabled<?php } ?>>
                <i class="glyphicon glyphicon-folder-open"></i>
            </button>
            <button id="btnEditar" class="btn btn-default btn-xs" name="btnEditar" busca="<?= $busca; ?>" page="<?= $PaginaAtual; ?>" idCursos="<?= $curso['idCursos']; ?>" title="Editar Curso" <?php if ($_SESSION['AcLiberaBtnEdiCur'] == NULL) { ?>disabled<?php } ?>>
                <i class="glyphicon glyphicon-edit"></i>
            </button>

            <button id="btnExcluir" class="btn btn-default btn-xs" name="btnExcluir" busca="<?= $busca; ?>" page="<?= $PaginaAtual; ?>" idCursos="<?= $curso['idCursos']; ?>" nmCurso="<?= $curso['nome']; ?>" <?php if ($_SESSION['AcLiberaBtnExcCur'] == NULL) { ?>disabled<?php } ?> data-toggle="modal" data-target="#modal_excluir" data-placement="bottom" title="Excluir Curso" data-delay="1">
                <i class="glyphicon glyphicon-trash"></i>
            </button>       
        </td>
        <td style="text-align: center;vertical-align: middle;">       
    <?php if ($curso['status'] == "0"): ?>
                <button id="btnAtivar_<?= $curso['idCursos']; ?>" class="btn btn-danger btn-xs" idCursos="<?= $curso['idCursos']; ?>" name="btnAtivar" title="Ativar Curso" <?php if ($_SESSION['AcLiberaBtnAtivCur'] == NULL) { ?>disabled<?php } ?>>
                    <i id="activ_<?= $curso['idCursos']; ?>" class="glyphicon glyphicon-ban-circle"></i>
                </button>
    <?php else: ?>
                <button id="btnAtivar_<?= $curso['idCursos']; ?>" class="btn btn-success btn-xs" idCursos="<?= $curso['idCursos']; ?>" name="btnAtivar" title="Destivar Curso" <?php if ($_SESSION['AcLiberaBtnAtivCur'] == NULL) { ?>disabled<?php } ?>>
                    <i id="activ_<?= $curso['idCursos']; ?>" class="glyphicon glyphicon-ok"></i>
                </button>
    <?php endif; ?>
        </td>
    </tr>
<?php endforeach; ?>
                                        

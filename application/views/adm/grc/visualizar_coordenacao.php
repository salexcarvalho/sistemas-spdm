<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - Gerenciamento de Cursos</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">  
        <style>
            .table > thead > tr > th,
            .table > tbody > tr > th,
            .table > tfoot > tr > th,
            .table > thead > tr > td,
            .table > tbody > tr > td,
            .table > tfoot > tr > td {
                padding: 8px;
                padding-bottom: 6px;
                line-height: 1.42857143;
                vertical-align: bottom;
                border-bottom: 1px solid #ddd;
                border-top: 0px solid #ddd;
            }
            .breadcrumb2 {
                padding: 8px 15px;
                margin-bottom: 5px;
                list-style: none;
                background-color: #f5f5f5;
                border-radius: 4px;
            }
        </style>
    </head>
    <body>
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">                        
                        <h1 class="page-header">Coordenação de Cursos</h1>
                        <ol class="breadcrumb">
                            <li><i class="fa fa-file-pdf-o"></i> Coodenação</li>
                            <li class="active"><i class="fa fa-list"></i> Lista de Coordenadores</li>
                            <li class="active"><i class="glyphicon glyphicon-file"></i> Cadastro de Coordenadores</li>
                        </ol>
                    </div>
                </div>
                <div class="row">    
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Dados do Coordenador
                            </div>
                            <div class="panel-body">
                                <!-- Inicio da Tela de Coordenador -->
                                <table width="100%" class="table table-condensed">
                                    <tbody> 
                                        <tr>
                                            <td colspan="2"><label class="control-label">Nome:</label><br><?= $coordenacao[0]['nome'] ?></td></tr>                                                         
                                        <tr>
                                            <td><label class="control-label">RG:</label><br><?= $coordenacao[0]['RG'] ?></td>
                                            <td><label class="control-label">CPF:</label><br><?= $coordenacao[0]['CPF'] ?></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2"><label class="control-label">Titulação(ões):</label><br>
                                                <?php
                                                $forma = explode(",", $coordenacao[0]['id_formacao']);
                                                $cont = count($formacoes);
                                                foreach ($formacoes as $formacao):
                                                    if (in_array($formacao['idFormacao'], $forma)):
                                                        $cont = $cont - 1;
                                                        echo $formacao['nome'];
                                                        if ($cont > 0) {
                                                            echo ", ";
                                                        }
                                                    endif;
                                                endforeach;
                                                ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><label class="control-label">Endereço:</label><br><?= $coordenacao[0]['endereco'] ?></td>
                                            <td><label class="control-label">Telefone:</label><br><?= $coordenacao[0]['telefone'] ?></td>
                                        </tr>
                                        <tr>
                                            <td><label class="control-label">Celular:</label><br><?= $coordenacao[0]['celular'] ?></td>
                                            <td><label class="control-label">Lattes:</label><br>http://lattes.cnpq.br/<?= $coordenacao[0]['lattes'] ?></td>
                                        </tr>
                                        <tr>
                                            <td><label class="control-label">Email:</label><br><?= $coordenacao[0]['email'] ?></td>
                                            <td><label class="control-label">Senha:</label><br><?= $coordenacao[0]['senha'] ?></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2"><label class="control-label">Descrição:</label><br>
<?= $coordenacao[0]['descricao'] ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <label class="control-label">Status:</label>&nbsp;<?php if ($coordenacao[0]['status'] == 0): ?>Inativo<?php else: ?>Ativo<?php endif; ?>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="col-md-12">
                                    <a href="home" class="btn btn-danger">Voltar</a>
                                </div>                                   
                            </div>
                        </div>
                    </div>
                </div>
<?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>
                <script>

                    jQuery(document).ready(function () {
                        // Remove seleção de ativo no menu.
                        $('.nav li').removeClass('active');
                        $('#side-menu li').removeClass('active');
                        // Ativa botão no menu.
                        $('#li-gerenciamento ul').addClass('collapse in');
                        $('#li-cad-formacao-curso a').addClass('active');
                        $('#li-cursos').addClass('active');
                        $('#li-cursos a').addClass('collapse in');
                    });

                </script>        
                </body>
                </html>
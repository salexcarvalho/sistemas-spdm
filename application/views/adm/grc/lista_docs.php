<?php
foreach ($data as $docs): 
    ?>
    <tr>
        <td style="text-align: center;vertical-align: middle;"><?= $docs['idDocs']; ?></td>
        
        <td style="vertical-align: middle;"><?= $docs['nome']; ?></td>    
        
        <td style="vertical-align: middle;"><?= $docs['alias']; ?></td>

        <td style="text-align: center;vertical-align: middle;">
       
            <button id="btnVisualizar" class="btn btn-default btn-xs" name="btnVisualizar" page="<?= $PaginaAtual; ?>" idDocs="<?= $docs['idDocs']; ?>" title="Visualizar Documento" <?php if ($_SESSION['AcLiberaBtnVizDoc'] == NULL) { ?>disabled<?php } ?>>
                <i class="glyphicon glyphicon-eye-open"></i>
            </button> 
            <button id="btnEditar" class="btn btn-default btn-xs" name="btnEditar" page="<?= $PaginaAtual; ?>" idDocs="<?= $docs['idDocs']; ?>" title="Editar Documento" <?php if ($_SESSION['AcLiberaBtnEdiDoc'] == NULL) { ?>disabled<?php } ?>>
                <i class="glyphicon glyphicon-edit"></i>
            </button>

            <button id="btnExcluir" class="btn btn-default btn-xs" name="btnExcluir" page="<?= $PaginaAtual; ?>" idDocs="<?= $docs['idDocs']; ?>" nmDocso="<?= $docs['nome']; ?>" <?php if ($_SESSION['AcLiberaBtnExcDoc'] == NULL) { ?>disabled<?php } ?> data-toggle="modal" data-target="#modal_excluir" data-placement="bottom" title="Excluir Documento" data-delay="1">
                <i class="glyphicon glyphicon-trash"></i>
            </button>       
        </td>
        <td style="text-align: center;vertical-align: middle;">       
    <?php if ($docs['status'] == "0"): ?>
                <button id="btnAtivar_<?= $docs['idDocs']; ?>" class="btn btn-danger btn-xs" idDocs="<?= $docs['idDocs']; ?>" name="btnAtivar" title="Ativar Documento" <?php if ($_SESSION['AcLiberaBtnAtivDoc'] == NULL) { ?>disabled<?php } ?>>
                    <i id="activ_<?= $docs['idDocs']; ?>" class="glyphicon glyphicon-ban-circle"></i>
                </button>
    <?php else: ?>
                <button id="btnAtivar_<?= $docs['idDocs']; ?>" class="btn btn-success btn-xs" idDocs="<?= $docs['idDocs']; ?>" name="btnAtivar" title="Destivar Documento" <?php if ($_SESSION['AcLiberaBtnAtivDoc'] == NULL) { ?>disabled<?php } ?>>
                    <i id="activ_<?= $docs['idDocs']; ?>" class="glyphicon glyphicon-ok"></i>
                </button>
    <?php endif; ?>
        </td>
    </tr>
<?php endforeach; ?>
                                        

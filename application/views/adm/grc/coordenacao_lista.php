<?php
$model_formacao = new Model_Grc_Formacao('default');
$formac = "";
foreach ($data as $coordenacao):   

$forma = explode(",", $coordenacao['id_formacao']);
$cont = count($forma);
$formac ="";
foreach($forma as $formacao):
    $formacoes = $model_formacao->select_formacao($formacao);  
    $formac .= $formacoes[0]['nome'];
    if ($cont > 1):
      $formac .= ", ";
      endif;  
   $cont = $cont - 1;
endforeach;
    ?>
    <tr>
        <td style="text-align: center;vertical-align: middle;"><?= $coordenacao['idCoordenacao']; ?></td>
        <td style="text-align: center;vertical-align: middle;"><?= $coordenacao['nome']; ?></td>
        <td style="text-align: center;vertical-align: middle;"><?= $formac; ?></td>
        <td style="text-align: center;vertical-align: middle;"><?= $coordenacao['email']; ?></td>
        <td style="text-align: center;vertical-align: middle;"><?= $coordenacao['telefone']."<br />".$coordenacao['celular']; ?></td>
        <td style="text-align: center;vertical-align: middle;"><?= ($coordenacao['lattes']=="")? "":"<a href='http://lattes.cnpq.br/'".$coordenacao['lattes']."' target='_blak'><i class='glyphicon glyphicon-eye-open'></i></a>";?></td>
                <td style="text-align: center;vertical-align: middle;">
       
            <button id="btnVisualizar" class="btn btn-default btn-xs" name="btnVisualizar" page="<?= $PaginaAtual; ?>" idCoordenacao="<?= $coordenacao['idCoordenacao']; ?>" title="Visualizar Coordenação" <?php if ($_SESSION['AcLiberaBtnVizCoo'] == NULL) { ?>disabled<?php } ?>>
                <i class="glyphicon glyphicon-eye-open"></i>
            </button> 

            <button id="btnEditar" class="btn btn-default btn-xs" name="btnEditar" page="<?= $PaginaAtual; ?>" idCoordenacao="<?= $coordenacao['idCoordenacao']; ?>" title="Editar Coordenação" <?php if ($_SESSION['AcLiberaBtnEdiCoo'] == NULL) { ?>disabled<?php } ?>>
                <i class="glyphicon glyphicon-edit"></i>
            </button>

            <button id="btnExcluir" class="btn btn-default btn-xs" name="btnExcluir" page="<?= $PaginaAtual; ?>" idCoordenacao="<?= $coordenacao['idCoordenacao']; ?>" nome="<?= $coordenacao['nome'] ?>" <?php if ($_SESSION['AcLiberaBtnExcCoo'] == NULL) { ?>disabled<?php } ?> data-toggle="modal" data-target="#modal_excluir" data-placement="bottom" title="Excluir Coordenação" data-delay="1">
                <i class="glyphicon glyphicon-trash"></i>
            </button>       
        </td>
        <td style="text-align: center;vertical-align: middle;">       
    <?php if ($coordenacao['status'] == "0"): ?>
                <button id="btnAtivar_<?= $coordenacao['idCoordenacao']; ?>" class="btn btn-danger btn-xs" idCoordenacao="<?= $coordenacao['idCoordenacao']; ?>" name="btnAtivar" title="Ativar Coordenação" <?php if ($_SESSION['AcLiberaBtnAtivCoo'] == NULL) { ?>disabled<?php } ?>>
                    <i id="activ_<?= $coordenacao['idCoordenacao']; ?>" class="glyphicon glyphicon-ban-circle"></i>
                </button>
    <?php else: ?>
                <button id="btnAtivar_<?= $coordenacao['idCoordenacao']; ?>" class="btn btn-success btn-xs" idCoordenacao="<?= $coordenacao['idCoordenacao']; ?>" name="btnAtivar" title="Destivar Coordenação" <?php if ($_SESSION['AcLiberaBtnAtivCoo'] == NULL) { ?>disabled<?php } ?>>
                    <i id="activ_<?= $coordenacao['idCoordenacao']; ?>" class="glyphicon glyphicon-ok"></i>
                </button>
    <?php endif; ?>
        </td>
    </tr>
<?php endforeach; ?>                                        

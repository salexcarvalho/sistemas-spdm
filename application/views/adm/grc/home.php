<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - GERENCIAMENTO DE CURSOS</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <?php
        $model_cursos = new Model_Grc_Cursos('default');
        $cursos = $model_cursos->select_cursos();
        $busca = ($busca == "")? "":$busca;
        ?>

        <!-- Modal -->
        <div class="modal fade" id="modal_excluir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Confirmar Exclusão</h4>
                    </div>
                    <div class="modal-body" id="modal_delete_participante">Deseja excluir este curso?</div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        <button type="button" class="btn btn-primary btn-sm" id="modal_btn-excluir">Excluir</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal -->

        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>

            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Lista de Cursos</h1>
                        <ol class="breadcrumb">
                            <li><i class="fa fa-file-pdf-o"></i> Cursos</li>
                            <li class="active"><i class="fa fa-list"></i> Lista de cursos</li>
                        </ol>

                        <?php if (isset($warning)) : ?>
                            <div id="msg" class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong><?= $warning; ?></strong>
                            </div>
                        <?php endif ?>                          

                        <div class="row" style="margin-bottom:10px; margin-top: -10px;">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <form class="form-inline" method="get" action="home">                      
                                        <input  type="text" style="height: 30px; width: 260px;" class="form-control" id="Busca" name="Busca" placeholder="Informe o Nome ou ID">
                                        <input type="hidden" id="busca" name="busca" value="<?= $busca ?>"/>
                                        <button type="submit" class="btn btn-danger btn-sm"><i class="glyphicon glyphicon-search"></i> Buscar</button>
                                    </form>
                                </div>    
                            </div>

                            <div class="col-md-9">
                                <?php if ($_SESSION['AcLiberaBtnAddCur'] == true) { ?>
                                    <a href="cadastro_curso">
                                        <button type="submit" id="btn_novo_curso" class="pull-right btn btn-primary btn-sm" name="btn_novo_curso"><i class="glyphicon glyphicon-plus-sign"></i>&nbsp;Adicionar</button>
                                    </a>
                                <?php } ?>
                            </div>
                        </div>      

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <span>Cursos </span>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">

                                    <table class="table table-striped table-bordered table-hover" id="participantes">
                                        <thead>
                                            <tr>
                                                <th width="10%" style="text-align: center;vertical-align: middle;">ID</th>
                                                <th width="25%" style="text-align: center;vertical-align: middle;">Nome do Curso</th>
                                                <th width="5%" style="text-align: center;vertical-align: middle;">Tipo</th>
                                                <th width="5%" style="text-align: center;vertical-align: middle;">Modalidade</th>
                                                <th width="5%" style="text-align: center;vertical-align: middle;">Carga Horária</th>
                                                <th width="25%" style="text-align: center;vertical-align: middle;">Coordenadores</th>   
                                                <th width="25%" style="text-align: center">Ações Cursos</th>
                                                <th width="5%" style="text-align: center">Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php include kohana::find_file('views', 'adm/grc/home_lista') ?>
                                        </tbody>    
                                    </table>
                                    <?php include(kohana::find_file('views/paginator', 'paginacao', 'php')) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>

        <script>
            jQuery(document).ready(function () {
                // Remove seleção de ativo no menu.
                $('.nav li').removeClass('active');
                $('#side-menu li').removeClass('active');
                // Ativa botão no menu.
                $('#li-gerenciamento ul').addClass('collapse in');
                $('#li-cad-cursos a').addClass('active');
                $('#li-cursos').addClass('active');
                $('#li-cursos a').addClass('collapse in');

                $('#cursos').dataTable(
                        {
                            "aoColumnDefs": [{"bSortable": false, "aTargets": [6, 7, 8]}],
                            "paging": false,
                            "info": false,
                            "aaSorting": [[0, 'desc']],
                            "bFilter": false,
                            "oLanguage":
                                    {
                                        "sSearch": " Buscar: "
                                    }
                        });

                var idCursos;
                var Nome;
                var page;

                $("body").on("click", 'button[name=btnExcluir]', function ()
                {
                    Nome = $(this).attr('nome');
                    idCursos = $(this).attr('idCursos');
                    $('#modal_delete_curso').html("Deseja excluir o participante <b>\"" + Nome + "\"</b> ?");
                });

                $("body").on("click", 'button[name=btn_filtrar]', function ()
                {
                    var Busca = $(this).attr('Busca');
                    window.location.href = "home?busca=" + Busca;
                });

                $('#modal_excluir').on('hidden.bs.modal', function ()
                {
                    window.location.replace("home");
                });


                $('#modal_btn-excluir').on("click", function ()
                {
                    var btn = $(this);
                    var excluir, txt;
                    btn.text('loading');
                    btn.removeClass('btn-primary');
                    btn.removeClass('btn-danger');
                    btn.removeClass('btn-success');
                    btn.addClass('btn-primary');
                    excluir = 'delete_cursos';
                    txt = 'Curso excluído com sucesso';
                    $('#modal_btn-excluir').prop('disabled', true);
                    $.ajax(excluir,
                            {
                                type: "POST",
                                data: {idCursos: idCursos}
                            })
                            .done(function (data)
                            {
                                $('#modal_delete_curso').html(txt);

                                btn.removeClass('btn-primary');
                                btn.addClass('btn-success');
                            })
                            .fail(function ()
                            {
                                $('#modal_delete_curso').html('Erro: ' + data);
                                btn.removeClass('btn-primary');
                                btn.addClass('btn-danger');
                            })
                            .always(function ()
                            {
                                btn.text('Excluído');
                            });
                });

                $("body").on("click", "button[name=btnEditar]", function () {
                    idCursos = $(this).attr('idCursos');
                    page = $(this).attr('page');
                    busca = $(this).attr('busca');
                    window.location.href = "editar_curso?idCursos=" + idCursos + "&page=" + page+"&busca="+busca;
                });

                $("body").on("click", "button[name=btnAtivar]", function () {
                    idCursos = $(this).attr('idCursos');
                  
                    $.get('update_ativar_curso?idCursos=' + idCursos, function (data) {
                        if (data === "1") {
                            $('#btnAtivar_' + idCursos).addClass('btn btn-success btn-xs');
                            $('#btnAtivar_' + idCursos).removeClass('btn-danger');
                            $('#activ_' + idCursos).removeClass('glyphicon-ban-circle');
                            $('#activ_' + idCursos).addClass('glyphicon-ok');
                        } else {
                            $('#btnAtivar_' + idCursos).addClass('btn btn-danger btn-xs');
                            $('#btnAtivar_' + idCursos).removeClass('btn-success');
                            $('#activ_' + idCursos).addClass('glyphicon-ok');
                            $('#activ_' + idCursos).addClass('glyphicon-ban-circle');
                        }
                    });
                });


                $('#btn_novo_curso').on("click", function ()
                {
                    window.location.href = "cadastro_curso";
                });


                $("body").on("click", "button[name=btnVisualizar]", function ()
                {
                    idCursos = $(this).attr('idCursos');
                    page = $(this).attr('page');
                    busca = $(this).attr('busca');
                    window.location.href = "visualizar_curso?idCursos=" + idCursos + "&page=" + page+"&busca="+busca;
                });
                
                $("body").on("click", "button[name=btnCoordenacao]", function ()
                {
                    idCursos = $(this).attr('idCursos');
                    page = $(this).attr('page');
                    busca = $(this).attr('busca');
                    window.location.href = "coordenador_curso?idCursos=" + idCursos + "&page=" + page+"&busca="+busca;
                });
                $("body").on("click", "button[name=btnInscricao]", function ()
                {
                    idCursos = $(this).attr('idCursos');
                    page = $(this).attr('page');
                    busca = $(this).attr('busca');
                    window.location.href = "inscricao_curso?idCursos=" + idCursos + "&page=" + page+"&busca="+busca;
                });
                $("body").on("click", "button[name=btnMatricula]", function ()
                {
                    idCursos = $(this).attr('idCursos');
                    page = $(this).attr('page');
                    busca = $(this).attr('busca');
                    window.location.href = "matricula_curso?idCursos=" + idCursos + "&page=" + page+"&busca="+busca;
                });
                $("body").on("click", "button[name=btnFinanceiro]", function ()
                {
                    idCursos = $(this).attr('idCursos');
                    page = $(this).attr('page');
                    busca = $(this).attr('busca');
                    window.location.href = "financeiro_curso?idCursos=" + idCursos + "&page=" + page+"&busca="+busca;
                });
                $("body").on("click", "button[name=btnPedagogico]", function ()
                {
                    idCursos = $(this).attr('idCursos');
                    page = $(this).attr('page');
                    busca = $(this).attr('busca');
                    window.location.href = "pedagogico_curso?idCursos=" + idCursos + "&page=" + page+"&busca="+busca;
                });

                $('.dataTables_filter label').addClass('pull-right');
            });
        </script> 

    </body>
</html>
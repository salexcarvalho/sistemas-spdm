<?php
foreach ($data as $area):    
    ?>
    <tr>
        <td style="text-align: center;vertical-align: middle;"><?= $area['idAreasCurso']; ?></td>
        <td style="vertical-align: middle;"><?= $area['nmArea']; ?></td>
                <td style="text-align: center;vertical-align: middle;">
       
            <button id="btnVisualizar" class="btn btn-default btn-xs" name="btnVisualizar" page="<?= $PaginaAtual; ?>" idAreasCurso="<?= $area['idAreasCurso']; ?>" title="Visualizar Area" <?php if ($_SESSION['AcLiberaBtnVizAre'] == NULL) { ?>disabled<?php } ?>>
                <i class="glyphicon glyphicon-eye-open"></i>
            </button> 

            <button id="btnEditar" class="btn btn-default btn-xs" name="btnEditar" page="<?= $PaginaAtual; ?>" idAreasCurso="<?= $area['idAreasCurso']; ?>" title="Editar Area" <?php if ($_SESSION['AcLiberaBtnEdiAre'] == NULL) { ?>disabled<?php } ?>>
                <i class="glyphicon glyphicon-edit"></i>
            </button>

            <button id="btnExcluir" class="btn btn-default btn-xs" name="btnExcluir" page="<?= $PaginaAtual; ?>" idAreasCurso="<?= $area['idAreasCurso']; ?>" nmArea="<?= $area['nmArea'] ?>" <?php if ($_SESSION['AcLiberaBtnExcAre'] == NULL) { ?>disabled<?php } ?> data-toggle="modal" data-target="#modal_excluir" data-placement="bottom" title="Excluir Area" data-delay="1">
                <i class="glyphicon glyphicon-trash"></i>
            </button>       
        </td>
        <td style="text-align: center;vertical-align: middle;">       
    <?php if ($area['status'] == "0"): ?>
                <button id="btnAtivar_<?= $area['idAreasCurso']; ?>" class="btn btn-danger btn-xs" idAreasCurso="<?= $area['idAreasCurso']; ?>" name="btnAtivar" title="Ativar Area" <?php if ($_SESSION['AcLiberaBtnAtivAre'] == NULL) { ?>disabled<?php } ?>>
                    <i id="activ_<?= $area['idAreasCurso']; ?>" class="glyphicon glyphicon-ban-circle"></i>
                </button>
    <?php else: ?>
                <button id="btnAtivar_<?= $area['idAreasCurso']; ?>" class="btn btn-success btn-xs" idAreasCurso="<?= $area['idAreasCurso']; ?>" name="btnAtivar" title="Destivar Area" <?php if ($_SESSION['AcLiberaBtnAtivAre'] == NULL) { ?>disabled<?php } ?>>
                    <i id="activ_<?= $area['idAreasCurso']; ?>" class="glyphicon glyphicon-ok"></i>
                </button>
    <?php endif; ?>
        </td>
    </tr>
<?php endforeach; ?>
                                        

<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - GERENCIAMENTO DE CURSOS</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <?php $busca = (isset($busca))? $busca:"";?>
       <!-- Modal -->
        <div class="modal fade" id="modal_excluir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Confirmar Exclusão</h4>
                    </div>
                    <div class="modal-body" id="modal_delete_participante">Deseja excluir este processo?</div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        <button type="button" class="btn btn-primary btn-sm" id="modal_btn-excluir">Excluir</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal -->

        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            
            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Isenções e Descontos</h1>
                        <ol class="breadcrumb">
                            <li><i class="fa fa-file-pdf-o"></i> Isenções e Descontos</li>
                            <li class="active"><i class="fa fa-list"></i> Lista de Isenções e Descontos</li>
                        </ol>

                        <?php if (isset($warning)) : ?>
                            <div id="msg" class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong><?= $warning; ?></strong>
                            </div>
                        <?php endif ?>                          

                        <div class="row" style="margin-bottom:10px; margin-top: -10px;">
                            <div class="col-lg-12">
                                <?php if ($_SESSION['AcLiberaBtnAddIse'] == true) { ?>
                                    <a href="cad_isencao">
                                        <button type="submit" id="btn_novo_curso" class="pull-right btn btn-primary btn-sm" name="btn_novo_isencao"><i class="glyphicon glyphicon-plus-sign"></i>&nbsp;Adicionar</button>
                                    </a>
                                <?php } ?>
                            </div>
                        </div>      

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <span>Isencaos </span>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">

                                    <table class="table table-striped table-bordered table-hover" id="tipo">
                                        <thead>
                                            <tr>
                                                <th width="20%" style="text-align: center">ID</th>
                                                <th width="20%" style="text-align: center">Nome</th>
                                                <th width="20%" style="text-align: center">Tipo</th>
                                                <th width="20%" style="text-align: center">valor</th>
                                                <th width="20%" style="text-align: center">Ações Processos</th>
                                                <th width="20%" style="text-align: center">Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php include kohana::find_file('views', 'adm/grc/lista_isencao') ?>
                                        </tbody>    
                                    </table>
                                    <?php include(kohana::find_file('views/paginator', 'paginacao', 'php')) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>

        <script>
            jQuery(document).ready(function () {
                // Remove seleção de ativo no menu.
                $('.nav li').removeClass('active');
                $('#side-menu li').removeClass('active');
                // Ativa botão no menu.
                $('#li-gerenciamento ul').addClass('collapse in');
                $('#li-cad-financ-curso a').addClass('active');
                $('#li-cursos').addClass('active');
                $('#li-cursos a').addClass('collapse in');
                           
               $('#tipo').dataTable(
                        {
                            "aoColumnDefs": [{"bSortable": false, "aTargets": [4,5]}],
                            "paging": false,
                            "info": false,
                            "aaSorting": [[0, 'desc']],
                            "bFilter": false,
                            "oLanguage":
                                    {
                                        "sSearch": " Buscar: "
                                    }
                        });

                var idIsencao;
                var nome;
                var page;

                $("body").on("click", 'button[name=btnExcluir]', function ()
                {
                    idIsencao = $(this).attr('idIsencao');
                    nome = $(this).attr('nome');
                    $('#modal_delete_isencao').html("Deseja excluir o Isencao <b>\"" + nome + "\"</b> ?");
                });

                $("body").on("click", 'button[name=btn_filtrar]', function ()
                {
                    var Busca = $(this).attr('Busca');
                    window.location.href = "home?Busca=" + Busca;
                });

                $('#modal_excluir').on('hidden.bs.modal', function ()
                {
                    window.location.replace("isencao");
                });


                $('#modal_btn-excluir').on("click", function ()
                {
                    var btn = $(this);
                    var excluir, txt;
                    btn.text('loading');
                    btn.removeClass('btn-primary');
                    btn.removeClass('btn-danger');
                    btn.removeClass('btn-success');
                    btn.addClass('btn-primary');
                    excluir = 'delete_isencao';
                    txt = 'Isencao excluído com sucesso';
                    $('#modal_btn-excluir').prop('disabled', true);
                    $.ajax(excluir,
                            {
                                type: "POST",
                                data: {idIsencao: idIsencao}
                            })
                            .done(function (data)
                            {
                                $('#modal_delete_isencao').html(txt);

                                btn.removeClass('btn-primary');
                                btn.addClass('btn-success');
                            })
                            .fail(function ()
                            {
                                $('#modal_delete_isencao').html('Erro: ' + data);
                                btn.removeClass('btn-primary');
                                btn.addClass('btn-danger');
                            })
                            .always(function ()
                            {
                                btn.text('Excluído');
                            });
                });

                $("body").on("click", "button[name=btnEditar]", function () {
                    idIsencao = $(this).attr('idIsencao');
                    page = $(this).attr('page');
                    window.location.href = "editar_isencao?idIsencao=" + idIsencao + "&page=" + page;
                });

                $("body").on("click", "button[name=btnAtivar]", function () {
                    idIsencao = $(this).attr('idIsencao');
                    $.get('update_ativar_isencao?idIsencao=' + idIsencao, function (data) {
                        if (data === "1") {
                            $('#btnAtivar_' + idIsencao).addClass('btn btn-success btn-xs');
                            $('#btnAtivar_' + idIsencao).removeClass('btn-danger');
                            $('#activ_' + idIsencao).removeClass('glyphicon-ban-circle');
                            $('#activ_' + idIsencao).addClass('glyphicon-ok');
                        } else {
                            $('#btnAtivar_' + idIsencao).addClass('btn btn-danger btn-xs');
                            $('#btnAtivar_' + idIsencao).removeClass('btn-success');
                            $('#activ_' + idIsencao).addClass('glyphicon-ok');
                            $('#activ_' + idIsencao).addClass('glyphicon-ban-circle');
                        }
                    });
                });


                $('#btn_novo_isencao').on("click", function ()
                {
                    window.location.href = "cad_isencao";
                });


                $("body").on("click", "button[name=btnVisualizar]", function ()
                {
                    idIsencao = $(this).attr('idIsencao');
                    page = $(this).attr('page');
                    window.location.href = "visualizar_isencao?idIsencao=" + idIsencao + "&page=" + page;
                });

                $('.dataTables_filter label').addClass('pull-right');
            });
        </script>         
    </body>
</html>
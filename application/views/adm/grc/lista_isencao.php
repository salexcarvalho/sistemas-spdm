<?php
foreach ($data as $isencao): 
    ?>
    <tr>
        <td style="text-align: center"><?= $isencao['idIsencao']; ?></td>
        
        <td style="text-align: center"><?= $isencao['nome']; ?></td>    
        <td style="text-align: center"><?php if($isencao['tipo']==0){echo "Inscrição";}else if($isencao['tipo']==1){echo "Matrícula";}else{echo "Mensalidade";} ?></td>
        <td style="text-align: center"><?= $isencao['valor']; ?> %</td>

        <td style="text-align: center">
       
            <button id="btnVisualizar" class="btn btn-default btn-xs" name="btnVisualizar" page="<?= $PaginaAtual; ?>" idIsencao="<?= $isencao['idIsencao']; ?>" title="Visualizar Isencao" <?php if ($_SESSION['AcLiberaBtnVizIse'] == NULL) { ?>disabled<?php } ?>>
                <i class="glyphicon glyphicon-eye-open"></i>
            </button> 
            <button id="btnEditar" class="btn btn-default btn-xs" name="btnEditar" page="<?= $PaginaAtual; ?>" idIsencao="<?= $isencao['idIsencao']; ?>" title="Editar Isencao" <?php if ($_SESSION['AcLiberaBtnEdiIse'] == NULL) { ?>disabled<?php } ?>>
                <i class="glyphicon glyphicon-edit"></i>
            </button>

            <button id="btnExcluir" class="btn btn-default btn-xs" name="btnExcluir" page="<?= $PaginaAtual; ?>" idIsencao="<?= $isencao['idIsencao']; ?>" nome="<?= $isencao['nome']; ?>" <?php if ($_SESSION['AcLiberaBtnExcIse'] == NULL) { ?>disabled<?php } ?> data-toggle="modal" data-target="#modal_excluir" data-placement="bottom" title="Excluir Isencao" data-delay="1">
                <i class="glyphicon glyphicon-trash"></i>
            </button>       
        </td>
        <td style="text-align: center">       
    <?php if ($isencao['status'] == "0"): ?>
                <button id="btnAtivar_<?= $isencao['idIsencao']; ?>" class="btn btn-danger btn-xs" idIsencao="<?= $isencao['idIsencao']; ?>" name="btnAtivar" title="Ativar Isencao" <?php if ($_SESSION['AcLiberaBtnAtivIse'] == NULL) { ?>disabled<?php } ?>>
                    <i id="activ_<?= $isencao['idIsencao']; ?>" class="glyphicon glyphicon-ban-circle"></i>
                </button>
    <?php else: ?>
                <button id="btnAtivar_<?= $isencao['idIsencao']; ?>" class="btn btn-success btn-xs" idIsencao="<?= $isencao['idIsencao']; ?>" name="btnAtivar" title="Destivar Isencao" <?php if ($_SESSION['AcLiberaBtnAtivIse'] == NULL) { ?>disabled<?php } ?>>
                    <i id="activ_<?= $isencao['idIsencao']; ?>" class="glyphicon glyphicon-ok"></i>
                </button>
    <?php endif; ?>
        </td>
    </tr>
<?php endforeach; ?>
                                        

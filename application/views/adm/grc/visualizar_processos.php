<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - Gerenciamento de Cursos</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">  
        <style>
            .table > thead > tr > th,
            .table > tbody > tr > th,
            .table > tfoot > tr > th,
            .table > thead > tr > td,
            .table > tbody > tr > td,
            .table > tfoot > tr > td {
                padding: 8px;
                padding-bottom: 6px;
                line-height: 1.42857143;
                vertical-align: bottom;
                border-bottom: 1px solid #ddd;
                border-top: 0px solid #ddd;
            }
            .breadcrumb2 {
                padding: 8px 15px;
                margin-bottom: 5px;
                list-style: none;
                background-color: #f5f5f5;
                border-radius: 4px;
            }
        </style>
    </head>
    <body>
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">                        
                         <h1 class="page-header"> Processos Seletivo</h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-file-pdf-o"></i> Processos</li>
                                <li><a href="home"><i class="fa fa-list"></i> Lista de Processos</a></li>
                                <li class="active"><i class="glyphicon glyphicon-file"></i> Visualizar Processo</li>
                            </ol>
                    </div>
                </div>
                <div class="row">    
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Dados do Processo
                            </div>
                            <div class="panel-body">
                                <!-- Inicio da Tela de Coordenador -->
                                <table width="100%" class="table table-condensed">
                                    <tbody> 
                                        <tr>
                                            <td colspan="2"><label class="control-label">Nome:</label><br><?= $processos[0]['nome'] ?></td></tr>                                                         
                                        <tr>
                                            <td><label class="control-label">Alias:</label><br><?= $processos[0]['alias'] ?></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <label class="control-label">Status:</label>&nbsp;<?php if ($processos[0]['status'] == 0): ?>Inativo<?php else: ?>Ativo<?php endif; ?>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="col-md-12">
                                    <a href="home" class="btn btn-danger">Voltar</a>
                                </div>                                   
                            </div>
                        </div>
                    </div>
                </div>
<?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>
                <script>

                    jQuery(document).ready(function () {
                        // Remove seleção de ativo no menu.
                        $('.nav li').removeClass('active');
                        $('#side-menu li').removeClass('active');
                        // Ativa botão no menu.
                        $('#li-gerenciamento ul').addClass('collapse in');
                        $('#li-cad-inscr-sel a').addClass('active');
                        $('#li-cursos').addClass('active');
                        $('#li-cursos a').addClass('collapse in');
                    });

                </script>        
                </body>
                </html>
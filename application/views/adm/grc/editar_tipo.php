<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - Gerenciamento de Cursos</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <?php if ($_SESSION['AcLiberaBtnAddFor'] == true) { ?>
                            <h1 class="page-header"> Tipos de Cursos</h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-file-pdf-o"></i> Tipos</li>
                                <li><a href="home"><i class="fa fa-list"></i> Lista de tipos de cursos</a></li>
                                <li class="active"><i class="glyphicon glyphicon-file"></i> Cadastro de tipos de cursos</li>
                            </ol>
                            <form class="form-horizontal" role="form" method="POST" action="update_formacao">
                                <div class="form-group">    
                                    <div class="col-lg-9">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <span>Dados do Tipo</span>
                                            </div>
                                            <div class="panel-body">                                               
                                                <div class="form-group">
                                                    <label for="nmArea" class="col-md-2 control-label">Tipo </label>
                                                    <div class="col-md-7">
                                                        <input type="hidden" id="idTipoCurso" name="idTipoCurso" value="<?=$tipoCurso[0]['nome']?>">
                                                        <input type="text" class="form-control" id="nmArea" name="nome" value="<?=$tipoCurso[0]['nome']?>" required>
                                                    </div>
                                                </div>                                                 
                                                <div class="form-group"> 
                                                    <label for="status" class="col-lg-2 control-label">Status</label>
                                                     <div class="col-lg-10">    
                                                            <label class="radio-inline">
                                                                <input required type="radio" name="status" id="inativo" value="0" <?php if($tipoCurso[0]['status']==0):?>checked="checked"<?php endif;?>>
                                                                Inativo
                                                            </label>
                                                            <label class="radio-inline">
                                                                <input required type="radio" name="status" id="inativo" value="1" <?php if($tipoCurso[0]['status']==1):?>checked="checked"<?php endif;?>>
                                                                Ativo
                                                            </label>
                                                     </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-7">
                                                <button type="submit" class="btn btn-primary btn-sm">Atualizar</button>
                                                <a href="home" class="btn btn-danger">Voltar</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        <?php } else { ?>
                            <h1 class="page-header"> Voce não possue acesso a está pagina, consulte o administrador.</h1>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>
        <script>
            jQuery(document).ready(function () {
                 // Remove seleção de ativo no menu.
                $('.nav li').removeClass('active');
                $('#side-menu li').removeClass('active');
                // Ativa botão no menu.
                $('#li-gerenciamento ul').addClass('collapse in');
                $('#li-cad-formacao-curso a').addClass('active');
                $('#li-cursos').addClass('active');
                $('#li-cursos a').addClass('collapse in');
            });   
            
        </script>        
    </body>
</html>
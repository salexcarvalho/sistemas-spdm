<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - Gerenciamento de Cursos</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <?php if ($_SESSION['AcLiberaBtnAddCur'] == true) { ?>
                            <h1 class="page-header"> Cadastro de Curso</h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-file-pdf-o"></i> Gerenciamento de Cursos</li>
                                <li><a href="home"><i class="fa fa-list"></i> Lista de cursos</a></li>
                                <li class="active"><i class="glyphicon glyphicon-file"></i> Cadastro de curso</li>
                            </ol>
                            <!-- Inicio Informações Curso-->
                            <form class="form-horizontal" role="form" method="POST" action="insert_curso">
                                <div class="form-group">    
                                    <div class="col-md-10">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <span>Dados do Curso</span>
                                            </div>
                                            <div class="panel-body"> 

                                                <div class="form-group">
                                                    <label for="nome" class="col-md-2 control-label">Nome</label>
                                                    <div class="col-md-10">
                                                        <input type="text" class="form-control" id="nome" name="nome" placeholder="Informe o nome do curso" required>
                                                    </div>
                                                </div>                                            
                                                <div class="form-group">
                                                    <label for="cargaHoraria" class="col-md-2 control-label">Carga Horária</label>
                                                    <div class="col-md-3">
                                                        <input type="cargaHoraria" class="form-control" id="cargaHoraria" name="cargaHoraria" placeholder="Informe o Carga Horária em horas">
                                                    </div>

                                                    <?php
                                                    $model_tipoCurso = new Model_Grc_TipoCursos('default');
                                                    $tipoCurso = $model_tipoCurso->select_tipoCursos();
                                                    ?>

                                                    <label for="id_TipoCurso" class="col-md-2 control-label">Tipo Curso</label>
                                                    <div class="col-md-5">
                                                        <select id="idTipoCurso" name="idTipoCurso" class="form-control" required>
                                                            <option value="">Selecione o Tipo do Curso</option>
                                                            <?php foreach ($tipoCurso as $tipo) {
                                                                ?>
                                                                <option value="<?= $tipo['idTipoCurso']; ?>"><?= $tipo['nome']; ?></option>    
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="cargaTeorica" class="col-md-2 control-label">Carga Teórica(%)</label>
                                                    <div class="col-md-3">
                                                        <input type="text" class="form-control" id="cargaTeorica" name="cargaTeorica" placeholder="Informe a Carga Teórica" required>
                                                    </div>

                                                    <label for="cargaPratica" class="col-md-2 control-label">Carga Prática(%)</label>
                                                    <div class="col-md-3">
                                                        <input type="text" class="form-control" id="cargaPratica" name="cargaPratica" placeholder="Informe a Carga Prática" readonly="true">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="minAlu" class="col-md-2 control-label">Numero Mínimo de Alunos*</label>
                                                    <div class="col-md-3">
                                                        <input type="text" class="form-control" id="minAlu" name="minAlu" placeholder="Informe o numero mínimo de Alunos" required>
                                                    </div>

                                                    <label for="maxAlu" class="col-md-2 control-label">Numero Máximo de Alunos</label>
                                                    <div class="col-md-3">
                                                        <input type="text" class="form-control" id="maxAlu" name="maxAlu" placeholder="Informe o numero máximo de Alunos" required>
                                                    </div>
                                                </div>
                                                <div class="form-group"> 
                                                    <label for="modalidade" class="col-md-2 control-label">Modalidade</label>
                                                    <div class="col-md-3">    
                                                        <label class="radio-inline">
                                                            <input type="checkbox" name="modalidade[]"  value="Presencial">
                                                            Presencial
                                                        </label>
                                                        <label class="radio-inline">
                                                            <input type="checkbox" name="modalidade[]"  value="Ead">
                                                            à Distância(EAD)
                                                        </label>
                                                    </div>
                                                </div> 
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group">    
                                    <div class="col-md-5">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <span>Areas do Conhecimento da Saúde:</span>
                                            </div>
                                            <div class="panel-body">
                                                <div class="form-group"> 
                                                    <div class="col-md-5">
                                                        <?php foreach ($areas as $area): ?>
                                                            <label class="radio-inline">
                                                                <input type="checkbox" name="area[]" value="<?= $area["idAreasCurso"] ?>"> <?= $area["nmArea"] ?>
                                                            </label><br />
                                                        <?php endforeach; ?>
                                                        <label class="radio-inline">
                                                            <input type="checkbox" id="outrosA" name="outrosA"  value="1"> Outros                                                           
                                                        </label>                                                             
                                                    </div>

                                                    <label for="espA" class="col-md-2 control-label">Especificar:</label>
                                                    <div class="col-md-3">
                                                        <input type="text" class="form-control" id="espA" name="espA" placeholder="Separar por ," readonly="true">
                                                    </div>
                                                </div> 
                                            </div>                                                
                                        </div> 
                                    </div>
                                    <div class="col-md-5">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <span>Alunado:</span>
                                            </div>
                                            <div class="panel-body">
                                                <div class="form-group"> 
                                                    <div class="col-md-5">
                                                        <?php foreach ($formacao as $form): ?>
                                                            <label class="radio-inline">
                                                                <input type="checkbox" name="formacao[]" value="<?= $form["idFormacao"] ?>"> <?= $form["nome"] ?>
                                                            </label><br />
                                                        <?php endforeach; ?>                                                 

                                                        <label class="radio-inline">
                                                            <input type="checkbox" id="outrosF" name="outrosF" value="1"> Outros <br/>
                                                        </label>                                                            
                                                    </div>
                                                    <label for="espF" class="col-md-2 control-label">Especificar:</label>
                                                    <div class="col-md-4">
                                                        <input type="text" class="form-control" id="espF" name="espF" placeholder="Separar por ," readonly="true">
                                                    </div>
                                                </div> 
                                            </div>
                                        </div> 
                                    </div>
                                    <div class="col-md-10">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <span>Periodicidade do Curso:<?=$_SESSION['idUsuario'];?></span>
                                            </div>
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <div class="col-md-5">
                                                        <?php foreach ($periodicidade as $periodo): ?>
                                                            <label class="radio-inline">
                                                                <input type="checkbox" name="periodicidade[]"  value="<?= $periodo["idPeriodicidade"] ?>"> <?= $periodo["nome"] ?>
                                                            </label>
                                                        <?php endforeach; ?> 
                                                        <label class="radio-inline">
                                                            <input type="checkbox" id="outrosP" name="outrosP" value="1"> Outros <br/>

                                                        </label>   
                                                    </div> 
                                                    <div class="col-md-5">
                                                        <label for="espP" class="col-md-2 control-label">Especificar:</label>
                                                        <div class="col-md-5">
                                                            <input type="text" class="form-control" id="espP" name="espP" placeholder="Separar por ," readonly="true">
                                                        </div> 
                                                    </div>                                             
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-5">
                                                        <label for="oferta" class="col-md-4 control-label">Ano(s) de Oferta:</label>
                                                        <div class="col-md-6">
                                                            <input type="text" class="form-control" id="oferta" name="oferta" placeholder="Informe os anos de Oferta">
                                                        </div>
                                                    </div>                                                        
                                                    <div class="col-md-5">
                                                        <label for="semestre" class="col-md-4 control-label">Semestre(s):</label>
                                                        <div class="col-md-6">
                                                            <input type="text" class="form-control" id="semestre" name="semestre" placeholder="Informe a Duração em semestres">
                                                        </div>
                                                    </div>                                                        
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-5">
                                                        <label for="dataIncio" class="col-md-4 control-label">Inicio:</label>
                                                        <div class="col-md-6">
                                                            <input type="text" class="form-control" id="dataIncio" name="dataInicio" placeholder="Informar data de Início do curso">
                                                        </div>
                                                    </div>                                                        
                                                    <div class="col-md-5">
                                                        <label for="dataFim" class="col-md-4 control-label">Encerramento:</label>
                                                        <div class="col-md-6">
                                                            <input type="text" class="form-control" id="dataFim" name="dataFim" placeholder="Informar data de Encerramento do curso">
                                                        </div>
                                                    </div>                                                     
                                                </div>
                                                 <div class="col-lg-offset-2 col-lg-5">
                                                    <div class="form-group"> 
                                                        <label class="radio-inline">
                                                            <input required type="radio" name="status" id="inativo" value="1" />
                                                            Ativo
                                                        </label>
                                                        <label class="radio-inline">                                                    
                                                            <input required type="radio" name="status" id="inativo" value="0" />
                                                            Inativo
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                <div class="form-group">
                                    <div class="col-md-7">
                                        <button type="submit" class="btn btn-primary btn-sm">Cadastrar</button>
                                        <a href="home" class="btn btn-danger">Voltar</a>
                                    </div>
                                </div>
                            </form>                            
                        <?php } else { ?>
                            <h1 class="page-header"> Voce não possue acesso a está pagina, consulte o administrador.</h1>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>
        <script>


           
            jQuery(document).ready(function () {
                // Remove seleção de ativo no menu.
                $('.nav li').removeClass('active');
                $('#side-menu li').removeClass('active');
                // Ativa botão no menu.
                $('#li-gerenciamento ul').addClass('collapse in');
                $('#li-cad-cursos a').addClass('active');
                $('#li-cursos').addClass('active');
                $('#li-cursos a').addClass('collapse in');


                $('#outrosA').on('click', function () {
                    if ($('#outrosA').is(':checked')) {
                        $('#espA').removeAttr('readonly');
                        $('#espA').attr('required');
                    } else {
                        $('#espA').attr('readonly', true);
                    }
                });

                $('#outrosF').on('click', function () {
                    if ($('#outrosF').is(':checked')) {
                        $('#espF').removeAttr('readonly');
                    } else {
                        $('#espF').attr('readonly', true);
                    }
                });

                $('#outrosP').on('click', function () {
                    if ($('#outrosP').is(':checked')) {
                        $('#espP').removeAttr('readonly');
                    } else {
                        $('#espP').attr('readonly', true);
                    }
                });
              
                           
            $("#cargaTeorica").on("keyup", function () {
                    var cargatotal = 100;
                    var cargaParcial = $(this).val();
                    var cargaPratica = cargatotal - cargaParcial;
                   $("#cargaPratica").val(cargaPratica);
            });
});
        </script>        
    </body>
</html>
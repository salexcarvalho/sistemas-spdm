<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - Gerenciamento de Cursos</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <?php if ($_SESSION['AcLiberaBtnAddCur'] == true) { ?>
                            <h1 class="page-header"> Cadastro de Curso</h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-file-pdf-o"></i> Complementos</li>
                                <li><a href="home"><i class="fa fa-list"></i> Coordenadores</a></li>
                                <li class="active"><i class="glyphicon glyphicon-file"></i> Seleção de Coordenadores</li>
                            </ol>
                            <!-- Inicio Informações Curso-->
                            <form class="form-horizontal" role="form" method="POST" action="">
                                <div class="form-group">    
                                    <div class="col-md-10">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <span>Selecionar Coordenador</span>
                                            </div>
                                            <div class="panel-body"> 
                                                <label for="" class="col-md-2 control-label">Tipo Curso</label>
                                                    <div class="col-md-5">
                                                       <select id="coordenadores" name="idCoordenador">
                                                           <?php 
                                                           $model_complemento = new Model_Grc_Complemento('default');
                                                                $complemento = $model_complemento->select_complemento_referencia($idCurso, 'coordenacao');
                                                           foreach($coordenadores as $value):
                                                                $complemento = $model_complemento->select_complemento_referencia($idCurso, 'coordenacao');
                                                           ?>
                                                            <option value="<?=$value['idCoordenacao']?>"><?=$value['nome']?></option>
                                                           <?php endforeach; ?> 
                                                        </select>
                                                    </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                               <div class="col-md-10">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <span>Coodenadores Selecionados:</span>                                                
                                            </div>
                                            <div class="panel-body">
                                                <div id="coordenadores" ondrop="drop(event)" ondragover="allowDrop(event)">
                                                    
                                                </div>
                                            </div>
                                        </div> 
                                    </div>
                                    
                                    </div>
                                <div class="form-group">
                                    <div class="col-md-7">
                                        <button type="submit" class="btn btn-primary btn-sm">Salvar</button>
                                        <a href="home?page=<?=$page?>" class="btn btn-danger">Voltar</a>
                                    </div>
                                </div>
                            </form>                            
                        <?php } else { ?>
                            <h1 class="page-header"> Voce não possue acesso a está pagina, consulte o administrador.</h1>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>
        <script>


           
            jQuery(document).ready(function () {
                // Remove seleção de ativo no menu.
                $('.nav li').removeClass('active');
                $('#side-menu li').removeClass('active');
                // Ativa botão no menu.
                $('#li-gerenciamento ul').addClass('collapse in');
                $('#li-cad-cursos a').addClass('active');
                $('#li-cursos').addClass('active');
                $('#li-cursos a').addClass('collapse in');


                $('#outrosA').on('click', function () {
                    if ($('#outrosA').is(':checked')) {
                        $('#espA').removeAttr('readonly');
                        $('#espA').attr('required');
                    } else {
                        $('#espA').attr('readonly', true);
                    }
                });

                $('#outrosF').on('click', function () {
                    if ($('#outrosF').is(':checked')) {
                        $('#espF').removeAttr('readonly');
                    } else {
                        $('#espF').attr('readonly', true);
                    }
                });

                $('#outrosP').on('click', function () {
                    if ($('#outrosP').is(':checked')) {
                        $('#espP').removeAttr('readonly');
                    } else {
                        $('#espP').attr('readonly', true);
                    }
                });
              
                           
            $("#cargaTeorica").on("keyup", function () {
                    var cargatotal = 100;
                    var cargaParcial = $(this).val();
                    var cargaPratica = cargatotal - cargaParcial;
                   $("#cargaPratica").val(cargaPratica);
            });
});
        </script>        
    </body>
</html>
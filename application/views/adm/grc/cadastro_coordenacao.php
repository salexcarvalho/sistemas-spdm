<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - Gerenciamento de Cursos</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">       
    </head>
    <body>
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <?php if ($_SESSION['AcLiberaBtnAddCoo'] == true) { ?>
                            <h1 class="page-header">Coordenação de Cursos</h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-file-pdf-o"></i> Coodenação</li>
                                <li class="active"><i class="fa fa-list"></i> Lista de Coordenadores</li>
                                <li class="active"><i class="glyphicon glyphicon-file"></i> Cadastro de Coordenadores</li>
                            </ol>
                            <form class="form-horizontal" role="form" method="POST" action="insert_coordenacao">
                                <div class="form-group">    
                                    <div class="col-lg-9">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <span>Dados do Coordenador</span>
                                            </div>
                                            <div class="panel-body">                                               
                                                <div class="form-group">
                                                    <label for="nome" class="col-md-2 control-label">Nome* </label>
                                                    <div class="col-md-7">
                                                        <input type="text" class="form-control" id="nome" name="nome" placeholder="Informe o nome do Coordenador" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="RG" class="col-md-2 control-label">RG* </label>
                                                    <div class="col-md-3">
                                                        <input type="text" class="form-control" id="RG" name="RG" placeholder="Informe o RG do Coordenador" required>
                                                    </div>

                                                    <label for="CPF" class="col-md-1 control-label">CPF* <i id="valida" class="glyphicon "></i></label>
                                                    <div class="col-md-3">
                                                        <input type="text" class="form-control" id="CPF" name="CPF" placeholder="Informe o CPF do Coordenador" data-mask="999.999.999-99" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="id_formacao" class="col-md-2 control-label">Titulação </label>
                                                    <div class="col-md-7">
                                                        <?php foreach ($formacoes as $formacao): ?>
                                                            <label class="checkbox-inline">                                                        
                                                                <input type="checkbox"  id="id_formacao" name="id_formacao[]" value="<?= $formacao['idFormacao']; ?>"><?= $formacao['nome']; ?>
                                                            </label>  
                                                        <?php endforeach; ?>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="endereco" class="col-md-2 control-label">Endereço* </label>
                                                    <div class="col-md-7">
                                                        <input type="text" class="form-control" id="endereco" name="endereco" placeholder="Informe o Endereço do Coordenador" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="telefone" class="col-md-2 control-label">Telefone* </label>
                                                    <div class="col-md-3">
                                                        <input type="text" class="form-control" id="telefone" name="telefone" placeholder="Informe o telefone do Coordenador" data-mask="(99)9999-9999" required>
                                                    </div>

                                                    <label for="celular" class="col-md-1 control-label">Celular* </label>
                                                    <div class="col-md-3">
                                                        <input type="text" class="form-control" id="celular" name="celular" placeholder="Informe o celular do Coordenador" data-mask="(99)9999-9999?9" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="email" class="col-md-2 control-label">Email* </label>
                                                    <div class="col-md-3">
                                                        <input type="text" class="form-control" id="email" name="email" placeholder="Informe o email do Coordenador" required>
                                                    </div>

                                                    <label for="lattes" class="col-md-1 control-label">Lattes </label>
                                                    <div class="col-md-3">
                                                        <input type="text" class="form-control" id="lattes" name="lattes" placeholder="Informe o lattes do Coordenador(somente os numeros)">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="descricao" class="col-md-2 control-label">Descrição </label>
                                                    <div class="col-md-7">
                                                        <textarea class="form-control" id="descricao" name="descricao" placeholder="Breve descrição da experiência acadêmica e profissional na área de atuação" required=""></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group"> 
                                                    <label for="status" class="col-lg-2 control-label">Status</label>
                                                    <div class="col-lg-10">    
                                                        <label class="radio-inline">
                                                            <input required type="radio" name="status" id="inativo" value="0">
                                                            Inativo
                                                        </label>
                                                        <label class="radio-inline">
                                                            <input required type="radio" name="status" id="inativo" value="1" checked="checked">
                                                            Ativo
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <span> <b>* Campos de preenchimento obrigatório</b></span>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-7">
                                                <button type="submit" class="btn btn-primary btn-sm">Cadastrar</button>
                                                <a href="home" class="btn btn-danger">Voltar</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        <?php } else { ?>
                            <h1 class="page-header"> Voce não possue acesso a está pagina, consulte o administrador.</h1>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>
        <script>
            function isCPF(val) {
                var value = val;
                var invalidos = [
                    '111.111.111-11',
                    '222.222.222-22',
                    '333.333.333-33',
                    '444.444.444-44',
                    '555.555.555-55',
                    '666.666.666-66',
                    '777.777.777-77',
                    '888.888.888-88',
                    '999.999.999-99',
                    '000.000.000-00'
                ];
                for (i = 0; i < invalidos.length; i++) {
                    if (invalidos[i] == value) {
                        return false;
                    }
                }

                value = value.replace("-", "");
                value = value.replace(/\./g, "");

                //validando primeiro digito
                var add = 0;
                for (i = 0; i < 9; i++) {
                    add += parseInt(value.charAt(i), 10) * (10 - i);
                }
                var rev = 11 - (add % 11);
                if (rev == 10 || rev == 11) {
                    rev = 0;
                }
                if (rev != parseInt(value.charAt(9), 10)) {
                    return false;
                }

                //validando segundo digito
                var add = 0;
                for (i = 0; i < 10; i++) {
                    add += parseInt(value.charAt(i), 10) * (11 - i);
                }
                var rev = 11 - (add % 11);
                if (rev == 10 || rev == 11) {
                    rev = 0;
                }
                if (rev != parseInt(value.charAt(10), 10)) {
                    return false;
                }
                return true;
            }           

jQuery(document).ready(function () {
                // Remove seleção de ativo no menu.
                $('.nav li').removeClass('active');
                $('#side-menu li').removeClass('active');
                // Ativa botão no menu.
                $('#li-gerenciamento ul').addClass('collapse in');
                $('#li-cad-formacao-curso a').addClass('active');
                $('#li-cursos').addClass('active');
                $('#li-cursos a').addClass('collapse in');

                $("#CPF").focusout(function () {
                    var cpf = $(this).val();
                    if(isCPF(cpf)){
                        $("#valida").removeClass('glyphicon-ban-circle');
                        $("#valida").addClass('glyphicon-ok');
                    }else{
                        $("#valida").removeClass('glyphicon-ok');
                        $("#valida").addClass('glyphicon-ban-circle');
                    };
                });
            });

        </script>        
    </body>
</html>
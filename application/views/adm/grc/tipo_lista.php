<?php
foreach ($data as $tipoCurso):
    ?>
    <tr>
        <td style="text-align: center;vertical-align: middle;"><?= $tipoCurso['idTipoCurso']; ?></td>
        <td style="vertical-align: middle;"><?= $tipoCurso['nome']; ?></td>       
        <td style="text-align: center;vertical-align: middle;">
       
            <button id="btnVisualizar" class="btn btn-default btn-xs" name="btnVisualizar" page="<?= $PaginaAtual; ?>" idTipoCurso="<?= $tipoCurso['idTipoCurso']; ?>" title="Visualizar Tipo de Curso" <?php if ($_SESSION['AcLiberaBtnVizTip'] == NULL) { ?>disabled<?php } ?>>
                <i class="glyphicon glyphicon-eye-open"></i>
            </button> 

            <button id="btnEditar" class="btn btn-default btn-xs" name="btnEditar" page="<?= $PaginaAtual; ?>" idTipoCurso="<?= $tipoCurso['idTipoCurso']; ?>" title="Editar Tipo de Curso" <?php if ($_SESSION['AcLiberaBtnEdiTip'] == NULL) { ?>disabled<?php } ?>>
                <i class="glyphicon glyphicon-edit"></i>
            </button>

            <button id="btnExcluir" class="btn btn-default btn-xs" name="btnExcluir" page="<?= $PaginaAtual; ?>" idTipoCurso="<?= $tipoCurso['idTipoCurso']; ?>" nome="<?= $tipoCurso['nome']; ?>" <?php if ($_SESSION['AcLiberaBtnExcTip'] == NULL) { ?>disabled<?php } ?> data-toggle="modal" data-target="#modal_excluir" data-placement="bottom" title="Excluir Tipo de Curso" data-delay="1">
                <i class="glyphicon glyphicon-trash"></i>
            </button>       
        </td>
        <td style="text-align: center;vertical-align: middle;">       
    <?php if ($tipoCurso['status'] == "0"): ?>
                <button id="btnAtivar_<?= $tipoCurso['idTipoCurso']; ?>" class="btn btn-danger btn-xs" idTipoCurso="<?= $tipoCurso['idTipoCurso']; ?>" name="btnAtivar" title="Ativar Tipo de Curso" <?php if ($_SESSION['AcLiberaBtnAtivTip'] == NULL) { ?>disabled<?php } ?>>
                    <i id="activ_<?= $tipoCurso['idTipoCurso']; ?>" class="glyphicon glyphicon-ban-circle"></i>
                </button>
    <?php else: ?>
                <button id="btnAtivar_<?= $tipoCurso['idTipoCurso']; ?>" class="btn btn-success btn-xs" idTipoCurso="<?= $tipoCurso['idTipoCurso']; ?>" name="btnAtivar" title="Destivar Tipo de Curso" <?php if ($_SESSION['AcLiberaBtnAtivTip'] == NULL) { ?>disabled<?php } ?>>
                    <i id="activ_<?= $tipoCurso['idTipoCurso']; ?>" class="glyphicon glyphicon-ok"></i>
                </button>
    <?php endif; ?>
        </td>
    </tr>
<?php endforeach; ?>
                                        

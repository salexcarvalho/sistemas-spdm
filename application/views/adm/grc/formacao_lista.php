<?php
foreach ($data as $formacao):    
    ?>
    <tr>
        <td style="text-align: center;vertical-align: middle;"><?= $formacao['idFormacao']; ?></td>
        <td style="text-align: center;vertical-align: middle;"><?= $formacao['nome']; ?></td>
        <td style="text-align: center;vertical-align: middle;"><?= ($formacao['tipo']==0)? "Alunado":"Titulação"; ?></td>
                <td style="text-align: center;vertical-align: middle;">
       
            <button id="btnVisualizar" class="btn btn-default btn-xs" name="btnVisualizar" page="<?= $PaginaAtual; ?>" idFormacao="<?= $formacao['idFormacao']; ?>" title="Visualizar Formação" <?php if ($_SESSION['AcLiberaBtnVizFor'] == NULL) { ?>disabled<?php } ?>>
                <i class="glyphicon glyphicon-eye-open"></i>
            </button> 

            <button id="btnEditar" class="btn btn-default btn-xs" name="btnEditar" page="<?= $PaginaAtual; ?>" idFormacao="<?= $formacao['idFormacao']; ?>" title="Editar Formação" <?php if ($_SESSION['AcLiberaBtnEdiFor'] == NULL) { ?>disabled<?php } ?>>
                <i class="glyphicon glyphicon-edit"></i>
            </button>

            <button id="btnExcluir" class="btn btn-default btn-xs" name="btnExcluir" page="<?= $PaginaAtual; ?>" idFormacao="<?= $formacao['idFormacao']; ?>" nome="<?= $formacao['nome'] ?>" <?php if ($_SESSION['AcLiberaBtnExcFor'] == NULL) { ?>disabled<?php } ?> data-toggle="modal" data-target="#modal_excluir" data-placement="bottom" title="Excluir Formação" data-delay="1">
                <i class="glyphicon glyphicon-trash"></i>
            </button>       
        </td>
        <td style="text-align: center;vertical-align: middle;">       
    <?php if ($formacao['status'] == "0"): ?>
                <button id="btnAtivar_<?= $formacao['idFormacao']; ?>" class="btn btn-danger btn-xs" idFormacao="<?= $formacao['idFormacao']; ?>" name="btnAtivar" title="Ativar Formação" <?php if ($_SESSION['AcLiberaBtnAtivFor'] == NULL) { ?>disabled<?php } ?>>
                    <i id="activ_<?= $formacao['idFormacao']; ?>" class="glyphicon glyphicon-ban-circle"></i>
                </button>
    <?php else: ?>
                <button id="btnAtivar_<?= $formacao['idFormacao']; ?>" class="btn btn-success btn-xs" idFormacao="<?= $formacao['idFormacao']; ?>" name="btnAtivar" title="Destivar Formação" <?php if ($_SESSION['AcLiberaBtnAtivFor'] == NULL) { ?>disabled<?php } ?>>
                    <i id="activ_<?= $formacao['idFormacao']; ?>" class="glyphicon glyphicon-ok"></i>
                </button>
    <?php endif; ?>
        </td>
    </tr>
<?php endforeach; ?>                                        

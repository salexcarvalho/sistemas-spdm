<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - Sistema de Controle e Geração de Protocolos</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <!-- Modal -->
        <div class="modal fade" id="modal_excluir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Confirmar Exclusão</h4>
                    </div>
                    <div class="modal-body" id="modal_delete_participante">Deseja excluir este participante?</div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        <button type="button" class="btn btn-primary btn-sm" id="modal_btn-excluir">Excluir</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal -->

        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            
            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Lista de Protocolos</h1>
                        <ol class="breadcrumb">
                            <li><i class="fa fa-lock"></i>Sistema de Gestão de Protocolos</li>
                            <li class="active"><i class="fa fa-list"></i> Listagem</li>
                        </ol>

                        <?php if (isset($warning)) : ?>
                            <div id="msg" class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong><?= $warning; ?></strong>
                            </div>
                        <?php endif ?>                          

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <span>Relatos</span>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">

                                    <table class="table table-striped table-bordered table-hover" id="relatos">
                                        <thead>
                                            <tr>
                                                <th width="6%" style="text-align: center">Protocolo</th>
                                                <th>Tipo de Documento</th>
                                                <th width="13%">Data de Entrada</th>
                                                <th width="18%">Departamento</th>
                                                <th width="8%" style="text-align: center">Data de Saída</th>
                                                <th width="14%" style="text-align: center">Retirado por</th>
                                                <th width="8%" style="text-align: center">Ações</th>
                                                <th width="11%" style="text-align: center">Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            foreach ($dados as $protocolo) {
                                                ?>
                                                <tr>
                                                    <td align="center"><?= $protocolo['protocolo']; ?></td>
                                                    <td><?= $protocolo['tipo']; ?></td>
                                                    <td><?= strftime('%d/%m/%Y', strtotime($protocolo['dataEntrada'])); ?></td>
                                                    <td><?= $protocolo['departamento_id']; ?></td>
                                                    <td align="center"><?= strftime('%d/%m/%Y', strtotime($protocolo['dataSaida'])); ?></td>
                                                    <td align="center"><?= $protocolo['retirado']; ?></td>
                                                    <td align="center">
                                                        <form style="display: inline-block;" role="form" method="POST" action="visualizar_relato">
                                                            <input hidden id="page" name="page" value="<?= $PaginaAtual; ?>">
                                                            <input type="hidden" id="idRelato_<?= $protocolo['idRelato']; ?>" name="idRelato" value="<?= $protocolo['idRelato']; ?>">
                                                            <button  type="submit" id="btnVisualizar" class="btn btn-default btn-xs" name="btnVisualizar" idRelato="<?= $protocolo['idRelato']; ?>" title="Visualizar Relato" <?php if ($_SESSION['AcLiberaBtnRespRel'] == NULL) { ?>disabled<?php } ?>>
                                                                <?php if ($protocolo['Atendimento'] == "1" or $protocolo['Atendimento'] == "2") { ?>
                                                                <i class="glyphicon glyphicon-eye-open"></i>
                                                                <?php } else { ?>
                                                                <i class="glyphicon glyphicon-eye-close"></i>
                                                                <?php } ?>
                                                            </button>
                                                        </form>
                                                        <form style="display: inline-block;" role="form" method="POST" action="abrir_pdf" target="_blank">
                                                            <input hidden id="page" name="page" value="<?= $PaginaAtual; ?>">
                                                            <input type="hidden" id="idRelato_<?= $protocolo['idRelato']; ?>" name="idRelato" value="<?= $protocolo['idRelato']; ?>">
                                                            <button  type="submit" id="btnEditar" class="btn btn-default btn-xs" name="btnEditar" idRelato="<?= $protocolo['idRelato']; ?>" title="Ver Relato em PDF" <?php if ($_SESSION['AcLiberaBtnRespRel'] == NULL) { ?>disabled<?php } ?>>
                                                                <i class="fa fa-file-pdf-o"></i>
                                                            </button>
                                                        </form>
                                                    </td>
                                                    <td align="center">
                                                  <?php
                                                            if ($protocolo['status'] == "0") {
                                                                ?>
                                                                <button type="submit" id="btnAtivar_<?= $protocolo['idProtocolo']; ?>" class="btn btn-danger btn-xs" idProtocolo="<?= $protocolo['idProtocolo']; ?>" name="btnAtivar" title="Ativar Protocolo" <?php if($_SESSION['AcLiberaBtnAtivProt'] == NULL) { ?>disabled<?php } ?>>
                                                                    <i id="activ_<?= $protocolo['idSalas']; ?>" class="glyphicon glyphicon-ban-circle"></i>
                                                                </button>
                                                                <?php } else { ?>
                                                                <button type="submit" id="btnAtivar_<?= $protocolo['idProtocolo']; ?>" class="btn btn-success btn-xs" idProtocolo="<?= $protocolo['idProtocolo']; ?>" name="btnAtivar" title="Destivar Protocolo" <?php if($_SESSION['AcLiberaBtnAtivProt'] == NULL) { ?>disabled<?php } ?>>
                                                                    <i id="activ_<?= $protocolo['idProtocolo']; ?>" class="glyphicon glyphicon-ok"></i>
                                                                </button>
                                                                <?php
                                                            }
                                                            ?>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>    
                                    </table>
                                    <?php include(kohana::find_file('views/paginator', 'paginacao', 'php')) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>

        <script>
            jQuery(document).ready(function () {
                // Remove seleção de ativo no menu.
                $('.nav li').removeClass('active');
                $('#side-menu li').removeClass('active');
                // Ativa botão no menu.
                $('#li-gestaoP ul').addClass('collapse in');
                $('#li-cad-protocolo a').addClass('active');
                $('#li-cad-protocolo').addClass('active');
                $('#li-cad-protocolo a').addClass('collapse in');
            });
        </script>
        
        <script>
            jQuery(document).ready(function()
            {
                $('#area').dataTable(
                        {
                            "aoColumnDefs": [{"bSortable": false, "aTargets": [0, 1]}],
                            "paging":   false,
                            "info":     false,
                            "aaSorting": [[0, 'desc']],
                            "bFilter": false,
                            "oLanguage":
                                    {
                                        "sSearch": " Buscar: "
                                    }
                        });
                var idSalas;
                var nome;
                $('button[name=btnExcluir]').on("click", function()
                {
                    idSalas = $(this).attr('idSalas');
                    nome = $(this).attr('nome');
                    $('#modal_delete_evento').html("Deseja excluir a Sala <b>\"" + nome + "\"</b> ?");
                });

                $('#modal_excluir').on('hidden.bs.modal', function()
                {
                    window.location.replace("home");
                });

                $('#modal_btn-excluir').on("click", function()
                {
                    var btn = $(this);
                    btn.text('loading');

                    btn.removeClass('btn-primary');
                    btn.removeClass('btn-danger');
                    btn.removeClass('btn-success');
                    btn.addClass('btn-primary');

                    $('#modal_btn-excluir').prop('disabled', true);

                    $.ajax("delete_sala",
                            {
                                type: "POST",
                                data:
                                        {
                                            idSalas: idSalas
                                        }
                            })
                            .done(function(data)
                            {
                                $('#modal_delete_salas').html('Sala excluída com sucesso');

                                btn.removeClass('btn-primary');
                                btn.addClass('btn-success');
                            })
                            .fail(function()
                            {
                                $('#modal_delete_sala').html('Erro: ' + data);

                                btn.removeClass('btn-primary');
                                btn.addClass('btn-danger');
                            })
                            .always(function()
                            {
                                btn.text('Excluído');
                            });
                });
                
                $("body").on("click", "button[name=btnAtivar]", function() {
                       var idSalas = $(this).attr('idSalas');
                        //alert(txtUsuario);
                            $.get('update_ativar_sala?idSalas=' + idSalas, function(data) {
                                    
                                if (data === "1") {
                                    $('#btnAtivar_' + idSalas).addClass('btn btn-success btn-xs');
                                    $('#btnAtivar_' + idSalas).removeClass('btn-danger');
                                    $('#activ_' + idSalas).removeClass('glyphicon-ban-circle');
                                    $('#activ_' + idSalas).addClass('glyphicon-ok');
                                } else {
                                    $('#btnAtivar_' + idSalas).addClass('btn btn-danger btn-xs');
                                    $('#btnAtivar_' + idSalas).removeClass('btn-success');
                                    $('#activ_' + idSalas).addClass('glyphicon-ok');
                                    $('#activ_' + idSalas).addClass('glyphicon-ban-circle');
                                }
                            });
                    });

                $('#btn_nova_area').on("click", function()
                {
                    window.location.href = "cadastro_salas";
                });
                /*
                 $("body").on("click", "button[name=btnEditar]", function()
                 {
                 idEvento = $(this).attr('idEvento');
                 window.location.href = "cadastro_evento?editar=1&idEvento=" + idEvento;
                 });*/

                $("body").on("click", "button[name=btnVisualizar]", function()
                {
                    idSalas = $(this).attr('idSalas');
                    window.location.href = "visualizar_salas?idSalas=" + idSalas;
                });

                $('.dataTables_filter label').addClass('pull-right');
            });
        </script>
    </body>
</html>
<!DOCTYPE html>
<html lang='pt-br'>
    <head>
        <title><?= $_SESSION['company_name'] ?> - Reserva de Salas</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <meta charset='utf-8'>
        <meta name='viewport' content='width=device-width, initial-scale=1.0'>
        <script type="text/javascript" src="<?= URL::base(); ?>theme/backend/js/reservas.js"></script> 
        <style>
            hr {
                margin-top: 0px;
            }
            @media print {
                body, html, #wrapper {
                    width: 100%;
                }
                div {
                    overflow: visible !important;      
                }

            }
        </style>

    </head>

    <body>
        <input type="hidden" id="idUsuario" value="<?php echo $_SESSION['idUsuario']; ?>" />
        <input type="hidden" id="Perfil" value="<?php echo $_SESSION['Perfil']; ?>" />
        <input type="hidden" id="Permissao" value="<?=(isset($_SESSION['AcLiberaAgeRes'])==false)? 'false':$_SESSION['AcLiberaAgeRes'];?>" />
        
        <div id='wrapper'>
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->

            <div id='page-wrapper'>
                <div class="row">
                    <div class="col-lg-12">
                        <?php if ($_SESSION['AcLiberaBtnAtivRes'] == true) { ?>
                            <h1 class="page-header">Reserva de Salas</h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-file-pdf-o"></i> Calendário de Reservas </li>                                                  
                            </ol>
                            
                            <?php if (isset($warning)) : ?>
                            <div id="msg" class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong><?= $warning; ?></strong>
                            </div>
                            <?php endif ?> 
                            
                            <div class="row">
                                <div class="form-group col-md-1">
                                    <label for='eventByResource' class="pull-right" style="margin-top:5px;">Filtro de Salas:</label>
                                </div>
                                <div class="form-group col-md-5">
                                    <select id="eventByResource" class="form-control">
                                        <option style="color:#fff;background-color:#000;">Todas</option>
                                        <?php foreach ($salas as $sala): ?>                                            
                                            <option style="color:#fff;background-color:#<?= $sala['rgb']; ?>;" value="<?= $sala['idSalas']; ?>"><?= $sala['Alias'] ?> - <?= $sala['nome'] ?></option>  
                                        <?php endforeach; ?>
                                    </select>                                         
                                </div>
                                <div class="form-group col-md-4"></div>
                                <div class="form-group col-md-1">
                                    <button id="btn_novo_reserva" class="pull-right btn btn-primary btn-md" name="btn_novo_reserva" style='<?= ($_SESSION['AcLiberaAgeRes'] == false) ? 'display:none;' : 'display:block;'; ?>'><i class="glyphicon glyphicon-plus-sign"> </i>  Nova Reserva</button>
                                </div>
                                <div class="form-group col-md-1">
                                    <button id="btn_imprimir" class="pull-right btn btn-primary btn-md" name="btn_imprimir" data-toggle="modal" data-target="#modal_imprimir" data-placement="bottom" title="Relatório de Reservas" data-delay="1"><i class="glyphicon glyphicon-save"> </i>  Reservas PDF</button>
                                </div>
                            </div>  
                            <hr>
                            <div id='calendar'></div>                               

                        <?php } else { ?>
                            <h1 class="page-header"> Voce não possue acesso a está pagina, consulte o administrador.</h1>
                        <?php } ?>

                    </div>
                </div> 
            </div>
        </div>
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>
        <script>
            jQuery(document).ready(function () {
                // Remove seleção de ativo no menu.
                $('.nav li').removeClass('active');
                $('#side-menu li').removeClass('active');
                // Ativa botão no menu.
                $('#li-salas ul').addClass('collapse in');
                $('#li-cad-reserva a').addClass('active');
                $('#li-salas').addClass('active');
                $('#li-salas a').addClass('collapse in');
            });
            
        </script>
    </body>
</html>
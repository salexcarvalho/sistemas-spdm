<?php Session::instance(); ?>
<?php Funcoes::carrega(); //echo "<pre>"; var_dump($_SESSION['idUsuario']); echo "</pre>";       ?>

<!-- Bootstrap Core CSS -->
<link href="<?= URL::base(); ?>theme/backend/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- MetisMenu CSS -->
<link href="<?= URL::base(); ?>theme/backend/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

<!-- SbAdmin CSS -->
<link href="<?= URL::base(); ?>theme/backend/dist/css/sb-admin-2.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="<?= URL::base(); ?>theme/backend/css/custom.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="<?= URL::base(); ?>theme/backend/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

<!-- jQuery Version 3.2.1 -->
<script src="<?= URL::base(); ?>theme/backend/js/jquery-3.2.1.js"></script>
<script src="<?= URL::base(); ?>theme/backend/js/jquery-migrate-1.4.1.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?= URL::base(); ?>theme/backend/vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="<?= URL::base(); ?>theme/backend/vendor/metisMenu/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="<?= URL::base(); ?>theme/backend/dist/js/sb-admin-2.js"></script>

<!-- datapicker plugin -->
<script src='<?= URL::base(); ?>theme/backend/vendor/fullcalendar/lib/moment.min.js'></script>       
<script src='<?= URL::base(); ?>theme/backend/vendor/fullcalendar/locale/pt-br.js'></script>

<script src='<?= URL::base(); ?>theme/backend/vendor/fullcalendar/lib/jquery-ui.min.js'></script>
<link href='<?= URL::base(); ?>theme/backend/vendor/datetimepicker/css/bootstrap-datetimepicker.min.css' rel='stylesheet' />
<script type="text/javascript" src="<?= URL::base(); ?>theme/backend/vendor/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>

<style>
    hr {
        margin-top: 0px;
    }
</style>


<div id="testedados" class="container-fluid bd-example-row"> 

    <div class="form-group col-md-12">
        <div class="panel panel-default"> 
            <div class="panel-heading">
                <span>Evento:</span>
            </div>    
            <div class="panel-body">
                <div class="col-md-12">
                    <label>Local:</label>                   
                    <label style="font-weight:1;"><?= $dados['local'] ?></label>                                
                </div>                


                <div class="col-md-12">
                    <label>Sala:</label>            
                    <label style="font-weight:1;"><?= $dados['sala'] ?></label>                                  
                </div>
                <div class="col-md-12">
                    <label>Nome do Evento:</label>            
                    <label style="font-weight:1;"><?= $dados['titulo'] ?></label>                                  
                </div>     

            </div>
        </div>

        <div class="panel panel-default"> 
            <div class="panel-heading">
                <span> Responsável pelo Evento</span>
            </div>    
            <div class="panel-body">

                <div class="col-md-12">
                    <label>Nome:</label>
                    <label style="font-weight:1;"><?= $dados['resp_nome'] ?></label>                                  
                </div>   
                <div class="col-md-12">
                    <label>Email:</label>
                    <label style="font-weight:1;"><?= $dados['resp_email'] ?></label>                                  
                </div> 

                <div class="col-md-12">
                    <label>Telefone:</label>
                    <label style="font-weight:1;"><?= $dados['resp_telefone'] ?></label>                                  
                </div> 
                <div class="col-md-12">
                    <label>Celular:</label>                    
                    <label style="font-weight:1;"><?= $dados['resp_celular'] ?></label>                                  
                </div> 

            </div></div>
        <div class="panel panel-default"> 
            <div class="panel-heading">
                <span> Responsável pelo Agendamento</span>
            </div>    
            <div class="panel-body">
                <div class="col-md-12">
                    <label>Nome:</label>                   
                    <label style="font-weight:1;"><?= $dados['resp_nome_ag'] ?></label>                                  
                </div>   
                <div class="col-md-12">
                    <label>Email:</label>
                    <label style="font-weight:1;"><?= $dados['resp_email_ag'] ?></label>                                  
                </div> 

                <div class="col-md-12">
                    <label>Telefone:</label>
                    <label style="font-weight:1;"><?= $dados['resp_telefone_ag'] ?></label>                                  
                </div> 
                <div class="col-md-12">
                    <label>Celular:</label>                    
                    <label style="font-weight:1;"><?= $dados['resp_celular_ag'] ?></label>                                  
                </div> 

            </div></div> 
        <div class="panel panel-default"> 
            <div class="panel-heading">
                <span> Data(s) do Evento</span> 
            </div>    
            <div class="panel-body" id="DatasAdicionais">
                <div class="col-md-6">
                    <label>Inicio:</label>                    
                    <label style="font-weight:1;"><?= date('d/m/Y H:i', strtotime($dados['start_data'])) ?></label>                                  
                </div> 
                <div class="col-md-6">
                    <label>Término:</label>
                    <label style="font-weight:1;"><?= date('d/m/Y H:i', strtotime($dados['end_data'])) ?></label>                                  
                </div>
            </div>     
        </div> 

    </div>
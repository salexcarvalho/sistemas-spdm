<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - RESERVA DE SALAS\ANFITEATRO</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Reserva de Salas</h1>
                        <ol class="breadcrumb">
                            <li><i class="fa fa-file-pdf-o"></i> Reserva de Salas</li>
                            <li class="active"><i class="fa fa-list"></i> Lista de Salas</li>
                        </ol> 
                        <?php if (isset($warning)) : ?>
                            <div id="msg" class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong><?= $warning; ?></strong>
                            </div>
                        <?php endif ?> 
                        <div class="row">
                            <?php if ($_SESSION['AcLiberaBtnAddSalas'] == true) { ?>
                                <div class="col-lg-12">
                                    <button type="submit" id="btn_nova_sala" class="pull-right btn btn-primary btn-md" name="btn_nova_sala" <?= ($area > 0) ? "" : "disabled='disabled'"; ?>><i class="glyphicon glyphicon-plus-sign"></i> Nova Sala</button>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="area">
                                <thead>
                                    <tr>
                                        <th width="5%" style="text-align: center">#</th>
                                        <th>Sala</th>
                                        <th width="18%">Localização</th>
                                        <th width="8%" style="text-align: center">Capacidade</th>
                                        <th width="18%">Extras</th>                                              
                                        <th width="9%" style="text-align: center">Ações</th>
                                        <th width="5%" style="text-align: center">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($data as $sala): ?>
                                        <tr>
                                            <td style="text-align: center;vertical-align: middle;"><?= $sala['idSalas']; ?></td>
                                            <td style="vertical-align: middle;"><?= $sala['nome']; ?></td>
                                            <td style="vertical-align: middle;"><?= $sala['NomeArea']; ?></td>
                                            <td style="text-align: center;vertical-align: middle;"><?= $sala['capacidade'] ?></td> 
                                            <td><?= ($sala['extras'] != '') ? str_replace(',', '<br />', $sala['extras']) : ""; ?></td>
                                            <td style="text-align: center;vertical-align: middle;">
                                                    <button type="submit" id="btnVisualizar" class="btn btn-default btn-xs" page="<?= $PaginaAtual; ?>" idSalas="<?= $sala['idSalas']; ?>" name="btnVisualizar" title="Visualizar Sala" <?php if ($_SESSION['AcLiberaBtnVizSala'] == NULL) { ?>disabled<?php } ?>>
                                                        <i class="glyphicon glyphicon-file"></i>
                                                    </button>
                                                    <button  type="submit" id="btnEditar" class="btn btn-default btn-xs" name="btnEditar" page="<?= $PaginaAtual; ?>" idSalas="<?= $sala['idSalas']; ?>" title="Editar Sala" <?php if ($_SESSION['AcLiberaBtnEdiSala'] == NULL) { ?>disabled<?php } ?>>
                                                        <i class="glyphicon glyphicon-edit"></i>
                                                    </button>
                                                <input type="hidden" id="idSalas_<?= $sala['idSalas']; ?>" name="idSalas" value="<?= $sala['idSalas']; ?>">
                                                <button  type="button" id="btnExcluir" class="btn btn-default btn-xs" name="btnExcluir" idSalas="<?= $sala['idSalas']; ?>" nome="<?= $sala['nome']; ?>" <?php if ($_SESSION['AcLiberaBtnExcSala'] == NULL) { ?>disabled<?php } ?> data-toggle="modal" data-target="#modal_excluir" data-placement="bottom" title="Excluir Sala" data-delay="1">
                                                    <i class="glyphicon glyphicon-trash"></i>
                                                </button>
                                            </td>
                                            <td style="text-align: center;vertical-align: middle;">

                                                <input type="hidden" name="idSalas" value="<?= $sala['idSalas']; ?>">
                                                <?php
                                                if ($sala['status'] == "0") {
                                                    ?>
                                                    <button type="submit" id="btnAtivar_<?= $sala['idSalas']; ?>" class="btn btn-danger btn-xs" idSalas="<?= $sala['idSalas']; ?>" name="btnAtivar" title="Ativar Sala" <?php if ($_SESSION['AcLiberaBtnAtivArea'] == NULL) { ?>disabled<?php } ?>>
                                                        <i id="activ_<?= $sala['idSalas']; ?>" class="glyphicon glyphicon-ban-circle"></i>
                                                    </button>
                                                <?php } else { ?>
                                                    <button type="submit" id="btnAtivar_<?= $sala['idSalas']; ?>" class="btn btn-success btn-xs" idSalas="<?= $sala['idSalas']; ?>" name="btnAtivar" title="Destivar Sala" <?php if ($_SESSION['AcLiberaBtnAtivArea'] == NULL) { ?>disabled<?php } ?>>
                                                        <i id="activ_<?= $sala['idSalas']; ?>" class="glyphicon glyphicon-ok"></i>
                                                    </button>
                                                    <?php
                                                }
                                                ?>

                                            </td>
                                        </tr>
                                        <?php
                                    endforeach;
                                    ?>
                                </tbody>
                            </table>
                            <?php include(kohana::find_file('views/paginator', 'paginacao', 'php')) ?> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- Modal -->
        <div class="modal fade" id="modal_excluir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Excluir Sala</h4>
                    </div>
                    <div class="modal-body" id="modal_delete_evento">Deseja excluir esta area ?</div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        <button type="button" class="btn btn-primary" id="modal_btn-excluir">Excluir</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal -->
        
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>

        <script>
            jQuery(document).ready(function () {
                // Remove seleção de ativo no menu.
                $('.nav li').removeClass('active');
                $('#side-menu li').removeClass('active');
                // Ativa botão no menu.
                $('#li-salas ul').addClass('collapse in');
                $('#li-cad-salas a').addClass('active');
                $('#li-salas').addClass('active');
                $('#li-salas a').addClass('collapse in');
                
                // Datatables
                $('#area').dataTable(
                        {
                            "aoColumnDefs": [{"bSortable": false, "aTargets": [1,2,3,4,5]}],
                            "paging": false,
                            "info": false,
                            "aaSorting": [[0, 'desc']],
                            "bFilter": false,
                            "oLanguage":
                                    {
                                        "sSearch": " Buscar: "
                                    }
                        });
                        
                // abre o modal para cadastro
                $('button[name=btn_nova_sala]').click(function () {
                    var idSalas = '';
                    var options = {
                        url: "cadastro_salas?idSalas=" + idSalas,
                        title: 'Cadastrar Sala',
                        size: eModal.size.md,
                        buttons: [
                            {text: 'GRAVAR', style: 'primary', close: false, type: 'submit', form: 'novasala'},
                            {text: 'FECHAR', style: 'danger', close: true}
                        ]
                    };
                    eModal.ajax(options);
                });

                //abre o modal de edição
                $('button[name=btnEditar]').click(function () {
                    var idSalas = $(this).attr('idSalas'); 
                    var cd = '1'; 
                    var page = $(this).attr('page');
                    var options = {
                        url: "visualizar_sala?idSalas=" + idSalas + "&page=" + page + "&cd=" + cd,
                        title: 'Editar Sala',
                        size: eModal.size.md,
                        buttons: [
                            {text: 'SALVAR', style: 'primary', close: false, type: 'submit', form: 'novasala'},
                            {text: 'FECHAR', style: 'danger', close: true}
                        ]
                    };
                    eModal.ajax(options);
                });
                
                //abre o modal de visualização
                $('button[name=btnVisualizar]').click(function () {
                    var idSalas = $(this).attr('idSalas');
                    var page = $(this).attr('page');
                    var options = {
                        url: "visualizar_sala?idSalas=" + idSalas + "&page=" + page,
                        title: 'Visualizar Sala',
                        size: eModal.size.md,
                        buttons: [
                            {text: 'FECHAR', style: 'danger', close: true}
                        ]
                    };
                    eModal.ajax(options);
                });
        
                // Opção usada na exclusão 
                var idSalas;
                var nome;
                $('button[name=btnExcluir]').on("click", function ()
                {
                    idSalas = $(this).attr('idSalas');
                    nome = $(this).attr('nome');
                    $('#modal_delete_evento').html("Deseja excluir a Sala <b>\"" + nome + "\"</b> ?");
                });

                $('#modal_excluir').on('hidden.bs.modal', function ()
                {
                    window.location.replace("home");
                });

                $('#modal_btn-excluir').on("click", function ()
                {
                    var btn = $(this);
                    btn.text('loading');

                    btn.removeClass('btn-primary');
                    btn.removeClass('btn-danger');
                    btn.removeClass('btn-success');
                    btn.addClass('btn-primary');

                    $('#modal_btn-excluir').prop('disabled', true);

                    $.ajax("delete_sala",
                            {
                                type: "POST",
                                data:
                                        {
                                            idSalas: idSalas
                                        }
                            })
                            .done(function (data)
                            {
                                $('#modal_delete_salas').html('Sala excluída com sucesso');

                                btn.removeClass('btn-primary');
                                btn.addClass('btn-success');
                            })
                            .fail(function ()
                            {
                                $('#modal_delete_sala').html('Erro: ' + data);

                                btn.removeClass('btn-primary');
                                btn.addClass('btn-danger');
                            })
                            .always(function ()
                            {
                                btn.text('Excluído');
                            });
                });
                
                // Opção para ativar e desativar
                $("body").on("click", "button[name=btnAtivar]", function () {
                    var idSalas = $(this).attr('idSalas');
                    //alert(txtUsuario);
                    $.get('update_ativar_sala?idSalas=' + idSalas, function (data) {

                        if (data === "1") {
                            $('#btnAtivar_' + idSalas).addClass('btn btn-success btn-xs');
                            $('#btnAtivar_' + idSalas).removeClass('btn-danger');
                            $('#activ_' + idSalas).removeClass('glyphicon-ban-circle');
                            $('#activ_' + idSalas).addClass('glyphicon-ok');
                        } else {
                            $('#btnAtivar_' + idSalas).addClass('btn btn-danger btn-xs');
                            $('#btnAtivar_' + idSalas).removeClass('btn-success');
                            $('#activ_' + idSalas).addClass('glyphicon-ok');
                            $('#activ_' + idSalas).addClass('glyphicon-ban-circle');
                        }
                    });
                });

                $('.dataTables_filter label').addClass('pull-right');
            });
        </script>
    </body>
</html>
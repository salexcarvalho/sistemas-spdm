<!DOCTYPE html>
<html lang='pt-br'>
    <head>
        <title><?= $_SESSION['company_name'] ?> - CERTIFICADOS</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>        
    </head>
    <body>     
        <div id='wrapper'>
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <div id='page-wrapper'>
                <div class="row">
                    <?php if ($_SESSION['AcLiberaBtnAtivRes'] == true) { ?>
                        <div class="col-md-12">
                            <h1 class="page-header">Reserva de Salas</h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-file-pdf-o"></i> Relatório de reservas</li>                                                  
                            </ol>
                        </div>
                        <div class="col-md-6">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Uso das Salas
                                </div>
                                <!-- /.panel-heading -->                        
                                <div class="panel-body">
                                    <div id='salas'></div> 
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Reservas por Mês
                                </div>
                                <!-- /.panel-heading -->                        
                                <div class="panel-body">
                                    <div id='mes'></div> 
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Ocupação das Areas
                                </div>
                                <!-- /.panel-heading -->                        
                                <div class="panel-body">
                                    <div id='areas'></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Tabelas
                                </div>
                                <!-- /.panel-heading -->                        
                                <div class="panel-body">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Areas Disponíveis</th><th>Salas</th><th>Equipamentos</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $model_relatorio = new Model_Rsa_Relatorio('default');
                                            $areas = $model_relatorio->select_areas();
                                            foreach ($areas as $area):
                                                ?>
                                                <tr>
                                                    <td style="vertical-align: middle;"><?= $area['nome'] ?></td>
                                                    <td  style="vertical-align: middle;">
                                                        <?php
                                                        $sala = $model_relatorio->select_salas_area($area['idArea']);
                                                        for ($i = 0; $i < count($sala); $i++):
                                                            if ($i > 0) {
                                                                echo "<hr>";
                                                            }
                                                            echo $sala[$i]['nome'];
                                                            if ($sala[$i]['excluido'] > 0) {
                                                                echo "<font color='red'>&nbsp;(removido)</font>";
                                                            }
                                                        endfor;
                                                        ?>
                                                    </td>
                                                    <td  style="vertical-align: middle;">
                                                        <?php
                                                        $sala = $model_relatorio->select_salas_area($area['idArea']);
                                                        for ($i = 0; $i < count($sala); $i++):
                                                            if ($i > 0) {
                                                                echo "<hr>";
                                                            }
                                                            if ($sala[$i]['extras'] != ""):
                                                                echo $sala[$i]['extras'];
                                                            else:
                                                                echo "Equipamento não listado";
                                                            endif;
                                                        endfor;
                                                        ?>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>                                       
                                    </table>
                                </div>
                            </div>
                        </div>
                    <?php } else { ?>
                        <h1 class="page-header"> Voce não possue acesso a está pagina, consulte o administrador.</h1>
                    <?php } ?>
                </div> 
            </div>
        </div>
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>
        <script>
            jQuery(document).ready(function () {
                // Remove seleção de ativo no menu.
                $('.nav li').removeClass('active');
                $('#side-menu li').removeClass('active');
                // Ativa botão no menu.
                $('#li-salas ul').addClass('collapse in');
                $('#li-cad-relatorio a').addClass('active');
                $('#li-salas').addClass('active');
                $('#li-salas a').addClass('collapse in');
            });
        </script>

        <script>
            $.getJSON('getReport?tipo=salas', function (data) {
                Morris.Donut({
                    element: 'salas',
                    data: data
                });
            });
            $.getJSON('getReport?tipo=areas', function (data) {
                Morris.Donut({
                    element: 'areas',
                    data: data
                });
            });
            $.getJSON('getReport?tipo=mes', function (data) {
                Morris.Donut({
                    element: 'mes',
                    data: data
                });
            });

            /*   var bar = new Morris.Bar({
             element: 'EventoSalaChart',
             xkey: 'titulo',
             ykeys: 'qtde',
             });
             
             
             $.ajax({
             type: "POST",
             url:  "getReport",
             data: {tipo:'bar'},
             success: function(resp)                    
             {                   
             if(resp == "error")
             {
             alert('error');
             }
             else
             {
             bar.setData(resp);
             
             }
             hide_loading();
             }
             });
             
             var donut = new Morris.Donut({               
             element: 'AreasOcupadasChart',
             xkey: 'year',
             ykeys: ['value'],               
             labels: ['Value']
             }); 
             $.ajax({
             type: "POST",
             url:  "getReport",
             data: {tipo:'donut'},
             success: function(resp)                    
             {   
             if(resp == "error")
             {
             alert('error');
             }
             else
             {
             donut.setData(resp);
             
             }
             hide_loading();
             }
             });*/
        </script>

    </body>
</html>
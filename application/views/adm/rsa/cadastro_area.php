<?php if ($_SESSION['AcLiberaBtnAddAss'] == true) { ?>
    <form id="novaarea" name="novaarea" class="form-horizontal" role="form" method="POST" action="insert_area" enctype="multipart/form-data">
        <div class="form-group">
            <div class="col-lg-12">
                <div class="form-group">
                    <label for="nome" class="col-lg-3 control-label">Local:</label>
                    <div class="col-lg-9">
                        <input type="text" min="0" class="form-control" id="nome" name="nome" placeholder="Nome do Local onde se encontram as salas" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="endereco" class="col-lg-3 control-label">Endereço:</label>
                    <div class="col-lg-9">
                        <input type="text" min="0" class="form-control" id="endereco" name="endereco" placeholder="Informe o endereço completo" required>
                    </div>
                </div>                                        
                <div class="col-lg-offset-4 col-lg-8">
                    <div class="form-group"> 
                        <label class="radio-inline">
                            <input required type="radio" name="status" id="ativo" value="1">
                            Ativo
                        </label>
                        <label class="radio-inline">
                            <input required type="radio" name="status" id="inativo" value="0">
                            Inativo
                        </label>
                    </div>
                </div>
            </div>
        </div>        
    </form>
<?php } else { ?>
    <h1 class="page-header"> Voce não possue acesso a está pagina, consulte o administrador.</h1>
<?php } ?>
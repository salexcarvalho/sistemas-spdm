<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - RESERVA DE SALAS\ANFITEATRO</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
    </head>
    <body>

        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Reserva de Salas</h1>
                        <ol class="breadcrumb">
                            <li><i class="fa fa-file-pdf-o"></i> Agendamento de Salas</li>
                            <li class="active"><i class="fa fa-list"></i> Lista de Solicitações</li>
                        </ol> 
                        <?php if (isset($warning)) : ?>
                            <div id="msg" class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong><?= $warning; ?></strong>
                            </div>
                        <?php endif ?> 
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="area">
                                <thead>
                                    <tr>                                       
                                        <th>Evento</th>
                                        <th>Responsável do Evento</th>
                                        <th>Agendado por</th>                                       
                                        <th>Data Solicitada</th>
                                        <th>Sala</th>                                                
                                        <th width="12%" style="text-align: center">Ações</th>
                                        <th width="7%" style="text-align: center">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $model_salas = new Model_Rsa_Salas('default');
                                    foreach ($agendamentos as $agendamento) {
                                        $salas = $model_salas->select_salas_id($agendamento['sala_id']);
                                        ?>
                                        <tr>                                            
                                            <td><?= $agendamento['titulo']; ?></td>
                                            <td><?= $agendamento['resp_nome'] ?></td>
                                            <td><?= $agendamento['resp_telefone'] ?></td>
                                            <td>Início: <?= date("d/m/Y H:i", strtotime($agendamento['start_data'])) ?> - Término: <?= date("d/m/Y H:i", strtotime($agendamento['end_data'])) ?></td>
                                            <td><?= $salas[0]['nome'] ?></td>

                                            <td align="center">                                                       
                                                <button id="btnAceitar" class="btn btn-default btn-xs" page="<?= $agendamento['idAgendamento']; ?>" idAgendamento="<?= $agendamento['idAgendamento']; ?>" title="Aceitar Agendamento" <?php if ($agendamento['status'] >= 1) {
                                        echo "disabled='true'";
                                    } ?>>
                                                    <i class="glyphicon glyphicon-ok"></i>
                                                </button>
                                                <button id="btnRejeitar" class="btn btn-default btn-xs"  page="<?= $agendamento['idAgendamento']; ?>" idAgendamento="<?= $agendamento['idAgendamento']; ?>"  title="Rejeitar Agendamento" <?php if ($agendamento['status'] >= 1) {
                                        echo "disabled='true'";
                                    } ?>>
                                                    <i class="glyphicon glyphicon-remove"></i>
                                                </button> 
                                                <button id="btnInfo" class="btn btn-default btn-xs" page="<?= $agendamento['idAgendamento']; ?>" idAgendamento="<?= $agendamento['idAgendamento']; ?>" title="Detalhe do Agendamento">
                                                    <i class="glyphicon glyphicon-info-sign"></i>
                                                </button>
                                            </td>
                                            <td align="center">
                                                <?php if ($agendamento['status'] == "2") { ?>
                                                    <button class="btn btn-xs btn-danger" style="width: 90px;">Rejeitado</button>
                                                <?php } elseif ($agendamento['status'] == "1") { ?>                                                     
                                                    <button class="btn btn-xs btn-success" style="width: 90px;">Aceito</button>
                                        <?php } else { ?>
                                                    <button class="btn btn-xs btn-warning" style="width: 90px;">Aberto</button>
                                        <?php } ?>
                                            </td>
                                        </tr>
                                <?php
                            }
                            ?>
                                </tbody>
                            </table>
            <?php include(kohana::find_file('views/paginator', 'paginacao', 'php')) ?> 
                        </div>
                    </div>
                </div>
            </div>
     </div>
<?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>

            <script>
                jQuery(document).ready(function () {
                    // Remove seleção de ativo no menu.
                    $('.nav li').removeClass('active');
                    $('#side-menu li').removeClass('active');
                    // Ativa botão no menu.
                    $('#li-salas ul').addClass('collapse in');
                    $('#li-ver-age a').addClass('active');
                    $('#li-salas').addClass('active');
                    $('#li-salas a').addClass('collapse in');

                    $('#area').dataTable(
                            {
                                "aoColumnDefs": [{"bSortable": false, "aTargets": [0, 1]}],
                                "paging": false,
                                "info": false,
                                "aaSorting": [[0, 'desc']],
                                "bFilter": false,
                                "oLanguage":
                                        {
                                            "sSearch": " Buscar: "
                                        }
                            });

                    $('#btnAceitar').click(function () {
                        var idAgendamento = $(this).attr('idAgendamento');
                        var page = $(this).attr('page');

                        $.get('confirmar?idAgendamento=' + idAgendamento, function (data) {
                            if (data === 1) {
                                window.location = 'home?page=' + page;
                            }
                        });
                    });
                    $('#btnInfo').click(function () {
                        var idAgendamento = $(this).attr('idAgendamento');
                        var options = {
                            url: "visualizar?idAgendamento=" + idAgendamento,
                            title: 'Visualizar Agendamento',
                            size: eModal.size.lg,
                            buttons: [
                                {text: 'FECHAR', style: 'danger', close: true}
                            ]
                        };

                        eModal.ajax(options);
                    });

                    $('.dataTables_filter label').addClass('pull-right');
                });
            </script>
    </body>
</html>
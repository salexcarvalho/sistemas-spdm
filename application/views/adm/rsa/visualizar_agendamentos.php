<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - RESERVA DE SALAS\ANFITEATRO</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <?php if($_SESSION['AcLiberaBtnVizPed'] == true) { ?>
                        <h1 class="page-header">Reserva de Salas</h1>
                        <ol class="breadcrumb">
                            <li><i class="fa fa-file-pdf-o"></i>Solicitação de Reserva</li>
                            <li class="active"><i class="fa fa-list"></i> Listagem de Solicitações</li>
                            <li class="active"><i class="glyphicon glyphicon-file"></i>Agendamentos</li>
                        </ol>
                          <form class="form-horizontal" role="form" method="POST" action="update_sala" >
                            <div class="form-group">
                            <div class="col-lg-6">
                                <div class="panel panel-default"> 
                                    <div class="panel-heading">
                                        <span>Dados da Sala</span>
                                    </div>    
                                    <div class="panel-body">
                                         <div class="form-group">
                                             <input type="hidden" name="idSalas" id="idSalas" value="<?=$agendamento[0]['idAgendamento']?>" />
                                                <label for="nome" class="col-lg-3 control-label">Evento:</label>
                                                <div class="col-lg-9">
                                                    <input type="text" min="0" class="form-control" id="nome" name="nome" placeholder="Nome da Sala" value="<?=$agendamento[0]['nome']?>" >
                                                </div>
                                            </div>
                                        <div class="form-group">
                                            <label for="endereco" class="col-lg-3 control-label">Capacidade:</label>
                                            <div class="col-lg-9">
                                                <input type="text" min="0" class="form-control" id="capacidade" name="capacidade" placeholder="Numero máximo de pessoas" <?=$cond;?> value="<?=$agendamento[0]['capacidade']?>">
                                            </div>
                                        </div>   
                                         <div class="form-group">
                                            <label for="endereco" class="col-lg-3 control-label">Extras:</label>
                                            <div class="col-lg-9">
                                                <textarea class="form-control" id="extras" name="extras" placeholder="Informar Equipamentos Disponiveis separando por ," <?=$cond;?>><?=$agendamento[0]['extras']?></textarea>
                                            </div>
                                        </div>
                                         <div class="form-group">
                                            <label for="endereco" class="col-lg-3 control-label">Area:</label>
                                            <div class="col-lg-9">
                                                <select class="form-control" id="area_id" name="area_id" <?=$cond;?>>
                                                    <?php 
                                                        $model_area = new Model_Rsa_Area('default');
                                                        $areas = $model_area->select_areas();
                                                        foreach ($areas as $area):
                                                    ?>
                                                    <option value="<?=$area['idArea']?>" <?php echo ($area['idArea']==$agendamento[0]['area_id'])? 'selected':''?>><?=$area['nome']?></option>
                                                    <?php endforeach;?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-offset-2 col-lg-5">
                                            <div class="form-group"> 
                                                <label class="radio-inline">
                                                    <input required type="radio" name="status" id="ativo" value="1" <?=$cond;?> <?php echo ($agendamento[0]['status']==1)? 'checked':''?>>
                                                    Ativo
                                                </label>
                                                <label class="radio-inline">
                                                    <input required type="radio" name="status" id="inativo" value="0" <?=$cond;?> <?php echo ($agendamento[0]['status']==0)? 'checked':''?>>
                                                    Inativo
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>        
                                <div class="form-group">
                                    <div class="col-lg-5">
                                        <button type="submit" class="btn btn-primary btn-sm" <?=$btn;?>><?php echo ($cond!='')? "Cadastrar":"Editar";?> </button>
                                        <a href="home" class="btn btn-danger">Voltar</a>
                                    </div>
                                </div>
                            </div>
                            </div>    
                        </form>
                        <?php } else { ?>
                            <h1 class="page-header"> Voce não possue acesso a está pagina, consulte o administrador.</h1>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>
        
            <script>
            jQuery(document).ready(function() {
                // Remove seleção de ativo no menu.
                $('.nav li').removeClass('active');
                $('#side-menu li').removeClass('active');
                // Ativa botão no menu.
                $('#li-salas ul').addClass('collapse in');
                $('#li-ver-age a').addClass('active');
                $('#li-salas').addClass('active');
                $('#li-salas a').addClass('collapse in');
            });
        </script>
</body>
</html>
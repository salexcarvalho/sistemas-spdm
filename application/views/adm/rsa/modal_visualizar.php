<?php Session::instance(); ?>
<?php Funcoes::carrega(); //echo "<pre>"; var_dump($_SESSION['idUsuario']); echo "</pre>";   ?>
<style>
    hr {
        margin-top: 0px;
    }
    .form-control[disabled], .form-control[disabled], fieldset[disabled] .form-control {
        background-color: #fdfdfd;
        opacity: 1;
    }
</style>
<script>
    $(document).ready(function () {

        $("#allDay").change(function () {
            if ($(this).prop('checked')) {
                $("#start").prop("disabled", true);
                $("#end").prop("disabled", true);
                $("#all").val(1);
            } else {
                $("#start").prop("disabled", false);
                $("#end").prop("disabled", false);
                $("#all").val(0);
            }
        });

        var d = new Date();
        var hora = d.getHours();
        if (hora < 7) {
            hora = 7;
        }
        var min = d.getMinutes();
        if (min < 10) {
            min = '0' + min;
        }
        var dataAtual, dia, mes, ano;
        dia = d.getDate();
        mes = d.getMonth() + 1;
        ano = d.getFullYear();

        if (mes < 10) {
            mes = "0" + mes;
        }
        if (dia < 10) {
            dia = "0" + dia;
        }

        dataAtual = ano + "-" + mes + "-" + dia;

        var horaAtual = hora + ":" + min;

        $('.form_date').datetimepicker({
            locale: 'pt-br',
            format: "D/M/YYYY",
            minDate: dataAtual
        });

        $('.form_date2').datetimepicker({
            locale: 'pt-br',
            format: "D/M/YYYY",
            minDate: dataAtual
        });

        $('.form_time').datetimepicker({
            format: 'LT',
            disabledHours: [0, 1, 2, 3, 4, 5, 6, 24],
            enabledHours: [8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23],
            locale: 'pt-br'
        });

        $('.form_time2').datetimepicker({
            format: 'LT',
            disabledHours: [0, 1, 2, 3, 4, 5, 6, 24],
            enabledHours: [8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23],
            locale: 'pt-br'
        });

        var end_data = moment(<?= "'" . $dados[0]['end_data'] . "'"; ?>).format('DD/MM/YYYY');
        var start_data = moment(<?= "'" . $dados[0]['start_data'] . "'"; ?>).format('DD/MM/YYYY');
        var end_hora = moment(<?= "'" . $dados[0]['end_data'] . "'"; ?>).format('HH:mm');
        var start_hora = moment(<?= "'" . $dados[0]['start_data'] . "'"; ?>).format('HH:mm');

        <?php if ($dados[0]['end_data'] != "") { ?>
            $('#start_data').val(start_data);
            $('#end_data').val(end_data);
            $('#start_hora').val(start_hora);
            $('#end_hora').val(end_hora);
        <?php } ?>
            
    });
</script>

<div style="height: 480px;" class="container-fluid">
        <div class='form-group col-md-12'>
            <input type='text' class='form-control' name='titulo' id='titulo' placeholder="Informe o nome do evento" value="<?= $dados[0]['titulo'] ?>" disabled />                                                                   
        </div>
        <div class='form-group col-md-12'>
            <select name='sala_id' id='sala_id' class='form-control col-md-10' disabled>
                <option value='0' rgb=''>Selecione o espaço</option>
                <?php foreach ($salasReserva as $salaR): ?>
                    <option value='<?= $salaR['idSalas'] ?>' <?php if ($dados[0]['sala_id'] == $salaR['idSalas']) {
                    echo 'selected=selected';
                } ?> rgb='<?= $salaR['rgb'] ?>' ><?= $salaR['Alias'] ?> - <?= $salaR['nome'] ?></option>   
<?php endforeach; ?>                  
            </select>
            <input type='hidden' id='idReserva' name='idReserva' value="<?= $dados[0]['idReserva'] ?>" />                                    
        </div>
        <div class='form-group col-md-6'>
            <label for='start_data' class='input-group'>Data Inicial: </label>
            <div class='input-group date form_date'>
                <input type='text' class='form-control' name='start_data' id='start_data' disabled /> 
                <span class='input-group-addon'>
                    <span class='glyphicon glyphicon-calendar'></span>                                                
                </span>
            </div>
        </div>
        <div class='form-group col-md-1'></div>
        <div class='form-group col-md-5'>
            <label for='start_hora' class='input-group'>Hora Inicial: </label>
            <div class='input-group date form_time' >
                <input type='text' class='form-control' name='start_hora' id='start_hora' disabled/>                                   
                <span class='input-group-addon'>
                    <span class='glyphicon glyphicon-time'></span>
                </span>                                         
            </div> 
        </div>
        <div class='form-group col-md-6'>
            <label for='end_data' class='input-group'>Data Término: </label>
            <div class='input-group date form_date2' >
                <input type='text' class='form-control' name='end_data' id='end_data' disabled/> 
                <span class='input-group-addon'>
                    <span class='glyphicon glyphicon-calendar'></span>
                </span>                                         
            </div>
        </div>
        <div class='form-group col-md-1'></div>
        <div class='form-group col-md-5'>
            <label for='end_hora' class='input-group'>Hora Término: </label>
            <div class='input-group date form_time2' > 
                <input type='text' class='form-control' name='end_hora' id='end_hora' disabled />                                   
                <span class='input-group-addon'>
                    <span class='glyphicon glyphicon-time'></span>
                </span>                                         
            </div> 
        </div>
        
        <?php if ($dados[0]['idSerie'] != '') : ?>
        <div class='form-group col-md-12'>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label>Gostaria de fazer a alteração em todas as séries deste agendamento?</label>&nbsp;&nbsp;&nbsp;&nbsp;
               <input disabled type='radio' name='Serie' id='Serie' value ='S' />&nbsp;Sim &nbsp;&nbsp;
                <input disabled type='radio' name='Serie' id='Serie' value ='N' />&nbsp;Não
        </div>
        <?php endif; ?>
        
        <div class='form-group col-md-12'>
            <label for="obs">Observação</label><br>                                 
            <textarea disabled class='form-control' rows="5" id="obs" name="obs"><?= $dados[0]['obs'] ?></textarea>
        </div>
        <div class='form-group col-md-1'></div>
                
        <div class='form-group col-md-12'>
            <input type='text' class='form-control' name='responsavel' id='responsavel' placeholder="Informe o nome do responsavel pelo Evento" value="<?= $dados[0]['responsavel'] ?>"  disabled />                                                                   
        </div>
        
        <?php if ($dados[0]['nmNome'] != '') { ?>
            <div class='form-group col-md-12'>                                     
                <div id='owner'><strong>Criado por:</strong> <?= $dados[0]['nmNome'] ?></div>
            </div>
        <?php } ?>                                   
</div>
<script>
        $('#inicioSerie1').on("click", function (){
            $('#semana').show();
        }); 
        $('#inicioSerie2').on("click", function (){
           $('#semana').hide();
        }); 
</script>
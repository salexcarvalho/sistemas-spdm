<?php if ($_SESSION['AcLiberaBtnAddAss'] == true) { ?>
    <form id="novasala" name="novasala" class="form-horizontal" role="form" method="POST" action="insert_sala" >
        <div class="form-group">
            <div class="col-lg-12">
                <div class="form-group">
                    <label for="nome" class="col-lg-3 control-label">Nome:</label>
                    <div class="col-lg-9">
                        <input type="text" min="0" class="form-control" id="nome" name="nome" placeholder="Nome da Sala" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="endereco" class="col-lg-3 control-label">Capacidade:</label>
                    <div class="col-lg-9">
                        <input type="text" min="0" class="form-control" id="capacidade" name="capacidade" placeholder="Numero máximo de pessoas" required>
                    </div>
                </div>   
                <div class="form-group">
                    <label for="endereco" class="col-lg-3 control-label">Extras:</label>
                    <div class="col-lg-9">
                        <textarea class="form-control" id="extras" name="extras" placeholder="Informar Equipamentos Disponiveis separando por ," ></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="endereco" class="col-lg-3 control-label">Area:</label>
                    <div class="col-lg-9">
                        <select class="form-control" id="area_id" name="area_id">
                            <?php
                            $model_area = new Model_Rsa_Area('default');
                            $areas = $model_area->select_areas();
                            foreach ($areas as $area):
                                ?>
                                <option value="<?= $area['idArea'] ?>"><?= $area['nome'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="col-lg-offset-4 col-lg-8">
                    <div class="form-group"> 
                        <label class="radio-inline">
                            <input required type="radio" name="status" id="ativo" value="1">
                            Ativo
                        </label>
                        <label class="radio-inline">
                            <input required type="radio" name="status" id="inativo" value="0">
                            Inativo
                        </label>
                    </div>
                </div>
            </div>
        </div>        
    </form>
<?php } else { ?>
    <h1 class="page-header"> Voce não possue acesso a está pagina, consulte o administrador.</h1>
<?php } ?>
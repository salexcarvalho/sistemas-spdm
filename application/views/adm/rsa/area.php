<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - RESERVA DE SALAS\ANFITEATRO</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
    </head>
    <body>
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Reserva de Salas</h1>
                        <ol class="breadcrumb">
                            <li><i class="fa fa-file-pdf-o"></i> Reserva de Salas</li>
                            <li class="active"><i class="fa fa-list"></i> Lista de Localizações</li>
                        </ol> 
                        <?php if (isset($warning)) : ?>
                            <div id="msg" class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong><?= $warning; ?></strong>
                            </div>
                        <?php endif ?> 
                        <div class="row">
                            <?php if ($_SESSION['AcLiberaBtnAddArea'] == true) { ?>
                                <div class="col-lg-12">
                                    <button type="submit" id="btn_nova_area" class="pull-right btn btn-primary btn-md" name="btn_nova_area"><i class="glyphicon glyphicon-plus-sign"></i> Nova Area</button>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="area">
                                <thead>
                                    <tr>
                                        <th width="5%" style="text-align: center">#</th>
                                        <th>Area</th>
                                        <th width="31%" >Local</th>
                                        <th width="22%" >Salas</th>                                              
                                        <th width="9%" style="text-align: center">Ações</th>
                                        <th width="5%" style="text-align: center">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $model_salas = new Model_Rsa_Salas('default');
                                    foreach ($areas as $area) {
                                        $salas = $model_salas->select_salas_area($area['idArea']);
                                        ?>
                                        <tr>
                                            <td align="center"><?= $area['idArea']; ?></td>
                                            <td><?= $area['nome']; ?></td>
                                            <td><?= $area['local'] ?></td>
                                            <td><?php
                                                foreach ($salas as $sala):
                                                    echo $sala['nome'] . "<br />";
                                                endforeach;
                                                ?></td>
                                            <td align="center">
                                                    <button type="submit" id="btnVisualizar" class="btn btn-default btn-xs" page="<?= $PaginaAtual; ?>" idArea="<?= $area['idArea']; ?>" name="btnVisualizar" title="Visualizar Area" <?php if ($_SESSION['AcLiberaBtnVizArea'] == NULL) { ?>disabled<?php } ?>>
                                                        <i class="glyphicon glyphicon-file"></i>
                                                    </button>
                                                    <button  type="submit" id="btnEditar" class="btn btn-default btn-xs" name="btnEditar" page="<?= $PaginaAtual; ?>" idArea="<?= $area['idArea']; ?>" title="Editar Area" <?php if ($_SESSION['AcLiberaBtnEdiArea'] == NULL) { ?>disabled<?php } ?>>
                                                        <i class="glyphicon glyphicon-edit"></i>
                                                    </button>
                                                <input type="hidden" id="idArea_<?= $area['idArea']; ?>" name="idArea" value="<?= $area['idArea']; ?>">
                                                <button  type="button" id="btnExcluir" class="btn btn-default btn-xs" name="btnExcluir" idArea="<?= $area['idArea']; ?>" nome="<?= $area['nome']; ?>" <?php if ($_SESSION['AcLiberaBtnExcArea'] == NULL) { ?>disabled<?php } ?> data-toggle="modal" data-target="#modal_excluir" data-placement="bottom" title="Excluir Area" data-delay="1">
                                                    <i class="glyphicon glyphicon-trash"></i>
                                                </button>
                                            </td>
                                            <td align="center">
                                                <input type="hidden" name="idArea" value="<?= $area['idArea']; ?>">
                                                <?php
                                                if ($area['status'] == "0") {
                                                    ?>
                                                    <button type="submit" id="btnAtivar_<?= $area['idArea']; ?>" class="btn btn-danger btn-xs" idArea="<?= $area['idArea']; ?>" name="btnAtivar" title="Ativar Area" <?php if ($_SESSION['AcLiberaBtnAtivArea'] == NULL) { ?>disabled<?php } ?>>
                                                        <i id="activ_<?= $area['idArea']; ?>" class="glyphicon glyphicon-ban-circle"></i>
                                                    </button>
                                                <?php } else { ?>
                                                    <button type="submit" id="btnAtivar_<?= $area['idArea']; ?>" class="btn btn-success btn-xs" idArea="<?= $area['idArea']; ?>" name="btnAtivar" title="Destivar Area" <?php if ($_SESSION['AcLiberaBtnAtivArea'] == NULL) { ?>disabled<?php } ?>>
                                                        <i id="activ_<?= $area['idArea']; ?>" class="glyphicon glyphicon-ok"></i>
                                                    </button>
                                                    <?php
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                            <?php include(kohana::find_file('views/paginator', 'paginacao', 'php')) ?> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>

        <script>
            jQuery(document).ready(function () {
                // Remove seleção de ativo no menu.
                $('.nav li').removeClass('active');
                $('#side-menu li').removeClass('active');
                // Ativa botão no menu.
                $('#li-salas ul').addClass('collapse in');
                $('#li-cad-areas a').addClass('active');
                $('#li-salas').addClass('active');
                $('#li-salas a').addClass('collapse in');
                
                // Datatables
                $('#area').dataTable(
                        {
                            "aoColumnDefs": [{"bSortable": false, "aTargets": [0, 1]}],
                            "paging": false,
                            "info": false,
                            "aaSorting": [[0, 'desc']],
                            "bFilter": false,
                            "oLanguage":
                                    {
                                        "sSearch": " Buscar: "
                                    }
                        });
                
                // abre o modal para cadastro
                $('button[name=btn_nova_area]').click(function () {
                    var idArea = '';
                    var options = {
                        url: "cadastro_area?idArea=" + idArea,
                        title: 'Cadastrar Área/Localizações',
                        size: eModal.size.lg,
                        buttons: [
                            {text: 'GRAVAR', style: 'primary', close: false, type: 'submit', form: 'novaarea'},
                            {text: 'FECHAR', style: 'danger', close: true}
                        ]
                    };
                    eModal.ajax(options);
                });

                //abre o modal de edição
                $('button[name=btnEditar]').click(function () {
                    var idArea = $(this).attr('idArea');
                    var cd = '1'; 
                    var page = $(this).attr('page');
                    var options = {
                        url: "visualizar_area?idArea=" + idArea + "&page=" + page + "&cd=" + cd,
                        title: 'Editar Área/Localizações',
                        size: eModal.size.lg,
                        buttons: [
                            {text: 'SALVAR', style: 'primary', close: false, type: 'submit', form: 'novaarea'},
                            {text: 'FECHAR', style: 'danger', close: true}
                        ]
                    };
                    eModal.ajax(options);
                });
                
                //abre o modal de visualização
                $('button[name=btnVisualizar]').click(function () {
                    var idArea = $(this).attr('idArea');
                    var page = $(this).attr('page');
                    var options = {
                        url: "visualizar_area?idArea=" + idArea + "&page=" + page,
                        title: 'Visualizar Área/Localizações',
                        size: eModal.size.lg,
                        buttons: [
                            {text: 'FECHAR', style: 'danger', close: true}
                        ]
                    };
                    eModal.ajax(options);
                });
        
                // Opção usada na exclusão 
                var idArea;
                var nome;
                $('button[name=btnExcluir]').on("click", function ()
                {
                    idArea = $(this).attr('idArea');
                    nome = $(this).attr('nome');
                    $('#modal_delete_evento').html("Deseja excluir o Evento <b>\"" + nome + "\"</b> ?");
                });

                $('#modal_excluir').on('hidden.bs.modal', function ()
                {
                    window.location.replace("home");
                });

                $('#modal_btn-excluir').on("click", function ()
                {
                    var btn = $(this);
                    btn.text('loading');

                    btn.removeClass('btn-primary');
                    btn.removeClass('btn-danger');
                    btn.removeClass('btn-success');
                    btn.addClass('btn-primary');

                    $('#modal_btn-excluir').prop('disabled', true);

                    $.ajax("delete_area",
                            {
                                type: "POST",
                                data:
                                        {
                                            idArea: idArea
                                        }
                            })
                            .done(function (data)
                            {
                                $('#modal_delete_area').html('Area excluída com sucesso');

                                btn.removeClass('btn-primary');
                                btn.addClass('btn-success');
                            })
                            .fail(function ()
                            {
                                $('#modal_delete_area').html('Erro: ' + data);

                                btn.removeClass('btn-primary');
                                btn.addClass('btn-danger');
                            })
                            .always(function ()
                            {
                                btn.text('Excluído');
                            });
                });

                // Opção para ativar e desativar
                $("body").on("click", "button[name=btnAtivar]", function () {
                    var idArea = $(this).attr('idArea');
                    //alert(txtUsuario);
                    $.get('update_ativar_area?idArea=' + idArea, function (data) {

                        if (data === "1") {
                            $('#btnAtivar_' + idArea).addClass('btn btn-success btn-xs');
                            $('#btnAtivar_' + idArea).removeClass('btn-danger');
                            $('#activ_' + idArea).removeClass('glyphicon-ban-circle');
                            $('#activ_' + idArea).addClass('glyphicon-ok');
                        } else {
                            $('#btnAtivar_' + idArea).addClass('btn btn-danger btn-xs');
                            $('#btnAtivar_' + idArea).removeClass('btn-success');
                            $('#activ_' + idArea).addClass('glyphicon-ok');
                            $('#activ_' + idArea).addClass('glyphicon-ban-circle');
                        }
                    });
                });

                $('.dataTables_filter label').addClass('pull-right');
            });
        </script>
    </body>
</html>
<?php Session::instance(); ?>
<?php Funcoes::carrega(); //echo "<pre>"; var_dump($_SESSION['idUsuario']); echo "</pre>";    ?>

<!-- Bootstrap Core CSS -->
<link href="<?= URL::base(); ?>theme/backend/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- MetisMenu CSS -->
<link href="<?= URL::base(); ?>theme/backend/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

<!-- SbAdmin CSS -->
<link href="<?= URL::base(); ?>theme/backend/dist/css/sb-admin-2.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="<?= URL::base(); ?>theme/backend/css/custom.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="<?= URL::base(); ?>theme/backend/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

<!-- jQuery Version 3.2.1 
<script src="<?= URL::base(); ?>theme/backend/js/jquery-migrate-1.4.1.min.js"></script>-->
<script src="<?= URL::base(); ?>theme/backend/js/jquery-migrate-3.0.0.min.js"></script>
<script src="<?= URL::base(); ?>theme/backend/js/jquery-3.2.1.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?= URL::base(); ?>theme/backend/vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="<?= URL::base(); ?>theme/backend/vendor/metisMenu/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="<?= URL::base(); ?>theme/backend/dist/js/sb-admin-2.js"></script>

<!-- datapicker plugin -->
<script src='<?= URL::base(); ?>theme/backend/vendor/fullcalendar/lib/moment.min.js'></script>       
<script src='<?= URL::base(); ?>theme/backend/vendor/fullcalendar/locale/pt-br.js'></script>

<script src='<?= URL::base(); ?>theme/backend/vendor/fullcalendar/lib/jquery-ui.min.js'></script>
<link href='<?= URL::base(); ?>theme/backend/vendor/datetimepicker/css/bootstrap-datetimepicker.min.css' rel='stylesheet' />
<script type="text/javascript" src="<?= URL::base(); ?>theme/backend/vendor/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>

<style>
    hr {
        margin-top: 0px;
    }
</style>
<script>
    $(document).ready(function () {

        var d = new Date();
        var hora = d.getHours();
        if (hora < 7) {
            hora = 7;
        }
        var min = d.getMinutes();
        if (min < 10) {
            min = '0' + min;
        }
        var dataAtual, dia, mes, ano;
        dia = d.getDate();
        mes = d.getMonth() + 1;
        ano = d.getFullYear();

        if (mes < 10) {
            mes = "0" + mes;
        }
        if (dia < 10) {
            dia = "0" + dia;
        }

        dataAtual = ano + "-" + mes + "-" + dia;

        var horaAtual = hora + ":" + min;

        $('.form_date3').datetimepicker({
            locale: 'pt-BR',
            format: "D/M/YYYY",
            minDate: dataAtual
        });
    });
</script>

<div id="modal_imprimir" class="container-fluid bd-example-row">
    <form name='imprimrirForm' id='imprimrirForm' action="imprimirReserva" class='form-horizontal'  method="GET" class='form-horizontal' target="_blank">
        <div class='form-group col-md-12'>
            <label for='ini_relat' class='input-group'>Relatório Semanal a partir: </label>
            <div class='input-group date form_date3' >
                <input type='text' class='form-control' id='ini_relat' name='ini_relat' value="<?= date('d/m/Y'); ?>"/> 
                <span class='input-group-addon'>
                    <span class='glyphicon glyphicon-calendar'></span>
                </span>  

            </div>                               

        </div>
        
        <div class='form-group col-md-12'>
            <label for='idArea' class='input-group'>Localização:</label>
            <select name='idArea' id='idArea' class='form-control col-md-10' required style="margin-bottom: 100px;">
                <option value='0'>Selecione o Area</option>
                <?php foreach ($dados as $area): ?>
                    <option value='<?= base64_encode($area['idArea']); ?>' ><?= $area['nome'] ?></option>   
                <?php endforeach; ?>                  
            </select>                                      
        </div>  
    </form>                             
</div>

<style type="text/css">
    table {width: 100%; border: none;}
    .verticalText
    {
        text-align: center;
        white-space: nowrap;
    };
</style>

<?php
$d1 = Funcoes::convertData($data);

$sema = date('w', strtotime($d1));

switch ($sema):
    case 0:
        $dom = date('Y-m-d', strtotime($d1));
        $seg = date('Y-m-d', strtotime('+1 days', strtotime($d1)));
        $ter = date('Y-m-d', strtotime('+2 days', strtotime($d1)));
        $qua = date('Y-m-d', strtotime('+3 days', strtotime($d1)));
        $qui = date('Y-m-d', strtotime('+4 days', strtotime($d1)));
        $sex = date('Y-m-d', strtotime('+5 days', strtotime($d1)));
        $sab = date('Y-m-d', strtotime('+6 days', strtotime($d1)));
        $dif = 6;
        break;
    case 1:
        $dom = date('Y-m-d', strtotime('-1 days', strtotime($d1)));
        $seg = date('Y-m-d', strtotime($d1));
        $ter = date('Y-m-d', strtotime('+1 days', strtotime($d1)));
        $qua = date('Y-m-d', strtotime('+2 days', strtotime($d1)));
        $qui = date('Y-m-d', strtotime('+3 days', strtotime($d1)));
        $sex = date('Y-m-d', strtotime('+4 days', strtotime($d1)));
        $sab = date('Y-m-d', strtotime('+5 days', strtotime($d1)));
        $dif = 5;
        break;
    case 2:
        $dom = date('Y-m-d', strtotime('-2 days', strtotime($d1)));
        $seg = date('Y-m-d', strtotime('-1 days', strtotime($d1)));
        $ter = date('Y-m-d', strtotime($d1));
        $qua = date('Y-m-d', strtotime('+1 days', strtotime($d1)));
        $qui = date('Y-m-d', strtotime('+2 days', strtotime($d1)));
        $sex = date('Y-m-d', strtotime('+3 days', strtotime($d1)));
        $sab = date('Y-m-d', strtotime('+4 days', strtotime($d1)));
        $dif = 4;
        break;
    case 3:
        $dom = date('Y-m-d', strtotime('-3 days', strtotime($d1)));
        $seg = date('Y-m-d', strtotime('-2 days', strtotime($d1)));
        $ter = date('Y-m-d', strtotime('-1 days', strtotime($d1)));
        $qua = date('Y-m-d', strtotime($d1));
        $qui = date('Y-m-d', strtotime('+1 days', strtotime($d1)));
        $sex = date('Y-m-d', strtotime('+2 days', strtotime($d1)));
        $sab = date('Y-m-d', strtotime('+3 days', strtotime($d1)));
        $dif = 3;
        break;
    case 4:
        $dom = date('Y-m-d', strtotime('-4 days', strtotime($d1)));
        $seg = date('Y-m-d', strtotime('-3 days', strtotime($d1)));
        $ter = date('Y-m-d', strtotime('-2 days', strtotime($d1)));
        $qua = date('Y-m-d', strtotime('-1 days', strtotime($d1)));
        $qui = date('Y-m-d', strtotime($d1));
        $sex = date('Y-m-d', strtotime('+1 days', strtotime($d1)));
        $sab = date('Y-m-d', strtotime('+2 days', strtotime($d1)));
        $dif = 2;
        break;
    case 5:
        $dom = date('Y-m-d', strtotime('-5 days', strtotime($d1)));
        $seg = date('Y-m-d', strtotime('-4 days', strtotime($d1)));
        $ter = date('Y-m-d', strtotime('-3 days', strtotime($d1)));
        $qua = date('Y-m-d', strtotime('-2 days', strtotime($d1)));
        $qui = date('Y-m-d', strtotime('-1 days', strtotime($d1)));
        $sex = date('Y-m-d', strtotime($d1));
        $sab = date('Y-m-d', strtotime('+1 days', strtotime($d1)));
        $dif = 1;
        break;
    case 6:
        $dom = date('Y-m-d', strtotime('-6 days', strtotime($d1)));
        $seg = date('Y-m-d', strtotime('-5 days', strtotime($d1)));
        $ter = date('Y-m-d', strtotime('-4 days', strtotime($d1)));
        $qua = date('Y-m-d', strtotime('-3 days', strtotime($d1)));
        $qui = date('Y-m-d', strtotime('-2 days', strtotime($d1)));
        $sex = date('Y-m-d', strtotime('-1 days', strtotime($d1)));
        $sab = date('Y-m-d', strtotime($d1));
        $dif = 0;
        break;
endswitch;

$model_reserva = new Model_Rsa_Reserva('default');
$count = count($local);

?>
<page backtop="20mm" backbottom="0mm" backleft="0mm" backright="0mm">
    <page_header>
        <table width="100%" border="0.1" cellspacing="0" cellpadding="0">
            <tr>		
                <th height="60" width="200" bgcolor="#f5f5f5" align="center"><img src="<?=LOGO?>" alt="logo" height="55" class="hover" /></th>
                <th height="60" width="555" bgcolor="#f5f5f5" align="center">
                    <span style="font-size:16px;"><?= $local[0]['Area'] ?></span><br><br>
                    <span style="font-size:12px;">Quadro de Horários</span></th>
            </tr>         
        </table>
    </page_header>                  

    <?php
    for ($j = 0; $j < count($local); $j++):
        ?>
        <table width="100%" border="0.5" cellspacing="0" cellpadding="2">
            <tr>
                <td align="center" width="751" height="20" bgcolor="#000" style="font-size:13px; color: #FFF; font-weight: bold; vertical-align:middle;">&nbsp;<?= $local[$j]['nome']; ?></td>	
            </tr>
        </table>
        <table width="100%" cellspacing="0" cellpadding="2">
            <tr>		
                <td height="2" valign="middle"></td>
            </tr>
        </table>
        <?php
        $sala = $local[$j]['idSalas'];
        $reserva = $model_reserva->select_reservas_relatorio2($dom, $sab, $sala);
        
        if (count($reserva) >= 1) {
    
        foreach ($reserva as $reservas):
            ?>
                <table width="100%" border="0.1" cellspacing="0" cellpadding="2">
                    <tr>		
                        <td colspan="2" height="25" style="font-size:11px; vertical-align:top;">
                            &nbsp;<span style="font-size:5px;">NOME DO EVENTO/CURSO:</span><br>
                            &nbsp;<span><strong><?= $reservas['titulo'] ?></strong></span>
                        </td>
                        <td colspan="2" style="font-size:11px; vertical-align:top;">
                            &nbsp;<span style="font-size:5px;">RESPONSÁVEL:</span><br>
                            &nbsp;<span><?= $reservas['responsavel'] ?></span>
                        </td>
                    </tr>
                    <tr>		
                        <td height="25" width="248" style="font-size:11px; vertical-align:top;">
                            &nbsp;<span style="font-size:5px;">DATA DE INÍCIO:</span><br>
                            &nbsp;<span><strong><?= Funcoes::convertDataBr($reservas['start_data']) ?></strong></span>
                        </td>
                        <td width="122" style="font-size:11px; vertical-align:top;">
                            &nbsp;<span style="font-size:5px;">HORA DE INÍCIO:</span><br>
                            &nbsp;<span><?= Funcoes::hora($reservas['start_data']) ?></span>
                        </td>
                        <td height="25" width="248" style="font-size:11px; vertical-align:top;">
                            &nbsp;<span style="font-size:5px;">DATA DE TERMINO:</span><br>
                            &nbsp;<span><strong><?= Funcoes::convertDataBr($reservas['end_data']) ?></strong></span>
                        </td>
                        <td width="122" style="font-size:11px; vertical-align:top;">
                            &nbsp;<span style="font-size:5px;">HORA DE TERMINO:</span><br>
                            &nbsp;<span><?= Funcoes::hora($reservas['end_data']) ?></span>
                        </td>
                    </tr>
                </table>
                <table width="100%" cellspacing="0" cellpadding="2">
                    <tr>		
                        <td height="3" valign="middle"></td>
                    </tr>
                </table>
                <?php endforeach; ?>
            <?php } else { ?>
                <table width="100%" border="0.1" cellspacing="0" cellpadding="2">
                    <tr>		
                        <td height="25" width="752" valign="middle" style="font-size:11px; vertical-align:top;"><span><strong>&nbsp;&nbsp;Sem Agendamento</strong></span></td>
                    </tr>
                </table>
                <table width="100%" cellspacing="0" cellpadding="2">
                    <tr>		
                        <td height="3" valign="middle"></td>
                    </tr>
                </table>
            <?php } ?>
        <br>
    <?php endfor; ?>                     
</page>

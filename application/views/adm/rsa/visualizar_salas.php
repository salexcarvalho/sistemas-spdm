<?php if ($_SESSION['AcLiberaBtnAddAss'] == true) { ?>
    <form id="novasala" name="novasala" class="form-horizontal" role="form" method="POST" action="update_sala" >
        <div class="form-group">
            <div class="col-lg-12">
                <div class="form-group">
                    <input type="hidden" name="idSalas" id="idSalas" value="<?= $salas[0]['idSalas'] ?>" />
                    <label for="nome" class="col-lg-3 control-label">Nome:</label>
                    <div class="col-lg-9">
                        <input type="text" min="0" class="form-control" id="nome" name="nome" placeholder="Nome da Sala" <?= $cond; ?> value="<?= $salas[0]['nome'] ?>" >
                    </div>
                </div>
                <div class="form-group">
                    <label for="endereco" class="col-lg-3 control-label">Capacidade:</label>
                    <div class="col-lg-9">
                        <input type="text" min="0" class="form-control" id="capacidade" name="capacidade" placeholder="Numero máximo de pessoas" <?= $cond; ?> value="<?= $salas[0]['capacidade'] ?>">
                    </div>
                </div>   
                <div class="form-group">
                    <label for="endereco" class="col-lg-3 control-label">Extras:</label>
                    <div class="col-lg-9">
                        <textarea class="form-control" id="extras" name="extras" placeholder="Informar Equipamentos Disponiveis separando por ," <?= $cond; ?>><?= $salas[0]['extras'] ?></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="endereco" class="col-lg-3 control-label">Area:</label>
                    <div class="col-lg-9">
                        <select class="form-control" id="area_id" name="area_id" <?= $cond; ?>>
                            <?php
                            $model_area = new Model_Rsa_Area('default');
                            $areas = $model_area->select_areas();
                            foreach ($areas as $area):
                                ?>
                                <option value="<?= $area['idArea'] ?>" <?php echo ($area['idArea'] == $salas[0]['area_id']) ? 'selected' : '' ?>><?= $area['nome'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="col-lg-offset-4 col-lg-8">
                    <div class="form-group"> 
                        <label class="radio-inline">
                            <input required type="radio" name="status" id="ativo" value="1" <?= $cond; ?> <?php echo ($salas[0]['status'] == 1) ? 'checked' : '' ?>>
                            Ativo
                        </label>
                        <label class="radio-inline">
                            <input required type="radio" name="status" id="inativo" value="0" <?= $cond; ?> <?php echo ($salas[0]['status'] == 0) ? 'checked' : '' ?>>
                            Inativo
                        </label>
                    </div>
                </div>
            </div>
        </div>        
    </form>
<?php } else { ?>
    <h1 class="page-header"> Voce não possue acesso a está pagina, consulte o administrador.</h1>
<?php } ?>
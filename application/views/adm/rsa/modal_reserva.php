<?php Session::instance(); ?>
<?php Funcoes::carrega(); //echo "<pre>"; var_dump($_SESSION['idUsuario']); echo "</pre>";   ?>
<style>
    hr {
        margin-top: 0px;
    }
</style>
<script>
    $(document).ready(function () {
        var d = new Date();
        var hora = d.getHours();
        if (hora < 7) {
            hora = 7;
        }
        var min = d.getMinutes();
        if (min < 10) {
            min = '0' + min;
        }
        var dataAtual, dia, mes, ano;
        dia = d.getDate();
        mes = d.getMonth() + 1;
        ano = d.getFullYear();

        if (mes < 10) {
            mes = "0" + mes;
        }
        if (dia < 10) {
            dia = "0" + dia;
        }

        dataAtual = ano + "-" + mes + "-" + dia;

        var horaAtual = hora + ":" + min;

        $('.form_date').datetimepicker({
            locale: 'pt-br',
            format: "D/M/YYYY",
            minDate: dataAtual
        });

        $('.form_date2').datetimepicker({
            locale: 'pt-br',
            format: "D/M/YYYY",
            minDate: dataAtual
        });

        $('.form_time').datetimepicker({
            format: 'LT',
            disabledHours: [0, 1, 2, 3, 4, 5, 6, 24],
            enabledHours: [8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23],
            locale: 'pt-br'
        });

        $('.form_time2').datetimepicker({
            format: 'LT',
            disabledHours: [0, 1, 2, 3, 4, 5, 6, 24],
            enabledHours: [8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23],
            locale: 'pt-br'
        });

        var end_data = moment(<?= "'" . $dados[0]['end_data'] . "'"; ?>).format('DD/MM/YYYY');
        var start_data = moment(<?= "'" . $dados[0]['start_data'] . "'"; ?>).format('DD/MM/YYYY');
        var end_data_serie = moment(<?= "'" . $seriefim[0]['end_data'] . "'"; ?>).format('DD/MM/YYYY');
        var start_data_serie = moment(<?= "'" . $serieinicio[0]['start_data'] . "'"; ?>).format('DD/MM/YYYY');
        var end_hora = moment(<?= "'" . $dados[0]['end_data'] . "'"; ?>).format('HH:mm');
        var start_hora = moment(<?= "'" . $dados[0]['start_data'] . "'"; ?>).format('HH:mm');

        <?php if ($dados[0]['end_data'] != "") { ?>
            $('#start_data').val(start_data);
            $('#end_data').val(end_data);
            $('#start_data_serie').val(start_data_serie);
            $('#end_data_serie').val(end_data_serie);
            $('#start_hora').val(start_hora);
            $('#end_hora').val(end_hora);
        <?php } ?>
            
    });
</script>

<div style="height: 480px;" class="container-fluid">
    
    <?php if ($idReserva !== '') : ?>
        <form name='reservaForm' id='reservaForm' class='form-horizontal' method="POST" action="updateReservas">
    <?php else : ?>
        <form name='reservaForm' id='reservaForm' class='form-horizontal' method="POST" action="setReservas" >
    <?php endif; ?>    
    
        <div class='form-group col-md-12'>
            <input type='text' class='form-control' name='titulo' id='titulo' placeholder="Informe o nome do evento" value="<?= $dados[0]['titulo'] ?>" required />                                                                   
        </div>
        <div class='form-group col-md-12'>
            <select name='sala_id' id='sala_id' class='form-control col-md-10' required>
                <option value='0' rgb=''>Selecione o espaço</option>
                <?php foreach ($salasReserva as $salaR): ?>
                    <option value='<?= $salaR['idSalas'] ?>' <?php if ($dados[0]['sala_id'] == $salaR['idSalas']) {
                    echo 'selected=selected';
                } ?> rgb='<?= $salaR['rgb'] ?>' ><?= $salaR['Alias'] ?> - <?= $salaR['nome'] ?></option>   
<?php endforeach; ?>                  
            </select>
            <input type='hidden' id='idReserva' name='idReserva' value="<?= $dados[0]['idReserva'] ?>" />                                    
        </div>
        <div class='form-group col-md-6'>
            <label for='start_data' class='input-group'>Data Inicial: </label>
            <div class='input-group date form_date'>
                <input type='text' class='form-control' name='start_data' id='start_data' required <?php if ($dados[0]['idSerie'] != '') : ?> readonly <?php endif; ?>/> 
                <span class='input-group-addon'>
                    <span class='glyphicon glyphicon-calendar'></span>                                                
                </span>
            </div>
            <input type='hidden' class='form-control' name='start_data_serie' id='start_data_serie' />  
        </div>
        <div class='form-group col-md-1'></div>
        <div class='form-group col-md-5'>
            <label for='start_hora' class='input-group'>Hora Inicial: </label>
            <div class='input-group date form_time' >
                <input type='text' class='form-control' name='start_hora' id='start_hora' required/>                                   
                <span class='input-group-addon'>
                    <span class='glyphicon glyphicon-time'></span>
                </span>                                         
            </div> 
        </div>
        <div class='form-group col-md-6'>
            <label for='end_data' class='input-group'>Data Término: </label>
            <div class='input-group date form_date2' >
                <input type='text' class='form-control' name='end_data' id='end_data' required <?php if ($dados[0]['idSerie'] != '') : ?> readonly <?php endif; ?>/> 
                <span class='input-group-addon'>
                    <span class='glyphicon glyphicon-calendar'></span>
                </span>                                         
            </div> 
            <input type='hidden' class='form-control' name='end_data_serie' id='end_data_serie' /> 
        </div>
        <div class='form-group col-md-1'></div>
        <div class='form-group col-md-5'>
            <label for='end_hora' class='input-group'>Hora Término: </label>
            <div class='input-group date form_time2' > 
                <input type='text' class='form-control' name='end_hora' id='end_hora' required />                                   
                <span class='input-group-addon'>
                    <span class='glyphicon glyphicon-time'></span>
                </span>                                         
            </div> 
        </div>
        
        <?php if ($dados[0]['idSerie'] != '') : ?>
        <div class='form-group col-md-12'>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label>Gostaria de fazer a alteração em todas as séries deste agendamento?</label>&nbsp;&nbsp;&nbsp;&nbsp;
               <input required type='radio' name='Serie' id='Serie' value ='S' />&nbsp;Sim &nbsp;&nbsp;
                <input required type='radio' name='Serie' id='Serie' value ='N' />&nbsp;Não
                <input type='hidden' class='form-control' name='idSerie' id='idSerie' value="<?= $dados[0]['idSerie'] ?>" />
                <input type='hidden' class='form-control' name='inicioSerie' id='inicioSerie' value="<?= $dados[0]['inicioSerie'] ?>" />
        </div>
        <?php endif; ?>
        
        <div class='form-group col-md-12'>
            <label for="obs">Observação</label><br>                                 
            <textarea class='form-control' id="obs" name="obs"><?= $dados[0]['obs'] ?></textarea>
        </div>
        <div class='form-group col-md-1'></div>
        
        <div class='form-group col-md-12'>
            <div class='form-group col-md-12'>
                <label>Repetir Evento</label> &nbsp;&nbsp;                                
                <label for='inicioSerie'><input required="" type='radio' name='inicioSerie' id='inicioSerie1' value ='1' <?php if ($dados[0]['idSerie'] != '') : ?> checked <?php endif; ?>>&nbsp;Sim</label>  &nbsp;&nbsp;
                <label for='inicioSerie'><input required="" type='radio' name='inicioSerie' id='inicioSerie2' value ='2' <?php if ($dados[0]['idSerie'] == '') : ?> checked <?php endif; ?>>&nbsp;Não</label>  
            </div>
            <div id='semana' class='form-group col-md-12' <?php if ($dados[0]['idSerie'] == '') : ?> style="display: none;" <?php endif; ?>>
                <?php $array = explode(',', $dados[0]['dow']); ?>                
                <input type='checkbox' name='dow[]' id='dow0' value ='0' <?php if (in_array('0', $array) !== false) { echo "checked"; } ?> />
                <label for='dow0'> Dom </label>&nbsp;&nbsp;&nbsp;
                <input type='checkbox' name='dow[]' id='dow1' value ='1' <?php if (in_array('1', $array) !== false) { echo "checked"; } ?>  />
                <label for='dow1'> Seg </label>&nbsp;&nbsp;&nbsp;
                <input type='checkbox' name='dow[]' id='dow2' value ='2' <?php if (in_array('2', $array) !== false) { echo "checked"; } ?>  />
                <label for='dow2'> Ter </label>&nbsp;&nbsp;&nbsp;
                <input type='checkbox' name='dow[]' id='dow3' value ='3' <?php if (in_array('3', $array) !== false) { echo "checked"; } ?>  />
                <label for='dow3'> Qua </label>&nbsp;&nbsp;&nbsp;
                <input type='checkbox' name='dow[]' id='dow4' value ='4' <?php if (in_array('4', $array) !== false) { echo "checked"; } ?>  />
                <label for='dow4'> Qui </label>&nbsp;&nbsp;&nbsp;
                <input type='checkbox' name='dow[]' id='dow5' value ='5' <?php if (in_array('5', $array) !== false) { echo "checked"; } ?>  />
                <label for='dow5'> Sex </label>&nbsp;&nbsp;&nbsp;
                <input type='checkbox' name='dow[]' id='dow6' value ='6' <?php if (in_array('6', $array) !== false) { echo "checked"; } ?>  />
                <label for='dow6'> Sab </label>
                <br>
                <span>Para que a repetição ocorra selecione o dia da semana e defina a <strong>Data Término</strong> na ultima ocorrência do evento</span>
            </div>    
        </div>
        
        <div class='form-group col-md-12'>
            <input type='text' class='form-control required' name='responsavel' id='responsavel' placeholder="Informe o nome do responsavel pelo Evento" value="<?= $dados[0]['responsavel'] ?>" required />                                                                   
        </div>
        
        <?php if ($dados[0]['nmNome'] != '') { ?>
            <div class='form-group col-md-12'>                                     
                <div id='owner'><strong>Criado por:</strong> <?= $dados[0]['nmNome'] ?></div>
            </div>
        <?php } ?>        
    </form>                             
</div>
<script>
        $('#inicioSerie1').on("click", function (){
            $('#semana').show();
        }); 
        $('#inicioSerie2').on("click", function (){
           $('#semana').hide();
        }); 
</script>
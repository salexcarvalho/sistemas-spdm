<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - CERTIFICADOS</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"> Editar Origem de Eventos</h1>
                        <ol class="breadcrumb"> 
                            <li><i class="fa fa-file-pdf-o"></i> Certificados</li>
                            <li><a href="home"><i class="fa fa-list"></i> Lista de origem de eventos</a></li>
                            <li class="active"><i class="glyphicon glyphicon-file"></i> Edição de dados do tipo de participante</li>
                        </ol>
                        <form class="form-horizontal" role="form" method="POST" action="update_origem">
                            <div class="form-group">
                                <div class="col-lg-6">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <span>Dados da Origem de Eventos</span>
                                        </div>
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <label for="nmOrigem" class="col-lg-3 control-label">Nome da Origem</label>
                                                <div class="col-lg-9">
                                                    <input hidden id="page" name="page" value="<?= $page; ?>">
                                                    <input hidden id="idOrigem" name="idOrigem" placeholder="" value="<?= $tpOrigem[0]['idOrigem']; ?>">  
                                                    <input type="text" min="0" class="form-control" id="nmOrigem" name="nmOrigem" placeholder="" value="<?= $tpOrigem[0]['Nome']; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="alias" class="col-lg-3 control-label">Alias</label>
                                                <div class="col-lg-9">
                                                    <input type="text" class="form-control" id="aliasOrigem" name="aliasOrigem" placeholder="" value="<?= $tpOrigem[0]['Alias']; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-offset-3 col-lg-9">
                                                    <div class="form-group"> 
                                                        <label class="radio-inline">&nbsp;&nbsp;&nbsp;
                                                            <input required type="radio" name="status" id="ativo" value="1" <?= ($tpOrigem[0]['Status'] == '1') ? 'checked' : ''; ?>>
                                                            Ativo
                                                        </label>
                                                        <label class="radio-inline">
                                                            <input required type="radio" name="status" id="inativo" value="0" <?= ($tpOrigem[0]['Status'] == '0') ? 'checked' : ''; ?>>
                                                            Inativo
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>    
                                    <div class="form-group">
                                        <div class="col-lg-5">
                                            <button type="submit" class="btn btn-primary btn-sm">Alterar</button>
                                            <a href="home?page=<?= $page; ?>" class="btn btn-danger">Voltar</a>
                                        </div>
                                    </div>
                                </div>
                            </div>  
                        </form>    
                    </div>
                </div>
            </div>
        </div>
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>
        <script>
            jQuery(document).ready(function () {
                // Remove seleção de ativo no menu.
                $('.nav li').removeClass('active');
                $('#side-menu li').removeClass('active');
                // Ativa botão no menu.
                $('#li-certificados ul').addClass('collapse in');
                $('#li-cad-tiposorigem a').addClass('active');
                $('#li-certificados').addClass('active');
                $('#li-certificados a').addClass('collapse in');
            });
        </script>        
    </body>
</html>
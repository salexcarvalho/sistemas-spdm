
<?php
    $idAssinaturas = (isset($dados[0]['idAssinaturas']))? $dados[0]['idAssinaturas']:"";
    $titulacao = (isset($dados[0]['titulacao']))? $dados[0]['titulacao']:"";
    $nome = (isset($dados[0]['nome']))? $dados[0]['nome']:"";
    $cargo = (isset($dados[0]['cargo']))? $dados[0]['cargo']:"";
    $PngAssinatura = (isset($dados[0]['PngAssinatura']))? $dados[0]['PngAssinatura']:"";
    $status = (isset($dados[0]['status']))? $dados[0]['status']:"";
    if ($idAssinaturas==""):    
    ?>
    <form class="form-horizontal" role="form" id="formAssinatura" name="formAssinatura" method="POST" action="insert_assinatura" enctype="multipart/form-data">           
    <?php else :?>
        <form class="form-horizontal" role="form" id="formAssinatura" name="formAssinatura" method="POST" action="update_assinatura" enctype="multipart/form-data">        
            <input hidden id="idAssinaturas" name="idAssinaturas" value="<?=$idAssinaturas?>">    
    <?php endif; ?> 
        <input hidden id="page" name="page" value="<?= $page; ?>">
        <div class='form-group'>
            <div class='col-md-12'>
                <div class="form-group">
                    <label for="titulacao" class="col-md-3 control-label">Titulação</label>
                    <div class="col-md-9">
                        <input type="text" min="0" class="form-control" id="titulacao" name="titulacao" placeholder="Informe a titulação, se houver" value="<?= $titulacao ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="nmAssinatura" class="col-md-3 control-label">Nome</label>
                    <div class="col-md-9">
                        <input type="text" min="0" class="form-control" id="nome" name="nome" placeholder="Informe o nome completo" value="<?= $nome; ?>" required>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">cargo</label>
                    <div class="col-md-9">
                        <input type="text" min="0" class="form-control" id="cargo" name="cargo" placeholder="Informe o cargo" value="<?= $cargo; ?>" required>
                    </div>
                </div>  
                <div class="form-group">
                    <label for="PngAssinatura" class="col-md-3 control-label">PNG da Assinatura</label>
                    <div class="col-md-9">
                        <input hidden id="img_PngAssinatura" name="img_PngAssinatura" value="<?= $idAssinaturas?>">
                        <input type="file" class="form-control" id="PngAssinatura" name="PngAssinatura" >
                    </div>
                   
                </div> 
                <div <?php if($PngAssinatura==""){ echo 'hidden';}?>>
                        <img src="<?= URL::base(); ?>upload/img/certificados/<?= $PngAssinatura; ?>" width="280" height="150" alt="" class="thumbnail"/>
                </div>  
                <div id="msg" class="alert alert-danger alert-dismissible" role="alert" hidden>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <strong>Arquivo de assinatura deve ter Altura = 105 e Largura = 280</strong>
                </div>
                <div class="col-md-offset-4 col-md-8">                   
                    <label class="radio-inline">
                        <input required type="radio" name="status" id="ativo" value="1" <?php if($status==1){ echo 'checked=checked';}?>>
                        Ativo
                    </label>
                    <label class="radio-inline">
                        <input required type="radio" name="status" id="inativo" value="0" <?php if($status==0){ echo 'checked=checked';}?>>
                        Inativo
                    </label>              
                </div>
            </div>
        </div>

    </form>                     

    <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>
    <script>
        jQuery("#PngAssinatura").change(function () { //campo de imagem

            var fr = new FileReader;
            fr.onload = function () {
                var img = new Image;
                img.onload = function () {
                    if ((img.width > 280 && this.height > 105) || (img.width < 280 && this.height < 105)) {
                        jQuery('#msg').show();
                        jQuery("#salvar").attr("disabled", true); //Desabilita o botão sumbit
                    } else {
                        jQuery("#salvar").removeAttr("disabled"); // Abilita o botão submit
                        jQuery("#msg").hide();
                    }
                };
                img.src = fr.result;
            };
            fr.readAsDataURL(this.files[0]);
        });
    </script>
    <script>
        jQuery(document).ready(function () {
            // Remove seleção de ativo no menu.
            $('.nav li').removeClass('active');
            $('#side-menu li').removeClass('active');
            // Ativa botão no menu.
            $('#li-certificados ul').addClass('collapse in');
            $('#li-assinatura a').addClass('active');
            $('#li-certificados').addClass('active');
            $('#li-certificados a').addClass('collapse in');
        });
    </script>
</body>
</html>
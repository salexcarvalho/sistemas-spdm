<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - CERTIFICADOS</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>

        <!-- Modal -->
        <div class="modal fade" id="modal_excluir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Excluir Modelos</h4>
                    </div>
                    <div class="modal-body" id="modal_delete_modelo">Deseja excluir este evento?</div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        <button type="button" class="btn btn-primary" id="modal_btn-excluir">Excluir</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal -->

        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Modelos de Certificados</h1>
                        <ol class="breadcrumb">
                            <li><i class="fa fa-file-pdf-o"></i> Certificados</li>
                            <li class="active"><i class="fa fa-list"></i> Listagem de Modelos de Certificados</li>
                        </ol>

                        <?php if (isset($warning)) : ?>
                            <div id="msg" class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong><?= $warning; ?></strong>
                            </div>
                        <?php endif ?>                          
                        <div class="row">
                            <?php if ($_SESSION['AcLiberaBtnAddEve'] == true) { ?>
                                <a href="cadastro_modelo" class="col-lg-12">
                                    <button type="submit" id="btn_novo_modelo" class="pull-right btn btn-primary btn-sm" name="btn_novo_modelo"><i class="glyphicon glyphicon-plus-sign"></i> Adicionar Novo</button>
                                </a>
                            <?php } ?>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="modelos">
                                <thead>
                                    <tr>
                                        <th width="5%" style="text-align: center">#</th>
                                        <th>Evento</th>
                                        <th width="8%" style="text-align: center">Data</th>
                                        <th width="8%" style="text-align: center">Hora</th>
                                        <th width="8%" style="text-align: center">Carga Horária</th>
                                        <th width="9%" style="text-align: center">Ações</th>
                                        <th width="6%" style="text-align: center">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($data as $modelo) {
                                        ?>
                                        <tr>
                                            <td align="center"><?= $modelo['idCertificados']; ?></td>
                                            <td><?= $modelo['eventoNome']; ?></td>
                                            <td align="center"><?= strftime('%d/%m/%Y', strtotime($modelo['dataEmissao'])); ?></td>
                                            <td align="center"><?= $modelo['horaEmissao']; ?></td>
                                            <td align="center"><?= $modelo['cargaHoraria']; ?></td>
                                            <td align="center">
                                                <form style="display: inline-block;" role="form" method="GET" action="visualizar_modelo">
                                                    <input hidden id="page" name="page" value="<?= $PaginaAtual; ?>">
                                                    <input type="hidden" id="idCertificados_<?= $modelo['idCertificados']; ?>" name="idCertificados" value="<?= $modelo['idCertificados']; ?>">
                                                    <button type="submit" id="btnVisualizar" class="btn btn-default btn-xs" name="" title="Visualizar Modelos" <?php if ($_SESSION['AcLiberaBtnVizEve'] == NULL) { ?>disabled<?php } ?>>
                                                        <i class="glyphicon glyphicon-file"></i>
                                                    </button>
                                                </form>
                                                <form style="display: inline-block;" role="form" method="POST" action="editar_modelo">
                                                    <input hidden id="page" name="page" value="<?= $PaginaAtual; ?>">
                                                    <input type="hidden" id="idCertificados_<?= $modelo['idCertificados']; ?>" name="idCertificados" value="<?= $modelo['idCertificados']; ?>">
                                                    <button  type="submit" id="btnEditar" class="btn btn-default btn-xs" name="btnEditar" idCertificados="<?= $modelo['idCertificados']; ?>" title="Editar Modelos" <?php if ($_SESSION['AcLiberaBtnEdiEve'] == NULL) { ?>disabled<?php } ?>>
                                                        <i class="glyphicon glyphicon-edit"></i>
                                                    </button>
                                                </form>

                                                <input type="hidden" id="idCertificados_<?= $modelo['idCertificados']; ?>" name="idCertificados" value="<?= $modelo['idCertificados']; ?>">
                                                <button  type="button" id="btnExcluir" class="btn btn-default btn-xs" name="btnExcluir" idCertificados="<?= $modelo['idCertificados']; ?>" eventoNome="<?= $modelo['eventoNome']; ?>" <?php if ($_SESSION['AcLiberaBtnExcEve'] == NULL) { ?>disabled<?php } ?> data-toggle="modal" data-target="#modal_excluir" data-placement="bottom" title="Excluir Modelos" data-delay="1">
                                                    <i class="glyphicon glyphicon-trash"></i>
                                                </button>
                                            </td>
                                            <td align="center">

                                                <input type="hidden" name="idCertificados" value="<?= $modelo['idCertificados']; ?>">
                                                <?php
                                                if ($modelo['status'] == "0") {
                                                    ?>
                                                    <button type="submit" id="btnAtivar_<?= $modelo['idCertificados']; ?>" class="btn btn-danger btn-xs" idCertificados="<?= $modelo['idCertificados']; ?>" name="btnAtivar" title="Ativar Modelos" <?php if ($_SESSION['AcLiberaBtnAtivEve'] == NULL) { ?>disabled<?php } ?>>
                                                        <i id="activ_<?= $modelo['idCertificados']; ?>" class="glyphicon glyphicon-ban-circle"></i>
                                                    </button>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <button type="submit" id="btnAtivar_<?= $modelo['idCertificados']; ?>" class="btn btn-success btn-xs" idCertificados="<?= $modelo['idCertificados']; ?>" name="btnAtivar" title="Destivar Modelos" <?php if ($_SESSION['AcLiberaBtnAtivEve'] == NULL) { ?>disabled<?php } ?>>
                                                        <i id="activ_<?= $modelo['idCertificados']; ?>" class="glyphicon glyphicon-ok"></i>
                                                    </button>
                                                    <?php
                                                }
                                                ?>

                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                            <?php include(kohana::find_file('views/paginator', 'paginacao', 'php')) ?> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>

        <script>
            jQuery(document).ready(function () {
                // Remove seleção de ativo no menu.
                $('.nav li').removeClass('active');
                $('#side-menu li').removeClass('active');
                // Ativa botão no menu.
                $('#li-certificados ul').addClass('collapse in');
                $('#li-cad-eventos a').addClass('active');
                $('#li-certificados').addClass('active');
                $('#li-certificados a').addClass('collapse in');
            });
        </script>

        <script>
            jQuery(document).ready(function ()
            {
                $('#modelos').dataTable(
                        {
                            "aoColumnDefs": [{"bSortable": false, "aTargets": [5, 6]}],
                            "paging": false,
                            "info": false,
                            "aaSorting": [[0, 'desc']],
                            "bFilter": false,
                            "oLanguage":
                                    {
                                        "sSearch": " Buscar: "
                                    }
                        });
                var idCertificados;
                var eventoNome;
                $('button[name=btnExcluir]').on("click", function ()
                {
                    idCertificados = $(this).attr('idCertificados');
                    eventoNome = $(this).attr('eventoNome');
                    $('#modal_delete_modelo').html("<font color='red'><strong>Atenção - ao excluir esse modelo todos os certificados relacionados a ele serão excluidos também!!<strong></font><br >Deseja excluir o Modelos <b>\"" + eventoNome + "\"</b> ?");
                });

                $('#modal_excluir').on('hidden.bs.modal', function ()
                {
                    window.location.replace("modelos");
                });

                $('#modal_btn-excluir').on("click", function ()
                {
                    var btn = $(this);
                    btn.text('loading');

                    btn.removeClass('btn-primary');
                    btn.removeClass('btn-danger');
                    btn.removeClass('btn-success');
                    btn.addClass('btn-primary');

                    $('#modal_btn-excluir').prop('disabled', true);

                    $.ajax("delete_modelo",
                            {
                                type: "POST",
                                data:
                                        {
                                            idCertificados: idCertificados
                                        }
                            })
                            .done(function (data)
                            {
                                $('#modal_delete_modelo').html('Modelos excluído com sucesso');

                                btn.removeClass('btn-primary');
                                btn.addClass('btn-success');
                            })
                            .fail(function ()
                            {
                                $('#modal_delete_modelo').html('Erro: ' + data);

                                btn.removeClass('btn-primary');
                                btn.addClass('btn-danger');
                            })
                            .always(function ()
                            {
                                btn.text('Excluído');
                            });
                });

                $("body").on("click", "button[name=btnAtivar]", function () {
                    idCertificados = $(this).attr('idCertificados');
                    //alert(txtUsuario);
                    $.get('update_ativar_modelo?idCertificados=' + idCertificados, function (data) {

                        if (data === "1") {
                            $('#btnAtivar_' + idCertificados).addClass('btn btn-success btn-xs');
                            $('#btnAtivar_' + idCertificados).removeClass('btn-danger');
                            $('#activ_' + idCertificados).removeClass('glyphicon-ban-circle');
                            $('#activ_' + idCertificados).addClass('glyphicon-ok');
                        } else {
                            $('#btnAtivar_' + idCertificados).addClass('btn btn-danger btn-xs');
                            $('#btnAtivar_' + idCertificados).removeClass('btn-success');
                            $('#activ_' + idCertificados).addClass('glyphicon-ok');
                            $('#activ_' + idCertificados).addClass('glyphicon-ban-circle');
                        }
                    });
                });
                $('.dataTables_filter label').addClass('pull-right');
            });
        </script>
    </body>
</html>
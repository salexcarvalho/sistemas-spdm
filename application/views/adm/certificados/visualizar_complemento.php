<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - CERTIFICADOS</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <?php if ($_SESSION['AcLiberaBtnEdiPte'] == true) { ?>
                            <h1 class="page-header"> Editar Participante</h1>
                            <ol class="breadcrumb"> 
                                <li><i class="fa fa-file-pdf-o"></i> Certificados</li>
                                <li><a href="home"><i class="fa fa-list"></i> Lista de participantes</a></li>
                                <li class="active"><i class="glyphicon glyphicon-file"></i> Edição de complementos</li>
                            </ol>
                            <form class="form-horizontal" role="form" method="POST" action="insert_complemento">
                                <div class="form-group">
                                    <div class="col-lg-7">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <span>Dados do Complemento</span>
                                            </div>
                                            <div class="panel-body">
                                                <?php
                                                $model_participantes = new Model_Sec_Participantes('default');
                                                $participante = $model_participantes->select_participante($dados[0]['idParticipante']);
                                                $model_origem = new Model_Sec_Origem('default');
                                                $origem = $model_origem->select_origem($dados[0]['idOrigem']);
                                                $model_tipos = new Model_Sec_Tipos('default');
                                                $tipos = $model_tipos->select_tipo($dados[0]['idTipo']);
                                                ?>                                            
                                                <div class="form-group">
                                                    <label for="nmParticipante" class="col-md-2 control-label">Titulação</label>
                                                    <div class="col-md-1">                                                                                                 
                                                        <input type="text" min="0" class="form-control" id="titulacao" disabled name="" placeholder="" value="<?= $participante[0]['titulacao']; ?>">                                                    
                                                    </div>

                                                    <label for="nmParticipante" class="col-md-1 control-label">Nome</label>
                                                    <div class="col-md-5">
                                                        <input type="text" min="0" class="form-control" id="" disabled name="" placeholder="" value="<?= $participante[0]['Nome']; ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">Origem</label>
                                                    <div class="col-md-3">                                                 
                                                        <input type="text" min="0" class="form-control" id="" disabled name="" placeholder="" value="<?= $origem[0]['Nome']; ?>">
                                                    </div>

                                                    <label class="col-md-1 control-label">Tipo</label>
                                                    <div class="col-md-3">
                                                        <input type="text" min="0" class="form-control" id="" disabled name="" placeholder="" value="<?= $tipos[0]['Nome']; ?>">
                                                    </div>
                                                </div>                                            
                                                <div class="form-group">
                                                    <label for="modulo" class="col-md-2 control-label">Módulo</label>
                                                    <div class="col-md-7">
                                                        <input type="text" class="form-control" id="modulo" disabled name="modulo" placeholder="" value="<?= $dados[0]['modulo']; ?>">
                                                    </div>
                                                </div>                                        
                                                <div class="form-group">
                                                    <label for="tema" class="col-md-2 control-label">Tema</label>
                                                    <div class="col-md-7">
                                                        <input type="text" class="form-control" id="tema" disabled name="tema" placeholder="" value="<?= $dados[0]['tema']; ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="data" class="col-md-2 control-label">Data</label>
                                                    <div class="col-md-7">
                                                        <input type="text" class="form-control" id="data" disabled name="data" placeholder="" data-mask='99/99/9999' value="<?= $dados[0]['data']; ?>">
                                                    </div>
                                                </div>  
                                                <div class="form-group">
                                                    <label for="livre" class="col-md-2 control-label">Data Livre</label>
                                                    <div class="col-md-7">
                                                        <input type="text" class="form-control" id="livre" disabled name="livre" placeholder="Informe datas e ou complementos necessários separadas por ','" value="<?= $dados[0]['livre']; ?>">
                                                    </div>
                                                </div>                                         
                                            </div>
                                        </div>    
                                        <div class="form-group">
                                            <div class="col-lg-5">
                                                <a href="home?page=<?= $page; ?>" class="btn btn-danger">Voltar</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        <?php } else { ?>
                            <h1 class="page-header"> Voce não possue acesso a está pagina, consulte o administrador.</h1>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>
        <script>
            jQuery(document).ready(function () {
                // Remove seleção de ativo no menu.
                $('.nav li').removeClass('active');
                $('#side-menu li').removeClass('active');
                // Ativa botão no menu.
                $('#li-certificados ul').addClass('collapse in');
                $('#li-cad-participantes a').addClass('active');
                $('#li-certificados').addClass('active');
                $('#li-certificados a').addClass('collapse in');
            });
        </script>        
    </body>
</html>
<?php
$model_tipoDoc = new Model_Sec_TipoDoc('default');
$tipoDoc = $model_tipoDoc->select_tipoDocs();
?>
<form class="form-horizontal" role="form" method="POST" id="FormPartAdd" name="FormPartAdd" action="insert_participante">
    <div class="form-group">    
        <div class="col-lg-12">
            <div class="form-group">
                <label for="titulacao" class="col-md-4 control-label">Titulação</label>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="titulacao" name="titulacao" placeholder="Informe a Titulação quando aplicavel">
                </div>
            </div>
            <div class="form-group">
                <label for="nmParticipante" class="col-md-4 control-label">Nome</label>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="nmParticipante" name="nmParticipante" placeholder="Informe o nome do participante" required>
                </div>
            </div>
            <div class="form-group">
                <label for="nasc" class="col-md-4 control-label">Data de Nascimento</label>
                <div class="col-md-4">
                    <input type="data" class="form-control" id="nasc" name="nasc" placeholder="Informe a Data de Nascimeto, caso possua.">
                </div>
            </div>
            <div class="form-group">
                <label for="nacionalidade" class="col-md-4 control-label">Nacionalidade</label>
                <div class="col-md-4">
                    <input type="nacionalidade" class="form-control" id="nacionalidade" name="nacionalidade" placeholder="Informe a nacionalidade, caso possua.">
                </div>
            </div>
            <div class="form-group">
                <label for="documento" class="col-md-4 control-label">Documento</label>
                <div class="col-md-4">
                    <input type="documento" class="form-control" id="docRG" name="documento" placeholder="Informe o Documento, caso possua.">
                </div>
            </div>
            <div class="form-group">
                <label for="tipoDoc" class="col-md-4 control-label">Tipo do Documento</label>
                <div class="col-md-4">
                    <select  id="tipoDoc" name="idtipoDoc" class="form-control" required>
                        <option value="">Selecione a Tipo do Documento</option>
                        <?php foreach ($tipoDoc as $docs) {?>
                            <option value="<?= $docs['idTipoDoc']; ?>"><?= $docs['Nome']; ?></option>    
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="email" class="col-md-4 control-label">Email</label>
                <div class="col-md-4">
                    <input type="email" class="form-control" id="email" name="email" placeholder="Informe o email.">
                    <span id='ret'></span>
                </div>
            </div>

        </div>                                       
    </div>
</div>
</form>
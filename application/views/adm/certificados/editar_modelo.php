<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - CERTIFICADOS</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <script>
            jQuery(document).ready(function () {
                // Remove seleção de ativo no menu.
                $('.nav li').removeClass('active');
                $('#side-menu li').removeClass('active');
                // Ativa botão no menu.
                $('#li-certificados ul').addClass('collapse in');
                $('#li-cad-eventos a').addClass('active');
                $('#li-certificados').addClass('active');
                $('#li-certificados a').addClass('collapse in');
            });
        </script>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

    </head>
    <body>
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>

            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>

            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <?php if ($_SESSION['AcLiberaBtnEdiEve'] == true) { ?>
                            <h1 class="page-header"> Editar Certificado</h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-file-pdf-o"></i> Certificados</li>
                                <li><a href="modelos"><i class="fa fa-list"></i> Listagem de Certificados</a></li>
                                <li class="active"><i class="glyphicon glyphicon-file"></i> Visualização do Certificado</li>
                            </ol>
                            <form class="form-horizontal" role="form" method="POST" action="update_modelo" enctype="multipart/form-data">
                                <div class="form-group">
                                    <div class="col-lg-9">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">

                                                <span>Dados do Evento</span>

                                            </div>
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <label for="nmEvento" class="col-lg-2 control-label">Nome do Evento</label>
                                                    <div class="col-lg-10">
                                                        <input hidden id="page" name="page" value="<?= $page; ?>">
                                                        <input hidden id="idCertificados" name="idCertificados" placeholder="" value="<?= $modelo[0]['idCertificados']; ?>"> 
                                                        <input type="text" min="0" class="form-control" id="nmEvento" name="nmEvento" placeholder="" value="<?= $modelo[0]['eventoNome']; ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="Local" class="col-lg-2 control-label">Local</label>
                                                    <div class="col-lg-10">
                                                        <input type="Local" class="form-control" id="Local" name="Local" placeholder="" value="<?= $modelo[0]['localEvento']; ?>">
                                                    </div>
                                                </div>                                        
                                                <div class="form-group">
                                                    <label for="dtEvento" class="col-lg-2 control-label">Data de emissão</label>
                                                    <div class="col-lg-2">
                                                        <input type="date" class="form-control" id="dtEvento" name="dtEvento" placeholder="" value="<?= $modelo[0]['dataEmissao']; ?>">
                                                    </div>
                                                    <label for="Hora" class="col-lg-1 control-label">Hora</label>
                                                    <div class="col-lg-2">
                                                        <input type="time" class="form-control" id="Hora" name="Hora" placeholder="" value="<?= $modelo[0]['horaEmissao']; ?>">
                                                    </div>
                                                    <label for="c_Horaria" class="col-lg-2 control-label">Carga Horaria</label>
                                                    <div class="col-lg-3">
                                                        <input type="text" class="form-control" id="c_Horaria" name="c_Horaria" placeholder="" value="<?= $modelo[0]['cargaHoraria']; ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="msg_Cert" class="col-lg-2 control-label">Texto do Certificado</label>
                                                    <div class="col-lg-10">
                                                        <textarea style="resize: vertical;" class="form-control" rows="3" name="msg_Cert" id="msg_Cert" placeholder=""><?= $modelo[0]['TextoPadrao']; ?></textarea>
                                                        <script>
                                                            CKEDITOR.replace('msg_Cert', {height: 75});
                                                        </script>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="idOrigem" class="col-lg-2 control-label">Origem</label>
                                                    <div class="col-lg-10">
                                                        <select id="idOrigem" name="idOrigem" class="form-control" required>
                                                            <option value="">Selecione a Origem do Evento</option>
                                                            <?php foreach ($origens as $origem) {
                                                                ?>
                                                                <option <?= ($modelo[0]['idOrigem'] == $origem['idOrigem']) ? 'selected' : ''; ?> value="<?= $origem['idOrigem']; ?>"><?= $origem['Nome']; ?></option> 
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="idAssinaturas1" class="col-lg-2 control-label">Assinatura Principal</label>
                                                    <div class="col-lg-4">
                                                        <select  id="idAssinaturas1" name="idAssinaturas1" class="form-control" required>
                                                            <option value="NULL">Selecione a Assinatura Principal</option>
                                                            <?php foreach ($assinaturas as $assinatura1) {
                                                                ?>
                                                                <option <?= ($assinatura1['idAssinaturas'] == $modelo[0]['fk_idAssinatura_1']) ? 'selected' : ''; ?>  value="<?= $assinatura1['idAssinaturas']; ?>"><?= $assinatura1['nome']; ?></option>    
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                    <label for="idAssinaturas2" class="col-lg-2 control-label">Assinatura Secundária</label>
                                                    <div class="col-lg-4">
                                                        <select  id="idAssinaturas2" name="idAssinaturas2" class="form-control">
                                                            <option value="">Selecione a Assinatura Secundária</option>
                                                            <?php foreach ($assinaturas as $assinatura2) {
                                                                ?>
                                                                <option <?= ($assinatura2['idAssinaturas'] == $modelo[0]['fk_idAssinatura_2']) ? 'selected' : ''; ?>  value="<?= $assinatura2['idAssinaturas']; ?>"><?= $assinatura2['nome']; ?></option>     
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="idAssinaturas3" class="col-lg-2 control-label">Assinatura Opcional 01</label>
                                                    <div class="col-lg-4">
                                                        <select  id="idAssinaturas3" name="idAssinaturas3" class="form-control">
                                                            <option value="">Selecione a Assinatura Opcional 01</option>
                                                            <?php foreach ($assinaturas as $assinatura3) {
                                                                ?>
                                                                <option <?= ($assinatura3['idAssinaturas'] == $modelo[0]['fk_idAssinatura_3']) ? 'selected' : ''; ?>  value="<?= $assinatura3['idAssinaturas']; ?>"><?= $assinatura3['nome']; ?></option>    
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                    <label for="idAssinaturas4" class="col-lg-2 control-label">Assinatura Opcional 02</label>
                                                    <div class="col-lg-4">
                                                        <select  id="idAssinaturas4" name="idAssinaturas4" class="form-control">
                                                            <option value="">Selecione a Assinatura Opcional 02</option>
                                                            <?php foreach ($assinaturas as $assinatura4) {
                                                                ?>
                                                                <option <?= ($assinatura4['idAssinaturas'] == $modelo[0]['fk_idAssinatura_4']) ? 'selected' : ''; ?>  value="<?= $assinatura4['idAssinaturas']; ?>"><?= $assinatura4['nome']; ?></option>     
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="img_Cert" class="col-lg-2 control-label">Imagem do Certificado</label>
                                                    <div class="col-lg-6">
                                                        <input hidden id="img_Cert_hidenn" name="img_Cert_hidenn" placeholder="" value="<?= $modelo[0]['imgCertificado']; ?>">
                                                        <input type="file" class="form-control" id="img_Cert" name="img_Cert">
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <img src="<?= URL::base(); ?>upload/img/certificados/<?= $modelo[0]["imgCertificado"]; ?>" width="300" alt="" class="thumbnail"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-lg-5">
                                                <button type="submit" class="btn btn-primary">Alterar</button>
                                                <a href="modelos?page=<?= $page; ?>" class="btn btn-danger">Voltar</a>
                                            </div>
                                        </div>    
                                    </div>
                                </div>
                            </form>
                        <?php } else { ?>
                            <h1 class="page-header"> Voce não possue acesso a está pagina, consulte o administrador.</h1>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>
        <script>
            jQuery(document).ready(function () {
                var op1, op2, op3, op4;
                $('#idAssinaturas1').change(function () {
                    var idOption = $('#idAssinaturas1').val();
                    if (idOption === '') {
                        if (op1 != '') {
                            $("#idAssinaturas2 option[value=" + op1 + "]").removeAttr('disabled');
                            $("#idAssinaturas3 option[value=" + op1 + "]").removeAttr('disabled');
                            $("#idAssinaturas4 option[value=" + op1 + "]").removeAttr('disabled');
                        }
                    } else {
                        if (op1 != '') {
                            $("#idAssinaturas2 option[value=" + op1 + "]").removeAttr('disabled');
                            $("#idAssinaturas3 option[value=" + op1 + "]").removeAttr('disabled');
                            $("#idAssinaturas4 option[value=" + op1 + "]").removeAttr('disabled');
                        }
                        $("#idAssinaturas2 option[value=" + idOption + "]").attr('disabled', 'disabled');
                        $("#idAssinaturas3 option[value=" + idOption + "]").attr('disabled', 'disabled');
                        $("#idAssinaturas4 option[value=" + idOption + "]").attr('disabled', 'disabled');


                    }
                    op1 = idOption;
                });
                $('#idAssinaturas2').change(function () {
                    var idOption = $('#idAssinaturas2').val();
                    if (idOption === '') {
                        if (op2 != '') {
                            $("#idAssinaturas1 option[value=" + op2 + "]").removeAttr('disabled');
                            $("#idAssinaturas3 option[value=" + op2 + "]").removeAttr('disabled');
                            $("#idAssinaturas4 option[value=" + op2 + "]").removeAttr('disabled');
                        }
                    } else {
                        if (op2 != '') {
                            $("#idAssinaturas1 option[value=" + op2 + "]").removeAttr('disabled');
                            $("#idAssinaturas3 option[value=" + op2 + "]").removeAttr('disabled');
                            $("#idAssinaturas4 option[value=" + op2 + "]").removeAttr('disabled');
                        }
                        $("#idAssinaturas1 option[value=" + idOption + "]").attr('disabled', 'disabled');
                        $("#idAssinaturas3 option[value=" + idOption + "]").attr('disabled', 'disabled');
                        $("#idAssinaturas4 option[value=" + idOption + "]").attr('disabled', 'disabled');
                    }
                    op2 = idOption;
                });
                $('#idAssinaturas3').change(function () {
                    var idOption = $('#idAssinaturas3').val();
                    if (idOption === '') {
                        if (op3 != '') {
                            $("#idAssinaturas2 option[value=" + op3 + "]").removeAttr('disabled');
                            $("#idAssinaturas1 option[value=" + op3 + "]").removeAttr('disabled');
                            $("#idAssinaturas4 option[value=" + op3 + "]").removeAttr('disabled');
                        }
                    } else {
                        if (op3 != '') {
                            $("#idAssinaturas2 option[value=" + op3 + "]").removeAttr('disabled');
                            $("#idAssinaturas1 option[value=" + op3 + "]").removeAttr('disabled');
                            $("#idAssinaturas4 option[value=" + op3 + "]").removeAttr('disabled');
                        }
                        $("#idAssinaturas2 option[value=" + idOption + "]").attr('disabled', 'disabled');
                        $("#idAssinaturas1 option[value=" + idOption + "]").attr('disabled', 'disabled');
                        $("#idAssinaturas4 option[value=" + idOption + "]").attr('disabled', 'disabled');
                    }
                    op3 = idOption;
                });
                $('#idAssinaturas4').change(function () {
                    var idOption = $('#idAssinaturas4').val();
                    if (idOption === '') {
                        if (op4 != '') {
                            $("#idAssinaturas2 option[value=" + op4 + "]").removeAttr('disabled');
                            $("#idAssinaturas3 option[value=" + op4 + "]").removeAttr('disabled');
                            $("#idAssinaturas1 option[value=" + op4 + "]").removeAttr('disabled');
                        }
                    } else {
                        if (op4 != '') {
                            $("#idAssinaturas2 option[value=" + op4 + "]").removeAttr('disabled');
                            $("#idAssinaturas3 option[value=" + op4 + "]").removeAttr('disabled');
                            $("#idAssinaturas1 option[value=" + op4 + "]").removeAttr('disabled');
                        }
                        $("#idAssinaturas2 option[value=" + idOption + "]").attr('disabled', 'disabled');
                        $("#idAssinaturas3 option[value=" + idOption + "]").attr('disabled', 'disabled');
                        $("#idAssinaturas1 option[value=" + idOption + "]").attr('disabled', 'disabled');
                    }
                    op4 = idOption;
                });
            });
        </script>
    </body>
</html>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - CERTIFICADOS</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>

        <!-- Modal -->
        <div class="modal fade" id="modal_excluir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Excluir Evento</h4>
                    </div>
                    <div class="modal-body" id="modal_delete_evento">Deseja excluir este evento?</div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        <button type="button" class="btn btn-primary btn-sm" id="modal_btn-excluir">Excluir</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal -->

        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Lista de Certificados</h1>
                        <ol class="breadcrumb">
                            <li><i class="fa fa-file-pdf-o"></i> Certificados</li>
                            <li class="active"><i class="fa fa-list"></i> Listagem de Certificados</li>
                        </ol>
                        
                        <?php if (isset($warning)) : ?>
                            <div id="msg" class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong><?= $warning; ?></strong>
                            </div>
                        <?php endif ?>                          
                        
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <span>Evento</span>
                                <?php if($_SESSION['AcLiberaBtnAddEve'] == true) { ?>
                                <a href="cadastro_evento">
                                    <button type="submit" id="btn_novo_evento" class="pull-right btn btn-primary btn-xs" name="btn_novo_evento"><i class="glyphicon glyphicon-plus-sign"></i>  Adicionar Novo</button>
                                </a>
                                <?php } ?>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover" id="eventos">
                                        <thead>
                                            <tr>
                                                <th width="6%" style="text-align: center">#</th>
                                                <th>Evento</th>
                                                <th>Data</th>
                                                <th>Hora</th>
                                                <th>Local</th>
                                                <th>Carga Horária</th>
                                                <th width="12%" style="text-align: center">Ações</th>
                                                <th width="7%" style="text-align: center">Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            foreach ($data as $evento) {
                                                ?>
                                                <tr>
                                                    <td align="center"><?= $evento['idEventos']; ?></td>
                                                    <td><?= $evento['NomeEvento']; ?></td>
                                                    <td><?= strftime('%d/%m/%Y', strtotime($evento['DataEvento'])); ?></td>
                                                    <td><?= $evento['Hora']; ?></td>
                                                    <td><?= $evento['Local']; ?></td>
                                                    <td><?= $evento['Carga_Horaria']; ?></td>
                                                    <td align="center">
                                                        <form style="display: inline-block;" role="form" method="GET" action="visualizar_evento">
                                                            <input hidden id="page" name="page" value="<?= $PaginaAtual; ?>">
                                                            <input type="hidden" id="idEvento_<?= $evento['idEventos']; ?>" name="idEvento" value="<?= $evento['idEventos']; ?>">
                                                            <button type="submit" id="btnVisualizar" class="btn btn-default btn-xs" name="" title="Visualizar Evento" <?php if($_SESSION['AcLiberaBtnVizEve'] == NULL) { ?>disabled<?php } ?>>
                                                                <i class="glyphicon glyphicon-file"></i>
                                                            </button>
                                                        </form>
                                                        <form style="display: inline-block;" role="form" method="POST" action="editar_evento">
                                                            <input hidden id="page" name="page" value="<?= $PaginaAtual; ?>">
                                                            <input type="hidden" id="idEvento_<?= $evento['idEventos']; ?>" name="idEvento" value="<?= $evento['idEventos']; ?>">
                                                            <button  type="submit" id="btnEditar" class="btn btn-default btn-xs" name="btnEditar" idEvento="<?= $evento['idEventos']; ?>" title="Editar Evento" <?php if($_SESSION['AcLiberaBtnEdiEve'] == NULL) { ?>disabled<?php } ?>>
                                                                <i class="glyphicon glyphicon-edit"></i>
                                                            </button>
                                                        </form>

                                                        <input type="hidden" id="idEvento_<?= $evento['idEventos']; ?>" name="idEvento" value="<?= $evento['idEventos']; ?>">
                                                        <button  type="button" id="btnExcluir" class="btn btn-default btn-xs" name="btnExcluir" idEvento="<?= $evento['idEventos']; ?>" nmEvento="<?= $evento['NomeEvento']; ?>" <?php if($_SESSION['AcLiberaBtnExcEve'] == NULL) { ?>disabled<?php } ?> data-toggle="modal" data-target="#modal_excluir" data-placement="bottom" title="Excluir Evento" data-delay="1">
                                                            <i class="glyphicon glyphicon-trash"></i>
                                                        </button>
                                                    </td>
                                                    <td align="center">
                                                        
                                                            <input type="hidden" name="idEvento" value="<?= $evento['idEventos']; ?>">
                                                            <?php
                                                            if ($evento['Status'] == "0") {
                                                                ?>
                                                                <button type="submit" id="btnAtivar_<?= $evento['idEventos']; ?>" class="btn btn-danger btn-xs" idEvento="<?= $evento['idEventos']; ?>" name="btnAtivar" title="Ativar Evento" <?php if($_SESSION['AcLiberaBtnAtivEve'] == NULL) { ?>disabled<?php } ?>>
                                                                    <i id="activ_<?= $evento['idEventos']; ?>" class="glyphicon glyphicon-ban-circle"></i>
                                                                </button>
        <?php
    } else {
        ?>
                                                                <button type="submit" id="btnAtivar_<?= $evento['idEventos']; ?>" class="btn btn-success btn-xs" idEvento="<?= $evento['idEventos']; ?>" name="btnAtivar" title="Destivar Evento" <?php if($_SESSION['AcLiberaBtnAtivEve'] == NULL) { ?>disabled<?php } ?>>
                                                                    <i id="activ_<?= $evento['idEventos']; ?>" class="glyphicon glyphicon-ok"></i>
                                                                </button>
                                                                <?php
                                                            }
                                                            ?>
                                                        
                                                    </td>
                                                </tr>
                                                            <?php
                                                        }
                                                        ?>
                                        </tbody>
                                    </table>
                                    <?php include(kohana::find_file('views/paginator', 'paginacao', 'php')) ?> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>
        
        <script>
            jQuery(document).ready(function() {
                // Remove seleção de ativo no menu.
                $('.nav li').removeClass('active');
                $('#side-menu li').removeClass('active');
                // Ativa botão no menu.
                $('#li-certificados ul').addClass('collapse in');
                $('#li-cad-eventos a').addClass('active');
                $('#li-certificados').addClass('active');
                $('#li-certificados a').addClass('collapse in');
            });
        </script>

        <script>
            jQuery(document).ready(function()
            {
                $('#eventos').dataTable(
                        {
                            "aoColumnDefs": [{"bSortable": false, "aTargets": [6, 7]}],
                            "paging":   false,
                            "info":     false,
                            "aaSorting": [[0, 'desc']],
                            "bFilter": false,
                            "oLanguage":
                                    {
                                        "sSearch": " Buscar: "
                                    }
                        });
                var idEvento;
                var nmEvento;
                $('button[name=btnExcluir]').on("click", function()
                {
                    idEvento = $(this).attr('idEvento');
                    nmEvento = $(this).attr('nmEvento');
                    $('#modal_delete_evento').html("Deseja excluir o Evento <b>\"" + nmEvento + "\"</b> ?");
                });

                $('#modal_excluir').on('hidden.bs.modal', function()
                {
                    window.location.replace("home");
                });

                $('#modal_btn-excluir').on("click", function()
                {
                    var btn = $(this);
                    btn.text('loading');

                    btn.removeClass('btn-primary');
                    btn.removeClass('btn-danger');
                    btn.removeClass('btn-success');
                    btn.addClass('btn-primary');

                    $('#modal_btn-excluir').prop('disabled', true);

                    $.ajax("delete_evento",
                            {
                                type: "POST",
                                data:
                                        {
                                            idEvento: idEvento
                                        }
                            })
                            .done(function(data)
                            {
                                $('#modal_delete_evento').html('Evento excluído com sucesso');

                                btn.removeClass('btn-primary');
                                btn.addClass('btn-success');
                            })
                            .fail(function()
                            {
                                $('#modal_delete_evento').html('Erro: ' + data);

                                btn.removeClass('btn-primary');
                                btn.addClass('btn-danger');
                            })
                            .always(function()
                            {
                                btn.text('Excluído');
                            });
                });
                
                $("body").on("click", "button[name=btnAtivar]", function() {
                        idEvento = $(this).attr('idEvento');
                        //alert(txtUsuario);
                            $.get('update_ativar_evento?idEvento=' + idEvento, function(data) {
                                    
                                if (data === "1") {
                                    $('#btnAtivar_' + idEvento).addClass('btn btn-success btn-xs');
                                    $('#btnAtivar_' + idEvento).removeClass('btn-danger');
                                    $('#activ_' + idEvento).removeClass('glyphicon-ban-circle');
                                    $('#activ_' + idEvento).addClass('glyphicon-ok');
                                } else {
                                    $('#btnAtivar_' + idEvento).addClass('btn btn-danger btn-xs');
                                    $('#btnAtivar_' + idEvento).removeClass('btn-success');
                                    $('#activ_' + idEvento).addClass('glyphicon-ok');
                                    $('#activ_' + idEvento).addClass('glyphicon-ban-circle');
                                }
                            });
                    });

                $('#btn_novo_evento').on("click", function()
                {
                    window.location.href = "cadastro_evento";
                });
                /*
                 $("body").on("click", "button[name=btnEditar]", function()
                 {
                 idEvento = $(this).attr('idEvento');
                 window.location.href = "cadastro_evento?editar=1&idEvento=" + idEvento;
                 });*/

                $("body").on("click", "button[name=btnVisualizar]", function()
                {
                    idEvento = $(this).attr('idEvento');
                    window.location.href = "visualizar_evento?idEvento=" + idEvento;
                });

                $('.dataTables_filter label').addClass('pull-right');
            });
        </script>
    </body>
</html>
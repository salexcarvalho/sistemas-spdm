<?php 
$nome = (isset($dados[0]["Nome"]))? $dados[0]["Nome"]:"";
$alias = (isset($dados[0]["Alias"]))? $dados[0]["Alias"]:"";
$status = (isset($dados[0]["Status"]))? $dados[0]["Status"]:"";

if(isset($dados[0]["idTipos"])):
    $id = $dados[0]["idTipos"];
else:
    $id = "";
endif;

if(isset($dados[0]["idOrigem"])):
    $id = $dados[0]["idOrigem"];
else:
    $id = "";
endif;

if($valor == 'Origem'):
    if(isset($dados[0]["idOrigem"])):
?>
       
        <form class="form-horizontal" role="form" id="formOrigem" name="formOrigem" method="POST" action="update_origem">
        <input type="hidden" id="idOrigem" name="idOrigem" value="<?= $id ?>"> 
<?php
    else:
?>
       <form class="form-horizontal" role="form" id="formOrigem" name="formOrigem" method="POST" action="insert_origem">
<?php
    endif;
else:
     if(isset($dados[0]["idTipos"])):
?>
        <input type="hidden" id="idTipos" name="idTipos" value="<?= $id ?>"> 
        <form class="form-horizontal" role="form" id="formTipo" name="formTipo" method="POST" action="update_tipo">
<?php
         else:
?>
       <form class="form-horizontal" role="form" id="formTipo" name="formTipo" method="POST" action="insert_tipo">    
<?php           
     endif;
endif;    
?> 
<input hidden id="page" name="page" value="<?= $page; ?>">
<div class='form-group'>
    <div class='col-md-12'>
        <div class="form-group">
            <label for="nm<?= $valor ?>" class="col-md-3 control-label">Nome <?= $valor ?></label>
            <div class="col-md-9">
                <input type="text" min="0" class="form-control" id="nome" name="nome" placeholder="Informe o nome" value="<?= $nome ?>" required>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Alias</label>
            <div class="col-md-9">
                <input type="text" min="0" class="form-control" id="alias" name="alias" placeholder="Informe o alias" value="<?= $alias ?>" required>
            </div>
        </div>
        <div class="col-md-offset-4 col-md-8">                   
            <label class="radio-inline">
                <input required type="radio" name="status" id="ativo" value="1" <?php if($status == 1){echo 'checked';}?>>
                Ativo
            </label>
            <label class="radio-inline">
                <input required type="radio" name="status" id="inativo" value="0" <?php if($status == 0){echo 'checked';}?>>
                Inativo
            </label>              
        </div>
    </div>
</div>
</form>                     

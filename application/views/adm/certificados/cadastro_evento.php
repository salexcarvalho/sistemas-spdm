<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title>SISTEMAS SPDM - CERTIFICADOS</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <script>
            jQuery(document).ready(function () {
                // Remove seleção de ativo no menu.
                $('.nav li').removeClass('active');
                $('#side-menu li').removeClass('active');
                // Ativa botão no menu.
                $('#li-certificados ul').addClass('collapse in');
                $('#li-cad-eventos a').addClass('active');
                $('#li-certificados').addClass('active');
                $('#li-certificados a').addClass('collapse in');
            });
        </script>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

    </head>
    <body>
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <?php if ($_SESSION['AcLiberaBtnAddEve'] == true) { ?>
                            <h1 class="page-header"> Cadastrar Certificado</h1>
                            <ol class="breadcrumb">                            
                                <li><i class="fa fa-file-pdf-o"></i> Certificados</li>
                                <li><a href="home"><i class="fa fa-list"></i> Listagem de Certificados</a></li>
                                <li class="active"><i class="glyphicon glyphicon-file"></i> Cadastro de Certificado</li>
                            </ol>
                            <form class="form-horizontal" role="form" method="POST" action="insert_evento" enctype="multipart/form-data">
                                <div class="form-group">
                                    <div class="col-lg-9">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <span>Dados do Certificado</span>
                                            </div>
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <label for="nmEvento" class="col-lg-2 control-label">Nome do Evento</label>
                                                    <div class="col-lg-10">
                                                        <input type="text" min="0" required class="form-control" id="nmEvento" name="nmEvento" placeholder="Informe o nome do modelo do certificado">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="Local" class="col-lg-2 control-label">Local</label>
                                                    <div class="col-lg-10">
                                                        <input type="Local" required class="form-control" id="Local" name="Local" placeholder="Informe o local do evento">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="Data" class="col-lg-2 control-label">Data de emissão</label>
                                                    <div class="col-lg-2">
                                                        <input type="date" required class="form-control" id="Data" name="Data" placeholder="Informe a data de emissão do certificado">
                                                    </div>
                                                    <label for="Hora" class="col-lg-1 control-label">Hora</label>
                                                    <div class="col-lg-2">
                                                        <input type="time" required class="form-control" id="Hora" name="Hora" placeholder="Informe a hora o evento">
                                                    </div>
                                                    <label for="c_Horaria" class="col-lg-2 control-label">Carga Horária</label>
                                                    <div class="col-lg-3">
                                                        <input type="c_Horaria" class="form-control" id="c_Horaria" name="c_Horaria" placeholder="Informe a carga horária">
                                                    </div>                                            
                                                </div>
                                                <div class="form-group">
                                                    <label for="msg_Cert" class="col-lg-2 control-label">Texto do Certificado</label>
                                                    <div class="col-lg-10">
                                                        <textarea style="resize: vertical;" class="form-control" rows="3" name="msg_Cert" id="msg_Cert" placeholder="Digite o texto do certificado" required></textarea>
                                                        <script>
                                                            //CKEDITOR.replace('msg_Cert', { height: 75 } );
                                                        </script>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="idOrigem" class="col-lg-2 control-label">Origem</label>
                                                    <div class="col-lg-10">
                                                        <select id="idOrigem" name="idOrigem" class="form-control" required>
                                                            <option value="">Selecione a Origem do Evento</option>
                                                            <?php foreach ($origens as $origem) {
                                                                ?>
                                                                <option value="<?= $origem['idTipoOrigem']; ?>"><?= $origem['Nome_Origem']; ?></option>    
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="idAssinatura1" class="col-lg-2 control-label">Assinatura Principal</label>
                                                    <div class="col-lg-4">
                                                        <select  id="idAssinatura1" name="idAssinatura1" class="form-control" required>
                                                            <option value="">Selecione a Assinatura Principal</option>
                                                            <?php foreach ($assinaturas as $assinatura1) {
                                                                ?>
                                                                <option value="<?= $assinatura1['idAssinatura']; ?>"><?= $assinatura1['NomeAssinatura']; ?></option>    
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                    <label for="idAssinatura2" class="col-lg-2 control-label">Assinatura Secundária</label>
                                                    <div class="col-lg-4">
                                                        <select  id="idAssinatura2" name="idAssinatura2" class="form-control" >
                                                            <option value="">Selecione a Assinatura Secundária</option>
                                                            <?php foreach ($assinaturas as $assinatura2) {
                                                                ?>
                                                                <option value="<?= $assinatura2['idAssinatura']; ?>"><?= $assinatura2['NomeAssinatura']; ?></option>    
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="idAssinatura3" class="col-lg-2 control-label">Assinatura Opcional 01</label>
                                                    <div class="col-lg-4">
                                                        <select  id="idAssinatura3" name="idAssinatura3" class="form-control">
                                                            <option value="">Selecione a Assinatura Opcional 01</option>
                                                            <?php foreach ($assinaturas as $assinatura3) {
                                                                ?>
                                                                <option value="<?= $assinatura3['idAssinatura']; ?>"><?= $assinatura3['NomeAssinatura']; ?></option>    
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                    <label for="idAssinatura4" class="col-lg-2 control-label">Assinatura Opcional 02</label>
                                                    <div class="col-lg-4">
                                                        <select  id="idAssinatura4" name="idAssinatura4" class="form-control" >
                                                            <option value="">Selecione a Assinatura Opcional 02</option>
                                                            <?php foreach ($assinaturas as $assinatura4) {
                                                                ?>
                                                                <option value="<?= $assinatura4['idAssinatura']; ?>"><?= $assinatura4['NomeAssinatura']; ?></option>    
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>                                            
                                                <div class="form-group">
                                                    <label for="img_Cert" class="col-lg-2 control-label">Imagem do Certificado</label>
                                                    <div class="col-lg-10">
                                                        <input type="file" class="form-control" id="img_Cert" name="img_Cert" required>
                                                    </div>
                                                </div>                                        
                                            </div>

                                        </div>
                                        <div class="form-group">
                                            <div class="col-lg-5">
                                                <button type="submit" class="btn btn-primary btn-sm">Cadastrar</button>
                                                <a href="home">
                                                    <button type="button" class="btn btn-danger">Voltar</button>
                                                    <a/>   
                                            </div>
                                        </div>
                                    </div>    
                                </div>

                            </form>
                        <?php } else { ?>
                            <h1 class="page-header"> Voce não possue acesso a está pagina, consulte o administrador.</h1>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>
    </body>
</html>
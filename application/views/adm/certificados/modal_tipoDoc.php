<?php 
$nome = (isset($dados[0]["Nome"]))? $dados[0]["Nome"]:"";
$alias = (isset($dados[0]["Alias"]))? $dados[0]["Alias"]:"";
$status = (isset($dados[0]["Status"]))? $dados[0]["Status"]:"";
$id = (isset($dados[0]["idTipoDoc"]))? $dados[0]["idTipoDoc"]:"";
if($id!=""):
?>
<form class="form-horizontal" role="form" id="formTipoDoc" name="formTipoDoc" method="POST" action="update_tipoDoc">
<input type="hidden" id="idTipoDoc" name="idTipoDoc" value="<?= $id ?>"> 
<?php
else:
?>
<form class="form-horizontal" role="form" id="formTipoDoc" name="formTipoDoc" method="POST" action="insert_tipoDoc">
<?php endif;?>
<input hidden id="page" name="page" value="<?= $page; ?>">
<div class='form-group'>
    <div class='col-md-12'>
        <div class="form-group">
            <label for="nmTipoDoc" class="col-md-3 control-label">Documento</label>
            <div class="col-md-9">
                <input type="text" min="0" class="form-control" id="nome" name="nome" placeholder="Informe o nome" value="<?= $nome ?>" required>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Alias</label>
            <div class="col-md-9">
                <input type="text" min="0" class="form-control" id="alias" name="alias" placeholder="Informe o alias" value="<?= $alias ?>" required>
            </div>
        </div>
        <div class="col-md-offset-4 col-md-8">                   
            <label class="radio-inline">
                <input required type="radio" name="status" id="ativo" value="1" <?php if($status == 1){echo 'checked';}?>>
                Ativo
            </label>
            <label class="radio-inline">
                <input required type="radio" name="status" id="inativo" value="0" <?php if($status == 0){echo 'checked';}?>>
                Inativo
            </label>              
        </div>
    </div>
</div>
</form>                     

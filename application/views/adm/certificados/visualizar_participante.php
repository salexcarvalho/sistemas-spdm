<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - CERTIFICADOS</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <?php if ($_SESSION['AcLiberaBtnVizPte'] == true) { ?>
                            <h1 class="page-header">Visualizar Participantes</h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-file-pdf-o"></i> Certificados</li>
                                <li><a href="home"><i class="fa fa-list"></i> Listagem de Participantes</a></li>
                                <li class="active"><i class="glyphicon glyphicon-file"></i> Visualizar participante</li>
                            </ol>
                            <form class="form-horizontal" role="form" method="POST" action="">
                                <div class="form-group">
                                    <div class="col-lg-9">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <span>Dados do Participante</span>
                                            </div>
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <label for="nmParticipante" class="col-md-2 control-label">Titulação</label>
                                                    <div class="col-md-1">                                                   
                                                        <input type="text" disabled class="form-control" id="titulacao" name="titulacao" value="<?= $dadosParticipante[0]['titulacao']; ?>">
                                                    </div>                                           
                                                    <label for="nmParticipante" class="col-md-1 control-label">Nome</label>
                                                    <div class="col-md-5"> 
                                                        <input type="text" disabled class="form-control" id="nmParticipante" name="nmParticipante" value="<?= $dadosParticipante[0]['Nome']; ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="docRG" class="col-md-2 control-label">R.G</label>
                                                    <div class="col-md-7">
                                                        <input type="docRG" disabled class="form-control" id="docRG" name="docRG"  value="<?= $dadosParticipante[0]['RG']; ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="email" class="col-md-2 control-label">Email</label>
                                                    <div class="col-md-7">
                                                        <input type="email" disabled class="form-control" id="email" name="email"  value="<?= $dadosParticipante[0]['Email']; ?>">
                                                    </div>
                                                </div>                                            
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-7">
                                                <a href="home?page=<?= $page; ?>">
                                                    <button type="button" class="btn btn-danger">Voltar</button>
                                                    <a/>   
                                            </div>
                                        </div>
                                    </div>
                                </div>    
                            </form>
                        <?php } else { ?>
                            <h1 class="page-header"> Voce não possue acesso a está pagina, consulte o administrador.</h1>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>

        <script>
            jQuery(document).ready(function () {
                // Remove seleção de ativo no menu.
                $('.nav li').removeClass('active');
                $('#side-menu li').removeClass('active');
                // Ativa botão no menu.
                $('#li-certificados ul').addClass('collapse in');
                $('#li-cad-participantes a').addClass('active');
                $('#li-certificados').addClass('active');
                $('#li-certificados a').addClass('collapse in');
            });
        </script>        
    </body>
</html>
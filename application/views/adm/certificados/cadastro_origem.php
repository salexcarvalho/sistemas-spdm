<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - CERTIFICADOS</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"> Cadastrar Origem de Eventos</h1>
                        <ol class="breadcrumb">                            
                            <li><i class="fa fa-file-pdf-o"></i> Certificados</li>
                            <li><a href="home"><i class="fa fa-list"></i> Lista de Origens de Eventos</a></li>
                            <li class="active"><i class="glyphicon glyphicon-file"></i> Cadastro de Origem de Eventos</li>
                        </ol>
                        <form class="form-horizontal" role="form" method="POST" action="insert_origem_eventos">
                            <div class="form-group">    
                                <div class="col-lg-6">
                                    <div class="panel panel-default"> 
                                        <div class="panel-heading">
                                            <span>Origem de Eventos</span>
                                        </div>    
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <label for="nmOrigem" class="col-lg-3 control-label">Nome da Origem</label>
                                                <div class="col-lg-9">
                                                    <input type="text" min="0" class="form-control" id="nmOrigem" name="nmOrigem" placeholder="Informe o nome da origem" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-3 control-label">Alias</label>
                                                <div class="col-lg-9">
                                                    <input type="text" min="0" class="form-control" id="aliasOrigem" name="aliasOrigem" placeholder="Informe o alias do nome da origem" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-offset-3 col-lg-9">
                                                <div class="form-group"> 
                                                    <label class="radio-inline">
                                                        <input required type="radio" name="status" id="inativo" value="1">
                                                        Ativo
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input required type="radio" name="status" id="inativo" value="0">
                                                        Inativo
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>        
                                    <div class="form-group">
                                        <div class="col-lg-5">
                                            <button type="submit" class="btn btn-primary btn-sm">Cadastrar</button>
                                            <a href="home" class="btn btn-danger">Voltar</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>
    <script>
        jQuery(document).ready(function () {
            // Remove seleção de ativo no menu.
            $('.nav li').removeClass('active');
            $('#side-menu li').removeClass('active');
            // Ativa botão no menu.
            $('#li-certificados ul').addClass('collapse in');
            $('#li-cad-tiposorigem a').addClass('active');
            $('#li-certificados').addClass('active');
            $('#li-certificados a').addClass('collapse in');
        });
    </script>
</body>
</html>
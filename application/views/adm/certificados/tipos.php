<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - CERTIFICADOS</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

    </head>
    <body>

        <!-- Modal -->
        <div class="modal fade" id="modal_excluir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Excluir Tipo de Participante</h4>
                    </div>
                    <div class="modal-body" id="modal_delete_tipo">Deseja excluir este tipo de participante?</div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        <button type="button" class="btn btn-primary" id="modal_btn-excluir">Excluir</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal -->
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Lista de Tipos de Participantes</h1>
                        <ol class="breadcrumb">
                            <li><i class="fa fa-file-pdf-o"></i> Certificados</li>
                            <li class="active"><i class="fa fa-list"></i> Listagem de Tipos de Participantes</li>
                        </ol>

                        <?php if (isset($warning)) : ?>
                            <div id="msg" class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong><?= $warning; ?></strong>
                            </div>
                        <?php endif ?>                          
                        <div class="row">                           
                            <?php if ($_SESSION['AcLiberaBtnVizEve'] == TRUE) { ?>
                            <div class="col-lg-12">                                
                                    <button type="submit" id="btn_novo_tipo" class="pull-right btn btn-primary btn-md" name="btn_novo_tipo"><i class="glyphicon glyphicon-plus-sign"></i>  Adicionar Tipo</button>
                            </div>    
                            <?php } ?>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="tipos">
                                <thead>
                                    <tr>
                                        <th style="text-align: center">#</th>
                                        <th >Nome</th>
                                        <th >Alias</th>
                                        <th style="text-align: center">Ações</th>
                                        <th style="text-align: center">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($data as $tipo) {
                                        ?>
                                        <tr><input hidden id="page" name="page" value="<?= $PaginaAtual; ?>">
                                            <td align="center"><?= $tipo['idTipos']; ?></td>
                                            <td><?= $tipo['Nome']; ?></td>
                                            <td><?= $tipo['Alias']; ?></td>
                                            <td align="center">
                                                <button  type="button" id="btnEditar" class="btn btn-default btn-xs" name="btnEditar" idTipos="<?= $tipo['idTipos']; ?>" title="Editar Tipo de Participante" <?php if ($_SESSION['AcLiberaBtnEdiTPte'] == NULL) { ?>disabled<?php } ?>>
                                                        <i class="glyphicon glyphicon-edit"></i>
                                                </button>                                             
                                                
                                                <button  type="button" id="btnExcluir" class="btn btn-default btn-xs" name="btnExcluir" idTipos="<?= $tipo['idTipos']; ?>" nome="<?= $tipo['Nome']; ?>" <?php if ($_SESSION['AcLiberaBtnExcTPte'] == NULL) { ?>disabled<?php } ?> data-toggle="modal" data-target="#modal_excluir" data-placement="bottom" title="Excluir Tipo de Participante" data-delay="1">
                                                    <i class="glyphicon glyphicon-trash"></i>
                                                </button>
                                            </td>
                                            <td align="center">
                                                <input type="hidden" name="idTipos" value="<?= $tipo['idTipos']; ?>">
                                                <?php
                                                if ($tipo['Status'] == "0") {
                                                    ?>
                                                    <button type="submit" id="btnAtivar_<?= $tipo['idTipos']; ?>" class="btn btn-danger btn-xs" idTipos="<?= $tipo['idTipos']; ?>" name="btnAtivar" title="Ativar Tipo de Participante" <?php if ($_SESSION['AcLiberaBtnAtivTPte'] == NULL) { ?>disabled<?php } ?>>
                                                        <i id="activ_<?= $tipo['idTipos']; ?>" class="glyphicon glyphicon-ban-circle"></i>
                                                    </button>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <button type="submit" id="btnAtivar_<?= $tipo['idTipos']; ?>" class="btn btn-success btn-xs" idTipos="<?= $tipo['idTipos']; ?>" name="btnAtivar" title="Destivar Tipo de Participante" <?php if ($_SESSION['AcLiberaBtnAtivTPte'] == NULL) { ?>disabled<?php } ?>>
                                                        <i id="activ_<?= $tipo['idTipos']; ?>" class="glyphicon glyphicon-ok"></i>
                                                    </button>
                                                    <?php
                                                }
                                                ?>

                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                            <?php include(kohana::find_file('views/paginator', 'paginacao', 'php')) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>

        <script>
            jQuery(document).ready(function () {
                // Remove seleção de ativo no menu.
                $('.nav li').removeClass('active');
                $('#side-menu li').removeClass('active');
                // Ativa botão no menu.
                $('#li-certificados ul').addClass('collapse in');
                $('#li-cad-tipos a').addClass('active');
                $('#li-certificados').addClass('active');
                $('#li-certificados a').addClass('collapse in');
            });
        </script>

        <script>
            jQuery(document).ready(function ()
            {
                $('#tipos').dataTable(
                        {
                            "aoColumnDefs": [{"bSortable": false, "aTargets": [3, 4]}],
                            "paging": false,
                            "info": false,
                            "aaSorting": [[0, 'desc']],
                            "bFilter": false,
                            "oLanguage":
                                    {
                                        "sSearch": " Buscar: "
                                    }
                        });

                var idTipos;
                var nome;
                $('body').on("click", 'button[name=btnExcluir]', function ()
                {
                    idTipos = $(this).attr('idTipos');
                    nome = $(this).attr('nome');
                    $('#modal_delete_tipo').html("Deseja excluir o Tipo de Participante <b>\"" + nome + "\"</b> ?");
                });


                $("body").on("click", "button[name=btnAtivar]", function () {
                    idTipos = $(this).attr('idTipos');
                    //alert(txtUsuario);
                    $.get('update_ativar_tipo?idTipos=' + idTipos, function (data) {

                        if (data === "1") {
                            $('#btnAtivar_' + idTipos).addClass('btn btn-success btn-xs');
                            $('#btnAtivar_' + idTipos).removeClass('btn-danger');
                            $('#activ_' + idTipos).removeClass('glyphicon-ban-circle');
                            $('#activ_' + idTipos).addClass('glyphicon-ok');
                        } else {
                            $('#btnAtivar_' + idTipos).addClass('btn btn-danger btn-xs');
                            $('#btnAtivar_' + idTipos).removeClass('btn-success');
                            $('#activ_' + idTipos).addClass('glyphicon-ok');
                            $('#activ_' + idTipos).addClass('glyphicon-ban-circle');
                        }
                    });
                });


                $('#modal_excluir').on('hidden.bs.modal', function ()
                {
                    window.location.replace("home");
                });

                $('#modal_btn-excluir').on("click", function ()
                {
                    var btn = $(this);
                    btn.text('loading');

                    btn.removeClass('btn-primary');
                    btn.removeClass('btn-danger');
                    btn.removeClass('btn-success');
                    btn.addClass('btn-primary');

                    $('#modal_btn-excluir').prop('disabled', true);

                    $.ajax("delete_tipo",
                            {
                                type: "POST",
                                data:
                                        {
                                            idTipos: idTipos
                                        }
                            })
                            .done(function (data)
                            {
                                $('#modal_delete_tipo').html('Tipo excluído com sucesso');

                                btn.removeClass('btn-primary');
                                btn.addClass('btn-success');
                            })
                            .fail(function ()
                            {
                                $('#modal_delete_tipo').html('Erro: ' + data);

                                btn.removeClass('btn-primary');
                                btn.addClass('btn-danger');
                            })
                            .always(function ()
                            {
                                btn.text('Excluído');
                            });
                });

                $("#btn_novo_tipo").click(function () {
                    var page = $("#page").val();
                    var params = {
                        url: "modal_tipos?page="+page,
                        title: 'Cadastrar Tipo',
                        size: eModal.size.md,
                        buttons: [
                            {text: 'GRAVAR', style: 'primary', close: false, type: 'submit', form: 'formTipo'},
                            {text: 'FECHAR', style: 'danger', close: true}                            
                        ]
                    };
                    return eModal.ajax(params);
                });
                
                 $('body').on("click", 'button[name=btnEditar]', function () {
                    var idTipos = $(this).attr('idTipos');
                    var editar = 0;
                    var page = $("#page").val();
                    var params = {
                        url: "modal_tipos?page="+page+"&idTipos="+idTipos+"&editar="+editar,
                        title: 'Editar Tipo',
                        size: eModal.size.md,
                        buttons: [
                            {text: 'SALVAR', style: 'primary', close: false, type: 'submit', form: 'formTipo', id:'salvar'},
                            {text: 'FECHAR', style: 'danger', close: true}
                        ]
                    };
                    return eModal.ajax(params);
                });
                
                $('body').on("click", 'button[name=btnVisualizar]', function () {
                    var idTipos = $(this).attr('idTipos');
                    var editar = 1;
                    var page = $("#page").val();
                    var params = {
                         url: "modal_tipos?page="+page+"&idTipos="+idTipos+"&editar="+editar,
                        title: 'Visualizar Origem',
                        size: eModal.size.md,
                        buttons: [
                            {text: 'FECHAR', style: 'danger', close: true}                          
                        ]
                    };
                    return eModal.ajax(params);
                });

                $('.dataTables_filter label').addClass('pull-right');
            });
        </script>

    </body>
</html>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title>SISTEMAS SPDM - CERTIFICADOS</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <script>
            jQuery(document).ready(function () {
                // Remove seleção de ativo no menu.
                $('.nav li').removeClass('active');
                $('#side-menu li').removeClass('active');
                // Ativa botão no menu.
                $('#li-certificados ul').addClass('collapse in');
                $('#li-cad-eventos a').addClass('active');
                $('#li-certificados').addClass('active');
                $('#li-certificados a').addClass('collapse in');
            });
        </script>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

    </head>
    <body>
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>

            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>

            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <?php if ($_SESSION['AcLiberaBtnEdiEve'] == true) { ?>
                            <h1 class="page-header"> Editar Certificado</h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-file-pdf-o"></i> Certificados</li>
                                <li><a href="home"><i class="fa fa-list"></i> Listagem de Certificados</a></li>
                                <li class="active"><i class="glyphicon glyphicon-file"></i> Visualização do Certificado</li>
                            </ol>
                            <form class="form-horizontal" role="form" method="POST" action="update_evento" enctype="multipart/form-data">
                                <div class="form-group">
                                    <div class="col-lg-9">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">

                                                <span>Dados do Evento</span>

                                            </div>
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <label for="nmEvento" class="col-lg-2 control-label">Nome do Evento</label>
                                                    <div class="col-lg-10">
                                                        <input hidden id="page" name="page" value="<?= $page; ?>">
                                                        <input hidden id="idEvento" name="idEvento" placeholder="" value="<?= $evento[0]['idEventos']; ?>"> 
                                                        <input type="text" min="0" class="form-control" id="nmEvento" name="nmEvento" placeholder="" value="<?= $evento[0]['NomeEvento']; ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="Local" class="col-lg-2 control-label">Local</label>
                                                    <div class="col-lg-10">
                                                        <input type="Local" class="form-control" id="Local" name="Local" placeholder="" value="<?= $evento[0]['Local']; ?>">
                                                    </div>
                                                </div>                                        
                                                <div class="form-group">
                                                    <label for="dtEvento" class="col-lg-2 control-label">Data de emissão</label>
                                                    <div class="col-lg-2">
                                                        <input type="date" class="form-control" id="dtEvento" name="dtEvento" placeholder="" value="<?= $evento[0]['DataEvento']; ?>">
                                                    </div>
                                                    <label for="Hora" class="col-lg-1 control-label">Hora</label>
                                                    <div class="col-lg-2">
                                                        <input type="time" class="form-control" id="Hora" name="Hora" placeholder="" value="<?= $evento[0]['Hora']; ?>">
                                                    </div>
                                                    <label for="c_Horaria" class="col-lg-2 control-label">Carga Horaria</label>
                                                    <div class="col-lg-3">
                                                        <input type="text" class="form-control" id="c_Horaria" name="c_Horaria" placeholder="" value="<?= $evento[0]['Carga_Horaria']; ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="msg_Cert" class="col-lg-2 control-label">Texto do Certificado</label>
                                                    <div class="col-lg-10">
                                                        <textarea style="resize: vertical;" class="form-control" rows="3" name="msg_Cert" id="msg_Cert" placeholder=""><?= $evento[0]['Mensagem_Certificado']; ?></textarea>
                                                        <script>
                                                            //CKEDITOR.replace('msg_Cert', { height: 75 } );
                                                        </script>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="idOrigem" class="col-lg-2 control-label">Origem</label>
                                                    <div class="col-lg-10">
                                                        <select id="idOrigem" name="idOrigem" class="form-control" required>
                                                            <option value="">Selecione a Origem do Evento</option>
                                                            <?php foreach ($origens as $origem) {
                                                                ?>
                                                                <option <?= ($evento[0]['Origem_idTipoOrigem'] == $origem['idTipoOrigem']) ? 'selected' : ''; ?> value="<?= $origem['idTipoOrigem']; ?>"><?= $origem['Nome_Origem']; ?></option> 
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="idAssinatura1" class="col-lg-2 control-label">Assinatura Principal</label>
                                                    <div class="col-lg-4">
                                                        <select  id="idAssinatura1" name="idAssinatura1" class="form-control" required>
                                                            <option value="NULL">Selecione a Assinatura Principal</option>
                                                            <?php foreach ($assinaturas as $assinatura1) {
                                                                ?>
                                                                <option <?= ($evento[0]['AssinaturaPrincipal_IdAssinatura'] == $assinatura1['idAssinatura']) ? 'selected' : ''; ?>  value="<?= $assinatura1['idAssinatura']; ?>"><?= $assinatura1['NomeAssinatura']; ?></option>    
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                    <label for="idAssinatura2" class="col-lg-2 control-label">Assinatura Secundária</label>
                                                    <div class="col-lg-4">
                                                        <select  id="idAssinatura2" name="idAssinatura2" class="form-control">
                                                            <option value="">Selecione a Assinatura Secundária</option>
                                                            <?php foreach ($assinaturas as $assinatura2) {
                                                                ?>
                                                                <option <?= ($evento[0]['AssinaturaSecundaria_IdAssinatura'] == $assinatura2['idAssinatura']) ? 'selected' : ''; ?>  value="<?= $assinatura2['idAssinatura']; ?>"><?= $assinatura2['NomeAssinatura']; ?></option>     
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="idAssinatura3" class="col-lg-2 control-label">Assinatura Opcional 01</label>
                                                    <div class="col-lg-4">
                                                        <select  id="idAssinatura3" name="idAssinatura3" class="form-control">
                                                            <option value="">Selecione a Assinatura Opcional 01</option>
                                                            <?php foreach ($assinaturas as $assinatura3) {
                                                                ?>
                                                                <option <?= ($evento[0]['AssinaturaTerceira_IdAssinatura'] == $assinatura3['idAssinatura']) ? 'selected' : ''; ?>  value="<?= $assinatura3['idAssinatura']; ?>"><?= $assinatura3['NomeAssinatura']; ?></option>    
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                    <label for="idAssinatura4" class="col-lg-2 control-label">Assinatura Opcional 02</label>
                                                    <div class="col-lg-4">
                                                        <select  id="idAssinatura4" name="idAssinatura4" class="form-control">
                                                            <option value="">Selecione a Assinatura Opcional 02</option>
                                                            <?php foreach ($assinaturas as $assinatura4) {
                                                                ?>
                                                                <option <?= ($evento[0]['AssinaturaQuarta_IdAssinatura'] == $assinatura4['idAssinatura']) ? 'selected' : ''; ?>  value="<?= $assinatura4['idAssinatura']; ?>"><?= $assinatura4['NomeAssinatura']; ?></option>     
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="img_Cert" class="col-lg-2 control-label">Imagem do Certificado</label>
                                                    <div class="col-lg-6">
                                                        <input hidden id="img_Cert_hidenn" name="img_Cert_hidenn" placeholder="" value="<?= $evento[0]['ImgCertificado']; ?>">
                                                        <input type="file" class="form-control" id="img_Cert" name="img_Cert">
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <img src="<?= URL::base(); ?>upload/img/certificados/<?= $evento[0]["ImgCertificado"]; ?>" width="300" alt="" class="thumbnail"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-lg-5">
                                                <button type="submit" class="btn btn-primary btn-sm">Alterar</button>
                                                <a href="home?page=<?= $page; ?>" class="btn btn-danger">Voltar</a>
                                            </div>
                                        </div>    
                                    </div>
                                </div>
                            </form>
                        <?php } else { ?>
                            <h1 class="page-header"> Voce não possue acesso a está pagina, consulte o administrador.</h1>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>
    </body>
</html>
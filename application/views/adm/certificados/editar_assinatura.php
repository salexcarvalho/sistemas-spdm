<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - CERTIFICADOS</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <?php if ($_SESSION['AcLiberaBtnEdiAss'] == true) { ?>
                            <h1 class="page-header"> Editar Assinatura</h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-file-pdf-o"></i> Assinaturas</li>
                                <li><a href="home"><i class="fa fa-list"></i> Listagem de Assinaturas</a></li>
                                <li class="active"><i class="glyphicon glyphicon-file"></i> Edição da Assinatura</li>
                            </ol>
                            <form class="form-horizontal" role="form" method="POST" action="update_assinatura" enctype="multipart/form-data">
                                <div class="form-group">
                                    <div class="col-lg-9">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">

                                                <span>Dados da Assinatura</span>

                                            </div>
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <label for="titulacao" class="col-lg-2 control-label">Titulação(ões)</label>
                                                    <div class="col-lg-10">
                                                        <input type="text" min="0" class="form-control" id="titulacao" name="titulacao" value="<?= $dados[0]['titulacao']; ?>">
                                                    </div>
                                                </div>  
                                                <div class="form-group">
                                                    <label for="nmAssinatura" class="col-lg-2 control-label">Nome</label>
                                                    <div class="col-lg-10">
                                                        <input hidden id="page" name="page" value="<?= $page; ?>">
                                                        <input hidden id="idAssinaturas" name="idAssinaturas" value="<?= $dados[0]['idAssinaturas']; ?>"> 
                                                        <input type="text" min="0" class="form-control" id="nome" name="nome" value="<?= $dados[0]['nome']; ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="Local" class="col-lg-2 control-label">cargo</label>
                                                    <div class="col-lg-10">
                                                        <input type="text" class="form-control" id="cargo" name="cargo" value="<?= $dados[0]['cargo']; ?>">
                                                    </div>
                                                </div>                                        
                                                <div class="form-group">
                                                    <label for="PngAssinatura" class="col-lg-2 control-label">PNG da Assinatura</label>
                                                    <div class="col-lg-6">
                                                        <input hidden id="PngAssinatura_hidenn" name="PngAssinatura_hidenn" value="<?= $dados[0]['PngAssinatura']; ?>">
                                                        <input type="file" class="form-control" id="PngAssinatura" name="PngAssinatura">
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <img src="<?= URL::base(); ?>upload/img/certificados/<?= $dados[0]["PngAssinatura"]; ?>" width="280" alt="" class="thumbnail"/>
                                                    </div>
                                                </div>
                                                <div class="col-lg-offset-2 col-lg-5">
                                                    <div class="form-group"> 
                                                        <label class="radio-inline">
                                                            <input required type="radio" name="status" id="inativo" value="1" <?= ($dados[0]['status'] == '1') ? 'checked' : ''; ?>>
                                                            Ativo
                                                        </label>
                                                        <label class="radio-inline">                                                    
                                                            <input required type="radio" name="status" id="inativo" value="0" <?= ($dados[0]['status'] == '0') ? 'checked' : ''; ?>>
                                                            Inativo
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-5">
                                            <button type="submit" class="btn btn-primary btn-sm">Alterar</button>
                                            <a href="../sec_assinaturas/home?page=<?= $page; ?>" class="btn btn-danger">Voltar</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        <?php } else { ?>
                            <h1 class="page-header"> Voce não possue acesso a está pagina, consulte o administrador.</h1>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>
        <script>
            jQuery("#PngAssinatura").change(function () { //campo de imagem

                var fr = new FileReader;

                fr.onload = function () {
                    var img = new Image;
                    img.onload = function () {
                        if ((img.width > 280 && this.height > 105) || (img.width < 280 && this.height < 105)) {
                            jQuery('.aviso').append("<h2>Arquivo de assinatura deve ter Altura = 105 e Largura = 280 </h2>");
                            jQuery(".btn-primary btn-sm").attr("disabled", true); //Desabilita o botão sumbit     		

                        } else {
                            jQuery("#submit").removeAttr("disabled"); // Abilita o botão submit
                        }
                    };
                    img.src = fr.result;
                };
                fr.readAsDataURL(this.files[0]);
            });
        </script>
        <script>
            jQuery(document).ready(function () {
                // Remove seleção de ativo no menu.
                $('.nav li').removeClass('active');
                $('#side-menu li').removeClass('active');
                // Ativa botão no menu.
                $('#li-certificados ul').addClass('collapse in');
                $('#li-assinatura a').addClass('active');
                $('#li-certificados').addClass('active');
                $('#li-certificados a').addClass('collapse in');
            });
        </script>
    </body>
</html>
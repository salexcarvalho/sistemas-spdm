<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - CERTIFICADOS</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <?php if ($_SESSION['AcLiberaBtnAddAss'] == true) { ?>
                            <h1 class="page-header"> Cadastrar Assinatura</h1>
                            <ol class="breadcrumb">                            
                                <li><i class="fa fa-file-pdf-o"></i> Assinaturas</li>
                                <li><a href="home"><i class="fa fa-list"></i> Lista de Assinaturas</a></li>
                                <li class="active"><i class="glyphicon glyphicon-file"></i> Cadastro de Assinatura</li>
                            </ol>
                            <form class="form-horizontal" role="form" method="POST" action="insert_assinatura" enctype="multipart/form-data">
                                <div class="form-group">
                                    <div class="col-lg-6">
                                        <div class="panel panel-default"> 
                                            <div class="panel-heading">
                                                <span>Dados da Assinatura</span>
                                            </div>    
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <label for="titulacao" class="col-lg-3 control-label">Titulação(ões)</label>
                                                    <div class="col-lg-9">
                                                        <input type="text" min="0" class="form-control" id="titulacao" name="titulacao" placeholder="Informe a(s) titulação(ões), se houver">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="nmAssinatura" class="col-lg-3 control-label">Nome</label>
                                                    <div class="col-lg-9">
                                                        <input type="text" min="0" class="form-control" id="nome" name="nome" placeholder="Informe o nome completo" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-3 control-label">cargo</label>
                                                    <div class="col-lg-9">
                                                        <input type="text" min="0" class="form-control" id="cargo" name="cargo" placeholder="Informe o cargo" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-lg-9 aviso"> </div>
                                                </div> 
                                                <div class="form-group">
                                                    <label for="PngAssinatura" class="col-lg-3 control-label">PNG da Assinatura</label>
                                                    <div class="col-lg-9">
                                                        <input type="file" class="form-control" id="PngAssinatura" name="PngAssinatura" required>
                                                    </div>
                                                </div> 
                                                <div class="col-lg-offset-2 col-lg-5">
                                                    <div class="form-group"> 
                                                        <label class="radio-inline">
                                                            <input required type="radio" name="status" id="inativo" value="1">
                                                            Ativo
                                                        </label>
                                                        <label class="radio-inline">
                                                            <input required type="radio" name="status" id="inativo" value="0">
                                                            Inativo
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>        
                                        <div class="form-group">
                                            <div class="col-lg-5">
                                                <button type="submit" class="btn btn-primary btn-sm">Cadastrar</button>
                                                <a href="../sec_assinaturas/home" class="btn btn-danger">Voltar</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>    
                            </form>
                        <?php } else { ?>
                            <h1 class="page-header"> Voce não possue acesso a está pagina, consulte o administrador.</h1>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>
    <script>
        jQuery("#PngAssinatura").change(function () { //campo de imagem

            var fr = new FileReader;

            fr.onload = function () {
                var img = new Image;

                img.onload = function () {


                    if ((img.width > 280 && this.height > 105) || (img.width < 280 && this.height < 105)) {
                        jQuery('.aviso').append("<h2>Arquivo de assinatura deve ter Altura = 105 e Largura = 280 </h2>");
                        jQuery(".btn-primary btn-sm").attr("disabled", true); //Desabilita o botão sumbit     		

                    } else {
                        jQuery("#submit").removeAttr("disabled"); // Abilita o botão submit
                    }



                };

                img.src = fr.result;
            };

            fr.readAsDataURL(this.files[0]);

        });
    </script>
    <script>
        jQuery(document).ready(function () {
            // Remove seleção de ativo no menu.
            $('.nav li').removeClass('active');
            $('#side-menu li').removeClass('active');
            // Ativa botão no menu.
            $('#li-certificados ul').addClass('collapse in');
            $('#li-assinatura a').addClass('active');
            $('#li-certificados').addClass('active');
            $('#li-certificados a').addClass('collapse in');
        });
    </script>
</body>
</html>
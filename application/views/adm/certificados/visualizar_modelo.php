<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - CERTIFICADOS</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <script>
            jQuery(document).ready(function () {
                // Remove seleção de ativo no menu.
                $('.nav li').removeClass('active');
                $('#side-menu li').removeClass('active');
                // Ativa botão no menu.
                $('#li-certificados ul').addClass('collapse in');
                $('#li-cad-eventos a').addClass('active');
                $('#li-certificados').addClass('active');
                $('#li-certificados a').addClass('collapse in');
            });
        </script>       
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <?php if ($_SESSION['AcLiberaBtnVizEve'] == true) { ?>
                            <h1 class="page-header">Visualizar Certificados</h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-file-pdf-o"></i> Certificados</li>
                                <li><a href="modelos"><i class="fa fa-list"></i> Listagem de Modelos de Certificados</a></li>
                                <li class="active"><i class="glyphicon glyphicon-file"></i> Visualizar de Modelos</li>
                            </ol>
                            <form class="form-horizontal" role="form" method="POST" action="">
                                <div class="form-group">
                                    <div class="col-lg-9">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <span>Dados do Modelo de Certificado</span>
                                            </div>
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <label for="eventoNome" class="col-lg-2 control-label">Nome do Evento</label>
                                                    <div class="col-lg-10">
                                                        <input type="text" class="form-control" id="eventoNome" name="eventoNome" placeholder="" disabled value="<?= $modelos[0]['eventoNome']; ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="localEvento" class="col-lg-2 control-label">Local</label>
                                                    <div class="col-lg-10">
                                                        <input type="text" class="form-control" id="localEvento" name="localEvento" placeholder="" disabled value="<?= $modelos[0]['localEvento']; ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="Data" class="col-lg-2 control-label">Data de emissão</label>
                                                    <div class="col-lg-2">
                                                        <input type="text" class="form-control" id="dataEmissao" name="dataEmissao" placeholder="" disabled value="<?= strftime('%d/%m/%Y', strtotime($modelos[0]['dataEmissao'])); ?>">
                                                    </div>
                                                    <label for="Hora" class="col-lg-1 control-label">Hora</label>
                                                    <div class="col-lg-2">
                                                        <input type="text" class="form-control" id="Hora" name="Hora" placeholder="" disabled value="<?= $modelos[0]['horaEmissao']; ?>">
                                                    </div>
                                                    <label for="c_Horaria" class="col-lg-2 control-label">Carga Horaria</label>
                                                    <div class="col-lg-3">
                                                        <input type="text" class="form-control" id="c_Horaria" name="c_Horaria" placeholder="" disabled value="<?= $modelos[0]['cargaHoraria']; ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="msg_Cert" class="col-lg-2 control-label">Texto do Certificado</label>
                                                    <div class="col-lg-10">
                                                        <textarea style="resize: vertical;" disabled class="form-control" rows="3" name="msg_Cert" id="msg_Cert" placeholder=""><?= $modelos[0]['TextoPadrao']; ?></textarea>
                                                        <script>
                                                            //CKEDITOR.replace('msg_Cert', { height: 75 } );
                                                        </script>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="idOrigem" class="col-lg-2 control-label">Origem</label>
                                                    <div class="col-lg-10">
                                                        <?php
                                                        $model_origem = new Model_Sec_Origem('default');
                                                        $origem = $model_origem->select_origem($modelos[0]['idOrigem']);
                                                        $model_assinatura = new Model_Sec_Assinatura('default');
                                                        ?>
                                                        <div class="col-lg-6">
                                                            <input type="text" class="form-control" id="" name="" disabled value="<?= $origem[0]['Nome']; ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <?php
                                                    $assinatura1 = $model_assinatura->select_assinatura($modelos[0]['fk_idAssinatura_1']);
                                                    $assinatura2 = $model_assinatura->select_assinatura($modelos[0]['fk_idAssinatura_2']);
                                                    $assinatura3 = $model_assinatura->select_assinatura($modelos[0]['fk_idAssinatura_3']);
                                                    $assinatura4 = $model_assinatura->select_assinatura($modelos[0]['fk_idAssinatura_4']);
                                                    ?>
                                                    <label for="idAssinaturas1" class="col-lg-2 control-label">Assinatura Principal</label>

                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" id="" name="" disabled value="<?= $assinatura1[0]['nome']; ?>">
                                                    </div>

                                                    <label for="idAssinaturas2" class="col-lg-2 control-label">Assinatura Secundária</label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" id="" name="" disabled value="<?= $assinatura2[0]['nome']; ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="idAssinaturas3" class="col-lg-2 control-label">Assinatura Opcional 01</label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" id="" name="" disabled value="<?= $assinatura3[0]['nome']; ?>">
                                                    </div>
                                                    <label for="idAssinaturas4" class="col-lg-2 control-label">Assinatura Opcional 02</label>
                                                    <div class="col-lg-4">                                            
                                                        <input type="text" class="form-control" id="" name="" disabled value="<?= $assinatura4[0]['nome']; ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="img_Cert" class="col-lg-2 control-label">Imagem do Certificado</label>
                                                    <div class="col-lg-6">
                                                        <input type="text" class="form-control" id="img_Cert" name="img_Cert" disabled value="<?= $modelos[0]['imgCertificado']; ?>">
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <img src="<?= URL::base(); ?>upload/img/certificados/<?= $modelos[0]["imgCertificado"]; ?>" width="300" alt="" class="thumbnail"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-lg-5">
                                                <a href="modelos?page=<?= $page; ?>">
                                                    <button type="button" class="btn btn-danger">Voltar</button>
                                                    <a/>   
                                            </div>
                                        </div>
                                    </div>
                                </div>    
                            </form>
                        <?php } else { ?>
                            <h1 class="page-header"> Voce não possue acesso a está pagina, consulte o administrador.</h1>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>
    </body>
</html>
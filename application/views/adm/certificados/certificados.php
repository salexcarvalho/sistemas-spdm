<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - CERTIFICADOS</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

    </head>
    <body>

        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Lista de Certificados</h1>
                        <ol class="breadcrumb">
                            <li><i class="fa fa-file-pdf-o"></i> Certificados</li>
                            <li><i class="fa fa-list"></i> Listagem de Certificados Gerados</li>
                        </ol>

                        <?php if (isset($warning)) : ?>
                            <div id="msg" class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong><?= $warning; ?></strong>
                            </div>
                        <?php endif ?>                        
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="certificados">
                                <thead>
                                    <tr>
                                        <th style="text-align: center" width="5%">id</th>
                                        <th width="30%">Evento</th>
                                        <th style="text-align: center" width="10%">Data</th>
                                        <th>Participante</th>
                                        <th>Autenticação</th>
                                        <th width="6%" style="text-align: center">Impresso</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $model_certificados = new Model_Sec_Certificado('default');
                                    $model_participantes = new Model_Sec_Participantes('default');
                                    foreach ($data as $apoio) {
                                        $certificado = $model_certificados->select_certificado($apoio['idCertificado']);
                                        $participante = $model_participantes->select_participante($apoio['idParticipante']);
                                        if ($apoio['idCertificado'] != NULL && $apoio['idCertificado'] > 0):
                                            ?>
                                            <tr>
                                                <td align="center"><?= $apoio['idApoio']; ?></td>
                                                <td><?= $certificado[0]['eventoNome']; ?></td>
                                                <td style="text-align: center"><?= strftime('%d/%m/%Y', strtotime($certificado[0]['dataEmissao'])); ?></td>
                                                <td><?= $participante[0]['Nome']; ?></td>
                                                <td><?= $apoio['Autenticacao']; ?></td>
                                                <td align="center"><?= $apoio['Impresso'] ? 'Sim' : 'Não'; ?></td>
                                            </tr>
                                            <?php
                                        else:
                                            ?>
                                            <tr>
                                                <td align="center"></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td align="center"></td>
                                            </tr>
                                        <?php
                                        endif;
                                    }
                                    ?>
                                </tbody>
                            </table>
                            <?php include(kohana::find_file('views/paginator', 'paginacao', 'php')) ?>                                   
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>

        <script>
            jQuery(document).ready(function () {
                // Remove seleção de ativo no menu.
                $('.nav li').removeClass('active');
                $('#side-menu li').removeClass('active');
                // Ativa botão no menu.
                $('#li-certificados ul').addClass('collapse in');
                $('#li-cad-certificados a').addClass('active');
                $('#li-certificados').addClass('active');
                $('#li-certificados a').addClass('collapse in');
            });
        </script>

        <script>
            jQuery(document).ready(function ()
            {
                $('#certificados').dataTable(
                        {
                            "aoColumnDefs": [{"bSortable": false, "aTargets": [4, 5]}],
                            "paging": false,
                            "info": false,
                            "bFilter": false,
                            "aaSorting": [[0, 'desc']],
                            "oLanguage":
                                    {
                                        "sSearch": " Buscar: "
                                    }
                        });

                var idCertificado;

                $('#modal_excluir').on('hidden.bs.modal', function ()
                {
                    window.location.replace("certificados");
                });

                $('#btn_novo_certificado').on("click", function ()
                {
                    window.location.href = "cadastro_certificado";
                });


                $("body").on("click", "button[name=btnVisualizar]", function ()
                {
                    idCertificados = $(this).attr('idCertificado');
                    window.location.href = "visualizar_certificado?idCertificado=" + idCertificados;
                });

                $('.dataTables_filter label').addClass('pull-right');
            });
        </script>

    </body>
</html>
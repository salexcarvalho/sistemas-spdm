<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - CERTIFICADOS</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <?php if ($_SESSION['AcLiberaBtnEdiTPte'] == true) { ?>
                            <h1 class="page-header"> Editar Tipo de Participante</h1>
                            <ol class="breadcrumb"> 
                                <li><i class="fa fa-file-pdf-o"></i> Certificados</li>
                                <li><a href="home"><i class="fa fa-list"></i> Lista de tipos de participantes</a></li>
                                <li class="active"><i class="glyphicon glyphicon-file"></i> Edição de dados do tipo de participante</li>
                            </ol>

                            <?php if (isset($warning)) : ?>
                                <div id="msg" class="alert alert-success alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <strong><?= $warning; ?></strong>
                                </div>
                            <?php endif ?>                          

                            <form class="form-horizontal" role="form" method="POST" action="update_tipo">
                                <div class="form-group">
                                    <div class="col-lg-6">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">

                                                <span>Dados do Tipo de Participante</span>

                                            </div>
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <label for="nome" class="col-lg-2 control-label">Nome</label>
                                                    <div class="col-lg-10">
                                                        <input type="hidden" id="page" name="page" value="<?= $page; ?>">
                                                        <input type="hidden" id="idTipos" name="idTipos" placeholder="" value="<?= $tipos[0]['idTipos']; ?>">  
                                                        <input type="text" min="0" class="form-control" id="nome" name="nome" placeholder="" value="<?= $tipos[0]['Nome']; ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="alias" class="col-lg-2 control-label">Alias</label>
                                                    <div class="col-lg-10">
                                                        <input type="text" class="form-control" id="alias" name="alias" placeholder="" value="<?= $tipos[0]['Alias']; ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-lg-offset-2 col-lg-10">
                                                        <div class="form-group"> 
                                                            <label class="radio-inline">&nbsp;&nbsp;&nbsp;
                                                                <input required type="radio" name="status" id="inativo" value="1" <?= ($tipos[0]['Status'] == '1') ? 'checked' : ''; ?>>
                                                                Ativo
                                                            </label>
                                                            <label class="radio-inline">
                                                                <input required type="radio" name="status" id="inativo" value="0" <?= ($tipos[0]['Status'] == '0') ? 'checked' : ''; ?>>
                                                                Inativo
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>    
                                        <div class="form-group">
                                            <div class="col-lg-10">
                                                <button type="submit" class="btn btn-primary btn-sm">Alterar</button>
                                                <a href="home?page=<?= $page; ?>" class="btn btn-danger">Voltar</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>   
                        <?php } else { ?>
                            <h1 class="page-header"> Voce não possue acesso a está pagina, consulte o administrador.</h1>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>
        <script>
            jQuery(document).ready(function () {
                // Remove seleção de ativo no menu.
                $('.nav li').removeClass('active');
                $('#side-menu li').removeClass('active');
                // Ativa botão no menu.
                $('#li-certificados ul').addClass('collapse in');
                $('#li-cad-Tipos a').addClass('active');
                $('#li-certificados').addClass('active');
                $('#li-certificados a').addClass('collapse in');
            });
        </script>        
    </body>
</html>
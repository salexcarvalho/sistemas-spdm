<?php Session::instance(); ?>
<?php Funcoes::carrega(); //echo "<pre>"; var_dump($_SESSION['idUsuario']); echo "</pre>";    ?>

<!-- Bootstrap Core CSS -->
<link href="<?= URL::base(); ?>theme/backend/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- MetisMenu CSS -->
<link href="<?= URL::base(); ?>theme/backend/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

<!-- SbAdmin CSS -->
<link href="<?= URL::base(); ?>theme/backend/dist/css/sb-admin-2.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="<?= URL::base(); ?>theme/backend/css/custom.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="<?= URL::base(); ?>theme/backend/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

<!-- jQuery Version 3.2.1 -->
<script src="<?= URL::base(); ?>theme/backend/js/jquery-3.2.1.js"></script>
<script src="<?= URL::base(); ?>theme/backend/js/jquery-migrate-1.4.1.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?= URL::base(); ?>theme/backend/vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="<?= URL::base(); ?>theme/backend/vendor/metisMenu/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="<?= URL::base(); ?>theme/backend/dist/js/sb-admin-2.js"></script>

<!-- datapicker plugin -->
<script src='<?= URL::base(); ?>theme/backend/vendor/fullcalendar/lib/moment.min.js'></script>       
<script src='<?= URL::base(); ?>theme/backend/vendor/fullcalendar/locale/pt-br.js'></script>

<script src='<?= URL::base(); ?>theme/backend/vendor/fullcalendar/lib/jquery-ui.min.js'></script>
<link href='<?= URL::base(); ?>theme/backend/vendor/datetimepicker/css/bootstrap-datetimepicker.min.css' rel='stylesheet' />
<script type="text/javascript" src="<?= URL::base(); ?>theme/backend/vendor/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>

<style>
    hr {
        margin-top: 0px;
    }
</style>
<script>
    $(document).ready(function () {

    });
</script>

<div id="testedados" class="container-fluid bd-example-row">

    <?php if ($tipo === 'individual') : ?>
            <form name="reservaForm" name="reservaForm" id="reservaForm" action="enviarCertificado" method="POST" class="form-horizontal">            
    <?php else : ?>
            <form name="reservaForm" name="reservaForm" id="reservaForm" action="enviarCertificados" method="POST" class="form-horizontal">  
   <?php endif; ?> 
            <div class='form-group col-md-12'>
                <?php
                $cont = 0;
                $contE = 0;
                $enviar = "";
                $evento = "";
                $model_participante = new Model_Sec_Participantes();
                $model_certificado = new Model_Sec_Certificado();
                $model_tipos = new Model_Sec_Tipos();
                
                foreach ($dados as $dado):                    
                    $participante = $model_participante->select_participantes_id($dado['idParticipante']);
                    $evento = $model_certificado->select_certificado($dado['idCertificado']);
                    $cert = $model_tipos->select_tipo($dado['idTipo']);
                    $cont++;
                    if ($participante[0]['Email'] != '' && !is_null($participante[0]['Email'])):
                        $contE++;
                        ?>
                        <div class="row">
                            <input type="hidden" id="idApoio" <?php if($tipo === 'individual'){echo "name='idApoio'";}else{echo "name='idApoio[]'";}?> value="<?= $dado['idApoio'] ?>">                                                        
                            <div class="col-md-4">                                
                                <label>Nome:</label>
                                <label style="font-weight:1;"> <?= $participante[0]['Nome']; ?></label>                                  
                            </div>   
                            <div class="col-md-4">
                                <label>Email:</label>
                                <label style="font-weight:1;"><?= $participante[0]['Email']; ?></label>                                  
                            </div>
                            <div class="col-md-4">
                                <label>Tipo de Certificado:</label>
                                <label style="font-weight:1;"><?= $cert[0]['Alias']; ?></label>                                  
                            </div>
                        </div>    
                    <?php endif;
                endforeach;
                if ($tipo !== 'individual'): ?>                                      
                    <div class="row">
                            <div class="col-md-12">
                                <label>Evento:</label>
                                <label style="font-weight:1;">  <?= $evento[0]['eventoNome']; ?> </label>                                  
                            </div>
                    </div>   
                     <div class="row">    
                            <div class="col-md-6">
                                <label>Participantes no Evento:</label>
                                <label style="font-weight:1;"><?= $cont ?></label>                                  
                            </div>
                            <div class="col-md-6">
                                <label>Emails Cadastrados:</label>
                                <label style="font-weight:1;"><?= $contE; ?></label>                                  
                            </div>
                        </div>    
<?php 
endif;

?>
            </div>                 
      </form>                             
</div>

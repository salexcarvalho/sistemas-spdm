<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - CERTIFICADOS</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
            .tabelaEditavel {
                border:solid 1px;
                width:100%
            }
            .tabelaEditavel td {
                border:solid 1px;
            }
            .tabelaEditavel .celulaEmEdicao {
                padding: 0;
            }
            .tabelaEditavel .celulaEmEdicao input[type=text]{
                width:100%;
                border:0;
                background-color:rgb(255,253,210);  
            }
        </style>
    </head>
    <body>
        <?php
        $model_participante = new Model_Sec_Participantes();
        $model_origem = new Model_Sec_Origem();
        $model_tipo = new Model_Sec_Tipos();
        $model_certificado = new Model_Sec_Certificado();
        $model_setor = new Model_Login_Setor('user');
        ?>
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <?php if ($_SESSION['AcLiberaBtnAddPte'] == true) { ?>

                            <h1 class="page-header">Importar Participantes</h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-file-pdf-o"></i> Certificados</li>
                                <li><a href="home"><i class="fa fa-list"></i> Lista de participantes</a></li>
                                <li class="active"><i class="glyphicon glyphicon-file"></i> Importar Participantes</li>
                            </ol>
                            <div> 
                                <div id="msg" class="alert alert-success alert-danger" role="alert" hidden>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                                </div>
                                <table class="table table-striped table-bordered table-hover dataTable no-footer">
                                    <thead>
                                        <tr>
                                            <th>Nº</th>                                            
                                            <?php
                                            foreach ($titulos as $tt):
                                                echo '<th>' . utf8_encode($tt) . '</th>';
                                            endforeach;
                                            ?>
                                            <th>Tipo</th>
                                            <th>Origem</th>
                                            <?php  if ($_SESSION['Perfil'] == 'Administrador'):?>
                                            <th>idSetor</th>
                                            <?php endif; ?>    
                                            <th>Único ?</th>
                                        </tr>

                                        <tr> <th>Linhas</th>                                           
                                            <?php
                                            $i = 0;
                                            $tc = count($titulos);                                            
                                            foreach ($titulos as $tt):
                                                ?>
                                                <th>
                                                    <select id="colunas_<?= $i ?>" name="colunas_<?= $i ?>"  coluna="<?= $tt ?>" onchange="troca('colunas_<?= $i ?>');">
                                                        <option value="">Indefinido</option>
                                                        <?php foreach ($colunas as $cc2): ?>
                                                            <option coluna="<?= $tt ?>" value="<?= $cc2 ?>" <?php
                                                            if (in_array($cc2, $titulos) && ($cc2 == $tt)) {
                                                                echo "SELECTED";
                                                            }
                                                            ?>><?= $cc2 ?></option>
                                                                <?php endforeach; ?>                                                    
                                                    </select>
                                                </th>
                                                <?php
                                                $i++;
                                            endforeach;
                                            ?>

                                    <input type="hidden" id="titulos" name="titulos" value="<?= $tc ?>">
                                    <th>
                                        <select id="colunas_tipo_nome" name="colunas_<?= $i ?>" coluna="tipo_nome" onchange="carregar('colunas_tipo_nome');">
                                            <option value="">Indefinido</option>
                                            <?php
                                            $tipos = $model_tipo->select_tipos();
                                            foreach ($tipos as $tipo):
                                                ?>
                                                <option coluna="tipo_nome" campo="<?= $tipo['idTipos'] ?>" value="<?= $tipo['Nome'] ?>"><?= $tipo['Nome'] ?></option>
    <?php endforeach; ?>                                                    
                                        </select>
                                    </th>
    <?php $i++; ?>
                                    <th>

                                        <select id="colunas_origem_nome" name="colunas_<?= $i ?>" coluna="origem_nome" onchange="carregar('colunas_origem_nome');">
                                            <option value="">Indefinido</option>
                                            <?php
                                            $origens = $model_origem->select_origens();
                                            foreach ($origens as $origem):
                                                ?>
                                                <option coluna="origem_nome" campo="<?= $origem['idOrigem'] ?>" value="<?= $origem['Nome'] ?>"><?= $origem['Nome'] ?></option>
                                    <?php endforeach; ?>                                                    
                                        </select></th> 
                                    <?php if ($_SESSION['Perfil'] == 'Administrador'): 
                                        $i++;?>
                                       <th>

                                        <select id="colunas_idSetor" name="colunas_<?= $i ?>" coluna="idSetor" onchange="carregar('colunas_idSetor');">
                                            <option value="">Indefinido</option>
                                            <?php
                                            $setores = $model_setor->select_tipo();
                                            foreach ($setores as $setor):
                                                ?>
                                                <option coluna="idSetor" campo="<?= $setor['idTipoSetor'] ?>" value="<?= $setor['Setor'] ?>"><?= $setor['Setor'] ?></option>
                                    <?php endforeach; ?>                                                    
                                        </select></th> 
    <?php endif; ?>                
                                    <th>&nbsp;</th>      
                                    </tr>
                                    </thead>
                                    <tbody> 

                                        <?php
                                        $model_participantes = new Model_Sec_Participantes('default');
                                        $contador = 0;
                                        foreach ($linhas as $ln):
                                            $contador++;
                                            echo "<tr><td align='center'>$contador</td>";
                                            foreach ($titulos as $tt2):
                                                ?>
                                            <td class='edit' linha="<?= $contador ?>" id="<?= $tt2 ?>_<?= $contador ?>" name="<?= $tt2 ?>"><?= utf8_encode($ln[$tt2]); ?></td>
                                            <?php
                                        endforeach;
                                        ?> 
                                        <td linha="<?= $contador ?>" id="tipo_nome_<?= $contador ?>" name="tipo_nome"></td>
                                        <td linha="<?= $contador ?>" id="origem_nome_<?= $contador ?>" name="origem_nome"></td>
                                        <?php if ($_SESSION['Perfil'] == 'Administrador'): ?>
                                            <td linha="<?= $contador ?>" id="idSetor_<?= $contador ?>" name="idSetor"></td>
        <?php endif; ?>  
                                        <td align="center">
                                            <button type="button" id="valida_<?= $contador ?>" onclick="valida(<?= $contador; ?>);" class="btn btn-success btn-xs"><i class='glyphicon glyphicon-ok'></i></button>
                                        </td>

                                        <?php
                                        echo '</tr>';
                                    endforeach;
                                    ?>
                                    <input type="hidden" id="contador" name="contador" value="<?= $contador; ?>"> 
                                    <tr>
                                        <td colspan="<?php echo $tc + 5; ?>">
                                            <button type="button" class="btn btn-primary" onclick="gravar()">Gravar</button>
                                            <a href="home" class="btn btn-danger">Voltar</a>
                                        </td>
                                    </tr>   
                                    </form>
                                    </tbody>
                                </table>   
                            <?php } else { ?>
                                <h1 class="page-header"> Voce não possue acesso a está pagina, consulte o administrador.</h1>
<?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>
        <script>
            jQuery(document).ready(function () {
                // Remove seleção de ativo no menu.
                $('.nav li').removeClass('active');
                $('#side-menu li').removeClass('active');
                // Ativa botão no menu.
                $('#li-certificados ul').addClass('collapse in');
                $('#li-cad-participantes a').addClass('active');
                $('#li-certificados').addClass('active');
                $('#li-certificados a').addClass('collapse in');
                var ct = $("#contador").val();
                for (i = 1; i <= ct; i++) {
                    valida(i);
                }
            });
            function gravar() {
                var titulos = $("#titulos").val();
                var contador = $("#contador").val();
                var colunas = [];
                var data = new Object();
                var enviar = true;
                
                for (i = 0; i < titulos; i++) {
                        colunas[i] = $("#colunas_" + i + " option:selected").val();
                        if (colunas[i] == '') {
                            $('#msg').text('Uma ou mais colunas não tem parametro definido!!!');
                            $('#msg').show();
                            enviar = false;                            
                }
                if($("#colunas_tipo_nome option:selected").val()==""){
                    $('#msg').text('Paramentro Tipo é Obrigatorio');
                    $('#msg').show();
                    enviar = false; 
                }
                if($("#colunas_origem_nome option:selected").val()==""){
                    $('#msg').text('Paramentro Origem é Obrigatorio');
                    $('#msg').show();
                    enviar = false; 
                }
                <?php if($_SESSION['Perfil'] == 'Administrador'):?>
                if($("#colunas_idSetor option:selected").val()==""){
                    $('#msg').text('Paramentro Setor é Obrigatorio');
                    $('#msg').show();
                    enviar = false; 
                }
                <?php endif;?>
            }
            console.log(colunas);
            if (enviar) { 
                for (j = 1; j <= contador; j++) {
                    
                        data[j] = new Object();
                        for (i = 0; i < titulos; i++) {                       
                        var nome = $("#" + colunas[i] + "_" + j).attr('name');
                        var valor = $("#" + colunas[i] + "_" + j).text();
                        data[j][nome] = valor;
                        }
                        data[j]['idTipo'] = $("#tipo_nome_" + j).attr('campo');
                        data[j]['idOrigem'] = $("#origem_nome_" + j).attr('campo');
                        data[j]['idSetor'] = $("#idSetor_" + j).attr('campo');                  
                }
                console.log(data);
              
                    $.ajax({
                        url: 'import_insert',
                        type: 'POST',
                        dataType: "json",
                        data: data,
                        success: function (data) {                           
                                window.location.href = 'home';
                        }
                    });
                }

            }
            $(function () {
                $(".edit").dblclick(function () {
                    var conteudoOriginal = $(this).text();
                    var controle = $(this).attr('linha');

                    $(this).addClass("celulaEmEdicao");
                    $(this).html("<input type='text' value='" + conteudoOriginal + "' />");
                    $(this).children().first().focus();

                    $(this).children().first().keypress(function (e) {
                        if (e.which == 13) {
                            var novoConteudo = $(this).val();
                            $(this).parent().text(novoConteudo);
                            $(this).parent().removeClass("celulaEmEdicao");
                        }
                    });

                    $(this).children().first().blur(function () {
                        var manter = $(this).val();
                        $(this).parent().text(manter);
                        $(this).parent().removeClass("celulaEmEdicao");
                        valida(controle);
                    });
                });
            });
            function valida(cont) {
                var nome, email, coluna;
                var coluna = $("#colunas_" + cont + " option:selected").val();
                nome = $("#nome_" + cont).text();
                email = $("#email_" + cont).text();
                if ((nome != '') || (email != '')) {
                    $.ajax({
                        url: 'valida',
                        type: 'POST',
                        data: {nome: nome, email: email},
                        success: function (data) {
                            var retorno = jQuery.parseJSON(data);
                            if (retorno != null) {
                                if (retorno.Nome == nome) {
                                    $("#nome_" + cont).css("color", "red");
                                } else {
                                    $("#nome_" + cont).css("color", "");
                                }
                                if (retorno.Email == email) {
                                    $("#email_" + cont).css("color", "red");
                                } else {
                                    $("#email_" + cont).css("color", "");
                                }
                                $("#valida_" + cont).removeClass("btn-success");
                                $("#valida_" + cont + " i").removeClass("glyphicon-ok");
                                $("#valida_" + cont + " i").addClass("glyphicon-alert");
                                $("#valida_" + cont).addClass("btn-warning");
                            } else {
                                $("#email_" + cont).css("color", "");
                                $("#nome_" + cont).css("color", "");
                                $("#valida_" + cont).removeClass("btn-warning");
                                $("#valida_" + cont + " i").removeClass("glyphicon-alert");
                                $("#valida_" + cont + " i").addClass("glyphicon-ok");
                                $("#valida_" + cont).addClass("btn-success");
                            }
                        }
                    });
                }
            }
            function troca(obj) {
                var ant = $("#" + obj).attr('coluna');
                var novo = $("#" + obj).find('option:selected').val();
                var contador = $("#contador").val();
                for (i = 1; i <= contador; i++) {
                    $("#" + ant + "_" + i).attr("name", novo);
                    $("#" + ant + "_" + i).attr("id", novo + "_" + i);
                }

            }
            function carregar(obj) {
                var id = $("#" + obj).attr('coluna');
                var selectVal = $("#" + obj).find('option:selected').val();
                var campo = $("#" + obj).find('option:selected').attr('campo');
                var contador = $("#contador").val();
                for (i = 1; i <= contador; i++) {
                   $("#"+id+"_"+i).attr('campo',campo);
                   $("#"+id+"_"+i).text(selectVal);
                }

            }
        </script> 

    </body>
</html>
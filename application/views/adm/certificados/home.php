<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - CERTIFICADOS</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <?php
        $model_certificado = new Model_Sec_Certificado('default');
        $eventos = $model_certificado->select_certificados();
        ?>

        <!-- Modal -->
        <div class="modal fade" id="modal_excluir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Confirmar Exclusão</h4>
                    </div>
                    <div class="modal-body" id="modal_delete_participante">Deseja excluir este participante?</div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        <button type="button" class="btn btn-primary" id="modal_btn-excluir">Excluir</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal -->

        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>

            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Lista de Participantes</h1>
                        <ol class="breadcrumb">
                            <li><i class="fa fa-file-pdf-o"></i> Certificados</li>
                            <li class="active"><i class="fa fa-list"></i> Lista de participantes</li>
                        </ol>

                        <?php if (isset($warning)) : ?>
                            <div id="msg" class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong><?= $warning; ?></strong>
                            </div>
                        <?php endif ?>                          

                        <div class="row" >
                            <div class="col-md-4">
                                <div class="form-group">
                                    <form class="form-inline" method="get" action="home">                      
                                        <input  type="text" style="height: 30px; width: 260px;" class="form-control" id="Busca" name="Busca" placeholder="Informe o Nome ou ID">
                                        <input type="hidden" id="eventoB" name="eventoB" value="<?= $VerificaEvento ?>"/>
                                        <button type="submit" class="btn btn-danger btn-sm"><i class="glyphicon glyphicon-search"></i> Buscar</button>
                                    </form>
                                </div>    
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label style="margin-top: 5px;" for="idCertificados" class="col-md-2">Eventos:</label>
                                    <div class="col-md-3">
                                        <?php if ($_SESSION['AcLiberaBtnGeraCert'] == true) { ?>
                                            <select class="form-control" id="idCertificados" name="idCertificados" style="margin-left: -40px; height: 30px; width: 230px;" required>
                                                <option value="">Evento/Curso</option>
                                                <?php
                                                foreach($eventos as $evento):
                                                    ?>
                                                    <option <?= ($VerificaEvento == $evento['idCertificados']) ? 'selected' : ''; ?> value="<?= $evento['idCertificados']; ?>"><?= $evento['eventoNome']; ?></option>
                                                    <?php
                                                endforeach;
                                                ?>
                                            </select>
                                        <?php } ?>
                                    </div>
                                    <div class="col-md-7">                                     
                                        <button type="submit" idEvento='<?= $VerificaEvento ?>' Busca="<?= (isset($Busca)) ? $Busca : ''; ?>" name="btn_filtrar" class="btn btn-danger btn-sm"><i class="glyphicon glyphicon-search"></i> Filtrar</button>                                 
                                        <?php if ($_SESSION['Perfil'] == 'Administrador') { ?>
                                            <button type="button" id="btn_pdf_down" class="btn btn-primary btn-sm" name="btn_pdf_down"><i class="fa fa-download"></i>&nbsp;Baixar PDF</button>
                                            <button type="button" id="btn_enviar_email" class="btn btn-primary btn-sm" name="btn_enviar_email"><i class="glyphicon glyphicon-envelope"></i>&nbsp;Enviar PDF via Email</button>
                                       <?php } ?>    
                                    </div>        
                                </div>                                        
                            </div>    
                            <div class="col-md-2">
                                <?php if ($_SESSION['AcLiberaBtnAddPte'] == true) { ?>
                                <button type="button" id="btnAddPart" class="btn btn-primary btn-sm pull-right" name="btnAddPart" style="margin-left: 5px;"><i class="glyphicon glyphicon-plus-sign"></i>&nbsp;Adicionar</button>
                                <?php } ?>
                                <?php if ($_SESSION['AcLiberaBtnAddPte'] == true) { ?>
                                    <a href="import_participante">
                                        <button type="submit" id="btn_novo_import" class="btn btn-primary btn-sm pull-right" name="btn_novo_import"><i class="fa fa-upload"></i>&nbsp;Importar</button>
                                    </a>
                                <?php } ?>    
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div id="AlertaEvento" class="alert alert-danger alert-dismissable hidden">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    Por favor, selecione uma opção para continuar.
                                </div>
                            </div>    
                        </div>

                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="participante">
                                <thead>
                                    <tr>
                                        <th width="5%" style="text-align: center">ID</th>
                                        <th>Nome do Participante</th>
                                        <th width="11%">Tipo</th>
                                        <th width="25%">Evento</th>
                                        <th width="14%" style="text-align: center">Ações Adicionais</th>
                                        <th width="12%" style="text-align: center">Ações Participantes</th>
                                        <th width="5%" style="text-align: center">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $model_apoio = new Model_Sec_Apoio('default');
                                    $model_certificado = new Model_Sec_Certificado('default');
                                    $model_tipo = new Model_Sec_Tipos('default');
                                    foreach($data as $participante):
                                        $apoios = $model_apoio->select_apoio_participante($participante['idParticipante']);
                                        ?>
                                        <tr>
                                            <td style="text-align: center;vertical-align: middle;"><?= $participante['idParticipante']; ?></td>
                                            <td style="vertical-align: middle;"><?= $participante['titulacao']; ?>&nbsp;<?= $participante['Nome']; ?></td>
                                            <td style="vertical-align: middle;">
                                                <?php
                                                $cont = 0;												
						if(sizeof($apoios)):						
                                                foreach($apoios as $apoio):
                                                    if ($cont > 0) {
                                                        echo "<hr>";
                                                    }
                                                    $tipo = $model_tipo->select_tipo($apoio['idTipo']);
                                                    ?>
                                                    <input type="hidden" id="idTipo_<?= $cont; ?>_<?= $participante['idParticipante']; ?>" name="idTipo_<?= $cont; ?>" value="<?= $apoio['idTipo']; ?>"><?= $tipo[0]['Nome']; ?>
                                                    <?php
                                                    $cont++;
                                                endforeach;
                                                endif;
                                                ?>
                                            </td>
                                            <td style="vertical-align: middle;">
                                                <?php
                                                $cont = 0;
                                                if(sizeof($apoios)):
                                                foreach($apoios as $apoio):
                                                    if ($cont > 0) {
                                                        echo "<hr>";
                                                    }
                                                    $certificado = $model_certificado->select_certificado($apoio['idCertificado']);
                                                    ?>
                                                    <?= $certificado[0]['eventoNome']; ?>
                                                    <?php
                                                    $cont++;
                                                  endforeach;
                                                endif;
                                                ?>
                                            </td>
                                            <td style="text-align: center;vertical-align: middle;">
                                                <?php
                                                $cont = 0;
                                                if(sizeof($apoios)):
                                                foreach($apoios as $apoio):
                                                    if ($cont > 0) {
                                                        echo "<hr>";
                                                    }
                                                    ?>  
                                                    <?php if (!empty($apoio['idCertificado'])): ?>
                                                        <button type="submit" id="btnExcluirCertificado_<?= $apoio['idApoio']; ?>" idCertificados="<?= $apoio['idCertificado']; ?>" class="btn btn-danger btn-xs" CertUsuario="<?= $apoio['idCertificado']; ?>" idApoio="<?= $apoio['idApoio']; ?>" idParticipante="<?= $participante['idParticipante']; ?>" name="btnExcluirCertificado" title="Excluir Certificado" <?php if ($_SESSION['AcLiberaBtnGeraCert'] == NULL) { ?>disabled<?php } ?>>
                                                            <i id="acaoplus_<?= $apoio['idApoio']; ?>" class="fa fa-times-circle"></i>
                                                        </button>

                                                    <?php else: ?>
                                                        <button type="submit" id="btnGerarCertificado_<?= $apoio['idApoio']; ?>" class="btn btn-primary btn-xs" CertUsuario="<?= $apoio['idCertificado']; ?>" idApoio="<?= $apoio['idApoio']; ?>" idParticipante="<?= $participante['idParticipante']; ?>" name="btnGerarCertificado" title="Gerar Certificado" <?php if ($_SESSION['AcLiberaBtnGeraCert'] == NULL) { ?>disabled<?php } ?>>
                                                            <i id="acaoplus_<?= $apoio['idApoio']; ?>" class="fa fa-plus-circle"></i>
                                                        </button>

                                                    <?php endif; ?>
                                                    <button id="btn_enviar_emailI" idCertificado="<?= $apoio['idCertificado']; ?>" idApoio="<?= $apoio['idApoio']; ?>" autenticacao="<?= $apoio['Autenticacao']; ?>" class="btn btn-primary btn-xs" name="EnviarEmailI" title="Enviar Certificado por Email" <?php if ($participante['Email'] == '' || $apoio['idCertificado'] < 1 || $participante['Status'] == 0) { ?>disabled<?php } ?>>
                                                        <i class="fa fa-envelope-o"></i>
                                                    </button>

                                                    <button type="button" id="btnGeraPDF_<?= $apoio['idApoio']; ?>" idCertificado="<?= $apoio['idCertificado']; ?>" idApoio="<?= $apoio['idApoio']; ?>" autenticacao="<?= $apoio['Autenticacao']; ?>" class="btn btn-success btn-xs" name="btnGeraPDF" title="Ver o certificado" <?php if ($apoio['idCertificado'] < 1) { ?>disabled<?php } ?>>
                                                        <i class="fa fa-file-pdf-o"></i>
                                                    </button>

                                                    <button type="button" id="btnBaixaPDF_<?= $apoio['idApoio']; ?>" autenticacao="<?= $apoio['Autenticacao']; ?>" class="btn btn-default btn-xs" name="btnBaixaPDF" title="Baixar o certificado" <?php if ($apoio['idCertificado'] == 0) { ?>disabled<?php } ?>>
                                                        <i class="fa fa-download"></i>
                                                    </button>

                                                    <button type="button" id="btnEditarComp" class="btn btn-default btn-xs" name="btnEditarComp" idParticipante="<?= $participante['idParticipante']; ?>" idApoio="<?= $apoio['idApoio']; ?>" page="<?= $PaginaAtual; ?>" title="Editar Complemento" <?php if ($_SESSION['AcLiberaBtnEdiPte'] == NULL) { ?>disabled<?php } ?>>
                                                        <i class="glyphicon glyphicon-edit"></i>
                                                    </button>

                                                    <button type="button" id="btnExcluir" class="btn btn-default btn-xs" name="btnExcluir" page="<?= $PaginaAtual; ?>" idParticipante="<?= $participante['idParticipante']; ?>" idApoio="<?= $apoio['idApoio']; ?>" nmParticipante="<?= $tipo[0]['Nome']; ?>" <?php if ($_SESSION['AcLiberaBtnExcPte'] == NULL) { ?>disabled<?php } ?> data-toggle="modal" data-target="#modal_excluir" data-placement="bottom" title="Excluir Complemento" data-delay="1">
                                                        <i class="glyphicon glyphicon-trash"></i>
                                                    </button>
                                                    <?php
                                                    $cont++;
                                                   endforeach;
                                                endif;
                                                ?>
                                            </td>
                                            <td id="id_<?= $participante['idParticipante']; ?>" style="text-align: center;vertical-align: middle;">
                                            
                                                <button id="btnAddComp" class="btn btn-default btn-xs" name="btnAddComp" page="<?= $PaginaAtual; ?>" idParticipante="<?= $participante['idParticipante']; ?>" title="Adicionar Complemento" <?php if ($_SESSION['AcLiberaBtnVizPte'] == NULL) { ?>disabled<?php } ?>>
                                                    <i class="glyphicon glyphicon-plus"></i>
                                                </button>                                            

                                                <button id="btnEditarPart" class="btn btn-default btn-xs" name="btnEditarPart" page="<?= $PaginaAtual; ?>" idParticipante="<?= $participante['idParticipante']; ?>" title="Editar Participante" <?php if ($_SESSION['AcLiberaBtnEdiPte'] == NULL) { ?>disabled<?php } ?>>
                                                    <i class="glyphicon glyphicon-edit"></i>
                                                </button>                                     
                                        
                                                <button id="btnExcluirPart" class="btn btn-default btn-xs" name="btnExcluirPart" page="<?= $PaginaAtual; ?>" idParticipante="<?= $participante['idParticipante']; ?>" nmParticipante="<?= $participante['Nome']; ?>" <?php if ($_SESSION['AcLiberaBtnExcPte'] == NULL) { ?>disabled<?php } ?> >
                                                    <i class="glyphicon glyphicon-trash"></i>
                                                </button> 
                                                  
                                            </td>
                                            <td style="text-align: center;vertical-align: middle;">       
                                                <?php if ($participante['Status'] == "0"): ?>
                                                    <button id="btnAtivar_<?= $participante['idParticipante']; ?>" class="btn btn-danger btn-xs" idParticipante="<?= $participante['idParticipante']; ?>" name="btnAtivar" title="Ativar Participante" <?php if ($_SESSION['AcLiberaBtnAtivPte'] == NULL) { ?>disabled<?php } ?>>
                                                        <i id="activ_<?= $participante['idParticipante']; ?>" class="glyphicon glyphicon-ban-circle"></i>
                                                    </button>
                                                <?php else: ?>
                                                    <button id="btnAtivar_<?= $participante['idParticipante']; ?>" class="btn btn-success btn-xs" idParticipante="<?= $participante['idParticipante']; ?>" name="btnAtivar" title="Destivar Participante" <?php if ($_SESSION['AcLiberaBtnAtivPte'] == NULL) { ?>disabled<?php } ?>>
                                                        <i id="activ_<?= $participante['idParticipante']; ?>" class="glyphicon glyphicon-ok"></i>
                                                    </button>
                                                <?php endif; ?>
                                            </td>
                                        </tr>                              
				<?php endforeach;?>
				</tbody>    
                            </table>
                            <?php include(kohana::find_file('views/paginator', 'paginacao', 'php')) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>

        <script>
            jQuery(document).ready(function () {

                // Remove seleção de ativo no menu.
                $('.nav li').removeClass('active');
                $('#side-menu li').removeClass('active');
                // Ativa botão no menu.
                $('#li-certificados ul').addClass('collapse in');
                $('#li-cad-participantes a').addClass('active');
                $('#li-certificados').addClass('active');
                $('#li-certificados a').addClass('collapse in');              
         
            $("body").on("click", "button[name=btnExcluirPart]", function () {
                    var id = $(this).attr('idParticipante');
                    var url = "delete_participante";
                    var page = $(this).attr('page');
                    var nmParticipante = $(this).attr('nmParticipante');
                    var params = {
                        url: "modal_remover?page="+page+"&url="+url+"&id="+id+"&nmParticipante="+nmParticipante,
                        title: 'Excluir Participante',
                        size: eModal.size.lg,
                        buttons: [
                            {text: 'EXCLUIR', style: 'primary', close: false, type: 'submit', form: 'formExcluir', id:'salvar'},
                            {text: 'FECHAR', style: 'danger', close: true}
                        ]
                    };
                    return eModal.ajax(params);
                });   
            });
        </script>
        <script src="<?= URL::base(); ?>theme/backend/js/certificados.js"></script>
    </body>
</html>
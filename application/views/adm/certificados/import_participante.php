<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - CERTIFICADOS</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <?php if ($_SESSION['AcLiberaBtnAddPte'] == true) { ?>

                            <h1 class="page-header">Importar Participantes</h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-file-pdf-o"></i> Certificados</li>
                                <li><a href="home"><i class="fa fa-list"></i> Lista de participantes</a></li>
                                <li class="active"><i class="glyphicon glyphicon-file"></i> Importar Participantes</li>
                            </ol>
                            <div class="form-group">
                                <form enctype='multipart/form-data' action='import_participante_open' method='post'>    
                                    <div class="col-lg-9">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <span>Upload Arquivo(.csv)</span>                                              
                                            </div>
                                            <div class="panel-body">                                       
                                                <label for="nmParticipante" class="col-lg-2 control-label">Arquivo:</label>
                                                <span class="col-md-8">Para realização do importe adequado certifique-se que o Certificado foi criado antes de importar os participantes e baixe o arquivo modelo para criação do csv a ser utilizado <a href="<?= URL::base(); ?>download/modelocsv.xls" target="_blank">aqui!</a></span>
                                                <div class="col-lg-10">
                                                    <input size='50' type='file' name='participantes' />
                                                </div> 
                                            </div>
                                            <div class="panel-footer">
                                                <button type="submit" class="btn btn-primary">Upload</button>
                                                <a href="home" class="btn btn-danger">Voltar</a>                                               
                                            </div>
                                        </div>                           
                                    </div>
                                </form> 
                            </div>
                        </div>    
                    <?php } else { ?>                    
                        <h1 class="page-header"> Voce não possue acesso a está pagina, consulte o administrador.</h1>
                    <?php } ?>
                </div>
            </div>
        </div>
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>
        <script>
            jQuery(document).ready(function () {
                // Remove seleção de ativo no menu.
                $('.nav li').removeClass('active');
                $('#side-menu li').removeClass('active');
                // Ativa botão no menu.
                $('#li-certificados ul').addClass('collapse in');
                $('#li-cad-participantes a').addClass('active');
                $('#li-certificados').addClass('active');
                $('#li-certificados a').addClass('collapse in');
            });
        </script>        
    </body>
</html>
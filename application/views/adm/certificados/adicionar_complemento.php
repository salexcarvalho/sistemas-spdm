<form class="form-horizontal" role="form" method="POST" action="insert_complemento" name="FormCompAdd" id="FormCompAdd">  
    <input hidden id="page" name="page" value="<?= $page; ?>">
    <input hidden id="idParticipante" name="idParticipante" placeholder="" value="<?= $dadosParticipante[0]['idParticipante']; ?>">
    <div class="form-group">
        <div class="col-md-12">
            <div class="form-group">
                <label for="nmParticipante" class="col-md-1 control-label">Titulação</label>
                <div class="col-md-2">                                                                                                 
                    <input type="text" min="0" class="form-control" id="titulacao" disabled name="" placeholder="" value="<?= $dadosParticipante[0]['titulacao']; ?>">                                                    
                </div>
            
                <label for="nmParticipante" class="col-md-1 control-label">Nome</label>
                <div class="col-md-8">
                    <input type="text" min="0" class="form-control" id="" disabled name="" placeholder="" value="<?= $dadosParticipante[0]['Nome']; ?>">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-1 control-label">Origem</label>
                <div class="col-md-5">
                    <select class="form-control" id="idOrigem" name="idOrigem" required >
                        <option value="">Selecione uma opção</option>
                        <?php
                        $model_origem = new Model_Sec_Origem('default');
                        $origem = $model_origem->select_origens();
                        foreach ($origem as $origens) {
                            ?>
                            <option  value="<?= $origens['idOrigem']; ?>"><?= $origens['Nome']; ?></option>
    <?php } ?>
                        </select>
                    </div>                       
                    <label class="col-md-1 control-label">Tipo</label>
                    <div class="col-md-5">
                        <select class="form-control" id="idTipo" name="idTipo" required >
                            <option value="">Selecione uma opção</option>
                            <?php
                            $model_tipos = new Model_Sec_Tipos('default');
                            $tipos = $model_tipos->select_tipos();
                            foreach ($tipos as $tipo) {
                                ?>
                                <option value="<?= $tipo['idTipos']; ?>"><?= $tipo['Nome']; ?></option>
    <?php } ?>
                    </select>
                </div>
            </div>  
            <div class="form-group">
                <label for="modulo" class="col-md-1 control-label">Módulo</label>
                <div class="col-md-11">
                    <input type="text" class="form-control" id="modulo" name="modulo" placeholder="" >
                </div>
            </div>                                        
            <div class="form-group">
                <label for="tema" class="col-md-1 control-label">Tema</label>
                <div class="col-md-11">
                    <input type="text" class="form-control" id="tema" name="tema" placeholder="" >
                </div>
            </div>
            <div class="form-group">
                <label for="data" class="col-md-1 control-label">Data</label>
                <div class="col-md-11">
                    <input type="text" class="form-control" id="data" data-mask='99/99/9999' name="data">
                </div>
            </div> 
            <div class="form-group">
                <label for="livre" class="col-md-1 control-label">Livre</label>
                <div class="col-md-11">
                    <input type="text" class="form-control" id="livre" name="livre" placeholder="Informe datas e ou complementos necessários separadas por ','" >
                </div>
            </div>                                         
        </div>
    </div>
</form>
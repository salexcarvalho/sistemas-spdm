<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - CERTIFICADOS</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

    </head>
    <body>

        <!-- Modal -->
        <div class="modal fade" id="modal_excluir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Excluir Assinatura de Evento</h4>
                    </div>
                    <div class="modal-body" id="modal_delete_assinatura">Deseja excluir esta Assinatura de Evento?</div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        <button type="button" class="btn btn-primary" id="modal_btn-excluir">Excluir</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal -->
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Lista de Assinaturas</h1>
                        <ol class="breadcrumb">
                            <li><i class="fa fa-file-pdf-o"></i> Assinaturas</li>
                            <li class="active"><i class="fa fa-list"></i> Listagem de Assinaturas</li>
                        </ol>

                        <?php if (isset($warning)) : ?>
                            <div id="msg" class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong><?= $warning; ?></strong>
                            </div>
                        <?php endif ?>                        
                        <div class="row">
                            <?php if ($_SESSION['AcLiberaBtnAtivAss'] == TRUE) { ?>
                                <div class="col-lg-12">
                                    <button type="button" id="btn_novo_assinatura" class="pull-right btn btn-primary btn-md" name="btn_novo_assinatura"><i class="glyphicon glyphicon-plus-sign"></i>&nbsp;&nbsp;Adicionar Assinatura</button>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="Assinaturas">
                                <thead>
                                    <tr>
                                        <th  width="50px" style="text-align: center">#</th>
                                        <th>Tratamento</th>
                                        <th>Nome</th>
                                        <th width="20%">cargo</th>
                                        <th width="15%">PNG</th>
                                        <th width="8%" style="text-align: center">Ações</th>
                                        <th width="5%"  style="text-align: center">status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    //  echo "<pre>";var_dump($data);exit;
                                    foreach ($data as $assinatura) {
                                        ?>
                                        <tr>
                                            <td align="center"><?= $assinatura['idAssinaturas']; ?></td>
                                            <td><?= $assinatura['titulacao']; ?></td>
                                            <td><?= $assinatura['nome']; ?></td>
                                            <td><?= $assinatura['cargo']; ?></td>
                                            <td><img src="<?= URL::base(); ?>upload/img/certificados/<?= $assinatura['PngAssinatura']; ?>" width="80" alt=""/></td>
                                            <td align="center">
                                            <input hidden id="page" name="page" value="<?= $PaginaAtual; ?>">
                                            <button  type="button" id="btnEditar" class="btn btn-default btn-xs" name="btnEditar" idAssinaturas="<?= $assinatura['idAssinaturas']; ?>" title="Editar Assinatura" <?php if ($_SESSION['AcLiberaBtnEdiAss'] == NULL) { ?>disabled<?php } ?>>
                                                <i class="glyphicon glyphicon-edit"></i>
                                            </button>                                       
                                            <button  type="button" id="btnExcluir" class="btn btn-default btn-xs" name="btnExcluir" idAssinaturas="<?= $assinatura['idAssinaturas']; ?>" nmAssinatura="<?= $assinatura['nome']; ?>" <?php if ($_SESSION['AcLiberaBtnExcAss'] == NULL) { ?>disabled<?php } ?> data-toggle="modal" data-target="#modal_excluir" data-placement="bottom" title="Excluir Assinatura" data-delay="1">
                                                <i class="glyphicon glyphicon-trash"></i>
                                            </button>
                                            </td>
                                            <td align="center">
                                                <form class="form-horizontal" role="form" method="POST" action="update_ativar_assinatura">
                                                    <input type="hidden" name="idAssinaturas" value="<?= $assinatura['idAssinaturas']; ?>">
                                                    <?php
                                                    if ($assinatura['status'] == "0") {
                                                        ?>
                                                        <button type="submit" id="btnAtivar" class="btn btn-danger btn-xs" name="btnAtivar" title="Ativar Tipo de Assinatura" <?php if ($_SESSION['AcLiberaBtnAtivAss'] == NULL) { ?>disabled<?php } ?>>
                                                            <i class="glyphicon glyphicon-ban-circle"></i>
                                                            <input type="hidden" name="status" value="1">
                                                        </button>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <button type="submit" id="btnAtivar" class="btn btn-success btn-xs" name="btnAtivar" title="Destivar Tipo de Assinatura" <?php if ($_SESSION['AcLiberaBtnAtivAss'] == NULL) { ?>disabled<?php } ?>>
                                                            <i class="glyphicon glyphicon-ok"></i>
                                                            <input type="hidden" name="status" value="0">
                                                        </button>
                                                        <?php
                                                    }
                                                    ?>
                                                </form>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                            <?php include(kohana::find_file('views/paginator', 'paginacao', 'php')) ?> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>

        <script>
            jQuery(document).ready(function () {
                // Remove seleção de ativo no menu.
                $('.nav li').removeClass('active');
                $('#side-menu li').removeClass('active');
                // Ativa botão no menu.
                $('#li-certificados ul').addClass('collapse in');
                $('#li-assinatura a').addClass('active');
                $('#li-certificados').addClass('active');
                $('#li-certificados a').addClass('collapse in');
            });
        </script>

        <script>
            jQuery(document).ready(function ()
            {
                $('#Assinaturas').dataTable(
                        {
                            "aoColumnDefs": [{"bSortable": false, "aTargets": [4, 5]}],
                            "paging": false,
                            "info": false,
                            "bFilter": false,
                            "aaSorting": [[0, 'desc']],
                            "oLanguage":
                                    {
                                        "sSearch": " Buscar: "
                                    }
                        });

                var idAssinaturas;
                var nmAssinatura;
                $('body').on("click", 'button[name=btnExcluir]', function ()
                {
                    idAssinaturas = $(this).attr('idAssinaturas');
                    nmAssinatura = $(this).attr('nmAssinatura');
                    $('#modal_delete_assinatura').html("Deseja excluir a assinatura <b>\"" + nmAssinatura + "\"</b> ?");
                });

                $('#modal_excluir').on('hidden.bs.modal', function ()
                {
                    window.location.replace("home");
                });

                $('#modal_btn-excluir').on("click", function ()
                {
                    var btn = $(this);
                    btn.text('loading');

                    btn.removeClass('btn-primary');
                    btn.removeClass('btn-danger');
                    btn.removeClass('btn-success');
                    btn.addClass('btn-primary');

                    $('#modal_btn-excluir').prop('disabled', true);

                    $.ajax("delete_assinatura",
                            {
                                type: "POST",
                                data:
                                        {
                                            idAssinaturas: idAssinaturas
                                        }
                            })
                            .done(function (data)
                            {
                                $('#modal_delete_assinatura').html('Assinatura excluída com sucesso');

                                btn.removeClass('btn-primary');
                                btn.addClass('btn-success');
                            })
                            .fail(function ()
                            {
                                $('#modal_delete_assinatura').html('Erro: ' + data);

                                btn.removeClass('btn-primary');
                                btn.addClass('btn-danger');
                            })
                            .always(function ()
                            {
                                btn.text('Excluído');
                            });
                });            

                $('.dataTables_filter label').addClass('pull-right');

                $("#btn_novo_assinatura").click(function () {
                    var page = $("#page").val();
                    var params = {
                        url: "modal_assinatura?page="+page,
                        title: 'Cadastrar Assinatura',
                        size: eModal.size.md,
                        buttons: [
                            
                            {text: 'GRAVAR', style: 'primary', close: false, type: 'submit', form: 'formAssinatura', id:'salvar'},
                            {text: 'FECHAR', style: 'danger', close: true}
                        ]
                    };
                    return eModal.ajax(params);
                });
                
                 $('body').on("click", 'button[name=btnEditar]', function () {
                    var idAssinaturas = $(this).attr('idAssinaturas');
                    var editar = 0;
                    var page = $("#page").val();
                    var params = {
                        url: "modal_assinatura?page="+page+"&idAssinatura="+idAssinaturas+"&editar="+editar,
                        title: 'Editar Assinatura',
                        size: eModal.size.md,
                        buttons: [                            
                            {text: 'SALVAR', style: 'primary', close: false, type: 'submit', form: 'formAssinatura', id:'salvar'},
                            {text: 'FECHAR', style: 'danger', close: true}
                        ]
                    };
                    return eModal.ajax(params);
                });
                $('body').on("click", 'button[name=btnVisualizar]', function () {
                    var idAssinaturas = $(this).attr('idAssinaturas');
                    var editar = 1;
                    var page = $("#page").val();
                    var params = {
                        url: "modal_assinatura?page="+page+"&idAssinatura="+idAssinaturas+"&editar="+editar,
                        title: 'Visualizar Assinatura',
                        size: eModal.size.md,
                        buttons: [
                            {text: 'FECHAR', style: 'danger', close: true}                          
                        ]
                    };
                    return eModal.ajax(params);
                });
            });
        </script>        

    </body>
</html>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - CERTIFICADOS</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

    </head>
    <body>

        <!-- Modal -->
        <div class="modal fade" id="modal_excluir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Excluir Origem de Evento</h4>
                    </div>
                    <div class="modal-body" id="modal_delete_origem">Deseja excluir esta Origem de Evento?</div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        <button type="button" class="btn btn-primary" id="modal_btn-excluir">Excluir</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal -->
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Lista de Origens de Eventos</h1>
                        <ol class="breadcrumb">
                            <li><i class="fa fa-file-pdf-o"></i> Certificados</li>
                            <li class="active"><i class="fa fa-list"></i> Listagem de Origens de Eventos</li>
                        </ol>
                        <?php if (isset($warning)) : ?>
                            <div id="msg" class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong><?= $warning; ?></strong>
                            </div>
                        <?php endif ?>                        
                        <div class="row">                     
                                <div class="col-lg-12">                           
                                        <button type="button" id="btn_novo_origem" class="pull-right btn btn-primary btn-md" name="btn_novo_origem"><i class="glyphicon glyphicon-plus-sign"></i>&nbsp;&nbsp;Adicionar Origem</button>
                                </div>
                        </div>
                     <br>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="Origem">
                                <thead>
                                    <tr>
                                        <th width="4%" style="text-align: center">#</th>
                                        <th width="40%">Nome</th>
                                        <th width="40%">Alias</th>
                                        <th style="text-align: center">Ações</th>
                                        <th style="text-align: center">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($data as $origem) {
                                        ?>
                                        <tr>
                                            <input hidden id="page" name="page" value="<?= $PaginaAtual; ?>">
                                            <td align="center"><?= $origem['idOrigem']; ?></td>
                                            <td><?= $origem['Nome']; ?></td>
                                            <td><?= $origem['Alias']; ?></td>
                                            <td align="center"> 
                                                <input type="hidden" id="idOrigem_<?= $origem['idOrigem']; ?>" name="idOrigem" value="<?= $origem['idOrigem']; ?>">
                                                <button  type="button" id="btnEditar" class="btn btn-default btn-xs" name="btnEditar" idOrigem="<?= $origem['idOrigem']; ?>" title="Editar Tipo de Participante">
                                                        <i class="glyphicon glyphicon-edit"></i>
                                                    </button>
                                               
                                                <button  type="button" id="btnExcluir" class="btn btn-default btn-xs" name="btnExcluir" idOrigem="<?= $origem['idOrigem']; ?>" nmOrigem="<?= $origem['Nome']; ?>" data-toggle="modal" data-target="#modal_excluir" data-placement="bottom" title="Excluir Tipo de Participante" data-delay="1">
                                                    <i class="glyphicon glyphicon-trash"></i>
                                                </button>
                                            </td>
                                            <td align="center">

                                                <input type="hidden" name="idOrigem" value="<?= $origem['idOrigem']; ?>">
                                                <?php
                                                if ($origem['Status'] == "0") {
                                                    ?>
                                                    <button type="submit" id="btnAtivar_<?= $origem['idOrigem']; ?>" class="btn btn-danger btn-xs" idOrigem="<?= $origem['idOrigem']; ?>" name="btnAtivar" title="Ativar Tipo de Origem">
                                                        <i id="activ_<?= $origem['idOrigem']; ?>" class="glyphicon glyphicon-ban-circle"></i>

                                                    </button>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <button type="submit" id="btnAtivar_<?= $origem['idOrigem']; ?>" class="btn btn-success btn-xs" idOrigem="<?= $origem['idOrigem']; ?>" name="btnAtivar" title="Destivar Tipo de Origem">
                                                        <i id="activ_<?= $origem['idOrigem']; ?>" class="glyphicon glyphicon-ok"></i>

                                                    </button>
                                                    <?php
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                            <?php include(kohana::find_file('views/paginator', 'paginacao', 'php')) ?> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>

        <script>
            jQuery(document).ready(function () {
                // Remove seleção de ativo no menu.
                $('.nav li').removeClass('active');
                $('#side-menu li').removeClass('active');
                // Ativa botão no menu.
                $('#li-certificados ul').addClass('collapse in');
                $('#li-cad-tiposorigem a').addClass('active');
                $('#li-certificados').addClass('active');
                $('#li-certificados a').addClass('collapse in');
            });
        </script>

        <script>
            jQuery(document).ready(function ()
            {
                $('#origensEventos').dataTable(
                        {
                            "aoColumnDefs": [{"bSortable": false, "aTargets": [4, 5]}],
                            "paging": false,
                            "info": false,
                            //"aaSorting": [[0, 'desc']],
                            "bFilter": false,
                            "oLanguage":
                                    {
                                        "sSearch": " Buscar: "
                                    }
                        });

                var idOrigem;
                var nmOrigem;
                $('body').on("click", 'button[name=btnExcluir]', function ()
                {
                    idOrigem = $(this).attr('idOrigem');
                    nmOrigem = $(this).attr('nmOrigem');
                    $('#modal_delete_origem').html("Deseja excluir a Origem de Evento <b>\"" + nmOrigem + "\"</b> ?");
                });

                $("body").on("click", "button[name=btnAtivar]", function () {
                    idOrigem = $(this).attr('idOrigem');
                    //alert(txtUsuario);
                    $.get('update_ativar_origem?idOrigem=' + idOrigem, function (data) {

                        if (data === "1") {
                            $('#btnAtivar_' + idOrigem).addClass('btn btn-success btn-xs');
                            $('#btnAtivar_' + idOrigem).removeClass('btn-danger');
                            $('#activ_' + idOrigem).removeClass('glyphicon-ban-circle');
                            $('#activ_' + idOrigem).addClass('glyphicon-ok');
                        } else {
                            $('#btnAtivar_' + idOrigem).addClass('btn btn-danger btn-xs');
                            $('#btnAtivar_' + idOrigem).removeClass('btn-success');
                            $('#activ_' + idOrigem).addClass('glyphicon-ok');
                            $('#activ_' + idOrigem).addClass('glyphicon-ban-circle');
                        }
                    });
                });

                $('#modal_excluir').on('hidden.bs.modal', function ()
                {
                    window.location.replace("home");
                });

                $('#modal_btn-excluir').on("click", function ()
                {
                    var btn = $(this);
                    btn.text('loading');

                    btn.removeClass('btn-primary');
                    btn.removeClass('btn-danger');
                    btn.removeClass('btn-success');
                    btn.addClass('btn-primary');

                    $('#modal_btn-excluir').prop('disabled', true);

                    $.ajax("delete_origem",
                            {
                                type: "POST",
                                data:
                                        {
                                            idOrigem: idOrigem
                                        }
                            })
                            .done(function (data)
                            {
                                $('#modal_delete_origem').html('Origem de Evento excluída com sucesso');

                                btn.removeClass('btn-primary');
                                btn.addClass('btn-success');
                            })
                            .fail(function ()
                            {
                                $('#modal_delete_origem').html('Erro: ' + data);

                                btn.removeClass('btn-primary');
                                btn.addClass('btn-danger');
                            })
                            .always(function ()
                            {
                                btn.text('Excluído');
                            });
                });

                $("#btn_novo_origem").click(function () {
                    var page = $("#page").val();
                    var params = {
                        url: "modal_origem?page="+page,
                        title: 'Cadastrar Origem',
                        size: eModal.size.md,
                        buttons: [
                             {text: 'SALVAR', style: 'primary', close: false, type: 'submit', form: 'formOrigem', id:'salvar'},
                             {text: 'FECHAR', style: 'danger', close: true}
                           
                        ]
                    };
                    return eModal.ajax(params);
                });
                
                 $('body').on("click", 'button[name=btnEditar]', function () {
                    var idOrigem = $(this).attr('idOrigem');
                    var editar = 0;
                    var page = $("#page").val();
                    var params = {
                        url: "modal_origem?page="+page+"&idOrigem="+idOrigem+"&editar="+editar,
                        title: 'Editar Origem',
                        size: eModal.size.md,
                        buttons: [
                           {text: 'SALVAR', style: 'primary', close: false, type: 'submit', form: 'formOrigem', id:'salvar'},
                           {text: 'FECHAR', style: 'danger', close: true}
                        ]
                    };
                    return eModal.ajax(params);
                });
                $('body').on("click", 'button[name=btnVisualizar]', function () {
                    var idOrigem = $(this).attr('idOrigem');
                    var editar = 1;
                    var page = $("#page").val();
                    var params = {
                         url: "modal_origem?page="+page+"&idOrigem="+idOrigem+"&editar="+editar,
                        title: 'Visualizar Origem',
                        size: eModal.size.md,
                        buttons: [
                            {text: 'FECHAR', style: 'danger', close: true}                          
                        ]
                    };
                    return eModal.ajax(params);
                });
                $('.dataTables_filter label').addClass('pull-right');
            });
        </script>


    </body>
</html>
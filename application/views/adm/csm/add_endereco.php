<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?= $_SESSION['company_name'] ?> - CSM</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <script>
            jQuery(document).ready(function ()
            {
                // Mostra e esconde selects da origem do endereço
                $('#f-g-disci').hide();
                $('#f-g-espe').hide();
                $('#tipoEntidade').change(function () {
                    if ($('#tipoEntidade').val() === '1') {
                        $('#f-g-dpt').show();
                        $('#f-g-disci').hide();
                        $('#f-g-espe').hide();
                    } else if ($('#tipoEntidade').val() === '2') {
                        $('#f-g-dpt').hide();
                        $('#f-g-disci').show();
                        $('#f-g-espe').hide();
                    } else if ($('#tipoEntidade').val() === '3') {
                        $('#f-g-dpt').hide();
                        $('#f-g-disci').hide();
                        $('#f-g-espe').show();
                    }
                });
            });
        </script>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->

            <div id="page-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-header">Cadastrar Endereço</h1>
                        <ol class="breadcrumb">
                            <li><a href="index.html"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                            <li class="active"><i class="fa fa-edit"></i> Forms</li>
                        </ol>
                        <form class="form-horizontal" role="form">
                            <div class="form-group">
                                <label for="tipoEntidade" class="col-lg-2 control-label">Selecione o tipo de entidade que está alocada neste endereço</label>
                                <div class="col-lg-5">
                                    <select id="tipoEntidade" class="form-control">
                                        <option value="1">Departamento</option>
                                        <option value="3">Especialidade</option>
                                    </select>
                                </div>
                            </div>
                            <div id="f-g-dpt" class="form-group">
                                <label for="dpt" class="col-lg-2 control-label">Selecione o Departamento que está alocado neste endereço</label>
                                <div class="col-lg-5">
                                    <select id="dpt" class="form-control">
                                        <option value="1">Medicina</option>
                                        <option value="2">Enfermagem</option>
                                    </select>
                                </div>
                            </div>
                            <div id="f-g-espe" class="form-group">
                                <label for="especialidade" class="col-lg-2 control-label">Selecione a Especialidade que está alocada neste endereço</label>
                                <div class="col-lg-5">
                                    <select id="especialidade" class="form-control">
                                        <option value="1">Medicina</option>
                                        <option value="3">Enfermagem</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="logradrouro" class="col-lg-2 control-label">Logradrouro</label>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control" id="logradrouro" name="logradrouro" placeholder="Nome do Logradrouro">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="numero" class="col-lg-2 control-label">Número</label>
                                <div class="col-lg-5">
                                    <input type="number" min="0" class="form-control" id="numero" name="numero" placeholder="Número do endereço">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="CEP" class="col-lg-2 control-label">CEP</label>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control" id="CEP" name="CEP" placeholder="Número CEP do endereço">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="UF" class="col-lg-2 control-label">UF</label>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control" id="uf" name="uf" placeholder="Unidade Federativa do endereço">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="cidade" class="col-lg-2 control-label">Cidade</label>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control" id="cidade" name="cidade" placeholder="Nome da Cidade">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-5">
                                    <button type="submit" class="btn btn-primary">Cadastrar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>    
        </div>
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>
    </body>
</html>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - CSM</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script>
            jQuery(document).ready(function ()
            {
                // Remove seleção de ativo no menu.
                $('.navbar-nav li').removeClass('active');
                // Ativa botão no menu.
                //$('.navbar-nav #mn_home').addClass('active');

                //Ao inicialiar a página verifica se existem checkbox 24hrs marcados e caso sim desabilita os imputs dos horários respectivos à este.
                for (var v = 1; v < 8; v++)
                {
                    if ($('#24horas' + v).prop('checked'))
                    {
                        $('#horarioin' + v).val('00:00');
                        $('#horariofi' + v).val('00:00');
                        $('#horarioin' + v).prop('readonly', true);
                        $('#horariofi' + v).prop('readonly', true);
                    } else if (($('#horarioin' + v).val('')) && (($('#horariofi' + v).val(''))))
                    {
                        $('#horarioin' + v).val('00:00');
                        $('#horariofi' + v).val('00:00');
                        $('#horarioin' + v).prop('readonly', true);
                        $('#horariofi' + v).prop('readonly', true);
                    }
                }
                ;

                if ($('#msg').is(':visible'))
                {
                    $('#ok').css('display', 'block');
                    $('#btncadastrar').css('display', 'none')
                    $('#back').css('display', 'none');
                } else {
                    $('#ok').css('display', 'none');
                }

                // Validação 24h ao alterar o checkbox
                $('#24horas1').change(function () {
                    if ($('#24horas1').prop('checked'))
                    {
                        $('#horarioin1').val('00:00');
                        $('#horariofi1').val('00:00');
                        $('#horarioin1').prop('readonly', true);
                        $('#horariofi1').prop('readonly', true);

                    } else
                    {
                        $('#horarioin1').prop('readonly', false);
                        $('#horariofi1').prop('readonly', false);
                    }
                });
                $('#24horas2').change(function ()
                {
                    if ($('#24horas2').prop('checked'))
                    {
                        $('#horarioin2').val('00:00');
                        $('#horariofi2').val('00:00');
                        $('#horarioin2').prop('readonly', true);
                        $('#horariofi2').prop('readonly', true);

                    } else
                    {
                        $('#horarioin2').prop('readonly', false);
                        $('#horariofi2').prop('readonly', false);
                    }
                });
                $('#24horas3').change(function ()
                {
                    if ($('#24horas3').prop('checked'))
                    {
                        $('#horarioin3').val('00:00');
                        $('#horariofi3').val('00:00');
                        $('#horarioin3').prop('readonly', true);
                        $('#horariofi3').prop('readonly', true);

                    } else
                    {
                        $('#horarioin3').prop('readonly', false);
                        $('#horariofi3').prop('readonly', false);
                    }
                });
                $('#24horas4').change(function ()
                {
                    if ($('#24horas4').prop('checked'))
                    {
                        $('#horarioin4').val('00:00');
                        $('#horariofi4').val('00:00');
                        $('#horarioin4').prop('readonly', true);
                        $('#horariofi4').prop('readonly', true);

                    } else
                    {
                        $('#horarioin4').prop('readonly', false);
                        $('#horariofi4').prop('readonly', false);
                    }
                });
                $('#24horas5').change(function ()
                {
                    if ($('#24horas5').prop('checked'))
                    {
                        $('#horarioin5').val('00:00');
                        $('#horariofi5').val('00:00');
                        $('#horarioin5').prop('readonly', true);
                        $('#horariofi5').prop('readonly', true);

                    } else
                    {
                        $('#horarioin5').prop('readonly', false);
                        $('#horariofi5').prop('readonly', false);
                    }
                });
                $('#24horas6').change(function ()
                {
                    if ($('#24horas6').prop('checked'))
                    {
                        $('#horarioin6').val('00:00');
                        $('#horariofi6').val('00:00');
                        $('#horarioin6').prop('readonly', true);
                        $('#horariofi6').prop('readonly', true);

                    } else
                    {
                        $('#horarioin6').prop('readonly', false);
                        $('#horariofi6').prop('readonly', false);
                    }
                });
                $('#24horas7').change(function ()
                {
                    if ($('#24horas7').prop('checked'))
                    {
                        $('#horarioin7').val('00:00');
                        $('#horariofi7').val('00:00');
                        $('#horarioin7').prop('readonly', true);
                        $('#horariofi7').prop('readonly', true);

                    } else
                    {
                        $('#horarioin7').prop('readonly', false);
                        $('#horariofi7').prop('readonly', false);
                    }
                });
                // Fim de valicação 24h
            });
        </script>
    </head>
    <body>
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="page-header">Editar Serviço</h2>
                        <ol class="breadcrumb">
                            <li><i class="fa fa-list"></i> Listagens</li>
                            <li><a href="lista"><i class="fa fa-stethoscope"></i> Serviços</a></li>
                            <li class="active"><i class="fa fa-file"></i> Editar Dados do Serviço</li>
                        </ol>
                        <div class="col-sm-12">
                            <?php if (isset($warning)) : ?>
                                <div class="alert alert-success" role="alert"><?= $warning; ?></div>
                            <?php endif ?>
                            <?php if (isset($errors)) : ?>
                                <div style="padding: 15px;" class="alert alert-danger">Alguns erros foram encontrados, por favor confira os dados que você inseriu.
                                    <ul class="errors">
                                        <?php foreach ($errors as $message): ?>
                                            <li><?php echo $message; ?></li>
                                        <?php endforeach ?>
                                    </ul></div>
                            <?php endif ?>
                        </div>
                        <form class="form-horizontal" role="form" method="POST" action="update_servico" enctype="multipart/form-data">
                            <!-- Inicio do Painel Dados do Serviço -->
                            <div class="row">
                                <div class="col-lg-12">
                                    <!-- Inicio do Painel Endereço -->
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            Dados do Serviço
                                        </div>
                                        <div class="panel-body">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="nmServico" class="col-lg-2 control-label">Nome</label>
                                                    <div class="col-lg-10">
                                                        <input type="text" class="form-control" id="nmServico" name="nmServico" placeholder="" value="<?= $servico[0]['nmServico']; ?>">
                                                    </div>
                                                </div>
                                                <div id="f-g-disci" class="form-group">
                                                    <label for="Especialidade" class="col-lg-2 control-label">Especialidade</label>
                                                    <div class="col-lg-10">
                                                        <select id="idEspecialidade" name="idEspecialidade" class="form-control" >
                                                            <option value=NULL>Nenhum</option>
                                                            <?php
                                                            foreach ($especialidades as $especialidade) {
                                                                ?>
                                                                <option 
                                                                <?php
                                                                foreach ($especialidades_servico as $especialidade_servico) {
                                                                    if (($especialidade_servico['idEspecialidade'] == $especialidade['idEspecialidade']) AND ( $servico[0]['idServico'] == $especialidade_servico['idServico'])) {
                                                                        echo ' selected ';
                                                                    }
                                                                }
                                                                ?>
                                                                    value="<?= $especialidade['idEspecialidade']; ?>"><?= $especialidade['nmEspecialidade']; ?></option>
                                                                    <?php
                                                                }
                                                                ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="dsServico" class="col-lg-2 control-label">Descrição</label>
                                                    <div class="col-lg-10">
                                                        <textarea style="resize: vertical;" class="form-control" rows="3" id="dsServico" name="dsServico" placeholder=""><?= $servico[0]['dsServico']; ?></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="dsDoenca" class="col-lg-2 control-label">O que é a Doença?</label>
                                                    <div class="col-lg-10">
                                                        <textarea style="resize: vertical;" class="form-control" rows="3" id="dsDoenca" name="dsDoenca" placeholder=""><?= $servico[0]['dsDoenca']; ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label for="chefeDep" class="col-lg-2 control-label">Chefe</label>
                                                    <div class="col-lg-10">
                                                        <select class="form-control" id="chefeDep" name="chefeDep" required>
                                                            <option></option>
                                                            <?php
                                                            foreach ($nmfuncionarios as $funcionario) {
                                                                ?>
                                                                <option <?= ($servico[0]['idFuncionarioChefe'] == $funcionario['idFuncionario']) ? 'selected' : ''; ?> value="<?= $funcionario['idFuncionario']; ?>"><?= $funcionario['nmFuncionario']; ?></option>
                                                                <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="nuCentroCusto" class="col-lg-2 control-label">Centro Custo</label>
                                                    <div class="col-lg-10">
                                                        <input type="text" class="form-control" id="nuCentroCusto" name="nuCentroCusto" placeholder="" value="<?= $servico[0]['nuCentroCusto']; ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="dsTratamento" class="col-lg-2 control-label">Diagnóstico e Tratamento</label>
                                                    <div class="col-lg-10">
                                                        <textarea style="resize: vertical;" class="form-control" rows="3" id="dsTratamento" name="dsTratamento" placeholder=""><?= $servico[0]['dsTratamento']; ?></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="formaAcesso" class="col-lg-2 control-label">Forma de acesso</label>
                                                    <div class="col-lg-10">
                                                        <textarea style="resize: vertical;" class="form-control" rows="3" id="formaAcesso" name="formaAcesso" placeholder=""><?= $servico[0]['formaAcesso']; ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> <!-- Fim do Painel Endereço -->
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <!-- Inicio do Painel Endereço -->
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            Endereço do Serviço
                                        </div>
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <label for="endLogradouro" class="col-lg-2 control-label">Rua</label>
                                                <div class="col-lg-7">
                                                    <input type="text" class="form-control" id="endLogradouro" name="endLogradouro" placeholder="" value="<?= $servico[0]['endLogradouro']; ?>">
                                                </div>

                                                <label for="nuLogradouro" class="col-lg-1 control-label">Número</label>
                                                <div class="col-lg-2">
                                                    <input type="number" min="0" class="form-control" id="nuLogradouro" name="nuLogradouro" placeholder="" value="<?= $servico[0]['nuLogradouro']; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="complemento" class="col-lg-2 control-label">Complemento</label>
                                                <div class="col-lg-7">
                                                    <input type="text" class="form-control" id="complemento" name="complemento" placeholder="" value="<?= $servico[0]['complemento']; ?>">
                                                </div>

                                                <label for="CEP" class="col-lg-1 control-label">CEP</label>
                                                <div class="col-lg-2">
                                                    <input type="text" class="form-control" id="CEP" name="CEP" placeholder="" value="<?= $servico[0]['CEP']; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="bairro" class="col-lg-2 control-label">Bairro</label>
                                                <div class="col-lg-10">
                                                    <input type="text" class="form-control" id="bairro" name="bairro" placeholder="" value="<?= $servico[0]['bairro']; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="cidade" class="col-lg-2 control-label">Cidade</label>
                                                <div class="col-lg-7">
                                                    <input type="text" class="form-control" id="cidade" name="cidade" placeholder="" value="<?= $servico[0]['cidade']; ?>">
                                                </div>
                                                <label for="estado" class="col-lg-1 control-label">Estado</label>
                                                <div class="col-lg-2">
                                                    <select class="form-control" id="estado" name="estado" required>
                                                        <option><?= $servico[0]['estado'] ?></option>
                                                        <option value="AC" id="AC" name="AC">AC</option>
                                                        <option value="AL" id="AL" name="AL">AL</option>
                                                        <option value="AM" id="AM" name="AM">AM</option>
                                                        <option value="AP" id="AP" name="AP">AP</option>
                                                        <option value="BA" id="BA" name="BA">BA</option>
                                                        <option value="CE" id="CE" name="CE">CE</option>
                                                        <option value="DF" id="DF" name="DF">DF</option>
                                                        <option value="ES" id="ES" name="ES">ES</option>
                                                        <option value="GO" id="GO" name="GO">GO</option>
                                                        <option value="MA" id="MA" name="MA">MA</option>
                                                        <option value="MG" id="MG" name="MG">MG</option>
                                                        <option value="MS" id="MS" name="MS">MS</option>
                                                        <option value="MT" id="MT" name="MT">MT</option>
                                                        <option value="PA" id="PA" name="PA">PA</option>
                                                        <option value="PB" id="PB" name="PB">PB</option>
                                                        <option value="PE" id="PE" name="PE">PE</option>
                                                        <option value="PI" id="PI" name="PI">PI</option>
                                                        <option value="RJ" id="RJ" name="RJ">RJ</option>
                                                        <option value="RN" id="RN" name="RN">RN</option>
                                                        <option value="RO" id="RO" name="RO">RO</option>
                                                        <option value="RR" id="RR" name="RR">RR</option>
                                                        <option value="RS" id="RS" name="RS">RS</option>
                                                        <option value="SC" id="SC" name="SC">SC</option>
                                                        <option value="SE" id="SE" name="SE">SE</option>                                
                                                        <option value="SP" id="SP" name="SP">SP</option>
                                                        <option value="TO" id="TO" name="TO">TO</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-12"><br></div>
                                            <div class="col-lg-12"><br></div>
                                        </div>
                                    </div> <!-- Fim do Painel Endereço -->
                                </div>
                                <div class="col-lg-6">
                                    <!-- Inicio do Painel Contato -->    
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            Contato
                                            <div class="checkbox pull-right">
                                                <span class="bold">Mostrar contato no site:</span>
                                                <label><input type="radio" name="mostrarContatoNoSite" value="S" <?= ($servico[0]['mostrarContatoNoSite'] == 'S') ? 'checked' : ''; ?>>SIM</label>
                                                <label><input type="radio" name="mostrarContatoNoSite" value="N" <?= ($servico[0]['mostrarContatoNoSite'] == 'N') ? 'checked' : ''; ?>>NÃO</label>
                                            </div>
                                        </div>
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <label for="telefone" class="col-lg-2 control-label">Telefone:</label>
                                                <div class="col-lg-7">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                                        <input type="tel" class="form-control" id="telefone" name="telefone" placeholder="" value="<?= $servico[0]['telefone']; ?>">
                                                    </div>
                                                </div>
                                                <label for="nuVOIP" class="col-lg-1 control-label">VOIP:</label>
                                                <div class="col-lg-2">
                                                    <input type="number" min="0" class="form-control" id="nuVOIP" name="nuVOIP" placeholder="" value="<?= $servico[0]['nuVOIP']; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">        
                                                <label for="email" class="col-lg-2 control-label">E-mail:</label>
                                                <div class="col-lg-10">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                                        <input type="email" class="form-control" id="email" name="email" placeholder="" value="<?= $servico[0]['email']; ?>">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="site" class="col-lg-2 control-label">Site</label>
                                                <div class="col-lg-10">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-globe"></i></span>
                                                        <input type="text" class="form-control" id="site" name="site" placeholder="" value="<?= $servico[0]['site']; ?>">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="facebook" class="col-lg-2 control-label">Facebook</label>
                                                <div class="col-lg-5">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-facebook"></i></span>
                                                        <input type="text" class="form-control" id="facebook" name="facebook" placeholder="" value="<?= $servico[0]['facebook']; ?>">
                                                    </div>
                                                </div>

                                                <label for="twitter" class="col-lg-1 control-label">Twitter</label>
                                                <div class="col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-twitter"></i></span>
                                                        <input type="text" class="form-control" id="twitter" name="twitter" placeholder="" value="<?= $servico[0]['twitter']; ?>">
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="form-group">
                                                <label for="googleplus" class="col-lg-2 control-label">Google+</label>
                                                <div class="col-lg-5">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-google-plus"></i></span>
                                                        <input type="text" class="form-control" id="googleplus" name="googleplus" placeholder="" value="<?= $servico[0]['googleplus']; ?>">
                                                    </div>
                                                </div>
                                                <label for="linkedIn" class="col-lg-1 control-label">LinkedIn</label>
                                                <div class="col-lg-4">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-linkedin"></i></span>
                                                        <input type="text" class="form-control" id="linkedIn" name="linkedIn" placeholder="" value="<?= $servico[0]['linkedIn']; ?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- Fim do Painel Contato -->
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <!-- Inicio do Painel Horário de Atendimento -->
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            Horário de Atendimento
                                        </div>
                                        <!-- .panel-heading -->
                                        <div class="panel-body">

                                            <table class="table table-responsive">
                                                <thead>
                                                    <tr>
                                                        <th></th>
                                                        <th width="13%">&nbsp;&nbsp;&nbsp;&nbsp;Domingo</th>
                                                        <th width="13%">&nbsp;&nbsp;&nbsp;&nbsp;Segunda</th>
                                                        <th width="13%">&nbsp;&nbsp;&nbsp;&nbsp;Terça</th>
                                                        <th width="13%">&nbsp;&nbsp;&nbsp;&nbsp;Quarta</th>
                                                        <th width="13%">&nbsp;&nbsp;&nbsp;&nbsp;Quinta</th>
                                                        <th width="13%">&nbsp;&nbsp;&nbsp;&nbsp;Sexta</th>
                                                        <th width="13%">&nbsp;&nbsp;&nbsp;&nbsp;Sábado</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td><div class="form-group-sm">
                                                                <label class="col-lg-12">&nbsp</label>

                                                                <label class="col-lg-12 control-label">Início</label>
                                                                <label class="col-lg-12 control-label">Fim</label>
                                                            </div></td>
                                                        <td><div class="form-group-sm">

                                                                <div class="col-lg-12">
                                                                    <div class="checkbox">
                                                                        <label>
                                                                            <input type="hidden" id="DOM" name="DOM" value="<?php
                                                                            foreach ($horarios_atendimento as $horarioatd) {
                                                                                if ($horarioatd['diaSemana'] == 'DOM' && $horarioatd['idServico'] == $servico[0]['idServico']) {
                                                                                    echo $horarioatd['idHorarioAtendimento'];
                                                                                }
                                                                            }
                                                                            ?>">                                                                         
                                                                            <input id="24horas1" name="24horas1" type="checkbox" value="S"  
                                                                            <?php
                                                                            foreach ($horarios_atendimento as $horarioatd) {
                                                                                if ($horarioatd['diaSemana'] == 'DOM' && $horarioatd['horario24Horas'] == 'S') {
                                                                                    echo 'checked';
                                                                                }
                                                                            }
                                                                            ?>>24 hs
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group-sm">
                                                                <div class="col-lg-12">
                                                                    <input type="time" required class="form-control" id="horarioin1" name="horarioin1" placeholder="Horário de Início"  
                                                                    <?php
                                                                    foreach ($horarios_atendimento as $horarioatd) {
                                                                        if ($horarioatd['diaSemana'] == 'DOM' && !($horarioatd['horario24Horas'] == 'S')) {
                                                                            echo 'value="' . $horarioatd['horarioInicio'] . '"';
                                                                        }
                                                                    }
                                                                    ?>>
                                                                </div>
                                                                <div class="col-lg-12">
                                                                    <input type="time" required class="form-control" id="horariofi1" name="horariofi1" placeholder="Horário de Término"  
                                                                    <?php
                                                                    foreach ($horarios_atendimento as $horarioatd) {
                                                                        if ($horarioatd['diaSemana'] == 'DOM' && !($horarioatd['horario24Horas'] == 'S')) {
                                                                            echo 'value="' . $horarioatd['horarioFim'] . '"';
                                                                        }
                                                                    }
                                                                    ?>>
                                                                </div> 
                                                            </div></td>
                                                        <td><div class="form-group-sm">
                                                                <div class="col-lg-12">
                                                                    <div class="checkbox">
                                                                        <label>
                                                                            <input type="hidden" id="SEG" name="SEG" value="<?php
                                                                    foreach ($horarios_atendimento as $horarioatd) {
                                                                        if ($horarioatd['diaSemana'] == 'SEG' && $horarioatd['idServico'] == $servico[0]['idServico']) {
                                                                            echo $horarioatd['idHorarioAtendimento'];
                                                                        }
                                                                    }
                                                                    ?>">                                                                             
                                                                            <input id="24horas2" name="24horas2" type="checkbox" value="S"  
                                                                                   <?php
                                                                                   foreach ($horarios_atendimento as $horarioatd) {
                                                                                       if ($horarioatd['diaSemana'] == 'SEG' && $horarioatd['horario24Horas'] == 'S') {
                                                                                           echo 'checked';
                                                                                       }
                                                                                   }
                                                                                   ?>>24 hs
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group-sm">
                                                                <div class="col-lg-12">
                                                                    <input type="time" required class="form-control" id="horarioin2" name="horarioin2" placeholder="Horário de Início"  
                                                                           <?php
                                                                           foreach ($horarios_atendimento as $horarioatd) {
                                                                               if ($horarioatd['diaSemana'] == 'SEG' && !($horarioatd['horario24Horas'] == 'S')) {
                                                                                   echo 'value="' . $horarioatd['horarioInicio'] . '"';
                                                                               }
                                                                           }
                                                                           ?>>
                                                                </div>
                                                                <div class="col-lg-12">
                                                                    <input type="time" required class="form-control" id="horariofi2" name="horariofi2" placeholder="Horário de Término"  
                                                                           <?php
                                                                           foreach ($horarios_atendimento as $horarioatd) {
                                                                               if ($horarioatd['diaSemana'] == 'SEG' && !($horarioatd['horario24Horas'] == 'S')) {
                                                                                   echo 'value="' . $horarioatd['horarioFim'] . '"';
                                                                               }
                                                                           }
                                                                           ?>>
                                                                </div> 
                                                            </div></td>
                                                        <td><div class="form-group-sm">
                                                                <div class="col-lg-12">
                                                                    <div class="checkbox">
                                                                        <label>
                                                                            <input type="hidden" id="TER" name="TER" value="<?php
                                                                            foreach ($horarios_atendimento as $horarioatd) {
                                                                                if ($horarioatd['diaSemana'] == 'TER' && $horarioatd['idServico'] == $servico[0]['idServico']) {
                                                                                    echo $horarioatd['idHorarioAtendimento'];
                                                                                }
                                                                            }
                                                                            ?>"> 
                                                                            <input id="24horas3" name="24horas3" type="checkbox" value="S"  
<?php
foreach ($horarios_atendimento as $horarioatd) {
    if ($horarioatd['diaSemana'] == 'TER' && $horarioatd['horario24Horas'] == 'S') {
        echo 'checked';
    }
}
?>>24 hs
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group-sm">

                                                                <div class="col-lg-12">
                                                                    <input type="time" required class="form-control" id="horarioin3" name="horarioin3" placeholder="Horário de Início"  
<?php
foreach ($horarios_atendimento as $horarioatd) {
    if ($horarioatd['diaSemana'] == 'TER' && !($horarioatd['horario24Horas'] == 'S')) {
        echo 'value="' . $horarioatd['horarioInicio'] . '"';
    }
}
?>>
                                                                </div>

                                                                <div class="col-lg-12">
                                                                    <input type="time" required class="form-control" id="horariofi3" name="horariofi3" placeholder="Horário de Término"  
<?php
foreach ($horarios_atendimento as $horarioatd) {
    if ($horarioatd['diaSemana'] == 'TER' && !($horarioatd['horario24Horas'] == 'S')) {
        echo 'value="' . $horarioatd['horarioFim'] . '"';
    }
}
?>>
                                                                </div> 
                                                            </div></td>
                                                        <td><div class="form-group-sm">
                                                                <div class="col-lg-12">
                                                                    <div class="checkbox">
                                                                        <label>
                                                                            <input type="hidden" id="QUA" name="QUA" value="<?php
                                                                            foreach ($horarios_atendimento as $horarioatd) {
                                                                                if ($horarioatd['diaSemana'] == 'QUA' && $horarioatd['idServico'] == $servico[0]['idServico']) {
                                                                                    echo $horarioatd['idHorarioAtendimento'];
                                                                                }
                                                                            }
                                                                            ?>"> 
                                                                            <input id="24horas4" name="24horas4" type="checkbox" value="S"  
<?php
foreach ($horarios_atendimento as $horarioatd) {
    if ($horarioatd['diaSemana'] == 'QUA' && $horarioatd['horario24Horas'] == 'S') {
        echo 'checked';
    }
}
?>>24 hs
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group-sm">
                                                                <div class="col-lg-12">
                                                                    <input type="time" required class="form-control" id="horarioin4" name="horarioin4" placeholder="Horário de Início"  
                                                                    <?php
                                                                    foreach ($horarios_atendimento as $horarioatd) {
                                                                        if ($horarioatd['diaSemana'] == 'QUA' && !($horarioatd['horario24Horas'] == 'S')) {
                                                                            echo 'value="' . $horarioatd['horarioInicio'] . '"';
                                                                        }
                                                                    }
                                                                    ?>>
                                                                </div>
                                                                <div class="col-lg-12">
                                                                    <input type="time" required class="form-control" id="horariofi4" name="horariofi4" placeholder="Horário de Término"  
<?php
foreach ($horarios_atendimento as $horarioatd) {
    if ($horarioatd['diaSemana'] == 'QUA' && !($horarioatd['horario24Horas'] == 'S')) {
        echo 'value="' . $horarioatd['horarioFim'] . '"';
    }
}
?>>
                                                                </div> 
                                                            </div></td>
                                                        <td><div class="form-group-sm">
                                                                <div class="col-lg-12">
                                                                    <div class="checkbox">
                                                                        <label>
                                                                            <input type="hidden" id="QUI" name="QUI" value="<?php
                                                                            foreach ($horarios_atendimento as $horarioatd) {
                                                                                if ($horarioatd['diaSemana'] == 'QUI' && $horarioatd['idServico'] == $servico[0]['idServico']) {
                                                                                    echo $horarioatd['idHorarioAtendimento'];
                                                                                }
                                                                            }
?>">                                                                             
                                                                            <input id="24horas5" name="24horas5" type="checkbox" value="S"  
                                                                    <?php
                                                                    foreach ($horarios_atendimento as $horarioatd) {
                                                                        if ($horarioatd['diaSemana'] == 'QUI' && $horarioatd['horario24Horas'] == 'S') {
                                                                            echo 'checked';
                                                                        }
                                                                    }
                                                                    ?>>24 hs
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group-sm">
                                                                <div class="col-lg-12">
                                                                    <input type="time" required class="form-control" id="horarioin5" name="horarioin5" placeholder="Horário de Início"  
                                                                    <?php
                                                                           foreach ($horarios_atendimento as $horarioatd) {
                                                                               if ($horarioatd['diaSemana'] == 'QUI' && !($horarioatd['horario24Horas'] == 'S')) {
                                                                                   echo 'value="' . $horarioatd['horarioInicio'] . '"';
                                                                               }
                                                                           }
                                                                           ?>>
                                                                </div>
                                                                <div class="col-lg-12">
                                                                    <input type="time" required class="form-control" id="horariofi5" name="horariofi5" placeholder="Horário de Término"  
                                                                            <?php
                                                                                   foreach ($horarios_atendimento as $horarioatd) {
                                                                                       if ($horarioatd['diaSemana'] == 'QUI' && !($horarioatd['horario24Horas'] == 'S')) {
                                                                                           echo 'value="' . $horarioatd['horarioFim'] . '"';
                                                                                       }
                                                                                   }
                                                                                   ?>>
                                                                </div> 
                                                            </div></td>
                                                        <td><div class="form-group-sm">
                                                                <div class="col-lg-12">
                                                                    <div class="checkbox">
                                                                        <label>
                                                                            <input type="hidden" id="SEX" name="SEX" value="<?php
                                                                            foreach ($horarios_atendimento as $horarioatd) {
                                                                                if ($horarioatd['diaSemana'] == 'SEX' && $horarioatd['idServico'] == $servico[0]['idServico']) {
                                                                                    echo $horarioatd['idHorarioAtendimento'];
                                                                                }
                                                                            }
                                                                                   ?>">                                                                             
                                                                            <input id="24horas6" name="24horas6" type="checkbox" value="S"  
                                                                    <?php
                                                                    foreach ($horarios_atendimento as $horarioatd) {
                                                                        if ($horarioatd['diaSemana'] == 'SEX' && $horarioatd['horario24Horas'] == 'S') {
                                                                            echo 'checked';
                                                                        }
                                                                    }
                                                                    ?>>24 hs
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group-sm">
                                                                <div class="col-lg-12">
                                                                    <input type="time" required class="form-control" id="horarioin6" name="horarioin6" placeholder="Horário de Início"  
<?php
foreach ($horarios_atendimento as $horarioatd) {
    if ($horarioatd['diaSemana'] == 'SEX' && !($horarioatd['horario24Horas'] == 'S')) {
        echo 'value="' . $horarioatd['horarioInicio'] . '"';
    }
}
?>>
                                                                </div>
                                                                <div class="col-lg-12">
                                                                    <input type="time" required class="form-control" id="horariofi6" name="horariofi6" placeholder="Horário de Término"  
                                                                            <?php
                                                                            foreach ($horarios_atendimento as $horarioatd) {
                                                                                if ($horarioatd['diaSemana'] == 'SEX' && !($horarioatd['horario24Horas'] == 'S')) {
                                                                                    echo 'value="' . $horarioatd['horarioFim'] . '"';
                                                                                }
                                                                            }
                                                                            ?>>
                                                                </div> 
                                                            </div></td>
                                                        <td><div class="form-group-sm">
                                                                <div class="col-lg-12">
                                                                    <div class="checkbox">
                                                                        <label>
                                                                            <input type="hidden" id="SAB" name="SAB" value="<?php
                                                                    foreach ($horarios_atendimento as $horarioatd) {
                                                                        if ($horarioatd['diaSemana'] == 'SAB' && $horarioatd['idServico'] == $servico[0]['idServico']) {
                                                                            echo $horarioatd['idHorarioAtendimento'];
                                                                        }
                                                                    }
                                                                    ?>">                                                                             
                                                                            <input id="24horas7" name="24horas7" type="checkbox" value="S"  
<?php
foreach ($horarios_atendimento as $horarioatd) {
    if ($horarioatd['diaSemana'] == 'SAB' && $horarioatd['horario24Horas'] == 'S') {
        echo 'checked';
    }
}
?>>24 hs
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group-sm">
                                                                <div class="col-lg-12">
                                                                    <input type="time" required class="form-control" id="horarioin7" name="horarioin7" placeholder="Horário de Início"  
<?php
foreach ($horarios_atendimento as $horarioatd) {
    if ($horarioatd['diaSemana'] == 'SAB' && !($horarioatd['horario24Horas'] == 'S')) {
        echo 'value="' . $horarioatd['horarioInicio'] . '"';
    }
}
?>>
                                                                </div>
                                                                <div class="col-lg-12">
                                                                    <input type="time" required class="form-control" id="horariofi7" name="horariofi7" placeholder="Horário de Término"  
                                            <?php
                                            foreach ($horarios_atendimento as $horarioatd) {
                                                if ($horarioatd['diaSemana'] == 'SAB' && !($horarioatd['horario24Horas'] == 'S')) {
                                                    echo 'value="' . $horarioatd['horarioFim'] . '"';
                                                }
                                            }
                                            ?>>
                                                                </div> 
                                                            </div></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div><!-- Fim Horarios de Atendimento -->
                                    <!-- Inicio Fotos -->
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            Fotos
                                        </div>
                                        <!-- .panel-heading -->
                                        <div class="panel-body">
<?php
for ($f = 1; $f <= 4; $f++) {
    if ((isset($imagem_servico[$f])) && (($imagem_servico[$f]["nmImagem"] != "") || ($imagem_servico[$f]['nmImagem'] != NULL))) {
        ?>
                                                    <div class="col-lg-3">    
                                                        <label class="col-lg-12"><?php echo 'Foto ' . $f; ?></label>
                                                        <img src="<?= URL::base() ?><?= $imagem_servico[$f]['caminhoImagem'] . $imagem_servico[$f]['nmImagem']; ?>" title="Foto 1" class="img-thumbnail" width="220">
                                                        <input type="hidden" name="idImagem<?php echo $f; ?>" id="idImagem<?php echo $f; ?>" value="<?= $imagem_servico[$f]['idImagem']; ?>" >
                                                        <input type="file" name="ft<?php echo $f; ?>" id="ft<?php echo $f; ?>" >
                                                        <input type="text"  name="dsft<?php echo $f; ?>" id="dsft<?php echo $f; ?>" class="form-control"/>
                                                    </div><?php } else { ?>
                                                    <div class="col-lg-3">    
                                                        <label class="col-lg-12"><?php echo 'Foto ' . $f; ?></label>
                                                        <input type="file" name="ft<?php echo $f; ?>" id="ft<?php echo $f; ?>" >
                                                        <input type="text"  name="dsft<?php echo $f; ?>" id="dsft<?php echo $f; ?>" class="form-control"/>
                                                    </div>    
    <?php
    }
}
?>    
                                        </div> <!-- Fim Fotos -->
                                    </div>    
                                    <!-- Inicio dos Sites Ref -->
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            Sites de Referência
                                        </div>
                                        <!-- .panel-heading -->
                                        <div class="panel-body">
                                            <?php
                                            $s = 0;
                                            foreach ($sites as $site) {

                                                $s ++;
                                                ?>
                                                <div class="col-lg-6">
                                                    <label class="col-lg-2 control-label">Empresa</label>
                                                    <div class=" col-lg-10">
                                                        <input class="form-control" type = "text" name = "Empresa<?php echo $s; ?>" value="<?= $site['nmEmpresa'] ?>">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label class="col-lg-2 control-label">Site</label> 
                                                    <div class=" col-lg-10">
                                                        <input class="form-control" type = "text" name = "site<?php echo $s; ?>" value="<?= $site['urlSite'] ?>">
                                                        <input class="form-control" type = "hidden" name = "idSite<?php echo $s; ?>" value="<?= $site['idSiteReferencia'] ?>">
                                                    </div>
                                                </div>    
    <?php
    //}
}
?>
                                        </div>
                                    </div><!-- Fim dos Sites Ref -->
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <input type="hidden" id="idServico" name="idServico" value="<?= $servico[0]['idServico']; ?>">
                                <input type="hidden" id="idContato" name="idContato" value="<?= $servico[0]['idContato']; ?>">
                                <input type="hidden" id="idEndereco" name="idEndereco" value="<?= $servico[0]['idEndereco']; ?>">
                                <input type="hidden" id="tipoContato" name="tipoContato" value="Serviço">
                                <button type="submit" class="btn btn-primary">Atualizar</button>
                                <a href="home">
                                    <button type="button" id="back" class="btn btn-danger" name="back">Voltar</button>
                                </a>
                            </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>
</body>
</html>
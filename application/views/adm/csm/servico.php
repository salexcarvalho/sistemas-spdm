<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - CSM</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <!-- Modal deletar servico -->
        <div class="modal fade" id="modal_excluir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Excluir Serviço</h4>
                    </div>
                    <div class="modal-body" id="modal_delete_servico">Deseja excluir este serviço ?</div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        <button type="button" class="btn btn-primary" id="modal_btn-excluir">Excluir</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal deletar servico -->
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Lista de Serviços</h1>
                        <ol class="breadcrumb">
                            <li><i class="fa fa-list"></i> Listagens</li>
                            <li class="active"><i class="fa fa-stethoscope"></i> Serviços</li>
                        </ol>
                        <?php if (isset($warning)) : ?>
                            <div class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <?= $warning; ?></div>
                        <?php endif ?>
                        <?php if (isset($errors)) : ?>
                            <div style="padding: 15px;" class="alert alert-danger">Alguns erros foram encontrados, por favor confira os dados que você inseriu.
                                <ul class="errors">
                                    <?php foreach ($errors as $message): ?>
                                        <li><?php echo $message; ?></li>
                                    <?php endforeach ?>
                                </ul></div>
                        <?php endif ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <span>Serviço</span>
                                <a href="cadastro_servico">
                                    <button type="submit" id="btn_novo_servico" class="pull-right btn btn-primary btn-xs" name="btn_novo_servico"><i class="glyphicon glyphicon-plus-sign"></i>  Adicionar Novo</button>
                                </a>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover" id="servicos">
                                        <thead>
                                            <tr>
                                                <th width="60" style="text-align: center">#</th>
                                                <th width="400" >Nome do Serviço</th>
                                                <th width="260" >Especialidade</th>
                                                <th width="300" >Nome do Chefe</th>
                                                <th width="145" style="text-align: center">Data de Cadastro</th>
                                                <th width="100" style="text-align: center">Ações</th>
                                                <th width="40" style="text-align: center">Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            foreach ($servicos as $servico) {
                                                ?>
                                                <tr>
                                                    <td align="center"><?= $servico['idServico']; ?></td>
                                                    <td><?= $servico['nmServico']; ?></td>
                                                    <td><?= $servico['nmEspecialidade']; ?></td>
                                                    <td><?= $servico['nmFuncionarioChefe']; ?></td>
                                                    <td align="center"><?= strftime('%d/%m/%Y - %H:%M', strtotime($servico['dtCadastro'])); ?></td>
                                                    <td align="center">
                                                        <form style="display: inline-block;" role="form" method="Post" action="visualizar_servico">
                                                            <input type="hidden" id="tipoContato" name="tipoContato" value="Serviço">
                                                            <input type="hidden" id="idServico_<?= $servico['idServico']; ?>" name="idServico" value="<?= $servico['idServico']; ?>">
                                                            <button type="submit" id="btnVisualizar" class="btn btn-default btn-xs" name="" title="Visualizar Serviço">
                                                                <i class="glyphicon glyphicon-file"></i>
                                                            </button>
                                                        </form>

                                                        <form style="display: inline-block;" role="form" method="Post" action="editar_servico">
                                                            <input type="hidden" id="idServico_<?= $servico['idServico']; ?>" name="idServico" value="<?= $servico['idServico']; ?>">
                                                            <input type="hidden" id="tipoContato" name="tipoContato" value="Serviço">
                                                            <button type="submit" id="btnServ" class="btn btn-default btn-xs" name="" title="Editar Serviço">
                                                                <i class="glyphicon glyphicon-edit"></i>
                                                            </button>
                                                        </form>
                                                        <button type="submit" id="btnExcluir" class="btn btn-default btn-xs" name="btnExcluir" idContato="<?= $servico['idContato']; ?>" idServico="<?= $servico['idServico']; ?>" nmServico="<?= $servico['nmServico']; ?>"  data-toggle="modal" data-target="#modal_excluir" data-placement="bottom" title="Excluir Serviço" data-delay="1">
                                                            <i class="glyphicon glyphicon-trash"></i>
                                                        </button>
                                                    </td>
                                                    <td align="center">
                                                        <input type="hidden" id="idServico_<?= $servico['idServico']; ?>" name="idServico" value="<?= $servico['idServico']; ?>">
                                                        <?php
                                                        if ($servico['ativo'] == "N") {
                                                            ?>
                                                            <button type="submit" id="btnAtivar" class="btn btn-danger btn-xs" idServico="<?= $servico['idServico']; ?>" name="btnAtivar" title="Ativar Serviço">
                                                                <i class="glyphicon glyphicon-ban-circle"></i>
                                                            </button>
                                                            <?php
                                                        } else {
                                                            ?>
                                                            <button type="submit" id="btnAtivar" class="btn btn-success btn-xs" idServico="<?= $servico['idServico']; ?>" name="btnAtivar" title="Destivar Serviço">
                                                                <i class="glyphicon glyphicon-ok"></i>
                                                            </button>
                                                            <?php
                                                        }
                                                        ?>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>
    </body>
    <script>
        jQuery(document).ready(function () {
            // Remove seleção de ativo no menu.
            $('#side-menu .nav li').removeClass('active');
            $('#side-menu li').removeClass('active');
            // Ativa botão no menu.
            $('#listagens').addClass('collapse in');
            $('#lista-servicos a').addClass('active');
            $('#lista-listagens').addClass('active');
        });
    </script>
    <script>
        jQuery(document).ready(function ()
        {
            $('#servicos').dataTable(
                    {
                        "bAutoWidth": false,
                        "sPaginationType": "full_numbers",
                        "sDom": '<"H"Tlfr>t<"F"ip>',
                        "aaSorting": [[0, 'asc']],
                        "aoColumnDefs": [{"bSortable": false, "aTargets": [5, 6]}],
                        "oLanguage":
                                {
                                    "sLengthMenu": "Mostrar _MENU_ registros por página",
                                    "sZeroRecords": "Não encontramos nada - desculpe",
                                    "sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros.",
                                    "sInfoEmpty": "Mostrando 0 a 0 de 0 registros encontrados",
                                    "sInfoFiltered": "(filtrada a partir de _MAX_ total de registros encontrados)",
                                    "sSearch": " Buscar: ",
                                    "oPaginate": {
                                        "sFirst": "Início",
                                        "sPrevious": "Anterior",
                                        "sNext": "Próximo",
                                        "sLast": "Último"}
                                }

                    });
            var idServico;
            var nmServico;
            var idContato;
            $("body").on("click", 'button[name=btnExcluir]', function ()
            {
                idServico = $(this).attr('idServico');
                nmServico = $(this).attr('nmServico');
                idContato = $(this).attr('idContato');
                $('#modal_delete_servico').html("Deseja excluir o serviço <b>\"" + nmServico + "\"</b> ?");
            });

            // Edita o contato utilizando atributo customizado e redefinindo url no modal.

            $("body").on("hidden.bs.modal", "#modal_excluir", function ()
            {
                window.location.replace("lista");
            });

            $("body").on("click", "#modal_btn-excluir", function ()
            {
                var btn = $(this);
                btn.text('Carregando...');
                btn.removeClass('btn-primary');
                btn.removeClass('btn-danger');
                btn.removeClass('btn-success');
                btn.addClass('btn-primary');

                $('#modal_btn-excluir').prop('disabled', true);
                $.ajax("delete_servico",
                        {
                            type: "POST",
                            data:
                                    {
                                        idServico: idServico,
                                        idContato: idContato
                                    }
                        })
                        .done(function (data)
                        {
                            $('#modal_delete_servico').html('Serviço excluído com sucesso');

                            btn.removeClass('btn-primary');
                            btn.addClass('btn-success');
                            btn.text('Excluído');
                        })
                        .fail(function ()
                        {
                            $('#modal_delete_servico').html('Erro: ' + data);
                            btn.removeClass('btn-primary');
                            btn.addClass('btn-danger');
                            btn.text('Falha');
                        })
            });

            $("body").on("click", "button[name=btnVisualizar]", function ()
            {
                idServico = $(this).attr('idServico');
                window.location.href = "visualizar_servico?idServico=" + idServico;
            });

            $("body").on("click", "button[name=btnAtivar]", function ()
            {
                var idServico = $(this).attr('idServico');
                $.ajax("update_ativar_servico",
                        {
                            type: "POST",
                            data:
                                    {
                                        'idServico': idServico
                                    }
                        })
                        .always(function ()
                        {
                            window.location.replace("lista");
                        });
            });

            $('.dataTables_filter label').addClass('pull-right');
        });
    </script>
    <style>
        .pagination
        {
            float: right !important;
        }
    </style>
</html>
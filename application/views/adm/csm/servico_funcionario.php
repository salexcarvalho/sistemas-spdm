<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - CSM</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <script>
            jQuery(document).ready(function ()
            {
                $('#contatos').dataTable(
                        {
                            "bAutoWidth": false,
                            "aoColumns":
                                    [
                                        {sWidth: 'auto'},
                                        {sWidth: 'auto'},
                                        {sWidth: '180px'},
                                        {sWidth: 'auto'},
                                    ],
                            "aoColumnDefs":
                                    [
                                        {
                                            "bSortable": false, "aTargets": [4]
                                        }
                                    ],
                            "oLanguage":
                                    {
                                        "sLengthMenu": "Mostrar _MENU_ registros por página",
                                        "sZeroRecords": "Não encontramos nada - desculpe",
                                        "sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros.",
                                        "sInfoEmpty": "Mostrando 0 a 0 de 0 registros encontrados",
                                        "sInfoFiltered": "(filtrada a partir de _MAX_ total de registros encontrados)",
                                        "sSearch": " Buscar: "
                                    }

                        });

                var idServico;
                var nmServico;
                var idFuncionario;
                var nmFuncionario;

                $("body").on("click", "button[name=btnExcluir]", function ()
                {
                    nmServico = $(this).attr('nmServico');
                    nmFuncionario = $(this).attr('nmFuncionario');
                    idServico = $(this).attr('idServico');
                    idFuncionario = $(this).attr('idFuncionario');

                    $('#modal_delete_servico_funcionario').html("Deseja desvincular o funcionário \"<b>" + nmFuncionario + "</b>\" da Servico \"<b>" + nmServico + "</b>\" ?");
                });

                $("body").on("hidden.bs.modal", "#modal_excluir", function ()
                {
                    window.location.replace("contato");
                });

                $("body").on("click", "#btn_novo_vinculo_servico_func", function ()
                {
                    window.location.href = "add_servico_funcionario";
                });

                $("body").on("click", "#modal_btn-excluir", function ()
                {
                    var btn = $(this);
                    btn.text('Carregando...');

                    btn.removeClass('btn-primary');
                    btn.removeClass('btn-danger');
                    btn.removeClass('btn-success');
                    btn.addClass('btn-primary');

                    $('#modal_btn-excluir').prop('disabled', true);

                    $.ajax("delete_contato",
                            {
                                type: "POST",
                                data:
                                        {
                                            idServico: idServico,
                                            idFuncionario: idFuncionario
                                        }
                            })
                            .done(function ()
                            {
                                $('#modal_delete_contato').html('Contato excluído com sucesso');

                                btn.removeClass('btn-primary');
                                btn.addClass('btn-success');
                                btn.text('Excluído');
                            })
                            .fail(function ()
                            {
                                $('#modal_delete_contato').html('Ocorreu um erro na exclusão do vínculo de "<b>' + nmFuncionario + '</b>" com "<b>' + nmServico + '"</b>');

                                btn.removeClass('btn-primary');
                                btn.addClass('btn-danger');
                                btn.text('Falha');
                            });
                });

                $('.dataTables_filter label').addClass('pull-right');
            });
        </script>
        <style>
            .pagination
            {
                float: right !important;
            }
        </style>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <!-- Modal -->
        <div class="modal fade" id="modal_excluir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Desvincular Funcionário de Servico</h4>
                    </div>
                    <div class="modal-body" id="modal_delete_servico_funcionario">Deseja desvincular este funcionário desta servico ?</div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        <button type="button" class="btn btn-primary" id="modal_btn-excluir">Excluir</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal -->
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Lista de Funcionários dos Servicos</h1>
                        <ol class="breadcrumb">
                            <li><i class="fa fa-list"></i> Listagens</li>
                            <li><i class="fa fa-chain"></i> Vínculos</li>
                            <li><i class="fa fa-user-md"></i> Funcionários</li>
                            <li class="active"><i class="fa fa-list-alt"></i> Serviços</li>
                        </ol>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Contato
                                <button type="submit" id="btn_novo_vinculo_servico_func" class="pull-right btn btn-primary btn-xs" name="btn_novo_vinculo_servico_func"><i class="glyphicon glyphicon-plus-sign"></i>  Adicionar Novo</button>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover" id="contatos">
                                        <thead>
                                            <tr>
                                                <th>ID Servico</th>
                                                <th>Nome Servico</th>
                                                <th>ID Funcionário</th>
                                                <th>Nome Funcionário</th>
                                                <th style="text-align: center">Ações</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            foreach ($servicos_funcionarios as $servico_funcionario) {
                                                ?>
                                                <tr>
                                                    <td><?= $servico_funcionario['idServico']; ?></td>
                                                    <td><?= $servico_funcionario['nmServico']; ?></td>
                                                    <td><?= $servico_funcionario['idFuncionario']; ?></td>
                                                    <td><?= $servico_funcionario['nmFuncionario']; ?></td>
                                                    <td align="center">
                                                        <form style="display: inline-block;" role="form" method="GET" action="visualizar_servico_funcionario">
                                                            <input type="hidden" name="idServico" value="<?= $servico_funcionario['idServico']; ?>">
                                                            <input type="hidden" name="idFuncionario" value="<?= $servico_funcionario['idFuncionario']; ?>">
                                                            <button type="submit" id="btnVisualizar" class="btn btn-default btn-xs" name="" title="Visualizar Contato" disabled>
                                                                <i class=" glyphicon glyphicon-file"></i>
                                                            </button>
                                                        </form>
                                                        <form style="display: inline-block;" role="form" method="GET" action="editar_servico_funcionario">
                                                            <input type="hidden" name="idServico" value="<?= $servico_funcionario['idServico']; ?>">
                                                            <input type="hidden" name="idFuncionario" value="<?= $servico_funcionario['idFuncionario']; ?>">
                                                            <button type="submit" id="btnEditar" class="btn btn-default btn-xs" name="" title="Editar Contato" disabled>
                                                                <i class=" glyphicon glyphicon-edit"></i>
                                                            </button>
                                                        </form>
                                                        <button disabled type="button" id="btnExcluir" class="btn btn-default btn-xs" idServico="<?= $servico_funcionario['idServico']; ?>" idFuncionario="<?= $servico_funcionario['idFuncionario']; ?>" name="btnExcluir" data-toggle="modal" data-target="#modal_excluir" data-placement="bottom" title="Excluir Contato" data-delay="1">
                                                            <i class="glyphicon glyphicon-trash"></i>
                                                        </button>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>
    </body>
</html>
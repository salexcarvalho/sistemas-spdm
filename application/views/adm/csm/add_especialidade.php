<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?= $_SESSION['company_name'] ?> - CSM</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Cadastrar Especialidade</h1>
                        <ol class="breadcrumb">
                            <li><i class="fa fa-list"></i> Listagens</li>
                            <li class="active"><i class="fa fa-hospital-o"></i> Especialidades</li>
                            <li class="active"><i class="fa fa-hospital-o"></i> Adicionar Novo</li>
                        </ol>
                        <form class="form-horizontal" role="form" method="POST" action="insert_especialidade" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="col-lg-6">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                Dados da Especialidade
                                            </div>
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <label for="nmEspecialidade" class="col-lg-2 control-label">Nome</label>
                                                    <div class="col-lg-10">
                                                        <input type="text" class="form-control" id="nmEspecialidade" name="nmEspecialidade" placeholder="" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="nuCentroCusto" class="col-lg-2 control-label">Centro de Custo</label>
                                                    <div class="col-lg-10">
                                                        <input type="text" class="form-control" id="nuCentroCusto" name="nuCentroCusto" placeholder="">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="dsEspecialidade" class="col-lg-2 control-label">Descrição da Especialidade</label>
                                                    <div class="col-lg-10">
                                                        <textarea style="resize: vertical;" class="form-control" rows="3" id="dsEspecialidade" name="dsEspecialidade" placeholder=""></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="chefeDep" class="col-lg-2 control-label">Chefe</label>
                                                    <div class="col-lg-10">
                                                        <select class="form-control" id="chefeDep" name="chefeDep" required>
                                                            <option></option>
                                                            <?php
                                                            foreach ($funcionarios as $funcionario) {
                                                                ?>
                                                                <option value="<?= $funcionario['idFuncionario']; ?>"><?= strtoupper($funcionario['nmFuncionario']); ?></option>
                                                                <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                Endereço
                                            </div>
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <label for="endLogradouro" class="col-lg-2 control-label">Rua</label>
                                                    <div class="col-lg-10">
                                                        <input type="text" class="form-control" id="endLogradouro" name="endLogradouro" placeholder="" required>
                                                    </div>
                                                </div>                    
                                                <div class="form-group">
                                                    <label for="nuLogradouro" class="col-lg-2 control-label">Número</label>
                                                    <div class="col-lg-4">
                                                        <input type="number" min="0" class="form-control" id="nuLogradouro" name="nuLogradouro" placeholder="" required>
                                                    </div>
                                                    <label for="complemento" class="col-lg-2 control-label">Complemento</label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" id="complemento" name="complemento" placeholder="">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="CEP" class="col-lg-2 control-label">CEP</label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" id="CEP" name="CEP" placeholder="" >
                                                    </div>
                                                    <label for="bairro" class="col-lg-2 control-label">Bairro</label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" id="bairro" name="bairro" placeholder="" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="cidade" class="col-lg-2 control-label">Cidade</label>
                                                    <div class="col-lg-4">
                                                        <input type="text" class="form-control" id="cidade" name="cidade" placeholder="" required>
                                                    </div>
                                                    <label for="estado" class="col-lg-2 control-label">Estado</label>
                                                    <div class="col-lg-4">
                                                        <select class="form-control" id="Estado" name="Estado" required>
                                                            <option></option>
                                                            <option value="AC" id="AC" name="AC">AC</option>
                                                            <option value="AL" id="AL" name="AL">AL</option>
                                                            <option value="AM" id="AM" name="AM">AM</option>
                                                            <option value="AP" id="AP" name="AP">AP</option>
                                                            <option value="BA" id="BA" name="BA">BA</option>
                                                            <option value="CE" id="CE" name="CE">CE</option>
                                                            <option value="DF" id="DF" name="DF">DF</option>
                                                            <option value="ES" id="ES" name="ES">ES</option>
                                                            <option value="GO" id="GO" name="GO">GO</option>
                                                            <option value="MA" id="MA" name="MA">MA</option>
                                                            <option value="MG" id="MG" name="MG">MG</option>
                                                            <option value="MS" id="MS" name="MS">MS</option>
                                                            <option value="MT" id="MT" name="MT">MT</option>
                                                            <option value="PA" id="PA" name="PA">PA</option>
                                                            <option value="PB" id="PB" name="PB">PB</option>
                                                            <option value="PE" id="PE" name="PE">PE</option>
                                                            <option value="PI" id="PI" name="PI">PI</option>
                                                            <option value="RJ" id="RJ" name="RJ">RJ</option>
                                                            <option value="RN" id="RN" name="RN">RN</option>
                                                            <option value="RO" id="RO" name="RO">RO</option>
                                                            <option value="RR" id="RR" name="RR">RR</option>
                                                            <option value="RS" id="RS" name="RS">RS</option>
                                                            <option value="SC" id="SC" name="SC">SC</option>
                                                            <option value="SE" id="SE" name="SE">SE</option>                                
                                                            <option value="SP" id="SP" name="SP">SP</option>
                                                            <option value="TO" id="TO" name="TO">TO</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12"><br></div>
                                                <div class="col-lg-12"><br></div>
                                                <div class="col-lg-12"><br></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="col-lg-6">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                Contato
                                                <div class="checkbox pull-right">
                                                    <span class="bold">Mostrar contato no site:</span>
                                                    <label><input type="radio" name="mostrarContatoNoSite" value='S' required>SIM</label>
                                                    <label><input type="radio" name="mostrarContatoNoSite" value='N'>NÃO</label>
                                                </div>                     
                                            </div>
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <label for="telefone" class="col-lg-2 control-label" >telefone</label>
                                                    <div class="col-lg-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                                            <input type="tel" class="form-control" id="telefone" name="telefone" placeholder="" required>
                                                        </div>
                                                    </div>
                                                    <label for="nuVOIP" class="col-lg-1 control-label">VOIP</label>
                                                    <div class="col-lg-3">
                                                        <input type="number" min="0" class="form-control" id="nuVOIP" name="nuVOIP" placeholder="">
                                                    </div>
                                                </div>                    
                                                <div class="form-group">
                                                    <label for="email" class="col-lg-2 control-label">E-mail</label>
                                                    <div class="col-lg-10">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                                            <input type="email" class="form-control" id="email" name="email" placeholder="" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="site" class="col-lg-2 control-label">Site</label>
                                                    <div class="col-lg-10">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="fa fa-globe"></i></span>
                                                            <input type="url" class="form-control" id="site" name="site" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="facebook" class="col-lg-2 control-label">Facebook</label>
                                                    <div class="col-lg-10">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="fa fa-facebook"></i></span>
                                                            <input type="url" class="form-control" id="facebook" name="facebook" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="twitter" class="col-lg-2 control-label">Twitter</label>
                                                    <div class="col-lg-10">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="fa fa-twitter"></i></span>
                                                            <input type="url" class="form-control" id="twitter" name="twitter" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="googleplus" class="col-lg-2 control-label">Google+</label>
                                                    <div class="col-lg-10">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="fa fa-google-plus"></i></span>
                                                            <input type="url" class="form-control" id="googleplus" name="googleplus" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="linkedin" class="col-lg-2 control-label">LinkedIn</label>
                                                    <div class="col-lg-10">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="fa fa-linkedin"></i></span>
                                                            <input type="url" class="form-control" id="linkedin" name="linkedin" placeholder="">
                                                        </div>
                                                    </div>
                                                </div> 
                                                <div class="col-lg-12"><br></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                Fotos
                                            </div>
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <label for="ft1" class="col-lg-2 control-label">Foto 1</label>
                                                    <div class="col-lg-10">
                                                        <input type="file" name="ft1" id="ft1">
                                                        <input type="text" class="form-control" id="dsImagem1" name="dsImagem1" placeholder="Descrição da Foto">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="ft2" class="col-lg-2 control-label">Foto 2</label>
                                                    <div class="col-lg-10">
                                                        <input type="file" name="ft2" id="ft2">
                                                        <input type="text" class="form-control" id="dsImagem2" name="dsImagem2" placeholder="Descrição da Foto">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="ft3" class="col-lg-2 control-label">Foto 3</label>
                                                    <div class="col-lg-10">
                                                        <input type="file" name="ft3" id="ft3">
                                                        <input type="text" class="form-control" id="dsImagem3" name="dsImagem3" placeholder="Descrição da Foto">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="ft4" class="col-lg-2 control-label">Foto 4</label>
                                                    <div class="col-lg-10">
                                                        <input type="file" name="ft4" id="ft4">
                                                        <input type="text" class="form-control" id="dsImagem4" name="dsImagem4" placeholder="Descrição da Foto">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="ft5" class="col-lg-2 control-label">Foto 5</label>
                                                    <div class="col-lg-10">
                                                        <input type="file" name="ft5" id="ft5">
                                                        <input type="text" class="form-control" id="dsImagem5" name="dsImagem5" placeholder="Descrição da Foto">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="col-lg-12">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                Horário de Atendimento
                                            </div>
                                            <div class="panel-body">
                                                <table class="table table-responsive">
                                                    <thead>
                                                        <tr>
                                                            <th></th>
                                                            <th width="13%">&nbsp;&nbsp;&nbsp;&nbsp;Domingo</th>
                                                            <th width="13%">&nbsp;&nbsp;&nbsp;&nbsp;Segunda</th>
                                                            <th width="13%">&nbsp;&nbsp;&nbsp;&nbsp;Terça</th>
                                                            <th width="13%">&nbsp;&nbsp;&nbsp;&nbsp;Quarta</th>
                                                            <th width="13%">&nbsp;&nbsp;&nbsp;&nbsp;Quinta</th>
                                                            <th width="13%">&nbsp;&nbsp;&nbsp;&nbsp;Sexta</th>
                                                            <th width="13%">&nbsp;&nbsp;&nbsp;&nbsp;Sábado</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                                <div class="form-group-sm">
                                                                    <label class="col-lg-12">&nbsp</label>
                                                                    <label class="col-lg-12 control-label">Início</label>
                                                                    <label class="col-lg-12 control-label">Fim</label>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="form-group-sm">
                                                                    <div class="col-lg-12">
                                                                        <div class="checkbox">
                                                                            <label><input id="24horas1" name="24horas1" type="checkbox" value="S">24 hs</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group-sm">
                                                                    <div class="col-lg-12">
                                                                        <input type="time" class="form-control" id="horarioin1" name="horarioin1" placeholder="Horário de Início" required>
                                                                    </div>
                                                                    <div class="col-lg-12">
                                                                        <input type="time" class="form-control" id="horariofi1" name="horariofi1" placeholder="Horário de Término" required>
                                                                    </div> 
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="form-group-sm">
                                                                    <div class="col-lg-12">
                                                                        <div class="checkbox">
                                                                            <label><input id="24horas2" name="24horas2" type="checkbox" value="S">24 hs</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group-sm">
                                                                    <div class="col-lg-12">
                                                                        <input type="time" class="form-control" id="horarioin2" name="horarioin2" placeholder="Horário de Início" required>
                                                                    </div>
                                                                    <div class="col-lg-12">
                                                                        <input type="time" class="form-control" id="horariofi2" name="horariofi2" placeholder="Horário de Término" required>
                                                                    </div> 
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="form-group-sm">
                                                                    <div class="col-lg-12">
                                                                        <div class="checkbox">
                                                                            <label><input id="24horas3" name="24horas3" type="checkbox" value="S">24 hs</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group-sm">
                                                                    <div class="col-lg-12">
                                                                        <input type="time" class="form-control" id="horarioin3" name="horarioin3" placeholder="Horário de Início" required>
                                                                    </div>
                                                                    <div class="col-lg-12">
                                                                        <input type="time" class="form-control" id="horariofi3" name="horariofi3" placeholder="Horário de Término" required>
                                                                    </div> 
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="form-group-sm">
                                                                    <div class="col-lg-12">
                                                                        <div class="checkbox">
                                                                            <label><input id="24horas4" name="24horas4" type="checkbox" value="S">24 hs</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group-sm">
                                                                    <div class="col-lg-12">
                                                                        <input type="time" class="form-control" id="horarioin4" name="horarioin4" placeholder="Horário de Início" required>
                                                                    </div>
                                                                    <div class="col-lg-12">
                                                                        <input type="time" class="form-control" id="horariofi4" name="horariofi4" placeholder="Horário de Término" required>
                                                                    </div> 
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="form-group-sm">
                                                                    <div class="col-lg-12">
                                                                        <div class="checkbox">
                                                                            <label><input id="24horas5" name="24horas5" type="checkbox" value="S">24 hs</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group-sm">
                                                                    <div class="col-lg-12">
                                                                        <input type="time" class="form-control" id="horarioin5" name="horarioin5" placeholder="Horário de Início" required>
                                                                    </div>
                                                                    <div class="col-lg-12">
                                                                        <input type="time" class="form-control" id="horariofi5" name="horariofi5" placeholder="Horário de Término" required>
                                                                    </div> 
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="form-group-sm">
                                                                    <div class="col-lg-12">
                                                                        <div class="checkbox">
                                                                            <label><input id="24horas6" name="24horas6" type="checkbox" value="S">24 hs</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group-sm">
                                                                    <div class="col-lg-12">
                                                                        <input type="time" class="form-control" id="horarioin6" name="horarioin6" placeholder="Horário de Início" required>
                                                                    </div>
                                                                    <div class="col-lg-12">
                                                                        <input type="time" class="form-control" id="horariofi6" name="horariofi6" placeholder="Horário de Término" required>
                                                                    </div> 
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="form-group-sm">
                                                                    <div class="col-lg-12">
                                                                        <div class="checkbox">
                                                                            <label><input id="24horas7" name="24horas7" type="checkbox" value="S">24 hs</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group-sm">
                                                                    <div class="col-lg-12">
                                                                        <input type="time" class="form-control" id="horarioin7" name="horarioin7" placeholder="Horário de Início" required>
                                                                    </div>
                                                                    <div class="col-lg-12">
                                                                        <input type="time" class="form-control" id="horariofi7" name="horariofi7" placeholder="Horário de Término" required>
                                                                    </div> 
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>            
                                </div>
                            </div>    
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <div class="col-lg-5">
                                        <button id="btncadastrar" type="submit" class="btn btn-primary">Cadastrar</button>
                                        <a href="lista">
                                            <button id="ok" type="button" class="btn btn-success">Fechar</button>
                                        </a>
                                        <a href="lista">
                                            <button id="back" type="button" class="btn btn-danger">Voltar</button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>
    </body>
    <script>
        jQuery(document).ready(function ()
        {

            if ($('#msg').is(':visible'))
            {
                $('#ok').css('display', 'block');
                $('#btncadastrar').css('display', 'none')
                $('#back').css('display', 'none');
            } else
            {
                $('#ok').css('display', 'none');
            }

            // Remove seleção de ativo no menu.
            $('.navbar-nav li').removeClass('active');
            // Ativa botão no menu.
            //$('.navbar-nav #mn_home').addClass('active');

            // Seleciona 1 index do select de especialidades
            $('#especialidadeper').val('NULL');

            // Valicação 24h
            $('#24horas1').change(function () {
                if ($('#24horas1').prop('checked'))
                {
                    $('#horarioin1').prop('disabled', true);
                    $('#horariofi1').prop('disabled', true);
                    $('#horarioin1').val('');
                    $('#horariofi1').val('');
                } else
                {
                    $('#horarioin1').prop('disabled', false);
                    $('#horariofi1').prop('disabled', false);
                }
            });
            $('#24horas2').change(function ()
            {
                if ($('#24horas2').prop('checked'))
                {
                    $('#horarioin2').prop('disabled', true);
                    $('#horariofi2').prop('disabled', true);
                    $('#horarioin2').val('');
                    $('#horariofi2').val('');
                } else
                {
                    $('#horarioin2').prop('disabled', false);
                    $('#horariofi2').prop('disabled', false);
                }
            });
            $('#24horas3').change(function ()
            {
                if ($('#24horas3').prop('checked'))
                {
                    $('#horarioin3').prop('disabled', true);
                    $('#horariofi3').prop('disabled', true);
                    $('#horarioin3').val('');
                    $('#horariofi3').val('');
                } else
                {
                    $('#horarioin3').prop('disabled', false);
                    $('#horariofi3').prop('disabled', false);
                }
            });
            $('#24horas4').change(function ()
            {
                if ($('#24horas4').prop('checked'))
                {
                    $('#horarioin4').prop('disabled', true);
                    $('#horariofi4').prop('disabled', true);
                    $('#horarioin4').val('');
                    $('#horariofi4').val('');
                } else
                {
                    $('#horarioin4').prop('disabled', false);
                    $('#horariofi4').prop('disabled', false);
                }
            });
            $('#24horas5').change(function ()
            {
                if ($('#24horas5').prop('checked'))
                {
                    $('#horarioin5').prop('disabled', true);
                    $('#horariofi5').prop('disabled', true);
                    $('#horarioin5').val('');
                    $('#horariofi5').val('');
                } else
                {
                    $('#horarioin5').prop('disabled', false);
                    $('#horariofi5').prop('disabled', false);
                }
            });
            $('#24horas6').change(function ()
            {
                if ($('#24horas6').prop('checked'))
                {
                    $('#horarioin6').prop('disabled', true);
                    $('#horariofi6').prop('disabled', true);
                    $('#horarioin6').val('');
                    $('#horariofi6').val('');
                } else
                {
                    $('#horarioin6').prop('disabled', false);
                    $('#horariofi6').prop('disabled', false);
                }
            });
            $('#24horas7').change(function ()
            {
                if ($('#24horas7').prop('checked'))
                {
                    $('#horarioin7').prop('disabled', true);
                    $('#horariofi7').prop('disabled', true);
                    $('#horarioin7').val('');
                    $('#horariofi7').val('');
                } else
                {
                    $('#horarioin7').prop('disabled', false);
                    $('#horariofi7').prop('disabled', false);
                }
            });
            // Fim de valicação 24h
        });
    </script>
</html>
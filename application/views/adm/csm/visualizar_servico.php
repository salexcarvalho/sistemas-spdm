<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - CSM</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <script>
            jQuery(document).ready(function ()
            {
                // Remove seleção de ativo no menu.
                $('.navbar-nav li').removeClass('active');
                // Ativa botão no menu.
                //$('.navbar-nav #mn_home').addClass('active');

                // Fim de valicação 24h
            });
        </script>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <form class="form-horizontal" role="form">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">Visualizar Serviço</h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-list"></i> Listagens</li>
                                <li><a href="lista"><i class="fa fa-stethoscope"></i> Serviços</a></li>
                                <li class="active"><i class="fa fa-file"></i> Visualizar Serviço</li>
                            </ol>
                        </div>
                    </div>                        
                    <div class="row">
                        <div class='col-lg-12'>
                            <div class="col-lg-6">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        Dados do Serviço  
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <label for="nmServico" class="col-lg-2">Nome</label>
                                            <div class="col-lg-10">
                                                <input type="text" class="form-control" id="nmServico" name="nmServico" placeholder="" disabled value="<?= $servico[0]['nmServico']; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="especialidadeper" class="col-lg-2 control-label">Especialidade</label>
                                            <div class="col-lg-10">
                                                <select id="especialidadeper" name="especialidadeper[]" class="form-control" disabled>
                                                    <option value="NULL">Nenhum</option>
                                                    <?php
                                                    foreach ($especialidades as $especialidade) {
                                                        ?>
                                                        <option 
                                                        <?php
                                                        foreach ($especialidades_servico as $especialidade_servico) {
                                                            if ($especialidade_servico['idEspecialidade'] == $especialidade['idEspecialidade']) {
                                                                echo ' selected ';
                                                            }
                                                        }
                                                        ?>
                                                            value="<?= $especialidade['idEspecialidade']; ?>"><?= $especialidade['nmEspecialidade']; ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="nuCentroCusto" class="col-lg-2">Centro de Custo</label>
                                            <div class="col-lg-10">
                                                <input type="text" class="form-control" id="nuCentroCusto" name="nuCentroCusto" placeholder="" disabled value="<?= $servico[0]['nuCentroCusto']; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="dsServico" class="col-lg-2">Descrição</label>
                                            <div class="col-lg-10">
                                                <textarea style="resize: vertical;" class="form-control" rows="3" id="dsServico" name="dsServico" disabled placeholder=""><?= $servico[0]['dsServico']; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="nmFuncionario" class="col-lg-2">Chefe</label>
                                            <div class="col-lg-10">
                                                <input type="text" min="0" class="form-control" id="nmFuncionario" name="nmFuncionario" placeholder="" disabled value="<?= $servico[0]['nmFuncionarioChefe']; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        Endereço
                                    </div>
                                    <div class='panel-body'>
                                        <div class="form-group">
                                            <label for="endLogradouro" class="col-lg-2 control-label">Rua</label>
                                            <div class="col-lg-10">
                                                <input type="text" class="form-control" id="endLogradouro" name="endLogradouro" placeholder="" disabled value="<?= $servico[0]['endLogradouro']; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="nuLogradouro" class="col-lg-2 control-label">Número</label>
                                            <div class="col-lg-2">
                                                <input type="number" min="0" class="form-control" id="nuLogradouro" name="nuLogradouro" placeholder="" disabled value="<?= $servico[0]['nuLogradouro']; ?>">
                                            </div>
                                            <label for="complemento" class="col-lg-2 control-label">Complemento</label>
                                            <div class="col-lg-6">
                                                <input type='text' class="form-control" id="complemento" name="complemento" placeholder="" disabled><?= $servico[0]['complemento']; ?>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="CEP" class="col-lg-2 control-label">CEP</label>
                                            <div class="col-lg-2">
                                                <input type="text" class="form-control" id="CEP" name="CEP" placeholder="" disabled value="<?= $servico[0]['CEP']; ?>">
                                            </div>
                                            <label for="bairro" class="col-lg-2 control-label">Bairro</label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control" id="bairro" name="bairro" placeholder="" disabled value="<?= $servico[0]['bairro']; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="cidade" class="col-lg-2 control-label">Cidade</label>
                                            <div class="col-lg-7">
                                                <input type="text" class="form-control" id="cidade" name="cidade" placeholder="" disabled value="<?= $servico[0]['cidade']; ?>">
                                            </div>
                                            <label for="estado" class="col-lg-1 control-label">Estado</label>
                                            <div class="col-lg-2">
                                                <input type="text" class="form-control" id="estado" name="estado" placeholder="" disabled value="<?= $servico[0]['estado']; ?>">
                                            </div>
                                        </div>
                                        <div class='col-lg-12'><br></div>
                                        <div class='col-lg-12'><br></div>
                                        <div class='col-lg-12'><br></div>
                                        <div class='col-lg-12'><br></div>
                                        <div class='col-lg-12'><br></div>
                                    </div>
                                </div>
                            </div>
                        </div>      
                    </div>
                    <div class="row">
                        <div class='col-lg-12'>
                            <div class="col-lg-6">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        Contato
                                        <div class="checkbox pull-right">
                                            <input type="checkbox" name="mostrarContatoNoSite" value="S" disabled <?= ($servico[0]['mostrarContatoNoSite'] == 'S') ? 'checked' : ''; ?>>Mostrar contato no site
                                        </div>
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <label for="telefone" class="col-lg-2 control-label">Telefone</label>
                                            <div class="col-lg-7">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                                    <input type="tel" class="form-control" id="telefone" name="telefone" placeholder="" disabled value="<?= $servico[0]['telefone']; ?>">
                                                </div>
                                            </div>
                                            <label for="nuVOIP" class="col-lg-1 control-label">VOIP</label>
                                            <div class="col-lg-2">
                                                <input type="number" min="0" class="form-control" id="nuVOIP" name="nuVOIP" placeholder="" disabled value="<?= $servico[0]['nuVOIP']; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="email" class="col-lg-2 control-label">E-mail</label>
                                            <div class="col-lg-10">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                                    <input type="email" class="form-control" id="email" name="email" placeholder="" disabled value="<?= $servico[0]['email']; ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="site" class="col-lg-2 control-label">Site</label>
                                            <div class="col-lg-10">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-globe"></i></span>                                    
                                                    <input type="url" class="form-control" id="site" name="site" placeholder="" disabled value="<?= $servico[0]['site']; ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="facebook" class="col-lg-2 control-label">Facebook</label>
                                            <div class="col-lg-10">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-facebook"></i></span>                                        
                                                    <input type="url" class="form-control" id="facebook" name="facebook" placeholder="" disabled value="<?= $servico[0]['facebook']; ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="twitter" class="col-lg-2 control-label">Twitter</label>
                                            <div class="col-lg-10">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-twitter"></i></span>  
                                                    <input type="url" class="form-control" id="twitter" name="twitter" placeholder="" disabled value="<?= $servico[0]['twitter']; ?>">
                                                </div>
                                            </div>
                                        </div>                                
                                        <div class="form-group">
                                            <label for="linkedin" class="col-lg-2 control-label">LinkedIn</label>
                                            <div class="col-lg-10">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-linkedin"></i></span>  
                                                    <input type="url" class="form-control" id="linkedin" name="linkedin" placeholder="" disabled value="<?= $servico[0]['linkedIn']; ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="googleplus" class="col-lg-2 control-label">Google+</label>
                                            <div class="col-lg-10">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-google-plus"></i></span>  
                                                    <input type="url" class="form-control" id="googleplus" name="googleplus" placeholder="" disabled value="<?= $servico[0]['googleplus']; ?>">
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        Informações Adicionais
                                    </div>
                                    <div class="panel-body">
                                        <div class="col-lg-12"><br></div>
                                        <div class="form-group">    
                                            <label for="dsDoenca" class="col-lg-2 control-label">O que é a Doença?</label>
                                            <div class="col-lg-10">
                                                <textarea style="resize: vertical;" class="form-control" rows="5" id="dsDoenca" name="dsDoenca" placeholder="" disabled><?= $servico[0]['dsDoenca']; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="col-lg-12"><br></div>
                                        <div class="form-group">
                                            <label for="formaAcesso" class="col-lg-2 control-label">Formas de acesso</label>
                                            <div class="col-lg-10">
                                                <textarea style="resize: vertical;" class="form-control" rows="5" id="formaAcesso" name="formaAcesso" placeholder="" disabled><?= $servico[0]['formaAcesso']; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="col-lg-12"><br></div>
                                        <div class="col-lg-12"><br></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="col-lg-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        Fotos da Unidade
                                    </div>
                                    <div class="panel-body">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="ft1" class="col-lg-2 control-label">Foto 1</label>
                                                <div class="col-lg-10">
                                                    <img src="" alt="Imagem não localizada" class="img-thumbnail"> 
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="dsImagem1" class="col-lg-2 control-label">Descrição</label>
                                                <div class="col-lg-10">
                                                    <textarea style="resize: vertical;" class="form-control" rows="3" id="dsImagem1" name="dsImagem1" placeholder="" disabled><?= $ft['ft1']['dsImagem']; ?></textarea>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="ft3" class="col-lg-2 control-label">Foto 3</label>
                                                <div class="col-lg-10">
                                                    <img src="<?= $ft['ft3']['caminhoImagem'] . $ft['ft3']['nmImagem']; ?>" alt="Imagem não localizada" class="img-thumbnail">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="dsImagem3" class="col-lg-2 control-label">Descrição</label>
                                                <div class="col-lg-10">
                                                    <textarea style="resize: vertical;" class="form-control" rows="3" id="dsImagem1" name="dsImagem1" placeholder="" disabled><?= $ft['ft3']['dsImagem']; ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="ft2" class="col-lg-2 control-label">Foto 2</label>
                                                <div class="col-lg-10">
                                                    <img src="<?= $ft['ft2']['caminhoImagem'] . $ft['ft2']['nmImagem']; ?>" alt="Imagem não localizada" class="img-thumbnail">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="dsImagem2" class="col-lg-2 control-label">Descrição</label>
                                                <div class="col-lg-10">
                                                    <textarea style="resize: vertical;" class="form-control" rows="3" id="dsImagem1" name="dsImagem1" placeholder="" disabled><?= $ft['ft2']['dsImagem']; ?></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="ft4" class="col-lg-2 control-label">Foto 4</label>
                                                <div class="col-lg-10">
                                                    <img src="<?= $ft['ft4']['caminhoImagem'] . $ft['ft4']['nmImagem']; ?>" alt="Imagem não localizada" class="img-thumbnail">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="dsImagem4" class="col-lg-2 control-label">Descrição</label>
                                                <div class="col-lg-10">
                                                    <textarea style="resize: vertical;" class="form-control" rows="3" id="dsImagem1" name="dsImagem1" placeholder="" disabled><?= $ft['ft4']['dsImagem']; ?></textarea>
                                                </div>
                                            </div>                                    
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="col-lg-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        Horário de Atendimento
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <label class="col-lg-offset-1 col-lg-1">Domingo</label>
                                            <label class="col-lg-1">Segunda</label>
                                            <label class="col-lg-1">Terça</label>
                                            <label class="col-lg-1">Quarta</label>
                                            <label class="col-lg-1">Quinta</label>
                                            <label class="col-lg-1">Sexta</label>
                                            <label class="col-lg-1">Sabado</label>
                                        </div>
                                        <div class="form-group">                            
                                            <div class="col-lg-offset-1 col-lg-1">
                                                <div class="checkbox">
                                                    <label>
                                                        <input id="24horas1" name="24horas1" type="checkbox" value="S" disabled 
<?php
foreach ($horarios_atendimento as $horarioatd) {
    if ($horarioatd['diaSemana'] == 'DOM' && $horarioatd['horario24Horas'] == 'S') {
        echo 'checked';
    }
}
?>>24 horas
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-lg-1">
                                                <div class="checkbox">
                                                    <label>
                                                        <input id="24horas2" name="24horas2" type="checkbox" value="S" disabled 
<?php
foreach ($horarios_atendimento as $horarioatd) {
    if ($horarioatd['diaSemana'] == 'SEG' && $horarioatd['horario24Horas'] == 'S') {
        echo 'checked';
    }
}
?>>24 horas
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-lg-1">
                                                <div class="checkbox">
                                                    <label>
                                                        <input id="24horas3" name="24horas3" type="checkbox" value="S" disabled 
<?php
foreach ($horarios_atendimento as $horarioatd) {
    if ($horarioatd['diaSemana'] == 'TER' && $horarioatd['horario24Horas'] == 'S') {
        echo 'checked';
    }
}
?>>24 horas
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-lg-1">
                                                <div class="checkbox">
                                                    <label>
                                                        <input id="24horas4" name="24horas4" type="checkbox" value="S" disabled 
                                                        <?php
                                                               foreach ($horarios_atendimento as $horarioatd) {
                                                                   if ($horarioatd['diaSemana'] == 'QUA' && $horarioatd['horario24Horas'] == 'S') {
                                                                       echo 'checked';
                                                                   }
                                                               }
                                                               ?>>24 horas
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-lg-1">
                                                <div class="checkbox">
                                                    <label>
                                                        <input id="24horas5" name="24horas5" type="checkbox" value="S" disabled 
                                                        <?php
                                                        foreach ($horarios_atendimento as $horarioatd) {
                                                            if ($horarioatd['diaSemana'] == 'QUI' && $horarioatd['horario24Horas'] == 'S') {
                                                                echo 'checked';
                                                            }
                                                        }
                                                        ?>>24 horas
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-lg-1">
                                                <div class="checkbox">
                                                    <label>
                                                        <input id="24horas6" name="24horas6" type="checkbox" value="S" disabled 
                                                        <?php
                                                        foreach ($horarios_atendimento as $horarioatd) {
                                                            if ($horarioatd['diaSemana'] == 'SEX' && $horarioatd['horario24Horas'] == 'S') {
                                                                echo 'checked';
                                                            }
                                                        }
                                                        ?>>24 horas
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-lg-1">
                                                <div class="checkbox">
                                                    <label>
                                                        <input id="24horas7" name="24horas7" type="checkbox" value="S" disabled 
                                                        <?php
                                                        foreach ($horarios_atendimento as $horarioatd) {
                                                            if ($horarioatd['diaSemana'] == 'SAB' && $horarioatd['horario24Horas'] == 'S') {
                                                                echo 'checked';
                                                            }
                                                        }
                                                        ?>>24 horas
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="horarioin2" class="col-lg-1 control-label">Início</label>
                                            <div class="col-lg-1">
                                                <input type="time" class="form-control" id="horarioin2" name="horarioin1" placeholder="Horário de Início de Atendimento" disabled 
                                                        <?php
                                                        foreach ($horarios_atendimento as $horarioatd) {
                                                            if ($horarioatd['diaSemana'] == 'DOM' && !($horarioatd['horario24Horas'] == 'S')) {
                                                                echo 'value="' . $horarioatd['horarioInicio'] . '"';
                                                            }
                                                        }
                                                        ?>>
                                            </div>
                                            <div class="col-lg-1">
                                                <input type="time" class="form-control" id="horarioin2" name="horarioin2" placeholder="Horário de Início de Atendimento" disabled 
<?php
foreach ($horarios_atendimento as $horarioatd) {
    if ($horarioatd['diaSemana'] == 'SEG' && !($horarioatd['horario24Horas'] == 'S')) {
        echo 'value="' . $horarioatd['horarioInicio'] . '"';
    }
}
?>>
                                            </div>
                                            <div class="col-lg-1">
                                                <input type="time" class="form-control" id="horarioin2" name="horarioin2" placeholder="Horário de Início de Atendimento" disabled 
                                                <?php
                                                foreach ($horarios_atendimento as $horarioatd) {
                                                    if ($horarioatd['diaSemana'] == 'TER' && !($horarioatd['horario24Horas'] == 'S')) {
                                                        echo 'value="' . $horarioatd['horarioInicio'] . '"';
                                                    }
                                                }
                                                ?>>
                                            </div>
                                            <div class="col-lg-1">
                                                <input type="time" class="form-control" id="horarioin4" name="horarioin4" placeholder="Horário de Início de Atendimento" disabled 
                                                <?php
                                                foreach ($horarios_atendimento as $horarioatd) {
                                                    if ($horarioatd['diaSemana'] == 'QUA' && !($horarioatd['horario24Horas'] == 'S')) {
                                                        echo 'value="' . $horarioatd['horarioInicio'] . '"';
                                                    }
                                                }
                                                ?>>
                                            </div>                                
                                            <div class="col-lg-1">
                                                <input type="time" class="form-control" id="horarioin5" name="horarioin5" placeholder="Horário de Início de Atendimento" disabled 
                                                <?php
                                                foreach ($horarios_atendimento as $horarioatd) {
                                                    if ($horarioatd['diaSemana'] == 'QUI' && !($horarioatd['horario24Horas'] == 'S')) {
                                                        echo 'value="' . $horarioatd['horarioInicio'] . '"';
                                                    }
                                                }
                                                ?>>
                                            </div>
                                            <div class="col-lg-1">
                                                <input type="time" class="form-control" id="horarioin6" name="horarioin6" placeholder="Horário de Início de Atendimento" disabled 
<?php
foreach ($horarios_atendimento as $horarioatd) {
    if ($horarioatd['diaSemana'] == 'SEX' && !($horarioatd['horario24Horas'] == 'S')) {
        echo 'value="' . $horarioatd['horarioInicio'] . '"';
    }
}
?>>
                                            </div>  
                                            <div class="col-lg-1">
                                                <input type="time" class="form-control" id="horarioin7" name="horarioin7" placeholder="Horário de Início de Atendimento" disabled 
                                                <?php
                                                       foreach ($horarios_atendimento as $horarioatd) {
                                                           if ($horarioatd['diaSemana'] == 'SAB' && !($horarioatd['horario24Horas'] == 'S')) {
                                                               echo 'value="' . $horarioatd['horarioInicio'] . '"';
                                                           }
                                                       }
                                                       ?>>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="horariofi1" class="col-lg-1 control-label">Fim</label>
                                            <div class="col-lg-1">
                                                <input type="time" class="form-control" id="horariofi1" name="horariofi1" placeholder="Horário de Término de Atendimento" disabled 
                                                       <?php
                                                       foreach ($horarios_atendimento as $horarioatd) {
                                                           if ($horarioatd['diaSemana'] == 'DOM' && !($horarioatd['horario24Horas'] == 'S')) {
                                                               echo 'value="' . $horarioatd['horarioFim'] . '"';
                                                           }
                                                       }
                                                       ?>>
                                            </div>
                                            <div class="col-lg-1">
                                                <input type="time" class="form-control" id="horariofi2" name="horariofi2" placeholder="Horário de Término de Atendimento" disabled 
                                                <?php
                                                foreach ($horarios_atendimento as $horarioatd) {
                                                    if ($horarioatd['diaSemana'] == 'SEG' && !($horarioatd['horario24Horas'] == 'S')) {
                                                        echo 'value="' . $horarioatd['horarioFim'] . '"';
                                                    }
                                                }
                                                ?>>
                                            </div>
                                            <div class="col-lg-1">
                                                <input type="time" class="form-control" id="horariofi3" name="horariofi3" placeholder="Horário de Término de Atendimento" disabled 
                                                <?php
                                                foreach ($horarios_atendimento as $horarioatd) {
                                                    if ($horarioatd['diaSemana'] == 'TER' && !($horarioatd['horario24Horas'] == 'S')) {
                                                        echo 'value="' . $horarioatd['horarioFim'] . '"';
                                                    }
                                                }
                                                ?>>
                                            </div>
                                            <div class="col-lg-1">
                                                <input type="time" class="form-control" id="horariofi4" name="horariofi4" placeholder="Horário de Término de Atendimento" disabled 
                                                <?php
                                                foreach ($horarios_atendimento as $horarioatd) {
                                                    if ($horarioatd['diaSemana'] == 'QUA' && !($horarioatd['horario24Horas'] == 'S')) {
                                                        echo 'value="' . $horarioatd['horarioFim'] . '"';
                                                    }
                                                }
                                                ?>>
                                            </div>
                                            <div class="col-lg-1">
                                                <input type="time" class="form-control" id="horariofi5" name="horariofi5" placeholder="Horário de Término de Atendimento" disabled 
<?php
foreach ($horarios_atendimento as $horarioatd) {
    if ($horarioatd['diaSemana'] == 'QUI' && !($horarioatd['horario24Horas'] == 'S')) {
        echo 'value="' . $horarioatd['horarioFim'] . '"';
    }
}
?>>
                                            </div>
                                            <div class="col-lg-1">
                                                <input type="time" class="form-control" id="horariofi6" name="horariofi6" placeholder="Horário de Término de Atendimento" disabled 
                                                <?php
                                                       foreach ($horarios_atendimento as $horarioatd) {
                                                           if ($horarioatd['diaSemana'] == 'SEX' && !($horarioatd['horario24Horas'] == 'S')) {
                                                               echo 'value="' . $horarioatd['horarioFim'] . '"';
                                                           }
                                                       }
                                                       ?>>
                                            </div>
                                            <div class="col-lg-1">
                                                <input type="time" class="form-control" id="horariofi7" name="horariofi7" placeholder="Horário de Término de Atendimento" disabled 
                                                <?php
                                                foreach ($horarios_atendimento as $horarioatd) {
                                                    if ($horarioatd['diaSemana'] == 'SAB' && !($horarioatd['horario24Horas'] == 'S')) {
                                                        echo 'value="' . $horarioatd['horarioFim'] . '"';
                                                    }
                                                }
                                                ?>>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="col-lg-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        Sites Relacionados
                                    </div>
                                    <div class="panel-body">
                                                <?php
                                                for ($i = 0; $i < 3; $i++) {
                                                    if (($sites_servico[$i]['nmEmpresa']) != "") {
                                                        ?>
                                                <div class="form-group">
                                                    <label for="nmEmpresa<?= $i + 1; ?>" class="col-lg-1 control-label">Empresa</label>
                                                    <div class="col-lg-5">
                                                        <input type="text" class="form-control" id="nmEmpresa<?= $i + 1; ?>" name="nmEmpresa<?= $i + 1; ?>" placeholder="" disabled value="<?= $sites_servico[$i]['nmEmpresa']; ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="urlSite<?= $i + 1; ?>" class="col-lg-1 control-label">Site</label>
                                                    <div class="col-lg-5">
                                                        <input type="url" class="form-control" id="urlSite<?= $i + 1; ?>" name="urlSite<?= $i + 1; ?>" placeholder="" disabled value="<?= $sites_servico[$i]['urlSite']; ?>">
                                                    </div>
                                                </div>
                                                    <?php }
                                                } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <a href="lista">
                                    <button type="button" id="back" class="btn btn-primary" name="back"> Voltar</button>
                                </a>                        
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
<?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>
    </body>
</html>
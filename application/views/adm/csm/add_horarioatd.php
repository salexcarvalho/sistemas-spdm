<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?= $_SESSION['company_name'] ?> - CSM</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <script>
            jQuery(document).ready(function ()
            {
                // Mostra e esconde selects da origem do endereço
                $('#f-g-subd').hide();
                $('#tipoEntidade').change(function () {
                    if ($('#tipoEntidade').val() === '1') {
                        $('#f-g-dpt').show();
                        $('#f-g-subd').hide();
                    } else if ($('#tipoEntidade').val() === '2') {
                        $('#f-g-dpt').hide();
                        $('#f-g-subd').show();
                    }
                });
                $('#24horas').change(function () {
                    if ($('#24horas').prop('checked')) {
                        $('#horarioin').prop('disabled', true);
                        $('#horariofi').prop('disabled', true);
                    } else {
                        $('#horarioin').prop('disabled', false);
                        $('#horariofi').prop('disabled', false);
                    }
                });
            });
        </script>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->

            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Cadastrar Horário de Atendimento</h1>
                        <ol class="breadcrumb">
                            <li><a href="index.html"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                            <li class="active"><i class="fa fa-edit"></i> Forms</li>
                        </ol>
                        <form class="form-horizontal" role="form">
                            <div class="form-group">
                                <label for="tipoEntidade" class="col-lg-2 control-label">Selecione o tipo de entidade que possui este Horário de Atendimento</label>
                                <div class="col-lg-9">
                                    <select id="tipoEntidade" class="form-control">
                                        <option value="1">Especialidade</option>

                                    </select>
                                </div>
                            </div>
                            <div id="f-g-dpt" class="form-group">
                                <label for="dpt" class="col-lg-2 control-label">Selecione a Especialidade que possui este Horário de Atendimento</label>
                                <div class="col-lg-9">
                                    <select id="dpt" class="form-control">
                                        <option value="1">Medicina</option>
                                        <option value="2">Enfermagem</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="semana" class="col-lg-2 control-label">Selecione o dia da semana(Os dias são adicionados um por um pois algumas especialidades não possuem horário fixo diariamente)</label>
                                <div class="col-lg-9">
                                    <select id="semana" class="form-control">
                                        <option value="DOM">Domingo</option>
                                        <option value="SEG">Segunda-feira</option>
                                        <option value="TER">Terça-feira</option>
                                        <option value="QUA">Quarta-feira</option>
                                        <option value="QUI">Quinta-feira</option>
                                        <option value="SEX">Sexta-feira</option>
                                        <option value="SAB">Sábado</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">                            
                                <div class="col-lg-offset-2 col-lg-9">
                                    <div class="checkbox">
                                        <label>
                                            <input id="24horas" type="checkbox">24 horas
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="horarioin" class="col-lg-2 control-label">Início</label>
                                <div class="col-lg-9">
                                    <input type="time" class="form-control" id="horarioin" placeholder="Horário de Início de Atendimento">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="horariofi" class="col-lg-2 control-label">Fim</label>
                                <div class="col-lg-9">
                                    <input type="time" class="form-control" id="horariofi" placeholder="Horário de Término de Atendimento">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-9">
                                    <button type="submit" class="btn btn-primary">Cadastrar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>
    </body>
</html>
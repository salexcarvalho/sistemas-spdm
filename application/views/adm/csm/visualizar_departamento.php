<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - CSM</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <script>
            jQuery(document).ready(function ()
            {
            });
        </script>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Visualizar Departamento</h1>
                        <ol class="breadcrumb">
                            <li><a href="lista"><i class="fa fa-list"></i> Listagem de Departamentos</a></li>
                            <li class="active"><i class="glyphicon glyphicon-file"></i> Visualização de Departamento</li>
                        </ol>
                        <form class="form-horizontal" role="form" method="POST" action="">
                            <div class="form-group">    
                                <div class="col-lg-6">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <span>Dados do departamento</span>
                                        </div>
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <label for="nmDepartamento" class="col-lg-2 control-label">Nome</label>
                                                <div class="col-lg-10">
                                                    <input type="text" class="form-control" id="nmDepartamento" name="nmDepartamento" placeholder="" disabled value="<?= $departamento[0]['nmDepartamento']; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="dsDepartamento" class="col-lg-2 control-label">Descrição</label>
                                                <div class="col-lg-10">
                                                    <textarea style="resize: vertical;" class="form-control" rows="3" id="dsDepartamento" name="dsDepartamento" disabled placeholder=""><?= $departamento[0]['dsDepartamento']; ?></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="chefeDep" class="col-lg-2 control-label">Chefe</label>
                                                <div class="col-lg-10">
                                                    <select class="form-control" id="chefeDep" name="chefeDep" disabled>
                                                        <option value="NULL">Nenhum</option>
                                                        <?php
                                                        foreach ($funcionarios as $funcionario) {
                                                            ?>
                                                            <option value="<?= $funcionario['idFuncionario']; ?>"><?= strtoupper($funcionario['nmFuncionario']); ?></option>
                                                            <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="endLogradouro" class="col-lg-2 control-label">Rua</label>
                                                <div class="col-lg-10">
                                                    <input type="text" class="form-control" id="endLogradouro" name="endLogradouro" placeholder="" disabled value="<?= $departamento[0]['endLogradouro']; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">    
                                                <label for="nuLogradouro" class="col-lg-2 control-label">Nº.</label>
                                                <div class="col-lg-2">
                                                    <input type="number" min="0" class="form-control" id="nuLogradouro" name="nuLogradouro" placeholder="" disabled value="<?= $departamento[0]['nuLogradouro']; ?>">
                                                </div>
                                                <label for="complemento" class="col-lg-2 control-label">Complemento</label>
                                                <div class="col-lg-6">
                                                    <input type="text" min="0" class="form-control" id="complemento" name="complemento" disabled value="<?= $departamento[0]['complemento']; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="CEP" class="col-lg-2 control-label">CEP</label>
                                                <div class="col-lg-3">
                                                    <input type="text" class="form-control" id="CEP" name="CEP" placeholder="" disabled value="<?= $departamento[0]['CEP']; ?>">
                                                </div>
                                                <label for="bairro" class="col-lg-1 control-label">Bairro</label>
                                                <div class="col-lg-6">
                                                    <input type="text" class="form-control" id="bairro" name="bairro" placeholder="" disabled value="<?= $departamento[0]['bairro']; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="cidade" class="col-lg-2 control-label">Cidade</label>
                                                <div class="col-lg-7">
                                                    <input type="text" class="form-control" id="cidade" name="cidade" placeholder="" disabled value="<?= $departamento[0]['cidade']; ?>">
                                                </div>
                                                <label for="estado" class="col-lg-1 control-label">Estado</label>
                                                <div class="col-lg-2">
                                                    <input type="text" class="form-control" id="estado" name="estado" placeholder="" disabled value="<?= $departamento[0]['estado']; ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- contato -->
                                <div class="col-lg-6">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">    
                                            <div class="col-lg-5">
                                                Contato
                                            </div>
                                            <div class="checkbox pull-right">
                                                <input type="checkbox" name="mostrarContatoNoSite" value="S" disabled <?= ($departamento[0]['mostrarContatoNoSite'] == 'S') ? 'checked' : ''; ?>> Mostrar contato no site
                                            </div> 
                                        </div>
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <label for="telefone" class="col-lg-2 control-label">Telefone</label>
                                                <div class="col-lg-5">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                                        <input type="tel" class="form-control" id="telefone" name="telefone" placeholder="" disabled value="<?= $departamento[0]['telefone']; ?>">
                                                    </div>
                                                </div>
                                                <label for="nuVOIP" class="col-lg-2 control-label">VOIP</label>
                                                <div class="col-lg-3">
                                                    <input type="number" min="0" class="form-control" id="nuVOIP" name="nuVOIP" placeholder="" disabled value="<?= $departamento[0]['nuVOIP']; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="email" class="col-lg-2 control-label">E-mail</label>
                                                <div class="col-lg-10">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                                        <input type="email" class="form-control" id="email" name="email" placeholder="" disabled value="<?= $departamento[0]['email']; ?>">
                                                    </div>
                                                </div>                                    
                                            </div>
                                            <div class="form-group">
                                                <label for="site" class="col-lg-2 control-label">Site</label>       
                                                <div class="col-lg-10">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-globe"></i></span>
                                                        <input type="url" class="form-control" id="site" name="site" placeholder="" disabled value="<?= $departamento[0]['site']; ?>">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="facebook" class="col-lg-2 control-label">Facebook</label>
                                                <div class="col-lg-10">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-facebook"></i></span>
                                                        <input type="url" class="form-control" id="facebook" name="facebook" placeholder="" disabled value="<?= $departamento[0]['facebook']; ?>">
                                                    </div></div>

                                            </div>
                                            <div class="form-group">
                                                <label for="twitter" class="col-lg-2 control-label">Twitter</label>
                                                <div class="col-lg-10">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-twitter"></i></span>
                                                        <input type="url" class="form-control" id="twitter" name="twitter" placeholder="" disabled value="<?= $departamento[0]['twitter']; ?>">
                                                    </div></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="linkedin" class="col-lg-2 control-label">LinkedIn</label>
                                                <div class="col-lg-10">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-linkedin"></i></span>
                                                        <input type="url" class="form-control" id="linkedin" name="linkedin" placeholder="" disabled value="<?= $departamento[0]['linkedIn']; ?>">
                                                    </div></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="googleplus" class="col-lg-2 control-label">Google+</label>
                                                <div class="col-lg-10">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-google-plus"></i></span>
                                                        <input type="url" class="form-control" id="googleplus" name="googleplus" placeholder="" disabled value="<?= $departamento[0]['googleplus']; ?>">
                                                    </div></div>
                                            </div>
                                            <div class="col-lg-12"><br></div>
                                            <div class="col-lg-12"><br></div>   
                                        </div>
                                    </div>
                                </div>
                        </form>

                        <div class="col-lg-12">
                            <a href="lista">
                                <button type="button" id="back" class="btn btn-primary" name="back">Voltar</button>
                            </a>
                        </div>
                    </div>
                </div>
                <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>

            </div>
        </div>
    </body>
</html>
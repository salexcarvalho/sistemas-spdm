<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - CSM</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

    </head>
    <body>
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"> Cadastrar Funcionário</h1>
                        <ol class="breadcrumb">                            
                            <li><a href="lista"><i class="fa fa-list"></i> Listagem de Funcionários</a></li>
                            <li class="active"><i class="glyphicon glyphicon-file"></i> Visualização de Funcionário</li>
                        </ol>
                        <form class="form-horizontal" role="form" method="POST" action="insert_funcionario" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="col-lg-6"><!-- INICIO DADOS DO FUNCIONARIO-->
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                Dados do funcionário
                                            </div>
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <label for="nmFuncionario" class="col-lg-2 control-label">Nome</label>
                                                    <div class="col-lg-10">
                                                        <input type="text" min="0" class="form-control" id="nmFuncionario" name="nmFuncionario" placeholder=""  required="">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="tipoFuncionarios" class="col-lg-2 control-label">Tipo de Funcionário</label>
                                                    <div class="col-lg-10">
                                                        <select class="form-control" id="tipoFuncionarios" name="tipoFuncionarios" required >
                                                            <option value=""></option>
                                                            <?php
                                                            foreach ($tipoFuncionarios as $tpFuncionario) {
                                                                ?>
                                                                <option value="<?= $tpFuncionario['idTipoFuncionario']; ?>"><?= $tpFuncionario['nmTipoFuncionario']; ?></option>
                                                                <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="sexo" class="col-lg-2 control-label">Sexo</label>
                                                    <div class="col-lg-10">
                                                        <label class="radio-inline">
                                                            <input type="radio" name="sexo" id="masc" value="M">
                                                            Masculino
                                                        </label>
                                                        <label class="radio-inline">
                                                            <input type="radio" name="sexo" id="femi" value="F">
                                                            Feminino
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="dtNascimento" class="col-lg-2 control-label">Data de Nascimento</label>
                                                    <div class="col-lg-10">
                                                        <input type="date" class="form-control" id="dtNascimento" name="dtNascimento" placeholder="">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="cdRegistroFuncional" class="col-lg-2 control-label">Registro Funcional</label>
                                                    <div class="col-lg-10">
                                                        <input type="text" class="form-control" id="cdRegistroFuncional" name="cdRegistroFuncional" placeholder="">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="nuConselho" class="col-lg-2 control-label">Número do Conselho</label>
                                                    <div class="col-lg-10">
                                                        <input type="text" class="form-control" id="nuConselho" name="nuConselho" placeholder="">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="miniCurriculum" class="col-lg-2 control-label">Mini Curriculum</label>
                                                    <div class="col-lg-10">
                                                        <textarea style="resize: none;" class="form-control" rows="5" id="miniCurriculum" name="miniCurriculum" placeholder=""></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- FIM DADOS FUNCIONARIO-->
                                    <div class="col-lg-6"><!-- INICIO CONTATO-->
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                Contato
                                                <div class="checkbox pull-right">
                                                    <span class="bold">Mostrar contato no site: </span>
                                                    <label><input type="radio" name="mostrarContatoNoSite" value="S">SIM</label>
                                                    <label><input type="radio" name="mostrarContatoNoSite" value="N"  checked >NÃO</label>
                                                </div>   
                                            </div>
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <label for="telefone" class="col-lg-2 control-label">Telefone : </label>
                                                    <div class="col-lg-5">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                                            <input type="tel" class="form-control" id="telefone" name="telefone" placeholder="" required>
                                                        </div>
                                                    </div>
                                                    <label for="nuVOIP" class="col-lg-2 control-label">VOIP : </label>
                                                    <div class="col-lg-3">
                                                        <input type="number" min="0" class="form-control" id="nuVOIP" name="nuVOIP" placeholder="">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="email" class="col-lg-2 control-label">E-mail : </label>
                                                    <div class="col-lg-10">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                                            <input type="email" class="form-control" id="email" name="email" placeholder="" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="site" class="col-lg-2 control-label">Site : </label>
                                                    <div class="col-lg-10">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="fa fa-globe"></i></span>
                                                            <input type="url" class="form-control" id="site" name="site" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="facebook" class="col-lg-2 control-label">Facebook : </label>
                                                    <div class="col-lg-10">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="fa fa-facebook"></i></span>
                                                            <input type="url" class="form-control" id="facebook" name="facebook" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="twitter" class="col-lg-2 control-label">Twitter : </label>
                                                    <div class="col-lg-10">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="fa fa-twitter"></i></span>
                                                            <input type="url" class="form-control" id="twitter" name="twitter" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="googleplus" class="col-lg-2 control-label">Google+ : </label>
                                                    <div class="col-lg-10">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="fa fa-google-plus"></i></span>
                                                            <input type="url" class="form-control" id="googleplus" name="googleplus" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="linkedin" class="col-lg-2 control-label">LinkedIn : </label>
                                                    <div class="col-lg-10">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="fa fa-linkedin"></i></span>
                                                            <input type="url" class="form-control" id="linkedIn" name="linkedIn" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12"><br></div>
                                                <div class="col-lg-12"><br></div>
                                                <div class="col-lg-12"><br></div>
                                                <div class="col-lg-12"><br></div>
                                                <div class="col-lg-12"><br></div>
                                                <div class="col-lg-12"><br></div>
                                            </div>
                                        </div>
                                    </div><!-- FIM CONTATO-->    
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="col-lg-12">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                Fotos do funcionário
                                            </div>
                                            <div class="panel-body">
                                                <div class="col-lg-12">
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <label for="ft1" class="col-lg-2 control-label">Foto 1</label>
                                                            <div class="col-lg-10">
                                                                <input type = "file" name = "ft1" id = "ft1">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="dsImagem1" class="col-lg-2 control-label">Descrição</label>
                                                            <div class="col-lg-10">
                                                                <textarea style="resize: vertical;" class="form-control" rows="5" id="dsImagem1" name="dsImagem1" placeholder=""></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <label for="ft2" class="col-lg-2 control-label">Foto 2</label>
                                                            <div class="col-lg-10">
                                                                <input type = "file" name = "ft2" id = "ft2">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="dsImagem2" class="col-lg-2 control-label">Descrição</label>
                                                            <div class="col-lg-10">
                                                                <textarea style="resize: vertical;" class="form-control" rows="5" id="dsImagem2" name="dsImagem2" placeholder=""></textarea>
                                                            </div>
                                                        </div>
                                                    </div>    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-5">
                                <button type="submit" id='btnatualizar' class="btn btn-primary">Cadastrar</button>
                                <a href="lista" id='back' class="btn btn-danger">Voltar</a>
                                <a href="lista" id='ok' class="col-lg-2 btn btn-success">Fechar</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>
    </body>
    <script>
        jQuery(document).ready(function ()
        {
            if (!$("#masc").prop("checked") && !$("#femi").prop("checked"))
            {
                $("#masc").prop("checked", true);
            }

            if ($('#msg').is(':visible'))
            {
                $('#ok').css('display', 'block');
                $('#btnatualizar').css('display', 'none')
                $('#back').css('display', 'none');
            } else {
                $('#ok').css('display', 'none');
            }
        });
    </script>
</html>
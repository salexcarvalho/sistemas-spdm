<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - CSM</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <!-- Modal -->
        <div class="modal fade" id="modal_excluir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Excluir Funcionário</h4>
                    </div>
                    <div class="modal-body" id="modal_delete_funcionario">Deseja excluir este funcionário ?</div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        <button type="button" class="btn btn-primary" id="modal_btn-excluir">Excluir</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal -->
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Lista de Funcionários</h1>
                        <ol class="breadcrumb">
                            <li><i class="fa fa-list"></i> Listagens</li>
                            <li class="active"><i class="fa fa-user-md"></i> Funcionários</li>
                        </ol>
                        <?php if (isset($warning)) : ?>
                            <div class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <?= $warning; ?></div>
                        <?php endif ?>
                        <?php if (isset($errors)) : ?>
                            <div style="padding: 15px;" class="alert alert-danger">Alguns erros foram encontrados, por favor confira os dados que você inseriu.
                                <ul class="errors">
                                    <?php foreach ($errors as $message): ?>
                                        <li><?php echo $message; ?></li>
                                    <?php endforeach ?>
                                </ul></div>
                        <?php endif ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <span>Funcionário</span>
                                <button type="submit" id="btn_novo_funcionario" class="pull-right btn btn-primary btn-xs" name="btn_novo_funcionario"><i class="glyphicon glyphicon-plus-sign"></i>  Adicionar novo</button>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover" id="funcionarios">
                                        <thead>
                                            <tr>
                                                <th width="80" style="text-align: center">#</th>
                                                <th>Nome</th>
                                                <th width="130">Sexo</th>
                                                <th width="170">RF</th>
                                                <th width="150">Vínculo</th>
                                                <th width="150">Código do Contato</th>
                                                <th width="130" style="text-align: center">Ações</th>
                                                <th width="100" style="text-align: center">Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            foreach ($funcionarios as $funcionario) {
                                                ?>
                                                <tr>
                                                    <td align="center"><?= $funcionario['idFuncionario']; ?></td>
                                                    <td><?= $funcionario['nmFuncionario']; ?></td>
                                                    <td><?= ($funcionario['sexo'] == 'M') ? 'Masculino' : 'Feminino'; ?></td>
                                                    <td><?= $funcionario['cdRegistroFuncional']; ?></td>
                                                    <td><?= $funcionario['nmTipoFuncionario']; ?></td>
                                                    <td><?= $funcionario['idContato']; ?></td>
                                                    <td align="center">
                                                        <form style="display: inline-block;" role="form" method="POST" action="visualizar_funcionario">
                                                            <input type="hidden" name="idFuncionario" value="<?= $funcionario['idFuncionario']; ?>">
                                                            <button type="submit" id="btnVisualizar" class="btn btn-default btn-xs" name="" title="Visualizar Funcionário">
                                                                <i class="glyphicon glyphicon-file"></i>
                                                            </button>
                                                        </form>
                                                        <form style="display: inline-block;" role="form" method="POST" action="editar_funcionario">
                                                            <input type="hidden" name="idFuncionario" value="<?= $funcionario['idFuncionario']; ?>">
                                                            <button type="submit" id="btnEditar" class="btn btn-default btn-xs" name="" idFuncionario="<?= $funcionario['idFuncionario']; ?>" title="Editar Funcionário">
                                                                <i class="glyphicon glyphicon-edit"></i>
                                                            </button>
                                                        </form>
                                                        <button type="submit" id="btnExcluir" class="btn btn-default btn-xs" name="btnExcluir" idFuncionario="<?= $funcionario['idFuncionario']; ?>" nmFuncionario="<?= $funcionario['nmFuncionario']; ?>" idContato="<?= $funcionario['idContato']; ?>" data-toggle="modal" data-target="#modal_excluir" data-placement="bottom" title="Excluir Funcionário" data-delay="1">
                                                            <input type="hidden" name="novoIdFuncionario" id="novoIdFuncionario" value="">
                                                            <i class="glyphicon glyphicon-trash"></i>

                                                        </button>
                                                    </td>
                                                    <td align="center">
                                                        <?php
                                                        if ($funcionario['ativo'] == "N") {
                                                            ?>
                                                            <button type="submit" id="btnAtivar" class="btn btn-danger btn-xs" idFuncionario="<?= $funcionario['idFuncionario']; ?>" name="btnAtivar" title="Ativar Funcionário">
                                                                <i class="glyphicon glyphicon-ban-circle"></i>
                                                            </button>
                                                            <?php
                                                        } else {
                                                            ?>
                                                            <button type="submit" id="btnAtivar" class="btn btn-success btn-xs" idFuncionario="<?= $funcionario['idFuncionario']; ?>" name="btnAtivar" title="Destivar Funcionário">
                                                                <i class="glyphicon glyphicon-ok"></i>
                                                            </button>
                                                            <?php
                                                        }
                                                        ?>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>
    </body>
    <script>
        jQuery(document).ready(function () {
            // Remove seleção de ativo no menu.
            $('.nav li').removeClass('active');
            $('#side-menu li').removeClass('active');
            // Ativa botão no menu.
            $('#listagens').addClass('collapse in');
            $('#lista-funcionarios a').addClass('active');
            $('#lista-listagens').addClass('active');
        });
    </script>
    <script>
        jQuery(document).ready(function ()
        {
            $('#funcionarios').dataTable(
                    {
                        "bAutoWidth": false,
                        "sPaginationType": "full_numbers",
                        "sDom": '<"H"Tlfr>t<"F"ip>',
                        "aaSorting": [[0, 'asc']],
                        "aoColumnDefs": [{"bSortable": false, "aTargets": [6, 7]}],
                        "oLanguage":
                                {
                                    "sLengthMenu": "Mostrar _MENU_ registros por página",
                                    "sZeroRecords": "Não encontramos nada - desculpe",
                                    "sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros.",
                                    "sInfoEmpty": "Mostrando 0 a 0 de 0 registros encontrados",
                                    "sInfoFiltered": "(filtrada a partir de _MAX_ total de registros encontrados)",
                                    "sSearch": " Buscar: ",
                                    "oPaginate": {
                                        "sFirst": "Início",
                                        "sPrevious": "Anterior",
                                        "sNext": "Próximo",
                                        "sLast": "Último"}
                                }

                    });

            var novoIdFuncionario;
            var idFuncionario;
            var nmFuncionario;
            var idContato;


            $("body").on("click", "button[name=btnExcluir]", function ()
            {
                novoIdFuncionario = "";
                idFuncionario = $(this).attr('idFuncionario');
                nmFuncionario = $(this).attr('nmFuncionario');
                idContato = $(this).attr('idContato');



                $('#modal_delete_funcionario').html("Deseja excluir o funcionário <b>\"" + nmFuncionario + "\"</b>?");
            });

            $("body").on("hidden.bs.modal", "#modal_excluir", function ()
            {
                window.location.replace("lista");
            });

            $("body").on("click", "#modal_btn-excluir", function ()
            {
                var btn = $(this);
                btn.text('Carregando');

                btn.removeClass('btn-primary');
                btn.removeClass('btn-danger');
                btn.removeClass('btn-success');
                btn.addClass('btn-primary');

                $('#modal_btn-excluir').prop('disabled', true);

                $.ajax("delete_funcionario",
                        {
                            type: "POST",
                            data:
                                    {
                                        idFuncionario: idFuncionario,
                                        novoIdFuncionario: novoIdFuncionario,
                                        idContato: idContato
                                    }

                        })
                        .done(function (data)
                        {
                            $('#modal_delete_funcionario').html('Funcionário excluído com sucesso');

                            btn.removeClass('btn-primary');
                            btn.addClass('btn-success');
                        })
                        .fail(function ()
                        {
                            $('#modal_delete_funcionario').html('Erro: ' + data);

                            btn.removeClass('btn-primary');
                            btn.addClass('btn-danger');
                        })
                        .always(function ()
                        {
                            btn.text('Excluído');
                        });
            });

            $("body").on("click", "#btn_novo_funcionario", function ()
            {
                window.location.href = "cadastro_funcionario";
            });
            $("body").on("click", "button[name=btnAtivar]", function ()
            {
                var idFuncionario = $(this).attr('idFuncionario');
                $.ajax("update_ativar_funcionario",
                        {
                            type: "POST",
                            data:
                                    {
                                        idFuncionario: idFuncionario
                                    }
                        })
                        .always(function ()
                        {
                            window.location.replace("lista");
                        });
            });

            $('.dataTables_filter label').addClass('pull-right');
        });
    </script>
    <style>
        .pagination
        {
            float: right !important;
        }
    </style>

</html>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - CSM</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>


        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <!-- Modal -->
        <div class="modal fade" id="modal_excluir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Excluir Departamento</h4>
                    </div>
                    <div class="modal-body" id="modal_delete_departamento">Deseja excluir esta departamento ?</div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        <button type="button" class="btn btn-primary" id="modal_btn-excluir">Excluir</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal -->
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Lista de Departamentos</h1>
                        <ol class="breadcrumb">
                            <li><i class="fa fa-list"></i> Listagens</li>
                            <li class="active"><i class="fa fa-hospital-o"></i> Departamentos</li>
                        </ol>
                        <?php if (isset($warning)) : ?>
                            <div class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <?= $warning; ?></div>
                        <?php endif ?>
                        <?php if (isset($errors)) : ?>
                            <div style="padding: 15px;" class="alert alert-danger">Alguns erros foram encontrados, por favor confira os dados que você inseriu.
                                <ul class="errors">
                                    <?php foreach ($errors as $message): ?>
                                        <li><?php echo $message; ?></li>
                                    <?php endforeach ?>
                                </ul></div>
                        <?php endif ?> 
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <span>Departamento</span>
                                <a href="cadastro_departamento">
                                    <button type="submit" id="btn_novo_departamento" class="pull-right btn btn-primary btn-xs" name="btn_novo_departamento"><i class="glyphicon glyphicon-plus-sign"></i>  Adicionar Novo</button>
                                </a>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover" id="departamentos">
                                        <thead>
                                            <tr>
                                                <th width="60" style="text-align: center">#</th>
                                                <th>Nome do Departamento</th>
                                                <th>Nome do Chefe</th>
                                                <th width="230" >Endereço</th>
                                                <th width="140" style="text-align: center">Ações</th>
                                                <th width="80" style="text-align: center">Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            foreach ($departamentos as $departamento) {
                                                ?>
                                                <tr>
                                                    <td align="center"><?= $departamento['idDepartamento']; ?></td>
                                                    <td><?= $departamento['nmDepartamento']; ?></td>
                                                    <td><?= $departamento['nmFuncionarioChefe']; ?></td>
                                                    <td><?= $departamento['endLogradouro']; ?><?= (!empty($departamento['nuLogradouro'])) ? ', ' . $departamento['nuLogradouro'] : ''; ?></td>
                                                    <td align="center">
                                                        <form style="display: inline-block;" role="form" method="GET" action="visualizar_departamento">
                                                            <input type="hidden" id="idDepartamento_<?= $departamento['idDepartamento']; ?>" name="idDepartamento" value="<?= $departamento['idDepartamento']; ?>">
                                                            <button type="submit" id="btnVisualizar" class="btn btn-default btn-xs" name="" title="Visualizar Departamento">
                                                                <i class="glyphicon glyphicon-file"></i>
                                                            </button>
                                                        </form>
                                                        <form style="display: inline-block;" role="form" method="POST" action="editar_departamento">
                                                            <input type="hidden" id="idDepartamento_<?= $departamento['idDepartamento']; ?>" name="idDepartamento" value="<?= $departamento['idDepartamento']; ?>">
                                                            <button  type="submit" id="btnEditar" class="btn btn-default btn-xs" name="btnEditar" idDepartamento="<?= $departamento['idDepartamento']; ?>" title="Editar Departamento">
                                                                <i class="glyphicon glyphicon-edit"></i>
                                                            </button>
                                                        </form>
                                                        <form style="display: inline-block;" role="form" method="POST" action="excluir_departamento">
                                                            <input type="hidden" id="idDepartamento_<?= $departamento['idDepartamento']; ?>" name="idDepartamento" value="<?= $departamento['idDepartamento']; ?>">
                                                            <button type="button" id="btnExcluir" class="btn btn-default btn-xs" name="btnExcluir" idDepartamento="<?= $departamento['idDepartamento']; ?>" nmDepartamento="<?= $departamento['nmDepartamento']; ?>" data-toggle="modal" data-target="#modal_excluir" data-placement="bottom" title="Excluir Departamento" data-delay="1">
                                                                <i class="glyphicon glyphicon-trash"></i>
                                                            </button>
                                                        </form>
                                                    </td>
                                                    <td align="center">
                                                        <form class="form-horizontal" role="form" method="POST" action="update_ativar_departamento">
                                                            <input type="hidden" name="idDepartamento" value="<?= $departamento['idDepartamento']; ?>">
                                                            <?php
                                                            if ($departamento['ativo'] == "N") {
                                                                ?>
                                                                <button type="submit" id="btnAtivar" class="btn btn-danger btn-xs" name="btnAtivar" title="Ativar Departamento">
                                                                    <i class="glyphicon glyphicon-ban-circle"></i>
                                                                </button>
                                                                <?php
                                                            } else {
                                                                ?>
                                                                <button type="submit" id="btnAtivar" class="btn btn-success btn-xs" name="btnAtivar" title="Destivar Departamento">
                                                                    <i class="glyphicon glyphicon-ok"></i>
                                                                </button>
                                                                <?php
                                                            }
                                                            ?>
                                                        </form>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>
    </body>
    <script>
        jQuery(document).ready(function () {
            // Remove seleção de ativo no menu.
            $('.nav li').removeClass('active');
            $('#side-menu li').removeClass('active');
            // Ativa botão no menu.
            $('#listagens').addClass('collapse in');
            $('#lista-departamentos a').addClass('active');
            $('#lista-listagens').addClass('active');
        });
    </script>
    <script>
        jQuery(document).ready(function ()
        {
            $('#departamentos').dataTable(
                    {
                        "bJQueryUI": false,
                        "aoColumnDefs": [{"bSortable": false, "aTargets": [4, 5]}],
                        "sPaginationType": "full_numbers",
                        "aaSorting": [[0, 'asc']],
                        "oLanguage":
                                {
                                    "sLengthMenu": "Mostrar _MENU_ registros por página",
                                    "sZeroRecords": "Não encontramos nada - desculpe",
                                    "sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros.",
                                    "sInfoEmpty": "Mostrando 0 a 0 de 0 registros encontrados",
                                    "sInfoFiltered": "(filtrada a partir de _MAX_ total de registros encontrados)",
                                    "sSearch": " Buscar: ",
                                    "oPaginate": {
                                        "sFirst": "Início",
                                        "sPrevious": "Anterior",
                                        "sNext": "Próximo",
                                        "sLast": "Último"}
                                }

                    });
            var idDepartamento;
            var nmDepartamento;
            $('button[name=btnExcluir]').on("click", function ()
            {
                idDepartamento = $(this).attr('idDepartamento');
                nmDepartamento = $(this).attr('nmDepartamento');
                $('#modal_delete_departamento').html("Deseja excluir o departamento <b>\"" + nmDepartamento + "\"</b> ?");
            });

            $('#modal_excluir').on('hidden.bs.modal', function ()
            {
                window.location.replace("lista");
            });

            $('#modal_btn-excluir').on("click", function ()
            {
                var btn = $(this);
                btn.text('loading');

                btn.removeClass('btn-primary');
                btn.removeClass('btn-danger');
                btn.removeClass('btn-success');
                btn.addClass('btn-primary');

                $('#modal_btn-excluir').prop('disabled', true);

                $.ajax("delete_departamento",
                        {
                            type: "POST",
                            data:
                                    {
                                        idDepartamento: idDepartamento
                                    }
                        })
                        .done(function (data)
                        {
                            $('#modal_delete_departamento').html('Departamento excluído com sucesso');

                            btn.removeClass('btn-primary');
                            btn.addClass('btn-success');
                        })
                        .fail(function ()
                        {
                            $('#modal_delete_departamento').html('Erro: ' + data);

                            btn.removeClass('btn-primary');
                            btn.addClass('btn-danger');
                        })
                        .always(function ()
                        {
                            btn.text('Excluído');
                        })
            });

            $('#btn_novo_departamento').on("click", function ()
            {
                window.location.href = "cadastro_departamento";
            });

            $("body").on("click", "button[name=btnVisualizar]", function ()
            {
                idDepartamento = $(this).attr('idDepartamento');
                window.location.href = "visualizar_departamento?idDepartamento=" + idDepartamento;
            });

            $('.dataTables_filter label').addClass('pull-right');
        });
    </script>
    <style>
        .pagination
        {
            float: right !important;
        }
    </style>
</html>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - CSM</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->

            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Cadastrar Departamento</h1>
                        <ol class="breadcrumb">
                            <li><i class="fa fa-list"></i> Listagens</li>
                            <li><a href="lista"><i class="fa fa-hospital-o"></i> Departamentos</a></li>
                            <li class="active"><i class="fa fa-plus-circle"></i> Adicionar Novo</li>
                        </ol>
                    </div>                    


                    <form class="form-horizontal" role="form" method="POST" action="insert_departamento">
                        <div class="form-group">
                            <div class="col-lg-12">
                                <div class="col-lg-6">
                                    <!--INICIO DADOS DO DEPARTAMENTO-->
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <span>Dados do Departamento</span>
                                        </div>
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <label for="nmDepartamento" class="col-lg-2 control-label">Nome</label>
                                                <div class="col-lg-10">
                                                    <input type="text" class="form-control" name="nmDepartamento" id="nmDepartamento" placeholder="" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="dsDepartamento" class="col-lg-2 control-label">Descrição</label>
                                                <div class="col-lg-10">
                                                    <textarea class="form-control" rows="3" name="dsDepartamento" id="dsDepartamento" placeholder=""></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="chefeDep" class="col-lg-2 control-label">Chefe</label>
                                                <div class="col-lg-10">
                                                    <select class="form-control" id="chefeDep" name="chefeDep" required>
                                                        <option value=''>Nenhum</option>
                                                        <?php
                                                        foreach ($funcionarios as $funcionario) {
                                                            ?>
                                                            <option value="<?= $funcionario['idFuncionario']; ?>"><?= strtoupper($funcionario['nmFuncionario']); ?></option>
                                                            <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="endLogradouro" class="col-lg-2 control-label">Rua</label>
                                                <div class="col-lg-10">
                                                    <input type="text" class="form-control" id="endLogradouro" name="endLogradouro" placeholder="">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="nuLogradouro" class="col-lg-2 control-label">Nº</label>
                                                <div class="col-lg-2">
                                                    <input type="number" min="0" class="form-control" id="nuLogradouro" name="nuLogradouro" placeholder="">
                                                </div>
                                                <label for="complemento" class="col-lg-2 control-label">Complemento</label>
                                                <div class="col-lg-6">
                                                    <input type="text" min="0" class="form-control" id="complemento" name="complemento" placeholder="">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="CEP" class="col-lg-2 control-label">CEP</label>
                                                <div class="col-lg-2">
                                                    <input type="text" class="form-control" id="CEP" name="CEP" placeholder="">
                                                </div>
                                                <label for="bairro" class="col-lg-2 control-label">Bairro</label>
                                                <div class="col-lg-6">
                                                    <input type="text" class="form-control" id="bairro" name="bairro" placeholder="">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="cidade" class="col-lg-2 control-label">Cidade</label>
                                                <div class="col-lg-7">
                                                    <input type="text" class="form-control" id="cidade" name="cidade" placeholder="">
                                                </div>
                                                <label for="estado" class="col-lg-1 control-label">Estado</label>
                                                <div class="col-lg-2">
                                                    <select class="form-control" id="estado" name="estado" required>
                                                        <option></option>
                                                        <option value="AC" id="AC" name="AC">AC</option>
                                                        <option value="AL" id="AL" name="AL">AL</option>
                                                        <option value="AM" id="AM" name="AM">AM</option>
                                                        <option value="AP" id="AP" name="AP">AP</option>
                                                        <option value="BA" id="BA" name="BA">BA</option>
                                                        <option value="CE" id="CE" name="CE">CE</option>
                                                        <option value="DF" id="DF" name="DF">DF</option>
                                                        <option value="ES" id="ES" name="ES">ES</option>
                                                        <option value="GO" id="GO" name="GO">GO</option>
                                                        <option value="MA" id="MA" name="MA">MA</option>
                                                        <option value="MG" id="MG" name="MG">MG</option>
                                                        <option value="MS" id="MS" name="MS">MS</option>
                                                        <option value="MT" id="MT" name="MT">MT</option>
                                                        <option value="PA" id="PA" name="PA">PA</option>
                                                        <option value="PB" id="PB" name="PB">PB</option>
                                                        <option value="PE" id="PE" name="PE">PE</option>
                                                        <option value="PI" id="PI" name="PI">PI</option>
                                                        <option value="RJ" id="RJ" name="RJ">RJ</option>
                                                        <option value="RN" id="RN" name="RN">RN</option>
                                                        <option value="RO" id="RO" name="RO">RO</option>
                                                        <option value="RR" id="RR" name="RR">RR</option>
                                                        <option value="RS" id="RS" name="RS">RS</option>
                                                        <option value="SC" id="SC" name="SC">SC</option>
                                                        <option value="SE" id="SE" name="SE">SE</option>                                
                                                        <option value="SP" id="SP" name="SP">SP</option>
                                                        <option value="TO" id="TO" name="TO">TO</option>
                                                    </select>                                            
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--FIM DADOS DO DEPARTAMENTO-->
                                <!--INICIO CONTATO-->
                                <div class="col-lg-6">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <span>Contato</span>
                                            <div class="checkbox pull-right">
                                                <span class="bold">Mostrar contato no site</span>
                                                <label><input type="radio" name="mostrarContatoNoSite" value="S"> SIM</label>
                                                <label><input type="radio" name="mostrarContatoNoSite" value="N"  checked > NÃO</label>
                                            </div> 
                                        </div>
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <label for="telefone" class="col-lg-2 control-label">Telefone</label>
                                                <div class="col-lg-6">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                                        <input type="tel" class="form-control" id="telefone" name="telefone" placeholder="" required>
                                                    </div>
                                                </div>
                                                <label for="nuVOIP" class="col-lg-1 control-label">VOIP</label>
                                                <div class="col-lg-3">
                                                    <input type="number" min="0" class="form-control" id="nuVOIP" name="nuVOIP" placeholder="">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="email" class="col-lg-2 control-label">E-mail</label>
                                                <div class="col-lg-10">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                                        <input type="email" class="form-control" id="email" name="email" placeholder="" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="site" class="col-lg-2 control-label">Site</label>
                                                <div class="col-lg-10">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-globe"></i></span>
                                                        <input type="url" class="form-control" id="site" name="site" placeholder="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="facebook" class="col-lg-2 control-label">Facebook</label>
                                                <div class="col-lg-10">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-facebook"></i></span>
                                                        <input type="url" class="form-control" id="facebook" name="facebook" placeholder="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="twitter" class="col-lg-2 control-label">Twitter</label>
                                                <div class="col-lg-10">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-twitter"></i></span>
                                                        <input type="url" class="form-control" id="twitter" name="twitter" placeholder="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="linkedin" class="col-lg-2 control-label">LinkedIn</label>
                                                <div class="col-lg-10">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-linkedin"></i></span>
                                                        <input type="url" class="form-control" id="linkedin" name="linkedin" placeholder="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="googleplus" class="col-lg-2 control-label">Google+</label>
                                                <div class="col-lg-10">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-google-plus"></i></span>
                                                        <input type="url" class="form-control" id="googleplus" name="googleplus" placeholder="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12"><br></div>
                                            <div class="col-lg-12"><br></div>
                                        </div>
                                    </div>
                                    <!--FIM CONTATO-->
                                </div>

                                <div class="col-lg-12">
                                    <div class="col-lg-5">
                                        <button type="submit" id="btncadastrar" class="btn btn-primary">Cadastrar</button>
                                        <a href="lista" id='back' class="btn btn-danger">Voltar</a>
                                        <a href="lista" id='ok' class="col-lg-2 btn btn-success">Fechar</a>
                                    </div>
                                </div>
                            </div>

                    </form>

                </div>
            </div>
        </div>
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>
    </body>
    <script>
        jQuery(document).ready(function ()
        {

            if ($('#msg').is(':visible'))
            {
                $('#ok').css('display', 'block');
                $('#btncadastrar').css('display', 'none')
                $('#back').css('display', 'none');
            } else {
                $('#ok').css('display', 'none');
            }
        });
    </script>
    <script>
        jQuery(document).ready(function () {
            // Remove seleção de ativo no menu.
            $('.nav li').removeClass('active');
            $('#side-menu li').removeClass('active');
            // Ativa botão no menu.
            $('#listagens').addClass('collapse in');
            $('#lista-departamentos a').addClass('active');
            $('#lista-listagens').addClass('active');
        });
    </script>
</html>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - CSM</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <script>
            jQuery(document).ready(function ()
            {
            });
        </script>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Editar Sites Referência</h1>
                        <ol class="breadcrumb">
                            <li><i class="fa fa-list"></i> Listagens</li>
                            <li><a href="lista"><i class="fa fa-stethoscope"></i> Serviços</a></li>
                            <li class="active"><i class="fa fa-file"></i> Editar Serviço</li>
                        </ol>
                        <form class="form-horizontal" role="form" method="POST" action="update_especialidade_resp_servico" novalidate>
                            <div class="form-group">
                                <label class="col-lg-offset-1 col-lg-5">Fotos da Unidade</label>
                            </div>
                            <?php
                            foreach ($imagens as $imagem) {
                                ?>
                                <div class="form-group">
                                    <label for="ft1" class="col-lg-1 control-label">Foto <?= $ft['idImagem']; ?></label>
                                    <div class="col-lg-5">
                                        <img src="<?= $ft['ft1']['caminhoImagem'] . $ft['ft1']['nmImagem']; ?>" class="img-thumbnail">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="dsImagem1" class="col-lg-1 control-label">Descrição</label>
                                    <div class="col-lg-5">
                                        <textarea style="resize: vertical;" class="form-control" rows="5" id="dsImagem1" name="dsImagem1" placeholder="" disabled><?= $ft['ft1']['dsImagem']; ?></textarea>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                            <div class="form-group">
                                <div class="col-lg-offset-1 col-lg-5">
                                    <input type="hidden" id="idServico" name="idServico" value="<?= $vars['idServico']; ?>">
                                    <button type="submit" class="btn btn-primary">Atualizar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>
    </body>
</html>
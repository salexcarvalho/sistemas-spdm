<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - CSM</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <script>
            jQuery(document).ready(function ()
            {
                // Remove seleção de ativo no menu.
                $('.navbar-nav li').removeClass('active');
                // Ativa botão no menu.
                //$('.navbar-nav #mn_home').addClass('active');

                // Fim de valicação 24h
            });
        </script>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Editar Serviço</h1>
                        <ol class="breadcrumb">
                            <li><i class="fa fa-list"></i> Listagens</li>
                            <li><a href="lista"><i class="fa fa-stethoscope"></i> Serviços</a></li>
                            <li class="active"><i class="fa fa-file"></i> Editar Serviço</li>
                        </ol>
                        <form class="form-horizontal" role="form" method="POST" action="update_chefe_servico">
                            <div class="form-group">
                                <label class="col-lg-offset-1 col-lg-5">Chefia do serviço</label>
                            </div>
                            <div class="form-group">
                                <label for="chefeDep" class="col-lg-1 control-label">Chefe</label>
                                <div class="col-lg-5">
                                    <select class="form-control" id="chefeDep" name="chefeDep">
                                        <option value="NULL">Nenhum</option>
                                        <?php
                                        foreach ($funcionarios as $funcionario) {
                                            ?>
                                            <option <?= ($servico[0]['idFuncionarioChefe'] == $funcionario['idFuncionario']) ? 'selected' : ''; ?> value="<?= $funcionario['idFuncionario']; ?>"><?= $funcionario['nmFuncionario']; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-offset-1 col-lg-5">
                                    <input type="hidden" id="idServico" name="idServico" value="<?= $servico[0]['idServico']; ?>">
                                    <button type="submit" class="btn btn-primary">Atualizar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>
    </body>
</html>
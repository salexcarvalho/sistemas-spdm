<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - CSM</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <script>
            jQuery(document).ready(function ()
            {
            });
        </script>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Víncular Funcionários a Especialidade</h1>
                        <ol class="breadcrumb">
                            <li><i class="fa fa-list"></i> Listagens</li>
                            <li><i class="fa fa-chain"></i> Vínculos</li>
                            <li><i class="fa fa-user-md"></i> Funcionários</li>
                            <li><a href="lista"><i class="fa fa-list-alt"></i> Especialidades</a></li>
                            <li class="active"><i class="fa fa-plus-circle"></i> Adicionar Novo</li>
                        </ol>
                        <form class="form-horizontal" role="form" method="POST" action="insert_especialidade_funcionario">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <?php if (isset($warning)) : ?>
                                        <p style="padding: 15px;" class="bg-success"><?= $warning; ?></p>
                                    <?php endif ?>
                                    <?php if (isset($errors)) : ?>
                                        <p style="padding: 15px;" class="bg-danger">Alguns erros foram encontrados, por favor confira os dados que você inseriu.</p>
                                        <ul class="errors">
                                            <?php foreach ($errors as $message): ?>
                                                <li><?php echo $message; ?></li>
                                            <?php endforeach ?>
                                        </ul>
                                    <?php endif ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-offset-1 col-lg-5">Funcionários pertencentes a Especialidade</label>
                            </div>
                            <div id="f-g-disci" class="form-group">
                                <label for="especialidadeper" class="col-lg-1 control-label">Especialidade</label>
                                <div class="col-lg-5">
                                    <select id="especialidadeper" name="especialidadeper" class="form-control">
                                        <?php
                                        foreach ($especialidades as $especialidade) {
                                            ?>
                                            <option value="<?= $especialidade['idEspecialidade']; ?>"><?= $especialidade['nmEspecialidade']; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div id="f-g-disci" class="form-group">
                                <label for="funcionariosper" class="col-lg-1 control-label">Funcionarios</label>
                                <div class="col-lg-5">
                                    <select id="funcionariosper" name="funcionariosper[]" class="form-control" size="20" multiple>
                                        <?php
                                        foreach ($funcionarios as $funcionario) {
                                            ?>
                                            <option value="<?= $funcionario['idFuncionario']; ?>"><?= strtoupper($funcionario['nmFuncionario']); ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-offset-1 col-lg-5">
                                    <button type="submit" class="btn btn-primary">Cadastrar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
<?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>
    </body>
</html>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - CSM</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <form class="form-horizontal" role="form">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header"> Visualizar Especialidade</h1>
                            <ol class="breadcrumb">                            
                                <li><a href="lista"><i class="fa fa-list"></i> Listagem de Especialidades</a></li>
                                <li class="active"><i class="glyphicon glyphicon-file"></i> Visualização de Especialidade</li>
                            </ol>
                        </div>
                    </div>                    

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="col-lg-6">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        Dados da Especialidade
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <label for="nmEspecialidade" class="col-lg-2 control-label">Nome</label>
                                            <div class="col-lg-10">
                                                <input type="text" class="form-control" id="nmEspecialidade" name="nmEspecialidade" value='<?= $especialidade[0]['nmEspecialidade'] ?>' disabled>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="nuCentroCusto" class="col-lg-2 control-label">Centro de Custo</label>
                                            <div class="col-lg-10">
                                                <input type="text" class="form-control" id="nuCentroCusto" name="nuCentroCusto" value='<?= $especialidade[0]['nuCentroCusto'] ?>' disabled>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="dsEspecialidade" class="col-lg-2 control-label">Descrição da Especialidade</label>
                                            <div class="col-lg-10">
                                                <textarea style="resize: vertical;" class="form-control" rows="3" id="dsEspecialidade" name="dsEspecialidade" disabled><?= $especialidade[0]['dsEspecialidade'] ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="chefeDep" class="col-lg-2 control-label">Chefe</label>
                                            <div class="col-lg-10">
                                                <input type='text' class="form-control" id="chefe" name="chefe" value='<?= $especialidade[0]['nmFuncionarioChefe'] ?>' disabled>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        Endereço
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <label for="endLogradouro" class="col-lg-2 control-label">Rua</label>
                                            <div class="col-lg-10">
                                                <input type="text" class="form-control" id="endLogradouro" name="endLogradouro" value='<?= $especialidade[0]['endLogradouro'] ?>'  disabled>
                                            </div>
                                        </div>                    
                                        <div class="form-group">
                                            <label for="nuLogradouro" class="col-lg-2 control-label">Número</label>
                                            <div class="col-lg-4">
                                                <input type="number" min="0" class="form-control" id="nuLogradouro" name="nuLogradouro" value='<?= $especialidade[0]['nuLogradouro'] ?>' disabled>
                                            </div>
                                            <label for="complemento" class="col-lg-2 control-label">Complemento</label>
                                            <div class="col-lg-4">
                                                <input type="text" class="form-control" id="complemento" name="complemento" value='<?= $especialidade[0]['complemento'] ?>' disabled>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="CEP" class="col-lg-2 control-label">CEP</label>
                                            <div class="col-lg-4">
                                                <input type="text" class="form-control" id="CEP" name="CEP" value='<?= $especialidade[0]['CEP'] ?>' disabled>
                                            </div>
                                            <label for="bairro" class="col-lg-2 control-label">Bairro</label>
                                            <div class="col-lg-4">
                                                <input type="text" class="form-control" id="bairro" name="bairro" value='<?= $especialidade[0]['bairro'] ?>' disabled>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="cidade" class="col-lg-2 control-label">Cidade</label>
                                            <div class="col-lg-4">
                                                <input type="text" class="form-control" id="cidade" name="cidade" value='<?= $especialidade[0]['cidade'] ?>' disabled>
                                            </div>
                                            <label for="estado" class="col-lg-2 control-label">Estado</label>
                                            <div class="col-lg-4">
                                                <input type="text" class="form-control" id="estado" name="estado" value='<?= $especialidade[0]['estado'] ?>' disabled>
                                            </div>
                                        </div>
                                        <div class="col-lg-12"><br></div>
                                        <div class="col-lg-12"><br></div>
                                        <div class="col-lg-12"><br></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="col-lg-6">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        Contato
                                        <div class="checkbox pull-right">
                                            <input type="checkbox" name="mostrarContatoNoSite" value="S" disabled <?= ($contato[0]['mostrarContatoNoSite'] == 'S') ? 'checked' : ''; ?>>Mostrar contato no site
                                        </div>                     
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <label for="telefone" class="col-lg-2 control-label" >telefone</label>
                                            <div class="col-lg-6">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                                    <input type="tel" class="form-control" id="telefone" name="telefone" value='<?= $contato[0]['telefone'] ?>' disabled>
                                                </div>
                                            </div>
                                            <label for="nuVOIP" class="col-lg-1 control-label">VOIP</label>
                                            <div class="col-lg-3">
                                                <input type="number" min="0" class="form-control" id="nuVOIP" name="nuVOIP" value='<?= $contato[0]['nuVOIP'] ?>' disabled>
                                            </div>
                                        </div>                    
                                        <div class="form-group">
                                            <label for="email" class="col-lg-2 control-label">E-mail</label>
                                            <div class="col-lg-10">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                                    <input type="email" class="form-control" id="email" name="email" value='<?= $contato[0]['email'] ?>' disabled>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="site" class="col-lg-2 control-label">Site</label>
                                            <div class="col-lg-10">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-globe"></i></span>
                                                    <input type="url" class="form-control" id="site" name="site" value='<?= $contato[0]['site'] ?>' disabled>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="facebook" class="col-lg-2 control-label">Facebook</label>
                                            <div class="col-lg-10">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-facebook"></i></span>
                                                    <input type="url" class="form-control" id="facebook" name="facebook" value='<?= $contato[0]['facebook'] ?>' disabled>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="twitter" class="col-lg-2 control-label">Twitter</label>
                                            <div class="col-lg-10">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-twitter"></i></span>
                                                    <input type="url" class="form-control" id="twitter" name="twitter" value='<?= $contato[0]['twitter'] ?>' disabled>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="googleplus" class="col-lg-2 control-label">Google+</label>
                                            <div class="col-lg-10">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-google-plus"></i></span>
                                                    <input type="url" class="form-control" id="googleplus" name="googleplus" value='<?= $contato[0]['googleplus'] ?>' disabled>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="linkedin" class="col-lg-2 control-label">LinkedIn</label>
                                            <div class="col-lg-10">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-linkedin"></i></span>
                                                    <input type="url" class="form-control" id="linkedIn" name="linkedIn" value='<?= $contato[0]['linkedIn'] ?>' disabled>
                                                </div>
                                            </div>
                                        </div> 
                                        <div class="col-lg-12"><br></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        Fotos
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <label for="ft1" class="col-lg-2 control-label">Foto 1</label>
                                            <div class="col-lg-10">
                                                <!--<input type="file" name="ft1" id="ft1">-->
                                                <input type="text" class="form-control" id="dsImagem1" name="dsImagem1" placeholder="Descrição da Foto" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="ft2" class="col-lg-2 control-label">Foto 2</label>
                                            <div class="col-lg-10">
                                                <!--<input type="file" name="ft2" id="ft2">-->
                                                <input type="text" class="form-control" id="dsImagem2" name="dsImagem2" placeholder="Descrição da Foto" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="ft3" class="col-lg-2 control-label">Foto 3</label>
                                            <div class="col-lg-10">
                                                <!--<input type="file" name="ft3" id="ft3">-->
                                                <input type="text" class="form-control" id="dsImagem3" name="dsImagem3" placeholder="Descrição da Foto" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="ft4" class="col-lg-2 control-label">Foto 4</label>
                                            <div class="col-lg-10">
                                                <!--<input type="file" name="ft4" id="ft4">-->
                                                <input type="text" class="form-control" id="dsImagem4" name="dsImagem4" placeholder="Descrição da Foto" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="ft5" class="col-lg-2 control-label">Foto 5</label>
                                            <div class="col-lg-10">
                                                <!--<input type="file" name="ft5" id="ft5">-->
                                                <input type="text" class="form-control" id="dsImagem5" name="dsImagem5" placeholder="Descrição da Foto" disabled>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="col-lg-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        Horário de Atendimento
                                    </div>
                                    <div class="panel-body">
                                        <table class="table table-responsive">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th width="13%">&nbsp;&nbsp;&nbsp;&nbsp;Domingo</th>
                                                    <th width="13%">&nbsp;&nbsp;&nbsp;&nbsp;Segunda</th>
                                                    <th width="13%">&nbsp;&nbsp;&nbsp;&nbsp;Terça</th>
                                                    <th width="13%">&nbsp;&nbsp;&nbsp;&nbsp;Quarta</th>
                                                    <th width="13%">&nbsp;&nbsp;&nbsp;&nbsp;Quinta</th>
                                                    <th width="13%">&nbsp;&nbsp;&nbsp;&nbsp;Sexta</th>
                                                    <th width="13%">&nbsp;&nbsp;&nbsp;&nbsp;Sábado</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <div class="form-group-sm">
                                                            <label class="col-lg-12">&nbsp</label>
                                                            <label class="col-lg-12 control-label">Início</label>
                                                            <label class="col-lg-12 control-label">Fim</label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group-sm">
                                                            <div class="col-lg-12">
                                                                <div class="checkbox">
                                                                    <label>
                                                                        <input id="24horas1" name="24horas1" type="checkbox" value="S"  
                                                                        <?php
                                                                        foreach ($horarios_atendimento as $horarioatd) {
                                                                            if ($horarioatd['diaSemana'] == 'DOM' && $horarioatd['horario24Horas'] == 'S') {
                                                                                echo 'checked';
                                                                            }
                                                                        }
                                                                        ?> >24 hs
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group-sm">
                                                            <div class="col-lg-12">
                                                                <input type="hidden" id="DOM" name="DOM" value="<?php
                                                                        foreach ($horarios_atendimento as $horarioatd) {
                                                                            if ($horarioatd['diaSemana'] == 'DOM' && $horarioatd['idEspecialidade'] == $especialidade[0]['idEspecialidade']) {
                                                                                echo $horarioatd['idHorarioAtendimento'];
                                                                            }
                                                                        }
                                                                        ?>">
                                                                <input type="time" class="form-control" id="horarioin1" name="horarioin1" placeholder="Horário de Início"  
                                                                       <?php
                                                                       foreach ($horarios_atendimento as $horarioatd) {
                                                                           if ($horarioatd['diaSemana'] == 'DOM' && !($horarioatd['horario24Horas'] == 'S')) {
                                                                               echo 'value="' . $horarioatd['horarioInicio'] . '"';
                                                                           }
                                                                       }
                                                                       ?>>
                                                            </div>
                                                            <div class="col-lg-12">
                                                                <input type="time" class="form-control" id="horariofi1" name="horariofi1" placeholder="Horário de Término"  
                                                                       <?php
                                                                       foreach ($horarios_atendimento as $horarioatd) {
                                                                           if ($horarioatd['diaSemana'] == 'DOM' && !($horarioatd['horario24Horas'] == 'S')) {
                                                                               echo 'value="' . $horarioatd['horarioFim'] . '"';
                                                                           }
                                                                       }
                                                                       ?>>
                                                            </div> 
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group-sm">
                                                            <div class="col-lg-12">
                                                                <div class="checkbox">
                                                                    <label>
                                                                        <input id="24horas2" name="24horas2" type="checkbox" value="S"  
                                                                               <?php
                                                                               foreach ($horarios_atendimento as $horarioatd) {
                                                                                   if ($horarioatd['diaSemana'] == 'SEG' && $horarioatd['horario24Horas'] == 'S') {
                                                                                       echo 'checked';
                                                                                   }
                                                                               }
                                                                               ?>>24 hs
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group-sm">
                                                            <div class="col-lg-12">
                                                                <input type="hidden" id="SEG" name="SEG" value="<?php
                                                                foreach ($horarios_atendimento as $horarioatd) {
                                                                    if ($horarioatd['diaSemana'] == 'SEG' && $horarioatd['idEspecialidade'] == $especialidade[0]['idEspecialidade']) {
                                                                        echo $horarioatd['idHorarioAtendimento'];
                                                                    }
                                                                }
                                                                ?>">
                                                                <input type="time" class="form-control" id="horarioin2" name="horarioin2" placeholder="Horário de Início"  
                                                                <?php
                                                                foreach ($horarios_atendimento as $horarioatd) {
                                                                    if ($horarioatd['diaSemana'] == 'SEG' && !($horarioatd['horario24Horas'] == 'S')) {
                                                                        echo 'value="' . $horarioatd['horarioInicio'] . '"';
                                                                    }
                                                                }
                                                                ?>>
                                                            </div>
                                                            <div class="col-lg-12">
                                                                <input type="time" class="form-control" id="horariofi2" name="horariofi2" placeholder="Horário de Término"  
<?php
foreach ($horarios_atendimento as $horarioatd) {
    if ($horarioatd['diaSemana'] == 'SEG' && !($horarioatd['horario24Horas'] == 'S')) {
        echo 'value="' . $horarioatd['horarioFim'] . '"';
    }
}
?>>
                                                            </div> 
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group-sm">
                                                            <div class="col-lg-12">
                                                                <div class="checkbox">
                                                                    <label>
                                                                        <input id="24horas3" name="24horas3" type="checkbox" value="S"  
<?php
foreach ($horarios_atendimento as $horarioatd) {
    if ($horarioatd['diaSemana'] == 'TER' && $horarioatd['horario24Horas'] == 'S') {
        echo 'checked';
    }
}
?>>24 hs
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group-sm">
                                                            <div class="col-lg-12">
                                                                <input type="hidden" id="TER" name="TER" value="<?php
                                                                foreach ($horarios_atendimento as $horarioatd) {
                                                                    if ($horarioatd['diaSemana'] == 'TER' && $horarioatd['idEspecialidade'] == $especialidade[0]['idEspecialidade']) {
                                                                        echo $horarioatd['idHorarioAtendimento'];
                                                                    }
                                                                }
                                                                ?>">
                                                                <input type="time" class="form-control" id="horarioin3" name="horarioin3" placeholder="Horário de Início"  
                                                                <?php
                                                                foreach ($horarios_atendimento as $horarioatd) {
                                                                    if ($horarioatd['diaSemana'] == 'TER' && !($horarioatd['horario24Horas'] == 'S')) {
                                                                        echo 'value="' . $horarioatd['horarioInicio'] . '"';
                                                                    }
                                                                }
                                                                ?>>
                                                            </div>
                                                            <div class="col-lg-12">
                                                                <input type="time" class="form-control" id="horariofi3" name="horariofi3" placeholder="Horário de Término"  
<?php
foreach ($horarios_atendimento as $horarioatd) {
    if ($horarioatd['diaSemana'] == 'TER' && !($horarioatd['horario24Horas'] == 'S')) {
        echo 'value="' . $horarioatd['horarioFim'] . '"';
    }
}
?>>
                                                            </div> 
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group-sm">
                                                            <div class="col-lg-12">
                                                                <div class="checkbox">
                                                                    <label>
                                                                        <input id="24horas4" name="24horas4" type="checkbox" value="S"  
<?php
foreach ($horarios_atendimento as $horarioatd) {
    if ($horarioatd['diaSemana'] == 'QUA' && $horarioatd['horario24Horas'] == 'S') {
        echo 'checked';
    }
}
?>>24 hs
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group-sm">
                                                            <div class="col-lg-12">
                                                                <input type="hidden" id="QUA" name="QUA" value="<?php
                                                                foreach ($horarios_atendimento as $horarioatd) {
                                                                    if ($horarioatd['diaSemana'] == 'QUA' && $horarioatd['idEspecialidade'] == $especialidade[0]['idEspecialidade']) {
                                                                        echo $horarioatd['idHorarioAtendimento'];
                                                                    }
                                                                }
?>">
                                                                <input type="time" class="form-control" id="horarioin4" name="horarioin4" placeholder="Horário de Início"  
                                                                <?php
                                                                       foreach ($horarios_atendimento as $horarioatd) {
                                                                           if ($horarioatd['diaSemana'] == 'QUA' && !($horarioatd['horario24Horas'] == 'S')) {
                                                                               echo 'value="' . $horarioatd['horarioInicio'] . '"';
                                                                           }
                                                                       }
                                                                       ?>>
                                                            </div>
                                                            <div class="col-lg-12">
                                                                <input type="time" class="form-control" id="horariofi4" name="horariofi4" placeholder="Horário de Término"  
                                                                        <?php
                                                                        foreach ($horarios_atendimento as $horarioatd) {
                                                                            if ($horarioatd['diaSemana'] == 'QUA' && !($horarioatd['horario24Horas'] == 'S')) {
                                                                                echo 'value="' . $horarioatd['horarioFim'] . '"';
                                                                            }
                                                                        }
                                                                        ?>>
                                                            </div> 
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group-sm">
                                                            <div class="col-lg-12">
                                                                <div class="checkbox">
                                                                    <label>
                                                                        <input id="24horas5" name="24horas5" type="checkbox" value="S"  
                                                                <?php
                                                                       foreach ($horarios_atendimento as $horarioatd) {
                                                                           if ($horarioatd['diaSemana'] == 'QUI' && $horarioatd['horario24Horas'] == 'S') {
                                                                               echo 'checked';
                                                                           }
                                                                       }
                                                                       ?>>24 hs
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group-sm">
                                                            <div class="col-lg-12">
                                                                <input type="hidden" id="QUI" name="QUI" value="<?php
                                                                foreach ($horarios_atendimento as $horarioatd) {
                                                                    if ($horarioatd['diaSemana'] == 'QUI' && $horarioatd['idEspecialidade'] == $especialidade[0]['idEspecialidade']) {
                                                                        echo $horarioatd['idHorarioAtendimento'];
                                                                    }
                                                                }
                                                                ?>">
                                                                <input type="time" class="form-control" id="horarioin5" name="horarioin5" placeholder="Horário de Início"  
<?php
foreach ($horarios_atendimento as $horarioatd) {
    if ($horarioatd['diaSemana'] == 'QUI' && !($horarioatd['horario24Horas'] == 'S')) {
        echo 'value="' . $horarioatd['horarioInicio'] . '"';
    }
}
?>>
                                                            </div>
                                                            <div class="col-lg-12">
                                                                <input type="time" class="form-control" id="horariofi5" name="horariofi5" placeholder="Horário de Término"  
                                                                        <?php
                                                                        foreach ($horarios_atendimento as $horarioatd) {
                                                                            if ($horarioatd['diaSemana'] == 'QUI' && !($horarioatd['horario24Horas'] == 'S')) {
                                                                                echo 'value="' . $horarioatd['horarioFim'] . '"';
                                                                            }
                                                                        }
                                                                        ?>>
                                                            </div> 
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group-sm">
                                                            <div class="col-lg-12">
                                                                <div class="checkbox">
                                                                    <label>
                                                                        <input id="24horas6" name="24horas6" type="checkbox" value="S"  
                                                                <?php
                                                                foreach ($horarios_atendimento as $horarioatd) {
                                                                    if ($horarioatd['diaSemana'] == 'SEX' && $horarioatd['horario24Horas'] == 'S') {
                                                                        echo 'checked';
                                                                    }
                                                                }
                                                                ?>>24 hs
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group-sm">
                                                            <div class="col-lg-12">
                                                                <input type="hidden" id="SEX" name="SEX" value="<?php
                                                                foreach ($horarios_atendimento as $horarioatd) {
                                                                    if ($horarioatd['diaSemana'] == 'SEX' && $horarioatd['idEspecialidade'] == $especialidade[0]['idEspecialidade']) {
                                                                        echo $horarioatd['idHorarioAtendimento'];
                                                                    }
                                                                }
                                                                ?>">
                                                                <input type="time" class="form-control" id="horarioin6" name="horarioin6" placeholder="Horário de Início"  
<?php
foreach ($horarios_atendimento as $horarioatd) {
    if ($horarioatd['diaSemana'] == 'SEX' && !($horarioatd['horario24Horas'] == 'S')) {
        echo 'value="' . $horarioatd['horarioInicio'] . '"';
    }
}
?>>
                                                            </div>
                                                            <div class="col-lg-12">
                                                                <input type="time" class="form-control" id="horariofi6" name="horariofi6" placeholder="Horário de Término"  
                                                                        <?php
                                                                               foreach ($horarios_atendimento as $horarioatd) {
                                                                                   if ($horarioatd['diaSemana'] == 'SEX' && !($horarioatd['horario24Horas'] == 'S')) {
                                                                                       echo 'value="' . $horarioatd['horarioFim'] . '"';
                                                                                   }
                                                                               }
                                                                               ?>>
                                                            </div> 
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group-sm">
                                                            <div class="col-lg-12">
                                                                <div class="checkbox">
                                                                    <label>
                                                                        <input id="24horas7" name="24horas7" type="checkbox" value="S"  
                                                                <?php
                                                                foreach ($horarios_atendimento as $horarioatd) {
                                                                    if ($horarioatd['diaSemana'] == 'SAB' && $horarioatd['horario24Horas'] == 'S') {
                                                                        echo 'checked';
                                                                    }
                                                                }
                                                                ?>>24 hs
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group-sm">
                                                            <div class="col-lg-12">
                                                                <input type="hidden" id="SAB" name="SAB" value="<?php
                                                                foreach ($horarios_atendimento as $horarioatd) {
                                                                    if ($horarioatd['diaSemana'] == 'SAB' && $horarioatd['idEspecialidade'] == $especialidade[0]['idEspecialidade']) {
                                                                        echo $horarioatd['idHorarioAtendimento'];
                                                                    }
                                                                }
                                                                ?>">
                                                                <input type="time" class="form-control" id="horarioin7" name="horarioin7" placeholder="Horário de Início"  
<?php
foreach ($horarios_atendimento as $horarioatd) {
    if ($horarioatd['diaSemana'] == 'SAB' && !($horarioatd['horario24Horas'] == 'S')) {
        echo 'value="' . $horarioatd['horarioInicio'] . '"';
    }
}
?>>
                                                            </div>
                                                            <div class="col-lg-12">
                                                                <input type="time" class="form-control" id="horariofi7" name="horariofi7" placeholder="Horário de Término"  
        <?php
        foreach ($horarios_atendimento as $horarioatd) {
            if ($horarioatd['diaSemana'] == 'SAB' && !($horarioatd['horario24Horas'] == 'S')) {
                echo 'value="' . $horarioatd['horarioFim'] . '"';
            }
        }
        ?>>
                                                            </div> 
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <a href="lista">
                                    <button id="back" type="button" class="btn btn-primary">Voltar</button>
                                </a>
                            </div>            
                        </div>
                    </div>    

                </form>
            </div>
        </div>
<?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>
    </body>
</html>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - CSM</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <script>
            jQuery(document).ready(function ()
            {
                $('#contatos').dataTable(
                        {
                            "bAutoWidth": false,
                            "aoColumns":
                                    [
                                        {sWidth: 'auto'},
                                        {sWidth: 'auto'},
                                        {sWidth: '180px'},
                                        {sWidth: 'auto'},
                                        {sWidth: 'auto'},
                                        {sWidth: 'auto'},
                                        {sWidth: 'auto'},
                                        {sWidth: '100px'},
                                        {sWidth: 'auto'}
                                    ],
                            "aoColumnDefs":
                                    [
                                        {
                                            "bSortable": false, "aTargets": [7, 8]
                                        }
                                    ],
                            "oLanguage":
                                    {
                                        "sLengthMenu": "Mostrar _MENU_ registros por página",
                                        "sZeroRecords": "Não encontramos nada - desculpe",
                                        "sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros.",
                                        "sInfoEmpty": "Mostrando 0 a 0 de 0 registros encontrados",
                                        "sInfoFiltered": "(filtrada a partir de _MAX_ total de registros encontrados)",
                                        "sSearch": " Buscar: "
                                    }

                        });

                var idContato;
                var tipoContato;
                //var nmContato;
                $("body").on("click", "button[name=btnExcluir]", function ()
                {
                    idContato = $(this).attr('idContato');
                    tipoContato = $(this).attr('tipoContato');
                    //nmContato = $(this).attr('nmContato');
                    $('#modal_delete_contato').html("Deseja excluir este contato ?");
                });

                $("body").on("hidden.bs.modal", "#modal_excluir", function ()
                {
                    window.location.replace("lista");
                });

                $("body").on("click", "#modal_btn-excluir", function ()
                {
                    var btn = $(this);
                    btn.text('Carregando...');

                    btn.removeClass('btn-primary');
                    btn.removeClass('btn-danger');
                    btn.removeClass('btn-success');
                    btn.addClass('btn-primary');

                    $('#modal_btn-excluir').prop('disabled', true);

                    $.ajax("delete_contato",
                            {
                                type: "POST",
                                data:
                                        {
                                            idContato: idContato,
                                            tipoContato: tipoContato
                                        }
                            })
                            .done(function ()
                            {
                                $('#modal_delete_contato').html('Contato excluído com sucesso');

                                btn.removeClass('btn-primary');
                                btn.addClass('btn-success');
                                btn.text('Excluído');
                            })
                            .fail(function ()
                            {
                                $('#modal_delete_contato').html('Ocorreu um erro na exclusão do contato de id "<b>' + idContato + '</b>"');

                                btn.removeClass('btn-primary');
                                btn.addClass('btn-danger');
                                btn.text('Falha');
                            });
                });

                $('.dataTables_filter label').addClass('pull-right');
            });
        </script>
        <style>
            .pagination
            {
                float: right !important;
            }
        </style>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <!-- Modal -->
        <div class="modal fade" id="modal_excluir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Excluir Serviço</h4>
                    </div>
                    <div class="modal-body" id="modal_delete_contato">Deseja excluir este contato ?</div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        <button type="button" class="btn btn-primary" id="modal_btn-excluir">Excluir</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal -->
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Lista de Contatos</h1>
                        <ol class="breadcrumb">
                            <li><i class="fa fa-list-alt"></i> Listagens</li>
                            <li class="active"><i class="fa fa-phone"></i> Contatos</li>
                        </ol>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Contato
                                <button disabled type="submit" id="btn_novo_contato" class="pull-right btn btn-primary btn-xs" name="btn_novo_contato"><i class="glyphicon glyphicon-plus-sign"></i>  Adicionar novo</button>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover" id="contatos">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Tipo</th>
                                                <th>Nome do Tipo</th>
                                                <th>Exibir</th>
                                                <th>Telefone</th>
                                                <th>VOIP</th>
                                                <th>E-mail</th>
                                                <th style="text-align: center">Ações</th>
                                                <th style="text-align: center">Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            foreach ($contatos as $contato) {
                                                ?>
                                                <tr>
                                                    <td><?= $contato['idContato']; ?></td>
                                                    <td><?= $contato['tipoContato']; ?></td>
                                                    <?php
                                                    if (!empty($contato['nmServico'])) {
                                                        ?>
                                                        <td><?= $contato['nmServico']; ?></td>
                                                        <?php
                                                    } elseif (!empty($contato['nmFuncionario'])) {
                                                        ?>
                                                        <td><?= $contato['nmFuncionario']; ?></td>
                                                        <?php
                                                    } elseif (!empty($contato['nmEspecialidade'])) {
                                                        ?>
                                                        <td><?= $contato['nmEspecialidade']; ?></td>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <td></td>
                                                        <?php
                                                    }
                                                    ?>
                                                    <td align="center">
                                                        <form class="form-horizontal" role="form" method="POST" action="update_mostrar_contato">
                                                            <input type="hidden" name="idContato" value="<?= $contato['idContato']; ?>">
                                                    <?php
                                                    if ($contato['mostrarContatoNoSite'] == "N") {
                                                        ?>
                                                                <button type="submit" id="btnAtivar" class="btn btn-danger btn-xs" name="btnAtivar" title="Mostrar Contato">
                                                                    <i class="glyphicon glyphicon-eye-close"></i>
                                                                </button>
        <?php
    } else {
        ?>
                                                                <button type="submit" id="btnAtivar" class="btn btn-success btn-xs" name="btnAtivar" title="Ocultar Contato">
                                                                    <i class="glyphicon glyphicon-eye-open"></i>
                                                                </button>
                                                                <?php
                                                            }
                                                            ?>
                                                        </form>
                                                    </td>
                                                    <td><?= $contato['telefone']; ?></td>
                                                    <td><?= $contato['nuVOIP']; ?></td>
                                                    <td><?= $contato['email']; ?></td>
                                                    <td align="center">
                                                        <form style="display: inline-block;" role="form" method="GET" action="visualizar_contato">
                                                            <input type="hidden" name="idContato" value="<?= $contato['idContato']; ?>">
                                                            <button type="submit" id="btnVisualizar" class="btn btn-default btn-xs" name="" title="Visualizar Contato">
                                                                <i class=" glyphicon glyphicon-file"></i>
                                                            </button>
                                                        </form>
                                                        <form style="display: inline-block;" role="form" method="GET" action="editar_contato">
                                                            <input type="hidden" name="idContato" value="<?= $contato['idContato']; ?>">
                                                            <button type="submit" id="btnEditar" class="btn btn-default btn-xs" name="" title="Editar Contato">
                                                                <i class=" glyphicon glyphicon-edit"></i>
                                                            </button>
                                                        </form>
                                                        <button type="button" id="btnExcluir" class="btn btn-default btn-xs" idContato="<?= $contato['idContato']; ?>" tipoContato="<?= $contato['tipoContato']; ?>" name="btnExcluir" data-toggle="modal" data-target="#modal_excluir" data-placement="bottom" title="Excluir Contato" data-delay="1">
                                                            <i class="glyphicon glyphicon-trash"></i>
                                                        </button>
                                                    </td>
                                                    <td align="center">
                                                        <form class="form-horizontal" role="form" method="POST" action="update_ativar_contato">
                                                            <input type="hidden" name="idContato" value="<?= $contato['idContato']; ?>">
    <?php
    if ($contato['ativo'] == "N") {
        ?>
                                                                <button type="submit" id="btnAtivar" class="btn btn-danger btn-xs" name="btnAtivar" title="Ativar Contato">
                                                                    <i class="glyphicon glyphicon-ban-circle"></i>
                                                                </button>
        <?php
    } else {
        ?>
                                                                <button type="submit" id="btnAtivar" class="btn btn-success btn-xs" name="btnAtivar" title="Destivar Contato">
                                                                    <i class="glyphicon glyphicon-ok"></i>
                                                                </button>
                                                                <?php
                                                            }
                                                            ?>
                                                        </form>
                                                    </td>
                                                </tr>
                                                            <?php
                                                        }
                                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>
    </body>
</html>
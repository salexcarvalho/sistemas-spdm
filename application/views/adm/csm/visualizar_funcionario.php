<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - CSM</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script>
            jQuery(document).ready(function ()
            {
                if (!$("#masc").prop("checked") && !$("#femi").prop("checked"))
                {
                    $("#masc").prop("checked", true);
                }
            });
        </script>
    </head>
    <body>
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <form class="form-horizontal" role="form">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header"> Visualizar Funcionário</h1>
                            <ol class="breadcrumb">                            
                                <li><a href="lista"><i class="fa fa-list"></i> Listagem de Funcionários</a></li>
                                <li class="active"><i class="glyphicon glyphicon-file"></i> Visualização de Funcionário</li>
                            </ol>

                            <div class="col-lg-6">
                                <div class="panel panel-default"> <!-- INICIO DO PAINEL DADOS DO FUNCIONARIO -->
                                    <div class="panel-heading">    
                                        Dados do funcionário
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <label for="nmFuncionario" class="col-lg-3 control-label">Nome:</label>
                                            <div class="col-lg-8">
                                                <input type="text" min="0" class="form-control" id="nmFuncionario" name="nmFuncionario" placeholder="" disabled value="<?= $funcionario[0]['nmFuncionario']; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="tipoFuncionarios" class="col-lg-3 control-label">Tipo de Funcionário:</label>
                                            <div class="col-lg-8">
                                                <select class="form-control" id="tipoFuncionarios" name="tipoFuncionarios" disabled>
                                                    <option value="NULL">Nenhum</option>
                                                    <?php
                                                    foreach ($tipoFuncionarios as $tpFuncionario) {
                                                        ?>
                                                        <option <?= ($funcionario[0]['idTipoFuncionario'] == $tpFuncionario['idTipoFuncionario']) ? 'selected' : ''; ?> value="<?= $tpFuncionario['idTipoFuncionario']; ?>"><?= $tpFuncionario['nmTipoFuncionario']; ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="sexo" class="col-lg-3 control-label">Sexo:</label>
                                            <div class="col-lg-8">
                                                <label class="radio-inline">
                                                    <input type="radio" name="sexo" id="masc" disabled value="M" <?= ($funcionario[0]['sexo'] == 'M') ? 'checked' : ''; ?>>
                                                    Masculino
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="sexo" id="femi" disabled value="F" <?= ($funcionario[0]['sexo'] == 'F') ? 'checked' : ''; ?>>
                                                    Feminino
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="dtNascimento" class="col-lg-3 control-label">Data de Nascimento:</label>
                                            <div class="col-lg-8">
                                                <input type="date" class="form-control" id="dtNascimento" name="dtNascimento" placeholder="" disabled value="<?= $funcionario[0]['dtNascimento']; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="cdRegistroFuncional" class="col-lg-3 control-label">Registro Funcional:</label>
                                            <div class="col-lg-8">
                                                <input type="text" class="form-control" id="cdRegistroFuncional" name="cdRegistroFuncional" placeholder="" disabled value="<?= $funcionario[0]['cdRegistroFuncional']; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="nuConselho" class="col-lg-3 control-label">Número do Conselho:</label>
                                            <div class="col-lg-8">
                                                <input type="text" class="form-control" id="nuConselho" name="nuConselho" placeholder="" disabled value="<?= $funcionario[0]['nuConselho']; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="miniCurriculum" class="col-lg-3 control-label">Mini Curriculum:</label>
                                            <div class="col-lg-8">
                                                <textarea style="resize: none;" class="form-control" rows="5" id="miniCurriculum" name="miniCurriculum" placeholder="" disabled><?= $funcionario[0]['miniCurriculum']; ?></textarea>
                                            </div>
                                        </div>
                                    </div></div> <!-- FIM DO PANEL DADOS DO FUNCIONÁRIO -->
                            </div>

                            <div class="col-lg-6">

                                <div class="panel panel-default"> <!-- INICIO DO PAINEL CONTATO -->
                                    <div class="panel-heading">
                                        Contato
                                        <div class="checkbox pull-right">
                                            <input type="checkbox" name="mostrarContatoNoSite" value="S" disabled <?= ($funcionario[0]['mostrarContatoNoSite'] == 'S') ? 'checked' : ''; ?>> Mostrar contato no site
                                        </div>
                                    </div>
                                    <div class="panel-body">    
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <label for="telefone" class="col-lg-2 control-label">Telefone:</label>
                                                <div class="col-lg-7">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                                        <input type="tel" class="form-control" id="telefone" name="telefone" disabled placeholder="" value="<?= $funcionario[0]['telefone']; ?>">
                                                    </div>
                                                </div>

                                                <label for="nuVOIP" class="col-lg-1 control-label">VOIP:</label>
                                                <div class="col-lg-2">
                                                    <input type="number" min="0" class="form-control" id="nuVOIP" name="nuVOIP" disabled placeholder="" value="<?= $funcionario[0]['nuVOIP']; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">        
                                                <label for="email" class="col-lg-2 control-label">E-mail:</label>
                                                <div class="col-lg-10">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                                        <input type="email" class="form-control" id="email" name="email" disabled placeholder="" value="<?= $funcionario[0]['email']; ?>">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="site" class="col-lg-2 control-label">Site:</label>
                                                <div class="col-lg-10">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-globe"></i></span>
                                                        <input type="text" class="form-control" id="site" name="site" disabled placeholder="" value="<?= $funcionario[0]['site']; ?>">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="facebook" class="col-lg-2 control-label">Facebook:</label>
                                                <div class="col-lg-10">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-facebook"></i></span>
                                                        <input type="text" class="form-control" id="facebook" name="facebook" disabled placeholder="" value="<?= $funcionario[0]['facebook']; ?>">
                                                    </div>
                                                </div>    
                                            </div>
                                            <div class="form-group">
                                                <label for="twitter" class="col-lg-2 control-label">Twitter:</label>
                                                <div class="col-lg-10">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-twitter"></i></span>
                                                        <input type="text" class="form-control" id="twitter" name="twitter" disabled placeholder="" value="<?= $funcionario[0]['twitter']; ?>">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="linkedin" class="col-lg-2 control-label">LinkedIn: </label>
                                                <div class="col-lg-10">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-linkedin"></i></span>
                                                        <input type="text" class="form-control" id="linkedin" name="linkedin" disabled placeholder="" value="<?= $funcionario[0]['linkedIn']; ?>">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="googleplus" class="col-lg-2 control-label">Google+:</label>
                                                <div class="col-lg-10">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-google-plus"></i></span>
                                                        <input type="text" class="form-control" id="googleplus" name="googleplus" disabled placeholder="" value="<?= $funcionario[0]['googleplus']; ?>">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12"><br></div>
                                            <div class="col-lg-12"><br></div><!-- espaços em branco -->



                                        </div></div></div><!-- FIM DO PANEL CONTATO -->   
                            </div>    
                        </div>
                    </div>
                    <div class="row">
                        <!-- inicio foto-->
                        <div class="col-lg-12">
                            <div class="col-lg-12">
                                <div class="panel panel-default"> <!-- INICIO DO PAINEL FOTOS -->
                                    <div class="panel-heading">
                                        Fotos
                                    </div> 
                                    <div class="panel-body">    
                                        <?php if ($imagens[0]['nmImagem'] != "") { ?>                           
                                            <div class="form-group">
                                                <label for="ft1" class="col-lg-1 control-label">Foto 1</label>
                                                <div class="col-lg-1">

                                                    <?php
                                                    foreach ($imagens as $imagem) {

                                                        $ex = explode('_', $imagem['nmImagem']);
                                                        $en = end($ex);
                                                        $ex2 = explode('.', $en);
                                                        if ($ex2[0] == 'ft1') {
                                                            ?>
                                                            <img src="<?= URL::base() ?><?= $imagem['caminhoImagem'] . $imagem['nmImagem']; ?>" title="Foto 1" class="img-thumbnail" width="150">
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </div>
                                                <div class="col-lg-4"><label for="dsImagem1" class="control-label">Descrição</label><br>                                    
                                                    <input style="text" class="form-control" id="dsImagem1" name="dsImagem1" placeholder="" disabled value="<?= $imagens[0]['dsImagem']; ?>">
                                                </div>
                                            </div>
                                        <?php } else { ?>

                                            <div class="form-group">
                                                <div class="col-lg-1"></div>
                                                <div class="col-lg-5">
                                                    Não existem fotos cadastradas.   
                                                </div>
                                            </div>
                                            <?php
                                        }
                                        if ($imagens[1]['nmImagem'] != "") {
                                            ?>

                                            <div class="form-group">
                                                <label for="ft2" class="col-lg-1 control-label">Foto 2</label>
                                                <div class="col-lg-1">

                                                    <?php
                                                    foreach ($imagens as $imagem) {

                                                        $ex = explode('_', $imagem['nmImagem']);
                                                        $en = end($ex);
                                                        $ex2 = explode('.', $en);
                                                        if ($ex2[0] == 'ft2') {
                                                            ?>
                                                            <img src="<?= URL::base() ?><?= $imagem['caminhoImagem'] . $imagem['nmImagem']; ?>" title="Foto 2" class="img-thumbnail" width="150">
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </div>
                                                <div class="col-lg-4"><label for="dsImagem2" class="control-label">Descrição</label><br>                                    
                                                    <input style="text" class="form-control" id="dsImagem2" name="dsImagem2" placeholder="" disabled value="<?= $imagens[1]['dsImagem']; ?>">
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <br>
                                        <br>
                                    </div>
                                    <!-- fim foto -->                    

                                </div>
                                <a href="lista">
                                    <button type="button" id="back" class="btn btn-primary" name="back"> Voltar</button>
                                </a>
                            </div>
                            </form>
                        </div>
                    </div>
            </div>
        </div>
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>
    </body>
</html>
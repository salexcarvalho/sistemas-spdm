<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - CSM</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script>
            jQuery(document).ready(function ()
            {
                if (!$("#masc").prop("checked") && !$("#femi").prop("checked"))
                {
                    $("#masc").prop("checked", true);
                }

                if ($('#msg').is(':visible'))
                {
                    $('#ok').css('display', 'block');
                    $('#btnatualizar').css('display', 'none');
                    $('#back').css('display', 'none');
                } else {
                    if ($('#erro').is(':visible')) {
                        $('#btnatualizar').css('display', 'none');
                    }
                    $('#ok').css('display', 'none');
                }

            });
        </script>

    </head>
    <body>
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"> Editar Funcionário</h1>
                        <ol class="breadcrumb">                            
                            <li><a href="lista"><i class="fa fa-list"></i> Listagem de Funcionários</a></li>
                            <li class="active"><i class="glyphicon glyphicon-file"></i> Edição de Funcionário</li>
                        </ol>
                        <div class="col-sm-12">
                            <?php if (isset($warning)) : ?>
                                <div id='msg' class="alert alert-success" role="alert"><?= $warning; ?>  <a href="lista">
                                    </a></div>
                            <?php endif ?>
                            <?php if (isset($errors)) : ?>
                                <div id="erro" class="alert alert-danger" role="alert">Alguns erros foram encontrados, por favor confira os dados que você inseriu.</div>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
                <form class="form-horizontal" role="form" action="update_funcionario" method="POST" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="col-lg-6">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        Dados do funcionário
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <label for="nmFuncionario" class="col-lg-2 control-label">Nome</label>
                                            <div class="col-lg-10">
                                                <input type="text" required min="0" class="form-control" id="nmFuncionario" name="nmFuncionario" placeholder="" value="<?= $funcionario[0]['nmFuncionario']; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="tipoFuncionarios" class="col-lg-2 control-label">Tipo de Funcionário</label>
                                            <div class="col-lg-10">
                                                <select class="form-control" id="tipoFuncionarios" name="tipoFuncionarios">
                                                    <option value="NULL">Nenhum</option>
                                                    <?php
                                                    foreach ($tipoFuncionarios as $tpFuncionario) {
                                                        ?>
                                                        <option <?= ($funcionario[0]['idTipoFuncionario'] == $tpFuncionario['idTipoFuncionario']) ? 'selected' : ''; ?> value="<?= $tpFuncionario['idTipoFuncionario']; ?>"><?= $tpFuncionario['nmTipoFuncionario']; ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="sexo" class="col-lg-2 control-label">Sexo</label>
                                            <div class="col-lg-4">
                                                <label class="radio-inline">
                                                    <input type="radio" name="sexo" id="masc" value="M" <?= ($funcionario[0]['sexo'] == 'M') ? 'checked' : ''; ?>>
                                                    Masculino
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="sexo" id="femi" value="F" <?= ($funcionario[0]['sexo'] == 'F') ? 'checked' : ''; ?>>
                                                    Feminino
                                                </label>
                                            </div>
                                            <label for="dtNascimento" class="col-lg-2 control-label">Nascimento</label>
                                            <div class="col-lg-4">
                                                <input type="date" class="form-control" id="dtNascimento" name="dtNascimento" placeholder="" value="<?= $funcionario[0]['dtNascimento']; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="cdRegistroFuncional" class="col-lg-2 control-label">Registro Funcional</label>
                                            <div class="col-lg-4">
                                                <input type="text" class="form-control" id="cdRegistroFuncional" name="cdRegistroFuncional" placeholder="" value="<?= $funcionario[0]['cdRegistroFuncional']; ?>">
                                            </div>
                                            <label for="nuConselho" class="col-lg-2 control-label">Número do Conselho</label>
                                            <div class="col-lg-4">
                                                <input type="text" class="form-control" id="nuConselho" name="nuConselho" placeholder="" value="<?= $funcionario[0]['nuConselho']; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="miniCurriculum" class="col-lg-2 control-label">Mini Curriculum</label>
                                            <div class="col-lg-10">
                                                <textarea style="resize: none;" class="form-control" rows="5" id="miniCurriculum" name="miniCurriculum" placeholder=""><?= $funcionario[0]['miniCurriculum']; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        Contato
                                        <div class="checkbox pull-right">
                                            <span class="bold">Mostrar contato no site: </span>
                                            <label><input type="radio" name="mostrarContatoNoSite" value="S" <?= ($funcionario[0]['mostrarContatoNoSite'] == 'S') ? 'checked' : ''; ?>>SIM</label>
                                            <label><input type="radio" name="mostrarContatoNoSite" value="N" <?= (($funcionario[0]['mostrarContatoNoSite'] == 'N') || $funcionario[0]['mostrarContatoNoSite'] == NULL) ? 'checked' : ''; ?>>NÃO</label>
                                        </div>    
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <label for="telefone" class="col-lg-2 control-label">Telefone : </label>
                                            <div class="col-lg-5">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                                    <input type="tel" required class="form-control" id="telefone" name="telefone" placeholder="" value="<?= $funcionario[0]['telefone'] ?>">
                                                </div>
                                            </div>
                                            <label for="nuVOIP" class="col-lg-2 control-label">VOIP : </label>
                                            <div class="col-lg-3">
                                                <input type="number" min="0" class="form-control" id="nuVOIP" name="nuVOIP" placeholder="" value="<?= $funcionario[0]['nuVOIP'] ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="email" class="col-lg-2 control-label">E-mail : </label>
                                            <div class="col-lg-10">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                                    <input type="email" required class="form-control" id="email" name="email" placeholder="" value="<?= $funcionario[0]['email'] ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="facebook" class="col-lg-2 control-label">Facebook : </label>
                                            <div class="col-lg-10">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-facebook"></i></span>
                                                    <input type="url" class="form-control" id="facebook" name="facebook" placeholder="" value="<?= $funcionario[0]['facebook'] ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="twitter" class="col-lg-2 control-label">Twitter : </label>
                                            <div class="col-lg-10">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-twitter"></i></span>
                                                    <input type="url" class="form-control" id="twitter" name="twitter" placeholder="" value="<?= $funcionario[0]['twitter'] ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="linkedIn" class="col-lg-2 control-label">LinkedIn : </label>
                                            <div class="col-lg-10">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-linkedin"></i></span>    
                                                    <input type="url" class="form-control" id="linkedIn" name="linkedIn" placeholder="" value="<?= $funcionario[0]['linkedIn'] ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="googleplus" class="col-lg-2 control-label">Google+ : </label>
                                            <div class="col-lg-10">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-google-plus"></i></span>
                                                    <input type="email" class="form-control" id="googleplus" name="googleplus" placeholder="" value="<?= $funcionario[0]['googleplus'] ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="site" class="col-lg-2 control-label">Site : </label>
                                            <div class="col-lg-10">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-globe"></i></span>
                                                    <input type="url" class="form-control" id="site" name="site" placeholder="" value="<?= $funcionario[0]['site'] ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row"><!-- inicio foto-->
                        <div class="col-lg-12">
                            <div class="col-lg-12"> 
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        Fotos do funcionário    
                                    </div>
                                    <div class="panel-body">
                                        <div class="col-lg-12">
                                            <div class="col-lg-1">    
                                                <img src="" width="85" height="85">
                                                <input type="hidden" name="idImagem1" id="idImagem1" value="" >
                                            </div>
                                            <div class="col-lg-5">    
                                                <textarea style="resize:none" rows="3" name="dsImagem1" id="dsImagem1" class="form-control"></textarea>
                                                <input type="file"  class="tumbnail" name="ft1" id="ft1" >
                                            </div>
                                            <div class="col-lg-1">    
                                                <img src="" width="85" height="85">
                                                <input type="hidden" name="idImagem1" id="idImagem1" value="" >
                                            </div>
                                            <div class="col-lg-5">    
                                                <textarea style="resize:none" rows="3" name="dsImagem1" id="dsImagem1" class="form-control"></textarea>
                                                <input type="file"  class="tumbnail" name="ft1" id="ft1" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>    
                    </div><!-- Fim foto-->
                    <div class="col-lg-12">
                        <div class="col-lg-12"><!-- Botões-->
                            <div class="form-group">
                                <input type="hidden" id="idFuncionario" name="idFuncionario" value="<?= $funcionario[0]['idFuncionario']; ?>">
                                <input type="hidden" id="idContato" name="idContato" value="<?= $funcionario[0]['idContato']; ?>">
                                <input type="hidden" id="tipoContato" name="tipoContato" value="<?= $funcionario[0]['tipoContato']; ?>">
                                <button type="submit" id='btnatualizar' class="btn btn-primary">Atualizar</button>
                                <a href="lista">
                                    <button type="button" id="back" class="btn btn-danger" name="back">Voltar</button>
                                    <button type="button" id="ok" class="btn btn-success" name="ok">Fechar</button>
                                </a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>
    </body>
</html>
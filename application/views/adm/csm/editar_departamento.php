<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - CSM</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"> Editar Departamento</h1>
                        <ol class="breadcrumb">                            
                            <li><a href="lista"><i class="fa fa-list"></i> Listagem de Departamentos</a></li>
                            <li class="active"><i class="glyphicon glyphicon-file"></i> Edição de Departamento</li>
                        </ol>
                        <div class="col-sm-12">
                            <?php if (isset($warning)) : ?>
                                <div id='msg' class="alert alert-success" role="alert"><?= $warning; ?>  <a href="lista">

                                    </a></div>

                            <?php endif ?>
                            <?php if (isset($errors)) : ?>
                                <spam style="padding: 15px;" class="bg-danger">Alguns erros foram encontrados, por favor confira os dados que você inseriu.</spam>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
                <form class="form-horizontal" role="form" action="update_departamento" method="POST" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="col-lg-6">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        Dados do Departamento
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <label for="nmDepartamento" class="col-lg-2 control-label">Nome</label>
                                            <div class="col-lg-10">
                                                <input type="text" required min="0" class="form-control" id="nmDepartamento" name="nmDepartamento" placeholder="" value="<?= $departamento[0]['nmDepartamento']; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="dsDepartamento" class="col-lg-2 control-label">Descrição</label>
                                            <div class="col-lg-10">
                                                <textarea style="resize: vertical;" class="form-control" rows="5" id="dsDepartamento" name="dsDepartamento" placeholder=""><?= $departamento[0]['dsDepartamento']; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="chefeDep" class="col-lg-2 control-label">Chefe</label>
                                            <div class="col-lg-10">
                                                <select required class="form-control" id="chefeDep" name="chefeDep">
                                                    <option value="<?= $departamento[0]['idFuncionarioChefe']; ?>"><?= $departamento[0]['nmFuncionarioChefe']; ?></option>
                                                    <?php
                                                    foreach ($funcionarios as $funcionario) {
                                                        ?>
                                                        <option value="<?= $funcionario['idFuncionario']; ?>"><?= strtoupper($funcionario['nmFuncionario']); ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="endLogradouro" class="col-lg-2 control-label">Logradouro</label>
                                            <div class="col-lg-7">
                                                <input type="text" required class="form-control" id="endLogradouro" name="endLogradouro" placeholder="" value="<?= $departamento[0]['endLogradouro']; ?>">
                                            </div>
                                            <label for="nuLogradouro" class="col-lg-1 control-label">Nº.</label>
                                            <div class="col-lg-2">
                                                <input type="number" min="0" class="form-control" id="nuLogradouro" name="nuLogradouro" placeholder="" value="<?= $departamento[0]['nuLogradouro']; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="complemento" class="col-lg-2 control-label">Complemento</label>
                                            <div class="col-lg-10">
                                                <textarea style="resize: vertical;" class="form-control" rows="5" id="complemento" name="complemento" placeholder="" ><?= $departamento[0]['complemento']; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="CEP" class="col-lg-2 control-label">CEP</label>
                                            <div class="col-lg-3">
                                                <input type="text" class="form-control" id="CEP" name="CEP" placeholder="" value="<?= $departamento[0]['CEP']; ?>">
                                            </div>
                                            <label for="bairro" class="col-lg-1 control-label">Bairro</label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control" id="bairro" name="bairro" placeholder="" value="<?= $departamento[0]['bairro']; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="cidade" class="col-lg-2 control-label">Cidade</label>
                                            <div class="col-lg-7">
                                                <input type="text" required class="form-control" id="cidade" name="cidade" placeholder="" value="<?= $departamento[0]['cidade']; ?>">
                                            </div>
                                            <label for="estado" class="col-lg-1 control-label">Estado</label>
                                            <div class="col-lg-2">
                                                <select class="form-control" id="estado" name="estado" required>
                                                    <option><?= $departamento[0]['estado'] ?></option>
                                                    <option value="AC" id="AC" name="AC">AC</option>
                                                    <option value="AL" id="AL" name="AL">AL</option>
                                                    <option value="AM" id="AM" name="AM">AM</option>
                                                    <option value="AP" id="AP" name="AP">AP</option>
                                                    <option value="BA" id="BA" name="BA">BA</option>
                                                    <option value="CE" id="CE" name="CE">CE</option>
                                                    <option value="DF" id="DF" name="DF">DF</option>
                                                    <option value="ES" id="ES" name="ES">ES</option>
                                                    <option value="GO" id="GO" name="GO">GO</option>
                                                    <option value="MA" id="MA" name="MA">MA</option>
                                                    <option value="MG" id="MG" name="MG">MG</option>
                                                    <option value="MS" id="MS" name="MS">MS</option>
                                                    <option value="MT" id="MT" name="MT">MT</option>
                                                    <option value="PA" id="PA" name="PA">PA</option>
                                                    <option value="PB" id="PB" name="PB">PB</option>
                                                    <option value="PE" id="PE" name="PE">PE</option>
                                                    <option value="PI" id="PI" name="PI">PI</option>
                                                    <option value="RJ" id="RJ" name="RJ">RJ</option>
                                                    <option value="RN" id="RN" name="RN">RN</option>
                                                    <option value="RO" id="RO" name="RO">RO</option>
                                                    <option value="RR" id="RR" name="RR">RR</option>
                                                    <option value="RS" id="RS" name="RS">RS</option>
                                                    <option value="SC" id="SC" name="SC">SC</option>
                                                    <option value="SE" id="SE" name="SE">SE</option>                                
                                                    <option value="SP" id="SP" name="SP">SP</option>
                                                    <option value="TO" id="TO" name="TO">TO</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        Contato
                                        <div class="checkbox pull-right">
                                            <span class="bold">Mostrar contato no site: </span>
                                            <label><input type="radio" name="mostrarContatoNoSite" value="S" <?= ($departamento[0]['mostrarContatoNoSite'] == 'S') ? 'checked' : ''; ?>>SIM</label>
                                            <label><input type="radio" name="mostrarContatoNoSite" value="N" <?= (($departamento[0]['mostrarContatoNoSite'] == 'N') || $departamento[0]['mostrarContatoNoSite'] == NULL) ? 'checked' : ''; ?>>NÃO</label>
                                        </div>    
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <label for="telefone" class="col-lg-2 control-label">Telefone : </label>
                                            <div class="col-lg-5">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                                    <input type="tel" required class="form-control" id="telefone" name="telefone" placeholder="" value="<?= $departamento[0]['telefone'] ?>">
                                                </div>
                                            </div>
                                            <label for="nuVOIP" class="col-lg-2 control-label">VOIP : </label>
                                            <div class="col-lg-3">
                                                <input type="number" min="0" class="form-control" id="nuVOIP" name="nuVOIP" placeholder="" value="<?= $departamento[0]['nuVOIP'] ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="email" class="col-lg-2 control-label">E-mail : </label>
                                            <div class="col-lg-10">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                                    <input type="email" required class="form-control" id="email" name="email" placeholder="" value="<?= $departamento[0]['email'] ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="facebook" class="col-lg-2 control-label">Facebook : </label>
                                            <div  class="col-lg-10">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-facebook"></i></span>
                                                    <input type="url" class="form-control" id="facebook" name="facebook" placeholder="" value="<?= $departamento[0]['facebook'] ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="twitter" class="col-lg-2 control-label">Twitter : </label>
                                            <div class="col-lg-10">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-twitter"></i></span>
                                                    <input type="url" class="form-control" id="twitter" name="twitter" placeholder="" value="<?= $departamento[0]['twitter'] ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="linkedin" class="col-lg-2 control-label">LinkedIn : </label>
                                            <div class="col-lg-10">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-linkedin"></i></span>    
                                                    <input type="url" class="form-control" id="linkedIn" name="linkedIn" placeholder="" value="<?= $departamento[0]['linkedIn'] ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="googleplus" class="col-lg-2 control-label">Google+ : </label>
                                            <div class="col-lg-10">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-google-plus"></i></span>
                                                    <input type="email" class="form-control" id="googleplus" name="googleplus" placeholder="" value="<?= $departamento[0]['googleplus'] ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="site" class="col-lg-2 control-label">Site : </label>
                                            <div class="col-lg-10">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-globe"></i></span>
                                                    <input type="url" class="form-control" id="site" name="site" placeholder="" value="<?= $departamento[0]['site'] ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12"><br></div>
                                        <div class="col-lg-12"><br></div>
                                        <div class="col-lg-12"><br></div>
                                        <div class="col-lg-12"><br></div>
                                        <div class="col-lg-12"><br></div>
                                        <div class="col-lg-12"><br></div>
                                        <div class="col-lg-12"><br></div>
                                        <div class="col-lg-12"><br></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="col-lg-12"><!-- Botões-->
                            <div class="form-group">
                                <input type="hidden" id="idDepartamento" name="idDepartamento" value="<?= $departamento[0]['idDepartamento']; ?>">
                                <input type="hidden" id="idContato" name="idContato" value="<?= $departamento[0]['idContato']; ?>">
                                <input type="hidden" id="idEndereco" name="idEndereco" value="<?= $departamento[0]['idEndereco']; ?>">
                                <button type="submit" id='btnatualizar' class="btn btn-primary">Atualizar</button>
                                <a href="lista">
                                    <button type="button" id="back" class="btn btn-danger" name="back">Voltar</button>
                                    <button type="button" id="ok" class="btn btn-success" name="ok">Fechar</button>
                                </a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>
    </body>
    <script>
        jQuery(document).ready(function ()
        {
            // Remove seleção de ativo no menu.
            $('.navbar-nav li').removeClass('active');

            if ($('#msg').is(':visible'))
            {
                $('#ok').css('display', 'block');
                $('#btnatualizar').css('display', 'none')
                $('#back').css('display', 'none');
            } else {
                $('#ok').css('display', 'none');
            }
        });
    </script>

</html>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - CSM</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <!-- Modal -->
        <div class="modal fade" id="modal_excluir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Excluir Especialidade</h4>
                    </div>
                    <div class="modal-body" id="modal_delete_especialidade">Deseja excluir este especialidade ?</div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        <button type="button" class="btn btn-primary" id="modal_btn-excluir">Excluir</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal -->
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Lista de Especialidades</h1>
                        <ol class="breadcrumb">
                            <li><i class="fa fa-list"></i> Listagens</li>
                            <li class="active"><i class="fa fa-hospital-o"></i> Especialidades</li>
                        </ol>
                        <?php if (isset($warning)) : ?>
                            <div class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <?= $warning; ?></div>
                        <?php endif ?>
                        <?php if (isset($errors)) : ?>
                            <div style="padding: 15px;" class="alert alert-danger">Alguns erros foram encontrados, por favor confira os dados que você inseriu.
                                <ul class="errors">
                                    <?php foreach ($errors as $message): ?>
                                        <li><?php echo $message; ?></li>
                                    <?php endforeach ?>
                                </ul></div>
                        <?php endif ?> 
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <span>Especialidade</span>
                                <a href="cadastro_especialidade">
                                    <button  type="submit" id="btn_novo_especialidade" class="pull-right btn btn-primary btn-xs" name="btn_novo_especialidade"><i class="glyphicon glyphicon-plus-sign"></i>  Adicionar novo</button>
                                </a>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover" id="especialidades">
                                        <thead>
                                            <tr>
                                                <th width="60">#</th>
                                                <th>Nome Especialidade</th>
                                                <th>Nome Chefe</th>
                                                <th width="140" style="text-align: center">Ações</th>
                                                <th width="80" style="text-align: center">Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            foreach ($especialidades as $especialidade) {
                                                ?>
                                                <tr>
                                                    <td><?= $especialidade['idEspecialidade']; ?></td>                                                    
                                                    <td><?= $especialidade['nmEspecialidade']; ?></td>
                                                    <td><?= $especialidade['nmFuncionarioChefe']; ?></td>
                                                    <td align="center">
                                                        <form style="display: inline-block;" role="form" method="GET" action="visualizar_especialidade">
                                                            <input type="hidden" name="idEspecialidade" value="<?= $especialidade['idEspecialidade']; ?>">
                                                            <input type="hidden" name="idContato" value="<?= $especialidade['idContato']; ?>">
                                                            <input type="hidden" name="tipoContato" value="Especialidade">
                                                            <button type="submit" id="btnVisualizar" class="btn btn-default btn-xs" name="" title="Visualizar Especialidade">
                                                                <i class=" glyphicon glyphicon-file"></i>
                                                            </button>
                                                        </form>
                                                        <form style="display: inline-block;" role="form" method="post" action="editar_especialidade">
                                                            <input type="hidden" name="idEspecialidade" value="<?= $especialidade['idEspecialidade']; ?>">
                                                            <input type="hidden" name="idContato" value="<?= $especialidade['idContato']; ?>">
                                                            <input type="hidden" name="tipoContato" value="Especialidade">
                                                            <button type="submit" id="btnEditar" class="btn btn-default btn-xs" name="" title="Editar Especialidade">
                                                                <i class=" glyphicon glyphicon-edit"></i>
                                                            </button>
                                                        </form>
                                                        <button type="submit" id="btnExcluir" class="btn btn-default btn-xs" name="btnExcluir" nmEspecialidade="<?= $especialidade['nmEspecialidade']; ?>" idContato="<?= $especialidade['idContato']; ?>" idEndereco="<?= $especialidade['idEndereco']; ?>" idEspecialidade="<?= $especialidade['idEspecialidade']; ?>" data-toggle="modal" data-target="#modal_excluir" data-placement="bottom"  data-delay="1"  title="Excluir Especialidade">
                                                            <i class="glyphicon glyphicon-trash"></i>
                                                        </button>
                                                    </td>
                                                    <td align="center">
                                                        <form class="form-horizontal" role="form" method="POST" action="update_ativar_especialidade">
                                                            <input type="hidden" name="idEspecialidade" value="<?= $especialidade['idEspecialidade']; ?>">
                                                            <?php
                                                            if ($especialidade['ativo'] == "N") {
                                                                ?>
                                                                <button type="submit" id="btnAtivar" class="btn btn-danger btn-xs" name="btnAtivar" title="Ativar Especialidade" disabled>
                                                                    <i class="glyphicon glyphicon-ban-circle"></i>
                                                                </button>
        <?php
    } else {
        ?>
                                                                <button type="submit" id="btnAtivar" class="btn btn-success btn-xs" name="btnAtivar" title="Destivar Especialidade" disabled>
                                                                    <i class="glyphicon glyphicon-ok"></i>
                                                                </button>
                                                                <?php
                                                            }
                                                            ?>
                                                        </form>
                                                    </td>
                                                </tr>
                                                            <?php
                                                        }
                                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>
    </body>
    <style>
        .pagination
        {
            float: right !important;
        }
    </style>
    <script>
        jQuery(document).ready(function () {
            // Remove seleção de ativo no menu.
            $('.nav li').removeClass('active');
            $('#side-menu li').removeClass('active');
            // Ativa botão no menu.
            $('#listagens').addClass('collapse in');
            $('#lista-especialidades a').addClass('active');
            $('#lista-listagens').addClass('active');
        });
    </script>
    <script>
        jQuery(document).ready(function ()
        {


            $('#especialidades').dataTable(
                    {
                        "bAutoWidth": false,
                        "sPaginationType": "full_numbers",
                        "sDom": '<"H"Tlfr>t<"F"ip>',
                        "aaSorting": [[0, 'asc']],
                        "aoColumnDefs":
                                [
                                    {
                                        "bSortable": false, "aTargets": [3, 4]
                                    }
                                ],
                        "oLanguage":
                                {
                                    "sLengthMenu": "Mostrar _MENU_ registros por página",
                                    "sZeroRecords": "Não encontramos nada - desculpe",
                                    "sInfo": "Mostrando _START_ a _END_ de _TOTAL_ registros.",
                                    "sInfoEmpty": "Mostrando 0 a 0 de 0 registros encontrados",
                                    "sInfoFiltered": "(filtrada a partir de _MAX_ total de registros encontrados)",
                                    "sSearch": " Buscar: ",
                                    "oPaginate": {
                                        "sFirst": "Início",
                                        "sPrevious": "Anterior",
                                        "sNext": "Próximo",
                                        "sLast": "Último"}
                                }

                    });

            var idEspecialidade;
            var nmEspecialidade;
            var idEndereco;
            var idContato;

            $("body").on("click", "button[name=btnExcluir]", function ()
            {
                idEspecialidade = $(this).attr('idEspecialidade');
                nmEspecialidade = $(this).attr('nmEspecialidade');
                idEndereco = $(this).attr('idEndereco');
                idContato = $(this).attr('idContato');

                $('#modal_delete_especialidade').html("Deseja excluir a especialidade <b>\"" + nmEspecialidade + "\"</b>?");
            });

            $("body").on("hidden.bs.modal", "#modal_excluir", function ()
            {
                window.location.replace("lista");
            });

            $("body").on("click", "#modal_btn-excluir", function ()
            {
                var btn = $(this);
                btn.text('Carregando...');

                btn.removeClass('btn-primary');
                btn.removeClass('btn-danger');
                btn.removeClass('btn-success');
                btn.addClass('btn-primary');


                $('#modal_btn-excluir').prop('disabled', true);

                $.ajax("delete_especialidade",
                        {
                            type: "POST",
                            data:
                                    {
                                        idEndereco: idEndereco,
                                        idEspecialidade: idEspecialidade,
                                        nmEspecialidade: nmEspecialidade,
                                        idContato: idContato

                                    }
                        })
                        .done(function (data)
                        {
                            $('#modal_delete_especialidade').html('Especialidade excluída com sucesso');

                            btn.removeClass('btn-primary');
                            btn.addClass('btn-success');
                            btn.text('Excluído');

                        })
                        .fail(function ()
                        {
                            $('#modal_delete_especialidade').html('Ocorreu um erro na exclusão da especialidade de id "<b>' + idEspecialidade + '</b>".');

                            btn.removeClass('btn-primary');
                            btn.addClass('btn-danger');
                            btn.text('Falha');

                        });
            });

            $('.dataTables_filter label').addClass('pull-right');
        });
    </script>
</html>
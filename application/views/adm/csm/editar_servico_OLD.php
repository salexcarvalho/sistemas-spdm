<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - CSM</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <script>
            jQuery(document).ready(function ()
            {
                // Remove seleção de ativo no menu.
                $('.navbar-nav li').removeClass('active');
                // Ativa botão no menu.
                //$('.navbar-nav #mn_home').addClass('active');


                //Ao inicialiar a página verifica se existem checkbox 24hrs marcados e caso sim desabilita os imputs dos horários respectivos à este.
                for (var v = 1; v < 8; v++) {
                    if ($('#24horas' + v).prop('checked'))
                    {
                        $('#horarioin' + v).val('00:00');
                        $('#horariofi' + v).val('00:00');
                        $('#horarioin' + v).prop('readonly', true);
                        $('#horariofi' + v).prop('readonly', true);
                    }
                    ;
                }
                ;


                // Validação 24h ao alterar o checkbox
                $('#24horas1').change(function () {
                    if ($('#24horas1').prop('checked'))
                    {
                        $('#horarioin1').val('00:00');
                        $('#horariofi1').val('00:00');
                        $('#horarioin1').prop('readonly', true);
                        $('#horariofi1').prop('readonly', true);

                    } else
                    {
                        $('#horarioin1').prop('readonly', false);
                        $('#horariofi1').prop('readonly', false);
                    }
                });
                $('#24horas2').change(function ()
                {
                    if ($('#24horas2').prop('checked'))
                    {
                        $('#horarioin2').val('00:00');
                        $('#horariofi2').val('00:00');
                        $('#horarioin2').prop('readonly', true);
                        $('#horariofi2').prop('readonly', true);

                    } else
                    {
                        $('#horarioin2').prop('readonly', false);
                        $('#horariofi2').prop('readonly', false);
                    }
                });
                $('#24horas3').change(function ()
                {
                    if ($('#24horas3').prop('checked'))
                    {
                        $('#horarioin3').val('00:00');
                        $('#horariofi3').val('00:00');
                        $('#horarioin3').prop('readonly', true);
                        $('#horariofi3').prop('readonly', true);

                    } else
                    {
                        $('#horarioin3').prop('readonly', false);
                        $('#horariofi3').prop('readonly', false);
                    }
                });
                $('#24horas4').change(function ()
                {
                    if ($('#24horas4').prop('checked'))
                    {
                        $('#horarioin4').val('00:00');
                        $('#horariofi4').val('00:00');
                        $('#horarioin4').prop('readonly', true);
                        $('#horariofi4').prop('readonly', true);

                    } else
                    {
                        $('#horarioin4').prop('readonly', false);
                        $('#horariofi4').prop('readonly', false);
                    }
                });
                $('#24horas5').change(function ()
                {
                    if ($('#24horas5').prop('checked'))
                    {
                        $('#horarioin5').val('00:00');
                        $('#horariofi5').val('00:00');
                        $('#horarioin5').prop('readonly', true);
                        $('#horariofi5').prop('readonly', true);

                    } else
                    {
                        $('#horarioin5').prop('readonly', false);
                        $('#horariofi5').prop('readonly', false);
                    }
                });
                $('#24horas6').change(function ()
                {
                    if ($('#24horas6').prop('checked'))
                    {
                        $('#horarioin6').val('00:00');
                        $('#horariofi6').val('00:00');
                        $('#horarioin6').prop('readonly', true);
                        $('#horariofi6').prop('readonly', true);

                    } else
                    {
                        $('#horarioin6').prop('readonly', false);
                        $('#horariofi6').prop('readonly', false);
                    }
                });
                $('#24horas7').change(function ()
                {
                    if ($('#24horas7').prop('checked'))
                    {
                        $('#horarioin7').val('00:00');
                        $('#horariofi7').val('00:00');
                        $('#horarioin7').prop('readonly', true);
                        $('#horariofi7').prop('readonly', true);

                    } else
                    {
                        $('#horarioin7').prop('readonly', false);
                        $('#horariofi7').prop('readonly', false);
                    }
                });

            });
        </script>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Editar Serviço</h1>

                        <form class="form-horizontal" role="form" method="POST" action="">
                            <div class="form-group">
                                <label class="col-lg-offset-1 col-lg-5">Dados da Especialidade responsável</label>
                            </div>
                            <!-- teste de tabs -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="home">Home</div>
                                <div role="tabpanel" class="tab-pane" id="profile">Perfil</div>
                                <div role="tabpanel" class="tab-pane" id="messages">Mensagens</div>
                                <div role="tabpanel" class="tab-pane" id="settings">Configurações</div>
                            </div>
                            <br>

                            <br>
                            <div class="form-group">
                                <label class="col-lg-offset-1 col-lg-5">Dados do serviço</label>
                            </div>
                            <div class="form-group">
                                <label for="nmServico" class="col-lg-1 control-label">Nome</label>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control" id="nmServico" name="nmServico" placeholder="" value="<?= $servico[0]['nmServico']; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="nuCentroCusto" class="col-lg-1 control-label">Centro de Custo</label>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control" id="nuCentroCusto" name="nuCentroCusto" placeholder="" value="<?= $servico[0]['nuCentroCusto']; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="dsServico" class="col-lg-1 control-label">Descrição do Serviço</label>
                                <div class="col-lg-5">
                                    <textarea style="resize: vertical;" class="form-control" rows="5" id="dsServico" name="dsServico" placeholder=""><?= $servico[0]['dsServico']; ?></textarea>
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="form-group">
                                <label class="col-lg-offset-1 col-lg-5">Dados da chefia deste serviço</label>
                            </div>
                            <div class="form-group">
                                <label for="chefeDep" class="col-lg-1 control-label">Chefe</label>
                                <div class="col-lg-5">
                                    <select class="form-control" id="chefeDep" name="chefeDep">
                                        <option value="NULL">Nenhum</option>
                                        <?php
                                        foreach ($funcionarios as $funcionario) {
                                            ?>
                                            <option <?= ($servico[0]['idFuncionarioChefe'] == $funcionario['idFuncionario']) ? 'selected' : ''; ?> value="<?= $funcionario['idFuncionario']; ?>"><?= strtoupper($funcionario['nmFuncionario']); ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="form-group">
                                <label class="col-lg-offset-1 col-lg-5">Fotos da Unidade</label>
                            </div>
                            <div class="form-group">
                                <label for="ft1" class="col-lg-1 control-label">Foto 1</label>
                                <div class="col-lg-5">
                                    <img src="<?= $ft['ft1']['caminhoImagem'] . $ft['ft1']['nmImagem']; ?>" class="img-thumbnail">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="dsImagem1" class="col-lg-1 control-label">Descrição</label>
                                <div class="col-lg-5">
                                    <textarea style="resize: vertical;" class="form-control" rows="5" id="dsImagem1" name="dsImagem1" placeholder=""><?= $ft['ft1']['dsImagem']; ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="ft2" class="col-lg-1 control-label">Foto 2</label>
                                <div class="col-lg-5">
                                    <img src="<?= $ft['ft2']['caminhoImagem'] . $ft['ft2']['nmImagem']; ?>" class="img-thumbnail">                                            
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="dsImagem2" class="col-lg-1 control-label">Descrição</label>
                                <div class="col-lg-5">
                                    <textarea style="resize: vertical;" class="form-control" rows="5" id="dsImagem2" name="dsImagem2" placeholder=""><?= $ft['ft2']['dsImagem']; ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="ft3" class="col-lg-1 control-label">Foto 3</label>
                                <div class="col-lg-5">
                                    <img src="<?= $ft['ft3']['caminhoImagem'] . $ft['ft3']['nmImagem']; ?>" class="img-thumbnail">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="dsImagem3" class="col-lg-1 control-label">Descrição</label>
                                <div class="col-lg-5">
                                    <textarea style="resize: vertical;" class="form-control" rows="5" id="dsImagem3" name="dsImagem3" placeholder=""><?= $ft['ft3']['dsImagem']; ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="ft4" class="col-lg-1 control-label">Foto 4</label>
                                <div class="col-lg-5">
                                    <input type="hidden" name="" value="<?= $ft['ft4']['idImagem']; ?>"
                                           <img src="<?= $ft['ft4']['caminhoImagem'] . $ft['ft4']['nmImagem']; ?>" class="img-thumbnail">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="dsImagem4" class="col-lg-1 control-label">Descrição</label>
                                <div class="col-lg-5">
                                    <textarea style="resize: vertical;" class="form-control" rows="5" id="dsImagem4" name="dsImagem4" placeholder=""><?= $ft['ft4']['dsImagem']; ?></textarea>
                                </div>
                            </div>
                            <!-- endereco -->
                            <div class="form-group">
                                <label class="col-lg-offset-1 col-lg-5">Endereço do Serviço</label>
                            </div>
                            <div class="form-group">
                                <label for="endLogradouro" class="col-lg-1 control-label">Rua</label>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control" id="endLogradouro" name="endLogradouro" placeholder="" value="<?= $servico[0]['endLogradouro']; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="nuLogradouro" class="col-lg-1 control-label">Número</label>
                                <div class="col-lg-5">
                                    <input type="number" min="0" class="form-control" id="nuLogradouro" name="nuLogradouro" placeholder="" value="<?= $servico[0]['nuLogradouro']; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="complemento" class="col-lg-1 control-label">Complemento</label>
                                <div class="col-lg-5">
                                    <textarea style="resize: vertical;" class="form-control" rows="5" id="complemento" name="complemento" placeholder=""><?= $servico[0]['complemento']; ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="CEP" class="col-lg-1 control-label">CEP</label>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control" id="CEP" name="CEP" placeholder="" value="<?= $servico[0]['CEP']; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="bairro" class="col-lg-1 control-label">Bairro</label>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control" id="bairro" name="bairro" placeholder="" value="<?= $servico[0]['bairro']; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="cidade" class="col-lg-1 control-label">Cidade</label>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control" id="cidade" name="cidade" placeholder="" value="<?= $servico[0]['cidade']; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="estado" class="col-lg-1 control-label">Estado</label>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control" id="estado" name="estado" placeholder="" value="<?= $servico[0]['estado']; ?>">
                                    <select class="form-control" id="estado" name="Estado" required>
                                        <option><?= $servico[0]['estado'] ?></option>
                                        <option value="AC" id="AC" name="AC">AC</option>
                                        <option value="AL" id="AL" name="AL">AL</option>
                                        <option value="AM" id="AM" name="AM">AM</option>
                                        <option value="AP" id="AP" name="AP">AP</option>
                                        <option value="BA" id="BA" name="BA">BA</option>
                                        <option value="CE" id="CE" name="CE">CE</option>
                                        <option value="DF" id="DF" name="DF">DF</option>
                                        <option value="ES" id="ES" name="ES">ES</option>
                                        <option value="GO" id="GO" name="GO">GO</option>
                                        <option value="MA" id="MA" name="MA">MA</option>
                                        <option value="MG" id="MG" name="MG">MG</option>
                                        <option value="MS" id="MS" name="MS">MS</option>
                                        <option value="MT" id="MT" name="MT">MT</option>
                                        <option value="PA" id="PA" name="PA">PA</option>
                                        <option value="PB" id="PB" name="PB">PB</option>
                                        <option value="PE" id="PE" name="PE">PE</option>
                                        <option value="PI" id="PI" name="PI">PI</option>
                                        <option value="RJ" id="RJ" name="RJ">RJ</option>
                                        <option value="RN" id="RN" name="RN">RN</option>
                                        <option value="RO" id="RO" name="RO">RO</option>
                                        <option value="RR" id="RR" name="RR">RR</option>
                                        <option value="RS" id="RS" name="RS">RS</option>
                                        <option value="SC" id="SC" name="SC">SC</option>
                                        <option value="SE" id="SE" name="SE">SE</option>                                
                                        <option value="SP" id="SP" name="SP">SP</option>
                                        <option value="TO" id="TO" name="TO">TO</option>
                                    </select>
                                </div>
                            </div>
                            <br>
                            <br>
                            <!-- fim endereco -->
                            <div class="form-group">
                                <label class="col-lg-offset-1 col-lg-5">Horário de Atendimento</label>
                            </div>
                            <br>

                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Domingo</th>
                                        <th>Segunda</th>
                                        <th>Terça</th>
                                        <th>Quarta</th>
                                        <th>Quinta</th>
                                        <th>Sexta</th>
                                        <th>Sábado</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <div class="form-group">                            
                                                <div class="col-lg-offset-1 col-lg-5">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="hidden" id="DOM" name="DOM" value="
                                                            <?php
                                                            foreach ($horarios_atendimento as $horarioatd) {
                                                                if ($horarioatd['diaSemana'] == 'DOM' && $horarioatd['idServico'] == $servico[0]['idServico']) {
                                                                    echo $horarioatd['idHorarioAtendimento'];
                                                                }
                                                            }
                                                            ?>">
                                                            <input id="24horas1" name="24horas1" type="checkbox" value="S" 
                                                            <?php
                                                            foreach ($horarios_atendimento as $horarioatd) {
                                                                if ($horarioatd['diaSemana'] == 'DOM' && $horarioatd['horario24Horas'] == 'S') {
                                                                    echo 'checked';
                                                                }
                                                            }
                                                            ?>>24 horas
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="horarioin1" class="col-lg-1 control-label">Início</label>
                                                <div class="col-lg-5">
                                                    <input type="time" required class="form-control" id="horarioin1" name="horarioin1" placeholder="Horário de Início de Atendimento" 
                                                    <?php
                                                    foreach ($horarios_atendimento as $horarioatd) {
                                                        if ($horarioatd['diaSemana'] == 'DOM' && !($horarioatd['horario24Horas'] == 'S')) {
                                                            echo 'value="' . $horarioatd['horarioInicio'] . '"';
                                                        }
                                                    }
                                                    ?>>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="horariofi1" class="col-lg-1 control-label">Fim</label>
                                                <div class="col-lg-5">
                                                    <input type="time" required class="form-control" id="horariofi1" name="horariofi1" placeholder="Horário de Término de Atendimento" 
                                                    <?php
                                                    foreach ($horarios_atendimento as $horarioatd) {
                                                        if ($horarioatd['diaSemana'] == 'DOM' && !($horarioatd['horario24Horas'] == 'S')) {
                                                            echo 'value="' . $horarioatd['horarioFim'] . '"';
                                                        }
                                                    }
                                                    ?>>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">                            
                                                <div class="col-lg-offset-1 col-lg-5">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="hidden" id="SEG" name="SEG" value="
                                                            <?php
                                                            foreach ($horarios_atendimento as $horarioatd) {
                                                                if ($horarioatd['diaSemana'] == 'SEG' && $horarioatd['idServico'] == $servico[0]['idServico']) {
                                                                    echo $horarioatd['idHorarioAtendimento'];
                                                                }
                                                            }
                                                            ?>">
                                                            <input id="24horas2" name="24horas2" type="checkbox" value="S" 
                                                            <?php
                                                            foreach ($horarios_atendimento as $horarioatd) {
                                                                if ($horarioatd['diaSemana'] == 'SEG' && $horarioatd['horario24Horas'] == 'S') {
                                                                    echo 'checked';
                                                                }
                                                            }
                                                            ?>>24 horas
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="horarioin2" class="col-lg-1 control-label">Início</label>
                                                <div class="col-lg-5">
                                                    <input type="time" required class="form-control" id="horarioin2" name="horarioin2" placeholder="Horário de Início de Atendimento"
                                                    <?php
                                                    foreach ($horarios_atendimento as $horarioatd) {
                                                        if ($horarioatd['diaSemana'] == 'SEG' && !($horarioatd['horario24Horas'] == 'S')) {
                                                            echo 'value="' . $horarioatd['horarioInicio'] . '"';
                                                        }
                                                    }
                                                    ?>>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="horariofi2" class="col-lg-1 control-label">Fim</label>
                                                <div class="col-lg-5">
                                                    <input type="time" required class="form-control" id="horariofi2" name="horariofi2" placeholder="Horário de Término de Atendimento"
                                                    <?php
                                                    foreach ($horarios_atendimento as $horarioatd) {
                                                        if ($horarioatd['diaSemana'] == 'SEG' && !($horarioatd['horario24Horas'] == 'S')) {
                                                            echo 'value="' . $horarioatd['horarioFim'] . '"';
                                                        }
                                                    }
                                                    ?>>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">                            
                                                <div class="col-lg-offset-1 col-lg-5">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="hidden" id="TER" name="TER" value="
                                                            <?php
                                                            foreach ($horarios_atendimento as $horarioatd) {
                                                                if ($horarioatd['diaSemana'] == 'TER' && $horarioatd['idServico'] == $servico[0]['idServico']) {
                                                                    echo $horarioatd['idHorarioAtendimento'];
                                                                }
                                                            }
                                                            ?>">
                                                            <input id="24horas3" name="24horas3" type="checkbox" value="S" 
                                                            <?php
                                                            foreach ($horarios_atendimento as $horarioatd) {
                                                                if ($horarioatd['diaSemana'] == 'TER' && $horarioatd['horario24Horas'] == 'S') {
                                                                    echo 'checked';
                                                                }
                                                            }
                                                            ?>>24 horas
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="horarioin3" class="col-lg-1 control-label">Início</label>
                                                <div class="col-lg-5">
                                                    <input type="time" required class="form-control" id="horarioin3" name="horarioin3" placeholder="Horário de Início de Atendimento"                                             
                                                    <?php
                                                    foreach ($horarios_atendimento as $horarioatd) {
                                                        if ($horarioatd['diaSemana'] == 'TER' && !($horarioatd['horario24Horas'] == 'S')) {
                                                            echo 'value="' . $horarioatd['horarioInicio'] . '"';
                                                        }
                                                    }
                                                    ?>>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="horariofi3" class="col-lg-1 control-label">Fim</label>
                                                <div class="col-lg-5">
                                                    <input type="time" required class="form-control" id="horariofi3" name="horariofi3" placeholder="Horário de Término de Atendimento"                                             
                                                    <?php
                                                    foreach ($horarios_atendimento as $horarioatd) {
                                                        if ($horarioatd['diaSemana'] == 'TER' && !($horarioatd['horario24Horas'] == 'S')) {
                                                            echo 'value="' . $horarioatd['horarioFim'] . '"';
                                                        }
                                                    }
                                                    ?>>
                                                </div>
                                            </div>
                                        </td>
                                        <td><div class="form-group">                            
                                                <div class="col-lg-offset-1 col-lg-5">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="hidden" id="QUA" name="QUA" value="
                                                            <?php
                                                            foreach ($horarios_atendimento as $horarioatd) {
                                                                if ($horarioatd['diaSemana'] == 'QUA' && $horarioatd['idServico'] == $servico[0]['idServico']) {
                                                                    echo $horarioatd['idHorarioAtendimento'];
                                                                }
                                                            }
                                                            ?>">
                                                            <input id="24horas4" name="24horas4" type="checkbox" value="S" 
                                                            <?php
                                                            foreach ($horarios_atendimento as $horarioatd) {
                                                                if ($horarioatd['diaSemana'] == 'QUA' && $horarioatd['horario24Horas'] == 'S') {
                                                                    echo 'checked';
                                                                }
                                                            }
                                                            ?>>24 horas
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="horarioin4" class="col-lg-1 control-label">Início</label>
                                                <div class="col-lg-5">
                                                    <input type="time" required class="form-control" id="horarioin4" name="horarioin4" placeholder="Horário de Início de Atendimento"                                             
                                                    <?php
                                                    foreach ($horarios_atendimento as $horarioatd) {
                                                        if ($horarioatd['diaSemana'] == 'QUA' && !($horarioatd['horario24Horas'] == 'S')) {
                                                            echo 'value="' . $horarioatd['horarioInicio'] . '"';
                                                        }
                                                    }
                                                    ?>>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="horariofi4" class="col-lg-1 control-label">Fim</label>
                                                <div class="col-lg-5">
                                                    <input type="time" required class="form-control" id="horariofi4" name="horariofi4" placeholder="Horário de Término de Atendimento"                                             
                                                    <?php
                                                    foreach ($horarios_atendimento as $horarioatd) {
                                                        if ($horarioatd['diaSemana'] == 'QUA' && !($horarioatd['horario24Horas'] == 'S')) {
                                                            echo 'value="' . $horarioatd['horarioFim'] . '"';
                                                        }
                                                    }
                                                    ?>>
                                                </div>
                                            </div></td>
                                        <td><div class="form-group">
                                                <div class="col-lg-offset-1 col-lg-5">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="hidden" id="QUI" name="QUI" value="
                                                            <?php
                                                            foreach ($horarios_atendimento as $horarioatd) {
                                                                if ($horarioatd['diaSemana'] == 'QUI' && $horarioatd['idServico'] == $servico[0]['idServico']) {
                                                                    echo $horarioatd['idHorarioAtendimento'];
                                                                }
                                                            }
                                                            ?>">
                                                            <input id="24horas5" name="24horas5" type="checkbox" value="S" 
                                                            <?php
                                                            foreach ($horarios_atendimento as $horarioatd) {
                                                                if ($horarioatd['diaSemana'] == 'QUI' && $horarioatd['horario24Horas'] == 'S') {
                                                                    echo 'checked';
                                                                }
                                                            }
                                                            ?>>24 horas
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="horarioin5" class="col-lg-1 control-label">Início</label>
                                                <div class="col-lg-5">
                                                    <input type="time" required class="form-control" id="horarioin5" name="horarioin5" placeholder="Horário de Início de Atendimento"                                             
                                                    <?php
                                                    foreach ($horarios_atendimento as $horarioatd) {
                                                        if ($horarioatd['diaSemana'] == 'QUI' && !($horarioatd['horario24Horas'] == 'S')) {
                                                            echo 'value="' . $horarioatd['horarioInicio'] . '"';
                                                        }
                                                    }
                                                    ?>>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="horariofi5" class="col-lg-1 control-label">Fim</label>
                                                <div class="col-lg-5">
                                                    <input type="time" required class="form-control" id="horariofi5" name="horariofi5" placeholder="Horário de Término de Atendimento"                                             
                                                    <?php
                                                    foreach ($horarios_atendimento as $horarioatd) {
                                                        if ($horarioatd['diaSemana'] == 'QUI' && !($horarioatd['horario24Horas'] == 'S')) {
                                                            echo 'value="' . $horarioatd['horarioFim'] . '"';
                                                        }
                                                    }
                                                    ?>>
                                                </div>
                                            </div></td>
                                        <td><div class="form-group">                            
                                                <div class="col-lg-offset-1 col-lg-5">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="hidden" id="SEX" name="SEX" value="
                                                            <?php
                                                            foreach ($horarios_atendimento as $horarioatd) {
                                                                if ($horarioatd['diaSemana'] == 'SEX' && $horarioatd['idServico'] == $servico[0]['idServico']) {
                                                                    echo $horarioatd['idHorarioAtendimento'];
                                                                }
                                                            }
                                                            ?>">
                                                            <input id="24horas6" name="24horas6" type="checkbox" value="S" 
                                                            <?php
                                                            foreach ($horarios_atendimento as $horarioatd) {
                                                                if ($horarioatd['diaSemana'] == 'SEX' && $horarioatd['horario24Horas'] == 'S') {
                                                                    echo 'checked';
                                                                }
                                                            }
                                                            ?>>24 horas
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="horarioin6" class="col-lg-1 control-label">Início</label>
                                                <div class="col-lg-5">
                                                    <input type="time" required class="form-control" id="horarioin6" name="horarioin6" placeholder="Horário de Início de Atendimento" 
                                                    <?php
                                                    foreach ($horarios_atendimento as $horarioatd) {
                                                        if ($horarioatd['diaSemana'] == 'SEX' && !($horarioatd['horario24Horas'] == 'S')) {
                                                            echo 'value="' . $horarioatd['horarioInicio'] . '"';
                                                        }
                                                    }
                                                    ?>>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="horariofi6" class="col-lg-1 control-label">Fim</label>
                                                <div class="col-lg-5">
                                                    <input type="time" required class="form-control" id="horariofi6" name="horariofi6" placeholder="Horário de Término de Atendimento" 
                                                    <?php
                                                    foreach ($horarios_atendimento as $horarioatd) {
                                                        if ($horarioatd['diaSemana'] == 'SEX' && !($horarioatd['horario24Horas'] == 'S')) {
                                                            echo 'value="' . $horarioatd['horarioFim'] . '"';
                                                        }
                                                    }
                                                    ?>>
                                                </div>
                                            </div></td>
                                        <td><div class="form-group">                            
                                                <div class="col-lg-offset-1 col-lg-5">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="hidden" id="SAB" name="SAB" value="
                                                            <?php
                                                            foreach ($horarios_atendimento as $horarioatd) {
                                                                if ($horarioatd['diaSemana'] == 'SAB' && $horarioatd['idServico'] == $servico[0]['idServico']) {
                                                                    echo $horarioatd['idHorarioAtendimento'];
                                                                }
                                                            }
                                                            ?>">
                                                            <input id="24horas7" name="24horas7" type="checkbox" value="S" 
                                                            <?php
                                                            foreach ($horarios_atendimento as $horarioatd) {
                                                                if ($horarioatd['diaSemana'] == 'SAB' && $horarioatd['horario24Horas'] == 'S') {
                                                                    echo 'checked';
                                                                }
                                                            }
                                                            ?>>24 horas
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="horarioin7" class="col-lg-1 control-label">Início</label>
                                                <div class="col-lg-5">
                                                    <input type="time" required class="form-control" id="horarioin7" name="horarioin7" placeholder="Horário de Início de Atendimento"                                             
                                                    <?php
                                                    foreach ($horarios_atendimento as $horarioatd) {
                                                        if ($horarioatd['diaSemana'] == 'SAB' && !($horarioatd['horario24Horas'] == 'S')) {
                                                            echo 'value="' . $horarioatd['horarioInicio'] . '"';
                                                        }
                                                    }
                                                    ?>>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="horariofi7" class="col-lg-1 control-label">Fim</label>
                                                <div class="col-lg-5">
                                                    <input type="time" required class="form-control" id="horariofi7" name="horariofi7" placeholder="Horário de Término de Atendimento"                                             
                                                    <?php
                                                    foreach ($horarios_atendimento as $horarioatd) {
                                                        if ($horarioatd['diaSemana'] == 'SAB' && !($horarioatd['horario24Horas'] == 'S')) {
                                                            echo 'value="' . $horarioatd['horarioFim'] . '"';
                                                        }
                                                    }
                                                    ?>>
                                                </div>
                                            </div></td>

                                    </tr>
                                </tbody>
                            </table>

                            <!-- contato -->
                            <div class="form-group">
                                <label class="col-lg-offset-1 col-lg-5">Contato</label>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-offset-1 col-lg-5">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="mostrarContatoNoSite" value="S" <?= ($servico[0]['mostrarContatoNoSite'] == 'S') ? 'checked' : ''; ?>>Mostrar contato no site
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="telefone" class="col-lg-1 control-label">Telefone</label>
                                <div class="col-lg-5">
                                    <input type="tel" class="form-control" id="telefone" name="telefone" placeholder="" value="<?= $servico[0]['telefone']; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="nuVOIP" class="col-lg-1 control-label">VOIP</label>
                                <div class="col-lg-5">
                                    <input type="number" min="0" class="form-control" id="nuVOIP" name="nuVOIP" placeholder="" value="<?= $servico[0]['nuVOIP']; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="email" class="col-lg-1 control-label">E-mail</label>
                                <div class="col-lg-5">
                                    <input type="email" class="form-control" id="email" name="email" placeholder="" value="<?= $servico[0]['email']; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="site" class="col-lg-1 control-label">Site</label>
                                <div class="col-lg-5">
                                    <input type="url" class="form-control" id="site" name="site" placeholder="" value="<?= $servico[0]['site']; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="facebook" class="col-lg-1 control-label">Facebook</label>
                                <div class="col-lg-5">
                                    <input type="url" class="form-control" id="facebook" name="facebook" placeholder="" value="<?= $servico[0]['facebook']; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="twitter" class="col-lg-1 control-label">Twitter</label>
                                <div class="col-lg-5">
                                    <input type="url" class="form-control" id="twitter" name="twitter" placeholder="" value="<?= $servico[0]['twitter']; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="googleplus" class="col-lg-1 control-label">Google+</label>
                                <div class="col-lg-5">
                                    <input type="url" class="form-control" id="googleplus" name="googleplus" placeholder="" value="<?= $servico[0]['googleplus']; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="linkedin" class="col-lg-1 control-label">LinkedIn</label>
                                <div class="col-lg-5">
                                    <input type="url" class="form-control" id="linkedin" name="linkedin" placeholder="" value="<?= $servico[0]['linkedIn']; ?>">
                                </div>
                            </div>
                            <br>
                            <br>
                            <!-- fim contato -->
                            <div class="form-group">
                                <label class="col-lg-offset-1 col-lg-5">Serviço</label>
                            </div>
                            <div class="form-group">
                                <label for="dsDoenca" class="col-lg-1 control-label">O que é a Doença?</label>
                                <div class="col-lg-5">
                                    <textarea style="resize: vertical;" class="form-control" rows="5" id="dsDoenca" name="dsDoenca" placeholder=""><?= $servico[0]['dsDoenca']; ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="dsTratamento" class="col-lg-1 control-label">Informações sobre o Diagnóstico e Tratamento</label>
                                <div class="col-lg-5">
                                    <textarea style="resize: vertical;" class="form-control" rows="5" id="dsTratamento" name="dsTratamento" placeholder=""><?= $servico[0]['dsTratamento']; ?></textarea>
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="form-group">
                                <label class="col-lg-offset-1 col-lg-5">Formas de acesso</label>
                            </div>
                            <div class="form-group">
                                <label for="formaAcesso" class="col-lg-1 control-label">Procedimento a seguir para acessar o serviço</label>
                                <div class="col-lg-5">
                                    <textarea style="resize: vertical;" class="form-control" rows="5" id="formaAcesso" name="formaAcesso" placeholder=""><?= $servico[0]['formaAcesso']; ?></textarea>
                                </div>
                            </div>
                            <br>
                            <br>                            
                            <div class="form-group">
                                <label class="col-lg-offset-1 col-lg-5">Sites com informações relacionadas</label>
                            </div>
                            <div class="form-group">
                                <label for="nmEmpresa1" class="col-lg-1 control-label">Nome da Empresa</label>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control" id="nmEmpresa1" name="nmEmpresa1" placeholder="" value="<?= $sites_servico[0]['nmEmpresa']; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="urlSite1" class="col-lg-1 control-label">Site</label>
                                <div class="col-lg-5">
                                    <input type="url" class="form-control" id="urlSite1" name="urlSite1" placeholder="" value="<?= $sites_servico[0]['urlSite']; ?>">
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="form-group">
                                <label for="nmEmpresa2" class="col-lg-1 control-label">Nome da Empresa</label>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control" id="nmEmpresa2" name="nmEmpresa2" placeholder="" value="<?= $sites_servico[1]['nmEmpresa']; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="urlSite2" class="col-lg-1 control-label">Site</label>
                                <div class="col-lg-5">
                                    <input type="url" class="form-control" id="urlSite2" name="urlSite2" placeholder="" value="<?= $sites_servico[1]['urlSite']; ?>">
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="form-group">
                                <label for="nmEmpresa3" class="col-lg-1 control-label">Nome da Empresa</label>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control" id="nmEmpresa3" name="nmEmpresa3" placeholder="" value="<?= $sites_servico[2]['nmEmpresa']; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="urlSite3" class="col-lg-1 control-label">Site</label>
                                <div class="col-lg-5">
                                    <input type="url" class="form-control" id="urlSite3" name="urlSite3" placeholder="" value="<?= $sites_servico[2]['urlSite']; ?>">
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="form-group">
                                <label for="nmEmpresa4" class="col-lg-1 control-label">Nome da Empresa</label>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control" id="nmEmpresa4" name="nmEmpresa4" placeholder="" value="<?= $sites_servico[3]['nmEmpresa']; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="urlSite4" class="col-lg-1 control-label">Site</label>
                                <div class="col-lg-5">
                                    <input type="url" class="form-control" id="urlSite4" name="urlSite4" placeholder="" value="<?= $sites_servico[3]['urlSite']; ?>">
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="form-group">
                                <label for="nmEmpresa5" class="col-lg-1 control-label">Nome da Empresa</label>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control" id="nmEmpresa5" name="nmEmpresa5" placeholder="" value="<?= $sites_servico[4]['nmEmpresa']; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="urlSite5" class="col-lg-1 control-label">Site</label>
                                <div class="col-lg-5">
                                    <input type="url" class="form-control" id="urlSite5" name="urlSite5" placeholder="" value="<?= $sites_servico[4]['urlSite']; ?>">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>
    </body>
</html>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - CANAL CONFIDENCIAL</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <!-- Modal -->
        <div class="modal fade" id="modal_excluir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Confirmar Exclusão</h4>
                    </div>
                    <div class="modal-body" id="modal_delete_vinculo">Deseja excluir esta Vínculo de RH?</div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        <button type="button" class="btn btn-primary" id="modal_btn-excluir">Excluir</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal -->

        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>

            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Vínculos de RH</h1>
                        <ol class="breadcrumb">
                            <li><i class="fa fa-lock"></i> Canal Confidencial</li>
                            <li class="active"><i class="fa fa-list"></i> Listagem</li>
                        </ol>

                        <?php if (isset($warning)) : ?>
                            <div id="msg" class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong><?= $warning; ?></strong>
                            </div>
                        <?php endif ?>                          
                        <div class="row">
                            <?php if ($_SESSION['AcLiberaBtnAddTpVin'] == TRUE) { ?>
                                <div class="col-lg-12">
                                    <button type="submit" id="btn_novo_vinculo" class="pull-right btn btn-primary btn-md" name="btn_novo_vinculo"><i class="glyphicon glyphicon-plus-sign"></i>  Adicionar Vínculo de RH</button>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="vinculos">
                                <thead>
                                    <tr>
                                        <th width="6%" style="text-align: center">ID</th>
                                        <th>Nome do Vínculo de RH</th>
                                        <th width="28%">Sigla da Vínculo de RH</th>
                                        <th width="18%" style="text-align: center">Ações Adicionais</th>
                                        <th width="8%" style="text-align: center">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($data as $vinculo) {
                                        ?>
                                        <tr>
                                            <td align="center"><?= $vinculo['idVinculo']; ?></td>
                                            <td><?= $vinculo['Nome_Vinculo']; ?></td>
                                            <td><?= $vinculo['Sigla_Vinculo']; ?></td>
                                            <td align="center">
                                                <button  type="submit" id="btnEditar" class="btn btn-default btn-xs" name="btnEditar" page="<?= $PaginaAtual; ?>" idVinculo="<?= $vinculo['idVinculo']; ?>" title="Editar Vínculo de RH" <?php if ($_SESSION['AcLiberaBtnEdiVin'] == NULL) { ?>disabled<?php } ?>>
                                                    <i class="glyphicon glyphicon-edit"></i>
                                                </button>
                                                <input type="hidden" id="idVinculo_<?= $vinculo['idVinculo']; ?>" name="idVinculo" value="<?= $vinculo['idVinculo']; ?>">
                                                <button  type="button" id="btnExcluir" class="btn btn-default btn-xs" name="btnExcluir" idVinculo="<?= $vinculo['idVinculo']; ?>" nome="<?= $vinculo['Nome_Vinculo']; ?>" <?php if ($_SESSION['AcLiberaBtnExcVin'] == NULL) { ?>disabled<?php } ?> data-toggle="modal" data-target="#modal_excluir" data-placement="bottom" title="Excluir Vínculo de RH" data-delay="1">
                                                    <i class="glyphicon glyphicon-trash"></i>
                                                </button>
                                            </td>
                                            <td align="center">
                                                <input type="hidden" name="idVinculo" value="<?= $vinculo['idVinculo']; ?>">
                                                <?php
                                                if ($vinculo['Status'] == "0") {
                                                    ?>
                                                    <button type="submit" id="btnAtivar_<?= $vinculo['idVinculo']; ?>" class="btn btn-danger btn-xs" idVinculo="<?= $vinculo['idVinculo']; ?>" name="btnAtivar" title="Ativar Vínculo de RH" <?php if ($_SESSION['AcLiberaBtnAtivVin'] == NULL) { ?>disabled<?php } ?>>
                                                        <i id="activ_<?= $vinculo['idVinculo']; ?>" class="glyphicon glyphicon-ban-circle"></i>

                                                    </button>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <button type="submit" id="btnAtivar_<?= $vinculo['idVinculo']; ?>" class="btn btn-success btn-xs" idVinculo="<?= $vinculo['idVinculo']; ?>" name="btnAtivar" title="Destivar Vínculo de RH" <?php if ($_SESSION['AcLiberaBtnAtivVin'] == NULL) { ?>disabled<?php } ?>>
                                                        <i id="activ_<?= $vinculo['idVinculo']; ?>" class="glyphicon glyphicon-ok"></i>

                                                    </button>
                                                    <?php
                                                }
                                                ?>

                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>    
                            </table>
                            <?php include(kohana::find_file('views/paginator', 'paginacao', 'php')) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>

        <script>
            jQuery(document).ready(function () {
                // Remove seleção de ativo no menu.
                $('.nav li').removeClass('active');
                $('#side-menu li').removeClass('active');
                // Ativa botão no menu.
                $('#lista-listagens-rh ul').addClass('collapse in');
                $('#lista-vinculos a').addClass('active');
                $('#lista-listagens-rh').addClass('active');
                $('#lista-listagens-rh a').addClass('collapse in');

                //Datatables
                $('#vinculos').dataTable(
                        {
                            "aoColumnDefs": [{"bSortable": false, "aTargets": [3, 4]}],
                            "paging": false,
                            "info": false,
                            "aaSorting": [[0, 'desc']],
                            "bFilter": false,
                            "oLanguage":
                                    {
                                        "sSearch": " Buscar: "
                                    }
                        });

                // abre o modal para cadastro
                $('button[name=btn_novo_vinculo]').click(function () {
                    var idVinculo = '';
                    var options = {
                        url: "cadastro_vinculo?idVinculo=" + idVinculo,
                        title: 'Cadastrar Vínculo de RH',
                        size: eModal.size.md,
                        buttons: [
                            {text: 'GRAVAR', style: 'primary', close: false, type: 'submit', form: 'novotipo'},
                            {text: 'FECHAR', style: 'danger', close: true}
                        ]
                    };
                    eModal.ajax(options);
                });

                //abre o modal de edição
                $('button[name=btnEditar]').click(function () {
                    var idVinculo = $(this).attr('idVinculo');
                    var page = $(this).attr('page');
                    var options = {
                        url: "editar_vinculo?idVinculo=" + idVinculo + "&page=" + page,
                        title: 'Editar Vínculo de RH',
                        size: eModal.size.md,
                        buttons: [
                            {text: 'SALVAR', style: 'primary', close: false, type: 'submit', form: 'novotipo'},
                            {text: 'FECHAR', style: 'danger', close: true}
                        ]
                    };
                    eModal.ajax(options);
                });

                // Opção usada na exclusão 
                var idVinculo;
                var nome;
                $('body').on("click", 'button[name=btnExcluir]', function ()
                {
                    idVinculo = $(this).attr('idVinculo');
                    nome = $(this).attr('nome');
                    $('#modal_delete_vinculo').html("Deseja excluir a Vínculo de RH <b>\"" + nome + "\"</b> ?");
                });

                $('#modal_excluir').on('hidden.bs.modal', function ()
                {
                    window.location.replace("home");
                });

                $('#modal_btn-excluir').on("click", function ()
                {
                    var btn = $(this);
                    btn.text('loading');

                    btn.removeClass('btn-primary');
                    btn.removeClass('btn-danger');
                    btn.removeClass('btn-success');
                    btn.addClass('btn-primary');

                    $('#modal_btn-excluir').prop('disabled', true);

                    $.ajax("delete_vinculo",
                            {
                                type: "POST",
                                data:
                                        {
                                            idVinculo: idVinculo
                                        }
                            })
                            .done(function (data)
                            {
                                $('#modal_delete_vinculo').html('Vínculo de RH excluído com sucesso');

                                btn.removeClass('btn-primary');
                                btn.addClass('btn-success');
                            })
                            .fail(function ()
                            {
                                $('#modal_delete_vinculo').html('Erro: ' + data);

                                btn.removeClass('btn-primary');
                                btn.addClass('btn-danger');
                            })
                            .always(function ()
                            {
                                btn.text('Excluído');
                            });
                });

                // Opção para ativar e desativar
                $("body").on("click", "button[name=btnAtivar]", function () {
                    idVinculo = $(this).attr('idVinculo');
                    //alert(txtUsuario);
                    $.get('update_ativar_vinculo?idVinculo=' + idVinculo, function (data) {

                        if (data === "1") {
                            $('#btnAtivar_' + idVinculo).addClass('btn btn-success btn-xs');
                            $('#btnAtivar_' + idVinculo).removeClass('btn-danger');
                            $('#activ_' + idVinculo).removeClass('glyphicon-ban-circle');
                            $('#activ_' + idVinculo).addClass('glyphicon-ok');
                        } else {
                            $('#btnAtivar_' + idVinculo).addClass('btn btn-danger btn-xs');
                            $('#btnAtivar_' + idVinculo).removeClass('btn-success');
                            $('#activ_' + idVinculo).removeClass('glyphicon-ok');
                            $('#activ_' + idVinculo).addClass('glyphicon-ban-circle');
                        }
                    });
                });

                $('.dataTables_filter label').addClass('pull-right');
            });
        </script>
    </body>
</html>
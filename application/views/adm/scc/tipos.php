<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - CANAL CONFIDENCIAL</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <!-- Modal -->
        <div class="modal fade" id="modal_excluir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Confirmar Exclusão</h4>
                    </div>
                    <div class="modal-body" id="modal_delete_tipo">Deseja excluir este Tipo de Relato?</div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        <button type="button" class="btn btn-primary" id="modal_btn-excluir">Excluir</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal -->

        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>

            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Tipo de Relatos</h1>
                        <ol class="breadcrumb">
                            <li><i class="fa fa-lock"></i> Canal Confidencial</li>
                            <li class="active"><i class="fa fa-list"></i> Listagem</li>
                        </ol>

                        <?php if (isset($warning)) : ?>
                            <div id="msg" class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong><?= $warning; ?></strong>
                            </div>
                        <?php endif ?>                          
                        <div class="row">
                            <?php if ($_SESSION['AcLiberaBtnAddTpRl'] == TRUE) { ?>
                                <div class="col-lg-12">
                                    <button type="submit" id="btn_novo_tipo" class="pull-right btn btn-primary btn-md" name="btn_novo_tipo"><i class="glyphicon glyphicon-plus-sign"></i>  Adicionar Tipo de Relato</button>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="tipos">
                                <thead>
                                    <tr>
                                        <th width="6%" style="text-align: center">ID</th>
                                        <th>Nome do Tipo de Relato</th>
                                        <th width="28%">Sigla do Tipo de Relato</th>
                                        <th width="18%" style="text-align: center">Ações Adicionais</th>
                                        <th width="8%" style="text-align: center">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($data as $tipo) {
                                        ?>
                                        <tr>
                                            <td align="center"><?= $tipo['idTipoRelato']; ?></td>
                                            <td><?= $tipo['Nome_TipoRelato']; ?></td>
                                            <td><?= $tipo['Sigla_TipoRelato']; ?></td>
                                            <td align="center">
                                                <button  type="submit" id="btnEditar" class="btn btn-default btn-xs" name="btnEditar" idTipoRelato="<?= $tipo['idTipoRelato']; ?>" page="<?= $PaginaAtual; ?>" title="Editar Tipo de Relato" <?php if ($_SESSION['AcLiberaBtnEdiTPRl'] == NULL) { ?>disabled<?php } ?>>
                                                    <i class="glyphicon glyphicon-edit"></i>
                                                </button>
                                                
                                                <input type="hidden" id="idTipoRelato_<?= $tipo['idTipoRelato']; ?>" name="idTipoRelato" value="<?= $tipo['idTipoRelato']; ?>">
                                                <button  type="button" id="btnExcluir" class="btn btn-default btn-xs" name="btnExcluir" idTipoRelato="<?= $tipo['idTipoRelato']; ?>" nome="<?= $tipo['Nome_TipoRelato']; ?>" <?php if ($_SESSION['AcLiberaBtnExcTPRl'] == NULL) { ?>disabled<?php } ?> data-toggle="modal" data-target="#modal_excluir" data-placement="bottom" title="Excluir Tipo de Relato" data-delay="1">
                                                    <i class="glyphicon glyphicon-trash"></i>
                                                </button>
                                            </td>
                                            <td align="center">
                                                <input type="hidden" name="idTipoRelato" value="<?= $tipo['idTipoRelato']; ?>">
                                                <?php
                                                if ($tipo['Status'] == "0") {
                                                    ?>
                                                    <button type="submit" id="btnAtivar_<?= $tipo['idTipoRelato']; ?>" class="btn btn-danger btn-xs" idTipoRelato="<?= $tipo['idTipoRelato']; ?>" name="btnAtivar" title="Ativar Tipo de Relato" <?php if ($_SESSION['AcLiberaBtnAtivTPRl'] == NULL) { ?>disabled<?php } ?>>
                                                        <i id="activ_<?= $tipo['idTipoRelato']; ?>" class="glyphicon glyphicon-ban-circle"></i>

                                                    </button>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <button type="submit" id="btnAtivar_<?= $tipo['idTipoRelato']; ?>" class="btn btn-success btn-xs" idTipoRelato="<?= $tipo['idTipoRelato']; ?>" name="btnAtivar" title="Destivar Tipo de Relato" <?php if ($_SESSION['AcLiberaBtnAtivTPRl'] == NULL) { ?>disabled<?php } ?>>
                                                        <i id="activ_<?= $tipo['idTipoRelato']; ?>" class="glyphicon glyphicon-ok"></i>

                                                    </button>
                                                    <?php
                                                }
                                                ?>

                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>    
                            </table>
                            <?php include(kohana::find_file('views/paginator', 'paginacao', 'php')) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>

        <script>
            jQuery(document).ready(function () {
                // Remove seleção de ativo no menu.
                $('.nav li').removeClass('active');
                $('#side-menu li').removeClass('active');
                // Ativa botão no menu.
                $('#lista-listagens-rh ul').addClass('collapse in');
                $('#lista-tipos-relatos a').addClass('active');
                $('#lista-listagens-rh').addClass('active');
                $('#lista-listagens-rh a').addClass('collapse in');

                 // Datatables
                $('#tipos').dataTable(
                        {
                            "aoColumnDefs": [{"bSortable": false, "aTargets": [3, 4]}],
                            "paging": false,
                            "info": false,
                            "aaSorting": [[0, 'desc']],
                            "bFilter": false,
                            "oLanguage":
                                    {
                                        "sSearch": " Buscar: "
                                    }
                        });

                // abre o modal para cadastro
                $('button[name=btn_novo_tipo]').click(function () {
                    var idTipoRelato = '';
                    var options = {
                        url: "cadastro_tipo?idTipoRelato=" + idTipoRelato,
                        title: 'Cadastrar Tipo de Relato',
                        size: eModal.size.md,
                        buttons: [
                            {text: 'GRAVAR', style: 'primary', close: false, type: 'submit', form: 'novotipo'},
                            {text: 'FECHAR', style: 'danger', close: true}
                        ]
                    };
                    eModal.ajax(options);
                });
                
                //abre o modal de edição
                $('button[name=btnEditar]').click(function () {
                    var idTipoRelato = $(this).attr('idTipoRelato');
                    var page = $(this).attr('page');
                    var options = {
                        url: "editar_tipo?idTipoRelato=" + idTipoRelato +"&page="+ page,
                        title: 'Editar Tipo de Relato',
                        size: eModal.size.md,
                        buttons: [
                            {text: 'SALVAR', style: 'primary', close: false, type: 'submit', form: 'novotipo'},
                            {text: 'FECHAR', style: 'danger', close: true}
                        ]
                    };
                    eModal.ajax(options);
                });
                
                // Opção usada na exclusão 
                var idTipoRelato;
                var nome;
                $('body').on("click", 'button[name=btnExcluir]', function ()
                {
                    idTipoRelato = $(this).attr('idTipoRelato');
                    nome = $(this).attr('nome');
                    $('#modal_delete_tipo').html("Deseja excluir o Tipo de Relato <b>\"" + nome + "\"</b> ?");
                });

                $('#modal_excluir').on('hidden.bs.modal', function ()
                {
                    window.location.replace("home");
                });

                $('#modal_btn-excluir').on("click", function ()
                {
                    var btn = $(this);
                    btn.text('loading');

                    btn.removeClass('btn-primary');
                    btn.removeClass('btn-danger');
                    btn.removeClass('btn-success');
                    btn.addClass('btn-primary');

                    $('#modal_btn-excluir').prop('disabled', true);

                    $.ajax("delete_tipo",
                            {
                                type: "POST",
                                data:
                                        {
                                            idTipoRelato: idTipoRelato
                                        }
                            })
                            .done(function (data)
                            {
                                $('#modal_delete_tipo').html('Tipo de Relato excluído com sucesso');

                                btn.removeClass('btn-primary');
                                btn.addClass('btn-success');
                            })
                            .fail(function ()
                            {
                                $('#modal_delete_tipo').html('Erro: ' + data);

                                btn.removeClass('btn-primary');
                                btn.addClass('btn-danger');
                            })
                            .always(function ()
                            {
                                btn.text('Excluído');
                            });
                });
                
                // Opção para ativar e desativar
                $("body").on("click", "button[name=btnAtivar]", function () {
                    idTipoRelato = $(this).attr('idTipoRelato');
                    //alert(txtUsuario);
                    $.get('update_ativar_tipo?idTipoRelato=' + idTipoRelato, function (data) {

                        if (data === "1") {
                            $('#btnAtivar_' + idTipoRelato).addClass('btn btn-success btn-xs');
                            $('#btnAtivar_' + idTipoRelato).removeClass('btn-danger');
                            $('#activ_' + idTipoRelato).removeClass('glyphicon-ban-circle');
                            $('#activ_' + idTipoRelato).addClass('glyphicon-ok');
                        } else {
                            $('#btnAtivar_' + idTipoRelato).addClass('btn btn-danger btn-xs');
                            $('#btnAtivar_' + idTipoRelato).removeClass('btn-success');
                            $('#activ_' + idTipoRelato).addClass('glyphicon-ok');
                            $('#activ_' + idTipoRelato).addClass('glyphicon-ban-circle');
                        }
                    });
                });

               $('.dataTables_filter label').addClass('pull-right');
            });
        </script>
    </body>
</html>
<style type="text/css">
    table {width: 100%; border: none;}
    .verticalText
            {
                text-align: center;
                white-space: nowrap;
            }
    label{
        margin-left: 10px;
        font-weight: bold;
    } 
    td {
        height: 25px;
        font-size: 12px;
    }
</style>
<page backtop="90px" backbottom="" backleft="" backright="">
    <page_header>
        <table width="100%" border="0.3" cellspacing="0" cellpadding="0">
            <tr>		
                <th height="80" width="251" bgcolor="#fff" align="center">
                    <img src="images/logo_spdm_2017.jpg" alt="Logo SPDM">
                </th>
                <th height="80" width="504" bgcolor="#fff" align="right">
                    <span style="font-size:16px;">RELATO ID: <span style="color:#f00;"><?= $relato[0]['idProtocolo']; ?></span></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br><br>
                    <span style="font-size:13px;">CANAL CONFIDENCIAL - SPDM</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                </th>
            </tr>         
        </table>
    </page_header>
    <page_footer style=" text-align: right; font-size: 11px;"><span >Impresso em: <?= date('d/m/Y - H:i:s'); ?></span></page_footer>
    
    <table width="100%" border="0.3" cellspacing="0" cellpadding="0">
        <tr>
            <td colspan="3" width="752" height="30" bgcolor="#dbdbdb" style="vertical-align:middle;">
                <span style="font-size:13px; text-transform: uppercase; font-weight: bold; margin-left: 15px;">DADOS DO SOLICITANTE</span>
            </td>
        </tr>
        <tr>
            <td><label>Nome:</label> <?php if ($relato[0]['Nome_Solicitante'] != "") { ?><?= $relato[0]['Nome_Solicitante']; ?><?php } else { ?><span style="color:#f00;">Anônimo</span><?php } ?></td>
            <td><label>Email:</label> <?php if ($relato[0]['Email_Solicitante'] != "") { ?><?= $relato[0]['Email_Solicitante']; ?><?php } else { ?><span style="color:#f00;">Anônimo</span><?php } ?></td>
            <td><label>Cargo:</label> <?= $relato[0]['Cargo_Solicitante']; ?></td>
        </tr>
        <tr>
            <td><label>Telefone:</label> <?= $relato[0]['Tel_Solicitante']; ?></td>
            <td><label>Celular:</label> <?= $relato[0]['Cel_Solicitante']; ?></td>
            <td><label>Data da Solitação:</label> <?= strftime('%d/%m/%Y', strtotime($relato[0]['dtCadastro'])); ?></td>                
        </tr>
        <tr>
            <td><label>Vínculo do RH:</label> <?= $relato[0]['Nome_Vinculo']; ?></td>
            <td colspan="2"><label>Unidade que ocorreu o Relato:</label> <?= $relato[0]['Nome_Unidade']; ?></td>
        </tr>
    </table>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="748" height="4" style="vertical-align:middle;"></td>
        </tr>
    </table>
    <table width="100%" border="0.3" cellspacing="0" cellpadding="0">
        <tr>
            <td colspan="3" width="752" height="30" bgcolor="#dbdbdb" style="vertical-align:middle;">
                <span style="font-size:13px; text-transform: uppercase; font-weight: bold; margin-left: 15px;">DADOS DO RELATO</span>
            </td>
        </tr>
        <tr>
            <td width="220"><label>Quando ocorreu:</label> <?= strftime('%d/%m/%Y', strtotime($relato[0]['dtQuando'])); ?></td>
            <td><label>Local do Relato:</label> <?= $relato[0]['Onde']; ?></td>
            
        </tr>
        <tr>
            <td width="220"><label>DSEI:</label> <?= $relato[0]['Nome_Dsei']; ?></td>
            <td><label>Número do Protocolo Anterior:</label> <?= $relato[0]['Numero_Protocolo_Anterior']; ?></td>
        </tr>
        <tr>
            <td colspan="2" width="740"><label>Nome dos envolvidos:</label> <?= $relato[0]['Nome_Envolvidos']; ?></td>
        </tr>
        <tr>
            <td colspan="2" style="padding:10px;" width="710">
                <label>Descrição do Relato:</label><br>
                <?= str_replace('?', '&#63;', $relato[0]['Descricao_Relato']); ?>
            </td>
        </tr>
        <?php if (!empty($anexo[0])) { ?>
        <tr>
            <td colspan="2" height="50" style="padding-left:10px;">
                <label>Anexos:</label><br>
                <?php $i = 1; foreach ($anexo as $anexos) { ?>
                    <?php if($anexos['idResposta'] == null or $anexos['idResposta'] == "") { ?>
                <a href="http://<?= $_SERVER['SERVER_NAME'] ?><?= URL::base(); ?>upload/scc/<?= $anexos['Caminho']; ?>" style="width: 120px; margin-right: 0px; margin-bottom: 10px;" target="_blank" download>ANEXO_<?= $i; ?></a>&nbsp;
                    <?php } ?>   
                <?php $i++; } ?>
            </td>
        </tr>
        <?php } ?>
    </table>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="748" height="4" style="vertical-align:middle;"></td>
        </tr>
    </table>
    <?php for ($numresp = 0; $numresp < count($resposta); $numresp++): ?>
    <table width="100%" border="0.3" cellspacing="0" cellpadding="0">
        <tbody>
            <tr>
            <td colspan="3" width="752" height="30" bgcolor="#dbdbdb" style="vertical-align:middle;">
                <span style="font-size:13px; text-transform: uppercase; font-weight: bold; margin-left: 15px;"><?php if ($resposta[$numresp]['Envio'] == 'reencaminhar_rh') { ?> Encaminhado pelo RH <?php } else if ($resposta[$numresp]['Envio'] == 'resposta_usuario') { ?> Resposta do Solicitante <?php } else { ?> Resposta do RH <?php }?> - <span style="color:#f00;">IntereÇÃo nº <?= 1 + $numresp; ?></span></span>
            </td>
        </tr>
            <tr>
                <?php if ($resposta[$numresp]['Envio'] == 'reencaminhar_rh') { ?>
                <td><label>Encaminhado em:</label> <?= strftime('%d/%m/%Y', strtotime($resposta[$numresp]['dtResposta'])); ?></td>
                <td><label>Encaminhado por:</label> <?= $resposta[$numresp]['Nome_Usuario']; ?></td>
                <?php } else { ?>
                <td><label>Respondido em:</label> <?= strftime('%d/%m/%Y', strtotime($resposta[$numresp]['dtResposta'])); ?></td>
                <td><label>Respondido por:</label> <?php if ($resposta[$numresp]['Nome_Usuario'] != '') { ?><?= $resposta[$numresp]['Nome_Usuario']; ?> <?php } else { ?> <?= $relato[0]['Nome_Solicitante']; ?> <?php } ?></td>
                <?php } ?>                                                                
            </tr>
            <tr>
                <td colspan="2" style="padding:10px;" width="710">
                    <label>Descrição do Resposta:</label>
                    <p><?= $resposta[$numresp]['Resposta']; ?></p></td>
            </tr>
            <?php 
            $model_relatos = new Model_Scc_Relato('default');
            $anexor = $model_relatos->select_anexos($relato[0]['idProtocolo'], $resposta[$numresp]['idResposta']);

            //if ($anexor[0] != "") {
            ?>
            <tr>
                <td colspan="3" style="padding-left:10px;">
                    <label>Anexos:</label>
                    <?php for ($numrel = 0; $numrel < count($anexor); $numrel++): ?>
                        <a href="http://<?= $_SERVER['SERVER_NAME'] ?><?= URL::base(); ?>upload/scc/<?= $anexor[$numrel]['Caminho']; ?>" class="btn btn-default btn-sm" style="width: 120px; margin-right: 0px; margin-bottom: 10px;" target="_blank" download>ANEXO_ADM_<?= 1 + $numrel; ?></a>&nbsp;
                    <?php endfor; ?>
                </td>
            </tr>
            <?php //} ?>
        </tbody>
    </table> 
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td width="748" height="4" style="vertical-align:middle;"></td>
        </tr>
    </table>  
    <?php endfor;  ?>    
</page>
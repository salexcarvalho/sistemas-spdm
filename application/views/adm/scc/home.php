<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - CANAL CONFIDENCIAL</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>

            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Lista de Relatos</h1>
                        <ol class="breadcrumb">
                            <li><i class="fa fa-lock"></i> Canal Confidencial</li>
                            <li class="active"><i class="fa fa-list"></i> Listagem</li>
                        </ol>
                        
                        <div class="row" style="margin-top:-5px; margin-bottom: 15px;">
                            <div class="form-group">
                                <div class="col-md-4">    
                                    <form class="form-inline" method="post" action="visualizar_relato_protocolo" target="_blank">                      
                                        <input type="text" class="form-control input-md" id="idProtocolo" name="idProtocolo" placeholder="Informe o Nº do Protocolo" style="width: 250px;" required>
                                        <button type="submit" class="btn btn-danger btn-md"><i class="glyphicon glyphicon-search"></i> Visualizar</button>
                                    </form>
                                </div>
                                <div class="col-md-8"> 
                                    <form class="form-inline" method="get" action="home"> 
                                        <div class="col-md-3">
                                            <label for="StatusAtendimento" class="pull-right" style="margin-top:8px;">STATUS DE ATENDIMENTO: </label>
                                        </div>
                                        <div class="col-md-9">
                                            <select id="StatusAtendimento" name="StatusAtendimento" class="form-control" style="width: 250px;" required>
                                            <option>selecione uma Opção</option>
                                            <option <?= ($StatusAtendimento == 1) ? 'selected' : ''; ?> value="1">ABERTO</option>
                                            <option <?= ($StatusAtendimento == 2) ? 'selected' : ''; ?> value="2">PROCESSANDO</option>
                                            <option <?= ($StatusAtendimento == 3) ? 'selected' : ''; ?> value="3">CONCLUÍDAS</option>
                                            <option <?= ($StatusAtendimento == 4) ? 'selected' : ''; ?> value="4">ENCERRADAS</option>
                                            <option <?= ($StatusAtendimento == "all") ? 'selected' : ''; ?> value="all">Todos as Opções</option>
                                        </select>   
                                        <button type="submit" class="btn btn-info btn-md"><i class="glyphicon glyphicon-search"></i> Filtrar</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>   

                        <?php if (isset($warning)) : ?>
                            <div id="msg" class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong><?= $warning; ?></strong>
                            </div>
                        <?php endif ?>                          
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="relatos">
                                <thead>
                                    <tr>
                                        <th width="14%">Nº Protocolo</th>
                                        <th>Nome do Solicitante</th>
                                        <th width="13%">Vínculo do RH</th>
                                        <th width="18%">Unidade do RH</th>
                                        <th width="8%" style="text-align: center">Data da Solicitação</th>
                                        <th width="8%" style="text-align: center">Ações</th>
                                        <th width="11%" style="text-align: center">Status de<br>Atendimento</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($data as $relato) {
                                        ?>
                                        <tr>
                                            <td><?= $relato['idProtocolo']; ?></td>
                                            <td><?php if ($relato['Nome_Solicitante'] != "") { ?><?= $relato['Nome_Solicitante']; ?><?php } else { ?><span style="color:#f00;">Anônimo</span><?php } ?></td>
                                            <td><?= $relato['Nome_Vinculo']; ?></td>
                                            <td><?= $relato['Nome_Unidade']; ?></td>
                                            <td align="center"><?= strftime('%d/%m/%Y', strtotime($relato['dtCadastro'])); ?></td>
                                            <td align="center">
                                                <form style="display: inline-block;" role="form" method="POST" action="visualizar_relato">
                                                    <input hidden id="page" name="page" value="<?= $PaginaAtual; ?>">
                                                    <input type="hidden" id="idRelato_<?= $relato['idRelato']; ?>" name="idRelato" value="<?= $relato['idRelato']; ?>">
                                                    <button  type="submit" id="btnVisualizar" class="btn btn-default btn-xs" name="btnVisualizar" idRelato="<?= $relato['idRelato']; ?>" title="Visualizar Relato" <?php if ($_SESSION['AcLiberaBtnRespRel'] == NULL) { ?>disabled<?php } ?>>
                                                        <?php if ($relato['Atendimento'] == "1" or $relato['Atendimento'] == "2") { ?>
                                                            <i class="glyphicon glyphicon-eye-open"></i>
                                                        <?php } else { ?>
                                                            <i class="glyphicon glyphicon-eye-close"></i>
                                                        <?php } ?>
                                                    </button>
                                                </form>
                                                <form style="display: inline-block;" role="form" method="POST" action="abrir_pdf" target="_blank">
                                                    <input hidden id="page" name="page" value="<?= $PaginaAtual; ?>">
                                                    <input type="hidden" id="idRelato_<?= $relato['idRelato']; ?>" name="idRelato" value="<?= $relato['idRelato']; ?>">
                                                    <button  type="submit" id="btnEditar" class="btn btn-default btn-xs" name="btnEditar" idRelato="<?= $relato['idRelato']; ?>" title="Ver Relato em PDF" <?php if ($_SESSION['AcLiberaBtnRespRel'] == NULL) { ?>disabled<?php } ?>>
                                                        <i class="fa fa-file-pdf-o"></i>
                                                    </button>
                                                </form>
                                            </td>
                                            <td align="center">
                                                <?php if ($relato['Atendimento'] == "4") { ?>
                                                    <button class="btn btn-xs btn-danger" style="width: 90px;">Encerrado</button>
                                                <?php } elseif ($relato['Atendimento'] == "2") { ?>
                                                    <button class="btn btn-xs btn-info " style="width: 90px;">Processando</button>
                                                <?php } else if ($relato['Atendimento'] == "3") { ?>
                                                    <button class="btn btn-xs btn-success" style="width: 90px;">Concluído</button>
                                                <?php } else { ?>
                                                    <button class="btn btn-xs btn-warning" style="width: 90px;">Aberto</button>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>    
                            </table>
                            <?php include(kohana::find_file('views/paginator', 'paginacao', 'php')) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>

        <script>
            jQuery(document).ready(function () {
                // Remove seleção de ativo no menu.
                $('.nav li').removeClass('active');
                $('#side-menu li').removeClass('active');
                // Ativa botão no menu.
                $('#lista-listagens-rh ul').addClass('collapse in');
                $('#lista-relatos a').addClass('active');
                $('#lista-listagens-rh').addClass('active');
                $('#lista-listagens-rh a').addClass('collapse in');
            });
        </script>
    </body>
</html>
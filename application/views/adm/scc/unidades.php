<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - CANAL CONFIDENCIAL</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <!-- Modal -->
        <div class="modal fade" id="modal_excluir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Confirmar Exclusão</h4>
                    </div>
                    <div class="modal-body" id="modal_delete_unidade">Deseja excluir esta Unidade de RH?</div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        <button type="button" class="btn btn-primary" id="modal_btn-excluir">Excluir</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal -->

        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>

            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Unidades de RH</h1>
                        <ol class="breadcrumb">
                            <li><i class="fa fa-lock"></i> Canal Confidencial</li>
                            <li class="active"><i class="fa fa-list"></i> Listagem</li>
                        </ol>

                        <?php if (isset($warning)) : ?>
                            <div id="msg" class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong><?= $warning; ?></strong>
                            </div>
                        <?php endif ?>                          
                        <div class="row">
                            <?php if ($_SESSION['AcLiberaBtnAddTpUrh'] == TRUE) { ?>
                                <div class="col-lg-12">
                                    <button type="submit" id="btn_novo_unidade" class="pull-right btn btn-primary btn-md" name="btn_novo_unidade"><i class="glyphicon glyphicon-plus-sign"></i>  Adicionar Unidade de RH</button>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="unidades">
                                <thead>
                                    <tr>
                                        <th width="6%" style="text-align: center">ID</th>
                                        <th>Nome da Unidade de RH</th>
                                        <th width="15%">Sigla da Unidade de RH</th>
                                        <th width="15%">Vincúlo RH</th>
                                        <th width="12%" style="text-align: center">Ações Adicionais</th>
                                        <th width="8%" style="text-align: center">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($data as $unidade) {
                                        ?>
                                        <tr>
                                            <td align="center"><?= $unidade['idUnidade']; ?></td>
                                            <td><?= $unidade['Nome_Unidade']; ?></td>
                                            <td><?= $unidade['Sigla_Unidade']; ?></td>
                                            <td><?= $unidade['Nome_Vinculo']; ?></td>
                                            <td align="center">
                                                <button  type="submit" id="btnEditar" class="btn btn-default btn-xs" name="btnEditar" page="<?= $PaginaAtual; ?>" idUnidade="<?= $unidade['idUnidade']; ?>" title="Editar Unidade de RH" <?php if ($_SESSION['AcLiberaBtnEdiUrh'] == NULL) { ?>disabled<?php } ?>>
                                                    <i class="glyphicon glyphicon-edit"></i>
                                                </button>
                                                </form>
                                                <input type="hidden" id="idUnidade_<?= $unidade['idUnidade']; ?>" name="idUnidade" value="<?= $unidade['idUnidade']; ?>">
                                                <button  type="button" id="btnExcluir" class="btn btn-default btn-xs" name="btnExcluir" idUnidade="<?= $unidade['idUnidade']; ?>" nome="<?= $unidade['Nome_Unidade']; ?>" <?php if ($_SESSION['AcLiberaBtnExcUrh'] == NULL) { ?>disabled<?php } ?> data-toggle="modal" data-target="#modal_excluir" data-placement="bottom" title="Excluir Unidade de RH" data-delay="1">
                                                    <i class="glyphicon glyphicon-trash"></i>
                                                </button>
                                            </td>
                                            <td align="center">
                                                <input type="hidden" name="idUnidade" value="<?= $unidade['idUnidade']; ?>">
                                                <?php
                                                if ($unidade['Status'] == "0") {
                                                    ?>
                                                    <button type="submit" id="btnAtivar_<?= $unidade['idUnidade']; ?>" class="btn btn-danger btn-xs" idUnidade="<?= $unidade['idUnidade']; ?>" name="btnAtivar" title="Ativar Unidade de RH" <?php if ($_SESSION['AcLiberaBtnAtivUrh'] == NULL) { ?>disabled<?php } ?>>
                                                        <i id="activ_<?= $unidade['idUnidade']; ?>" class="glyphicon glyphicon-ban-circle"></i>

                                                    </button>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <button type="submit" id="btnAtivar_<?= $unidade['idUnidade']; ?>" class="btn btn-success btn-xs" idUnidade="<?= $unidade['idUnidade']; ?>" name="btnAtivar" title="Destivar Unidade de RH" <?php if ($_SESSION['AcLiberaBtnAtivUrh'] == NULL) { ?>disabled<?php } ?>>
                                                        <i id="activ_<?= $unidade['idUnidade']; ?>" class="glyphicon glyphicon-ok"></i>

                                                    </button>
                                                    <?php
                                                }
                                                ?>

                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>    
                            </table>
                            <?php include(kohana::find_file('views/paginator', 'paginacao', 'php')) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>

        <script>
            jQuery(document).ready(function () {
                // Remove seleção de ativo no menu.
                $('.nav li').removeClass('active');
                $('#side-menu li').removeClass('active');
                // Ativa botão no menu.
                $('#lista-listagens-rh ul').addClass('collapse in');
                $('#lista-unidades-rh a').addClass('active');
                $('#lista-listagens-rh').addClass('active');
                $('#lista-listagens-rh a').addClass('collapse in');

                // Datatables
                $('#unidades').dataTable(
                        {
                            "aoColumnDefs": [{"bSortable": false, "aTargets": [1, 2, 4, 5]}],
                            "paging": false,
                            "info": false,
                            "aaSorting": [[0, 'desc']],
                            "bFilter": false,
                            "oLanguage":
                                    {
                                        "sSearch": " Buscar: "
                                    }
                        });

                // abre o modal para cadastro
                $('button[name=btn_novo_unidade]').click(function () {
                    var idUnidade = '';
                    var options = {
                        url: "cadastro_unidade?idUnidade=" + idUnidade,
                        title: 'Cadastrar Unidade',
                        size: eModal.size.md,
                        buttons: [
                            {text: 'GRAVAR', style: 'primary', close: false, type: 'submit', form: 'novotipo'},
                            {text: 'FECHAR', style: 'danger', close: true}
                        ]
                    };
                    eModal.ajax(options);
                });

                //abre o modal de edição
                $('button[name=btnEditar]').click(function () {
                    var idUnidade = $(this).attr('idUnidade');
                    var page = $(this).attr('page');
                    var options = {
                        url: "editar_unidade?idUnidade=" + idUnidade + "&page=" + page,
                        title: 'Editar Unidade',
                        size: eModal.size.md,
                        buttons: [
                            {text: 'SALVAR', style: 'primary', close: false, type: 'submit', form: 'novotipo'},
                            {text: 'FECHAR', style: 'danger', close: true}
                        ]
                    };
                    eModal.ajax(options);
                });

                // Opção usada na exclusão 
                var idUnidade;
                var nome;
                $('body').on("click", 'button[name=btnExcluir]', function ()
                {
                    idUnidade = $(this).attr('idUnidade');
                    nome = $(this).attr('nome');
                    $('#modal_delete_unidade').html("Deseja excluir a Unidade de RH <b>\"" + nome + "\"</b> ?");
                });

                $('#modal_excluir').on('hidden.bs.modal', function ()
                {
                    window.location.replace("home");
                });

                $('#modal_btn-excluir').on("click", function ()
                {
                    var btn = $(this);
                    btn.text('loading');

                    btn.removeClass('btn-primary');
                    btn.removeClass('btn-danger');
                    btn.removeClass('btn-success');
                    btn.addClass('btn-primary');

                    $('#modal_btn-excluir').prop('disabled', true);

                    $.ajax("delete_unidade",
                            {
                                type: "POST",
                                data:
                                        {
                                            idUnidade: idUnidade
                                        }
                            })
                            .done(function (data)
                            {
                                $('#modal_delete_unidade').html('Unidade de RH excluída com sucesso');

                                btn.removeClass('btn-primary');
                                btn.addClass('btn-success');
                            })
                            .fail(function ()
                            {
                                $('#modal_delete_unidade').html('Erro: ' + data);

                                btn.removeClass('btn-primary');
                                btn.addClass('btn-danger');
                            })
                            .always(function ()
                            {
                                btn.text('Excluído');
                            });
                });

                // Opção para ativar e desativar
                $("body").on("click", "button[name=btnAtivar]", function () {
                    idUnidade = $(this).attr('idUnidade');
                    //alert(txtUsuario);
                    $.get('update_ativar_unidade?idUnidade=' + idUnidade, function (data) {

                        if (data === "1") {
                            $('#btnAtivar_' + idUnidade).addClass('btn btn-success btn-xs');
                            $('#btnAtivar_' + idUnidade).removeClass('btn-danger');
                            $('#activ_' + idUnidade).removeClass('glyphicon-ban-circle');
                            $('#activ_' + idUnidade).addClass('glyphicon-ok');
                        } else {
                            $('#btnAtivar_' + idUnidade).addClass('btn btn-danger btn-xs');
                            $('#btnAtivar_' + idUnidade).removeClass('btn-success');
                            $('#activ_' + idUnidade).removeClass('glyphicon-ok');
                            $('#activ_' + idUnidade).addClass('glyphicon-ban-circle');
                        }
                    });
                });

                $('.dataTables_filter label').addClass('pull-right');
            });
        </script>
    </body>
</html>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - CANAL CONFIDENCIAL</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <!-- Modal -->
        <div class="modal fade" id="modal_excluir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Confirmar Exclusão</h4>
                    </div>
                    <div class="modal-body" id="modal_delete_dsei">Deseja excluir esta DSEI?</div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        <button type="button" class="btn btn-primary" id="modal_btn-excluir">Excluir</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal -->

        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>

            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">DSEI (Saúde Indígena)</h1>
                        <ol class="breadcrumb">
                            <li><i class="fa fa-lock"></i> Canal Confidencial</li>
                            <li class="active"><i class="fa fa-list"></i> Listagem</li>
                        </ol>

                        <?php if (isset($warning)) : ?>
                            <div id="msg" class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong><?= $warning; ?></strong>
                            </div>
                        <?php endif ?>                          

                        <div class="row">
                            <?php if ($_SESSION['AcLiberaBtnCadDsei'] == TRUE) { ?>
                                <div class="col-lg-12">
                                    <button type="submit" id="btn_novo_dsei" class="pull-right btn btn-primary btn-md" name="btn_novo_dsei"><i class="glyphicon glyphicon-plus-sign"></i>  Adicionar DSEI</button>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="dseis">
                                <thead>
                                    <tr>
                                        <th width="6%" style="text-align: center">ID</th>
                                        <th>Nome da DSEI</th>
                                        <th width="28%">Sigla da DSEI</th>
                                        <th width="18%" style="text-align: center">Ações Adicionais</th>
                                        <th width="8%" style="text-align: center">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($data as $dsei) {
                                        ?>
                                        <tr>
                                            <td align="center"><?= $dsei['idDsei']; ?></td>
                                            <td><?= $dsei['Nome_Dsei']; ?></td>
                                            <td><?= $dsei['Sigla_Dsei']; ?></td>
                                            <td align="center">
                                                <button  type="submit" id="btnEditar" class="btn btn-default btn-xs" name="btnEditar" page="<?= $PaginaAtual; ?>" iddsei="<?= $dsei['idDsei']; ?>" title="Editar DSEI" <?php if ($_SESSION['AcLiberaBtnEdiDsei'] == NULL) { ?>disabled<?php } ?>>
                                                    <i class="glyphicon glyphicon-edit"></i>
                                                </button>
                                                <input type="hidden" id="idDsei_<?= $dsei['idDsei']; ?>" name="idDsei" value="<?= $dsei['idDsei']; ?>">
                                                <button  type="button" id="btnExcluir" class="btn btn-default btn-xs" name="btnExcluir" iddsei="<?= $dsei['idDsei']; ?>" nome="<?= $dsei['Nome_Dsei']; ?>" <?php if ($_SESSION['AcLiberaBtnExcDsei'] == NULL) { ?>disabled<?php } ?> data-toggle="modal" data-target="#modal_excluir" data-placement="bottom" title="Excluir DSEI" data-delay="1">
                                                    <i class="glyphicon glyphicon-trash"></i>
                                                </button>
                                            </td>
                                            <td align="center">
                                                <input type="hidden" name="idDsei" value="<?= $dsei['idDsei']; ?>">
                                                <?php
                                                if ($dsei['Status'] == "0") {
                                                    ?>
                                                    <button type="submit" id="btnAtivar_<?= $dsei['idDsei']; ?>" class="btn btn-danger btn-xs" iddsei="<?= $dsei['idDsei']; ?>" name="btnAtivar" title="Ativar DSEI" <?php if ($_SESSION['AcLiberaBtnAtivDsei'] == NULL) { ?>disabled<?php } ?>>
                                                        <i id="activ_<?= $dsei['idDsei']; ?>" class="glyphicon glyphicon-ban-circle"></i>

                                                    </button>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <button type="submit" id="btnAtivar_<?= $dsei['idDsei']; ?>" class="btn btn-success btn-xs" iddsei="<?= $dsei['idDsei']; ?>" name="btnAtivar" title="Destivar DSEI" <?php if ($_SESSION['AcLiberaBtnAtivDsei'] == NULL) { ?>disabled<?php } ?>>
                                                        <i id="activ_<?= $dsei['idDsei']; ?>" class="glyphicon glyphicon-ok"></i>

                                                    </button>
                                                    <?php
                                                }
                                                ?>

                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>    
                            </table>
                            <?php include(kohana::find_file('views/paginator', 'paginacao', 'php')) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>

        <script>
            jQuery(document).ready(function () {
                // Remove seleção de ativo no menu.
                $('.nav li').removeClass('active');
                $('#side-menu li').removeClass('active');
                // Ativa botão no menu.
                $('#lista-listagens-rh ul').addClass('collapse in');
                $('#lista-dsei a').addClass('active');
                $('#lista-listagens-rh').addClass('active');
                $('#lista-listagens-rh a').addClass('collapse in');

                // Datatables
                $('#dseis').dataTable(
                        {
                            "aoColumnDefs": [{"bSortable": false, "aTargets": [3, 4]}],
                            "paging": false,
                            "info": false,
                            "aaSorting": [[0, 'desc']],
                            "bFilter": false,
                            "oLanguage":
                                    {
                                        "sSearch": " Buscar: "
                                    }
                        });

                // abre o modal para cadastro
                $('button[name=btn_novo_dsei]').click(function () {
                    var idDsei = '';
                    var options = {
                        url: "cadastro_dsei?idDsei=" + idDsei,
                        title: 'Cadastrar DSEI',
                        size: eModal.size.md,
                        buttons: [
                            {text: 'GRAVAR', style: 'primary', close: false, type: 'submit', form: 'novotipo'},
                            {text: 'FECHAR', style: 'danger', close: true}
                        ]
                    };
                    eModal.ajax(options);
                });

                //abre o modal de edição
                $('button[name=btnEditar]').click(function () {
                    var idDsei = $(this).attr('idDsei');
                    var page = $(this).attr('page');
                    var options = {
                        url: "editar_dsei?idDsei=" + idDsei + "&page=" + page,
                        title: 'Editar DSEI',
                        size: eModal.size.md,
                        buttons: [
                            {text: 'SALVAR', style: 'primary', close: false, type: 'submit', form: 'novotipo'},
                            {text: 'FECHAR', style: 'danger', close: true}
                        ]
                    };
                    eModal.ajax(options);
                });
                
                // Opção usada na exclusão 
                var idDsei;
                var nome;
                $('body').on("click", 'button[name=btnExcluir]', function ()
                {
                    idDsei = $(this).attr('iddsei');
                    nome = $(this).attr('nome');
                    $('#modal_delete_dsei').html("Deseja excluir o Dsei <b>\"" + nome + "\"</b> ?");
                });

                $('#modal_excluir').on('hidden.bs.modal', function ()
                {
                    window.location.replace("home");
                });

                $('#modal_btn-excluir').on("click", function ()
                {
                    var btn = $(this);
                    btn.text('loading');

                    btn.removeClass('btn-primary');
                    btn.removeClass('btn-danger');
                    btn.removeClass('btn-success');
                    btn.addClass('btn-primary');

                    $('#modal_btn-excluir').prop('disabled', true);

                    $.ajax("delete_dsei",
                            {
                                type: "POST",
                                data:
                                        {
                                            idDsei: idDsei
                                        }
                            })
                            .done(function (data)
                            {
                                $('#modal_delete_dsei').html('Tipo excluído com sucesso');

                                btn.removeClass('btn-primary');
                                btn.addClass('btn-success');
                            })
                            .fail(function ()
                            {
                                $('#modal_delete_dsei').html('Erro: ' + data);

                                btn.removeClass('btn-primary');
                                btn.addClass('btn-danger');
                            })
                            .always(function ()
                            {
                                btn.text('Excluído');
                            });
                });
                
                // Opção para ativar e desativar
                $("body").on("click", "button[name=btnAtivar]", function () {
                    idDsei = $(this).attr('iddsei');
                    //alert(txtUsuario);
                    $.get('update_ativar_dsei?iddsei=' + idDsei, function (data) {

                        if (data === "1") {
                            $('#btnAtivar_' + idDsei).addClass('btn btn-success btn-xs');
                            $('#btnAtivar_' + idDsei).removeClass('btn-danger');
                            $('#activ_' + idDsei).removeClass('glyphicon-ban-circle');
                            $('#activ_' + idDsei).addClass('glyphicon-ok');
                        } else {
                            $('#btnAtivar_' + idDsei).addClass('btn btn-danger btn-xs');
                            $('#btnAtivar_' + idDsei).removeClass('btn-success');
                            $('#activ_' + idDsei).addClass('glyphicon-ok');
                            $('#activ_' + idDsei).addClass('glyphicon-ban-circle');
                        }
                    });
                });

                $('.dataTables_filter label').addClass('pull-right');
            });
        </script>
    </body>
</html>
<?php if ($_SESSION['AcLiberaBtnAddTpRl'] == true) { ?>
    <form id="novotipo" nome="novotipo" class="form-horizontal" role="form" method="POST" action="insert_tipo">
        <div class="form-group">
            <div class="col-lg-12">
                <div class="form-group">
                    <label for="tipo" class="col-lg-2 control-label">Nome</label>
                    <div class="col-lg-10">
                        <input type="text" min="0" class="form-control" id="nome_tiporelato" name="nome_tiporelato" placeholder="Informe o tipo">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Sigla</label>
                    <div class="col-lg-10">
                        <input type="text" min="0" class="form-control" id="sigla_tiporelato" name="sigla_tiporelato" placeholder="Informe a sigla do tipo">
                    </div>
                </div>
                <div class="col-lg-offset-3 col-lg-9">
                    <div class="form-group"> 
                        <label class="radio-inline">
                            <input required type="radio" name="status" id="inativo" value="1">
                            Ativo
                        </label>
                        <label class="radio-inline">
                            <input required type="radio" name="status" id="inativo" value="0">
                            Inativo
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </form>
<?php } else { ?>
    <h1 class="page-header"> Voce não possue acesso a está pagina, consulte o administrador.</h1>
<?php } ?>
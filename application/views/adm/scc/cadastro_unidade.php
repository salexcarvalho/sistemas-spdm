<?php if ($_SESSION['AcLiberaBtnAddTpUrh'] == true) { ?>
    <form id="novotipo" nome="novotipo" class="form-horizontal" role="form" method="POST" action="insert_unidade">
        <div class="form-group">
            <div class="col-lg-12">
                <div class="form-group">
                    <label for="nome_unidade" class="col-lg-2 control-label">Nome</label>
                    <div class="col-lg-10">
                        <input required="" type="text" min="0" class="form-control" id="nome_unidade" name="nome_unidade" placeholder="Informe a Unidade de RH">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 control-label">Sigla</label>
                    <div class="col-lg-10">
                        <input required="" type="text" min="0" class="form-control" id="sigla_unidade" name="sigla_unidade" placeholder="Informe a sigla da Unidade de RH">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2  control-label">Vínculo</label>
                    <div class="col-md-10">
                        <select class="form-control" id='vinculo_solicitante' name='vinculo_solicitante' required="">
                            <option value="" selected='selected'>Selecione uma Opção</option>
                            <?php foreach ($vinculos as $vinculo) { ?>
                                <option value="<?= $vinculo['idVinculo']; ?>"><?= $vinculo['Nome_Vinculo']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="col-lg-offset-3 col-lg-9">
                    <div class="form-group"> 
                        <label class="radio-inline">
                            <input required type="radio" name="status" id="inativo" value="1">
                            Ativo
                        </label>
                        <label class="radio-inline">
                            <input required type="radio" name="status" id="inativo" value="0">
                            Inativo
                        </label>
                    </div>
                </div>
            </div>
        </div>        
    </form>
<?php } else { ?>
    <h1 class="page-header"> Voce não possue acesso a está pagina, consulte o administrador.</h1>
<?php } ?>
<?php if ($_SESSION['AcLiberaBtnEdiTPRl'] == true) { ?>
    <form id="novotipo" nome="novotipo" class="form-horizontal" role="form" method="POST" action="update_tipo">
        <div class="form-group">
            <div class="col-lg-12">
                <div class="form-group">
                    <label for="nome" class="col-lg-2 control-label">Nome</label>
                    <div class="col-lg-10">
                        <input type="hidden" id="page" name="page" value="<?= $page; ?>">
                        <input type="hidden" id="idTipoRelato" name="idTipoRelato" placeholder="" value="<?= $tipos[0]['idTipoRelato']; ?>">  
                        <input type="text" min="0" class="form-control" id="nome_tiporelato" name="nome_tiporelato" placeholder="" value="<?= $tipos[0]['Nome_TipoRelato']; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="alias" class="col-lg-2 control-label">Sigla</label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control" id="sigla_tiporelato" name="sigla_tiporelato" placeholder="" value="<?= $tipos[0]['Sigla_TipoRelato']; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-offset-3 col-lg-9">
                        <div class="form-group"> 
                            <label class="radio-inline">&nbsp;&nbsp;&nbsp;
                                <input required type="radio" name="status" id="inativo" value="1" <?= ($tipos[0]['Status'] == '1') ? 'checked' : ''; ?>>
                                Ativo
                            </label>
                            <label class="radio-inline">
                                <input required type="radio" name="status" id="inativo" value="0" <?= ($tipos[0]['Status'] == '0') ? 'checked' : ''; ?>>
                                Inativo
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>   
<?php } else { ?>
    <h1 class="page-header"> Voce não possue acesso a está pagina, consulte o administrador.</h1>
<?php } ?>
<?php if ($_SESSION['AcLiberaBtnEdiVin'] == true) { ?>
    <form id="novotipo" nome="novotipo" class="form-horizontal" role="form" method="POST" action="update_vinculo">
        <div class="form-group">
            <div class="col-lg-12">
                <div class="form-group">
                    <label for="nome" class="col-lg-2 control-label">Nome</label>
                    <div class="col-lg-10">
                        <input type="hidden" id="page" name="page" value="<?= $page; ?>">
                        <input type="hidden" id="idVinculo" name="idVinculo" placeholder="" value="<?= $vinculos[0]['idVinculo']; ?>">  
                        <input type="text" min="0" class="form-control" id="nome_vinculorelato" name="nome_vinculo" placeholder="" value="<?= $vinculos[0]['Nome_Vinculo']; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="alias" class="col-lg-2 control-label">Sigla</label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control" id="sigla_vinculorelato" name="sigla_vinculo" placeholder="" value="<?= $vinculos[0]['Sigla_Vinculo']; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-offset-3 col-lg-9">
                        <div class="form-group"> 
                            <label class="radio-inline">&nbsp;&nbsp;&nbsp;
                                <input required type="radio" name="status" id="inativo" value="1" <?= ($vinculos[0]['Status'] == '1') ? 'checked' : ''; ?>>
                                Ativo
                            </label>
                            <label class="radio-inline">
                                <input required type="radio" name="status" id="inativo" value="0" <?= ($vinculos[0]['Status'] == '0') ? 'checked' : ''; ?>>
                                Inativo
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>   
<?php } else { ?>
    <h1 class="page-header"> Voce não possue acesso a está pagina, consulte o administrador.</h1>
<?php } ?>
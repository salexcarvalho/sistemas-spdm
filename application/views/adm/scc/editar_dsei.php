<?php if ($_SESSION['AcLiberaBtnEdiTPte'] == true) { ?>
    <form id="novotipo" nome="novotipo" class="form-horizontal" role="form" method="POST" action="update_dsei">
        <div class="form-group">
            <div class="col-lg-12">
                <div class="form-group">
                    <label for="nome" class="col-lg-2 control-label">Nome</label>
                    <div class="col-lg-10">
                        <input type="hidden" id="page" name="page" value="<?= $page; ?>">
                        <input type="hidden" id="iddsei" name="iddsei" placeholder="" value="<?= $Dsei[0]['idDsei']; ?>">  
                        <input type="text" min="0" class="form-control" id="nome_dsei" name="nome_dsei" placeholder="" value="<?= $Dsei[0]['Nome_Dsei']; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="sigla_dsei" class="col-lg-2 control-label">Sigla</label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control" id="sigla_dsei" name="sigla_dsei" placeholder="" value="<?= $Dsei[0]['Sigla_Dsei']; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-offset-3 col-lg-9">
                        <div class="form-group"> 
                            <label class="radio-inline">&nbsp;&nbsp;&nbsp;
                                <input required type="radio" name="status" id="inativo" value="1" <?= ($Dsei[0]['Status'] == '1') ? 'checked' : ''; ?>>
                                Ativo
                            </label>
                            <label class="radio-inline">
                                <input required type="radio" name="status" id="inativo" value="0" <?= ($Dsei[0]['Status'] == '0') ? 'checked' : ''; ?>>
                                Inativo
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>   
<?php } else { ?>
    <h1 class="page-header"> Voce não possue acesso a está pagina, consulte o administrador.</h1>
<?php } ?>
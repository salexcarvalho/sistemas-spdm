<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title>SISTEMAS SPDM - CANAL CONFIDENCIAL</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
            .table {
                width: 98%;
                margin-left: 15px;
                margin-right: 15px;
                margin-bottom: -10px;
                margin-top: -5px;
            }
            .table > thead > tr > th,
            .table > tbody > tr > th,
            .table > tfoot > tr > th,
            .table > thead > tr > td,
            .table > tbody > tr > td,
            .table > tfoot > tr > td {
                padding: 8px;
                padding-bottom: 6px;
                line-height: 1.42857143;
                vertical-align: bottom;
                border-bottom: 1px solid #ddd;
                border-top: 0px solid #ddd;
            }
        </style>
    </head>
    <body>
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <?php if ($_SESSION['AcLiberaBtnRespRel'] == true) { ?>
                            <h1 class="page-header">Relatos</h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-lock"></i> Canal Confidencial</li>
                                <li class="active"><i class="fa fa-list"></i> Listagem</li>
                                <li class="active"><i class="glyphicon glyphicon-file"></i> Dados do Relato</li>
                            </ol>
                            <form class="form-horizontal" role="form" method="POST" action="responder_relato" enctype="multipart/form-data">

                                <div class="form-group">
                                    <div class="col-lg-12">
                                        <h4 class="page-header" style="margin-top:5px;">&nbsp;Dados do Protocolo - <strong style="color: #f00;">ID: <?= $relato[0]['idProtocolo']; ?></strong></h4>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <span><strong>SOLICITANTE</strong></span>
                                                <div class="pull-right">
                                                    <?php if ($relato[0]['Atendimento'] == "4") { ?>
                                                        <span>Status do Atendimento: </span><span class="btn btn-xs btn-danger" style="width: 90px;">Encerrado</span>
                                                    <?php } elseif ($relato[0]['Atendimento'] == "2") { ?>
                                                        <span>Status do Atendimento: </span><span class="btn btn-xs btn-info" style="width: 90px;">Processando</span>
                                                    <?php } else if ($relato[0]['Atendimento'] == "3") { ?>
                                                        <span>Status do Atendimento: </span><span class="btn btn-xs btn-success" style="width: 90px;">Concluído</span>
                                                    <?php } else { ?>
                                                        <span>Status do Atendimento: </span><span class="btn btn-xs btn-warning" style="width: 90px;">Aberto</span>
                                                    <?php } ?>
                                                </div>    
                                            </div>
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <input type="hidden" id="page" name="page" value="<?= $page; ?>">
                                                    <input type="hidden" id="idRelato" name="idRelato" value="<?= $relato[0]['idRelato']; ?>">
                                                    <input type="hidden" id="idProtocolo" name="idProtocolo" value="<?= $relato[0]['idProtocolo']; ?>">
                                                    <input type="hidden" id="idSolicitante" name="idSolicitante" value="<?= $relato[0]['idSolicitante']; ?>">
                                                    <input type="hidden" id="email_solicitante" name="email_solicitante" value="<?= $relato[0]['Email_Solicitante']; ?>">
                                                    <input type="hidden" name="nome_solicitante" id="nome_solicitante" class="form-control" value="<?= $relato[0]['Nome_Solicitante']; ?>">
                                                    <table border="0" class="table table-condensed">
                                                        <tbody>
                                                            <tr>
                                                                <td><label class="control-label">Nome:</label> <?php if ($relato[0]['Nome_Solicitante'] != "") { ?><?= $relato[0]['Nome_Solicitante']; ?><?php } else { ?><span style="color:#f00;">Anônimo</span><?php } ?></td>
                                                                <td><label class="control-label">Email:</label> <?php if ($relato[0]['Email_Solicitante'] != "") { ?><?= $relato[0]['Email_Solicitante']; ?><?php } else { ?><span style="color:#f00;">Anônimo</span><?php } ?></td>
                                                                <td><label class="control-label">Cargo:</label> <?= $relato[0]['Cargo_Solicitante']; ?></td>
                                                                <td><label class="control-label">Telefone:</label> <?= $relato[0]['Tel_Solicitante']; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td><label class="control-label">Celular:</label> <?= $relato[0]['Cel_Solicitante']; ?></td>
                                                                <td><label class="control-label">Data da Solitação:</label> <?= strftime('%d/%m/%Y', strtotime($relato[0]['dtCadastro'])); ?></td>
                                                                <td><label class="control-label">Vínculo do RH:</label> <?= $relato[0]['Nome_Vinculo']; ?></td>
                                                                <td><label class="control-label">Unidade que ocorreu o Relato:</label> <?= $relato[0]['Nome_Unidade']; ?></td>
                                                            </tr>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>

                                            <div class="panel-heading" style="border-top: 1px solid; border-top-color: #ddd; border-top-left-radius: 0px; border-top-right-radius: 0px; ">
                                                <span><strong>RELATO</strong></span>   
                                            </div>
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <table border="0" class="table table-condensed">
                                                        <tbody>
                                                            <tr>
                                                                <td width="18%"><label class="control-label">Quando ocorreu:</label> <?= strftime('%d/%m/%Y', strtotime($relato[0]['dtQuando'])); ?></td>
                                                                <td width="38%"><label class="control-label">Local do Relato:</label> <?= $relato[0]['Onde']; ?></td>
                                                                <td><label class="control-label">DSEI:</label> <?= $relato[0]['Nome_Dsei']; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2"><label class="control-label">Nome dos envolvidos:</label> <?= $relato[0]['Nome_Envolvidos']; ?></td>
                                                                <td><label class="control-label">Número do Protocolo Anterior:</label> <?= $relato[0]['Numero_Protocolo_Anterior']; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="3">
                                                                    <h5><strong>Descrição do Relato:</strong></h5>
                                                                    <p><?= $relato[0]['Descricao_Relato']; ?></p></td>
                                                            </tr>
                                                            <?php if (!empty($anexo[0])) { ?>
                                                                <tr>
                                                                    <td colspan="3">
                                                                        <h5><strong>Anexos:</strong></h5>
                                                                        <?php $i = 1;
                                                                        foreach ($anexo as $anexos) { ?>
                                                                            <?php if ($anexos['idResposta'] == null or $anexos['idResposta'] == "") { ?>
                                                                                <a href="<?= URL::base(); ?>upload/scc/<?= $anexos['Caminho']; ?>" class="btn btn-default btn-sm" style="width: 120px; margin-right: 0px; margin-bottom: 10px;" target="_blank" download>ANEXO_<?= $i; ?></a>
                                                                            <?php } ?>   
            <?php $i++;
        } ?>
                                                                    </td>
                                                                </tr>
    <?php } ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>


    <?php $numresp = 1;
    foreach ($resposta as $respostas) { ?>
                                            <div class="panel <?php if ($respostas['Envio'] == 'reencaminhar_rh') { ?>panel-info<?php } else if ($respostas['Envio'] == 'resposta_usuario') { ?>panel-warning<?php } else { ?>panel-danger<?php } ?>">
                                                <div class="panel-heading">
                                                    <span><strong><?php if ($respostas['Envio'] == 'reencaminhar_rh') { ?> Encaminhado pelo RH <?php } else if ($respostas['Envio'] == 'resposta_usuario') { ?> Resposta do Solicitante <?php } else { ?> Resposta do RH <?php } ?> - <span style="color:#f00;">Intereção nº <?= $numresp; ?></span></strong></span>
                                                </div>
                                                <div class="panel-body">
                                                    <div class="form-group" >
                                                        <table border="0" class="table table-condensed">
                                                            <tbody>
                                                                <tr>
                                                                    <?php if ($respostas['Envio'] == 'reencaminhar_rh') { ?>
                                                                        <td><label class="control-label">Encaminhado em:</label> <?= strftime('%d/%m/%Y', strtotime($respostas['dtResposta'])); ?></td>
                                                                        <td><label class="control-label">Encaminhado por:</label> <?= $respostas['Nome_Usuario']; ?></td>
                                                                    <?php } else { ?>
                                                                        <td><label class="control-label">Respondido em:</label> <?= strftime('%d/%m/%Y', strtotime($respostas['dtResposta'])); ?></td>
                                                                        <td><label class="control-label">Respondido por:</label>  <?php if ($respostas['Nome_Usuario'] != '') { ?><?= $respostas['Nome_Usuario']; ?> <?php } else { ?> <?= $relato[0]['Nome_Solicitante']; ?> <?php } ?></td>
        <?php } ?>                                                                
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2">
                                                                        <h5><strong>Descrição do Resposta:</strong></h5>
                                                                        <p><?= $respostas['Resposta']; ?></p></td>
                                                                </tr>

                                                                <?php
                                                                $model_relatos = new Model_Scc_Relato('default');
                                                                $anexor = $model_relatos->select_anexos($relato[0]['idProtocolo'], $respostas['idResposta']);

                                                                if ($anexor[0] != "") {
                                                                    ?>

                                                                    <tr>
                                                                        <td colspan="3">
                                                                            <label class="control-label">Anexos:</label>
            <?php $i = 1;
            foreach ($anexor as $anexosr) { ?>
                                                                                <a href="<?= URL::base(); ?>upload/scc/<?= $anexosr['Caminho']; ?>" class="btn btn-default btn-sm" style="width: 120px; margin-right: 0px; margin-bottom: 10px;" target="_blank" download>ANEXO_ADM_<?= $i; ?></a>
                                                                        <?php $i++;
                                                                    } ?>
                                                                        </td>
                                                                    </tr>

        <?php } ?>

                                                            </tbody>
                                                        </table>   
                                                    </div>


                                                </div>
                                            </div>
        <?php $numresp++;
    } ?>


    <?php if ($relato[0]['Atendimento'] != "4") { ?>

                                            <div class="form-group">
                                                <label class="col-lg-2 control-label">O que deseja fazer?</label>
                                                <div class="col-lg-6">
                                                    <button type="button" class="btn btn-success" id="responder"><i class="fa fa-check"></i> Responder</button>
                                                    <button type="button" class="btn btn-warning" id="reencaminhar"><i class="fa fa-share"></i>  Reencaminhar</button>
                                                    <button type="button" class="btn btn-info" id="concluir"><i class="fa fa-share"></i>  Finalizar o Relato</button>
                                                    <button type="button" class="btn btn-danger" id="encerrar"><i class="fa fa-close"></i>  Fechar o Relato</button>
                                                </div>
                                            </div>

                                            <div class="panel panel-default" id="responderorelato" style="display: none;">
                                                <div class="panel-heading">
                                                    <span><strong>RESPONDER O RELATO</strong></span>
                                                </div>
                                                <div class="panel-body">
                                                    <div class="form-group" >
                                                        <div class="col-md-2">
                                                            <label>Informe a sua resposta</label>
                                                        </div>    
                                                        <div class="col-md-9">
                                                            <textarea rows="6" class="form-control" name="resposta_relato" id="resposta_relato" value="" placeholder="" type="textarea"></textarea>  
                                                        </div>    
                                                    </div>

                                                    <div class="form-group" >
                                                        <div class="col-md-2"></div>
                                                        <div class="col-md-9">
                                                            <label class="leftfalse">Caso deseje, você poderá anexar arquivos para a sua resposta.</label>
                                                        </div>
                                                        <div class="col-md-2"></div>
                                                        <div class="col-md-4">
                                                            <input type="file" class="btn btn-default" id="anexo" name="anexo[]" multiple="" onchange="makeFileList();">
                                                        </div>
                                                        <div class="col-md-5">
                                                            <ol id="fileList" style="margin-top: 10px; margin-bottom: 10px;"></ol>
                                                        </div>    
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="panel panel-default" id="concluirorelato" style="display: none;">
                                                <div class="panel-heading">
                                                    <span><strong>CONCLUIR O RELATO</strong></span>
                                                </div>
                                                <div class="panel-body">
                                                    <div class="form-group" >
                                                        <div class="col-md-2">
                                                            <label>Informe a sua resposta</label>
                                                        </div>    
                                                        <div class="col-md-9">
                                                            <textarea rows="6" class="form-control" name="concluir_relato" id="concluir_relato" value="" placeholder="" type="textarea"></textarea>  
                                                        </div>    
                                                    </div>

                                                    <div class="form-group" >
                                                        <div class="col-md-2"></div>
                                                        <div class="col-md-9">
                                                            <label class="leftfalse">Caso deseje, você poderá anexar arquivos para a sua resposta.</label>
                                                        </div>
                                                        <div class="col-md-2"></div>
                                                        <div class="col-md-4">
                                                            <input type="file" class="btn btn-default" id="anexo" name="anexo[]" multiple="" onchange="makeFileList();">
                                                        </div>
                                                        <div class="col-md-5">
                                                            <ol id="fileList" style="margin-top: 10px; margin-bottom: 10px;"></ol>
                                                        </div>    
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="panel panel-default" id="reencaminharorelato" style="display: none;">
                                                <div class="panel-heading">
                                                    <span><strong>REENCAMINHAR O RELATO</strong></span>
                                                </div>
                                                <div class="panel-body">
                                                    <div class="form-group">
                                                        <div class="col-md-3">
                                                            <label>Informe o RH para reencaminhar o relato?</label>
                                                        </div>    
                                                        <div class="col-md-8">
                                                            <select class="form-control" id='vinculo_solicitante' name='vinculo_solicitante'>
                                                                <option value="" selected='selected'>Selecione uma Opção</option>
        <?php foreach ($vinculos as $vinculo) { ?>
                                                                    <option <?= ($relato[0]['Nome_Vinculo'] == $vinculo['Nome_Vinculo']) ? 'selected' : ''; ?> value="<?= $vinculo['idVinculo']; ?>"><?= $vinculo['Nome_Vinculo']; ?></option>
        <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group" >
                                                        <div class="col-md-3">
                                                            <label>Caso deseje, informe a sua observação</label>
                                                        </div>    
                                                        <div class="col-md-8">
                                                            <textarea rows="3" class="form-control" name="reencaminhar_relato" id="reencaminhar_relato" value="" placeholder="" type="textarea"></textarea>  
                                                        </div>    
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="panel panel-default" id="encerrarorelato" style="display: none;">
                                                <div class="panel-heading">
                                                    <span><strong>ENCERRAR O RELATO</strong></span>
                                                </div>
                                                <div class="panel-body">
                                                    <div class="form-group">
                                                        <p class="col-lg-12">Após a escolha desta opção este relato, não poderá ser modificado.</p>
                                                        <p class="col-lg-12">Caso seja necessário a alteração, por favor solicite ao Administrador do Sistema.</p>
                                                    </div>
                                                </div>
                                            </div>

                                            <input type="hidden" id="envio" name="envio" value="">
                                            <input type="hidden" id="acaorelato" name="acaorelato" value="">

                                            <div class="form-group" id="acaobtn" style="display: none;">
                                                <div class="col-lg-10">
                                                    <button type="submit" class="btn btn-primary">Salvar e Gravar</button>
                                                </div>
                                            </div>

                                            <div class="form-group" id="acaobtnresponder" style="display: none;">
                                                <div class="col-lg-10">
                                                    <button type="submit" class="btn btn-primary">Salvar e Enviar</button>
                                                </div>
                                            </div>  

                            <?php } ?>

                                    </div>
                                </div>
                            </form>   
        <?php } else { ?>
                            <h1 class="page-header"> Voce não possue acesso a está pagina, consulte o administrador.</h1>
<?php } ?>
                    </div>
                </div>
            </div>
        </div>
<?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>
        <script>
            jQuery(document).ready(function () {
                // Remove seleção de ativo no menu.
                $('.nav li').removeClass('active');
                $('#side-menu li').removeClass('active');
                // Ativa botão no menu.
                $('#lista-listagens-rh ul').addClass('collapse in');
                $('#lista-relatos a').addClass('active');
                $('#lista-listagens-rh').addClass('active');
                $('#lista-listagens-rh a').addClass('collapse in');


                // Função para mostrar a opção "O que deseja fazer?" 
                $('#responder').click(function () {
                    $('#encerrarorelato').hide();
                    $('#reencaminharorelato').hide();
                    $('#concluirorelato').hide();
                    $('#responderorelato').show();
                    $('#acaobtnresponder').show();
                    $('#acaobtn').hide();
                    $('#acaorelato').val('responder_relato');
                    $('#envio').val('enviar_solicitante');
                    $('#vinculo_solicitante').removeAttr("required");
                    $('#resposta_relato').attr("required", "true");
                });
                
                $('#concluir').click(function () {
                    $('#encerrarorelato').hide();
                    $('#reencaminharorelato').hide();
                    $('#concluirorelato').show();
                    $('#responderorelato').hide();
                    $('#acaobtnresponder').show();
                    $('#acaobtn').hide();
                    $('#acaorelato').val('concluir_relato');
                    $('#envio').val('enviar_solicitante');
                    $('#vinculo_solicitante').removeAttr("required");
                    $('#concluir_relato').attr("required", "true");
                });

                $('#reencaminhar').click(function () {
                    $('#encerrarorelato').hide();
                    $('#reencaminharorelato').show();
                    $('#concluirorelato').hide();
                    $('#responderorelato').hide();
                    $('#acaobtnresponder').hide();
                    $('#acaobtn').show();
                    $('#acaorelato').val('reencaminhar_relato');
                    $('#envio').val('reencaminhar_rh');
                    $('#vinculo_solicitante').attr("required", "true");
                    $('#reencaminhar_relato').removeAttr("required");
                });

                $('#encerrar').click(function () {
                    $('#encerrarorelato').show();
                    $('#reencaminharorelato').hide();
                    $('#concluirorelato').hide();
                    $('#acaobtnresponder').hide();
                    $('#acaobtn').show();
                    $('#envio').val('encerrar');
                    $('#acaorelato').val('encerrar_relato');
                    $('#vinculo_solicitante').removeAttr("required");
                });

            });

            function makeFileList() {
                var input = document.getElementById("anexo");
                var ol = document.getElementById("fileList");
                while (ol.hasChildNodes()) {
                    ol.removeChild(ol.firstChild);
                }
                for (var i = 0; i < input.files.length; i++) {
                    var li = document.createElement("li");
                    li.innerHTML = input.files[i].name;
                    ol.appendChild(li);
                }
                if (!ol.hasChildNodes()) {
                    var li = document.createElement("li");
                    li.innerHTML = 'Nenhum arquivo selecionado';
                    ol.appendChild(li);
                }
            }
        </script>      
    </body>
</html>
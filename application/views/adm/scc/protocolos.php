<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - CANAL CONFIDENCIAL</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <!-- Modal -->
        <div class="modal fade" id="modal_excluir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Confirmar Exclusão</h4>
                    </div>
                    <div class="modal-body" id="modal_delete_participante">Deseja excluir este participante?</div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        <button type="button" class="btn btn-primary btn-sm" id="modal_btn-excluir">Excluir</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal -->

        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            
            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Lista de Protocolos</h1>
                        <ol class="breadcrumb">
                            <li><i class="fa fa-lock"></i> Canal Confidencial</li>
                            <li class="active"><i class="fa fa-list"></i> Listagem</li>
                        </ol>

                        <?php if (isset($warning)) : ?>
                            <div id="msg" class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong><?= $warning; ?></strong>
                            </div>
                        <?php endif ?>                          

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <span>Protocolos</span>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">

                                    <table class="table table-striped table-bordered table-hover" id="relatos">
                                        <thead>
                                            <tr>
                                                <th width="6%" style="text-align: center">ID</th>
                                                <th>Nome do Solicitante</th>
                                                <th width="28%">Email do Solicitante</th>
                                                <th width="18%" style="text-align: center">Ações Adicionais</th>
                                                <th width="8%" style="text-align: center">Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php //include kohana::find_file('views', 'adm/scc/solicitantes_lista') ?>
                                        </tbody>    
                                    </table>
                                    <?php //include(kohana::find_file('views/paginator', 'paginacao', 'php')) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>

        <script>
            jQuery(document).ready(function () {
                // Remove seleção de ativo no menu.
                $('.nav li').removeClass('active');
                $('#side-menu li').removeClass('active');
                // Ativa botão no menu.
                $('#lista-listagens-rh ul').addClass('collapse in');
                $('#lista-protocolos a').addClass('active');
                $('#lista-listagens-rh').addClass('active');
                $('#lista-listagens-rh a').addClass('collapse in');
            });
        </script>
    </body>
</html>
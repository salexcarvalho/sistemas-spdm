<?php if ($_SESSION['AcLiberaBtnEdiUrh'] == true) { ?>
<form id="novotipo" nome="novotipo" class="form-horizontal" role="form" method="POST" action="update_unidade">
        <div class="form-group">
            <div class="col-lg-12">
                <div class="form-group">
                    <label for="nome" class="col-lg-2 control-label">Nome</label>
                    <div class="col-lg-10">
                        <input type="hidden" id="page" name="page" value="<?= $page; ?>">
                        <input type="hidden" id="idUnidade" name="idUnidade" placeholder="" value="<?= $unidades[0]['idUnidade']; ?>">  
                        <input type="text" min="0" class="form-control" id="nome_unidaderelato" name="nome_unidade" placeholder="" value="<?= $unidades[0]['Nome_Unidade']; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="alias" class="col-lg-2 control-label">Sigla</label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control" id="sigla_unidaderelato" name="sigla_unidade" placeholder="" value="<?= $unidades[0]['Sigla_Unidade']; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2  control-label">Vínculo</label>
                    <div class="col-md-10">
                        <select class="form-control" id='vinculo_solicitante' name='vinculo_solicitante'>
                            <option value="" selected='selected'>Selecione uma Opção</option>
                            <?php foreach ($vinculos as $vinculo) { ?>
                                <option <?= ($unidades[0]['idVinculo'] == $vinculo['idVinculo']) ? 'selected' : ''; ?> value="<?= $vinculo['idVinculo']; ?>"><?= $vinculo['Nome_Vinculo']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-offset-3 col-lg-9">
                        <div class="form-group"> 
                            <label class="radio-inline">&nbsp;&nbsp;&nbsp;
                                <input required type="radio" name="status" id="inativo" value="1" <?= ($unidades[0]['Status'] == '1') ? 'checked' : ''; ?>>
                                Ativo
                            </label>
                            <label class="radio-inline">
                                <input required type="radio" name="status" id="inativo" value="0" <?= ($unidades[0]['Status'] == '0') ? 'checked' : ''; ?>>
                                Inativo
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>    
    </form>   
<?php } else { ?>
    <h1 class="page-header"> Voce não possue acesso a está pagina, consulte o administrador.</h1>
<?php } ?>
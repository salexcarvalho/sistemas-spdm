<?php if ($_SESSION['AcLiberaBtnEdiTpGr'] == true) { ?>
    <form id="novotipo" name="novotipo" class="form-horizontal" role="form" method="POST" action="update_tipo">
        <div class="form-group">
            <div class="col-lg-12">
                <div class="form-group">
                    <label for="nome" class="col-lg-2 control-label">Nome</label>
                    <div class="col-lg-10">
                        <input type="hidden" id="page" name="page" value="<?= $page; ?>">
                        <input type="hidden" id="idTipoGrupo" name="idTipoGrupo" placeholder="" value="<?= $tipos[0]['idTipoGrupo']; ?>">  
                        <input type="text" min="0" class="form-control" id="Grupo" name="Grupo" placeholder="" value="<?= $tipos[0]['Grupo']; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="nome" class="col-lg-2 control-label">Descrição</label>
                    <div class="col-lg-10"> 
                        <input type="text" min="0" class="form-control" id="Descricao" name="Descricao" placeholder="" value="<?= $tipos[0]['Descricao']; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-offset-3 col-lg-9">
                        <div class="form-group"> 
                            <label class="radio-inline">&nbsp;&nbsp;&nbsp;
                                <input required type="radio" name="status" id="inativo" value="1" <?= ($tipos[0]['Status'] == '1') ? 'checked' : ''; ?>>
                                Ativo
                            </label>
                            <label class="radio-inline">
                                <input required type="radio" name="status" id="inativo" value="0" <?= ($tipos[0]['Status'] == '0') ? 'checked' : ''; ?>>
                                Inativo
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>    
    </form>   
<?php } else { ?>
    <h1 class="page-header"> Voce não possue acesso a está pagina, consulte o administrador.</h1>
<?php } ?>
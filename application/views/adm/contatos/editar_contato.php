<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - CONTATOS TELEFÔNICOS</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <?php if ($_SESSION['AcLiberaBtnAddTpCon'] == TRUE) { ?>
                            <h1 class="page-header">Editar Contato</h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-phone"></i> Contatos Telefônicos</li>
                                <li class="active"><i class="fa fa-edit"></i> Edição de Contato</li>
                            </ol>
                            <form class="form-horizontal" role="form" method="POST" action="update_contato">
                                <div class="form-group">
                                    <div class="col-md-6">   
                                        <div class="panel panel-default"> 
                                            <div class="panel-heading">
                                                <div class="col-md-9">
                                                    Dados do Contato
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group" style="margin-top: -7px; float:right;"> 
                                                        <label class="radio-inline">
                                                            <input required type="radio" name="status" id="inativo" value="1" <?= ($contato[0]['Status'] == '1') ? 'checked' : ''; ?>>
                                                            Ativo
                                                        </label>
                                                        <label class="radio-inline">
                                                            <input required type="radio" name="status" id="inativo" value="0" <?= ($contato[0]['Status'] == '0') ? 'checked' : ''; ?>>
                                                            Inativo
                                                        </label>
                                                    </div>
                                                </div>
                                            </div> 
                                            <div class="panel-body">
                                                <!-- Inicio da Tela de Contato -->     
                                                <div id="dados">
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Nome</label>
                                                        <div class="col-md-2" style="padding-right: 0px;">
                                                            <input type="hidden" id="page" name="page" value="<?= $page; ?>">
                                                            <input type="hidden" id="idContato" name="idContato" placeholder="" value="<?= $contato[0]['idContato']; ?>">
                                                            <select class="form-control input-sm" id="idTipoTratamento" name="idTipoTratamento" required >
                                                                <option value="NULL">Selecione uma Opção</option>
                                                                <?php
                                                                foreach ($tratamentos as $tratamento) {
                                                                    ?>
                                                                    <option <?= ($tratamento['idTipoTratamento'] == $contato[0]['idTratamento']) ? 'selected' : ''; ?> value="<?= $tratamento['idTipoTratamento']; ?>"><?= $tratamento['Descricao']; ?></option>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <input type="text" min="0" class="form-control input-sm" id="Nome" name="Nome" value="<?= $contato[0]['Nome']; ?>" placeholder="Informe o nome" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Aniversário</label>
                                                        <div class="col-md-3">
                                                            <input type="date" min="0" class="form-control input-sm" id="dtNascimento" name="dtNascimento" value="<?= $contato[0]['dtNascimento']; ?>" placeholder="Informe a data de nascimento">
                                                        </div>
                                                        <label class="col-md-1 control-label">Cargo</label>
                                                        <div class="col-md-6">
                                                            <input type="text" min="0" class="form-control input-sm" id="Cargo" name="Cargo" value="<?= $contato[0]['Cargo']; ?>" placeholder="Informe o Cargo">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Departamento</label>
                                                        <div class="col-md-10">
                                                            <input type="text" min="0" class="form-control input-sm" id="Departamento" name="Departamento" value="<?= $contato[0]['Departamento']; ?>" placeholder="Informe o Departamento ou Unidade">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Observação</label>
                                                        <div class="col-md-10">
                                                            <input type="text" min="0" class="form-control input-sm" id="Obs" name="Obs" value="<?= $contato[0]['Obs']; ?>" placeholder="Descreva a observação que deseja informar">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Grupo</label>
                                                        <div class="col-md-10">
                                                            <select class="form-control input-sm" id="idTipoGrupo" name="idTipoGrupo" required >
                                                                <option value="">Selecione uma Grupo</option>
                                                                <?php
                                                                foreach ($grupos as $grupo) {
                                                                    ?>
                                                                    <option <?= ($grupo['idTipoGrupo'] == $contato[0]['idGrupo']) ? 'selected' : ''; ?> value="<?= $grupo['idTipoGrupo']; ?>"><?= $grupo['Grupo']; ?></option>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Email</label>
                                                        <div class="col-md-10">
                                                            <input type="email" min="0" class="form-control input-sm" id="EmailP" name="EmailP" value="<?= $contato[0]['EmailP']; ?>" placeholder="Informe o Email">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Celular</label>
                                                        <div class="col-md-4">
                                                            <input type="tel" min="0" class="form-control input-sm" id="Celular_Principal" name="Celular_Principal" value="<?php echo Funcoes::formataTelefone($contato[0]['Celular_Principal']); ?>" placeholder="Informe o Celular" data-mask="(99) 9999-9999?9">
                                                        </div>
                                                        <label class="col-md-2 control-label">Tel. Comercial</label>
                                                        <div class="col-md-4">
                                                            <input type="tel" min="0" class="form-control input-sm" id="Telefone_Escritorio" name="Telefone_Escritorio" value="<?php echo Funcoes::formataTelefone($contato[0]['Telefone_Escritorio']); ?>" placeholder="Informe o Telefone Comercial" data-mask="(99) 9999-9999">
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Fim da Tela de Contato -->

                                                <ol class="breadcrumb">
                                                    <li><i class="fa fa-home"></i> Endereço Principal</li>
                                                </ol>

                                                <!-- Inicio da Tela de Endereços -->
                                                <div id="Endereco_Principal">
                                                    <div class="form-group">    
                                                        <label class="col-md-2 control-label">Informe o CEP</label>
                                                        <div class="col-md-3">
                                                            <input type="text" class="form-control input-sm Cep" id="Endereco_Cep_P" name="Endereco_Cep_P" value="<?php echo Funcoes::formataCep($contato[0]['Endereco_Cep']); ?>" placeholder="CEP" data-mask="99999-999" iddiv="Endereco_Principal" onblur="CarregaCep(this)" >
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Logradouro</label>
                                                        <div class="col-md-8">
                                                            <input type="text" class="form-control input-sm Logradouro" id="Endereco_Logradouro_P" name="Endereco_Logradouro_P" value="<?= $contato[0]['Endereco_Logradouro']; ?>" placeholder="Informe o Endereço">
                                                        </div>
                                                        <div class="col-md-2">
                                                            <input type="number" min="0" class="form-control input-sm" id="Endereco_Numero_P" name="Endereco_Numero_P" value="<?= $contato[0]['Endereco_Numero']; ?>" placeholder="Número">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">    
                                                        <label class="col-md-2 control-label">Complemento</label>
                                                        <div class="col-md-3">
                                                            <input type="text" min="0" class="form-control input-sm Complemento" id="Endereco_Complemento_P" name="Endereco_Complemento_P" value="<?= $contato[0]['Endereco_Complemento']; ?>" placeholder="Informe o Complemento">
                                                        </div>
                                                        <label class="col-md-1 control-label">Bairro</label>
                                                        <div class="col-md-6">
                                                            <input type="text" class="form-control input-sm Bairro" id="Endereco_Bairro_P" name="Endereco_Bairro_P" value="<?= $contato[0]['Endereco_Bairro']; ?>" placeholder="Informe o Bairro">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Estado</label>
                                                        <div class="col-md-3">
                                                            <input type="text" class="col-md-12 form-control input-sm Estado" id="Endereco_Estado_P" name="Endereco_Estado_P" value="<?= $contato[0]['Endereco_Estado']; ?>" placeholder="Pesquisar estado..." autocomplete="off" onkeyup="CarregaEstado()" />                                            
                                                        </div>
                                                        <label class="col-md-1 control-label">Cidade</label>
                                                        <div class="col-md-6">
                                                            <input type="text" class="col-md-12 form-control input-sm Cidade" id="Endereco_Cidade_P" name="Endereco_Cidade_P" value="<?= $contato[0]['Endereco_Cidade']; ?>" placeholder="Pesquisar cidade..." autocomplete="off" onkeyup="CarregaCidade()" />
                                                        </div>
                                                    </div>
                                                </div>    
                                                <!-- Fim da Tela de Endereços -->    
                                            </div>
                                        </div> 
                                    </div>

                                    <div class="col-md-6">
                                        <!-- Inicio da Tela de Emails Adicionais -->
                                        <div class="panel panel-default"> 
                                            <div class="panel-heading">
                                                Dados Adicionais 
                                            </div>

                                            <div class="panel-body">
                                                <ol class="breadcrumb">
                                                    <li><i class="fa fa-envelope"></i> Emails Adicionais</li>
                                                </ol>

                                                <div id="EmailAdicional">
                                                    <?php foreach ($emailscontatos as $emailscontato) { ?>
                                                        <div id="ConteudoEmail" class="form-group">
                                                            <div class="col-md-3">
                                                                <input type="hidden" id="idEmail" name="idEmail[]" value="<?= $emailscontato['idEmail']; ?>">
                                                                <select class="form-control input-sm" id="idTipoEmail" name="idTipoEmail[]">
                                                                    <option value="">Selecione um tipo</option>
                                                                    <?php
                                                                    foreach ($emails as $email) {
                                                                        ?>
                                                                        <option <?= ($email['idTipoEmail'] == $emailscontato['idTipoEmail']) ? 'selected' : ''; ?> value="<?= $email['idTipoEmail']; ?>"><?= $email['Descricao']; ?></option>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <input type="text" min="0" class="form-control input-sm" id="Obs_Email_Add_1" name="Obs_Email_Add[]" value="<?= $emailscontato['Observacao']; ?>" placeholder="Observações">
                                                            </div>
                                                            <div class="col-md-5 input-group">
                                                                <input type="email" min="0" class="form-control input-sm" id="Email" name="Email_Add[]" value="<?= $emailscontato['Email']; ?>" placeholder="Informe o email adicional 1">
                                                                <span class="input-group-btn">
                                                                    <a id="linkRemoverEmail" class="btn btn-danger btn-sm" idEmail="<?= $emailscontato['idEmail']; ?>">
                                                                        <i class="fa fa-minus-circle"></i>
                                                                    </a>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    <?php } ?>    
                                                </div>   

                                                <!-- Fim da Tela de Emails Adicionais -->  

                                                <ol class="breadcrumb">
                                                    <li><i class="fa fa-phone"></i> Telefones Adicionais</li>
                                                </ol>

                                                <!-- Inicio da Tela de Telefones Adicionais -->

                                                <div id="TelefoneAdicional">
                                                    <?php foreach ($telcontatos as $telcontato) { ?>    
                                                        <div id="ConteudoTelefone" class="form-group">
                                                            <div class="col-md-3">
                                                                <input type="hidden" id="idTelefone" name="idTelefone[]" value="<?= $telcontato['idTelefone']; ?>">
                                                                <select class="form-control input-sm" id="idTipoTel" name="idTipoTel[]">
                                                                    <option value="">Selecione um tipo</option>
                                                                    <?php
                                                                    foreach ($tels as $tel) {
                                                                        ?>
                                                                        <option <?= ($tel['idTipoTel'] == $telcontato['idTipoTel']) ? 'selected' : ''; ?> value="<?= $tel['idTipoTel']; ?>"><?= $tel['Descricao']; ?></option>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <input type="text" min="0" class="form-control input-sm" id="Obs_Tel_Add_1" name="Obs_Tel_Add[]" value="<?= $telcontato['Observacao']; ?>" placeholder="Observações">
                                                            </div>
                                                            <div class="col-md-5 input-group">
                                                                <input type="text" min="0" class="form-control input-sm" id="Tel" name="Tel_Add[]" value="<?php echo Funcoes::formataTelefone($telcontato['Numero']); ?>" placeholder="Informe o telefone ou celular adicional 1" data-mask="(99) 9999-9999?9">
                                                                <span class="input-group-btn">
                                                                    <a id="linkRemoverTelefone" class="btn btn-danger btn-sm" idTelefone="<?= $telcontato['idTelefone']; ?>">
                                                                        <i class="fa fa-minus-circle"></i>
                                                                    </a>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    <?php } ?>    
                                                </div>    

                                                <!-- Fim da Tela de Telefones Adicionais -->
                                                <div id="EnderecoAdicional">

                                                    <?php foreach ($enderecos as $endereco) { ?>
                                                        <div id="ConteudoEndereco">
                                                            <ol class="breadcrumb">
                                                                <li><i class="fa fa-hospital-o"></i> Endereços Adicionais | <?= $endereco['Local_Endereco']; ?></li>
                                                            </ol>
                                                            <!-- Inicio da Tela de Endereços Adicionais -->
                                                            <div id="Endereco_<?= $endereco['idEndereco']; ?>">
                                                                <div class="form-group">
                                                                    <input type="hidden" id="idEndereco" name="idEndereco[]" value="<?= $endereco['idEndereco']; ?>">
                                                                    <label class="col-md-2 control-label">Informe o CEP</label>
                                                                    <div class="col-md-3">
                                                                        <input type="text" class="form-control input-sm Cep" id="Endereco_Cep" name="Endereco_Cep[]" value="<?php echo Funcoes::formataCep($endereco['Endereco_Cep']); ?>" placeholder="CEP" data-mask="99999-999" iddiv="Endereco_<?= $endereco['idEndereco']; ?>" onblur="CarregaCep(this)">
                                                                    </div>
                                                                    <label class="col-md-1 control-label">Local</label>
                                                                    <div class="col-md-5 input-group">
                                                                        <input type="text" class="form-control input-sm" id="Local_Endereco" name="Local_Endereco[]" value="<?= $endereco['Local_Endereco']; ?>" placeholder="Informe o Local. Ex: Casa, Escritório, etc...">
                                                                        <span class="input-group-btn">
                                                                            <a id="linkRemoverEndereco" class="btn btn-danger btn-sm" idEndereco="<?= $endereco['idEndereco']; ?>">
                                                                                <i class="fa fa-minus-circle"></i>
                                                                            </a>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-md-2 control-label">Logradouro</label>
                                                                    <div class="col-md-8">
                                                                        <input type="text" class="form-control input-sm Logradouro" id="Endereco_Logradouro" name="Endereco_Logradouro[]" value="<?= $endereco['Endereco_Logradouro']; ?>" placeholder="Informe o Endereço">
                                                                    </div>
                                                                    <div class="col-md-2">
                                                                        <input type="number" min="0" class="form-control input-sm" id="Endereco_Numero" name="Endereco_Numero[]" value="<?= $endereco['Endereco_Numero']; ?>" placeholder="Número">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">    
                                                                    <label class="col-md-2 control-label">Complemento</label>
                                                                    <div class="col-md-3">
                                                                        <input type="text" min="0" class="form-control input-sm Complemento" id="Endereco_Complemento" name="Endereco_Complemento[]" value="<?= $endereco['Endereco_Complemento']; ?>" placeholder="Informe o Complemento">
                                                                    </div>
                                                                    <label class="col-md-1 control-label">Bairro</label>
                                                                    <div class="col-md-6">
                                                                        <input type="text" class="form-control input-sm Bairro" id="Endereco_Bairro" name="Endereco_Bairro[]" value="<?= $endereco['Endereco_Bairro']; ?>" placeholder="Informe o Bairro">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-md-2 control-label">Estado</label>
                                                                    <div class="col-md-3">
                                                                        <input type="text" class="col-md-12 form-control input-sm Estado" id="Endereco_Estado" name="Endereco_Estado[]" value="<?= $endereco['Endereco_Estado']; ?>" placeholder="Pesquisar estado..." autocomplete="off" onkeyup="CarregaEstado()" />                                            
                                                                    </div>
                                                                    <label class="col-md-1 control-label">Cidade</label>
                                                                    <div class="col-md-6">
                                                                        <input type="text" class="col-md-12 form-control input-sm Cidade" id="Endereco_Cidade" name="Endereco_Cidade[]" value="<?= $endereco['Endereco_Cidade']; ?>" placeholder="Pesquisar cidade..." autocomplete="off" onkeyup="CarregaCidade()" />
                                                                    </div>
                                                                </div>
                                                            </div> 
                                                        </div>
                                                    <?php } ?>  
                                                </div>
                                            </div>
                                            <!-- Fim da Tela de Endereços Adicionais -->
                                        </div>
                                    </div>  
                                </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary">Atualizar</button>
                                <a href="home" class="btn btn-danger">Voltar</a>
                            </div>
                        </div>
                        </form>
                    <?php } else { ?>
                        <h1 class="page-header"> Voce não possue acesso a está pagina, consulte o administrador.</h1>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>

<script>
    jQuery(document).ready(function () {
        // Remove seleção de ativo no menu.
        $('.nav li').removeClass('active');
        $('#side-menu li').removeClass('active');
        // Ativa botão no menu.
        $('#li-contatos ul').addClass('collapse in');
        $('#li-cad-tipos-contato a').addClass('active');
        $('#li-contatos').addClass('active');
        $('#li-contatos a').addClass('collapse in');


        // -- INICIO - Ações para eliminar dados dos bancos -- //
        var divContentEmail = $('#EmailAdicional');
        var divContentTelefone = $('#TelefoneAdicional');
        var divContentEndereco = $('#EnderecoAdicional');

        //Cliquando em remover a linha é eliminada
        $('#EmailAdicional').on('click', '#linkRemoverEmail', function () {
            var idEmail = $(this).attr('idemail');
            $.get('delete_email?idEmail=' + idEmail);
            $(this).parents('#ConteudoEmail').remove();
        });

        //Cliquando em remover a linha é eliminada
        $('#TelefoneAdicional').on('click', '#linkRemoverTelefone', function () {
            var idTelefone = $(this).attr('idTelefone');
            $.get('delete_telefone?idTelefone=' + idTelefone);
            $(this).parents('#ConteudoTelefone').remove();
        });

        //Cliquando em remover a linha é eliminada
        $('#EnderecoAdicional').on('click', '#linkRemoverEndereco', function () {
            var idEndereco = $(this).attr('idEndereco');
            $.get('delete_endereco?idEndereco=' + idEndereco);
            $(this).parents('#ConteudoEndereco').remove();
        });
        // -- FIM - Ações para eliminar dados dos bancos -- //


    });
</script>
<?php include(kohana::find_file('views/templates/adm', 'busca_cep_cidade_estados', 'php')) ?>
</body>
</html>
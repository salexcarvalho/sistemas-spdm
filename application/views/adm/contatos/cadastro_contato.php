<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - CONTATOS TELEFÔNICOS</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <?php if ($_SESSION['AcLiberaBtnAddTpCon'] == TRUE) { ?>
                            <h1 class="page-header">Cadastrar Contato</h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-phone"></i> Contatos Telefônicos</li>
                                <li class="active"><i class="fa fa-plus-circle"></i> Adicionar Novo</li>
                            </ol>
                            <form class="form-horizontal" role="form" method="POST" action="insert_contato">
                                <div class="form-group">
                                    <div class="col-md-6">   
                                        <div class="panel panel-default"> 
                                            <div class="panel-heading">
                                                <div class="col-md-9">
                                                    Dados do Contato
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group" style="margin-top: -7px; float:right;"> 
                                                        <label class="radio-inline">
                                                            <input required type="radio" name="status" id="inativo" value="1">
                                                            Ativo
                                                        </label>
                                                        <label class="radio-inline">
                                                            <input required type="radio" name="status" id="inativo" value="0">
                                                            Inativo
                                                        </label>
                                                    </div>
                                                </div>
                                            </div> 
                                            <div class="panel-body">
                                                <!-- Inicio da Tela de Contato -->     
                                                <div id="dados">
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Nome</label>
                                                        <div class="col-md-2" style="padding-right: 0px;">
                                                            <select class="form-control input-md" id="idTipoTratamento" name="idTipoTratamento" required >
                                                                <option value="NULL">Selecione uma Opção</option>
                                                                <?php
                                                                foreach ($tratamentos as $tratamento) {
                                                                    ?>
                                                                    <option value="<?= $tratamento['idTipoTratamento']; ?>"><?= $tratamento['Descricao']; ?></option>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <input type="text" min="0" class="form-control input-md" id="Nome" name="Nome" placeholder="Informe o nome" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Aniversário</label>
                                                        <div class="col-md-3">
                                                            <input type="date" min="0" class="form-control input-md" id="dtNascimento" name="dtNascimento" placeholder="Informe a data de nascimento">
                                                        </div>
                                                        <label class="col-md-1 control-label">Cargo</label>
                                                        <div class="col-md-6">
                                                            <input type="text" min="0" class="form-control input-md" id="Cargo" name="Cargo" placeholder="Informe o Cargo">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Departamento</label>
                                                        <div class="col-md-10">
                                                            <input type="text" min="0" class="form-control input-md" id="Departamento" name="Departamento" placeholder="Informe o Departamento ou Unidade">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Observação</label>
                                                        <div class="col-md-10">
                                                            <input type="text" min="0" class="form-control input-md" id="Obs" name="Obs" placeholder="Descreva a observação que deseja informar">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Grupo</label>
                                                        <div class="col-md-10">
                                                            <select class="form-control input-md" id="idTipoGrupo" name="idTipoGrupo" required >
                                                                <option value="">Selecione uma Grupo</option>
                                                                <?php
                                                                foreach ($grupos as $grupo) {
                                                                    ?>
                                                                    <option value="<?= $grupo['idTipoGrupo']; ?>"><?= $grupo['Grupo']; ?></option>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Email</label>
                                                        <div class="col-md-10">
                                                            <input type="email" min="0" class="form-control input-md" id="EmailP" name="EmailP" placeholder="Informe o Email">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Celular</label>
                                                        <div class="col-md-4">
                                                            <input type="tel" min="0" class="form-control input-md" id="Celular_Principal" name="Celular_Principal" placeholder="Informe o Celular" data-mask="(99) 9999-9999?9">
                                                        </div>
                                                        <label class="col-md-2 control-label">Tel. Comercial</label>
                                                        <div class="col-md-4">
                                                            <input type="tel" min="0" class="form-control input-md" id="Telefone_Escritorio" name="Telefone_Escritorio" placeholder="Informe o Telefone Comercial" data-mask="(99) 9999-9999">
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Fim da Tela de Contato -->
                                                <ol class="breadcrumb">
                                                    <li><i class="fa fa-home"></i> Endereço Principal</li>
                                                </ol>
                                                <!-- Inicio da Tela de Endereços -->
                                                <div id="Endereco_Principal">
                                                    <div class="form-group">    
                                                        <label class="col-md-2 control-label">Informe o CEP</label>
                                                        <div class="col-md-3">
                                                            <input type="text" class="form-control input-md Cep" id="Endereco_Cep_P" name="Endereco_Cep_P" placeholder="CEP" data-mask="99999-999" iddiv="Endereco_Principal" onblur="CarregaCep(this)" >
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Logradouro</label>
                                                        <div class="col-md-8">
                                                            <input type="text" class="form-control input-md Logradouro" id="Endereco_Logradouro_P" name="Endereco_Logradouro_P" placeholder="Informe o Endereço">
                                                        </div>
                                                        <div class="col-md-2">
                                                            <input type="number" min="0" class="form-control input-md" id="Endereco_Numero_P" name="Endereco_Numero_P" placeholder="Número">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">    
                                                        <label class="col-md-2 control-label">Complemento</label>
                                                        <div class="col-md-3">
                                                            <input type="text" min="0" class="form-control input-md Complemento" id="Endereco_Complemento_P" name="Endereco_Complemento_P" placeholder="Informe o Complemento">
                                                        </div>
                                                        <label class="col-md-1 control-label">Bairro</label>
                                                        <div class="col-md-6">
                                                            <input type="text" class="form-control input-md Bairro" id="Endereco_Bairro_P" name="Endereco_Bairro_P" placeholder="Informe o Bairro">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-2 control-label">Estado</label>
                                                        <div class="col-md-3">
                                                            <input type="text" class="col-md-12 form-control input-md Estado" id="Endereco_Estado_P" name="Endereco_Estado_P" placeholder="Pesquisar estado..." autocomplete="off" onkeyup="CarregaEstado()" />                                            
                                                        </div>
                                                        <label class="col-md-1 control-label">Cidade</label>
                                                        <div class="col-md-6">
                                                            <input type="text" class="col-md-12 form-control input-md Cidade" id="Endereco_Cidade_P" name="Endereco_Cidade_P" placeholder="Pesquisar cidade..." autocomplete="off" onkeyup="CarregaCidade()" />
                                                        </div>
                                                    </div>
                                                </div>    
                                                <!-- Fim da Tela de Endereços -->    
                                            </div>
                                        </div> 
                                    </div>

                                    
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-primary">Cadastrar</button>
                                        <a href="home" class="btn btn-danger">Voltar</a>
                                    </div>
                                </div>
                            </form>
                        <?php } else { ?>
                            <h1 class="page-header"> Voce não possue acesso a está pagina, consulte o administrador.</h1>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>

    <script>
        jQuery(document).ready(function () {
            // Remove seleção de ativo no menu.
            $('.nav li').removeClass('active');
            $('#side-menu li').removeClass('active');
            // Ativa botão no menu.
            $('#li-contatos ul').addClass('collapse in');
            $('#li-cad-tipos-contato a').addClass('active');
            $('#li-contatos').addClass('active');
            $('#li-contatos a').addClass('collapse in');

            // -- INICIO - Addiciona campo para o email adcional-- //
            var divContentEmail = $('#EmailAdicional');
            var botaoAdicionarEmail = $('#addEmail');
            var ContEmail = 2;

            //Ao clicar em adicionar ele cria uma linha com novos campos
            $(botaoAdicionarEmail).click(function () {

                var codEmail = '<div id="ConteudoEmail" class="form-group"><div class="col-md-3"><select class="form-control input-md" id="idTipoEmail_Add_' + ContEmail + '" name="idTipoEmail[]"><option value="">Selecione um tipo</option><?php foreach ($emails as $email) { ?> <option value="<?= $email['idTipoEmail']; ?>"><?= $email['Descricao']; ?></option><?php } ?></select></div><div class="col-md-3"><input type="text" min="0" class="form-control input-md" id="Obs_Email_Add_' + ContEmail + '" name="Obs_Email_Add[]" placeholder="Observações"></div><div class="col-md-5 input-group"><input type="email" min="0" class="form-control input-md" id="Email_Add_' + ContEmail + '" name="Email_Add[]" placeholder="Informe o email adicional ' + ContEmail + '"><span class="input-group-btn"><a id="linkRemoverEmail" class="btn btn-danger btn-md"><i class="fa fa-minus-circle"></i></a></span></div></div></div>';

                $(codEmail).appendTo(divContentEmail);
                ContEmail++;
            });

            //Cliquando em remover a linha é eliminada

            $('#EmailAdicional').on('click', '#linkRemoverEmail', function () {
                $(this).parents('#ConteudoEmail').remove();
                ContEmail--;
            });
            // -- FIM - Addiciona campo para o email adcional-- //


            // --Addiciona campo para o telefone adcional-- //
            var divContentTelefone = $('#TelefoneAdicional');
            var botaoAdicionarTelefone = $('#addTel');
            var ContTelefone = 2;

            //Ao clicar em adicionar ele cria uma linha com novos campos
            $(botaoAdicionarTelefone).click(function () {

                var codTelefone = '<div id="ConteudoTelefone" class="form-group"><div class="col-md-3"><select class="form-control input-md" id="idTipoTel_Add_' + ContTelefone + '" name="idTipoTel[]"><option value="">Selecione um tipo</option><?php foreach ($tels as $tel) { ?> <option value="<?= $tel['idTipoTel']; ?>"><?= $tel['Descricao']; ?></option> <?php } ?></select></div><div class="col-md-3"><input type="text" min="0" class="form-control input-md" id="Obs_Tel_Add_' + ContTelefone + '" name="Obs_Tel_Add[]" placeholder="Observações"></div><div class="col-md-5 input-group"><input type="text" min="0" class="form-control input-md" id="Tel_Add_' + ContTelefone + '" name="Tel_Add[]" placeholder="Informe o telefone ou celular adicional ' + ContTelefone + '" data-mask="(99) 9999-9999?9"><span class="input-group-btn"><a id="linkRemoverTelefone" class="btn btn-danger btn-md"><i class="fa fa-minus-circle"></i></a></span></div></div>';

                $(codTelefone).appendTo(divContentTelefone);
                ContTelefone++;
            });

            //Cliquando em remover a linha é eliminada
            $('#TelefoneAdicional').on('click', '#linkRemoverTelefone', function () {
                $(this).parents('#ConteudoTelefone').remove();
                ContTelefone--;
            });
            // --Addiciona campo para o telefone adcional-- //



            // -- INICIO - Addiciona campo para o endereço adcional-- //
            var divContentEndereco = $('#EnderecoAdicional');
            var botaoAdicionarEndereco = $('#addEndereco');
            var ContEndereco = 2;

            //Ao clicar em adicionar ele cria uma linha com novos campos
            $(botaoAdicionarEndereco).click(function () {

                var codEndereco = '<div id="ConteudoEndereco"><ol class="breadcrumb"><li><i class="fa fa-hospital-o"></i>Endereço Adicional Nº ' + ContEndereco + '</li></ol><div id="Endereco_Add_' + ContEndereco + '"><div class="form-group"><label class="col-md-2 control-label">Informe o CEP</label><div class="col-md-3"><input type="text" class="form-control input-md Cep" id="Endereco_Cep_Add_' + ContEndereco + '" name="Endereco_Cep[]" placeholder="CEP" data-mask="99999-999" iddiv="Endereco_Add_' + ContEndereco + '" onblur="CarregaCep(this)"></div><label class="col-md-1 control-label">Local</label><div class="col-md-5 input-group"><input type="text" class="form-control input-md" id="Local_Endereco_Add_' + ContEndereco + '" name="Local_Endereco[]" placeholder="Informe o Local. Ex: Casa, Escritório, etc..."><span class="input-group-btn"><a id="linkRemoverEndereco" class="btn btn-danger btn-md"><i class="fa fa-minus-circle"></i></a></span></div></div><div class="form-group"><label class="col-md-2 control-label">Logradouro</label><div class="col-md-8"><input type="text" class="form-control input-md Logradouro" id="Endereco_Logradouro_Add_' + ContEndereco + '" name="Endereco_Logradouro[]" placeholder="Informe o Endereço"></div><div class="col-md-2"><input type="number" min="0" class="form-control input-md" id="Endereco_Numero_Add_' + ContEndereco + '" name="Endereco_Numero[]" placeholder="Número"></div></div><div class="form-group"><label class="col-md-2 control-label">Complemento</label><div class="col-md-3"><input type="text" min="0" class="form-control input-md Complemento" id="Endereco_Complemento_Add_' + ContEndereco + '" name="Endereco_Complemento[]" placeholder="Informe o Complemento"></div><label class="col-md-1 control-label">Bairro</label><div class="col-md-6"><input type="text" class="form-control input-md Bairro" id="Endereco_Bairro_Add_' + ContEndereco + '" name="Endereco_Bairro[]" placeholder="Informe o Bairro"></div></div><div class="form-group"><label class="col-md-2 control-label">Estado</label><div class="col-md-3"><input type="text" class="col-md-12 form-control input-md Estado" id="Endereco_Estado_Add_' + ContEndereco + '" name="Endereco_Estado[]" placeholder="Pesquisar estado..." autocomplete="off" onkeyup="CarregaEstado()" /></div><label class="col-md-1 control-label">Cidade</label><div class="col-md-6"><input type="text" class="col-md-12 form-control input-md Cidade" id="Endereco_Cidade_Add_' + ContEndereco + '" name="Endereco_Cidade[]" placeholder="Pesquisar cidade..." autocomplete="off" onkeyup="CarregaCidade()" /></div></div></div></div>';

                $(codEndereco).appendTo(divContentEndereco);
                ContEndereco++;
            });

            //Cliquando em remover a linha é eliminada

            $('#EnderecoAdicional').on('click', '#linkRemoverEndereco', function () {
                $(this).parents('#ConteudoEndereco').remove();
                ContEndereco--;
            });
            // -- FIM - Addiciona campo para o endereço adcional-- //

        });
    </script>
    <?php include(kohana::find_file('views/templates/adm', 'busca_cep_cidade_estados', 'php')) ?>
</body>
</html>
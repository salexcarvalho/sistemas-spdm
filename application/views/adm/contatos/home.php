<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - CONTATOS TELEFÔNICOS</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Contatos</h1>
                        <ol class="breadcrumb">
                            <li><i class="fa fa-phone"></i> Contatos Telefônicos</li>
                            <li class="active"><i class="fa fa-user"></i> Lista de Contatos</li>
                        </ol>

                        <?php if (isset($warning)) : ?>
                            <div id="msg" class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong><?= $warning; ?></strong>
                            </div>
                        <?php endif ?>                          

                        <div class="row" style="margin-top:-5px; margin-bottom: 15px;">
                            <div class="form-group">
                                <div class="col-md-6">    
                                    <form class="form-inline" method="get" action="home">                      
                                        <input  type="text" class="form-control input-md" id="Busca" name="Busca" placeholder="Informe o Nome de Contato ou Empresa" style="width: 350px;">
                                        <button type="submit" class="btn btn-danger btn-md"><i class="glyphicon glyphicon-search"></i> Buscar</button>
                                    </form>
                                </div>
                                <div class="col-md-6">    
                                    <?php if ($_SESSION['AcLiberaBtnAddTpCon'] == TRUE) { ?>
                                        <a href="cadastro_contato">
                                            <button type="submit" id="btn_novo_contato" class="pull-right btn btn-primary btn-md" name="btn_novo_contato"><i class="glyphicon glyphicon-plus-sign"></i> Adicionar Contato</button>
                                        </a>
                                    <?php } ?>
                                </div>
                            </div>  
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="tipos-contato">
                                <thead>
                                    <tr>
                                        <th width="5%" style="text-align: center">#</th>
                                        <th >Nome</th>
                                        <th width="22%">Email</th>
                                        <th width="10%" style="text-align: center">Telefone</th>
                                        <th width="10%" style="text-align: center">Celular</th>
                                        <th width="14%" style="text-align: center" >Ações</th>
                                        <th width="6%" style="text-align: center">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($data as $contato) {
                                        ?>
                                        <tr>
                                            <td align="center"><?= $contato['idContato']; ?></td>
                                            <td><?php if ($contato['Tratamento'] !== 'Sem Tratamento') { ?> <?= $contato['Tratamento']; ?><?php } ?> <?= $contato['Nome']; ?></td>
                                            <td><?= $contato['EmailP']; ?></td>
                                            <td style="text-align: center"><?php echo Funcoes::formataTelefone($contato['Telefone_Escritorio']); ?></td>
                                            <td style="text-align: center"><?php echo Funcoes::formataTelefone($contato['Celular_Principal']); ?></td>
                                            <td align="center">
                                                <form style="display: inline-block;" role="form" method="POST" action="visualiza_contato">
                                                    <input type="hidden" id="idContato_<?= $contato['idContato']; ?>" name="idContato" value="<?= $contato['idContato']; ?>">
                                                    <button  type="submit" id="btnVisualiza" class="btn btn-default btn-xs" name="btnVisualiza" title="Visualizar">
                                                        <i class="fa fa-eye"></i>
                                                    </button>
                                                </form>    
                                                <?php if ($_SESSION['AcLiberaBtnAddTpCon'] == TRUE) { ?>
                                                    <button  type="button" id="btnInserirEmail" class="btn btn-primary btn-xs" name="btnInserirEmail" idContato="<?= $contato['idContato']; ?>" Nome="<?= $contato['Nome']; ?>" Status="<?= $contato['Status']; ?>" <?php if ($_SESSION['AcLiberaBtnEdiTpCon'] == NULL) { ?>disabled<?php } ?> data-toggle="modal" data-target="#modal_email" data-placement="bottom" title="Inserir Emails" data-delay="1">
                                                        <i class="fa fa-envelope-o"></i>
                                                    </button>
                                                    <button  type="button" id="btnInserirTel" class="btn btn-primary btn-xs" name="btnInserirTel" idContato="<?= $contato['idContato']; ?>" Nome="<?= $contato['Nome']; ?>" Status="<?= $contato['Status']; ?>" <?php if ($_SESSION['AcLiberaBtnEdiTpCon'] == NULL) { ?>disabled<?php } ?> data-toggle="modal" data-target="#modal_telefone" data-placement="bottom" title="Inserir Telefones" data-delay="1">
                                                        <i class="fa fa-phone"></i>
                                                    </button>
                                                    <button  type="button" id="btnInserirEndereco" class="btn btn-primary btn-xs" name="btnInserirEndereco" idContato="<?= $contato['idContato']; ?>" Nome="<?= $contato['Nome']; ?>" Status="<?= $contato['Status']; ?>" <?php if ($_SESSION['AcLiberaBtnEdiTpCon'] == NULL) { ?>disabled<?php } ?> data-toggle="modal" data-target="#modal_endereco" data-placement="bottom" title="Inserir Endereços" data-delay="1">
                                                        <i class="fa fa-home"></i>
                                                    </button>
                                                <?php } ?>
                                                <form style="display: inline-block;" role="form" method="POST" action="editar_contato">
                                                    <input hidden id="page" name="page" value="<?= $PaginaAtual; ?>">
                                                    <input type="hidden" id="idContato_<?= $contato['idContato']; ?>" name="idContato" value="<?= $contato['idContato']; ?>">
                                                    <button  type="submit" id="btnEditar" class="btn btn-default btn-xs" name="btnEditar" idContato="<?= $contato['idContato']; ?>" title="Editar Contato" <?php if ($_SESSION['AcLiberaBtnEdiTpCon'] == NULL) { ?>disabled<?php } ?>>
                                                        <i class="glyphicon glyphicon-edit"></i>
                                                    </button>
                                                </form>
                                                <input type="hidden" id="idContato_<?= $contato['idContato']; ?>" name="idContato" value="<?= $contato['idContato']; ?>">
                                                <button  type="button" id="btnExcluir" class="btn btn-danger btn-xs" name="btnExcluir" idContato="<?= $contato['idContato']; ?>" Nome="<?= $contato['Nome']; ?>" <?php if ($_SESSION['AcLiberaBtnExcTpCon'] == NULL) { ?>disabled<?php } ?> data-toggle="modal" data-target="#modal_excluir" data-placement="bottom" title="Excluir Contato" data-delay="1">
                                                    <i class="glyphicon glyphicon-trash"></i>
                                                </button>
                                            </td>
                                            <td align="center">
                                                <input type="hidden" name="idContato" value="<?= $contato['idContato']; ?>">
                                                <?php
                                                if ($contato['Status'] == "0") {
                                                    ?>
                                                    <button type="submit" id="btnAtivar_<?= $contato['idContato']; ?>" class="btn btn-danger btn-xs" idContato="<?= $contato['idContato']; ?>" name="btnAtivar" title="Ativar Contato" <?php if ($_SESSION['AcLiberaBtnAtivTpCon'] == NULL) { ?>disabled<?php } ?>>
                                                        <i id="activ_<?= $contato['idContato']; ?>" class="glyphicon glyphicon-ban-circle"></i>
                                                    </button>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <button type="submit" id="btnAtivar_<?= $contato['idContato']; ?>" class="btn btn-success btn-xs" idContato="<?= $contato['idContato']; ?>" name="btnAtivar" title="Destivar Contato" <?php if ($_SESSION['AcLiberaBtnAtivTpCon'] == NULL) { ?>disabled<?php } ?>>
                                                        <i id="activ_<?= $contato['idContato']; ?>" class="glyphicon glyphicon-ok"></i>
                                                    </button>
                                                    <?php
                                                }
                                                ?>

                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                            <?php include(kohana::find_file('views/paginator', 'paginacao', 'php')) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Inicio Modal Excluir -->
        <div class="modal fade" id="modal_excluir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Excluir Contato</h4>
                    </div>
                    <form class="form-horizontal" role="form" method="POST" action="delete_contato">
                        <input type="hidden" id="idContato" name="idContato" value="">
                        <div class="modal-body" id="modal_delete_contato">Deseja excluir este contato?</div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                            <button type="submit" class="btn btn-primary" id="modal_btn-excluir">Excluir</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Fim Modal Excluir -->

        <!-- Inicio Modal Email -->
        <div class="modal fade" id="modal_email" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Adicionar novos emails</h4>
                    </div>
                    <form class="form-horizontal" role="form" method="POST" action="insert_emails">
                        <div class="modal-body" id="modal_insere_email">
                            <ol class="breadcrumb">
                                <li><i class="fa fa-envelope"></i> Emails Adicionais</li>
                                <a href="#" id="addEmail" class="pull-right btn btn-primary btn-xs">
                                    <i class="glyphicon glyphicon-plus-sign"></i> Adicionar Novo Email 
                                </a>
                            </ol>
                            <div id="EmailAdicional">
                                <div id="NomeEmail" style="margin-bottom:18px; margin-left: 10px;"></div>
                                <input type="hidden" id="idContatoEmail" name="idContatoEmail" value="">
                                <input type="hidden" id="StatusEmail" name="status" value="">
                                <div class="form-group">
                                    <div class="col-md-3">
                                        <select class="form-control" id="idTipoEmail_Add_1" name="idTipoEmail[]">
                                            <option value="">Selecione um tipo</option>
                                            <?php
                                            foreach ($emails as $email) {
                                                ?>
                                                <option value="<?= $email['idTipoEmail']; ?>"><?= $email['Descricao']; ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <input type="text" min="0" class="form-control" id="Obs_Email_Add_1" name="Obs_Email_Add[]" placeholder="Observações">
                                    </div>
                                    <div class="col-md-5 input-group">
                                        <input type="email" min="0" class="form-control" id="Email_Add_1" name="Email_Add[]" placeholder="Informe o email adicional 1">
                                        <span class="input-group-btn">
                                            <a class="btn btn-default disabled">
                                                <i class="fa fa-envelope-o"></i>
                                            </a>
                                        </span>
                                    </div>
                                </div>    
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                            <button type="submit" class="btn btn-primary">Cadastrar</button>
                        </div>
                    </form>    
                </div>
            </div>
        </div>
        <!-- Fim Modal Email -->

        <!-- Inicio Modal Telefone -->
        <div class="modal fade" id="modal_telefone" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Adicionar novos telefones</h4>
                    </div>
                    <form class="form-horizontal" role="form" method="POST" action="insert_telefones">
                        <div class="modal-body" id="modal_insere_telefone">
                            <ol class="breadcrumb">
                                <li><i class="fa fa-phone"></i> Telefones Adicionais</li>
                                <a href="#" id="addTel" class="pull-right btn btn-primary btn-xs">
                                    <i class="glyphicon glyphicon-plus-sign"></i> Adicionar Novo Telefone
                                </a>
                            </ol>

                            <div id="TelefoneAdicional">
                                <div id="NomeTelefone" style="margin-bottom:18px; margin-left: 10px;"></div>
                                <input type="hidden" id="idContatoTelefone" name="idContatoTelefone" value="">
                                <input type="hidden" id="StatusTelefone" name="status" value="">
                                <div class="form-group">
                                    <div class="col-md-3">
                                        <select class="form-control" id="idTipoTel_Add_1" name="idTipoTel[]">
                                            <option value="">Selecione um tipo</option>
                                            <?php
                                            foreach ($tels as $tel) {
                                                ?>
                                                <option value="<?= $tel['idTipoTel']; ?>"><?= $tel['Descricao']; ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <input type="text" min="0" class="form-control" id="Obs_Tel_Add_1" name="Obs_Tel_Add[]" placeholder="Observações">
                                    </div>
                                    <div class="col-md-5 input-group">
                                        <input type="text" min="0" class="form-control" id="Tel_Add_1" name="Tel_Add[]" placeholder="Informe o telefone ou celular adicional 1" data-mask="(99) 9999-9999?9">
                                        <span class="input-group-btn">
                                            <a class="btn btn-default disabled">
                                                <i class="fa fa-phone"></i>
                                            </a>
                                        </span>
                                    </div>
                                </div>
                            </div> 
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                            <button type="submit" class="btn btn-primary">Cadastrar</button>
                        </div>
                    </form>    
                </div>
            </div>
        </div>
        <!-- Fim Modal Telefone -->

        <!-- Inicio Modal Endereço -->
        <div class="modal modal-wide fade" id="modal_endereco" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Adicionar novos endereços</h4>
                    </div>
                    <form class="form-horizontal" role="form" method="POST" action="insert_enderecos">
                        <div class="modal-body" id="modal_insere_endereco">
                            <ol class="breadcrumb">
                                <li><i class="fa fa-hospital-o"></i> Endereço Adicional Nº 1</li>
                                <a href="#" id="addEndereco" class="pull-right btn btn-primary btn-xs">
                                    <i class="glyphicon glyphicon-plus-sign"></i> Adicionar Novo Endereço
                                </a>
                            </ol>
                            <div id="EnderecoAdicional">
                                <div id="NomeEndereco" style="margin-bottom:18px; margin-left: 10px;"></div>
                                <input type="hidden" id="idContatoEndereco" name="idContatoEndereco" value="">
                                <input type="hidden" id="StatusEndereco" name="status" value="">
                                <div id="Endereco_Add_1">
                                    <div class="form-group">  
                                        <label class="col-md-2 control-label">Informe o CEP</label>
                                        <div class="col-md-3">
                                            <input type="text" class="form-control Cep" id="Endereco_Cep_Add_1" name="Endereco_Cep[]" placeholder="CEP" data-mask="99999-999" iddiv="Endereco_Add_1" onblur="CarregaCep(this)">
                                        </div>
                                        <label class="col-md-1 control-label">Local</label>
                                        <div class="col-md-5 input-group">
                                            <input type="text" class="form-control" id="Local_Endereco_Add_1" name="Local_Endereco[]" placeholder="Informe o Local. Ex: Casa, Escritório, etc...">
                                            <span class="input-group-btn">
                                                <a class="btn btn-default disabled">
                                                    <i class="fa fa-home"></i>
                                                </a>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Logradouro</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control Logradouro" id="Endereco_Logradouro_Add_1" name="Endereco_Logradouro[]" placeholder="Informe o Endereço">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="number" min="0" class="form-control" id="Endereco_Numero_Add_1" name="Endereco_Numero[]" placeholder="Número">
                                        </div>
                                    </div>
                                    <div class="form-group">    
                                        <label class="col-md-2 control-label">Complemento</label>
                                        <div class="col-md-3">
                                            <input type="text" min="0" class="form-control Complemento" id="Endereco_Complemento_Add_1" name="Endereco_Complemento[]" placeholder="Informe o Complemento">
                                        </div>
                                        <label class="col-md-1 control-label">Bairro</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control Bairro" id="Endereco_Bairro_Add_1" name="Endereco_Bairro[]" placeholder="Informe o Bairro">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Estado</label>
                                        <div class="col-md-3">
                                            <input type="text" class="col-md-12 form-control Estado" id="Endereco_Estado_Add_1" name="Endereco_Estado[]" placeholder="Pesquisar estado..." autocomplete="off" onkeyup="CarregaEstado()" />                                            
                                        </div>
                                        <label class="col-md-1 control-label">Cidade</label>
                                        <div class="col-md-6">
                                            <input type="text" class="col-md-12 form-control Cidade" id="Endereco_Cidade_Add_1" name="Endereco_Cidade[]" placeholder="Pesquisar cidade..." autocomplete="off" onkeyup="CarregaCidade()" />
                                        </div>
                                    </div>
                                </div>   
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                            <button type="submit" class="btn btn-primary">Cadastrar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Fim Modal Endereço -->

        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>

        <script>
            jQuery(document).ready(function () {
                // Remove seleção de ativo no menu.
                $('.nav li').removeClass('active');
                $('#side-menu li').removeClass('active');
                // Ativa botão no menu.
                $('#li-contatos ul').addClass('collapse in');
                $('#li-cad-tipos-contato a').addClass('active');
                $('#li-contatos').addClass('active');
                $('#li-contatos a').addClass('collapse in');
            });
        </script>

        <script>
            jQuery(document).ready(function ()
            {
                $('#tipos-contato').dataTable(
                        {
                            "aoColumnDefs": [{"bSortable": false, "aTargets": [0, 1, 2, 3, 4, 5, 6, 7]}],
                            "paging": false,
                            "info": false,
                            "bFilter": false,
                            "oLanguage":
                                    {
                                        "sSearch": " Buscar: "
                                    }
                        });

                var idContato;
                var Nome;
                var Status;

                $('body').on("click", 'button[name=btnExcluir]', function () {
                    idContato = $(this).attr('idContato');
                    Nome = $(this).attr('Nome');
                    $('#modal_delete_contato').html("Deseja excluir o Contato <b>" + Nome + "</b> ?");
                    $('#idContato').val(idContato);
                });

                $('body').on("click", 'button[name=btnInserirEmail]', function () {
                    idContato = $(this).attr('idContato');
                    Nome = $(this).attr('Nome');
                    Status = $(this).attr('Status');
                    $('#NomeEmail').html("Você está adicionado email(s) para: <b>" + Nome + "</b>");
                    $('#idContatoEmail').val(idContato);
                    $('#StatusEmail').val(Status);
                });

                $('body').on("click", 'button[name=btnInserirTel]', function () {
                    idContato = $(this).attr('idContato');
                    Nome = $(this).attr('Nome');
                    Status = $(this).attr('Status');
                    $('#NomeTelefone').html("Você está adicionado telefone(s) para: <b>" + Nome + "</b>");
                    $('#idContatoTelefone').val(idContato);
                    $('#StatusTelefone').val(Status);
                });

                $('body').on("click", 'button[name=btnInserirEndereco]', function () {
                    idContato = $(this).attr('idContato');
                    Nome = $(this).attr('Nome');
                    Status = $(this).attr('Status');
                    $('#NomeEndereco').html("Você está adicionado endereço(s) para: <b>" + Nome + "</b>");
                    $('#idContatoEndereco').val(idContato);
                    $('#StatusEndereco').val(Status);
                });


                $("body").on("click", "button[name=btnAtivar]", function () {
                    idContato = $(this).attr('idContato');
                    //alert(idContato);
                    $.get('update_ativar_contato?idContato=' + idContato, function (data) {

                        if (data === "1") {
                            $('#btnAtivar_' + idContato).addClass('btn btn-success btn-xs');
                            $('#btnAtivar_' + idContato).removeClass('btn-danger');
                            $('#activ_' + idContato).removeClass('glyphicon-ban-circle');
                            $('#activ_' + idContato).addClass('glyphicon-ok');
                        } else {
                            $('#btnAtivar_' + idContato).addClass('btn btn-danger btn-xs');
                            $('#btnAtivar_' + idContato).removeClass('btn-success');
                            $('#activ_' + idContato).addClass('glyphicon-ok');
                            $('#activ_' + idContato).addClass('glyphicon-ban-circle');
                        }
                    });
                });




                $('#btn_novo_contato').on("click", function ()
                {
                    window.location.href = "cadastro_contato";
                });

                $('.dataTables_filter label').addClass('pull-right');

                // -- INICIO - Addiciona campo para o email adcional-- //
                var divContentEmail = $('#EmailAdicional');
                var botaoAdicionarEmail = $('#addEmail');
                var ContEmail = 2;

                //Ao clicar em adicionar ele cria uma linha com novos campos
                $(botaoAdicionarEmail).click(function () {

                    var codEmail = '<div id="ConteudoEmail" class="form-group"><div class="col-md-3"><select class="form-control" id="idTipoEmail_Add_' + ContEmail + '" name="idTipoEmail[]"><option value="">Selecione um tipo</option><?php foreach ($emails as $email) { ?> <option value="<?= $email['idTipoEmail']; ?>"><?= $email['Descricao']; ?></option><?php } ?></select></div><div class="col-md-3"><input type="text" min="0" class="form-control" id="Obs_Email_Add_' + ContEmail + '" name="Obs_Email_Add[]" placeholder="Observações"></div><div class="col-md-5 input-group"><input type="email" min="0" class="form-control" id="Email_Add_' + ContEmail + '" name="Email_Add[]" placeholder="Informe o email adicional ' + ContEmail + '"><span class="input-group-btn"><a id="linkRemoverEmail" class="btn btn-danger btn-sm"><i class="fa fa-minus-circle"></i></a></span></div></div></div>';

                    $(codEmail).appendTo(divContentEmail);
                    ContEmail++;
                });

                //Cliquando em remover a linha é eliminada

                $('#EmailAdicional').on('click', '#linkRemoverEmail', function () {
                    $(this).parents('#ConteudoEmail').remove();
                    ContEmail--;
                });
                // -- FIM - Addiciona campo para o email adcional-- //


                // --Addiciona campo para o telefone adcional-- //
                var divContentTelefone = $('#TelefoneAdicional');
                var botaoAdicionarTelefone = $('#addTel');
                var ContTelefone = 2;

                //Ao clicar em adicionar ele cria uma linha com novos campos
                $(botaoAdicionarTelefone).click(function () {

                    var codTelefone = '<div id="ConteudoTelefone" class="form-group"><div class="col-md-3"><select class="form-control" id="idTipoTel_Add_' + ContTelefone + '" name="idTipoTel[]"><option value="">Selecione um tipo</option><?php foreach ($tels as $tel) { ?> <option value="<?= $tel['idTipoTel']; ?>"><?= $tel['Descricao']; ?></option> <?php } ?></select></div><div class="col-md-3"><input type="text" min="0" class="form-control" id="Obs_Tel_Add_' + ContTelefone + '" name="Obs_Tel_Add[]" placeholder="Observações"></div><div class="col-md-5 input-group"><input type="text" min="0" class="form-control" id="Tel_Add_' + ContTelefone + '" name="Tel_Add[]" placeholder="Informe o telefone ou celular adicional ' + ContTelefone + '" data-mask="(99) 9999-9999?9"><span class="input-group-btn"><a id="linkRemoverTelefone" class="btn btn-danger btn-sm"><i class="fa fa-minus-circle"></i></a></span></div></div>';

                    $(codTelefone).appendTo(divContentTelefone);
                    ContTelefone++;
                });

                //Cliquando em remover a linha é eliminada
                $('#TelefoneAdicional').on('click', '#linkRemoverTelefone', function () {
                    $(this).parents('#ConteudoTelefone').remove();
                    ContTelefone--;
                });
                // --Addiciona campo para o telefone adcional-- //



                // -- INICIO - Addiciona campo para o endereço adcional-- //
                var divContentEndereco = $('#EnderecoAdicional');
                var botaoAdicionarEndereco = $('#addEndereco');
                var ContEndereco = 2;

                //Ao clicar em adicionar ele cria uma linha com novos campos
                $(botaoAdicionarEndereco).click(function () {

                    var codEndereco = '<div id="ConteudoEndereco"><ol class="breadcrumb"><li><i class="fa fa-hospital-o"></i> Endereço Adicional Nº ' + ContEndereco + '</li></ol><div id="Endereco_Add_' + ContEndereco + '"><div class="form-group"><label class="col-md-2 control-label">Informe o CEP</label><div class="col-md-3"><input type="text" class="form-control Cep" id="Endereco_Cep_Add_' + ContEndereco + '" name="Endereco_Cep[]" placeholder="CEP" data-mask="99999-999" iddiv="Endereco_Add_' + ContEndereco + '" onblur="CarregaCep(this)"></div><label class="col-md-1 control-label">Local</label><div class="col-md-5 input-group"><input type="text" class="form-control" id="Local_Endereco_Add_' + ContEndereco + '" name="Local_Endereco[]" placeholder="Informe o Local. Ex: Casa, Escritório, etc..."><span class="input-group-btn"><a id="linkRemoverEndereco" class="btn btn-danger"><i class="fa fa-minus-circle"></i></a></span></div></div><div class="form-group"><label class="col-md-2 control-label">Logradouro</label><div class="col-md-8"><input type="text" class="form-control Logradouro" id="Endereco_Logradouro_Add_' + ContEndereco + '" name="Endereco_Logradouro[]" placeholder="Informe o Endereço"></div><div class="col-md-2"><input type="number" min="0" class="form-control" id="Endereco_Numero_Add_' + ContEndereco + '" name="Endereco_Numero[]" placeholder="Número"></div></div><div class="form-group"><label class="col-md-2 control-label">Complemento</label><div class="col-md-3"><input type="text" min="0" class="form-control Complemento" id="Endereco_Complemento_Add_' + ContEndereco + '" name="Endereco_Complemento[]" placeholder="Informe o Complemento"></div><label class="col-md-1 control-label">Bairro</label><div class="col-md-6"><input type="text" class="form-control Bairro" id="Endereco_Bairro_Add_' + ContEndereco + '" name="Endereco_Bairro[]" placeholder="Informe o Bairro"></div></div><div class="form-group"><label class="col-md-2 control-label">Estado</label><div class="col-md-3"><input type="text" class="col-md-12 form-control Estado" id="Endereco_Estado_Add_' + ContEndereco + '" name="Endereco_Estado[]" placeholder="Pesquisar estado..." autocomplete="off" onkeyup="CarregaEstado()" /></div><label class="col-md-1 control-label">Cidade</label><div class="col-md-6"><input type="text" class="col-md-12 form-control Cidade" id="Endereco_Cidade_Add_' + ContEndereco + '" name="Endereco_Cidade[]" placeholder="Pesquisar cidade..." autocomplete="off" onkeyup="CarregaCidade()" /></div></div></div></div>';

                    $(codEndereco).appendTo(divContentEndereco);
                    ContEndereco++;
                });

                //Cliquando em remover a linha é eliminada

                $('#EnderecoAdicional').on('click', '#linkRemoverEndereco', function () {
                    $(this).parents('#ConteudoEndereco').remove();
                    ContEndereco--;
                });
                // -- FIM - Addiciona campo para o endereço adcional-- //
            });
        </script>
        <?php include(kohana::find_file('views/templates/adm', 'busca_cep_cidade_estados', 'php')) ?>
    </body>
</html>
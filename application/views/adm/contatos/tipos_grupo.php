<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - CONTATOS TELEFÔNICOS</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Tipos de Grupos</h1>
                        <ol class="breadcrumb">
                            <li><i class="fa fa-phone"></i> Contatos Telefônicos</li>
                            <li class="active"><i class="fa fa-group"></i> Tipos de Grupos</li>
                            <li class="active"><i class="fa fa-list"></i> Listagem</li>
                        </ol>

                        <?php if (isset($warning)) : ?>
                            <div id="msg" class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong><?= $warning; ?></strong>
                            </div>
                        <?php endif ?>                          
                        <div class="row">
                            <?php if ($_SESSION['AcLiberaBtnAddTpGr'] == TRUE) { ?>
                                <div class="col-lg-12">
                                    <button type="submit" id="btn_novo_tipo" class="pull-right btn btn-primary btn-md" name="btn_novo_tipo"><i class="glyphicon glyphicon-plus-sign"></i> Adicionar Tipo</button>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="tipos-grupo">
                                <thead>
                                    <tr>
                                        <th width="5%" style="text-align: center">#</th>
                                        <th >Nome do tipo</th>
                                        <th >Descrição</th>
                                        <th width="12%" style="text-align: center">Data de Cadastro</th>
                                        <th width="10%" style="text-align: center" >Ações</th>
                                        <th width="6%" style="text-align: center">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($data as $tipo) {
                                        ?>
                                        <tr>
                                            <td align="center"><?= $tipo['idTipoGrupo']; ?></td>
                                            <td><?= $tipo['Grupo']; ?></td>
                                            <td><?= $tipo['Descricao']; ?></td>
                                            <td style="text-align: center"><?= strftime('%d/%m/%Y', strtotime($tipo['dtCadastro'])); ?></td>
                                            <td align="center">
                                                <button  type="submit" id="btnEditar" class="btn btn-default btn-xs" name="btnEditar" page="<?= $PaginaAtual; ?>" idTipoGrupo="<?= $tipo['idTipoGrupo']; ?>" title="Editar Tipo de Grupo" <?php if ($_SESSION['AcLiberaBtnEdiTpGr'] == NULL) { ?>disabled<?php } ?>>
                                                    <i class="glyphicon glyphicon-edit"></i>
                                                </button>
                                                <input type="hidden" id="idTipoGrupo_<?= $tipo['idTipoGrupo']; ?>" name="idTipoGrupo" value="<?= $tipo['idTipoGrupo']; ?>">
                                                <button  type="button" id="btnExcluir" class="btn btn-default btn-xs" name="btnExcluir" idTipoGrupo="<?= $tipo['idTipoGrupo']; ?>" Grupo="<?= $tipo['Grupo']; ?>" <?php if ($_SESSION['AcLiberaBtnExcTpGr'] == NULL) { ?>disabled<?php } ?> data-toggle="modal" data-target="#modal_excluir" data-placement="bottom" title="Excluir Tipo de Grupo" data-delay="1">
                                                    <i class="glyphicon glyphicon-trash"></i>
                                                </button>
                                            </td>
                                            <td align="center">
                                                <input type="hidden" name="idTipoGrupo" value="<?= $tipo['idTipoGrupo']; ?>">
                                                <?php
                                                if ($tipo['Status'] == "0") {
                                                    ?>
                                                    <button type="submit" id="btnAtivar_<?= $tipo['idTipoGrupo']; ?>" class="btn btn-danger btn-xs" idTipoGrupo="<?= $tipo['idTipoGrupo']; ?>" name="btnAtivar" title="Ativar Tipo de Grupo" <?php if ($_SESSION['AcLiberaBtnAtivTpGr'] == NULL) { ?>disabled<?php } ?>>
                                                        <i id="activ_<?= $tipo['idTipoGrupo']; ?>" class="glyphicon glyphicon-ban-circle"></i>

                                                    </button>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <button type="submit" id="btnAtivar_<?= $tipo['idTipoGrupo']; ?>" class="btn btn-success btn-xs" idTipoGrupo="<?= $tipo['idTipoGrupo']; ?>" name="btnAtivar" title="Destivar Tipo de Grupo" <?php if ($_SESSION['AcLiberaBtnAtivTpGr'] == NULL) { ?>disabled<?php } ?>>
                                                        <i id="activ_<?= $tipo['idTipoGrupo']; ?>" class="glyphicon glyphicon-ok"></i>

                                                    </button>
                                                    <?php
                                                }
                                                ?>

                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                            <?php include(kohana::find_file('views/paginator', 'paginacao', 'php')) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="modal_excluir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Excluir Tipo de Grupo</h4>
                    </div>
                    <div class="modal-body" id="modal_delete_tipo">Deseja excluir este tipo de grupo?</div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        <button type="button" class="btn btn-primary" id="modal_btn-excluir">Excluir</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal -->

        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>

        <script>
            jQuery(document).ready(function () {
                // Remove seleção de ativo no menu.
                $('.nav li').removeClass('active');
                $('#side-menu li').removeClass('active');
                // Ativa botão no menu.
                $('#li-contatos ul').addClass('collapse in');
                $('#li-cad-tipos-grupos a').addClass('active');
                $('#li-contatos').addClass('active');
                $('#li-contatos a').addClass('collapse in');

                // Datatables
                $('#tipos-grupo').dataTable(
                        {
                            "aoColumnDefs": [{"bSortable": false, "aTargets": [3, 4]}],
                            "paging": false,
                            "info": false,
                            "bFilter": false,
                            "oLanguage":
                                    {
                                        "sSearch": " Buscar: "
                                    }
                        });

                // abre o modal para cadastro
                $('button[name=btn_novo_tipo]').click(function () {
                    var idTipoGrupo = '';
                    var options = {
                        url: "cadastro_tipo?idTipoGrupo=" + idTipoGrupo,
                        title: 'Cadastrar Tipo de Grupo',
                        size: eModal.size.md,
                        buttons: [
                            {text: 'GRAVAR', style: 'primary', close: false, type: 'submit', form: 'novotipo'},
                            {text: 'FECHAR', style: 'danger', close: true}
                        ]
                    };
                    eModal.ajax(options);
                });

                //abre o modal de edição
                $('button[name=btnEditar]').click(function () {
                    var idTipoGrupo = $(this).attr('idTipoGrupo');
                    var page = $(this).attr('page');
                    var options = {
                        url: "editar_tipo?idTipoGrupo=" + idTipoGrupo + "&page=" + page,
                        title: 'Editar Tipo de Grupo',
                        size: eModal.size.md,
                        buttons: [
                            {text: 'SALVAR', style: 'primary', close: false, type: 'submit', form: 'novotipo'},
                            {text: 'FECHAR', style: 'danger', close: true}
                        ]
                    };
                    eModal.ajax(options);
                });

                // Opção usada na exclusão 
                var idTipoGrupo;
                var Grupo;
                $('body').on("click", 'button[name=btnExcluir]', function ()
                {
                    idTipoGrupo = $(this).attr('idTipoGrupo');
                    Grupo = $(this).attr('Grupo');
                    $('#modal_delete_tipo').html("Deseja excluir o Tipo de Grupo <b>\"" + Grupo + "\"</b> ?");
                });

                $('#modal_excluir').on('hidden.bs.modal', function ()
                {
                    window.location.replace("home");
                });

                $('#modal_btn-excluir').on("click", function ()
                {
                    var btn = $(this);
                    btn.text('loading');

                    btn.removeClass('btn-primary');
                    btn.removeClass('btn-danger');
                    btn.removeClass('btn-success');
                    btn.addClass('btn-primary');

                    $('#modal_btn-excluir').prop('disabled', true);

                    $.ajax("delete_tipo",
                            {
                                type: "POST",
                                data:
                                        {
                                            idTipoGrupo: idTipoGrupo
                                        }
                            })
                            .done(function (data)
                            {
                                $('#modal_delete_tipo').html('Tipo excluído com sucesso');

                                btn.removeClass('btn-primary');
                                btn.addClass('btn-success');
                            })
                            .fail(function ()
                            {
                                $('#modal_delete_tipo').html('Erro: ' + data);

                                btn.removeClass('btn-primary');
                                btn.addClass('btn-danger');
                            })
                            .always(function ()
                            {
                                btn.text('Excluído');
                            });
                });

                // Opção para ativar e desativar
                $("body").on("click", "button[name=btnAtivar]", function () {
                    idTipoGrupo = $(this).attr('idTipoGrupo');
                    //alert(idTipoGrupo);
                    $.get('update_ativar_tipo?idTipoGrupo=' + idTipoGrupo, function (data) {

                        if (data === "1") {
                            $('#btnAtivar_' + idTipoGrupo).addClass('btn btn-success btn-xs');
                            $('#btnAtivar_' + idTipoGrupo).removeClass('btn-danger');
                            $('#activ_' + idTipoGrupo).removeClass('glyphicon-ban-circle');
                            $('#activ_' + idTipoGrupo).addClass('glyphicon-ok');
                        } else {
                            $('#btnAtivar_' + idTipoGrupo).addClass('btn btn-danger btn-xs');
                            $('#btnAtivar_' + idTipoGrupo).removeClass('btn-success');
                            $('#activ_' + idTipoGrupo).addClass('glyphicon-ok');
                            $('#activ_' + idTipoGrupo).addClass('glyphicon-ban-circle');
                        }
                    });
                });

                $('.dataTables_filter label').addClass('pull-right');
            });
        </script>

    </body>
</html>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - CONTATOS TELEFÔNICOS</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <style>
            .table > thead > tr > th,
            .table > tbody > tr > th,
            .table > tfoot > tr > th,
            .table > thead > tr > td,
            .table > tbody > tr > td,
            .table > tfoot > tr > td {
                padding: 8px;
                padding-bottom: 6px;
                line-height: 1.42857143;
                vertical-align: bottom;
                border-bottom: 1px solid #ddd;
                border-top: 0px solid #ddd;
            }
            .breadcrumb2 {
                padding: 8px 15px;
                margin-bottom: 5px;
                list-style: none;
                background-color: #f5f5f5;
                border-radius: 4px;
            }
        </style>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-header">Dados do Contato</h1>
                        <ol class="breadcrumb">
                            <li><i class="fa fa-phone"></i> Contatos Telefônicos</li>
                            <li class="active"><i class="fa fa-list"></i> Dados do Contato</li>
                        </ol>
                    </div>
                </div>
                <div class="row">    
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Dados do Contato
                            </div>
                            <div class="panel-body">
                                <!-- Inicio da Tela de Contato -->
                                <table width="100%" class="table table-condensed">
                                    <tbody>
                                        <tr>
                                            <td colspan="2"><label class="control-label">Nome:</label><br>
                                                <?php if ($contato[0]['Descricao'] !== 'Sem Tratamento') { ?> <?= $contato[0]['Descricao']; ?><?php } ?>
                                                <?= $contato[0]['Nome']; ?></td>
                                            <td><label class="control-label">Data Nasc:</label><br>
                                                <?= strftime('%d/%m/%Y', strtotime($contato[0]['dtNascimento'])); ?></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" width="50%" ><label class="control-label">Cargo: </label><br> <?= $contato[0]['Cargo']; ?></td>
                                            <td width="50%" ><label class="control-label">Departamento: </label><br> <?= $contato[0]['Departamento']; ?></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" width="100%"><label class="control-label">Observação: </label><br> <?= $contato[0]['Obs']; ?></td>
                                        </tr>
                                        <tr>
                                            <td ><label class="control-label">Email: </label><br> <?= $contato[0]['EmailP']; ?></td>
                                            <td ><label class="control-label">Celular: </label><br> <?php echo Funcoes::formataTelefone($contato[0]['Celular_Principal']); ?></td>
                                            <td><label class="control-label">Tel. Comercial: </label><br> <?php echo Funcoes::formataTelefone($contato[0]['Telefone_Escritorio']); ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <!-- Fim da Tela de Contato -->

                                <!-- Inicio da Tela de Endereços -->
                                <h4>&nbsp;Endereço Principal</h4>
                                <div id="Endereco_Principal">
                                    <table width="100%" class="table table-condensed">
                                        <tbody>
                                            <tr>
                                                <td width="20%"><label class="control-label">CEP: </label> <?php echo Funcoes::formataCep($contato[0]['Endereco_Cep']); ?></td>
                                                <td colspan="4"><label class="control-label">Logradouro: </label> <?= $contato[0]['Endereco_Logradouro']; ?></td>
                                                <td width="16%"><label class="control-label">Nº: </label> <?= $contato[0]['Endereco_Numero']; ?></td>
                                            </tr>
                                            <tr>
                                                <td colspan="3"><label class="control-label">Complemento: </label> <?= $contato[0]['Endereco_Complemento']; ?></td>
                                                <td colspan="3"><label class="control-label">Bairro: </label> <?= $contato[0]['Endereco_Bairro']; ?></td>
                                            </tr>
                                            <tr>
                                                <td colspan="4"><label class="control-label">Cidade: </label> <?= $contato[0]['Endereco_Cidade']; ?></td>
                                                <td colspan="2"><label class="control-label">Estado: </label> <?= $contato[0]['Endereco_Estado']; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- Fim da Tela de Endereços -->
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Telefones e Emails Adicionais
                            </div>
                            <div class="panel-body">
                                <!-- Inicio da Tela de Emails Adicionais -->
                                <table width="100%" class="table table-striped table-bordered  table-condensed">
                                    <thead>
                                        <tr>
                                            <th width="25%" style="text-align: left">Tipo de Email</th>
                                            <th width="20%" style="text-align: left">Observação</th>
                                            <th style="text-align: left">Endereço de Email</th>
                                        </tr>    
                                    </thead>
                                    <tbody>
                                        <?php foreach ($emailscontatos as $emailscontato) { ?>
                                            <tr>
                                                <td><?= $emailscontato['Descricao']; ?></td>
                                                <td><?= $emailscontato['Observacao']; ?></td>
                                                <td><?= $emailscontato['Email']; ?></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                <!-- Fim da Tela de Emails Adicionais -->

                                <!-- Inicio da Tela de Telefones Adicionais -->
                                <table width="100%" class="table table-striped table-bordered table-condensed">
                                    <thead>
                                        <tr>
                                            <th width="25%" style="text-align: left">Tipo de Telefone</th>
                                            <th width="20%" style="text-align: left">Observação</th>
                                            <th style="text-align: left">Número de Telefone</th>
                                        </tr>    
                                    </thead>
                                    <tbody>
                                        <?php foreach ($telcontatos as $telcontato) { ?>
                                            <tr>
                                                <td><?= $telcontato['Descricao']; ?></td>
                                                <td><?= $telcontato['Observacao']; ?></td>
                                                <td><?php echo Funcoes::formataTelefone($telcontato['Numero']); ?></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                <!-- Fim da Tela de Telefones Adicionais -->
                            </div>
                            <!-- Fim da Tela de Endereços Adicionais -->
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Endereços Adicionais
                            </div>
                            <div class="panel-body">
                                <?php foreach ($enderecos as $endereco) { ?>
                                    <!-- Inicio da Tela de Endereços Adicionais -->
                                    <ol class="breadcrumb2">
                                        <li><i class="fa fa-location-arrow"></i> Local do Endereço: <strong><?= $endereco['Local_Endereco']; ?></strong></li>
                                    </ol>

                                    <table class="table table-condensed">
                                        <tbody>
                                            <tr>
                                                <td width="10%"><label class="control-label">CEP: </label> <?php echo Funcoes::formataCep($endereco['Endereco_Cep']); ?></td>
                                                <td colspan="2"><label class="control-label">Logradouro: </label> <?= $endereco['Endereco_Logradouro']; ?></td>
                                                <td width="10%"><label class="control-label">
                                                        <label class="control-label">Nº: </label>
                                                    <?= $endereco['Endereco_Numero']; ?></td>
                                                <td width="25%"><label class="control-label">Complemento: </label>
                                                    <?= $endereco['Endereco_Complemento']; ?></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2"><label class="control-label">Bairro: </label> <?= $endereco['Endereco_Bairro']; ?></td>
                                                <td colspan="2"><label class="control-label">Cidade: </label>
                                                    <?= $endereco['Endereco_Cidade']; ?></td>
                                                <td><label class="control-label">Estado: </label>
                                                    <?= $endereco['Endereco_Estado']; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>             
                                <?php } ?>
                            </div>
                            <!-- Fim da Tela de Endereços Adicionais -->
                        </div>
                    </div>
                    <div class="col-md-12">
                        <a href="home" class="btn btn-danger">Voltar</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>

<script>
    jQuery(document).ready(function () {
        // Remove seleção de ativo no menu.
        $('.nav li').removeClass('active');
        $('#side-menu li').removeClass('active');
        // Ativa botão no menu.
        $('#li-contatos ul').addClass('collapse in');
        $('#li-cad-tipos-contato a').addClass('active');
        $('#li-contatos').addClass('active');
        $('#li-contatos a').addClass('collapse in');
    });
</script>
</body>
</html>
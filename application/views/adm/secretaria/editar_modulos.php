<?php if ($_SESSION['AcBtnEditMod'] == true) { ?>
    <form class="form-horizontal" role="form" method="POST" action="update_modulo" name="modulos" id="modulos">
        <div class="form-group">
            <div class="col-lg-12">
                <div class="form-group">
                    <label for="nome" class="col-lg-3 control-label">Nome</label>
                    <div class="col-lg-9">
                        <input type="hidden" id="page" name="page" value="<?= $page; ?>">
                        <input type="hidden" id="idModulo" name="idModulo" placeholder="" value="<?= $modulos[0]['idModulo']; ?>">  
                        <input type="text" min="0" class="form-control" id="Nome" name="Nome" placeholder="" value="<?= $modulos[0]['Nome']; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="alias" class="col-lg-3 control-label">Sigla do módulo</label>
                    <div class="col-lg-9">
                        <input type="text" class="form-control" id="sigla_modulorelato" name="Alias" placeholder="" value="<?= $modulos[0]['Alias']; ?>">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-offset-4 col-lg-8">
                        <div class="form-group"> 
                            <label class="radio-inline">&nbsp;&nbsp;&nbsp;
                                <input required type="radio" name="status" id="inativo" value="1" <?= ($modulos[0]['Status'] == '1') ? 'checked' : ''; ?>>
                                Ativo
                            </label>
                            <label class="radio-inline">
                                <input required type="radio" name="status" id="inativo" value="0" <?= ($modulos[0]['Status'] == '0') ? 'checked' : ''; ?>>
                                Inativo
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>    
    </form>   
<?php } else { ?>
    <h1 class="page-header"> Voce não possue acesso a está pagina, consulte o administrador.</h1>
<?php } ?>
<script>
    $(document).ready(function () {

        var d = new Date();
        var hora = d.getHours();
        if (hora < 7) {
            hora = 7;
        }
        var min = d.getMinutes();
        if (min < 10) {
            min = '0' + min;
        }
        var dataAtual, dia, mes, ano;
        dia = d.getDate();
        mes = d.getMonth() + 1;
        ano = d.getFullYear();

        if (mes < 10) {
            mes = "0" + mes;
        }
        if (dia < 10) {
            dia = "0" + dia;
        }

        dataAtual = ano + "-" + mes + "-" + dia;

        var horaAtual = hora + ":" + min;

        $('#dtAssinatura').datetimepicker({
            locale: 'pt-br',
            format: "D/M/YYYY"
        });

        $('#horaAssinatura').datetimepicker({
            format: 'LT',
            disabledHours: [0, 1, 2, 3, 4, 5, 6, 24],
            enabledHours: [8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23],
            locale: 'pt-br'
        });

        var dtAssinatura = moment(<?= "'" . $Contrato[0]['dtAssinatura'] . "'"; ?>).format('DD/MM/YYYY');
        var HoraAssinatura = moment(<?= "'" . $Contrato[0]['dtAssinatura'] . "'"; ?>).format('HH:mm');

<?php if ($Contrato[0]['dtAssinatura'] != "") { ?>
            $('#dtAssinatura').val(dtAssinatura);
            $('#horaAssinatura').val(HoraAssinatura);
<?php } ?>
    });
</script>
<style>
    .form-control[disabled], .form-control[disabled], fieldset[disabled] .form-control {
        background-color: #fdfdfd;
        opacity: 1;
    }
</style>
<?php if ($_SESSION['AcVisualizaCont'] == true) { ?>
    <form class="form-horizontal">
        <div class="col-lg-12">
            <div class="form-group">
                <label for="tipo" class="col-lg-4">Numero da Contrato</label>
                <label for="tipo" class="col-lg-4">Assunto</label>
                <label class="col-lg-4">Número de Registro</label>

                <div class="col-lg-4">
                    <input type="text" class="form-control" id="Contrato" name="Contrato" placeholder="Informe o número Contrato" value="<?= $Contrato[0]['Contrato'] ?>" disabled>
                </div>

                <div class="col-lg-4">
                    <input type="text" class="form-control" id="Assunto" name="Assunto" placeholder="Informe o Assunto" value="<?= $Contrato[0]['Assunto'] ?>" disabled>
                </div>

                <div class="col-lg-4">
                    <input type="text" class="form-control" id="NRegistro" name="NRegistro" placeholder="Informe o número de registro da contrato, se houver" value="<?= $Contrato[0]['NRegistro'] ?>" disabled>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-4">Setor/Superintendêcias</label>
                <label class="col-lg-3">Tipo de Contrato</label>
                <label class="col-lg-2">Data da Assinatura</label>
                <label class="col-lg-3">Hora</label>
                <div class="col-lg-4">
                    <select class="form-control" id='idSetor' name='idSetor' disabled>
                        <option value="" selected='selected'>Selecione uma Opção</option>
                        <?php foreach ($Setor as $Setores) { ?>
                            <option <?= ($Contrato[0]['idSetor'] == $Setores['idTipoSetor']) ? 'selected' : ''; ?> value="<?= $Setores['idTipoSetor']; ?>"><?= $Setores['Setor']; ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-lg-3">
                    <input type="text" class="form-control" id="idTipoDoc" name="idTipoDoc" value="<?= $Contrato[0]['Nome']; ?>" disabled>
                </div>
                <div class="col-lg-2">
                    <input type="text" class="form-control" id="dtAssinatura" name="dtAssinatura" disabled>
                </div>
                <div class="col-lg-3">
                    <input type="text" class="form-control" id="horaAssinatura" name="horaAssinatura" disabled>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-12">Observação</label>

                <div class="col-lg-12">
                    <textarea class='form-control' rows="2" id="Obs" name="Obs" placeholder="Informações adicionais" disabled><?= $Contrato[0]['Obs'] ?></textarea>
                </div>                                                    
            </div>
            <?php if ($Anexo[0]['idAnexo'] != '') { ?>
                <div class="form-group">
                    <div class="col-lg-12">
                        <ul class="mailbox-attachments clearfix">
                            <?php foreach ($Anexo as $Anexos) { ?>  
                                <li id="Anexo_<?= $Anexos['idAnexo'] ?>">
                                    <span class="mailbox-attachment-icon">
                                        <?php if (substr($Anexos['Caminho'], '-3') == "jpg") { ?> 
                                            <i class="fa fa-file-image-o"></i>
                                        <?php } else if (substr($Anexos['Caminho'], '-3') == "png") { ?>    
                                            <i class="fa fa-photo"></i>
                                        <?php } else if (substr($Anexos['Caminho'], '-3') == "doc") { ?>    
                                            <i class="fa fa-file-word-o"></i>
                                        <?php } else if (substr($Anexos['Caminho'], '-3') == "pdf") { ?>    
                                            <i class="fa fa-file-pdf-o"></i>  
                                        <?php } else { ?>
                                            <i class="fa fa-file-o"></i>
                                        <?php } ?>  
                                    </span>
                                    <div class="mailbox-attachment-info">
                                        <a href="<?= URL::base(); ?>upload/sep/contrato/<?= $Anexos['Caminho'] ?>" class="mailbox-attachment-name" download><i class="fa fa-paperclip"></i> <?= substr($Anexos['Caminho'], '17') ?></a>
                                    </div>
                                </li>
                            <?php } ?>                                                            
                        </ul>
                    </div>
                </div>
            <?php } ?>
            <div class="col-lg-offset-1 col-lg-11">
                <div class="form-group"> 
                    <label class="radio-inline">
                        <input disabled type="radio" name="Status" id="Status" value="1" <?php if ($Contrato[0]['Status'] == '1') { ?> checked <?php } ?>>
                        Ativo
                    </label>
                    <label class="radio-inline">
                        <input disabled type="radio" name="Status" id="Status" value="0" <?php if ($Contrato[0]['Status'] == '0') { ?> checked <?php } ?>>
                        Inativo
                    </label>
                </div>    
            </div>
        </div>
    </form>
<?php } else { ?>
    <h1 class="page-header"> Voce não possue acesso a está pagina, consulte o administrador.</h1>
<?php } ?>
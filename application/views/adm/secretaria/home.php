<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - Sistema de Apoio a Secretaria</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>

            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Controle de Protocolos</h1>
                        <ol class="breadcrumb">
                            <li><i class="fa fa-folder-open"></i> Secretaria</li>
                            <li class="active"><i class="fa fa-list"></i> Listagem</li>
                        </ol>

                        <?php if (isset($warning)) : ?>
                            <div id="msg" class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong><?= $warning; ?></strong>
                            </div>
                        <?php endif ?>  
                        <div class="row">
                            <div class="col-lg-4">
                                <form class="form-inline" method="get" action="home">
                                    <input  type="text" class="form-control input-sm" id="Busca" name="Busca" value="<?= $Pesquisa; ?>" placeholder="Informe o Assunto" style="width: 280px;">
                                    <button type="submit" class="btn btn-danger btn-sm"><i class="glyphicon glyphicon-search"></i> Buscar</button>
                                    <a href="home" type="reset" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-erase"></i> Limpar</a>
                                </form>
                            </div>
                            <div class="col-lg-4">
                                
                            </div>
                            <?php if ($_SESSION['AcLiberaBtnAddProt'] == TRUE) { ?>
                                <div class="col-lg-4">
                                    <button type="submit" id="btn_novo_protocolo" class="pull-right btn btn-primary btn-sm" name="btn_novo_protocolo"><i class="glyphicon glyphicon-plus-sign">&nbsp;</i>Protocolar Documento</button>
                                </div>
                            <?php } ?>
                        </div>                        
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="protocolos">
                                <thead>
                                    <tr>
                                        <th width="5%" style="text-align: center">ID</th>
                                        <th>Assunto</th>
                                        <th width="8%" style="text-align: center">Entrada</th>
                                        <th width="13%">Entregue por</th>
                                        <th width="15%">Departamento</th>
                                        <th width="8%" style="text-align: center">Saída</th>
                                        <th width="13%">Retirado por</th>
                                        <th width="8%" style="text-align: center">Ações</th>
                                        <th width="5%" style="text-align: center">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($data as $protocolo) {
                                        ?>
                                        <tr>
                                            <td align="center"><?= $protocolo['idProtocolo']; ?></td>
                                            <td><?= $protocolo['Assunto']; ?></td>
                                            <td align="center"><?= strftime('%d/%m/%Y', strtotime($protocolo['dtEntrada'])); ?></td>
                                            <td><?= $protocolo['Entregue']; ?></td>
                                            <td><?= $protocolo['Setor']; ?></td>
                                            <td align="center"><?= strftime('%d/%m/%Y', strtotime($protocolo['dtSaida'])); ?></td>
                                            <td><?= $protocolo['Retirada']; ?></td>
                                            <td align="center">
                                                <button  type="submit" id="btnVisualizar" class="btn btn-default btn-xs" name="btnVisualizar"  page="<?= $PaginaAtual; ?>" idProtocolo="<?= $protocolo['idProtocolo']; ?>" title="Visualizar Protocolo" <?php if ($_SESSION['AcVisualizaProto'] == NULL) { ?>disabled<?php } ?>>
                                                    <i class="fa fa-eye"></i>
                                                </button>
                                                
                                                <button  type="submit" id="btnEditarProt" class="btn btn-default btn-xs" name="btnEditarProt" page="<?= $PaginaAtual; ?>" idProtocolo="<?= $protocolo['idProtocolo']; ?>" title="Editar Protocolo" <?php if ($_SESSION['AcBtnEditProto'] == NULL) { ?>disabled<?php } ?>>
                                                    <i class="glyphicon glyphicon-edit"></i>
                                                </button>
                                                
                                                <input type="hidden" id="idProtocolo<?= $protocolo['idProtocolo']; ?>" name="idProtocolo" value="<?= $protocolo['idProtocolo']; ?>">
                                                <button  type="button" id="btnExcluir" class="btn btn-default btn-xs" name="btnExcluir" idProtocolo="<?= $protocolo['idProtocolo']; ?>" nome="<?= $protocolo['Assunto']; ?>" <?php if ($_SESSION['AcBtnExcProto'] == NULL) { ?>disabled<?php } ?> data-toggle="modal" data-target="#modal_excluir" data-placement="bottom" title="Excluir Protocolo" data-delay="1">
                                                    <i class="glyphicon glyphicon-trash"></i>
                                                </button>
                                            </td>
                                            <td align="center">
                                                <?php
                                                if ($protocolo['Status'] == "0") {
                                                    ?>
                                                    <button type="submit" id="btnAtivar_<?= $protocolo['idProtocolo']; ?>" class="btn btn-danger btn-xs" idProtocolo="<?= $protocolo['idProtocolo']; ?>" name="btnAtivar" title="Ativar Protocolo" <?php if ($_SESSION['AcBtnAtivProto'] == NULL) { ?>disabled<?php } ?>>
                                                        <i id="activ_<?= $protocolo['idProtocolo']; ?>" class="glyphicon glyphicon-ban-circle"></i>
                                                    </button>
                                                <?php } else { ?>
                                                    <button type="submit" id="btnAtivar_<?= $protocolo['idProtocolo']; ?>" class="btn btn-success btn-xs" idProtocolo="<?= $protocolo['idProtocolo']; ?>" name="btnAtivar" title="Destivar Protocolo" <?php if ($_SESSION['AcBtnAtivProto'] == NULL) { ?>disabled<?php } ?>>
                                                        <i id="activ_<?= $protocolo['idProtocolo']; ?>" class="glyphicon glyphicon-ok"></i>
                                                    </button>
                                                    <?php
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>    
                            </table>
                            <?php include(kohana::find_file('views/paginator', 'paginacao', 'php')) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="modal_excluir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Confirmar Exclusão</h4>
                    </div>
                    <div class="modal-body" id="modal_delete_protocolo">Deseja excluir este protocolo?</div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        <button type="button" class="btn btn-primary" id="modal_btn-excluir">Excluir</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal -->
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>

        <script>
            jQuery(document).ready(function () {
                // Remove seleção de ativo no menu.
                $('.nav li').removeClass('active');
                $('#side-menu li').removeClass('active');
                // Ativa botão no menu.
                $('#li-gestaoP ul').addClass('collapse in');
                $('#li-protocolo a').addClass('active');
                $('#li-protocolo').addClass('active');
                $('#li-protocolo a').addClass('collapse in');
                
                // Datatables
                $('#protocolos').dataTable(
                {
                    "aoColumnDefs": [{"bSortable": false, "aTargets": [2,3,4,5,6,7,8]}],
                    "paging": false,
                    "info": false,
                    "aaSorting": [[0, 'desc']],
                    "bFilter": false,
                    "oLanguage":
                            {
                                "sSearch": " Buscar: "
                            }
                });
                
                // abre o modal para cadastro
                $('button[name=btn_novo_protocolo]').click(function () {
                    var idProtocolo = '';
                    var options = {
                        url: "protocolo?idProtocolo=" + idProtocolo,
                        title: 'Cadastrar Protocolo',
                        size: eModal.size.lg,
                        buttons: [
                            {text: 'GRAVAR', style: 'primary', close: false, type: 'submit', form: 'novoprotocolo'},
                            {text: 'FECHAR', style: 'danger', close: true}
                        ]
                    };
                    eModal.ajax(options);
                });
                
                //abre o modal de edição
                $('button[name=btnEditarProt]').click(function () {
                    var idProtocolo = $(this).attr('idProtocolo');
                    var page = $(this).attr('page');
                    var options = {
                        url: "protocolo?idProtocolo=" + idProtocolo +"&page="+ page,
                        title: 'Editar Protocolo',
                        size: eModal.size.lg,
                        buttons: [
                            {text: 'SALVAR', style: 'primary', close: false, type: 'submit', form: 'novoprotocolo'},
                            {text: 'FECHAR', style: 'danger', close: true}
                        ]
                    };
                    eModal.ajax(options);
                });
                
                // Abre o maodal de visualização
                $('button[name=btnVisualizar]').click(function () {
                    var idProtocolo = $(this).attr('idProtocolo');
                    var page = $(this).attr('page');
                    var options = {
                        url: "visualizar_protocolo?idProtocolo=" + idProtocolo +"&page="+ page,
                        title: 'Visualizar Protocolo',
                        size: eModal.size.lg,
                        buttons: [
                            {text: 'FECHAR', style: 'danger', close: true}
                        ]
                    };
                    eModal.ajax(options);
                });
                
                
                // Opção usada na exclusão
                var idProtocolo;
                var nome;
                $('button[name=btnExcluir]').on("click", function ()
                {
                    idProtocolo = $(this).attr('idProtocolo');
                    nome = $(this).attr('nome');
                    $('#modal_delete_protocolo').html("Deseja excluir este Protocolo <b>\"" + nome + "\"</b> ?");
                });

                $('#modal_btn-excluir').on("click", function ()
                {
                    var btn = $(this);
                    btn.text('loading');

                    btn.removeClass('btn-primary');
                    btn.removeClass('btn-danger');
                    btn.removeClass('btn-success');
                    btn.addClass('btn-primary');

                    $('#modal_btn-excluir').prop('disabled', true);

                    $.ajax("delete_protocolo",
                            {
                                type: "POST",
                                data:
                                        {
                                            idProtocolo: idProtocolo
                                        }
                            })
                            .done(function (data)
                            {
                                $('#modal_delete_protocolo').html('Protocolo excluído com sucesso');

                                btn.removeClass('btn-primary');
                                btn.addClass('btn-success');
                            })
                            .fail(function ()
                            {
                                $('#modal_delete_protocolo').html('Erro: ' + data);

                                btn.removeClass('btn-primary');
                                btn.addClass('btn-danger');
                            })
                            .always(function ()
                            {
                                btn.text('Excluído');
                            });
                });
                
                $('#modal_excluir').on('hidden.bs.modal', function ()
                {
                    window.location.replace("home");
                });
                
                // Opção para ativar e desativar
                $("body").on("click", "button[name=btnAtivar]", function () {
                    var idProtocolo = $(this).attr('idProtocolo');
                    //alert(idProtocolo);
                    $.get('update_ativar_protocolo?idProtocolo=' + idProtocolo, function (data) {
                        //alert(data);
                        if (data === "1") {
                            $('#btnAtivar_' + idProtocolo).addClass('btn btn-success btn-xs');
                            $('#btnAtivar_' + idProtocolo).removeClass('btn-danger');
                            $('#activ_' + idProtocolo).removeClass('glyphicon-ban-circle');
                            $('#activ_' + idProtocolo).addClass('glyphicon-ok');
                        } else {
                            $('#btnAtivar_' + idProtocolo).addClass('btn btn-danger btn-xs');
                            $('#btnAtivar_' + idProtocolo).removeClass('btn-success');
                            $('#activ_' + idProtocolo).addClass('glyphicon-ok');
                            $('#activ_' + idProtocolo).addClass('glyphicon-ban-circle');
                        }
                    });
                });

                $('.dataTables_filter label').addClass('pull-right');
            });
        </script>
    </body>
</html>
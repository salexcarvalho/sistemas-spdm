<script>
    $(document).ready(function () {

        var d = new Date();
        var hora = d.getHours();
        if (hora < 7) {
            hora = 7;
        }
        var min = d.getMinutes();
        if (min < 10) {
            min = '0' + min;
        }
        var dataAtual, dia, mes, ano;
        dia = d.getDate();
        mes = d.getMonth() + 1;
        ano = d.getFullYear();

        if (mes < 10) {
            mes = "0" + mes;
        }
        if (dia < 10) {
            dia = "0" + dia;
        }

        dataAtual = ano + "-" + mes + "-" + dia;

        var horaAtual = hora + ":" + min;

        $('#dtEntrada').datetimepicker({
            locale: 'pt-br',
            format: "D/M/YYYY"
        });

        $('#dtSaida').datetimepicker({
            locale: 'pt-br',
            format: "D/M/YYYY"
        });

        $('#horaEntrada').datetimepicker({
            format: 'LT',
            disabledHours: [0, 1, 2, 3, 4, 5, 6, 24],
            enabledHours: [8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23],
            locale: 'pt-br'
        });

        $('#horaSaida').datetimepicker({
            format: 'LT',
            disabledHours: [0, 1, 2, 3, 4, 5, 6, 24],
            enabledHours: [8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23],
            locale: 'pt-br'
        });

        var dtEntrada = moment(<?= "'" . $Protocolo[0]['dtEntrada'] . "'"; ?>).format('DD/MM/YYYY');
        var dtSaida = moment(<?= "'" . $Protocolo[0]['dtSaida'] . "'"; ?>).format('DD/MM/YYYY');
        var HoraEntrada = moment(<?= "'" . $Protocolo[0]['dtEntrada'] . "'"; ?>).format('HH:mm');
        var HoraSaida = moment(<?= "'" . $Protocolo[0]['dtSaida'] . "'"; ?>).format('HH:mm');

<?php if ($Protocolo[0]['dtEntrada'] != "") { ?>
            $('#dtEntrada').val(dtEntrada);
            $('#dtSaida').val(dtSaida);
            $('#horaEntrada').val(HoraEntrada);
            $('#horaSaida').val(HoraSaida);
<?php } ?>
    });
</script>
<?php if ($_SESSION['AcLiberaBtnAddProt'] == true) { ?>
    <?php if ($idProtocolo == '') : ?>
        <form name='novoprotocolo' id='novoprotocolo' class='form-horizontal' method="POST" action="insert_protocolo" enctype="multipart/form-data">
        <?php else : ?>
            <form name='novoprotocolo' id='novoprotocolo' class="form-horizontal" method="POST" action="update_protocolo" enctype="multipart/form-data">
            <?php endif; ?>
            <input type="hidden" id="idProtocolo" name="idProtocolo" value="<?= $Protocolo[0]['idProtocolo'] ?>">
            <input type="hidden" id="page" name="page" value="<?= $page ?>">
            <div class="col-lg-12">
                <div class="form-group">
                    <label for="tipo" class="col-lg-4">Protocolo</label>
                    <label for="tipo" class="col-lg-4">Assunto</label>
                    <label class="col-lg-4">Número de Procuração</label>

                    <div class="col-lg-4">
                        <input type="text" class="form-control" id="Protocolo" name="Protocolo" placeholder="Informe o Protocolo" value="<?= $Protocolo[0]['Protocolo'] ?>" required>
                    </div>

                    <div class="col-lg-4">
                        <input type="text" class="form-control" id="Assunto" name="Assunto" placeholder="Informe o Assunto" value="<?= $Protocolo[0]['Assunto'] ?>" required>
                    </div>

                    <div class="col-lg-4">
                        <input type="text" class="form-control" id="NProcuracao" name="NProcuracao" placeholder="Informe o número de protocolo, se houver" value="<?= $Protocolo[0]['NProcuracao'] ?>" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-6">Setor</label>
                    <label class="col-lg-6">Tipo de Documento</label>

                    <div class="col-lg-6">
                        <select class="form-control" id='idSetor' name='idSetor' required>
                            <option value="" selected='selected'>Selecione uma Opção</option>
                            <?php foreach ($Setor as $Setores) { ?>
                                <option <?= ($Protocolo[0]['idSetor'] == $Setores['idTipoSetor']) ? 'selected' : ''; ?> value="<?= $Setores['idTipoSetor']; ?>"><?= $Setores['Setor']; ?></option>
                            <?php } ?>
                        </select>
                    </div>

                    <div class="col-lg-6">
                        <select class="form-control" id='idTipoDoc' name='idTipoDoc' required>
                            <option value="" selected='selected'>Selecione uma Opção</option>
                            <?php foreach ($TipoDocumento as $TipoDocumentos) { ?>
                                <option <?= ($Protocolo[0]['idTipoDoc'] == $TipoDocumentos['idTipoDoc']) ? 'selected' : ''; ?> value="<?= $TipoDocumentos['idTipoDoc']; ?>"><?= $TipoDocumentos['Nome']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-8">Entregue por</label>
                    <label class="col-lg-2">Data da Entrada</label>
                    <label class="col-lg-2">Hora</label>

                    <div class="col-lg-8">
                        <input type="text" class="form-control" id="Entregue" name="Entregue" placeholder="Informe o nome de quem entregou" value="<?= $Protocolo[0]['Entregue'] ?>" required>
                    </div>

                    <div class="col-lg-2">
                        <input type="text" class="form-control" id="dtEntrada" name="dtEntrada" required>
                    </div>

                    <div class="col-lg-2">
                        <input type="text" class="form-control" id="horaEntrada" name="horaEntrada" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-8">Retirado por</label>
                    <label class="col-lg-2">Data da Saída</label>
                    <label class="col-lg-2">Hora</label>

                    <div class="col-lg-8">
                        <input type="text" class="form-control" id="Retirada" name="Retirada" placeholder="Informe o nome de quem retirou" value="<?= $Protocolo[0]['Retirada'] ?>" required>
                    </div>

                    <div class="col-lg-2">
                        <input type="text" class="form-control" id="dtSaida" name="dtSaida">
                    </div>

                    <div class="col-lg-2">
                        <input type="text" class="form-control" id="horaSaida" name="horaSaida">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-12">Observação</label>

                    <div class="col-lg-12">
                        <textarea class='form-control' rows="2" id="Obs" name="Obs" placeholder="Informações adicionais"><?= $Protocolo[0]['Obs'] ?></textarea>
                    </div>                                                    
                </div>
                <div class="form-group" >
                    <label class="col-lg-4">O tamanho máximo dos arquivos é de 30 MB.</label>
                    <div class="col-lg-4">
                        <input type="file" class="btn btn-default" id="anexo" name="anexo[]" multiple="" onchange="makeFileList();">
                    </div>
                    <div class="col-lg-4">
                        <ul id="fileList" style="margin-top: 10px; margin-bottom: 10px;"></ul>
                    </div>    
                </div>
                <?php if ($Anexo[0]['idAnexo'] != '') { ?>
                    <div class="form-group">
                        <div class="col-lg-12">
                            <ul class="mailbox-attachments clearfix">
                                <?php foreach ($Anexo as $Anexos) { ?>  
                                    <li id="Anexo_<?= $Anexos['idAnexo'] ?>">
                                        <span class="mailbox-attachment-icon">
                                            <?php if (substr($Anexos['Caminho'], '-3') == "jpg") { ?> 
                                                <i class="fa fa-file-image-o"></i>
                                            <?php } else if (substr($Anexos['Caminho'], '-3') == "png") { ?>    
                                                <i class="fa fa-photo"></i>
                                            <?php } else if (substr($Anexos['Caminho'], '-3') == "doc") { ?>    
                                                <i class="fa fa-file-word-o"></i>
                                            <?php } else if (substr($Anexos['Caminho'], '-3') == "pdf") { ?>    
                                                <i class="fa fa-file-pdf-o"></i>  
                                            <?php } else { ?>
                                                <i class="fa fa-file-o"></i>
                                            <?php } ?>  
                                        </span>
                                        <div class="mailbox-attachment-info">
                                            <a href="<?= URL::base(); ?>upload/sep/protocolo/<?= $Anexos['Caminho'] ?>" class="mailbox-attachment-name" download><i class="fa fa-paperclip"></i> <?= substr($Anexos['Caminho'], '17') ?></a>
                                            <button type="button" id="excluir_anexo" name="excluir_anexo" idAnexo="<?= $Anexos['idAnexo'] ?>" idProtocolo="<?= $Anexos['idProtocolo'] ?>" nome="<?= $Anexos['Caminho'] ?>" class="btn btn-default btn-xs pull-right" title="Excluir Anexo"><i class="fa fa-trash"></i></button>
                                        </div>
                                    </li>
                                <?php } ?>                                                            
                            </ul>
                        </div>
                    </div>
                <?php } ?>
                <div class="col-lg-offset-1 col-lg-11">
                    <div class="form-group"> 
                        <label class="radio-inline">
                            <input required type="radio" name="Status" id="Status" value="1" <?php if ($Protocolo[0]['Status'] == '1') { ?> checked <?php } ?>>
                            Ativo
                        </label>
                        <label class="radio-inline">
                            <input required type="radio" name="Status" id="Status" value="0" <?php if ($Protocolo[0]['Status'] == '0') { ?> checked <?php } ?>>
                            Inativo
                        </label>
                    </div>
                </div>
            </div>
        </form>
    <?php } else { ?>
        <h1 class="page-header"> Voce não possue acesso a está pagina, consulte o administrador.</h1>
    <?php } ?>
    <script>
        $(document).ready(function () {
            var idAnexo;
            var idProtocolo;
            var nome;

            $('button[name=excluir_anexo]').on("click", function ()
            {
                idAnexo = $(this).attr('idAnexo');
                idProtocolo = $(this).attr('idProtocolo');
                var btn = $(this);
                btn.text('Excluindo');

                $.ajax("delete_anexo",
                        {
                            type: "GET",
                            data:
                                    {
                                        idAnexo: idAnexo,
                                        idProtocolo: idProtocolo
                                    }
                        })
                        .done(function (data)
                        {
                            var message = 'Seu anexo foi excluído com sucesso';
                            var options = {
                                message: message,
                                title: 'Anexo Excluído',
                                size: 'md',
                                useBin: true
                            };
                            eModal.alert(options);
                        })
                        .fail(function ()
                        {
                            var message = 'Erro: ' + data;
                            var options = {
                                message: message,
                                title: 'Erro de  Exclusão',
                                size: 'md',
                                useBin: true
                            };
                            eModal.alert(options);
                        })
                        .always(function ()
                        {
                            $('#Anexo_' + idAnexo).hide();
                        });
            });
        });

        function makeFileList() {
            var input = document.getElementById("anexo");
            var ul = document.getElementById("fileList");
            while (ul.hasChildNodes()) {
                ul.removeChild(ul.firstChild);
            }
            for (var i = 0; i < input.files.length; i++) {
                var li = document.createElement("li");
                li.innerHTML = input.files[i].name;
                ul.appendChild(li);
            }
            if (!ul.hasChildNodes()) {
                var li = document.createElement("li");
                li.innerHTML = 'Nenhum arquivo selecionado';
                ul.appendChild(li);
            }
        }
    </script>
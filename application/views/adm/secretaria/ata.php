<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - Sistema de Apoio a Secretaria</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>

            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Controle de Atas</h1>
                        <ol class="breadcrumb">
                            <li><i class="fa fa-folder-open"></i> Secretaria</li>
                            <li class="active"><i class="fa fa-list"></i> Listagem</li>
                        </ol>

                        <?php if (isset($warning)) : ?>
                            <div id="msg" class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong><?= $warning; ?></strong>
                            </div>
                        <?php endif ?>  
                        <div class="row">
                            <div class="col-lg-4">
                                <form class="form-inline" method="get" action="home">
                                    <input  type="text" class="form-control input-sm" id="Busca" name="Busca" value="<?= $Pesquisa; ?>" placeholder="Informe o Assunto" style="width: 280px;">
                                    <button type="submit" class="btn btn-danger btn-sm"><i class="glyphicon glyphicon-search"></i> Buscar</button>
                                    <a href="home" type="reset" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-erase"></i> Limpar</a>
                                </form>
                            </div>
                            <div class="col-lg-4">
                                
                            </div>
                            <?php if ($_SESSION['AcLiberaBtnAddAta'] == TRUE) { ?>
                                <div class="col-lg-4">
                                    <button type="submit" id="btn_novo_ata" class="pull-right btn btn-primary btn-sm" name="btn_novo_ata"><i class="glyphicon glyphicon-plus-sign"></i> Cadastrar ATA</button>
                                </div>
                            <?php } ?>
                        </div>                        
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="atas">
                                <thead>
                                    <tr>
                                        <th width="5%" style="text-align: center">ID</th>
                                        <th>Assunto</th>
                                        <th width="12%" style="text-align: center">Data Reunião</th>
                                        <th width="18%">Nº Registro</th>
                                        <th width="12%">Tipo de Ata</th>
                                        <th width="8%" style="text-align: center">Ações</th>
                                        <th width="5%" style="text-align: center">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($data as $ata) {
                                        ?>
                                        <tr>
                                            <td align="center"><?= $ata['idAta']; ?></td>
                                            <td><?= $ata['Assunto']; ?></td>
                                            <td align="center"><?= strftime('%d/%m/%Y', strtotime($ata['dtReuniao'])); ?></td>
                                            <td><?= $ata['NRegistro']; ?></td>
                                            <td><?= $ata['Nome']; ?></td>
                                            <td align="center">
                                                <button  type="submit" id="btnVisualizar" class="btn btn-default btn-xs" name="btnVisualizar"  page="<?= $PaginaAtual; ?>" idAta="<?= $ata['idAta']; ?>" title="Visualizar Ata" <?php if ($_SESSION['AcVisualizaAta'] == NULL) { ?>disabled<?php } ?>>
                                                    <i class="fa fa-eye"></i>
                                                </button>
                                                
                                                <button  type="submit" id="btnEditarAta" class="btn btn-default btn-xs" name="btnEditarAta" page="<?= $PaginaAtual; ?>" idAta="<?= $ata['idAta']; ?>" title="Editar Ata" <?php if ($_SESSION['AcBtnEditAta'] == NULL) { ?>disabled<?php } ?>>
                                                    <i class="glyphicon glyphicon-edit"></i>
                                                </button>
                                                
                                                <input type="hidden" id="idAta<?= $ata['idAta']; ?>" name="idAta" value="<?= $ata['idAta']; ?>">
                                                <button  type="button" id="btnExcluir" class="btn btn-default btn-xs" name="btnExcluir" idAta="<?= $ata['idAta']; ?>" nome="<?= $ata['Assunto']; ?>" <?php if ($_SESSION['AcBtnExcAta'] == NULL) { ?>disabled<?php } ?> data-toggle="modal" data-target="#modal_excluir" data-placement="bottom" title="Excluir Ata" data-delay="1">
                                                    <i class="glyphicon glyphicon-trash"></i>
                                                </button>
                                            </td>
                                            <td align="center">
                                                <?php
                                                if ($ata['Status'] == "0") {
                                                    ?>
                                                    <button type="submit" id="btnAtivar_<?= $ata['idAta']; ?>" class="btn btn-danger btn-xs" idAta="<?= $ata['idAta']; ?>" name="btnAtivar" title="Ativar Ata" <?php if ($_SESSION['AcBtnAtivAta'] == NULL) { ?>disabled<?php } ?>>
                                                        <i id="activ_<?= $ata['idAta']; ?>" class="glyphicon glyphicon-ban-circle"></i>
                                                    </button>
                                                <?php } else { ?>
                                                    <button type="submit" id="btnAtivar_<?= $ata['idAta']; ?>" class="btn btn-success btn-xs" idAta="<?= $ata['idAta']; ?>" name="btnAtivar" title="Destivar Ata" <?php if ($_SESSION['AcBtnAtivAta'] == NULL) { ?>disabled<?php } ?>>
                                                        <i id="activ_<?= $ata['idAta']; ?>" class="glyphicon glyphicon-ok"></i>
                                                    </button>
                                                    <?php
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>    
                            </table>
                            <?php include(kohana::find_file('views/paginator', 'paginacao', 'php')) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="modal_excluir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Confirmar Exclusão</h4>
                    </div>
                    <div class="modal-body" id="modal_delete_ata">Deseja excluir este ata?</div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        <button type="button" class="btn btn-primary" id="modal_btn-excluir">Excluir</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal -->
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>

        <script>
            jQuery(document).ready(function () {
                // Remove seleção de ativo no menu.
                $('.nav li').removeClass('active');
                $('#side-menu li').removeClass('active');
                // Ativa botão no menu.
                $('#li-gestaoP ul').addClass('collapse in');
                $('#li-ata a').addClass('active');
                $('#li-ata').addClass('active');
                $('#li-ata a').addClass('collapse in');
                
                // Datatables
                $('#atas').dataTable(
                {
                    "aoColumnDefs": [{"bSortable": false, "aTargets": [3,4,5]}],
                    "paging": false,
                    "info": false,
                    "aaSorting": [[0, 'desc']],
                    "bFilter": false,
                    "oLanguage":
                            {
                                "sSearch": " Buscar: "
                            }
                });
                
                // abre o modal para cadastro
                $('button[name=btn_novo_ata]').click(function () {
                    var idAta = '';
                    var options = {
                        url: "ata?idAta=" + idAta,
                        title: 'Cadastrar Ata',
                        size: eModal.size.lg,
                        buttons: [
                            {text: 'GRAVAR', style: 'primary', close: false, type: 'submit', form: 'novaata'},
                            {text: 'FECHAR', style: 'danger', close: true}
                        ]
                    };
                    eModal.ajax(options);
                });
                
                //abre o modal de edição
                $('button[name=btnEditarAta]').click(function () {
                    var idAta = $(this).attr('idAta');
                    var page = $(this).attr('page');
                    var options = {
                        url: "ata?idAta=" + idAta +"&page="+ page,
                        title: 'Editar Ata',
                        size: eModal.size.lg,
                        buttons: [
                            {text: 'SALVAR', style: 'primary', close: false, type: 'submit', form: 'novaata'},
                            {text: 'FECHAR', style: 'danger', close: true}
                        ]
                    };
                    eModal.ajax(options);
                });
                
                // Abre o maodal de visualização
                $('button[name=btnVisualizar]').click(function () {
                    var idAta = $(this).attr('idAta');
                    var page = $(this).attr('page');
                    var options = {
                        url: "visualizar_ata?idAta=" + idAta +"&page="+ page,
                        title: 'Visualizar Ata',
                        size: eModal.size.lg,
                        buttons: [
                            {text: 'FECHAR', style: 'danger', close: true}
                        ]
                    };
                    eModal.ajax(options);
                });
                
                
                // Opção usada na exclusão
                var idAta;
                var nome;
                $('button[name=btnExcluir]').on("click", function ()
                {
                    idAta = $(this).attr('idAta');
                    nome = $(this).attr('nome');
                    $('#modal_delete_ata').html("Deseja excluir esta Ata <b>\"" + nome + "\"</b> ?");
                });

                $('#modal_btn-excluir').on("click", function ()
                {
                    var btn = $(this);
                    btn.text('loading');

                    btn.removeClass('btn-primary');
                    btn.removeClass('btn-danger');
                    btn.removeClass('btn-success');
                    btn.addClass('btn-primary');

                    $('#modal_btn-excluir').prop('disabled', true);

                    $.ajax("delete_ata",
                            {
                                type: "POST",
                                data:
                                        {
                                            idAta: idAta
                                        }
                            })
                            .done(function (data)
                            {
                                $('#modal_delete_ata').html('Ata excluída com sucesso');

                                btn.removeClass('btn-primary');
                                btn.addClass('btn-success');
                            })
                            .fail(function ()
                            {
                                $('#modal_delete_ata').html('Erro: ' + data);

                                btn.removeClass('btn-primary');
                                btn.addClass('btn-danger');
                            })
                            .always(function ()
                            {
                                btn.text('Excluído');
                            });
                });
                
                $('#modal_excluir').on('hidden.bs.modal', function ()
                {
                    window.location.replace("home");
                });
                
                // Opção para ativar e desativar
                $("body").on("click", "button[name=btnAtivar]", function () {
                    var idAta = $(this).attr('idAta');
                    //alert(idAta);
                    $.get('update_ativar_ata?idAta=' + idAta, function (data) {
                        //alert(data);
                        if (data === "1") {
                            $('#btnAtivar_' + idAta).addClass('btn btn-success btn-xs');
                            $('#btnAtivar_' + idAta).removeClass('btn-danger');
                            $('#activ_' + idAta).removeClass('glyphicon-ban-circle');
                            $('#activ_' + idAta).addClass('glyphicon-ok');
                        } else {
                            $('#btnAtivar_' + idAta).addClass('btn btn-danger btn-xs');
                            $('#btnAtivar_' + idAta).removeClass('btn-success');
                            $('#activ_' + idAta).addClass('glyphicon-ok');
                            $('#activ_' + idAta).addClass('glyphicon-ban-circle');
                        }
                    });
                });

                //$('.dataTables_filter label').addClass('pull-right');
            });
        </script>
    </body>
</html>
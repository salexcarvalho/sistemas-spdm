<script>
    $(document).ready(function () {

        var d = new Date();
        var hora = d.getHours();
        if (hora < 7) {
            hora = 7;
        }
        var min = d.getMinutes();
        if (min < 10) {
            min = '0' + min;
        }
        var dataAtual, dia, mes, ano;
        dia = d.getDate();
        mes = d.getMonth() + 1;
        ano = d.getFullYear();

        if (mes < 10) {
            mes = "0" + mes;
        }
        if (dia < 10) {
            dia = "0" + dia;
        }

        dataAtual = ano + "-" + mes + "-" + dia;

        var horaAtual = hora + ":" + min;

        $('#dtAssinatura').datetimepicker({
            locale: 'pt-br',
            format: "D/M/YYYY"
        });

        $('#horaAssinatura').datetimepicker({
            format: 'LT',
            disabledHours: [0, 1, 2, 3, 4, 5, 6, 24],
            enabledHours: [8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23],
            locale: 'pt-br'
        });

        var dtAssinatura = moment(<?= "'" . $Contrato[0]['dtAssinatura'] . "'"; ?>).format('DD/MM/YYYY');
        var HoraAssinatura = moment(<?= "'" . $Contrato[0]['dtAssinatura'] . "'"; ?>).format('HH:mm');

<?php if ($Contrato[0]['dtAssinatura'] != "") { ?>
            $('#dtAssinatura').val(dtAssinatura);
            $('#horaAssinatura').val(HoraAssinatura);
<?php } ?>
    });
</script>
<?php if ($_SESSION['AcLiberaBtnAddCont'] == true) { ?>
    <?php if ($idContrato == '') : ?>
        <form name='novocontrato' id='novocontrato' class='form-horizontal' method="POST" action="insert_contrato" enctype="multipart/form-data">
        <?php else : ?>
            <form name='novocontrato' id='novocontrato' class="form-horizontal" method="POST" action="update_contrato" enctype="multipart/form-data">
            <?php endif; ?>
            <input type="hidden" id="idContrato" name="idContrato" value="<?= $Contrato[0]['idContrato'] ?>">
            <input type="hidden" id="page" name="page" value="<?= $page ?>">
            <div class="col-lg-12">
                <div class="form-group">
                    <label class="col-lg-4">Numero da Contrato</label>
                    <label class="col-lg-4">Assunto</label>
                    <label class="col-lg-4">Número de Registro</label>

                    <div class="col-lg-4">
                        <input type="text" class="form-control" id="Contrato" name="Contrato" placeholder="Informe o número Contrato" value="<?= $Contrato[0]['Contrato'] ?>" required>
                    </div>

                    <div class="col-lg-4">
                        <input type="text" class="form-control" id="Assunto" name="Assunto" placeholder="Informe o Assunto" value="<?= $Contrato[0]['Assunto'] ?>" required>
                    </div>

                    <div class="col-lg-4">
                        <input type="text" class="form-control" id="NRegistro" name="NRegistro" placeholder="Informe o número de registro do contrato, se houver" value="<?= $Contrato[0]['NRegistro'] ?>" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-4">Setor/Superintendêcias</label>
                    <label class="col-lg-3">Tipo de Contrato</label>
                    <label class="col-lg-2">Data da Assinatura</label>
                    <label class="col-lg-3">Hora</label>
                    <div class="col-lg-4">
                        <select class="form-control" id='idSetor' name='idSetor' required>
                            <option value="" selected='selected'>Selecione uma Opção</option>
                            <?php foreach ($Setor as $Setores) { ?>
                                <option <?= ($Contrato[0]['idSetor'] == $Setores['idTipoSetor']) ? 'selected' : ''; ?> value="<?= $Setores['idTipoSetor']; ?>"><?= $Setores['Setor']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-lg-3">
                        <select class="form-control" id='idTipoDoc' name='idTipoDoc' required>
                            <option value="" selected='selected'>Selecione uma Opção</option>
                            <?php foreach ($TipoDocs as $TipoDoc) { ?>
                                <option <?= ($Contrato[0]['idTipoDoc'] == $TipoDoc['idTipoDoc']) ? 'selected' : ''; ?> value="<?= $TipoDoc['idTipoDoc']; ?>"><?= $TipoDoc['Nome']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-lg-2">
                        <input type="text" class="form-control" id="dtAssinatura" name="dtAssinatura" required>
                    </div>

                    <div class="col-lg-3">
                        <input type="text" class="form-control" id="horaAssinatura" name="horaAssinatura" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-12">Observação</label>

                    <div class="col-lg-12">
                        <textarea class='form-control' rows="2" id="Obs" name="Obs" placeholder="Informações adicionais"><?= $Contrato[0]['Obs'] ?></textarea>
                    </div>                                                    
                </div>
                <div class="form-group" >
                    <label class="col-lg-4">O tamanho máximo dos arquivos é de 30 MB.</label>
                    <div class="col-lg-4">
                        <input type="file" class="btn btn-default" id="anexo" name="anexo[]" multiple="" onchange="makeFileList();">
                    </div>
                    <div class="col-lg-4">
                        <ul id="fileList" style="margin-top: 10px; margin-bottom: 10px;"></ul>
                    </div>    
                </div>
                <?php if ($Anexo[0]['idAnexo'] != '') { ?>
                    <div class="form-group">
                        <div class="col-lg-12">
                            <ul class="mailbox-attachments clearfix">
                                <?php foreach ($Anexo as $Anexos) { ?>  
                                    <li id="Anexo_<?= $Anexos['idAnexo'] ?>">
                                        <span class="mailbox-attachment-icon">
                                            <?php if (substr($Anexos['Caminho'], '-3') == "jpg") { ?> 
                                                <i class="fa fa-file-image-o"></i>
                                            <?php } else if (substr($Anexos['Caminho'], '-3') == "png") { ?>    
                                                <i class="fa fa-photo"></i>
                                            <?php } else if (substr($Anexos['Caminho'], '-3') == "doc") { ?>    
                                                <i class="fa fa-file-word-o"></i>
                                            <?php } else if (substr($Anexos['Caminho'], '-3') == "pdf") { ?>    
                                                <i class="fa fa-file-pdf-o"></i>  
                                            <?php } else { ?>
                                                <i class="fa fa-file-o"></i>
                                            <?php } ?>  
                                        </span>
                                        <div class="mailbox-attachment-info">
                                            <a href="<?= URL::base(); ?>upload/sep/contrato/<?= $Anexos['Caminho'] ?>" class="mailbox-attachment-name" download><i class="fa fa-paperclip"></i> <?= substr($Anexos['Caminho'], '17') ?></a>
                                            <button type="button" id="excluir_anexo" name="excluir_anexo" idAnexo="<?= $Anexos['idAnexo'] ?>" idContrato="<?= $Anexos['idContrato'] ?>" nome="<?= $Anexos['Caminho'] ?>" class="btn btn-default btn-xs pull-right"  data-toggle="modal" data-target="#modal_excluir" data-placement="bottom" title="Excluir Anexo" data-delay="1"><i class="fa fa-trash"></i></button>
                                        </div>
                                    </li>
                                <?php } ?>                                                            
                            </ul>
                        </div>
                    </div>
                <?php } ?>
                <div class="col-lg-offset-1 col-lg-11">
                    <div class="form-group"> 
                        <label class="radio-inline">
                            <input required type="radio" name="Status" id="Status" value="1" <?php if ($Contrato[0]['Status'] == '1') { ?> checked <?php } ?>>
                            Ativo
                        </label>
                        <label class="radio-inline">
                            <input required type="radio" name="Status" id="Status" value="0" <?php if ($Contrato[0]['Status'] == '0') { ?> checked <?php } ?>>
                            Inativo
                        </label>
                    </div>
                </div>
            </div>
        </form>
    <?php } else { ?>
        <h1 class="page-header"> Voce não possue acesso a está pagina, consulte o administrador.</h1>
    <?php } ?>
    <script>
        $(document).ready(function () {
            var idAnexo;
            var idContrato;
            var nome;

            $('button[name=excluir_anexo]').on("click", function ()
            {
                idAnexo = $(this).attr('idAnexo');
                idContrato = $(this).attr('idContrato');
                var btn = $(this);
                btn.text('Excluindo');

                $.ajax("delete_anexo",
                        {
                            type: "GET",
                            data:
                                    {
                                        idAnexo: idAnexo,
                                        idContrato: idContrato
                                    }
                        })
                        .done(function (data)
                        {
                            var message = 'Seu anexo foi excluído com sucesso';
                            var options = {
                                message: message,
                                title: 'Anexo Excluído',
                                size: 'md',
                                useBin: true
                            };
                            eModal.alert(options);
                        })
                        .fail(function ()
                        {
                            var message = 'Erro: ' + data;
                            var options = {
                                message: message,
                                title: 'Erro de  Exclusão',
                                size: 'md',
                                useBin: true
                            };
                            eModal.alert(options);
                        })
                        .always(function ()
                        {
                            $('#Anexo_' + idAnexo).hide();
                        });
            });
        });

        function makeFileList() {
            var input = document.getElementById("anexo");
            var ul = document.getElementById("fileList");
            while (ul.hasChildNodes()) {
                ul.removeChild(ul.firstChild);
            }
            for (var i = 0; i < input.files.length; i++) {
                var li = document.createElement("li");
                li.innerHTML = input.files[i].name;
                ul.appendChild(li);
            }
            if (!ul.hasChildNodes()) {
                var li = document.createElement("li");
                li.innerHTML = 'Nenhum arquivo selecionado';
                ul.appendChild(li);
            }
        }
    </script>
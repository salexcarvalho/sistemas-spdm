<?php if ($_SESSION['AcLiberaBtnAddMod'] == true) { ?>
    <form class="form-horizontal" role="form" method="POST" action="insert_modulo" name="modulos" id="modulos">
        <div class="form-group">
            <div class="col-lg-12">
                <div class="form-group">
                    <label for="modulo" class="col-lg-3 control-label">Nome do módulo</label>
                    <div class="col-lg-9">
                        <input type="text" min="0" class="form-control" id="Nome" name="Nome" placeholder="Informe o módulo">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label">Sigla do módulo</label>
                    <div class="col-lg-9">
                        <input type="text" min="0" class="form-control" id="Alias" name="Alias" placeholder="Informe a sigla do módulo">
                    </div>
                </div>
                <div class="col-lg-offset-4 col-lg-8">
                    <div class="form-group"> 
                        <label class="radio-inline">
                            <input required type="radio" name="status" id="inativo" value="1">
                            Ativo
                        </label>
                        <label class="radio-inline">
                            <input required type="radio" name="status" id="inativo" value="0">
                            Inativo
                        </label>
                    </div>
                </div>
            </div>
        </div>        
    </form>
<?php } else { ?>
    <h1 class="page-header"> Voce não possue acesso a está pagina, consulte o administrador.</h1>
<?php } ?>
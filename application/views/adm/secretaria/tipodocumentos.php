<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - Sistema de Apoio a Secretaria</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>

            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Tipos de Documentos</h1>
                        <ol class="breadcrumb">
                            <li><i class="fa fa-folder-open"></i> Secretaria</li>
                            <li class="active"><i class="fa fa-list"></i> Listagem</li>
                        </ol>

                        <?php if (isset($warning)) : ?>
                            <div id="msg" class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong><?= $warning; ?></strong>
                            </div>
                        <?php endif ?>                          
                        <div class="row">
                            <div class="col-lg-4">
                                <form class="form-inline" method="get" action="home">
                                    <input  type="text" class="form-control input-sm" id="Busca" name="Busca" value="<?= $Pesquisa; ?>" placeholder="Informe o Assunto" style="width: 280px;">
                                    <button type="submit" class="btn btn-danger btn-sm"><i class="glyphicon glyphicon-search"></i> Buscar</button>
                                    <a href="home" type="reset" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-erase"></i> Limpar</a>
                                </form>
                            </div>
                            <div class="col-lg-4">
                                
                            </div>
                            <?php if ($_SESSION['AcLiberaBtnAddTpDoc'] == TRUE) { ?>
                                <div class="col-lg-4">
                                    <button type="submit" id="btn_novo_tipodocumentos" class="pull-right btn btn-primary btn-sm" name="btn_novo_tipodocumentos"><i class="glyphicon glyphicon-plus-sign">&nbsp;</i>Adicionar Tipo de Docs</button>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="tipos">
                                <thead>
                                    <tr>
                                        <th width="5%" style="text-align: center">ID</th>
                                        <th>Tipo</th>
                                        <th width="22%">Alias</th>
                                        <th width="20%">Módulo</th>
                                        <th width="8%" style="text-align: center">Ações</th>
                                        <th width="6%" style="text-align: center">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($data as $tipo) {
                                        ?>
                                        <tr>
                                            <td align="center"><?= $tipo['idTipoDoc']; ?></td>
                                            <td><?= $tipo['Nome']; ?></td>
                                            <td><?= $tipo['Alias']; ?></td>
                                            <td><?= $tipo['nmModulo']; ?></td>
                                            <td align="center">
                                                <button  type="submit" id="btnEditar" class="btn btn-default btn-xs" name="btnEditar" page="<?= $PaginaAtual; ?>" idTipoDoc="<?= $tipo['idTipoDoc']; ?>" title="Editar Tipo de Documentos" <?php if ($_SESSION['AcBtnEditTpDoc'] == NULL) { ?>disabled<?php } ?>>
                                                    <i class="glyphicon glyphicon-edit"></i>
                                                </button>
                                                
                                                <input type="hidden" id="idTipoDoc<?= $tipo['idTipoDoc']; ?>" name="idTipoDoc" value="<?= $tipo['idTipoDoc']; ?>">
                                                <button  type="button" id="btnExcluir" class="btn btn-default btn-xs" name="btnExcluir" idTipoDoc="<?= $tipo['idTipoDoc']; ?>" nome="<?= $tipo['Nome']; ?>" <?php if ($_SESSION['AcBtnExcTpDoc'] == NULL) { ?>disabled<?php } ?> data-toggle="modal" data-target="#modal_excluir" data-placement="bottom" title="Excluir Tipo de Documentos" data-delay="1">
                                                    <i class="glyphicon glyphicon-trash"></i>
                                                </button>
                                            </td>
                                            <td align="center">
                                                <input type="hidden" name="idTipoDoc" value="<?= $tipo['idTipoDoc']; ?>">
                                                <?php
                                                if ($tipo['Status'] == "0") {
                                                    ?>
                                                    <button type="submit" id="btnAtivar_<?= $tipo['idTipoDoc']; ?>" class="btn btn-danger btn-xs" idTipoDoc="<?= $tipo['idTipoDoc']; ?>" name="btnAtivar" title="Ativar Tipo de Documentos" <?php if ($_SESSION['AcBtnAtivTpDoc'] == NULL) { ?>disabled<?php } ?>>
                                                        <i id="activ_<?= $tipo['idTipoDoc']; ?>" class="glyphicon glyphicon-ban-circle"></i>

                                                    </button>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <button type="submit" id="btnAtivar_<?= $tipo['idTipoDoc']; ?>" class="btn btn-success btn-xs" idTipoDoc="<?= $tipo['idTipoDoc']; ?>" name="btnAtivar" title="Destivar Tipo de Documentos" <?php if ($_SESSION['AcBtnAtivTpDoc'] == NULL) { ?>disabled<?php } ?>>
                                                        <i id="activ_<?= $tipo['idTipoDoc']; ?>" class="glyphicon glyphicon-ok"></i>

                                                    </button>
                                                    <?php
                                                }
                                                ?>

                                            </td>
                                            <?php
                                        }
                                        ?>
                                </tbody>    
                            </table>
                            <?php include(kohana::find_file('views/paginator', 'paginacao', 'php')) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="modal_excluir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Confirmar Exclusão</h4>
                    </div>
                    <div class="modal-body" id="modal_delete_tipodocumentos">Deseja excluir este tipo de documento?</div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        <button type="button" class="btn btn-primary" id="modal_btn-excluir">Excluir</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal -->
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>

        <script>
            jQuery(document).ready(function () {
                // Remove seleção de ativo no menu.
                $('.nav li').removeClass('active');
                $('#side-menu li').removeClass('active');
                // Ativa botão no menu.
                $('#li-gestaoP ul').addClass('collapse in');
                $('#li-tipodoc a').addClass('active');
                $('#li-tipodoc').addClass('active');
                $('#li-tipodoc a').addClass('collapse in');
                
                // Datatables
                $('#tipos').dataTable(
                        {
                            "aoColumnDefs": [{"bSortable": false, "aTargets": [2, 3, 4]}],
                            "paging": false,
                            "info": false,
                            "aaSorting": [[0, 'desc']],
                            "bFilter": false,
                            "oLanguage":
                                    {
                                        "sSearch": " Buscar: "
                                    }
                        });
                        
                // abre o modal para cadastro
                $('button[name=btn_novo_tipodocumentos]').click(function () {
                    var idTipoDoc = '';
                    var options = {
                        url: "cadastro_tipodocumentos?idTipoDoc=" + idTipoDoc,
                        title: 'Cadastrar Tipo de Documento',
                        size: eModal.size.md,
                        buttons: [
                            {text: 'GRAVAR', style: 'primary', close: false, type: 'submit', form: 'tipodocumentos'},
                            {text: 'FECHAR', style: 'danger', close: true}
                        ]
                    };
                    eModal.ajax(options);
                });
                
                //abre o modal de edição
                $('button[name=btnEditar]').click(function () {
                    var idTipoDoc = $(this).attr('idTipoDoc');
                    var page = $(this).attr('page');
                    var options = {
                        url: "editar_tipodocumentos?idTipoDoc=" + idTipoDoc +"&page="+ page,
                        title: 'Editar Tipo de Documento',
                        size: eModal.size.md,
                        buttons: [
                            {text: 'SALVAR', style: 'primary', close: false, type: 'submit', form: 'tipodocumentos'},
                            {text: 'FECHAR', style: 'danger', close: true}
                        ]
                    };
                    eModal.ajax(options);
                });
                
                // Opção usada na exclusão        
                var idTipoDoc;
                var nome;
                $('button[name=btnExcluir]').on("click", function ()
                {
                    idTipoDoc = $(this).attr('idTipoDoc');
                    nome = $(this).attr('nome');
                    $('#modal_delete_tipodocumentos').html("Deseja excluir este Tipo de Documentos <b>\"" + nome + "\"</b> ?");
                });

                $('#modal_excluir').on('hidden.bs.modal', function ()
                {
                    window.location.replace("home");
                });

                $('#modal_btn-excluir').on("click", function ()
                {
                    var btn = $(this);
                    btn.text('loading');

                    btn.removeClass('btn-primary');
                    btn.removeClass('btn-danger');
                    btn.removeClass('btn-success');
                    btn.addClass('btn-primary');

                    $('#modal_btn-excluir').prop('disabled', true);

                    $.ajax("delete_tipodocumentos",
                            {
                                type: "POST",
                                data:
                                        {
                                            idTipoDoc: idTipoDoc
                                        }
                            })
                            .done(function (data)
                            {
                                $('#modal_delete_tipodocumentos').html('Tipo de Documentos excluído com sucesso');

                                btn.removeClass('btn-primary');
                                btn.addClass('btn-success');
                            })
                            .fail(function ()
                            {
                                $('#modal_delete_tipodocumentos').html('Erro: ' + data);

                                btn.removeClass('btn-primary');
                                btn.addClass('btn-danger');
                            })
                            .always(function ()
                            {
                                btn.text('Excluído');
                            });
                });
                
                // Opção para ativar e desativar
                $("body").on("click", "button[name=btnAtivar]", function () {
                    var idTipoDoc = $(this).attr('idTipoDoc');
                    //alert(txtUsuario);
                    $.get('update_ativar_tipodocumentos?idTipoDoc=' + idTipoDoc, function (data) {

                        if (data === "1") {
                            $('#btnAtivar_' + idTipoDoc).addClass('btn btn-success btn-xs');
                            $('#btnAtivar_' + idTipoDoc).removeClass('btn-danger');
                            $('#activ_' + idTipoDoc).removeClass('glyphicon-ban-circle');
                            $('#activ_' + idTipoDoc).addClass('glyphicon-ok');
                        } else {
                            $('#btnAtivar_' + idTipoDoc).addClass('btn btn-danger btn-xs');
                            $('#btnAtivar_' + idTipoDoc).removeClass('btn-success');
                            $('#activ_' + idTipoDoc).addClass('glyphicon-ok');
                            $('#activ_' + idTipoDoc).addClass('glyphicon-ban-circle');
                        }
                    });
                });

                $('.dataTables_filter label').addClass('pull-right');
            });
        </script>
    </body>
</html>
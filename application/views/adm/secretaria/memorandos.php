<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - Sistema de Apoio a Secretaria</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>

            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Controle de Memorandos</h1>
                        <ol class="breadcrumb">
                            <li><i class="fa fa-folder-open"></i> Secretaria</li>
                            <li class="active"><i class="fa fa-list"></i> Listagem</li>
                        </ol>

                        <?php if (isset($warning)) : ?>
                            <div id="msg" class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong><?= $warning; ?></strong>
                            </div>
                        <?php endif ?>  
                        <div class="row">
                            <div class="col-lg-4">
                                <form class="form-inline" method="get" action="home">
                                    <input  type="text" class="form-control input-sm" id="Busca" name="Busca" value="<?= $Pesquisa; ?>" placeholder="Informe o Assunto" style="width: 280px;">
                                    <button type="submit" class="btn btn-danger btn-sm"><i class="glyphicon glyphicon-search"></i> Buscar</button>
                                    <a href="home" type="reset" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-erase"></i> Limpar</a>
                                </form>
                            </div>
                            <div class="col-lg-4">
                                
                            </div>
                            <?php if ($_SESSION['AcLiberaBtnAddMem'] == TRUE) { ?>
                                <div class="col-lg-4">
                                    <button type="submit" id="btn_novo_memorando" class="pull-right btn btn-primary btn-sm" name="btn_novo_memorando"><i class="glyphicon glyphicon-plus-sign">&nbsp;</i>Registrar Memorando</button>
                                </div>
                            <?php } ?>
                        </div>                        
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="memorandos">
                                <thead>
                                    <tr>
                                        <th width="5%" style="text-align: center">ID</th>
                                        <th width="12%" style="text-align: center">Número e Ano</th>
                                        <th>Assunto</th>
                                        <th width="15%">Departamento</th>
                                        <th width="8%" style="text-align: center">Data de Emissão</th>
                                        <th width="8%" style="text-align: center">Ações</th>
                                        <th width="5%" style="text-align: center">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($data as $memorando) {
                                        ?>
                                        <tr>
                                            <td align="center"><?= $memorando['idMemorando']; ?></td>
                                            <td align="center"><?= $memorando['Numero']; ?>/<?= $memorando['AnoCorrente']; ?></td>
                                            <td><?= $memorando['Assunto']; ?></td>
                                            <td><?= $memorando['Setor']; ?></td>
                                            <td align="center"><?= strftime('%d/%m/%Y', strtotime($memorando['dtEmissao'])); ?></td>
                                            <td align="center">
                                                <button  type="submit" id="btnVisualizar" class="btn btn-default btn-xs" name="btnVisualizar" page="<?= $PaginaAtual; ?>" idMemorando="<?= $memorando['idMemorando']; ?>" title="Visualizar Memorando" <?php if ($_SESSION['AcVisualizaMem'] == NULL) { ?>disabled<?php } ?>>
                                                    <i class="fa fa-eye"></i>
                                                </button>

                                                <button  type="submit" id="btnEditar" class="btn btn-default btn-xs" name="btnEditar" page="<?= $PaginaAtual; ?>" idMemorando="<?= $memorando['idMemorando']; ?>" title="Editar Memorando" <?php if ($_SESSION['AcBtnEditMem'] == NULL) { ?>disabled<?php } ?>>
                                                    <i class="glyphicon glyphicon-edit"></i>
                                                </button>
                                                
                                                <input type="hidden" id="idMemorando<?= $memorando['idMemorando']; ?>" name="idMemorando" value="<?= $memorando['idMemorando']; ?>">
                                                <button  type="button" id="btnExcluir" class="btn btn-default btn-xs" name="btnExcluir" idMemorando="<?= $memorando['idMemorando']; ?>" nome="<?= $memorando['Assunto']; ?>" <?php if ($_SESSION['AcBtnExcMem'] == NULL) { ?>disabled<?php } ?> data-toggle="modal" data-target="#modal_excluir" data-placement="bottom" title="Excluir Memorando" data-delay="1">
                                                    <i class="glyphicon glyphicon-trash"></i>
                                                </button>
                                            </td>
                                            <td align="center">
                                                <?php
                                                if ($memorando['Status'] == "0") {
                                                    ?>
                                                    <button type="submit" id="btnAtivar_<?= $memorando['idMemorando']; ?>" class="btn btn-danger btn-xs" idMemorando="<?= $memorando['idMemorando']; ?>" name="btnAtivar" title="Ativar Memorando" <?php if ($_SESSION['AcBtnAtivMem'] == NULL) { ?>disabled<?php } ?>>
                                                        <i id="activ_<?= $memorando['idMemorando']; ?>" class="glyphicon glyphicon-ban-circle"></i>
                                                    </button>
                                                <?php } else { ?>
                                                    <button type="submit" id="btnAtivar_<?= $memorando['idMemorando']; ?>" class="btn btn-success btn-xs" idMemorando="<?= $memorando['idMemorando']; ?>" name="btnAtivar" title="Destivar Memorando" <?php if ($_SESSION['AcBtnAtivMem'] == NULL) { ?>disabled<?php } ?>>
                                                        <i id="activ_<?= $memorando['idMemorando']; ?>" class="glyphicon glyphicon-ok"></i>
                                                    </button>
                                                    <?php
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>    
                            </table>
                            <?php include(kohana::find_file('views/paginator', 'paginacao', 'php')) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="modal_excluir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Confirmar Exclusão</h4>
                    </div>
                    <div class="modal-body" id="modal_delete_memorando">Deseja excluir este memorando?</div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        <button type="button" class="btn btn-primary" id="modal_btn-excluir">Excluir</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal -->
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>

        <script>
            jQuery(document).ready(function () {
                // Remove seleção de ativo no menu.
                $('.nav li').removeClass('active');
                $('#side-menu li').removeClass('active');
                // Ativa botão no menu.
                $('#li-gestaoP ul').addClass('collapse in');
                $('#li-memorando a').addClass('active');
                $('#li-memorando').addClass('active');
                $('#li-memorando a').addClass('collapse in');
                
                // Datatables
                $('#memorandos').dataTable(
                        {
                            "aoColumnDefs": [{"bSortable": false, "aTargets": [2,3,4,5,6]}],
                            "paging": false,
                            "info": false,
                            "aaSorting": [[0, 'desc']],
                            "bFilter": false,
                            "oLanguage":
                                    {
                                        "sSearch": " Buscar: "
                                    }
                        });
                        
                // abre o modal para cadastro
                $('button[name=btn_novo_memorando]').click(function () {
                    var idMemorando = '';
                    var options = {
                        url: "memorando?idMemorando=" + idMemorando,
                        title: 'Cadastrar Memorando',
                        size: eModal.size.lg,
                        buttons: [
                            {text: 'GRAVAR', style: 'primary', close: false, type: 'submit', form: 'novomemorando'},
                            {text: 'FECHAR', style: 'danger', close: true}
                        ]
                    };
                    eModal.ajax(options);
                });
                
                //abre o modal de edição
                $('button[name=btnEditar]').click(function () {
                    var idMemorando = $(this).attr('idMemorando');
                    var page = $(this).attr('page');
                    var options = {
                        url: "memorando?idMemorando=" + idMemorando +"&page="+ page,
                        title: 'Editar Memorando',
                        size: eModal.size.lg,
                        buttons: [
                            {text: 'SALVAR', style: 'primary', close: false, type: 'submit', form: 'novomemorando'},
                            {text: 'FECHAR', style: 'danger', close: true}
                        ]
                    };
                    eModal.ajax(options);
                });
                
                // Abre o maodal de visualização
                $('button[name=btnVisualizar]').click(function () {
                    var idMemorando = $(this).attr('idMemorando');
                    var page = $(this).attr('page');
                    var options = {
                        url: "visualizar_memorando?idMemorando=" + idMemorando +"&page="+ page,
                        title: 'Visualizar Memorando',
                        size: eModal.size.lg,
                        buttons: [
                            {text: 'FECHAR', style: 'danger', close: true}
                        ]
                    };
                    eModal.ajax(options);
                });
                                
                // Opção usada na exclusão        
                var idMemorando;
                var nome;
                $('button[name=btnExcluir]').on("click", function ()
                {
                    idMemorando = $(this).attr('idMemorando');
                    nome = $(this).attr('nome');
                    $('#modal_delete_memorando').html("Deseja excluir este Memorando <b>\"" + nome + "\"</b> ?");
                });

                $('#modal_excluir').on('hidden.bs.modal', function ()
                {
                    window.location.replace("home");
                });

                $('#modal_btn-excluir').on("click", function ()
                {
                    var btn = $(this);
                    btn.text('loading');

                    btn.removeClass('btn-primary');
                    btn.removeClass('btn-danger');
                    btn.removeClass('btn-success');
                    btn.addClass('btn-primary');

                    $('#modal_btn-excluir').prop('disabled', true);

                    $.ajax("delete_memorando",
                            {
                                type: "POST",
                                data:
                                        {
                                            idMemorando: idMemorando
                                        }
                            })
                            .done(function (data)
                            {
                                $('#modal_delete_memorando').html('Memorando excluído com sucesso');

                                btn.removeClass('btn-primary');
                                btn.addClass('btn-success');
                            })
                            .fail(function ()
                            {
                                $('#modal_delete_memorando').html('Erro: ' + data);

                                btn.removeClass('btn-primary');
                                btn.addClass('btn-danger');
                            })
                            .always(function ()
                            {
                                btn.text('Excluído');
                            });
                });
                
                // Opção para ativar e desativar
                $("body").on("click", "button[name=btnAtivar]", function () {
                    var idMemorando = $(this).attr('idMemorando');
                    //alert(idMemorando);
                    $.get('update_ativar_memorando?idMemorando=' + idMemorando, function (data) {
                        //alert(data);
                        if (data === "1") {
                            $('#btnAtivar_' + idMemorando).addClass('btn btn-success btn-xs');
                            $('#btnAtivar_' + idMemorando).removeClass('btn-danger');
                            $('#activ_' + idMemorando).removeClass('glyphicon-ban-circle');
                            $('#activ_' + idMemorando).addClass('glyphicon-ok');
                        } else {
                            $('#btnAtivar_' + idMemorando).addClass('btn btn-danger btn-xs');
                            $('#btnAtivar_' + idMemorando).removeClass('btn-success');
                            $('#activ_' + idMemorando).addClass('glyphicon-ok');
                            $('#activ_' + idMemorando).addClass('glyphicon-ban-circle');
                        }
                    });
                });

                //$('.dataTables_filter label').addClass('pull-right');
            });
        </script>
    </body>
</html>
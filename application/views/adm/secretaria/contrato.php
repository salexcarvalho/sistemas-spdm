<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - Sistema de Apoio a Secretaria</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>

            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Controle de Contratos</h1>
                        <ol class="breadcrumb">
                            <li><i class="fa fa-folder-open"></i> Secretaria</li>
                            <li class="active"><i class="fa fa-list"></i> Listagem</li>
                        </ol>

                        <?php if (isset($warning)) : ?>
                            <div id="msg" class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong><?= $warning; ?></strong>
                            </div>
                        <?php endif ?>  
                        <div class="row">
                            <div class="col-lg-6">    
                                    <form class="form-inline" method="get" action="home">                      
                                        <input  type="text" class="form-control input-sm" id="Busca" name="Busca" value="<?= $Pesquisa; ?>" placeholder="Informe o Assunto" style="width: 350px;">
                                        <button type="submit" class="btn btn-danger btn-sm"><i class="glyphicon glyphicon-search"></i> Buscar</button>
                                        <a href="home" type="reset" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-erase"></i> Limpar</a>
                                    </form>
                                </div>
                            <?php if ($_SESSION['AcLiberaBtnAddCont'] == TRUE) { ?>
                                <div class="col-lg-6">
                                    <button type="submit" id="btn_novo_contrato" class="pull-right btn btn-primary btn-sm" name="btn_novo_contrato"><i class="glyphicon glyphicon-plus-sign"></i> Cadastrar Contrato</button>
                                </div>
                            <?php } ?>
                        </div>                        
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="contratos">
                                <thead>
                                    <tr>
                                        <th width="5%" style="text-align: center">ID</th>
                                        <th>Assunto</th>
                                        <th width="14%">Tipo de Contrato</th>
                                        <th width="12%" style="text-align: center">Data da Assinatura</th>
                                        <th width="16%">Nº Registro</th>
                                        <th width="8%" style="text-align: center">Ações</th>
                                        <th width="5%" style="text-align: center">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($data as $contrato) {
                                        ?>
                                        <tr>
                                            <td align="center"><?= $contrato['idContrato']; ?></td>
                                            <td><?= $contrato['Assunto']; ?></td>
                                            <td><?= $contrato['Nome']; ?></td>
                                            <td align="center"><?= strftime('%d/%m/%Y', strtotime($contrato['dtAssinatura'])); ?></td>
                                            <td><?= $contrato['NRegistro']; ?></td>
                                            <td align="center">
                                                <button  type="submit" id="btnVisualizar" class="btn btn-default btn-xs" name="btnVisualizar"  page="<?= $PaginaAtual; ?>" idContrato="<?= $contrato['idContrato']; ?>" title="Visualizar Contrato" <?php if ($_SESSION['AcVisualizaCont'] == NULL) { ?>disabled<?php } ?>>
                                                    <i class="fa fa-eye"></i>
                                                </button>
                                                
                                                <button  type="submit" id="btnEditarContrato" class="btn btn-default btn-xs" name="btnEditarContrato" page="<?= $PaginaAtual; ?>" idContrato="<?= $contrato['idContrato']; ?>" title="Editar Contrato" <?php if ($_SESSION['AcBtnEditCont'] == NULL) { ?>disabled<?php } ?>>
                                                    <i class="glyphicon glyphicon-edit"></i>
                                                </button>
                                                
                                                <input type="hidden" id="idContrato<?= $contrato['idContrato']; ?>" name="idContrato" value="<?= $contrato['idContrato']; ?>">
                                                <button  type="button" id="btnExcluir" class="btn btn-default btn-xs" name="btnExcluir" idContrato="<?= $contrato['idContrato']; ?>" nome="<?= $contrato['Assunto']; ?>" <?php if ($_SESSION['AcBtnExcCont'] == NULL) { ?>disabled<?php } ?> data-toggle="modal" data-target="#modal_excluir" data-placement="bottom" title="Excluir Contrato" data-delay="1">
                                                    <i class="glyphicon glyphicon-trash"></i>
                                                </button>
                                            </td>
                                            <td align="center">
                                                <?php
                                                if ($contrato['Status'] == "0") {
                                                    ?>
                                                    <button type="submit" id="btnAtivar_<?= $contrato['idContrato']; ?>" class="btn btn-danger btn-xs" idContrato="<?= $contrato['idContrato']; ?>" name="btnAtivar" title="Ativar Contrato" <?php if ($_SESSION['AcBtnAtivCont'] == NULL) { ?>disabled<?php } ?>>
                                                        <i id="activ_<?= $contrato['idContrato']; ?>" class="glyphicon glyphicon-ban-circle"></i>
                                                    </button>
                                                <?php } else { ?>
                                                    <button type="submit" id="btnAtivar_<?= $contrato['idContrato']; ?>" class="btn btn-success btn-xs" idContrato="<?= $contrato['idContrato']; ?>" name="btnAtivar" title="Destivar Contrato" <?php if ($_SESSION['AcBtnAtivCont'] == NULL) { ?>disabled<?php } ?>>
                                                        <i id="activ_<?= $contrato['idContrato']; ?>" class="glyphicon glyphicon-ok"></i>
                                                    </button>
                                                    <?php
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>    
                            </table>
                            <?php include(kohana::find_file('views/paginator', 'paginacao', 'php')) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="modal_excluir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Confirmar Exclusão</h4>
                    </div>
                    <div class="modal-body" id="modal_delete_contrato">Deseja excluir este contrato?</div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        <button type="button" class="btn btn-primary" id="modal_btn-excluir">Excluir</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal -->
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>

        <script>
            jQuery(document).ready(function () {
                // Remove seleção de ativo no menu.
                $('.nav li').removeClass('active');
                $('#side-menu li').removeClass('active');
                // Ativa botão no menu.
                $('#li-gestaoP ul').addClass('collapse in');
                $('#li-contrato a').addClass('active');
                $('#li-contrato').addClass('active');
                $('#li-contrato a').addClass('collapse in');
                
                // Dcontratotables
                $('#contratos').dataTable(
                {
                    "aoColumnDefs": [{"bSortable": false, "aTargets": [3,4,5]}],
                    "paging": false,
                    "info": false,
                    "aaSorting": [[0, 'desc']],
                    "bFilter": false,
                    "oLanguage":
                            {
                                "sSearch": " Buscar: "
                            }
                });
                
                // abre o modal para cadastro
                $('button[name=btn_novo_contrato]').click(function () {
                    var idContrato = '';
                    var options = {
                        url: "contrato?idContrato=" + idContrato,
                        title: 'Cadastrar Contrato',
                        size: eModal.size.lg,
                        buttons: [
                            {text: 'GRAVAR', style: 'primary', close: false, type: 'submit', form: 'novocontrato'},
                            {text: 'FECHAR', style: 'danger', close: true}
                        ]
                    };
                    eModal.ajax(options);
                });
                
                //abre o modal de edição
                $('button[name=btnEditarContrato]').click(function () {
                    var idContrato = $(this).attr('idContrato');
                    var page = $(this).attr('page');
                    var options = {
                        url: "contrato?idContrato=" + idContrato +"&page="+ page,
                        title: 'Editar Contrato',
                        size: eModal.size.lg,
                        buttons: [
                            {text: 'SALVAR', style: 'primary', close: false, type: 'submit', form: 'novocontrato'},
                            {text: 'FECHAR', style: 'danger', close: true}
                        ]
                    };
                    eModal.ajax(options);
                });
                
                // Abre o maodal de visualização
                $('button[name=btnVisualizar]').click(function () {
                    var idContrato = $(this).attr('idContrato');
                    var page = $(this).attr('page');
                    var options = {
                        url: "visualizar_contrato?idContrato=" + idContrato +"&page="+ page,
                        title: 'Visualizar Contrato',
                        size: eModal.size.lg,
                        buttons: [
                            {text: 'FECHAR', style: 'danger', close: true}
                        ]
                    };
                    eModal.ajax(options);
                });
                
                
                // Opção usada na exclusão
                var idContrato;
                var nome;
                $('button[name=btnExcluir]').on("click", function ()
                {
                    idContrato = $(this).attr('idContrato');
                    nome = $(this).attr('nome');
                    $('#modal_delete_contrato').html("Deseja excluir este Contrato <b>\"" + nome + "\"</b> ?");
                });

                $('#modal_btn-excluir').on("click", function ()
                {
                    var btn = $(this);
                    btn.text('loading');

                    btn.removeClass('btn-primary');
                    btn.removeClass('btn-danger');
                    btn.removeClass('btn-success');
                    btn.addClass('btn-primary');

                    $('#modal_btn-excluir').prop('disabled', true);

                    $.ajax("delete_contrato",
                            {
                                type: "POST",
                                data:
                                        {
                                            idContrato: idContrato
                                        }
                            })
                            .done(function (data)
                            {
                                $('#modal_delete_contrato').html('Contrato excluída com sucesso');

                                btn.removeClass('btn-primary');
                                btn.addClass('btn-success');
                            })
                            .fail(function ()
                            {
                                $('#modal_delete_contrato').html('Erro: ' + data);

                                btn.removeClass('btn-primary');
                                btn.addClass('btn-danger');
                            })
                            .always(function ()
                            {
                                btn.text('Excluído');
                            });
                });
                
                $('#modal_excluir').on('hidden.bs.modal', function ()
                {
                    window.location.replace("home");
                });
                
                // Opção para ativar e desativar
                $("body").on("click", "button[name=btnAtivar]", function () {
                    var idContrato = $(this).attr('idContrato');
                    //alert(idContrato);
                    $.get('update_ativar_contrato?idContrato=' + idContrato, function (data) {
                        //alert(data);
                        if (data === "1") {
                            $('#btnAtivar_' + idContrato).addClass('btn btn-success btn-xs');
                            $('#btnAtivar_' + idContrato).removeClass('btn-danger');
                            $('#activ_' + idContrato).removeClass('glyphicon-ban-circle');
                            $('#activ_' + idContrato).addClass('glyphicon-ok');
                        } else {
                            $('#btnAtivar_' + idContrato).addClass('btn btn-danger btn-xs');
                            $('#btnAtivar_' + idContrato).removeClass('btn-success');
                            $('#activ_' + idContrato).addClass('glyphicon-ok');
                            $('#activ_' + idContrato).addClass('glyphicon-ban-circle');
                        }
                    });
                });

                //$('.dataTables_filter label').addClass('pull-right');
            });
        </script>
    </body>
</html>
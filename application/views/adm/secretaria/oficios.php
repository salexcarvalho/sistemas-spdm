<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - Sistema de Apoio a Secretaria</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>

            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Controle de Ofícios</h1>
                        <ol class="breadcrumb">
                            <li><i class="fa fa-folder-open"></i> Secretaria</li>
                            <li class="active"><i class="fa fa-list"></i> Listagem</li>
                        </ol>

                        <?php if (isset($warning)) : ?>
                            <div id="msg" class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong><?= $warning; ?></strong>
                            </div>
                        <?php endif ?>  
                        <div class="row">
                            <div class="col-lg-4">
                                <form class="form-inline" method="get" action="home">
                                    <input  type="text" class="form-control input-sm" id="Busca" name="Busca" value="<?= $Pesquisa; ?>" placeholder="Informe o Assunto" style="width: 280px;">
                                    <button type="submit" class="btn btn-danger btn-sm"><i class="glyphicon glyphicon-search"></i> Buscar</button>
                                    <a href="home" type="reset" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-erase"></i> Limpar</a>
                                </form>
                            </div>
                            <div class="col-lg-4">
                                
                            </div>
                            <?php if ($_SESSION['AcLiberaBtnAddOfic'] == TRUE) { ?>
                                <div class="col-lg-4">
                                    <button type="submit" id="btn_novo_oficio" class="pull-right btn btn-primary btn-sm" name="btn_novo_oficio"><i class="glyphicon glyphicon-plus-sign">&nbsp;</i>Registrar Ofício</button>
                                </div>
                            <?php } ?>
                        </div>                        
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="oficios">
                                <thead>
                                    <tr>
                                        <th width="5%" style="text-align: center">ID</th>
                                        <th width="12%" style="text-align: center">Número e Ano</th>
                                        <th>Assunto</th>
                                        <th width="15%">Departamento</th>
                                        <th width="8%" style="text-align: center">Data de Emissão</th>
                                        <th width="8%" style="text-align: center">Ações</th>
                                        <th width="5%" style="text-align: center">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($data as $oficio) {
                                        ?>
                                        <tr>
                                            <td align="center"><?= $oficio['idOficio']; ?></td>
                                            <td align="center"><?= $oficio['Numero']; ?>/<?= $oficio['AnoCorrente']; ?></td>
                                            <td><?= $oficio['Assunto']; ?></td>
                                            <td><?= $oficio['Setor']; ?></td>
                                            <td align="center"><?= strftime('%d/%m/%Y', strtotime($oficio['dtEmissao'])); ?></td>
                                            <td align="center">
                                                <button  type="submit" id="btnVisualizar" class="btn btn-default btn-xs" name="btnVisualizar" page="<?= $PaginaAtual; ?>" idOficio="<?= $oficio['idOficio']; ?>" title="Visualizar Oficio" <?php if ($_SESSION['AcVisualizaOfic'] == NULL) { ?>disabled<?php } ?>>
                                                    <i class="fa fa-eye"></i>
                                                </button>

                                                <button  type="submit" id="btnEditar" class="btn btn-default btn-xs" name="btnEditar" page="<?= $PaginaAtual; ?>" idOficio="<?= $oficio['idOficio']; ?>" title="Editar Oficio" <?php if ($_SESSION['AcBtnEditOfic'] == NULL) { ?>disabled<?php } ?>>
                                                    <i class="glyphicon glyphicon-edit"></i>
                                                </button>
                                                
                                                <input type="hidden" id="idOficio<?= $oficio['idOficio']; ?>" name="idOficio" value="<?= $oficio['idOficio']; ?>">
                                                <button  type="button" id="btnExcluir" class="btn btn-default btn-xs" name="btnExcluir" idOficio="<?= $oficio['idOficio']; ?>" nome="<?= $oficio['Assunto']; ?>" <?php if ($_SESSION['AcBtnExcOfic'] == NULL) { ?>disabled<?php } ?> data-toggle="modal" data-target="#modal_excluir" data-placement="bottom" title="Excluir Oficio" data-delay="1">
                                                    <i class="glyphicon glyphicon-trash"></i>
                                                </button>
                                            </td>
                                            <td align="center">
                                                <?php
                                                if ($oficio['Status'] == "0") {
                                                    ?>
                                                    <button type="submit" id="btnAtivar_<?= $oficio['idOficio']; ?>" class="btn btn-danger btn-xs" idOficio="<?= $oficio['idOficio']; ?>" name="btnAtivar" title="Ativar Oficio" <?php if ($_SESSION['AcBtnAtivOfic'] == NULL) { ?>disabled<?php } ?>>
                                                        <i id="activ_<?= $oficio['idOficio']; ?>" class="glyphicon glyphicon-ban-circle"></i>
                                                    </button>
                                                <?php } else { ?>
                                                    <button type="submit" id="btnAtivar_<?= $oficio['idOficio']; ?>" class="btn btn-success btn-xs" idOficio="<?= $oficio['idOficio']; ?>" name="btnAtivar" title="Destivar Oficio" <?php if ($_SESSION['AcBtnAtivOfic'] == NULL) { ?>disabled<?php } ?>>
                                                        <i id="activ_<?= $oficio['idOficio']; ?>" class="glyphicon glyphicon-ok"></i>
                                                    </button>
                                                    <?php
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>    
                            </table>
                            <?php include(kohana::find_file('views/paginator', 'paginacao', 'php')) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="modal_excluir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Confirmar Exclusão</h4>
                    </div>
                    <div class="modal-body" id="modal_delete_oficio">Deseja excluir este oficio?</div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        <button type="button" class="btn btn-primary" id="modal_btn-excluir">Excluir</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal -->
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>

        <script>
            jQuery(document).ready(function () {
                // Remove seleção de ativo no menu.
                $('.nav li').removeClass('active');
                $('#side-menu li').removeClass('active');
                // Ativa botão no menu.
                $('#li-gestaoP ul').addClass('collapse in');
                $('#li-oficio a').addClass('active');
                $('#li-oficio').addClass('active');
                $('#li-oficio a').addClass('collapse in');
                
                // Datatables
                $('#oficios').dataTable(
                        {
                            "aoColumnDefs": [{"bSortable": false, "aTargets": [2,3,4,5,6]}],
                            "paging": false,
                            "info": false,
                            "aaSorting": [[0, 'desc']],
                            "bFilter": false,
                            "oLanguage":
                                    {
                                        "sSearch": " Buscar: "
                                    }
                        });
                        
                // abre o modal para cadastro
                $('button[name=btn_novo_oficio]').click(function () {
                    var idOficio = '';
                    var options = {
                        url: "oficio?idOficio=" + idOficio,
                        title: 'Cadastrar Ofício',
                        size: eModal.size.lg,
                        buttons: [
                            {text: 'GRAVAR', style: 'primary', close: false, type: 'submit', form: 'novooficio'},
                            {text: 'FECHAR', style: 'danger', close: true}
                        ]
                    };
                    eModal.ajax(options);
                });
                
                //abre o modal de edição
                $('button[name=btnEditar]').click(function () {
                    var idOficio = $(this).attr('idOficio');
                    var page = $(this).attr('page');
                    var options = {
                        url: "oficio?idOficio=" + idOficio +"&page="+ page,
                        title: 'Editar Ofício',
                        size: eModal.size.lg,
                        buttons: [
                            {text: 'SALVAR', style: 'primary', close: false, type: 'submit', form: 'novooficio'},
                            {text: 'FECHAR', style: 'danger', close: true}
                        ]
                    };
                    eModal.ajax(options);
                });
                
                // Abre o maodal de visualização
                $('button[name=btnVisualizar]').click(function () {
                    var idOficio = $(this).attr('idOficio');
                    var page = $(this).attr('page');
                    var options = {
                        url: "visualizar_oficio?idOficio=" + idOficio +"&page="+ page,
                        title: 'Visualizar Ofício',
                        size: eModal.size.lg,
                        buttons: [
                            {text: 'FECHAR', style: 'danger', close: true}
                        ]
                    };
                    eModal.ajax(options);
                });
                                
                // Opção usada na exclusão        
                var idOficio;
                var nome;
                $('button[name=btnExcluir]').on("click", function ()
                {
                    idOficio = $(this).attr('idOficio');
                    nome = $(this).attr('nome');
                    $('#modal_delete_oficio').html("Deseja excluir este Oficio <b>\"" + nome + "\"</b> ?");
                });

                $('#modal_excluir').on('hidden.bs.modal', function ()
                {
                    window.location.replace("home");
                });

                $('#modal_btn-excluir').on("click", function ()
                {
                    var btn = $(this);
                    btn.text('loading');

                    btn.removeClass('btn-primary');
                    btn.removeClass('btn-danger');
                    btn.removeClass('btn-success');
                    btn.addClass('btn-primary');

                    $('#modal_btn-excluir').prop('disabled', true);

                    $.ajax("delete_oficio",
                            {
                                type: "POST",
                                data:
                                        {
                                            idOficio: idOficio
                                        }
                            })
                            .done(function (data)
                            {
                                $('#modal_delete_oficio').html('Oficio excluído com sucesso');

                                btn.removeClass('btn-primary');
                                btn.addClass('btn-success');
                            })
                            .fail(function ()
                            {
                                $('#modal_delete_oficio').html('Erro: ' + data);

                                btn.removeClass('btn-primary');
                                btn.addClass('btn-danger');
                            })
                            .always(function ()
                            {
                                btn.text('Excluído');
                            });
                });
                
                // Opção para ativar e desativar
                $("body").on("click", "button[name=btnAtivar]", function () {
                    var idOficio = $(this).attr('idOficio');
                    //alert(idOficio);
                    $.get('update_ativar_oficio?idOficio=' + idOficio, function (data) {
                        //alert(data);
                        if (data === "1") {
                            $('#btnAtivar_' + idOficio).addClass('btn btn-success btn-xs');
                            $('#btnAtivar_' + idOficio).removeClass('btn-danger');
                            $('#activ_' + idOficio).removeClass('glyphicon-ban-circle');
                            $('#activ_' + idOficio).addClass('glyphicon-ok');
                        } else {
                            $('#btnAtivar_' + idOficio).addClass('btn btn-danger btn-xs');
                            $('#btnAtivar_' + idOficio).removeClass('btn-success');
                            $('#activ_' + idOficio).addClass('glyphicon-ok');
                            $('#activ_' + idOficio).addClass('glyphicon-ban-circle');
                        }
                    });
                });

                $('.dataTables_filter label').addClass('pull-right');
            });
        </script>
    </body>
</html>
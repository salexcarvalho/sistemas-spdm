<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - Sistema de Apoio a Secretaria</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>

            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Módulos</h1>
                        <ol class="breadcrumb">
                            <li><i class="fa fa-folder-open"></i> Secretaria</li>
                            <li class="active"><i class="fa fa-list"></i> Listagem</li>
                        </ol>

                        <?php if (isset($warning)) : ?>
                            <div id="msg" class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong><?= $warning; ?></strong>
                            </div>
                        <?php endif ?>                          
                        <div class="row">
                            <?php if ($_SESSION['AcLiberaBtnAddMod'] == TRUE) { ?>
                                <div class="col-lg-12">
                                    <button type="submit" id="btn_novo_modulo" class="pull-right btn btn-primary btn-sm" name="btn_novo_modulo"><i class="glyphicon glyphicon-plus-sign">&nbsp;</i>Cadastrar Módulo</button>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="tbmodulos">
                                <thead>
                                    <tr>
                                        <th width="5%" style="text-align: center">ID</th>
                                        <th>Tipo</th>
                                        <th width="26%">Alias</th>
                                        <th width="8%" style="text-align: center">Ações</th>
                                        <th width="6%" style="text-align: center">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($data as $modulo) {
                                        ?>
                                        <tr>
                                            <td align="center"><?= $modulo['idModulo']; ?></td>
                                            <td><?= $modulo['Nome']; ?></td>
                                            <td><?= $modulo['Alias']; ?></td>
                                            <td align="center">
                                                <button  type="submit" id="btnEditar" class="btn btn-default btn-xs" name="btnEditar" page="<?= $PaginaAtual; ?>" idModulo="<?= $modulo['idModulo']; ?>" title="Editar Módulo" <?php if ($_SESSION['AcBtnEditMod'] == NULL) { ?>disabled<?php } ?>>
                                                    <i class="glyphicon glyphicon-edit"></i>
                                                </button>
                                                
                                                <input type="hidden" id="idModulo<?= $modulo['idModulo']; ?>" name="idModulo" value="<?= $modulo['idModulo']; ?>">
                                                <button  type="button" id="btnExcluir" class="btn btn-default btn-xs" name="btnExcluir" idModulo="<?= $modulo['idModulo']; ?>" nome="<?= $modulo['Nome']; ?>" <?php if ($_SESSION['AcBtnExcMod'] == NULL) { ?>disabled<?php } ?> data-toggle="modal" data-target="#modal_excluir" data-placement="bottom" title="Excluir Módulo" data-delay="1">
                                                    <i class="glyphicon glyphicon-trash"></i>
                                                </button>
                                            </td>
                                            <td align="center">
                                                <input type="hidden" name="idModulo" value="<?= $modulo['idModulo']; ?>">
                                                <?php
                                                if ($modulo['Status'] == "0") {
                                                    ?>
                                                    <button type="submit" id="btnAtivar_<?= $modulo['idModulo']; ?>" class="btn btn-danger btn-xs" idModulo="<?= $modulo['idModulo']; ?>" name="btnAtivar" title="Ativar Módulo" <?php if ($_SESSION['AcBtnAtivMod'] == NULL) { ?>disabled<?php } ?>>
                                                        <i id="activ_<?= $modulo['idModulo']; ?>" class="glyphicon glyphicon-ban-circle"></i>

                                                    </button>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <button type="submit" id="btnAtivar_<?= $modulo['idModulo']; ?>" class="btn btn-success btn-xs" idModulo="<?= $modulo['idModulo']; ?>" name="btnAtivar" title="Destivar Módulo" <?php if ($_SESSION['AcBtnAtivMod'] == NULL) { ?>disabled<?php } ?>>
                                                        <i id="activ_<?= $modulo['idModulo']; ?>" class="glyphicon glyphicon-ok"></i>

                                                    </button>
                                                    <?php
                                                }
                                                ?>

                                            </td>
                                            <?php
                                        }
                                        ?>
                                </tbody>    
                            </table>
                            <?php include(kohana::find_file('views/paginator', 'paginacao', 'php')) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="modal_excluir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Confirmar Exclusão</h4>
                    </div>
                    <div class="modal-body" id="modal_delete_modulo">Deseja excluir este módulo?</div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        <button type="button" class="btn btn-primary" id="modal_btn-excluir">Excluir</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal -->
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>

        <script>
            jQuery(document).ready(function () {
                // Remove seleção de ativo no menu.
                $('.nav li').removeClass('active');
                $('#side-menu li').removeClass('active');
                // Ativa botão no menu.
                $('#li-gestaoP ul').addClass('collapse in');
                $('#li-modulo a').addClass('active');
                $('#li-modulo').addClass('active');
                $('#li-modulo a').addClass('collapse in');
                
                // Datatables
                $('#tbmodulos').dataTable(
                        {
                            "aoColumnDefs": [{"bSortable": false, "aTargets": [2, 3, 4]}],
                            "paging": false,
                            "info": false,
                            "aaSorting": [[0, 'desc']],
                            "bFilter": false,
                            "oLanguage":
                                    {
                                        "sSearch": " Buscar: "
                                    }
                        });
                        
                // abre o modal para cadastro
                $('button[name=btn_novo_modulo]').click(function () {
                    var idModulo = '';
                    var options = {
                        url: "cadastro_modulo?idModulo=" + idModulo,
                        title: 'Cadastrar Módulo',
                        size: eModal.size.md,
                        buttons: [
                            {text: 'GRAVAR', style: 'primary', close: false, type: 'submit', form: 'modulos'},
                            {text: 'FECHAR', style: 'danger', close: true}
                        ]
                    };
                    eModal.ajax(options);
                });
                
                //abre o modal de edição
                $('button[name=btnEditar]').click(function () {
                    var idModulo = $(this).attr('idModulo');
                    var page = $(this).attr('page');
                    var options = {
                        url: "editar_modulo?idModulo=" + idModulo +"&page="+ page,
                        title: 'Editar Módulo',
                        size: eModal.size.md,
                        buttons: [
                            {text: 'SALVAR', style: 'primary', close: false, type: 'submit', form: 'modulos'},
                            {text: 'FECHAR', style: 'danger', close: true}
                        ]
                    };
                    eModal.ajax(options);
                });
                
                // Opção usada na exclusão        
                var idModulo;
                var nome;
                $('button[name=btnExcluir]').on("click", function ()
                {
                    idModulo = $(this).attr('idModulo');
                    nome = $(this).attr('nome');
                    $('#modal_delete_modulo').html("Deseja excluir este Módulo <b>\"" + nome + "\"</b> ?");
                });

                $('#modal_excluir').on('hidden.bs.modal', function ()
                {
                    window.location.replace("home");
                });

                $('#modal_btn-excluir').on("click", function ()
                {
                    var btn = $(this);
                    btn.text('loading');

                    btn.removeClass('btn-primary');
                    btn.removeClass('btn-danger');
                    btn.removeClass('btn-success');
                    btn.addClass('btn-primary');

                    $('#modal_btn-excluir').prop('disabled', true);

                    $.ajax("delete_modulo",
                            {
                                type: "POST",
                                data:
                                        {
                                            idModulo: idModulo
                                        }
                            })
                            .done(function (data)
                            {
                                $('#modal_delete_modulo').html('Módulo excluído com sucesso');

                                btn.removeClass('btn-primary');
                                btn.addClass('btn-success');
                            })
                            .fail(function ()
                            {
                                $('#modal_delete_modulo').html('Erro: ' + data);

                                btn.removeClass('btn-primary');
                                btn.addClass('btn-danger');
                            })
                            .always(function ()
                            {
                                btn.text('Excluído');
                            });
                });
                
                // Opção para ativar e desativar
                $("body").on("click", "button[name=btnAtivar]", function () {
                    var idModulo = $(this).attr('idModulo');
                    //alert(txtUsuario);
                    $.get('update_ativar_modulo?idModulo=' + idModulo, function (data) {

                        if (data === "1") {
                            $('#btnAtivar_' + idModulo).addClass('btn btn-success btn-xs');
                            $('#btnAtivar_' + idModulo).removeClass('btn-danger');
                            $('#activ_' + idModulo).removeClass('glyphicon-ban-circle');
                            $('#activ_' + idModulo).addClass('glyphicon-ok');
                        } else {
                            $('#btnAtivar_' + idModulo).addClass('btn btn-danger btn-xs');
                            $('#btnAtivar_' + idModulo).removeClass('btn-success');
                            $('#activ_' + idModulo).addClass('glyphicon-ok');
                            $('#activ_' + idModulo).addClass('glyphicon-ban-circle');
                        }
                    });
                });

                $('.dataTables_filter label').addClass('pull-right');
            });
        </script>
    </body>
</html>
<?php if ($_SESSION['AcLiberaBtnAddTpDoc'] == true) { ?>
    <form class="form-horizontal" role="form" method="POST" action="insert_tipodocumentos" name="tipodocumentos" id="tipodocumentos">
        <div class="form-group">
            <div class="col-lg-12">
                <div class="form-group">
                    <label for="tipo" class="col-lg-3 control-label">Nome do tipo</label>
                    <div class="col-lg-9">
                        <input type="text" min="0" class="form-control" id="Nome" name="Nome" placeholder="Informe o tipo">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3 control-label">Sigla do tipo</label>
                    <div class="col-lg-9">
                        <input type="text" min="0" class="form-control" id="Alias" name="Alias" placeholder="Informe a sigla do tipo">
                    </div>
                </div>
                <div class="form-group">
                    <label for="alias" class="col-lg-3 control-label">Módulo</label>
                    <div class="col-lg-9">
                        <select class="form-control" id='idModulo' name='idModulo' required>
                            <option value="" selected='selected'>Selecione uma Opção</option>
                                <?php foreach ($modulos as $modulo) { ?>
                                <option value="<?= $modulo['idModulo']; ?>"><?= $modulo['Nome']; ?></option>
                                <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="col-lg-offset-4 col-lg-8">
                    <div class="form-group"> 
                        <label class="radio-inline">
                            <input required type="radio" name="status" id="inativo" value="1">
                            Ativo
                        </label>
                        <label class="radio-inline">
                            <input required type="radio" name="status" id="inativo" value="0">
                            Inativo
                        </label>
                    </div>
                </div>
            </div>
        </div>        
    </form>
<?php } else { ?>
    <h1 class="page-header"> Voce não possue acesso a está pagina, consulte o administrador.</h1>
<?php } ?>
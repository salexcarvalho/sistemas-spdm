<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - ACESSO</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <style>
            .pagination
            {
                float: right !important;
            }
        </style>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Lista de Perfis</h1>
                        <ol class="breadcrumb">
                            <li><i class="fa fa-lock"></i>  Controle de Perfis</li>
                            <li class="active"><i class="fa fa-cogs"></i>  Lista de Perfis</li>
                        </ol>

                        <?php if (isset($warning)) : ?>
                            <div id="msg" class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong><?= $warning; ?></strong>
                            </div>
                        <?php endif ?>
                        <div class="row">
                            <a href="add_perfil" class="col-lg-12">
                                <button type="submit" id="btn_novo_perfil" class="pull-right btn btn-primary btn-md" name="btn_novo_perfil"><i class="glyphicon glyphicon-plus-sign"></i>  Adicionar Perfil</button>
                            </a>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="perfis">
                                <thead>
                                    <tr>
                                        <th width="5%" style="text-align: center">#</th>
                                        <th>Alias do perfil</th>
                                        <th>Descrição do perfil</th>
                                        <th width="9%" style="text-align: center">Perfis</th>
                                        <th width="6%" style="text-align: center">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($data as $perfil) {
                                        ?>
                                        <tr>
                                            <td align="center"><?= $perfil['idPerfil']; ?></td>
                                            <td><?= $perfil['nmPerfil']; ?></td>
                                            <td><?= $perfil['dsPerfil']; ?></td>
                                            <td align="center">
                                                <form style="display: inline-block;" role="form" method="GET" action="visualizar_perfil">
                                                    <input hidden id="page" name="page" value="<?= $PaginaAtual; ?>">
                                                    <input type="hidden" id="idPerfil_<?= $perfil['idPerfil']; ?>" name="idPerfil" value="<?= $perfil['idPerfil']; ?>">
                                                    <button type="submit" id="btnVisualizar" class="btn btn-default btn-xs" name="btnVisualizar" title="Visualizar Perfil">
                                                        <i class="glyphicon glyphicon-file"></i>
                                                    </button>
                                                </form>
                                                <form style="display: inline-block;" role="form" method="GET" action="editar_dados_perfil">
                                                    <input hidden id="page" name="page" value="<?= $PaginaAtual; ?>">
                                                    <input type="hidden" id="idPerfil_<?= $perfil['idPerfil']; ?>" name="idPerfil" value="<?= $perfil['idPerfil']; ?>">
                                                    <button type="submit" id="btnEdit" class="btn btn-default btn-xs" name="btnEdit" title="Editar Perfil">
                                                        <i class="glyphicon glyphicon-edit"></i>
                                                    </button>
                                                </form>
                                                    <button type="submit" id="btnExcluir" class="btn btn-default btn-xs" nmPerfil="<?= $perfil['nmPerfil']; ?>" idPerfil="<?= $perfil['idPerfil']; ?>" name="btnExcluir" title="Excluir Perfil"  data-toggle="modal" data-target="#modal_excluir" data-placement="bottom" data-delay="1">
                                                        <i class="glyphicon glyphicon-trash"></i>
                                                    </button>
                                            </td>
                                            <td align="center">
                                                <input type="hidden" id="idPerfil" name="idPerfil" value="<?= $perfil['idPerfil']; ?>">
                                                <?php
                                                if ($perfil['ativo'] == "N") {
                                                    ?>
                                                    <button type="submit" id="btnAtivar_<?= $perfil['idPerfil']; ?>" class="btn btn-danger btn-xs" idPerfil="<?= $perfil['idPerfil']; ?>" name="btnAtivar" title="Ativar Perfil">
                                                        <i id="activ_<?= $perfil['idPerfil']; ?>" class="glyphicon glyphicon-ban-circle"></i>
                                                    </button>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <button type="submit" id="btnAtivar_<?= $perfil['idPerfil']; ?>" class="btn btn-success btn-xs" idPerfil="<?= $perfil['idPerfil']; ?>" name="btnAtivar" title="Destivar Perfil">
                                                        <i id="activ_<?= $perfil['idPerfil']; ?>" class="glyphicon glyphicon-ok"></i>
                                                    </button>
                                                    <?php
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                            <?php include(kohana::find_file('views/paginator', 'paginacao', 'php')) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- Modal -->
        <div class="modal fade" id="modal_excluir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Excluir Perfil</h4>
                    </div>
                    <div class="modal-body" id="modal_delete_usuario">Deseja excluir este perfil?</div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        <button type="button" class="btn btn-primary" id="modal_btn-excluir">Excluir</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal --> 
        
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>

        <script>
            jQuery(document).ready(function () {
                // Remove seleção de ativo no menu.
                $('.nav li').removeClass('active');
                $('#side-menu li').removeClass('active');
                
                // Datatables
                $('#perfis').dataTable(
                        {
                            "bJQueryUI": false,
                            "aoColumnDefs": [{"bSortable": false, "aTargets": [0, 1, 2, 3, 4]}],
                            "paging": false,
                            "info": false,
                            "bFilter": false,
                            "oLanguage":
                                    {
                                        "sSearch": " Buscar: "
                                    }

                        });
                
                // Opção usada na exclusão
                var idPerfil;
                var nmPerfil;
                $('body').on("click", 'button[name=btnExcluir]', function ()
                {
                    idPerfil = $(this).attr('idPerfil');
                    nmPerfil = $(this).attr('nmPerfil');
                    $('#modal_delete_perfil').html("Deseja excluir o Perfil <b>\"" + nmPerfil + "\"</b> ?");
                });
                
                $('#modal_excluir').on('hidden.bs.modal', function ()
                {
                    window.location.replace("lista_perfis");
                });

                $('#modal_btn-excluir').on("click", function ()
                {
                    var btn = $(this);
                    btn.text('loading');

                    btn.removeClass('btn-primary');
                    btn.removeClass('btn-danger');
                    btn.removeClass('btn-success');
                    btn.addClass('btn-primary');

                    $('#modal_btn-excluir').prop('disabled', true);

                    $.ajax("excluir_perfil",
                            {
                                type: "GET",
                                data:
                                        {
                                            idPerfil: idPerfil
                                        }
                            })
                            .done(function (data)
                            {
                                $('#modal_delete_perfil').html('Perfil excluído com sucesso');

                                btn.removeClass('btn-primary');
                                btn.addClass('btn-success');
                            })
                            .fail(function ()
                            {
                                $('#modal_delete_perfil').html('Erro: ' + data);

                                btn.removeClass('btn-primary');
                                btn.addClass('btn-danger');
                            })
                            .always(function ()
                            {
                                btn.text('Excluído');
                            });
                });
                
                // Opção para ativar e desativar
                $("body").on("click", "button[name=btnAtivar]", function () {
                    idPerfil = $(this).attr('idPerfil');
                    //alert(idAcao);
                    $.get('update_ativar_perfil?idPerfil=' + idPerfil, function (data) {

                        if (data === "1") {
                            $('#btnAtivar_' + idPerfil).addClass('btn btn-success btn-xs');
                            $('#btnAtivar_' + idPerfil).removeClass('btn-danger');
                            $('#activ_' + idPerfil).removeClass('glyphicon-ban-circle');
                            $('#activ_' + idPerfil).addClass('glyphicon-ok');
                        } else {
                            $('#btnAtivar_' + idPerfil).addClass('btn btn-danger btn-xs');
                            $('#btnAtivar_' + idPerfil).removeClass('btn-success');
                            $('#activ_' + idPerfil).addClass('glyphicon-ok');
                            $('#activ_' + idPerfil).addClass('glyphicon-ban-circle');
                        }
                    });
                });

                $('.dataTables_filter label').addClass('pull-right');
            });
        </script>

    </body>
</html>
<form id="novocontroller" name="novocontroller" class="form-horizontal" role="form" method="POST" action="insert_controller">
    <div class="form-group">
        <div class="col-lg-12">
            <div class="form-group">
                <label class="col-lg-2 control-label">Nome</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" id="nmController" name="nmController" placeholder="Digite o alias do controller" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-2 control-label">Descrição</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" id="dsController" name="dsController" placeholder="Digite a descrição do controller" required>
                </div>
            </div>
        </div>
    </div>
</form>
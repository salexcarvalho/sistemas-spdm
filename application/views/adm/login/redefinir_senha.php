<!DOCTYPE html>
<?php Funcoes::carrega(); ?>
<html lang="pt_br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title><?= $_SESSION['company_name'] ?> - ACESSO</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
    </head>
    <body>
        <?php $pag = (isset($_GET['pag'])) ? $_GET['pag'] : ""; ?>
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div id="logo" class="login-panel-logo" style="text-align: center;">
                        <img src="<?= URL::base(); ?>upload/config/<?= $_SESSION['logo_admin'] ?>" alt="Logo" width="350">
                    </div>
                    <div class="login-panel panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Redefinição de Senha</h3>
                        </div>
                        <div class="panel-body">
                            <form role="form" method="POST" action="resetar_senha">
                                <fieldset>
                                    <div class="form-group">
                                        <?php if (isset($warning)) : ?>
                                            <div id="msg" class="alert alert-<?= $estilo; ?> alert-dismissible" role="alert">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <strong><?= $warning; ?></strong>
                                            </div>
                                        <?php endif ?>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="nmUsuarioEmail" name="nmUsuarioEmail" placeholder="Digite seu usuário ou email de acesso" autofocus="">
                                    </div>
                                    <input class="form-control" type="hidden" name="url" value="<?= $pag; ?>" />
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-lg btn-primary btn-block">Redefinir Senha</button>
                                    </div>
                                    <span style="font-size:12px; font-style: italic;" class="col-rm-1"><?= VERSAO ?></span>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
<!DOCTYPE html>
<?php Funcoes::carrega(); ?>
<html lang="pt_br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title><?= $_SESSION['company_name'] ?> - ACESSO</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
    </head>
    <body>
        <?php $pag = (isset($_GET['pag'])) ? $_GET['pag'] : ""; ?>
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div id="logo" class="login-panel-logo" style="text-align: center;">
                        <img src="<?= URL::base(); ?>upload/config/<?= $_SESSION['logo_admin'] ?>" alt="Logo" width="350">
                    </div>
                    <div class="login-panel panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Alteração da Senha</h3>
                        </div>
                        <div class="panel-body">
                            <form role="form" method="POST" action="resetar">
                                <fieldset>
                                    <div class="form-group">
                                        <?php if (isset($warning)) : ?>
                                            <div id="msg" class="alert alert-<?= $estilo; ?> alert-dismissible" role="alert">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <strong><?= $warning; ?></strong>
                                            </div>
                                        <?php endif ?>
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control" id="txtSenha" name="txtSenha" placeholder="Digite a nova senha" required>
                                    </div>
                                    <div class="form-group">
                                        <input type="password" class="form-control" id="confirmatxtSenha" name="confirmatxtSenha" placeholder="Confirme a nova senha" onblur="validatePassword()" required>
                                        <div id="progressbar">
                                            <div class="progress-label"></div>
                                        </div>
                                    </div>
                                    <input class="form-control" type="hidden" name="Email" value="<?= $Email; ?>" />
                                    <input class="form-control" type="hidden" name="Token" value="<?= $Token; ?>" />
                                    <input class="form-control" type="hidden" name="url" value="<?= $pag; ?>" />
                                    <div class="form-group">
                                        <button id="resetsenha" type="submit" class="btn btn-lg btn-primary btn-block">Redefinir Senha</button>
                                    </div>
                                    <span style="font-size:12px; font-style: italic;" class="col-rm-1"><?= VERSAO ?></span>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>

<script>
    function validatePassword() {
        var password = $("#txtSenha").val(), confirm_password = $("#confirmatxtSenha").val();
        if (password !== confirm_password) {
            $(".ui-progressbar-value").addClass('ui-widget-header-danger').removeClass('ui-widget-header-warning ui-widget-header-success');
            $(".progress-label").html('AS SENHAS NÃO SÃO IGUAIS!');
            $("#resetsenha").addClass('disabled');
        } else {
            $("#resetsenha").removeClass('disabled');
        }
    }
</script>

<script src="https://code.jquery.com/ui/1.10.0/jquery-ui.js"></script>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.10.0/themes/base/jquery-ui.css" />
<script src="<?= URL::base(); ?>theme/backend/js/plugins/verificasenha/jquery.complexify.banlist.js"></script>
<script src="<?= URL::base(); ?>theme/backend/js/plugins/verificasenha/jquery.complexify.js"></script>
<link href="<?= URL::base(); ?>theme/backend/css/custom.css" rel="stylesheet">
<script type="text/javascript">
$(function () {
$("#txtSenha").complexify({},
        function (valid, complexity) {
            $("#progressbar").progressbar({
                value: complexity,
            });
            if (complexity < '6') {
                $(".ui-progressbar-value").addClass('ui-widget-header-danger').removeClass('ui-widget-header-warning ui-widget-header-success');
                $(".progress-label").html('Esta senha não pode ser usada!');
                //alert(complexity);
            } else if (complexity < '10') {
                $(".ui-progressbar-value").addClass('ui-widget-header-danger').removeClass('ui-widget-header-warning ui-widget-header-success');
                $(".progress-label").html('Nível de Segurança - Fraca!');
            } else if (complexity < '25') {
                $(".ui-progressbar-value").addClass('ui-widget-header-warning').removeClass('ui-widget-header-danger ui-widget-header-success');
                $(".progress-label").html('Nível de Segurança - Fraca!');
            } else if (complexity < '50') {
                $(".ui-progressbar-value").addClass('ui-widget-header-warning').removeClass('ui-widget-header-danger ui-widget-header-success');
                $(".progress-label").html('Nível de Segurança - Médio!');
            } else if (complexity < '80') {
                $(".ui-progressbar-value").addClass('ui-widget-header-success').removeClass('ui-widget-header-danger ui-widget-header-warning');
                $(".progress-label").html('Nível de Segurança - Médio Alto');
            } else {
                $(".ui-progressbar-value").addClass('ui-widget-header-success').removeClass('ui-widget-header-danger ui-widget-header-warning');
                $(".progress-label").html('Nível de Segurança - Forte');
            }
        });
});
</script>
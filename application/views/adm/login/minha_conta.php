<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - ACESSOM</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <script>
            jQuery(document).ready(function () {
                // Remove seleção de ativo no menu.
                $('.nav li').removeClass('active');
                $('#side-menu li').removeClass('active');
            });
        </script>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
            .centered{
                margin : auto;
            }
        </style>
    </head>
    <body>
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-header">Minha conta</h1>
                        <ol class="breadcrumb">
                            <li><i class="fa fa-user"></i> Seus dados</li>
                            <li class="active"><i class="fa fa-list"></i> Meus dados</li>
                        </ol>

                        <?php if (isset($warning)) : ?>
                            <div id="msg" class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong><?= $warning; ?></strong>
                            </div>
                        <?php endif ?>
                        <form class="form-horizontal" role="form">
                            <div class="form-group">
                                <div class="col-lg-9">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <span>Seus dados</span>
                                        </div>
                                        <div class="panel-body">   
                                            <div class="form-horizontal">    
                                                <div class="form-group">
                                                    <label for="txtNome" class="col-lg-3 control-label">Nome</label>
                                                    <div class="col-lg-9">
                                                        <input type="text" class="form-control" id="txtNome" name="txtNome" value="<?= $usuario[0]['nmNome']; ?>" disabled>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="txtUsuario" class="col-lg-3 control-label">Usuário</label>
                                                    <div class="col-lg-9">
                                                        <input type="text" class="form-control" id="txtUsuario" name="txtUsuario" value="<?= $usuario[0]['nmUsuario']; ?>" disabled>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-3 control-label">Perfil de Administração</label>
                                                    <div class="col-lg-9">
                                                        <select class="form-control" id="idPerfil" name="idPerfil" disabled>
                                                            <option value="">Selecione uma opção</option>
                                                            <?php
                                                            foreach ($perfil as $perfis) {
                                                                ?>
                                                                <option <?= ($usuario[0]['perfil'] == $perfis['idPerfil']) ? 'selected' : ''; ?> value="<?= $perfis['idPerfil']; ?>"><?= $perfis['nmPerfil']; ?></option>
                                                                <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-lg-3 control-label">Setor ou Departamento</label>
                                                    <div class="col-lg-9">
                                                        <select class="form-control" id="idTipoSetor" name="idTipoSetor" disabled>
                                                            <option value="">Selecione uma opção</option>
                                                            <?php
                                                            foreach ($setor as $setores) {
                                                                ?>
                                                                <option <?= ($setores['idTipoSetor'] == $usuario[0]['idTipoSetor']) ? 'selected' : ''; ?> value="<?= $setores['idTipoSetor']; ?>"><?= $setores['Setor']; ?></option>
                                                                <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="txtEmail" class="col-lg-3 control-label">Email</label>
                                                    <div class="col-lg-9">
                                                        <input type="email" class="form-control" id="txtEmail" name="txtEmail" value="<?= $usuario[0]['email']; ?>" disabled>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="txtEmail" class="col-lg-3 control-label">Nº de Logins</label>
                                                    <div class="col-lg-9">
                                                        <input type="text" class="form-control" id="txtlogin" name="txtlogin" value="<?= $usuario[0]['logins']; ?>" disabled>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="txtEmail" class="col-lg-3 control-label">Cadastrado</label>
                                                    <div class="col-lg-9">
                                                        <input type="data" class="form-control" id="dtCadastro" name="dtCadastro" value="<?= strftime('%d/%m/%Y ás %H:%M', strtotime($usuario[0]["dtCadastro"])); ?>" disabled>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="txtEmail" class="col-lg-3 control-label">Ultimo Login</label>
                                                    <div class="col-lg-9">
                                                        <input type="data" class="form-control" id="ultimoLogin" name="ultimoLogin" value="<?= strftime('%d/%m/%Y ás %H:%M', strtotime($usuario[0]["ultimoLogin"])); ?>" disabled>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>

    </body>
</html>
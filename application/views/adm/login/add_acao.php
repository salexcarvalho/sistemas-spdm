<form id="novaacao" name="novaacao" class="form-horizontal" role="form" method="POST" action="insert_acao">
    <div class="form-group">
        <div class="col-lg-12">
            <div class="form-group">
                <label for="txtNome" class="col-lg-2 control-label">Nome</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" id="nmAcao" name="nmAcao" placeholder="Digite o alias da ação, como está no controller" required>
                </div>
            </div>
            <div class="form-group">
                <label for="txtUsuario" class="col-lg-2 control-label">Descrição</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" id="dsAcao" name="dsAcao" placeholder="Digite a descrição da ação" required>
                </div>
            </div>
            <div class="form-group">
                <label for="idCurto" class="col-lg-2 control-label">ID Curto</label>
                <div class="col-lg-10">
                    <input type="text" size="20" maxlength="20" class="form-control" id="idCurto" name="idCurto" placeholder="Digite o ID curto da ação" required>
                    <span id="resultado" style="margin-top:3px;"></span>
                </div>
            </div>                                            
            <div class="form-group">
                <label class="col-lg-2 control-label">Controller</label>
                <div class="col-lg-10">
                    <select class="form-control" id="nmController" name="nmController" required>
                        <option value="">Selecione uma opção</option>
                        <?php
                        foreach ($controller as $controllers) {
                            ?>
                            <option value="<?= $controllers['nmController']; ?>"><?= $controllers['nmController']; ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
        </div>
    </div>
</form>
<script>
    // verifica se existe o idCurt no banco
    $("input[name='idCurto']").on('blur', function () {
        var idCurto = $(this).val();
        //alert(idCurto);
        if (idCurto !== "") {
            $.get('verifica_idcurto?idCurto=' + idCurto, function (data) {

                if (data === "") {
                    $('#resultado').addClass('btn btn-success btn-xs');
                    $('#resultado').removeClass('btn-danger');
                    $('#resultado').html("ID CURTO VÁLIDO");
                } else {
                    $('#resultado').addClass('btn btn-danger btn-xs');
                    $('#resultado').removeClass('btn-success');
                    $('#resultado').html("ID CURTO INVÁLIDO");

                }
            });
        }
    });
</script>
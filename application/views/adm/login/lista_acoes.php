<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - ACESSO</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <style>
            .pagination
            {
                float: right !important;
            }
        </style>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Lista de Ações</h1>
                        <ol class="breadcrumb">
                            <li><i class="fa fa-lock"></i>  Controle de Ações</li>
                            <li class="active"><i class="fa fa-cogs"></i>  Lista de ações</li>
                        </ol>

                        <?php if (isset($warning)) : ?>
                            <div id="msg" class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong><?= $warning; ?></strong>
                            </div>
                        <?php endif ?>
                        <div class="row">
                            <div class="col-lg-12">
                                <button type="submit" id="btn_novo_acao" class="pull-right btn btn-primary btn-md" name="btn_novo_acao"><i class="glyphicon glyphicon-plus-sign"></i>  Adicionar Ação</button>
                                <button type="submit" id="btn_novo_controller" class="pull-right btn btn-primary btn-md" name="btn_novo_controller" style="margin-right:10px;"><i class="glyphicon glyphicon-plus-sign"></i>  Adicionar Controller</button>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="acoes">
                                <thead>
                                    <tr>
                                        <th width="5%" style="text-align: center">#</th>
                                        <th>Alias da ação</th>
                                        <th>Descrição da ação</th>
                                        <th>ID Curto</th>
                                        <th>Controller</th>
                                        <th width="9%" style="text-align: center">Ações</th>
                                        <th width="5%" style="text-align: center">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($data as $acao) { ?>
                                        <tr>
                                            <td align="center"><?= $acao['idAcao']; ?></td>
                                            <td><?= $acao['nmAcao']; ?></td>
                                            <td><?= $acao['dsAcao']; ?></td>
                                            <td><?= $acao['idCurto']; ?></td>
                                            <td><?= $acao['Controller']; ?></td>
                                            <td align="center">
                                                <button type="submit" id="btnEditar" class="btn btn-default btn-xs" page="<?= $PaginaAtual; ?>" idAcao="<?= $acao['idAcao']; ?>" name="btnEditar" title="Editar Ação">
                                                    <i class="glyphicon glyphicon-edit"></i>
                                                </button>
                                                    <button type="submit" id="btnExcluir" class="btn btn-default btn-xs" nmAcao="<?= $acao['nmAcao']; ?>" idAcao="<?= $acao['idAcao']; ?>" name="btnExcluir"  data-toggle="modal" data-target="#modal_excluir" data-placement="bottom" data-delay="1" title="Excluir Ação">
                                                        <i class="glyphicon glyphicon-trash"></i>
                                                    </button>
                                            </td>
                                            <td align="center">
                                                <input type="hidden" id="idAcao" name="idAcao" value="<?= $acao['idAcao']; ?>">
                                                <?php
                                                if ($acao['ativo'] == "N") {
                                                    ?>
                                                    <button type="submit" id="btnAtivar_<?= $acao['idAcao']; ?>" class="btn btn-danger btn-xs" idAcao="<?= $acao['idAcao']; ?>" name="btnAtivar" title="Ativar Ação">
                                                        <i id="activ_<?= $acao['idAcao']; ?>" class="glyphicon glyphicon-ban-circle"></i>
                                                    </button>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <button type="submit" id="btnAtivar_<?= $acao['idAcao']; ?>" class="btn btn-success btn-xs" idAcao="<?= $acao['idAcao']; ?>" name="btnAtivar" title="Destivar Ação">
                                                        <i id="activ_<?= $acao['idAcao']; ?>" class="glyphicon glyphicon-ok"></i>
                                                    </button>
                                                    <?php
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                            <?php include(kohana::find_file('views/paginator', 'paginacao', 'php')) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- Modal -->
        <div class="modal fade" id="modal_excluir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Excluir Ação</h4>
                    </div>
                    <div class="modal-body" id="modal_delete_acao">Deseja excluir esta ação?</div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        <button type="button" class="btn btn-primary" id="modal_btn-excluir">Excluir</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal -->        
        
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>

        <script>
            jQuery(document).ready(function () {
                // Remove seleção de ativo no menu.
                $('.nav li').removeClass('active');
                $('#side-menu li').removeClass('active');

                // Datatables
                $('#acoes').dataTable(
                        {
                            "bJQueryUI": false,
                            "aoColumnDefs": [{"bSortable": false, "aTargets": [0, 1, 2, 3, 4, 5, 6]}],
                            "paging": false,
                            "info": false,
                            "bFilter": false,
                            "oLanguage":
                                    {
                                        "sSearch": " Buscar: "
                                    }
                        });

                // abre o modal para cadastro controller
                $('button[name=btn_novo_controller]').click(function () {
                    var options = {
                        url: "add_controller",
                        title: 'Cadastrar Controller',
                        size: eModal.size.md,
                        buttons: [
                            {text: 'GRAVAR', style: 'primary', close: false, type: 'submit', form: 'novocontroller'},
                            {text: 'FECHAR', style: 'danger', close: true}
                        ]
                    };
                    eModal.ajax(options);
                });

                // abre o modal para cadastro ação
                $('button[name=btn_novo_acao]').click(function () {
                    var options = {
                        url: "add_acao",
                        title: 'Cadastrar Ação',
                        size: eModal.size.md,
                        buttons: [
                            {text: 'GRAVAR', style: 'primary', close: false, type: 'submit', form: 'novaacao'},
                            {text: 'FECHAR', style: 'danger', close: true}
                        ]
                    };
                    eModal.ajax(options);
                });

                //abre o modal de edição da ação
                $('button[name=btnEditar]').click(function () {
                    var idAcao = $(this).attr('idAcao');
                    var page = $(this).attr('page');
                    var options = {
                        url: "editar_dados_acao?idAcao=" + idAcao + "&page=" + page,
                        title: 'Editar Ação',
                        size: eModal.size.md,
                        buttons: [
                            {text: 'SALVAR', style: 'primary', close: false, type: 'submit', form: 'novaacao'},
                            {text: 'FECHAR', style: 'danger', close: true}
                        ]
                    };
                    eModal.ajax(options);
                });

                // Opção usada na exclusão
                var idAcao;
                var nmAcao;
                $('body').on("click", 'button[name=btnExcluir]', function ()
                {
                    idAcao = $(this).attr('idAcao'); 
                    nmAcao = $(this).attr('nmAcao');
                    $('#modal_delete_acao').html("Deseja excluir a Ação <b>\"" + nmAcao + "\"</b> ?");
                });
                
                $('#modal_excluir').on('hidden.bs.modal', function ()
                {
                    var page = $(this).attr('page');
                    window.location.replace("lista_acoes?page=" + page);
                });

                $('#modal_btn-excluir').on("click", function ()
                {
                    var btn = $(this);
                    btn.text('loading');

                    btn.removeClass('btn-primary');
                    btn.removeClass('btn-danger');
                    btn.removeClass('btn-success');
                    btn.addClass('btn-primary');

                    $('#modal_btn-excluir').prop('disabled', true);

                    $.ajax("excluir_acao",
                            {
                                type: "GET",
                                data:
                                        {
                                            idAcao: idAcao
                                        }
                            })
                            .done(function (data)
                            {
                                $('#modal_delete_acao').html('Ação excluída com sucesso');

                                btn.removeClass('btn-primary');
                                btn.addClass('btn-success');
                            })
                            .fail(function ()
                            {
                                $('#modal_delete_acao').html('Erro: ' + data);

                                btn.removeClass('btn-primary');
                                btn.addClass('btn-danger');
                            })
                            .always(function ()
                            {
                                btn.text('Excluído');
                            });
                });

                // Opção para ativar e desativar
                $("body").on("click", "button[name=btnAtivar]", function () {
                    idAcao = $(this).attr('idAcao');
                    //alert(idAcao);
                    $.get('update_ativar_acao?idAcao=' + idAcao, function (data) {

                        if (data === "1") {
                            $('#btnAtivar_' + idAcao).addClass('btn btn-success btn-xs');
                            $('#btnAtivar_' + idAcao).removeClass('btn-danger');
                            $('#activ_' + idAcao).removeClass('glyphicon-ban-circle');
                            $('#activ_' + idAcao).addClass('glyphicon-ok');
                        } else {
                            $('#btnAtivar_' + idAcao).addClass('btn btn-danger btn-xs');
                            $('#btnAtivar_' + idAcao).removeClass('btn-success');
                            $('#activ_' + idAcao).addClass('glyphicon-ok');
                            $('#activ_' + idAcao).addClass('glyphicon-ban-circle');
                        }
                    });
                });

                $('.dataTables_filter label').addClass('pull-right');
            });
        </script>

    </body>
</html>
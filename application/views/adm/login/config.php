<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - ACESSO</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <script>
            jQuery(document).ready(function () {
                // Remove seleção de ativo no menu.
                $('.nav li').removeClass('active');
                $('#side-menu li').removeClass('active');
                // Ativa botão no menu.
                $('#li-configuracoes a').addClass('active');
            });
        </script>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
            .centered{
                margin : auto;
            }
        </style>
    </head>
    <body>
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-header">Configurações do Sistema</h1>
                        <ol class="breadcrumb">
                            <li class="active"><i class="fa fa-cog"></i> Dados</li>
                        </ol>

                        <?php if (isset($warning)) : ?>
                            <div id="msg" class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong><?= $warning; ?></strong>
                            </div>
                        <?php endif ?>

                        <form class="form-horizontal" role="form" method="POST" action="<?= URL::base(); ?>dashboard/configuracoes/atualizar" enctype="multipart/form-data">
                            <div class="form-group">
                                <div class="col-lg-9">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <span>Informações do Sistema</span>
                                        </div>
                                        <div class="panel-body">    
                                            <div class="form-group">
                                                <label for="txtNome" class="col-lg-3 control-label">Nome do Sistema*</label>
                                                <div class="col-lg-9">
                                                    <input type="text" class="form-control" id="company_name" name="company_name" value="<?= $config[0]['company_name']; ?>" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="txtNome" class="col-lg-3 control-label">Nome Legal*</label>
                                                <div class="col-lg-9">
                                                    <input type="text" class="form-control" id="company_legal_name" name="company_legal_name" value="<?= $config[0]['company_legal_name']; ?>" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="txtNome" class="col-lg-3 control-label">Contato</label>
                                                <div class="col-lg-9">
                                                    <input type="text" class="form-control" id="contact_person" name="contact_person" value="<?= $config[0]['contact_person']; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="txtNome" class="col-lg-3 control-label">Endereço da Empresa*</label>
                                                <div class="col-lg-9">
                                                    <input type="text" class="form-control" id="company_address" name="company_address" value="<?= $config[0]['company_address']; ?>" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="txtNome" class="col-lg-3 control-label">CEP</label>
                                                <div class="col-lg-9">
                                                    <input type="text" class="form-control" id="company_cep" name="company_cep" value="<?= $config[0]['company_cep']; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="txtNome" class="col-lg-3 control-label">Cidade</label>
                                                <div class="col-lg-9">
                                                    <input type="text" class="form-control" id="company_city" name="company_city" value="<?= $config[0]['company_city']; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="txtNome" class="col-lg-3 control-label">Estado</label>
                                                <div class="col-lg-9">
                                                    <input type="text" class="form-control" id="company_state" name="company_state" value="<?= $config[0]['company_state']; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="txtNome" class="col-lg-3 control-label">Email da empresa</label>
                                                <div class="col-lg-9">
                                                    <input type="text" class="form-control" id="company_email" name="company_email" value="<?= $config[0]['company_email']; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="txtNome" class="col-lg-3 control-label">Telefone</label>
                                                <div class="col-lg-9">
                                                    <input type="text" class="form-control" id="company_phone" name="company_phone" value="<?= $config[0]['company_phone']; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="txtNome" class="col-lg-3 control-label">Celular</label>
                                                <div class="col-lg-9">
                                                    <input type="text" class="form-control" id="company_cellphone" name="company_cellphone" value="<?= $config[0]['company_cellphone']; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="txtNome" class="col-lg-3 control-label">URL da empresa</label>
                                                <div class="col-lg-9">
                                                    <input type="text" class="form-control" id="company_url" name="company_url" value="<?= $config[0]['company_url']; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="logo_admin" class="col-lg-3 control-label">Logo Interna</label>
                                                <div class="col-lg-9">
                                                    <input type="file" class="form-control" id="logo_admin" name="logo_admin" >
                                                </div>
                                                <div class="col-lg-3"></div>
                                                <div class="col-lg-9">
                                                    <br>
                                                    <img src="<?= URL::base(); ?>upload/config/<?= $config[0]['logo_admin']; ?>" width="350" alt="" class="thumbnail"/>
                                                </div>
                                            </div>                                            
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-lg-5">
                                            <button type="submit" class="btn btn-primary">Atualizar</button>
                                            <a href="<?= URL::base(); ?>/dashboard">
                                                <button type="button" class="btn btn-danger">Voltar</button>
                                                <a/>   
                                        </div>
                                    </div>
                                    </form>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>

        <script src="https://code.jquery.com/ui/1.10.0/jquery-ui.js"></script>
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.10.0/themes/base/jquery-ui.css" />
        <link href="<?= URL::base(); ?>theme/backend/css/custom.css" rel="stylesheet">
    </body>
</html>
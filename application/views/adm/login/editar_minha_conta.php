<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - ACESSO</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <script>
            jQuery(document).ready(function () {
                // Remove seleção de ativo no menu.
                $('.nav li').removeClass('active');
                $('#side-menu li').removeClass('active');
            });
        </script>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
            .centered{
                margin : auto;
            }
        </style>
    </head>
    <body>
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-header">Editar minha conta</h1>
                        <ol class="breadcrumb">
                            <li><i class="fa fa-user"></i> Seus dados</li>
                            <li class="active"><i class="fa fa-list"></i> Meus dados</li>
                        </ol>
                        <form class="form-horizontal" role="form" method="POST" action="editar_conta">
                            <div class="form-group">
                                <div class="col-lg-9">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <span>Seus dados</span>
                                        </div>
                                        <div class="panel-body">    
                                            <div class="form-group">
                                                <label for="txtNome" class="col-lg-3 control-label">Nome</label>
                                                <div class="col-lg-9">
                                                    <input hidden id="idUsuario" name="idUsuario" value="<?= $usuario[0]['idUsuario']; ?>">
                                                    <input type="text" class="form-control" id="txtNome" name="txtNome" value="<?= $usuario[0]['nmNome']; ?>" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="txtUsuario" class="col-lg-3 control-label">Usuário</label>
                                                <div class="col-lg-9">
                                                    <input type="text" class="form-control" id="txtUsuario" name="txtUsuario" value="<?= $usuario[0]['nmUsuario']; ?>" required>
                                                    <span id="resultado" style="margin-top:3px;"></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-3 control-label">Perfil de Administração</label>
                                                <div class="col-lg-9">
                                                    <select class="form-control" id="idPerfil" name="idPerfil" readonly>
                                                        <option value="">Selecione uma opção</option>
                                                        <?php
                                                        foreach ($perfil as $perfis) {
                                                            ?>
                                                            <option <?= ($usuario[0]['perfil'] == $perfis['idPerfil']) ? 'selected' : ''; ?> value="<?= $perfis['idPerfil']; ?>"><?= $perfis['nmPerfil']; ?></option>
                                                            <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-3 control-label">Setor ou Departamento</label>
                                                <div class="col-lg-9">
                                                    <select class="form-control" id="idTipoSetor" name="idTipoSetor" readonly>
                                                        <option value="">Selecione uma opção</option>
                                                        <?php
                                                        foreach ($setor as $setores) {
                                                            ?>
                                                            <option <?= ($setores['idTipoSetor'] == $usuario[0]['idTipoSetor']) ? 'selected' : ''; ?> value="<?= $setores['idTipoSetor']; ?>"><?= $setores['Setor']; ?></option>
                                                            <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="txtSenha" class="col-lg-3 control-label">Senha</label>
                                                <div class="col-lg-9">
                                                    <input type="password" class="form-control" id="txtSenha" name="txtSenha" placeholder="Digite a nova senha">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="txtSenha" class="col-lg-3 control-label">Confirme a Senha</label>
                                                <div class="col-lg-9">
                                                    <input type="password" class="form-control" id="confirmatxtSenha" name="confirmatxtSenha" placeholder="Confirma a nova senha">
                                                    <div id="progressbar">
                                                        <div class="progress-label"></div>
                                                    </div>                                                    
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="txtEmail" class="col-lg-3 control-label">Email</label>
                                                <div class="col-lg-9">
                                                    <input type="email" class="form-control" id="txtEmail" name="txtEmail" value="<?= $usuario[0]['email']; ?>" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-lg-5">
                                            <button type="submit" class="btn btn-primary">Atualizar</button>
                                        </div>
                                    </div>
                                    </form>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>

        <script>

            var password = document.getElementById("txtSenha"), confirm_password = document.getElementById("confirmatxtSenha");

            function validatePassword() {
                if (password.value != confirm_password.value) {
                    confirm_password.setCustomValidity("As senhas são diferentes!");
                } else {
                    confirm_password.setCustomValidity('');
                }
            }

            password.onchange = validatePassword;
            confirm_password.onkeyup = validatePassword;

            // verifica se existe o usuário no banco
            $("input[name='txtUsuario']").on('blur', function () {
                var txtUsuario = $(this).val();
                //alert(txtUsuario);
                if (txtUsuario != "") {
                    $.get('verifica_usuario?txtUsuario=' + txtUsuario, function (data) {

                        if (data === "") {
                            $('#resultado').addClass('btn btn-success btn-xs');
                            $('#resultado').removeClass('btn-danger');
                            $('#resultado').html("USUÁRIO VÁLIDO");
                        } else {
                            $('#resultado').addClass('btn btn-danger btn-xs');
                            $('#resultado').removeClass('btn-success');
                            $('#resultado').html("USUÁRIO INVÁLIDO");

                        }
                    });
                }
            });

        </script>


        <script src="https://code.jquery.com/ui/1.10.0/jquery-ui.js"></script>
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.10.0/themes/base/jquery-ui.css" />
        <script src="<?= URL::base(); ?>theme/backend/js/plugins/verificasenha/jquery.complexify.banlist.js"></script>
        <script src="<?= URL::base(); ?>theme/backend/js/plugins/verificasenha/jquery.complexify.js"></script>
        <link href="<?= URL::base(); ?>theme/backend/css/custom.css" rel="stylesheet">
        <script type="text/javascript">
            $(function () {
                $("#txtSenha").complexify({},
                        function (valid, complexity) {
                            $("#progressbar").progressbar({
                                value: complexity,
                            });
                            if (complexity < '5') {
                                $(".ui-progressbar-value").addClass('ui-widget-header-danger').removeClass('ui-widget-header-warning ui-widget-header-success');
                                $(".progress-label").html('Esta senha não pode ser usada!');
                                //alert(complexity);
                            } else if (complexity < '10') {
                                $(".ui-progressbar-value").addClass('ui-widget-header-danger').removeClass('ui-widget-header-warning ui-widget-header-success');
                                $(".progress-label").html('Nível de Segurança - Fraca!');
                            } else if (complexity < '25') {
                                $(".ui-progressbar-value").addClass('ui-widget-header-warning').removeClass('ui-widget-header-danger ui-widget-header-success');
                                $(".progress-label").html('Nível de Segurança - Fraca!');
                            } else if (complexity < '50') {
                                $(".ui-progressbar-value").addClass('ui-widget-header-warning').removeClass('ui-widget-header-danger ui-widget-header-success');
                                $(".progress-label").html('Nível de Segurança - Médio!');
                            } else if (complexity < '80') {
                                $(".ui-progressbar-value").addClass('ui-widget-header-success').removeClass('ui-widget-header-danger ui-widget-header-warning');
                                $(".progress-label").html('Nível de Segurança - Médio Alto');
                            } else {
                                $(".ui-progressbar-value").addClass('ui-widget-header-success').removeClass('ui-widget-header-danger ui-widget-header-warning');
                                $(".progress-label").html('Nível de Segurança - Forte');
                            }
                        });
            });
        </script>          

    </body>
</html>
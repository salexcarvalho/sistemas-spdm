<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - ACESSO</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <script>
            jQuery(document).ready(function () {
                // Remove seleção de ativo no menu.
                $('.nav li').removeClass('active');
                $('#side-menu li').removeClass('active');
            });
        </script>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
            .centered{
                margin : auto;
            }
        </style>
    </head>
    <body>
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-header">Dados do Perfil</h1>
                        <ol class="breadcrumb">
                            <li><i class="fa fa-lock"></i> Controle de Perfil</li>
                            <li><i class="fa fa-cogs"></i> Lista de Perfil</li>
                            <li class="active"><i class="fa fa-edit"></i> Visualização de Perfil</li>
                        </ol>
                        <form class="form-horizontal" role="form" method="POST" action="edita_perfil">
                            <div class="form-group">
                                <div class="col-lg-5">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <span>Dados do Perfil</span>
                                        </div>
                                        <div class="panel-body">    
                                            <div class="form-group">
                                                <label class="col-lg-3 control-label">Nome do Perfil</label>
                                                <div class="col-lg-9">
                                                    <input type="text" class="form-control" id="nmPerfil" name="nmPerfil" value="<?= $Perfil[0]['nmPerfil']; ?>" disabled>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-lg-3 control-label">Descrição do Perfil</label>
                                                <div class="col-lg-9">
                                                    <input type="text" class="form-control" id="dsPerfil" name="dsPerfil" value="<?= $Perfil[0]['dsPerfil']; ?>" disabled>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="panel panel-default">
                                        <div class="panel-heading">Ações do Perfil</div>
                                        <div class="panel-body">
                                            <div class="panel-group" id="accordion">
                                                <?php foreach ($controllersMenu as $controllerMenu) { ?>

                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" data-parent="#accordion" href="#Menu_<?= $controllerMenu['nmController']; ?>" aria-expanded="false" class="collapsed"><?= $controllerMenu['dsController']; ?></a>
                                                            </h4>
                                                        </div>
                                                        <div id="Menu_<?= $controllerMenu['nmController']; ?>" class="panel-collapse collapse" aria-expanded="false">
                                                            <div class="panel-body">

                                                                <?php foreach ($PerfilTemAcaoMenu as $acaoMenu) { ?>
                                                                    <?php if ($controllerMenu['nmController'] == $acaoMenu['cdController']) { ?>
                                                                        <label class="checkbox col-lg-12">
                                                                            <input type="checkbox" id="Acao_<?= $acaoMenu['idAcao']; ?>" name="Acao_<?= $acaoMenu['idAcao']; ?>" <?php if ($acaoMenu['Status'] == "true") {
                                                                echo "checked";
                                                            } ?> data-toggle="toggle" data-size="small" data-onstyle="success" data-on="Sim" data-off="Não" data-width="60" value="<?= $acaoMenu['Status']; ?>" disabled> <?= $acaoMenu['dsAcao']; ?>
                                                                        </label>
                                                                        <script>
                                                                            $(function () {
                                                                                $('#Acao_<?= $acaoMenu['idAcao']; ?>').change(function () {
                                                                                    $('#Acao_<?= $acaoMenu['idAcao']; ?>').val($(this).prop('checked'))
                                                                                })
                                                                            })
                                                                        </script>
        <?php } ?>
    <?php } ?>

                                                            </div>
                                                        </div>
                                                    </div>

<?php } ?>
                                            </div>
                                        </div>
                                    </div>                                     

                                </div>
                                <div class="col-lg-7">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">Ações do Perfil</div>
                                        <div class="panel-body">
                                            <div class="panel-group" id="accordion">
<?php foreach ($controllers as $controller) { ?>

                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" data-parent="#accordion" href="#perfil_<?= $controller['nmController']; ?>" aria-expanded="false" class="collapsed"><?= $controller['dsController']; ?></a>
                                                            </h4>
                                                        </div>
                                                        <div id="perfil_<?= $controller['nmController']; ?>" class="panel-collapse collapse" aria-expanded="false">
                                                            <div class="panel-body">

    <?php foreach ($PerfilTemAcao as $acao) { ?>
        <?php if ($controller['nmController'] == $acao['cdController']) { ?>
                                                                        <label class="checkbox col-lg-6">
                                                                            <input type="checkbox" id="Acao_<?= $acao['idAcao']; ?>" name="Acao_<?= $acao['idAcao']; ?>" <?php if ($acao['Status'] == "true") {
                echo "checked";
            } ?> data-toggle="toggle" data-size="small" data-onstyle="success" data-on="Sim" data-off="Não" data-width="60" value="<?= $acao['Status']; ?>" disabled> <?= $acao['dsAcao']; ?>
                                                                        </label>
                                                                        <script>
                                                                            $(function () {
                                                                                $('#Acao_<?= $acao['idAcao']; ?>').change(function () {
                                                                                    $('#Acao_<?= $acao['idAcao']; ?>').val($(this).prop('checked'))
                                                                                })
                                                                            })
                                                                        </script>
        <?php } ?>
    <?php } ?>

                                                            </div>
                                                        </div>
                                                    </div>

<?php } ?>
                                            </div>
                                        </div>
                                    </div>    

                                </div>                                
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <div class="col-lg-5">
                                            <a href="lista_perfis">
                                                <button type="button" class="btn btn-danger">Voltar</button>
                                                <a/>   
                                        </div>
                                    </div>

                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>
</body>
</html>
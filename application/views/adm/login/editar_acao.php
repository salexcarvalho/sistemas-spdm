<form id="novaacao" name="novaacao" class="form-horizontal" role="form" method="POST" action="editar_acao">
    <div class="form-group">
        <div class="col-lg-12">
            <div class="form-group">
                <input hidden id="page" name="page" value="<?= $page; ?>">
                <label for="txtNome" class="col-lg-2 control-label">Nome</label>
                <div class="col-lg-10">
                    <input hidden id="idAcao" name="idAcao" value="<?= $acao[0]['idAcao']; ?>">
                    <input type="text" class="form-control" id="nmAcao" name="nmAcao" value="<?= $acao[0]['nmAcao']; ?>" required>
                </div>
            </div>
            <div class="form-group">
                <label for="txtUsuario" class="col-lg-2 control-label">Descrição</label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" id="dsAcao" name="dsAcao" value="<?= $acao[0]['dsAcao']; ?>" required>
                </div>
            </div>
            <div class="form-group">
                <label for="txtUsuario" class="col-lg-2 control-label">ID Curto</label>
                <div class="col-lg-10">
                    <input type="text" size="20" maxlength="20" class="form-control" id="idCurto" name="idCurto" value="<?= $acao[0]['idCurto']; ?>" required>
                </div>
            </div>                                              
            <div class="form-group">
                <label class="col-lg-2 control-label">Controller</label>
                <div class="col-lg-10">
                    <select class="form-control" id="nmController" name="nmController" required>
                        <option value="">Selecione uma opção</option>
                        <?php
                        foreach ($controller as $controllers) {
                            ?>
                            <option <?= ($acao[0]['cdController'] == $controllers['nmController']) ? 'selected' : ''; ?> value="<?= $controllers['nmController']; ?>"><?= $controllers['nmController']; ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
        </div>
    </div>
</form>
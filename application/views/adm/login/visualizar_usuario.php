<div class="form-horizontal">
    <div class="col-lg-12">
        <div class="form-group">
            <label for="txtNome" class="col-lg-2 control-label">Nome</label>
            <div class="col-lg-10">
                <input type="text" class="form-control" id="txtNome" name="txtNome" value="<?= $usuario[0]['nmNome']; ?>" disabled>
            </div>
        </div>
        <div class="form-group">
            <label for="txtUsuario" class="col-lg-2 control-label">Usuário</label>
            <div class="col-lg-10">
                <input type="text" class="form-control" id="txtUsuario" name="txtUsuario" value="<?= $usuario[0]['nmUsuario']; ?>" disabled>
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label">Perfil de Administração</label>
            <div class="col-lg-10">
                <select class="form-control" id="idPerfil" name="idPerfil" disabled>
                    <option value="">Selecione uma opção</option>
                    <?php
                    foreach ($perfil as $perfis) {
                        ?>
                        <option <?= ($usuario[0]['perfil'] == $perfis['idPerfil']) ? 'selected' : ''; ?> value="<?= $perfis['idPerfil']; ?>"><?= $perfis['nmPerfil']; ?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label">Setor ou Departamento</label>
            <div class="col-lg-10">
                <select class="form-control" id="idTipoSetor" name="idTipoSetor" disabled>
                    <option value="">Selecione uma opção</option>
                    <?php
                    foreach ($setor as $setores) {
                        ?>
                        <option <?= ($setores['idTipoSetor'] == $usuario[0]['idTipoSetor']) ? 'selected' : ''; ?> value="<?= $setores['idTipoSetor']; ?>"><?= $setores['Setor']; ?></option>
                        <?php
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="txtEmail" class="col-lg-2 control-label">Email</label>
            <div class="col-lg-10">
                <input type="email" class="form-control" id="txtEmail" name="txtEmail" value="<?= $usuario[0]['email']; ?>" disabled>
            </div>
        </div>
        <div class="form-group">
            <label for="txtEmail" class="col-lg-2 control-label">Nº de Logins</label>
            <div class="col-lg-10">
                <input type="text" class="form-control" id="txtlogin" name="txtlogin" value="<?= $usuario[0]['logins']; ?>" disabled>
            </div>
        </div>
        <div class="form-group">
            <label for="txtEmail" class="col-lg-2 control-label">Cadastrado</label>
            <div class="col-lg-10">
                <input type="data" class="form-control" id="dtCadastro" name="dtCadastro" value="<?= strftime('%d/%m/%Y ás %H:%M', strtotime($usuario[0]["dtCadastro"])); ?>" disabled>
            </div>
        </div>
        <div class="form-group">
            <label for="txtEmail" class="col-lg-2 control-label">Ultimo Login</label>
            <div class="col-lg-10">
                <input type="data" class="form-control" id="ultimoLogin" name="ultimoLogin" value="<?= strftime('%d/%m/%Y ás %H:%M', strtotime($usuario[0]["ultimoLogin"])); ?>" disabled>
            </div>
        </div>
    </div>
</div>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - ACESSO</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Lista de Usuários</h1>
                        <ol class="breadcrumb">
                            <li><i class="fa fa-lock"></i> Controle de Acesso</li>
                            <li class="active"><i class="fa fa-users"></i> Lista de usuários</li>
                        </ol>

                        <?php if (isset($warning)) : ?>
                            <div id="msg" class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong><?= $warning; ?></strong>
                            </div>
                        <?php endif ?>
                        <div class="row">
                            <div class="col-lg-12">
                                <button type="submit" id="btn_novo_usuario" class="pull-right btn btn-primary btn-md" name="btn_novo_usuario"><i class="glyphicon glyphicon-plus-sign"></i>  Adicionar Novo</button>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="usuarios">
                                <thead>
                                    <tr>
                                        <th width="5%" style="text-align: center">#</th>
                                        <th >Nome</th>
                                        <th width="10%" >Usuário</th>
                                        <th width="16%" >Email</th>
                                        <th width="15%" >Perfil</th>
                                        <th width="6%" style="text-align: center">Acessos</th>
                                        <th width="11%" style="text-align: center">Data de Cadastro</th>
                                        <th width="8%" style="text-align: center">Ações</th>
                                        <th width="5%" style="text-align: center">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($data as $usuario) {
                                        ?>
                                        <tr>
                                            <td align="center"><?= $usuario['idUsuario']; ?></td>
                                            <td><?= $usuario['nmNome']; ?></td>
                                            <td><?= $usuario['nmUsuario']; ?></td>
                                            <td><?= $usuario['email']; ?></td>
                                            <td><?= $usuario['perfil']; ?></td>
                                            <td style="text-align: center"><?= $usuario['logins']; ?></td>
                                            <td align="center"><?= strftime('%d/%m/%Y - %H:%M', strtotime($usuario['dtCadastro'])); ?></td>
                                            <td align="center">
                                                    <button type="submit" id="btnVisualizar" class="btn btn-default btn-xs" page="<?= $PaginaAtual; ?>" idUsuario="<?= $usuario['idUsuario']; ?>" name="btnVisualizar" title="Visualizar Usuário">
                                                        <i class="glyphicon glyphicon-file"></i>
                                                    </button>
                                                    <button type="submit" id="btnEditar" class="btn btn-default btn-xs" page="<?= $PaginaAtual; ?>" idUsuario="<?= $usuario['idUsuario']; ?>" name="btnEditar" title="Editar Usuário">
                                                        <i class="glyphicon glyphicon-edit"></i>
                                                    </button>
                                                    <button type="submit" id="btnExcluir" class="btn btn-default btn-xs" nmUsuario="<?= $usuario['nmNome']; ?>" idUsuario="<?= $usuario['idUsuario']; ?>" name="btnExcluir"  data-toggle="modal" data-target="#modal_excluir" data-placement="bottom" data-delay="1" title="Excluir Usuário">
                                                        <i class="glyphicon glyphicon-trash"></i>
                                                    </button>
                                            </td>
                                            <td align="center">
                                                <input type="hidden" id="idUsuario" name="idUsuario" value="<?= $usuario['idUsuario']; ?>">
                                                <?php
                                                if ($usuario['ativo'] == "N") {
                                                    ?>
                                                    <button type="submit" id="btnAtivar_<?= $usuario['idUsuario']; ?>" class="btn btn-danger btn-xs" idUsuario="<?= $usuario['idUsuario']; ?>" name="btnAtivar" title="Ativar Usuário">
                                                        <i id="activ_<?= $usuario['idUsuario']; ?>" class="glyphicon glyphicon-ban-circle"></i>
                                                    </button>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <button type="submit" id="btnAtivar_<?= $usuario['idUsuario']; ?>" class="btn btn-success btn-xs" idUsuario="<?= $usuario['idUsuario']; ?>" name="btnAtivar" title="Destivar Usuário">
                                                        <i id="activ_<?= $usuario['idUsuario']; ?>" class="glyphicon glyphicon-ok"></i>
                                                    </button>
                                                    <?php
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                            <?php include(kohana::find_file('views/paginator', 'paginacao', 'php')) ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- Modal -->
        <div class="modal fade" id="modal_excluir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Excluir Usuário</h4>
                    </div>
                    <div class="modal-body" id="modal_delete_usuario">Deseja excluir este usuário?</div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        <button type="button" class="btn btn-primary" id="modal_btn-excluir">Excluir</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal --> 
        
        <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>

        <script>
            jQuery(document).ready(function () {
                // Remove seleção de ativo no menu.
                $('.nav li').removeClass('active');
                $('#side-menu li').removeClass('active');
                
                // Datatables
                $('#usuarios').dataTable(
                        {
                            "bJQueryUI": false,
                            "aoColumnDefs": [{"bSortable": false, "aTargets": [0, 3, 4, 5, 6, 7, 8]}],
                            "paging": false,
                            "info": false,
                            "bFilter": false,
                            "oLanguage":
                                    {
                                        "sSearch": " Buscar: "
                                    }

                        });
                
                // abre o modal para cadastro
                $('button[name=btn_novo_usuario]').click(function () {
                    var idUsuario = '';
                    var options = {
                        url: "add_usuario?idUsuario=" + idUsuario,
                        title: 'Cadastrar Usuário',
                        size: eModal.size.lg,
                        buttons: [
                            {text: 'GRAVAR', style: 'primary', close: false, type: 'submit', form: 'novousuario'},
                            {text: 'FECHAR', style: 'danger', close: true}
                        ]
                    };
                    eModal.ajax(options);
                });

                //abre o modal de edição
                $('button[name=btnEditar]').click(function () {
                    var idUsuario = $(this).attr('idUsuario');
                    var page = $(this).attr('page');
                    var options = {
                        url: "editar_dados_usuarios?idUsuario=" + idUsuario + "&page=" + page,
                        title: 'Editar Usuário',
                        size: eModal.size.lg,
                        buttons: [
                            {text: 'SALVAR', style: 'primary', close: false, type: 'submit', form: 'editarusuario'},
                            {text: 'FECHAR', style: 'danger', close: true}
                        ]
                    };
                    eModal.ajax(options);
                });
                
                //abre o modal de visualização
                $('button[name=btnVisualizar]').click(function () {
                    var idUsuario = $(this).attr('idUsuario');
                    var page = $(this).attr('page');
                    var options = {
                        url: "visualizar_usuario?idUsuario=" + idUsuario + "&page=" + page,
                        title: 'Visualizar Usuário',
                        size: eModal.size.lg,
                        buttons: [
                            {text: 'FECHAR', style: 'danger', close: true}
                        ]
                    };
                    eModal.ajax(options);
                });
        
                // Opção usada na exclusão
                var idUsuario;
                var nmUsuario;
                $('body').on("click", 'button[name=btnExcluir]', function ()
                {
                    idUsuario = $(this).attr('idUsuario');
                    nmUsuario = $(this).attr('nmUsuario');
                    $('#modal_delete_usuario').html("Deseja excluir este usuário <b>\"" + nmUsuario + "\"</b> ?");
                });
                
                $('#modal_excluir').on('hidden.bs.modal', function ()
                {
                    window.location.replace("lista_usuario/lista");
                });

                $('#modal_btn-excluir').on("click", function ()
                {
                    var btn = $(this);
                    btn.text('loading');

                    btn.removeClass('btn-primary');
                    btn.removeClass('btn-danger');
                    btn.removeClass('btn-success');
                    btn.addClass('btn-primary');

                    $('#modal_btn-excluir').prop('disabled', true);

                    $.ajax("excluir_usuario",
                            {
                                type: "GET",
                                data:
                                        {
                                            idUsuario: idUsuario
                                        }
                            })
                            .done(function (data)
                            {
                                $('#modal_delete_usuario').html('Usuário excluído com sucesso');

                                btn.removeClass('btn-primary');
                                btn.addClass('btn-success');
                            })
                            .fail(function ()
                            {
                                $('#modal_delete_usuario').html('Erro: ' + data);

                                btn.removeClass('btn-primary');
                                btn.addClass('btn-danger');
                            })
                            .always(function ()
                            {
                                btn.text('Excluído');
                            });
                });

                // Opção para ativar e desativar
                $("body").on("click", "button[name=btnAtivar]", function () {
                    var idUsuario = $(this).attr('idUsuario');
                    //alert(txtUsuario);
                    $.get('update_ativar_usuario?idUsuario=' + idUsuario, function (data) {

                        if (data === "1") {
                            $('#btnAtivar_' + idUsuario).addClass('btn btn-success btn-xs');
                            $('#btnAtivar_' + idUsuario).removeClass('btn-danger');
                            $('#activ_' + idUsuario).removeClass('glyphicon-ban-circle');
                            $('#activ_' + idUsuario).addClass('glyphicon-ok');
                        } else {
                            $('#btnAtivar_' + idUsuario).addClass('btn btn-danger btn-xs');
                            $('#btnAtivar_' + idUsuario).removeClass('btn-success');
                            $('#activ_' + idUsuario).addClass('glyphicon-ok');
                            $('#activ_' + idUsuario).addClass('glyphicon-ban-circle');
                        }
                    });
                });

                $('.dataTables_filter label').addClass('pull-right');
            });
        </script>

    </body>
</html>
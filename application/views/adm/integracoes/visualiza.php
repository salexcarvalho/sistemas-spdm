<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title>SPDM - <?= $video[0]['title']; ?></title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"><?= $video[0]['title']; ?></h1>
                    <div class="row">
                        <div class="col-md-12">
                            <p><?= $video[0]['introtext']; ?></p>
                            <iframe width="100%" height="642" src="https://www.youtube.com/embed/<?= preg_replace(array('/YouTube/', '|/|', '|{|', '|}|'), '', $video[0]['video']); ?>" frameborder="0" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </body>
    <script>
        jQuery(document).ready(function () {
            // Remove seleção de ativo no menu.
            $('.nav li').removeClass('active');
            $('#side-menu li').removeClass('active');
            // Ativa botão no menu.
            $('#li-videos ul').addClass('collapse in');
            $('#li-cad-suasaude a').addClass('active');
            $('#li-videos').addClass('active');
            $('#li-integracao a').addClass('collapse in');
        });
    </script> 
    <style>
        .pagination
        {
            float: right !important;
        }
    </style>
</html>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $_SESSION['company_name'] ?> - CSM</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div id="wrapper">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/adm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/adm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Galeria de Vídeos - "Boas Práticas em Enfermagem"</h1>
                        <ol class="breadcrumb">
                            <li><i class="fa fa-list"></i> Listagens</li>
                            <li class="active"><i class="fa fa-youtube"></i> Vídeos</li>
                        </ol>
                        <?php if (isset($warning)) : ?>
                            <div class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <?= $warning; ?></div>
                        <?php endif ?>
                        <?php if (isset($errors)) : ?>
                            <div style="padding: 15px;" class="alert alert-danger">Alguns erros foram encontrados, por favor confira os dados que você inseriu.
                                <ul class="errors">
                                    <?php foreach ($errors as $message): ?>
                                        <li><?php echo $message; ?></li>
                                    <?php endforeach ?>
                                </ul></div>
                        <?php endif ?>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="servicos">
                                    <thead>
                                        <tr>
                                            <th width="5%" style="text-align: center">#</th>
                                            <th>Título do Vídeo</th>
                                            <th width="22%" >Categoria</th>
                                            <th width="16%" style="text-align: center">Data de Publicação</th>
                                            <th width="6%" style="text-align: center">Ações</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($data as $video) {
                                            ?>
                                            <tr>
                                                <td align="center"><?= $video['id']; ?></td>
                                                <td><?= $video['title']; ?></td>
                                                <td><?= $video['nmCategoria']; ?></td>
                                                <td align="center"><?= strftime('%d/%m/%Y - %H:%M', strtotime($video['created'])); ?></td>
                                                <td align="center">
                                                    <a href="<?= URL::base(); ?>integracoes/vervideo?v=<?= $video['id']; ?>" target="_blank">
                                                        <button type="submit" id="btnVisualizar" class="btn btn-danger btn-xs" name="" title="Visualizar Vídeo">
                                                            <i class="fa fa-youtube-play"></i>
                                                        </button>
                                                    </a>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                                <?php include(kohana::find_file('views/paginator', 'paginacao', 'php')) ?>
                            </div>
                    </div>
                </div>
            </div>
            <?php include(kohana::find_file('views/templates/adm', 'footer', 'php')) ?>
    </body>
    <script>
        jQuery(document).ready(function () {
            // Remove seleção de ativo no menu.
            $('.nav li').removeClass('active');
            $('#side-menu li').removeClass('active');
            // Ativa botão no menu.
            $('#li-videos ul').addClass('collapse in');
            $('#li-cad-boaspraticas a').addClass('active');
            $('#li-videos').addClass('active');
            $('#li-videos a').addClass('collapse in');
        });
    </script>    
    <style>
        .pagination
        {
            float: right !important;
        }
    </style>
</html>
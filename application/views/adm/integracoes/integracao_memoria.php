<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title>SISTEMAS SPDM - CSM</title>
        <?php include(kohana::find_file('views/templates/adm', 'init', 'php')) ?>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Galeria de Vídeos "Memória SPDM"</h1>
                    <div class="row">
                        <?php
                        foreach ($data as $video) {
                            ?>
                            <div class="col-md-4" style="height:265px;">
                                <a href="<?= URL::base(); ?>integracoes/vervideo?v=<?= $video['id']; ?>" target="_blank">
                                    <img class="img-thumbnail" src="https://www.spdm.org.br/media/k2/items/cache/<?= md5('Image' . $video['id']); ?>_L.jpg" height="210px">
                                </a>
                                <h5><strong><?= $video['title']; ?></strong></h5>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                    <?php include(kohana::find_file('views/paginator', 'paginacao', 'php')) ?>
                </div>
            </div>
        </div>

    </body>
    <script>
        jQuery(document).ready(function () {
            // Remove seleção de ativo no menu.
            $('.nav li').removeClass('active');
            $('#side-menu li').removeClass('active');
            // Ativa botão no menu.
            $('#li-videos ul').addClass('collapse in');
            $('#li-cad-integracao-memoria a').addClass('active');
            $('#li-videos').addClass('active');
            $('#li-integracao a').addClass('collapse in');
        });
    </script> 
    <style>
        .pagination
        {
            float: right !important;
        }
        .img-thumbnail {
            width: 360px;
            height: 207px;
        }
        .page-header {
            margin: 20px 0 10px;
        }
    </style>
</html>
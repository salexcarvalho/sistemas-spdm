<div class="col-md-6">
    <?php
    $first_string = 'Início';
    $previous_string = 'Anterior';
    $next_string = 'Próximo';
    $last_string = 'Último';
    ?>
    <div id="pagination">
        <ul class="pagination pagination-md">
            <?php
            if ($first) {
                echo '<li> <a href="' . $first . '" rel="first">' . $first_string . '</a> </li>';
            }
            if ($previous) {
                echo '<li> <a href="' . $previous . '" rel="previous">' . $previous_string . '</a> </li>';
            }
            foreach ($pages_in_range as $key => $value) {
                $value ? print('<li> <a id="linkpaginacao" href="' . $value . '">' . $key . '</a> </li>') : print ('<li class="active"> <a id="' . $value . '" href="#">' . $key . '</a> </li>');
            }
            if ($next) {
                echo '<li> <a href="' . $next . '" rel="next">' . $next_string . '</a> </li>';
            }
            if ($last) {
                echo '<li> <a href="' . $last . '" rel="last">' . $last_string . '</a> </li>';
            }
            ?>
        </ul>
    </div>
</div>


<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>SPDM - Agendamento de Sala</title>
        <?php include(kohana::find_file('views/templates/rsa', 'Finit', 'php')) ?>
        <script>
            jQuery(document).ready(function ()
            {
                // Remove seleção de ativo no menu.
                $('.navbar-nav li').removeClass('active');
                $('#menu-superior li').removeClass('active');
                // Ativa botão no menu.
                $('.navbar-nav #mn_acompanhar').addClass('active');
            });
        </script>
    </head>
    <body>
        <div class="content">
            <?php include(kohana::find_file('views/templates/rsa', 'header', 'php')) ?>
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/rsa', 'menu', 'php')) ?>
            <!-- Fim do Menu -->

            <div class="container" >
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="bold uppercase azul">Consultar Agendamento</h4>
                        <hr style="width: 97%;">
                    </div>
                </div>            
                <div class="row container">
                    <?php if (isset($warning)) : ?>
                        <div id="msg" class="alert alert-danger alert-dismissible" role="alert" >
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <strong><?= $warning; ?></strong>
                        </div>
                    <?php endif ?>
                    <p>Para acompanhar o andamento do seu agendamento, por favor digite o ID do seu agendamento no campo a abaixo:</p>
                    <br>
                </div>
                <form class="form-horizontal" role="form" method="POST" action="consultar">
                    <div class="form-group col-md-12">
                        <div class="col-md-3">
                            <label>E-mail de Agendamento<span id="obrigatorio" style="color: #ff0000;">*</span></label>
                        </div>    
                        <div class="col-md-3">
                            <input type="email" name="email" id="email" class="form-control" size="16" required>
                        </div>  
                    </div>
                    <div class="form-group col-md-12">
                        <div class="col-md-3"></div>
                        <div class="col-md-5">
                            <button class="btn btn-sm btn-primary uppercase" type="submit" name="submit" id="submit" value="">Consultar</button>
                            <button class="btn btn-sm btn-info uppercase" type="reset" name="reset" id="reset" value="">Cancelar</button>
                        </div> 
                    </div> 
                </form>    
                <div class="pager" style="height: 180px;"></div>
            </div>

        </div>
    </div>
    <?php include(kohana::find_file('views/templates/rsa', 'footer', 'php')) ?>
</body>
</html>
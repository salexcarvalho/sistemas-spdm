<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?= SISTEMA; ?> - Agendamento de Sala</title>
        <?php include(kohana::find_file('views/templates/rsa', 'init', 'php')) ?>
        <script>
            $(document).ready(function () {
                var d = new Date();
                var hora = d.getHours();
                if (hora < 7) {
                    hora = 7;
                }
                var min = d.getMinutes();
                if (min < 10) {
                    min = '0' + min;
                }
                var dataAtual, dia, mes, ano;
                dia = d.getDate();
                mes = d.getMonth() + 1;
                ano = d.getFullYear();

                if (mes < 10) {
                    mes = "0" + mes;
                }
                if (dia < 10) {
                    dia = "0" + dia;
                }

                dataAtual = ano + "-" + mes + "-" + dia;

                var horaAtual = hora + ":" + min;

                $('.form_date').datetimepicker({
                    locale: 'pt-br',
                    format: "D/M/YYYY",
                    minDate: dataAtual
                });

                $('.form_date2').datetimepicker({
                    locale: 'pt-br',
                    format: "D/M/YYYY",
                    minDate: dataAtual
                });

                $('.form_time').datetimepicker({
                    format: 'LT',
                    disabledHours: [0, 1, 2, 3, 4, 5, 6, 24],
                    enabledHours: [8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23],
                    locale: 'pt-br'
                });

                $('.form_time2').datetimepicker({
                    format: 'LT',
                    disabledHours: [0, 1, 2, 3, 4, 5, 6, 24],
                    enabledHours: [8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23],
                    locale: 'pt-br'
                });

            });
        </script>
        <style>
            .breadcrumb {
                background-color: #fff;
            }
        </style>
    </head>
    <body>
        <div class="content">
            <?php include(kohana::find_file("views/templates/rsa", "header", "php")) ?>
            <!-Inicio do Menu -->
            <?php include(kohana::find_file("views/templates/rsa", "menu", "php")) ?>
            <!-Fim do Menu -->

            <div class="container" >
                <div class="row">
                    <div class="col-lg-12">
                        <h4 class="bold uppercase azul">Realizar Agendamento</h4>
                        <hr style="width: 97%;">
                    </div>
                </div>
                <div class="row col-lg-12" style="margin-left:15px; margin-bottom:15px;">
                    <p>O Agendamento ocorrerá da seguinte forma:</p>
                        <ol type="1">
                            <li>Selecione a sala que deseja</li>
                            <li>Coloque o nome do evento a ser realizado</li>
                            <li>Coloque o nome e os contatos do responsável pelo evento</li>
                            <li>Coloque o nome e os contatos do responsável pelo Agendamento</li>
                            <li>Escolhas as datas de início e termino do evento</li>
                        </ol>
                </div>
                    <div class="row col-lg-12">
                        <form class="form-horizontal" role="form" method="POST" action="agendar">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <span> Dados do Agendamento</span>
                                </div>
                                <div class="panel-body">

                                    <div class="form-group col-lg-12">
                                        <div id="vinculo_solicitante_grupo"  >
                                            <div class="col-lg-2">
                                                <label>Local<span id="obrigatorio" style="color: #ff0000;">*</span></label>
                                            </div>
                                            <div class="col-lg-10">
                                                <select class="form-control" id="idArea" name="idArea" required>
                                                    <option value="" selected="selected">Selecione uma Opção</option>
                                                    <?php foreach ($areas as $area) { ?>
                                                        <option value="<?= $area["idArea"]; ?>"><?= $area["nome"]; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-12">
                                        <div id="vinculo_solicitante_grupo"  >
                                            <div class="col-lg-2">
                                                <label>Sala<span id="obrigatorio" style="color: #ff0000;">*</span></label>
                                            </div>
                                            <div class="col-lg-10">
                                                <select class="form-control" id="id_sala" name="id_sala" required>
                                                    <option value="" selected="selected">Selecione uma Opção</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="nome_solicitante_grupo" class="form-group col-lg-12">
                                        <div class="col-lg-2">
                                            <label id="nome_solicitante_label">Nome do Evento<span id="obrigatorio" style="color: #ff0000;">*</span></label>
                                        </div>
                                        <div class="col-lg-10">
                                            <input type="text" name="titulo" id="nome_solicitante" class="form-control" placeholder="Digite aqui nome do Evento" required>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <span> Responsável pelo Evento</span>
                                </div>
                                <div class="panel-body">
                                    <div class="form-group col-lg-12">
                                    <div class="col-lg-2">
                                        <label>Nome<span id="obrigatorio" style="color: #ff0000;">*</span></label>
                                    </div>
                                    <div class="col-lg-10">
                                        <input type="text" name="resp_nome" id="resp_nome" class="form-control" placeholder="Digite aqui nome do responsavel">
                                    </div>
                                    </div>    
                                    <div class="form-group col-lg-12">
                                    <div class="col-lg-2">
                                        <label>Email<span id="obrigatorio" style="color: #ff0000;">*</span></label>
                                    </div>
                                    <div class="col-lg-10">
                                        <input type="text" name="resp_email" id="resp_email" class="form-control" placeholder="Digite aqui o email do responsavel">
                                    </div>
                                    </div>    
                                    <div class="form-group col-lg-12">    
                                    <div class="col-lg-2">
                                        <label>Telefone<span id="obrigatorio" style="color: #ff0000;">*</span></label>
                                    </div>
                                    <div class="col-lg-4">
                                        <input type="tel" min="0" name="resp_telefone" id="resp_telefone" class="form-control" placeholder="Digite aqui o telefone do responsavel" data-mask="(99) 9999-9999">
                                    </div>
                                    <div class="col-lg-2">
                                        <label>Celular</label>
                                    </div>
                                    <div class="col-lg-4">
                                        <input type="tel" min="0" name="resp_celular" id="resp_celular" class="form-control" placeholder="Digite aqui o seu celular do responsavel" data-mask="(99) 9999-9999?9">
                                    </div>
                                    </div>    
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <span> Responsável pelo Agendamento</span>
                                </div>
                                <div class="panel-body"> 
                                    <div class="form-group col-lg-12"> 
                                    <div class="col-lg-2">
                                        <label>Nome<span id="obrigatorio" style="color: #ff0000;">*</span></label>
                                    </div>
                                    <div class="col-lg-10">
                                        <input type="text" name="resp_nome" id="resp_nome" class="form-control" placeholder="Digite aqui nome do responsavel pelo Agendamento">
                                    </div>
                                    </div>    
                                    <div class="form-group col-lg-12">     
                                    <div class="col-lg-2">
                                        <label>Email<span id="obrigatorio" style="color: #ff0000;">*</span></label>
                                    </div>
                                    <div class="col-lg-10">
                                        <input type="text" name="resp_email" id="resp_email" class="form-control" placeholder="Digite aqui o email do responsavel pelo Agendamento">
                                    </div>
                                    </div>    
                                    <div class="form-group col-lg-12"> 
                                    <div class="col-lg-2">
                                        <label>Telefone<span id="obrigatorio" style="color: #ff0000;">*</span></label>
                                    </div>
                                    <div class="col-lg-4">
                                        <input type="tel" min="0" name="resp_telefone" id="resp_telefone" class="form-control" placeholder="Digite aqui o telefone do responsavel pelo Agendamento" data-mask="(99) 9999-9999">
                                    </div>
                                    <div class="col-lg-2">
                                        <label>Celular</label>
                                    </div>
                                    <div class="col-lg-4">
                                        <input type="tel" min="0" name="resp_celular" id="resp_celular" class="form-control" placeholder="Digite aqui o seu celular do responsavel pelo Agendamento" data-mask="(99) 9999-9999?9">
                                    </div>
                                    </div>    
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <span> Data(s) do Evento</span>
                                    
                                </div>
                                <div class="panel-body" id="DatasAdicionais">
                                    <div class="col-lg-12">
                                        <div id="DatasAdicionais_1"  class="form-group">
                                            <input type="hidden" class="form-control" id="cont" name="cont" value="0"/>
                                            <div class="form-group col-lg-6">
                                                <div class="col-lg-1">
                                                    <label for="start_data_1" class="input-group">Início: </label>
                                                </div>
                                                <div class="col-lg-5">
                                                    <div class="input-group date form_date">
                                                        <input type="text" class="form-control" id="start_data_1" name="start_data[]" disabled />
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="input-group date form_time" >
                                                        <input type="text" class="form-control" id="start_hora_1" name="start_hora[]" disabled />
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-time"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group col-lg-6">
                                                <div class="col-lg-1">
                                                    <label for="end_data_1" class="input-group">Término: </label>
                                                </div>
                                                <div class="col-lg-5">
                                                    <div class="input-group date form_date2" >
                                                        <input type="text" class="form-control" id="end_data_1" name="end_data[]" disabled />
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="input-group date form_time2" >
                                                        <input type="text" class="form-control" id="end_hora_1" name="end_hora[]" onblur="validarData(1)" disabled />
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-time"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                                <a id="addDatas" class="pull-right btn btn-primary btn-sm pull-right disabled" alt="" >
                                                    <i class="glyphicon glyphicon-plus-sign"></i> Datas
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="warnings_1" class="alert alert-danger alert-dismissible col-lg-12" style="display:none;text-align:center">
                                        <strong>Data e Hora indisponível</strong>
                                    </div>
                                    
                                </div>
                                <div class="panel-footer">
                                    Todos os campos marcados com <span id="obrigatorio" style="color: #ff0000;">*</span> são origatórios.
                                </div>
                            </div>
                            <div id="btn_agendar" class="form-group col-lg-12" >
                                <div class="col-lg-2">
                                    <button class="btn btn-primary uppercase" type="submit" name="dados_agendamento" id="dados_agendamento" disabled>Agendar</button>
                                </div>
                                <div class="col-lg-10"></div>
                            </div>
                        </form>
                    </div>
                <div class="row col-lg-12"><br></div>
            </div>
        </div>
        <?php include(kohana::find_file("views/templates/rsa", "footer", "php")) ?>

        <script>
            jQuery(document).ready(function ()
            {
                // Remove seleção de ativo no menu.
                $('.navbar-nav li').removeClass('active');
                $('#menu-superior li').removeClass('active');
                // Ativa botão no menu.
                $('.navbar-nav #mn_realizar').addClass('active');
                
                $('#idArea').on('change', function () {
                    $('#id_sala').load('salas?idArea=' + $('#idArea').val());
                });
                $('#id_sala').on('change', function () {
                    $('#addDatas').removeClass('disabled');
                    $('#start_data_1').removeAttr('disabled');
                    $('#start_hora_1').removeAttr('disabled');
                    $('#end_data_1').removeAttr('disabled');
                    $('#end_hora_1').removeAttr('disabled');
                    $('#dados_agendamento').removeAttr('disabled');
                });
                var divContentDatas = $('#DatasAdicionais');
                var botaoAdicionarData = $('#addDatas');

                var ContDatas = 2;
                //Ao clicar em adicionar ele cria uma linha com novos campos
                $(botaoAdicionarData).click(function () {
                    var codData = '<div class="col-lg-12">\n\
                                        <div id="DataAdicional"  class="form-group">\n\
                                            <input type="hidden" class="form-control" id="cont" name="cont" value="0"/>\n\
                                            <div class="form-group col-lg-6">\n\
                                                <div class="col-lg-1">\n\
                                                    <label for="start_data_' + ContDatas + '" class="input-group">Início: </label>\n\
                                                </div>\n\
                                                <div class="col-lg-5">\n\
                                                    <div class="input-group date form_date">\n\
                                                        <input type="text" class="form-control" id="start_data_' + ContDatas + '" name="start_data[]" />\n\
                                                        <span class="input-group-addon">\n\
                                                            <span class="glyphicon glyphicon-calendar"></span>\n\
                                                        </span>\n\
                                                    </div>\n\
                                                </div>\n\
                                                <div class="col-lg-4">\n\
                                                    <div class="input-group date form_time" >\n\
                                                        <input type="text" class="form-control" id="start_hora_' + ContDatas + '" name="start_hora[]" />\n\
                                                        <span class="input-group-addon">\n\
                                                            <span class="glyphicon glyphicon-time"></span>\n\
                                                        </span>\n\
                                                    </div>\n\
                                                </div>\n\
                                            </div>\n\
                                            <div class="form-group col-lg-6">\n\
                                                <div class="col-lg-1">\n\
                                                    <label for="end_data_' + ContDatas + '" class="input-group">Término: </label>\n\
                                                </div>\n\
                                                <div class="col-lg-5">\n\
                                                    <div class="input-group date form_date2" >\n\
                                                        <input type="text" class="form-control" id="end_data_' + ContDatas + '" name="end_data[]" />\n\
                                                        <span class="input-group-addon">\n\
                                                            <span class="glyphicon glyphicon-calendar"></span>\n\
                                                        </span>\n\
                                                    </div>\n\
                                                </div>\n\
                                                <div class="col-lg-4">\n\
                                                    <div class="input-group date form_time2" >\n\
                                                        <input type="text" class="form-control" id="end_hora_' + ContDatas + '" name="end_hora[]" onblur="validarData(1)" />\n\
                                                        <span class="input-group-addon">\n\
                                                            <span class="glyphicon glyphicon-time"></span>\n\
                                                        </span>\n\
                                                    </div>\n\
                                                </div>\n\
                                            <a id="linkRemoverData" class="pull-right btn btn-danger btn-sm pull-right">\n\
                                                <i class="fa fa-minus-circle"></i>\n\
                                            </a>\n\
                                            </div>\n\
                                        </div>\n\
                                    </div>\n\
                                    <div id="warnings_' + ContDatas + '" class="alert alert-danger alert-dismissible col-lg-12" style="display:none;text-align:center">\n\
                                        <strong>Data e Hora indisponível</strong>\n\
                                    </div>\n\
                                    ';
                    $(codData).appendTo(divContentDatas);
                    $(".form_date_" + ContDatas).datetimepicker({
                        locale: 'pt-BR',
                        format: "D/M/Y"
                    });
                    $(".form_time_" + ContDatas).datetimepicker({
                        format: 'LT',
                        disabledHours: [0, 1, 2, 3, 4, 5, 6, 24],
                        enabledHours: [7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23],
                        useCurrent: false,
                        locale: 'pt-BR'
                    });
                    $(".form_date2_" + ContDatas).datetimepicker({
                        locale: 'pt-BR',
                        format: "D/M/Y"
                    });
                    $(".form_time2_" + ContDatas).datetimepicker({
                        format: 'LT',
                        disabledHours: [0, 1, 2, 3, 4, 5, 6, 24],
                        enabledHours: [7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23],
                        useCurrent: false,
                        locale: 'pt-BR'
                    });
                    ContDatas++;

                });
                //Cliquando em remover a linha é eliminada

                $(divContentDatas).on('click', '#linkRemoverData', function () {
                    $(this).parents('#DataAdicional').remove();
                    ContDatas--;
                });
            });
            function validarData(cont) {
                var start_data = $("#start_data_" + cont).val() + " " + $("#start_hora_" + cont).val();
                var end_data = $("#end_data_" + cont).val() + " " + $("#end_hora_" + cont).val();
                var idSala = $('#id_sala').find('option:selected').val();
                var warning = $("#warnings_" + cont);
                if (idSala == "") {
                    alert("Selecione a Sala Primeiro");
                } else {
                    $.ajax('conciliar',
                            {
                                type: "POST",
                                data:
                                        {
                                            start_data: start_data,
                                            end_data: end_data,
                                            idSala: idSala
                                        }
                            }).done(function (result) {

                        if (result != "") {
                            warning.show();
                        } else {
                            warning.hide();
                        }
                    });
                }
            }
        </script>

    </body>
</html>
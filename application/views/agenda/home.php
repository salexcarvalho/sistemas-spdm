<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?= SISTEMA; ?> - Agendamento de Salas</title>
        <?php include(kohana::find_file('views/templates/rsa', 'init', 'php')) ?>
        <script type="text/javascript" src="<?= URL::base(); ?>theme/frontend/js/reservas.js"></script> 
        <script>
            jQuery(document).ready(function ()
            {
                // Remove seleção de ativo no menu.
                $('.navbar-nav li').removeClass('active');
                $('#menu-superior li').removeClass('active');
                // Ativa botão no menu.
                $('.navbar-nav #mn_home').addClass('active');
            });
        </script>
        <style>
    label {
        float: none;
    }
    
</style>
    </head>
    <body>
        <div class="content">
            <?php include(kohana::find_file('views/templates/rsa', 'header', 'php')) ?>
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/rsa', 'menu', 'php')) ?>
            <!-- Fim do Menu -->

            <div class="container" >
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="bold uppercase azul" st>Agenda de Espaços</h4>
                        <hr style="width:98%;">
                    </div>
                </div>
                    <div class="row col-lg-12">
                            <div class="form-group col-md-6"></div>
                            <div class="form-group col-md-2">
                                <label>Filtro de Salas:</label>
                            </div>
                            <div class="form-group col-md-4">
                                <select id="eventByResource" class="form-control">
                                    <option style="color:#fff;background-color:#000;">Todas</option>
                                    <?php foreach ($salas as $sala): ?>                                            
                                        <option style="color:#fff;background-color:#<?= $sala['rgb']; ?>;" value="<?= $sala['idSalas']; ?>"><?= $sala['Alias'] ?> - <?= $sala['nome'] ?></option>  
                                    <?php endforeach; ?>
                                </select>                                         
                            </div>
                        
                        <br><br><br>                              
                    </div>
                    <div class="row col-lg-12">
                        <div id='calendar'></div>
                        <br><br>
                    </div>
            </div>

        </div>
    </div>
    <?php include(kohana::find_file('views/templates/rsa', 'footer', 'php')) ?>
</body>
</html>
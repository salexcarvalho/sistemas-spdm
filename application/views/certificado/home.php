<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?= SISTEMA; ?> - Validador de Autenticidade</title>
        <?php include(kohana::find_file('views/templates/certificado', 'init', 'php')) ?>
    </head>
    <body>
        <?php include(kohana::find_file('views/templates/certificado', 'header', 'php')) ?>

        <div class="container" >
            <div id="txtcentral">
                <div class="row">
                    <div class="form-group" >
                        <div class="col-md-3"></div>
                        <div class="col-md-5">
                            <input name="autenticacao" id="autenticacao" class="form-control input-lg" placeholder="Informe o Código aqui" required>
                        </div>
                        <div class="col-md-1"><button type="submit" name="btnValidacao" id="btnValidacao" class="btn btn-primary btn-lg">Validar</button></div>
                        <div class="col-md-3"></div>
                    </div>
                </div>    
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-5">
                        <div id="erro" class="alert alert-danger alert-dismissible hidden" role="alert">
                            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <strong>Atenção!</strong> Certificado não encontrado!<br>
                            Esta tela será atualizada em <span id="counterro">5</span> segundos.
                        </div>
                        <div id="sucesso" class="alert alert-success alert-dismissible hidden" role="alert">
                            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <strong>Parabéns!</strong> Seu certificado está sendo gerado e será aberto em <span id="count">5</span> segundos.
                        </div>
                        <span id="msginicial" class="help-block" >
                            &nbsp;&nbsp;&nbsp; Infome o código do certficado (Localizado no rodapé)
                        </span> 
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-3"></div>
                </div>
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-5"></div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-3"></div>
            </div>
        </div>

    </div>
</div>
<?php include(kohana::find_file('views/templates/certificado', 'footer', 'php')) ?>

<script>

    jQuery(document).ready(function ()
    {
        // Verifica se tem o certificado
        $("body").on("click", "button[name=btnValidacao]", function ()
        {
            var autenticacao = $('#autenticacao').val();
            $('button#btnValidacao').text("Processando");
            $.ajax("gerapdf",
                    {
                        type: "get",
                        data:
                                {
                                    autenticacao: autenticacao
                                },
                        success: function () {
                            $('#sucesso').addClass('visible-lg');
                            $('#sucesso').removeClass('hidden');

                            var counter = 5;
                            setInterval(function () {
                                counter--;
                                if (counter >= 0) {
                                    span = document.getElementById("count");
                                    span.innerHTML = counter;
                                }
                                // Display 'counter' wherever you want to display it.
                                if (counter === 0) {
                                    //window.open('gerapdf?autenticacao=' + autenticacao, '_blank');
                                    window.location.replace('gerapdf?autenticacao=' + autenticacao, '_blank');
                                    clearInterval(counter);
                                }

                            }, 1000);
                            $('button#btnValidacao').text("Validar");

                        },
                        error: function (data) {
                            $('button#btnValidacao').text("Validar");
                            $('#erro').addClass('visible-lg');
                            $('#erro').removeClass('hidden');
                            var counter = 5;
                            setInterval(function () {
                                counter--;
                                if (counter >= 0) {
                                    span = document.getElementById("counterro");
                                    span.innerHTML = counter;
                                }
                                if (counter === 0) {
                                    window.location.replace("home");
                                    clearInterval(counter);
                                }

                            }, 1000);
                        }
                    });
        });
    });

</script>

</body>
</html>
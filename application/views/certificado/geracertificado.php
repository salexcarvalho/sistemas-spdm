<?php if (($apoio[0]["idOrigem"]) == "1") { ?>

    <!-- SPDM PRESIDÊNCIA -->
    <style type="text/css">
        <!--
        table {width: 100%; border: none;}
        #fundo { position: relative; z-index: -1000;}
        #corpo { width: 918px; position: absolute; margin-top: 270px; padding-left: 100px; padding-right: 110px; font-size: 24px; line-height: 32px; text-align: justify;}
        span {color: #cb2027; text-transform: uppercase;}
        #assleft  {width: 562px; margin-top: 630px; font-size: 12px; line-height: 21px; position: absolute; margin-left: 10px; text-align: center;}
        #assright {width: 562px; margin-top: 630px; font-size: 12px; line-height: 21px; position: absolute; margin-left: 552px; text-align: center;}
        #asspng {width: 562px; margin-top: 565px; position: absolute; margin-left: 562px; text-align: center;}
        #info01 {width: 500px; margin-top: 725px; position: absolute; margin-left: 40px; font-size: 13px;}
        #info02 {width: 500px; margin-top: 745px; position: absolute; margin-left: 40px; font-size: 10px;}
        -->
    </style>
    <page backtop="-1mm" backbottom="-20mm" backleft="-1mm" backright="-4mm">
        <table style="width: 100%;">
            <tr>
                <td>
                        <div id="fundo"><img src="upload/img/certificados/<?= $certificado[0]["imgCertificado"]; ?>" width="1124" height="793" alt=""/></div>
                    <div id="corpo">
                        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Certificamos que <span><?= mb_strtoupper($participante[0]["titulacao"] . " " . $participante[0]["Nome"]); ?></span>, <?= strip_tags($certificado[0]["TextoPadrao"], '<strong>'); ?>, na qualidade de <?= $tipo[0]["Alias"]; ?><?php if (($tipo[0]["Alias"]) == "PALESTRANTE") { ?> com o tema <?= $apoio[0]["tema"]; ?>, no dia <?=strftime('%d de %B de %Y', strtotime($apoio[0]["data"]));
                            }
                            ?>.

                            <br><br>
                            <strong>Carga horária:</strong> <?= $certificado[0]["cargaHoraria"]; ?>
                            <br><br>
                            São Paulo, <?= strftime('%d de %B de %Y', strtotime($certificado[0]["dataEmissao"])); ?></p>
                    </div>
                    <div id="assleft">
                        __________________________________________________
                        <br>
                        Assinatura do participante
                    </div>
                    <div id="assright">
                        __________________________________________________
                        <br>
                        <?= $dados01[0]["titulacao"]; ?>&nbsp;<?= $dados01[0]["nome"]; ?>
                        <br>
                        <?= $dados01[0]["cargo"]; ?>
                    </div>
                    <div id="asspng">
                        <img src="upload/img/certificados/<?= $dados01[0]["PngAssinatura"]; ?>" width="280" />
                    </div>
                    <div id="info01">
                        Nº de Autenticação: <?= $apoio[0]["Autenticacao"]; ?>
                    </div>
                    <div id="info02">
                        Para verificar a autenticidade deste certificado, acesse: www.spdm.org.br/sistemas/certificado
                    </div>
                </td>
            </tr>
        </table>
    </page>

<?php } else if (($apoio[0]["idOrigem"]) == "2") { ?>

    <!-- SPDM EDUCAÇÃO -->
    <style type="text/css">
        table {width: 100%; border: none;}
        #fundo { position: relative; z-index: -1000;}
        #certicamos { width: 1124px; position: absolute; margin-top: 240px; padding-left: 0px; padding-right: 110px; font-size: 18px; line-height: 20px; text-align: center;}
        #nome { width: 1124px; position: absolute; margin-top: 275px; padding-left: 0px; font-size: 23px; line-height: 20px; text-align: center;}
        #corpo { width: 880px; height: 150px; position: absolute; margin-top: 300px; padding-left: 122px; font-size: 19px; line-height: 27px; text-align: center;}
        #carga { width: 880px; position: absolute; margin-top: 500px; padding-left: 102px; font-size: 20px; line-height: 25px; text-align: right;}
        span {color: #cb2027; text-transform: uppercase;}
        #info01 {width: 500px; margin-top: 710px; position: absolute; margin-left: 60px; font-size: 11px;}
        #info02 {width: 500px; margin-top: 725px; position: absolute; margin-left: 60px; font-size: 9px;}
        #assleft  {width: 562px; margin-top: 605px; font-size: 12px; line-height: 21px; position: absolute; margin-left: 50px; text-align: center;}
        #assright {width: 562px; margin-top: 605px; font-size: 12px; line-height: 21px; position: absolute; margin-left: 512px; text-align: center;}
        #asspngleft {width: 562px; margin-top: 560px; position: absolute; margin-left: 50px; text-align: center;}
        #asspng {width: 562px; margin-top: 560px; position: absolute; margin-left: 512px; text-align: center;}
    </style>
    <page backtop="-1mm" backbottom="-20mm" backleft="-1mm" backright="-4mm">
        <table style="width: 100%;">
            <tr>
                <td>
                    <div id="fundo"><img src="upload/img/certificados/<?= $certificado[0]["imgCertificado"]; ?>" width="1124" height="793" alt=""/></div>
                    <div id="certicamos">Certificamos que</div>
                    <div id="nome"><strong><span><?= mb_strtoupper($participante[0]["titulacao"] . " " . $participante[0]["Nome"]); ?></span></strong></div>
                    <div id="corpo">
                        <p>
                            <?= strip_tags($certificado[0]["TextoPadrao"], '<strong>'); ?>, na qualidade de <strong><?= $tipo[0]["Alias"]; ?></strong><?php
                            switch ($tipo[0]["Alias"]) {
                                case 'PALESTRANTE':
                                    ?><?php if (($apoio[0]["modulo"]) != "") { ?> do <strong><?= $apoio[0]["modulo"]; ?></strong><?php } ?><?php if (($apoio[0]["tema"]) != "") { ?>, com o tema: "<strong><?= $apoio[0]["tema"]; ?></strong>"<?php } ?><?php if (($apoio[0]["livre"]) != "") { ?>, <?= str_replace(";", ",", $apoio[0]["livre"]); ?>.<?php } else { ?>, ministrada em <?= strftime('%d de %B de %Y', strtotime($apoio[0]["data"])); ?>.<?php } ?>                                
                                    <?php break;
                                case 'PRESIDENTE':
                                    ?><?php if (($apoio[0]["livre"]) != "") { ?> <?= str_replace(";", ",", $apoio[0]["livre"]); ?>.<?php } else { ?>.<?php } ?>
                                    <?php break;
                                case 'COORDENADOR':
                                    ?><?php if (($apoio[0]["livre"]) != "") { ?> <?= str_replace(";", ",", $apoio[0]["livre"]); ?>.<?php } else { ?>.<?php } ?>
                                    <?php break;
                                case 'COORDENADORA':
                                    ?><?php if (($apoio[0]["livre"]) != "") { ?> <?= str_replace(";", ",", $apoio[0]["livre"]); ?>.<?php } else { ?>.<?php } ?>
                                    <?php break;
                                case 'MODERADORA':
                                    ?><?php if (($apoio[0]["tema"]) != "") { ?> da mesa redonda com o tema "<strong><?= $apoio[0]["tema"]; ?></strong>"<?php if (($apoio[0]["livre"]) != "") { ?>, <?= str_replace(";", ",", $apoio[0]["livre"]); ?>.<?php } ?><?php if (($apoio[0]["data"]) != "") { ?>, ministrada em <?= strftime('%d de %B de %Y', strtotime($apoio[0]["data"])); ?>.<?php } ?><?php } else { ?>.<?php } ?>         
                                    <?php break;
                                case 'MODERADOR':
                                    ?><?php if (($apoio[0]["tema"]) != "") { ?> da mesa redonda com o tema "<strong><?= $apoio[0]["tema"]; ?></strong>"<?php if (($apoio[0]["livre"]) != "") { ?>, <?= str_replace(";", ",", $apoio[0]["livre"]); ?>.<?php } ?><?php if (($apoio[0]["data"]) != "") { ?>, ministrada em <?= strftime('%d de %B de %Y', strtotime($apoio[0]["data"])); ?>.<?php } ?><?php } else { ?>.<?php } ?>
                                    <?php break;
                                case 'COMPONENTE DA MESA':
                                    ?><?php if (($apoio[0]["tema"]) != "") { ?> com o tema "<strong><?= $apoio[0]["tema"]; ?></strong>"<?php if (($apoio[0]["livre"]) != "") { ?>, <?= str_replace(";", ",", $apoio[0]["livre"]); ?><?php } ?><?php if (($apoio[0]["data"]) != "") { ?>, ministrada em <?= strftime('%d de %B de %Y', strtotime($apoio[0]["data"])); ?><?php } ?>.<?php } else { ?>.<?php } ?>
                                    <?php break;
                                case 'DEBATEDOR':
                                    ?><?php if (($apoio[0]["tema"]) != "") { ?> da mesa redonda com o tema "<strong><?= $apoio[0]["tema"]; ?></strong>"<?php if (($apoio[0]["livre"]) != "") { ?>, <?= str_replace(";", ",", $apoio[0]["livre"]); ?>.<?php } ?><?php if (($apoio[0]["data"]) != "") { ?>, ministrada em <?= strftime('%d de %B de %Y', strtotime($apoio[0]["data"])); ?>.<?php } ?><?php } else { ?>.<?php } ?>
                                    <?php break;
                                case 'DEBATEDORA':
                                    ?><?php if (($apoio[0]["tema"]) != "") { ?>
                                        da mesa redonda com o tema "<strong><?= $apoio[0]["tema"]; ?></strong>"<?php if (($apoio[0]["livre"]) != "") { ?>, <?= str_replace(";", ",", $apoio[0]["livre"]); ?>.<?php } ?><?php if (($apoio[0]["data"]) != "") { ?>, ministrada em <?= strftime('%d de %B de %Y', strtotime($apoio[0]["data"])); ?>.<?php } ?><?php } else { ?>.<?php } ?>
                                    <?php break;
                                case 'PARTICIPANTE DA MESA DE ABERTURA':
                                    ?><?php if (($apoio[0]["tema"]) != "") { ?>
                                        da mesa redonda com o tema "<strong><?= $apoio[0]["tema"]; ?></strong>"<?php if (($apoio[0]["livre"]) != "") { ?>, <?= str_replace(";", ",", $apoio[0]["livre"]); ?>.<?php } ?><?php if (($apoio[0]["data"]) != "") { ?>, ministrada em <?= strftime('%d de %B de %Y', strtotime($apoio[0]["data"])); ?>.<?php } ?><?php } else { ?>.<?php } ?>
                                    <?php break;
                                case 'ALUNO':
                                    ?><?php if (($apoio[0]["modulo"]) != "") { ?> do <strong><?= $apoio[0]["modulo"]; ?></strong><?php } ?><?php if (($apoio[0]["livre"]) != "") { ?>, <?= str_replace(";", ",", $apoio[0]["livre"]); ?>.<?php } else { ?>.<?php } ?>
                                    <?php break;
                                case 'PROFESSOR(A)':
                                    ?><?php if (($apoio[0]["livre"]) != "") { ?>, <?= str_replace(";", ",", $apoio[0]["livre"]); ?>.<?php } else { ?>.<?php } ?>
                                    <?php break;
                                default:
                                    echo "."; 
                                    }?>
                        </p>
                    </div>
    <?php if (($certificado[0]["cargaHoraria"]) != "") { ?>
                        <div id="carga">
                            <p><strong>Carga horária:</strong> <?= $certificado[0]["cargaHoraria"]; ?></p>
                        </div>
    <?php } ?>
                    <div id="data">
                        <p>São Paulo, <?= strftime('%d de %B de %Y', strtotime($certificado[0]["dataEmissao"])); ?></p>
                    </div>
                        <?php if (($dados04[0]["nome"]) != NULL) { ?>
                        <style type="text/css">
                            #ass01  {width: 246px; margin-top: 615px; font-size: 11px; line-height: 18px; position: absolute; margin-left: 60px; text-align: center;}
                            #ass02 {width: 246px; margin-top: 615px; font-size: 11px; line-height: 18px; position: absolute; margin-left: 316px; text-align: center;}
                            #ass03 {width: 246px; margin-top: 615px; font-size: 11px; line-height: 18px; position: absolute; margin-left: 572px; text-align: center;}
                            #ass04 {width: 246px; margin-top: 615px; font-size: 11px; line-height: 18px; position: absolute; margin-left: 818px; text-align: center;}
                            #asspng01 {width: 246px; margin-top: 580px; position: absolute; margin-left: 60px; text-align: center;}
                            #asspng02 {width: 246px; margin-top: 580px; position: absolute; margin-left: 316px; text-align: center;}
                            #asspng03 {width: 246px; margin-top: 580px; position: absolute; margin-left: 572px; text-align: center;}
                            #asspng04 {width: 246px; margin-top: 580px; position: absolute; margin-left: 818px; text-align: center;}
                            #data { width: 800px; position: absolute; margin-top: 500px; padding-left: 162px; font-size: 20px; line-height: 25px; text-align: left;}
                        </style> 
                        <div id="ass01">
                            <br>
                            <?= $dados01[0]["titulacao"]; ?>&nbsp;<?= $dados01[0]["nome"]; ?>
                            <br>
                            <?= $dados01[0]["cargo"]; ?>
                        </div>
                        <div id="ass02"> <!-- Principal -->
                            <br>
                            <?= $dados02[0]["titulacao"]; ?>&nbsp;<?= $dados02[0]["nome"]; ?>
                            <br>
                            <?= $dados02[0]["cargo"]; ?>
                        </div>
                        <div id="ass03">
                            <br>
                            <?= $dados03[0]["titulacao"]; ?>&nbsp;<?= $dados03[0]["nome"]; ?>
                            <br>
                            <?= $dados03[0]["cargo"]; ?>
                        </div>
                        <div id="ass04">
                            <br>
                            <?= $dados04[0]["titulacao"]; ?>&nbsp;<?= $dados04[0]["nome"]; ?>
                            <br>
                            <?= $dados04[0]["cargo"]; ?>
                        </div>
                        
                        <div id="asspng01">
                            <img src="upload/img/certificados/<?= $dados01[0]["PngAssinatura"]; ?>" width="200" />
                        </div>
                        <div id="asspng02">
                            <img src="upload/img/certificados/<?= $dados02[0]["PngAssinatura"]; ?>" width="200" />
                        </div>
                        <div id="asspng03">
                            <img src="upload/img/certificados/<?= $dados03[0]["PngAssinatura"]; ?>" width="200" />
                        </div>
                        <div id="asspng04">
                            <img src="upload/img/certificados/<?= $dados04[0]["PngAssinatura"]; ?>" width="200" />
                        </div>
<?php } else if (($dados03[0]["nome"]) != NULL) { ?>
                        <style type="text/css">
                            #ass01  {width: 374px; margin-top: 615px; font-size: 12px; line-height: 21px; position: absolute; margin-left: 50px; text-align: center;}
                            #ass02 {width: 374px; margin-top: 615px; font-size: 12px; line-height: 21px; position: absolute; margin-left: 376px; text-align: center;}
                            #ass03 {width: 374px; margin-top: 615px; font-size: 12px; line-height: 21px; position: absolute; margin-left: 700px; text-align: center;}
                            #asspng01 {width: 374px; margin-top: 580px; position: absolute; margin-left: 50px; text-align: center;}
                            #asspng02 {width: 374px; margin-top: 580px; position: absolute; margin-left: 376px; text-align: center;}
                            #asspng03 {width: 374px; margin-top: 580px; position: absolute; margin-left: 700px; text-align: center;}
                            #data { width: 800px; position: absolute; margin-top: 500px; padding-left: 162px; font-size: 20px; line-height: 25px; text-align: left;}
                        </style> 
                        <div id="ass01">
                            <br>
                            <?= $dados01[0]["titulacao"]; ?>&nbsp;<?= $dados01[0]["nome"]; ?>
                            <br>
                            <?= $dados01[0]["cargo"]; ?>
                        </div>
                        <div id="ass02"> <!-- Principal -->
                            <br>
                            <?= $dados02[0]["titulacao"]; ?>&nbsp;<?= $dados02[0]["nome"]; ?>
                            <br>
                            <?= $dados02[0]["cargo"]; ?>
                        </div>
                        <div id="ass03">
                            <br>
                            <?= $dados03[0]["titulacao"]; ?>&nbsp;<?= $dados03[0]["nome"]; ?>
                            <br>
                            <?= $dados03[0]["cargo"]; ?>
                        </div>

                        <div id="asspng01">
                            <img src="upload/img/certificados/<?= $dados01[0]["PngAssinatura"]; ?>" width="240" />
                        </div>
                        <div id="asspng02">
                            <img src="upload/img/certificados/<?= $dados02[0]["PngAssinatura"]; ?>" width="240" />
                        </div>
                        <div id="asspng03">
                            <img src="upload/img/certificados/<?= $dados03[0]["PngAssinatura"]; ?>" width="240" />
                        </div>
                        <?php } else { ?>
                        <style type="text/css">
                            #data { width: 800px; position: absolute; margin-top: 470px; padding-left: 162px; font-size: 20px; line-height: 25px; text-align: left;}
                        </style> 
                        <div id="assleft">
                            <br>
                            <?= $dados01[0]["titulacao"]; ?>&nbsp;<?= $dados01[0]["nome"]; ?>
                            <br>
                            <?= $dados01[0]["cargo"]; ?>
                        </div>
                        <div id="assright">
                            <br>
                            <?= $dados02[0]["titulacao"]; ?>&nbsp;<?= $dados02[0]["nome"]; ?>
                            <br>
                            <?= $dados02[0]["cargo"]; ?>
                        </div>

                        <div id="asspngleft">
                            <img src="upload/img/certificados/<?= $dados01[0]["PngAssinatura"]; ?>" width="240" />
                        </div>
                        <div id="asspng">
                            <img src="upload/img/certificados/<?= $dados02[0]["PngAssinatura"]; ?>" width="240" />
                        </div>
<?php } ?>
                    <div id="info01">
                        Nº de Autenticação: <?= $apoio[0]["Autenticacao"]; ?>
                    </div>
                    <div id="info02">
                        Para verificar a autenticidade deste certificado, acesse: www.spdm.org.br/sistemas/certificado
                    </div>

                </td>
            </tr>
        </table>
    </page>
<?php } else if (($apoio[0]["idOrigem"]) == "8") { ?>    
   
    <!-- HGG -->
    <style type="text/css">
        table {width: 100%; border: none;}
        #fundo { position: relative; z-index: -1000;}
        #certicamos { width: 1124px; position: absolute; margin-top: 236px; padding-left: 0px; padding-right: 110px; font-size: 18px; line-height: 20px; text-align: center;}
        #nome { width: 1124px; position: absolute; margin-top: 275px; padding-left: 0px; font-size: 23px; line-height: 20px; text-align: center;}
        #corpo { width: 880px; height: 150px; position: absolute; margin-top: 300px; padding-left: 122px; font-size: 19px; line-height: 27px; text-align: center;}
        #carga { width: 880px; position: absolute; margin-top: 500px; padding-left: 102px; font-size: 20px; line-height: 25px; text-align: right;}
        span {color: #cb2027; text-transform: uppercase;}
        #info01 {width: 500px; margin-top: 710px; position: absolute; margin-left: 60px; font-size: 11px;}
        #info02 {width: 500px; margin-top: 725px; position: absolute; margin-left: 60px; font-size: 9px;}
        #assleft  {width: 1124px; margin-top: 605px; font-size: 12px; line-height: 21px; position: absolute; margin-left: 0px; text-align: center;}
        #asspngleft {width: 1124px; margin-top: 560px; position: absolute; margin-left: 0px; text-align: center;}
    </style>
    <page backtop="-1mm" backbottom="-20mm" backleft="-1mm" backright="-4mm">
        <table style="width: 100%;">
            <tr>
                <td>
                    <div id="fundo"><img src="upload/img/certificados/<?= $certificado[0]["imgCertificado"]; ?>" width="1124" height="793" alt=""/></div>
                    <div id="certicamos">Este certificado está sendo conferido à</div>
                    <div id="nome"><strong><span><?= mb_strtoupper($participante[0]["titulacao"] . " " . $participante[0]["Nome"]); ?></span></strong></div>
                    <div id="corpo">
                        <p><?= strip_tags($certificado[0]["TextoPadrao"], '<strong>'); ?><?php if (($apoio[0]["livre"]) != "") { ?> <?= str_replace(";", ",", $apoio[0]["livre"]); ?>.<?php } else { ?>.<?php } ?></p>
                        <p><strong>ASSOCIAÇÃO PAULISTA PARA O DESENVOLVIMENTO DA MEDICINA<br>INSTITUIÇÕES AFILIADAS</strong></p>
                    </div>
                    <?php if (($certificado[0]["cargaHoraria"]) != "") { ?>
                        <div id="carga">
                            <p><strong>Carga horária:</strong> <?= $certificado[0]["cargaHoraria"]; ?></p>
                        </div>
                    <?php } ?>
                    <div id="data">
                        <p>São Paulo, <?php if (($apoio[0]["data"]) != "") { ?> <?= strftime('%d de %B de %Y', strtotime($apoio[0]["data"])); ?><?php } else { ?><?= strftime('%d de %B de %Y', strtotime($certificado[0]["dataEmissao"])); ?><?php } ?></p>
                    </div>
                        <style type="text/css">
                            #data { width: 800px; position: absolute; margin-top: 510px; padding-left: 162px; font-size: 20px; line-height: 25px; text-align: left;}
                        </style> 
                        <div id="assleft">
                            <br>
                            <?= $dados01[0]["titulacao"]; ?><strong><?= $dados01[0]["nome"]; ?></strong>
                            <br>
                            <?= $dados01[0]["cargo"]; ?>
                        </div>
                        
                        <div id="asspngleft">
                            <img src="upload/img/certificados/<?= $dados01[0]["PngAssinatura"]; ?>" width="240" />
                        </div>
                        
                    <div id="info01">
                        Nº de Autenticação: <?= $apoio[0]["Autenticacao"]; ?>
                    </div>
                    <div id="info02">
                        Para verificar a autenticidade deste certificado, acesse: www.spdm.org.br/sistemas/certificado
                    </div>

                </td>
            </tr>
        </table>
    </page>  

<?php } else if (($apoio[0]["idOrigem"]) == "11") { ?>    
   
    <!-- Compliance -->
    <style type="text/css">
        table {width: 100%; border: none;}
        #fundo { position: relative; z-index: -1000;}
        #certicamos { width: 1124px; position: absolute; margin-top: 236px; padding-left: 0px; padding-right: 110px; font-size: 18px; line-height: 20px; text-align: center;}
        #nome { width: 1124px; position: absolute; margin-top: 275px; padding-left: 0px; font-size: 23px; line-height: 20px; text-align: center;}
        #corpo { width: 880px; height: 150px; position: absolute; margin-top: 300px; padding-left: 122px; font-size: 19px; line-height: 27px; text-align: center;}
        #carga { width: 880px; position: absolute; margin-top: 500px; padding-left: 102px; font-size: 20px; line-height: 25px; text-align: right;}
        span {color: #cb2027; text-transform: uppercase;}
        #info01 {width: 500px; margin-top: 710px; position: absolute; margin-left: 60px; font-size: 11px;}
        #info02 {width: 500px; margin-top: 725px; position: absolute; margin-left: 60px; font-size: 9px;}
        #assleft  {width: 1124px; margin-top: 605px; font-size: 12px; line-height: 21px; position: absolute; margin-left: 0px; text-align: center;}
        #asspngleft {width: 1124px; margin-top: 560px; position: absolute; margin-left: 0px; text-align: center;}
    </style>
    <page backtop="-1mm" backbottom="-20mm" backleft="-1mm" backright="-4mm">
        <table style="width: 100%;">
            <tr>
                <td>
                    <div id="fundo"><img src="upload/img/certificados/<?= $certificado[0]["imgCertificado"]; ?>" width="1124" height="793" alt=""/></div>
                    <div id="certicamos">Este certificado está sendo conferido à</div>
                    <div id="nome"><strong><span><?= mb_strtoupper($participante[0]["titulacao"] . " " . $participante[0]["Nome"]); ?></span></strong></div>
                    <div id="corpo">
                        <p><?= strip_tags($certificado[0]["TextoPadrao"], '<strong>'); ?><?php if (($apoio[0]["livre"]) != "") { ?> <?= str_replace(";", ",", $apoio[0]["livre"]); ?>.<?php } else { ?>.<?php } ?></p>
                        <p><strong>ASSOCIAÇÃO PAULISTA PARA O DESENVOLVIMENTO DA MEDICINA<br>SISTEMA DE COMPLIANCE</strong></p>
                    </div>
                    <?php if (($certificado[0]["cargaHoraria"]) != "") { ?>
                        <div id="carga">
                            <p><strong>Carga horária:</strong> <?= $certificado[0]["cargaHoraria"]; ?></p>
                        </div>
                    <?php } ?>
                    <div id="data">
                        <p>São Paulo, <?php if (($apoio[0]["data"]) != "") { ?> <?= strftime('%d de %B de %Y', strtotime($apoio[0]["data"])); ?><?php } else { ?><?= strftime('%d de %B de %Y', strtotime($certificado[0]["dataEmissao"])); ?><?php } ?></p>
                    </div>
                        <style type="text/css">
                            #data { width: 800px; position: absolute; margin-top: 510px; padding-left: 162px; font-size: 20px; line-height: 25px; text-align: left;}
                        </style> 
                        <div id="assleft">
                            <br>
                            <strong><?= $dados01[0]["titulacao"]; ?> <?= $dados01[0]["nome"]; ?></strong>
                            <br>
                            <?= $dados01[0]["cargo"]; ?>
                        </div>
                        
                        <div id="asspngleft">
                            <img src="upload/img/certificados/<?= $dados01[0]["PngAssinatura"]; ?>" width="240" />
                        </div>
                        
                    <div id="info01">
                        Nº de Autenticação: <?= $apoio[0]["Autenticacao"]; ?>
                    </div>
                    <div id="info02">
                        Para verificar a autenticidade deste certificado, acesse: www.spdm.org.br/sistemas/certificado
                    </div>

                </td>
            </tr>
        </table>
    </page>  
    
    
<?php } else if (($apoio[0]["idOrigem"]) == "3") { ?>

    <!-- SPDM EDUCAÇÃO - TRABALHOS -->
    <style type="text/css">
        <!--
        table {width: 100%; border: none;}
        #fundo { position: relative; z-index: -1000;}
        #certicamos { width: 1124px; position: absolute; margin-top: 240px; padding-left: 0px; padding-right: 110px; font-size: 18px; line-height: 20px; text-align: center;}
        #nome { width: 1124px; position: absolute; margin-top: 275px; padding-left: 0px; font-size: 23px; line-height: 28px; text-align: center;}
        #corpo { width: 880px; position: absolute; margin-top: 330px; padding-left: 122px; font-size: 18px; line-height: 27px; text-align: center;}
        span {color: #cb2027;}
        #assleft  {width: 562px; margin-top: 625px; font-size: 12px; line-height: 21px; position: absolute; margin-left: 50px; text-align: center;}
        #assright {width: 562px; margin-top: 625px; font-size: 12px; line-height: 21px; position: absolute; margin-left: 512px; text-align: center;}
        #asspngleft {width: 562px; margin-top: 570px; position: absolute; margin-left: 50px; text-align: center;}
        #asspng {width: 562px; margin-top: 570px; position: absolute; margin-left: 512px; text-align: center;}
        #info01 {width: 500px; margin-top: 710px; position: absolute; margin-left: 60px; font-size: 11px;}
        #info02 {width: 500px; margin-top: 725px; position: absolute; margin-left: 60px; font-size: 9px;}
        -->
    </style>
    <page backtop="-1mm" backbottom="-20mm" backleft="-1mm" backright="-4mm">
        <table style="width: 100%;">
            <tr>
                <td>
                    <div id="fundo"><img src="upload/img/certificados/spdm_educacao_trabalhos.png" width="1124" height="793" alt=""/></div>
                    <div id="certicamos">Certificamos que o trabalho intitulado</div>
                    <div id="nome"><strong><span><?= $apoio[0]["tema"]; ?></span></strong></div>
                    <div id="corpo">
                        <p>dos autores <strong><?= $participante[0]["titulacao"] . " " . $participante[0]["Nome"]; ?></strong> <?= strip_tags($certificado[0]["TextoPadrao"], '<strong>'); ?><?php if (($apoio[0]["livre"]) != "") { ?> , <?= str_replace(";", ",", $apoio[0]["livre"]); ?><?php } ?>.
                        </p></div>
                    <div id="data">
                        <p>São Paulo, <?= strftime('%d de %B de %Y', strtotime($certificado[0]["dataEmissao"])); ?></p>
                    </div>

                        <?php if (($dados04[0]["nome"]) != NULL) { ?>
                        <style type="text/css">
                            #ass01  {width: 246px; margin-top: 615px; font-size: 11px; line-height: 18px; position: absolute; margin-left: 60px; text-align: center;}
                            #ass02 {width: 246px; margin-top: 615px; font-size: 11px; line-height: 18px; position: absolute; margin-left: 316px; text-align: center;}
                            #ass03 {width: 246px; margin-top: 615px; font-size: 11px; line-height: 18px; position: absolute; margin-left: 572px; text-align: center;}
                            #ass04 {width: 246px; margin-top: 615px; font-size: 11px; line-height: 18px; position: absolute; margin-left: 818px; text-align: center;}
                            #asspng01 {width: 246px; margin-top: 580px; position: absolute; margin-left: 60px; text-align: center;}
                            #asspng02 {width: 246px; margin-top: 580px; position: absolute; margin-left: 316px; text-align: center;}
                            #asspng03 {width: 246px; margin-top: 580px; position: absolute; margin-left: 572px; text-align: center;}
                            #asspng04 {width: 246px; margin-top: 580px; position: absolute; margin-left: 818px; text-align: center;}
                            #data { width: 800px; position: absolute; margin-top: 520px; padding-left: 162px; font-size: 19px; line-height: 25px; text-align: right;}
                        </style> 
                        <div id="ass01">
                            <br>
                            <?= $dados01[0]["titulacao"]; ?>&nbsp;<?= $dados01[0]["nome"]; ?>
                            <br>
                            <?= $dados01[0]["cargo"]; ?>
                        </div>
                        <div id="ass02"> <!-- Principal -->
                            <br>
                            <?= $dados02[0]["titulacao"]; ?>&nbsp;<?= $dados02[0]["nome"]; ?>
                            <br>
                            <?= $dados02[0]["cargo"]; ?>
                        </div>
                        <div id="ass03">
                            <br>
                            <?= $dados03[0]["titulacao"]; ?>&nbsp;<?= $dados03[0]["nome"]; ?>
                            <br>
                            <?= $dados03[0]["cargo"]; ?>
                        </div>
                        <div id="ass04">
                            <br>
                            <?= $dados04[0]["titulacao"]; ?>&nbsp;<?= $dados04[0]["nome"]; ?>
                            <br>
                            <?= $dados04[0]["cargo"]; ?>
                        </div>

                        <div id="asspng01">
                            <img src="upload/img/certificados/<?= $dados01[0]["PngAssinatura"]; ?>" width="200" />
                        </div>
                        <div id="asspng02">
                            <img src="upload/img/certificados/<?= $dados02[0]["PngAssinatura"]; ?>" width="200" />
                        </div>
                        <div id="asspng03">
                            <img src="upload/img/certificados/<?= $dados03[0]["PngAssinatura"]; ?>" width="200" />
                        </div>
                        <div id="asspng04">
                            <img src="upload/img/certificados/<?= $dados04[0]["PngAssinatura"]; ?>" width="200" />
                        </div>
                        <?php } else if (($dados03[0]["nome"]) != NULL) { ?>
                        <style type="text/css">
                            #ass01  {width: 374px; margin-top: 615px; font-size: 12px; line-height: 21px; position: absolute; margin-left: 50px; text-align: center;}
                            #ass02 {width: 374px; margin-top: 615px; font-size: 12px; line-height: 21px; position: absolute; margin-left: 376px; text-align: center;}
                            #ass03 {width: 374px; margin-top: 615px; font-size: 12px; line-height: 21px; position: absolute; margin-left: 700px; text-align: center;}
                            #asspng01 {width: 374px; margin-top: 580px; position: absolute; margin-left: 50px; text-align: center;}
                            #asspng02 {width: 374px; margin-top: 580px; position: absolute; margin-left: 376px; text-align: center;}
                            #asspng03 {width: 374px; margin-top: 580px; position: absolute; margin-left: 700px; text-align: center;}
                            #data { width: 800px; position: absolute; margin-top: 520px; padding-left: 162px; font-size: 19px; line-height: 25px; text-align: right;}
                        </style> 
                        <div id="ass01">
                            <br>
                            <?= $dados01[0]["titulacao"]; ?>&nbsp;<?= $dados01[0]["nome"]; ?>
                            <br>
                            <?= $dados01[0]["cargo"]; ?>
                        </div>
                        <div id="ass02"> <!-- Principal -->
                            <br>
                            <?= $dados02[0]["titulacao"]; ?>&nbsp;<?= $dados02[0]["nome"]; ?>
                            <br>
                            <?= $dados02[0]["cargo"]; ?>
                        </div>
                        <div id="ass03">
                            <br>
                            <?= $dados03[0]["titulacao"]; ?>&nbsp;<?= $dados03[0]["nome"]; ?>
                            <br>
                            <?= $dados03[0]["cargo"]; ?>
                        </div>
                        <div id="asspng01">
                            <img src="upload/img/certificados/<?= $dados01[0]["PngAssinatura"]; ?>" width="240" />
                        </div>
                        <div id="asspng02">
                            <img src="upload/img/certificados/<?= $dados02[0]["PngAssinatura"]; ?>" width="240" />
                        </div>
                        <div id="asspng03">
                            <img src="upload/img/certificados/<?= $dados03[0]["PngAssinatura"]; ?>" width="240" />
                        </div>
                        <?php } else { ?>
                        <style type="text/css">
                            #data { width: 800px; position: absolute; margin-top: 520px; padding-left: 162px; font-size: 20px; line-height: 25px; text-align: left;}
                        </style> 
                        <div id="assleft">
                            <br>
                            <?= $dados01[0]["titulacao"]; ?>&nbsp;<?= $dados01[0]["nome"]; ?>
                            <br>
                            <?= $dados01[0]["cargo"]; ?>
                        </div>
                        <div id="assright">
                            <br>
                            <?= $dados02[0]["titulacao"]; ?>&nbsp;<?= $dados02[0]["nome"]; ?>
                            <br>
                            <?= $dados02[0]["cargo"]; ?>
                        </div>

                        <div id="asspngleft">
                            <img src="upload/img/certificados/<?= $dados01[0]["PngAssinatura"]; ?>" width="240" />
                        </div>
                        <div id="asspng">
                            <img src="upload/img/certificados/<?= $dados02[0]["PngAssinatura"]; ?>" width="240" />
                        </div>
    <?php } ?>
                    <div id="info01">
                        Nº de Autenticação: <?= $apoio[0]["Autenticacao"]; ?>
                    </div>
                    <div id="info02">
                        Para verificar a autenticidade deste certificado, acesse: www.spdm.org.br/sistemas/certificado
                    </div>
                </td>
            </tr>
        </table>
    </page>

<?php } else if (($apoio[0]["idOrigem"]) == "6") { ?>

    <!-- SPDM EDUCAÇÃO - INGLÊS -->
    <style type="text/css">
        table {width: 100%; border: none;}
        #fundo { position: relative; z-index: -1000;}
        #certicamos { width: 1124px; position: absolute; margin-top: 240px; padding-left: 0px; padding-right: 110px; font-size: 18px; line-height: 20px; text-align: center;}
        #nome { width: 1124px; position: absolute; margin-top: 275px; padding-left: 0px; font-size: 23px; line-height: 20px; text-align: center;}
        #corpo { width: 880px; height: 150px; position: absolute; margin-top: 300px; padding-left: 122px; font-size: 19px; line-height: 27px; text-align: center;}
        #carga { width: 880px; position: absolute; margin-top: 460px; padding-left: 102px; font-size: 20px; line-height: 25px; text-align: right;}
        span {color: #cb2027; text-transform: uppercase;}
        #info01 {width: 500px; margin-top: 710px; position: absolute; margin-left: 60px; font-size: 11px;}
        #info02 {width: 500px; margin-top: 725px; position: absolute; margin-left: 60px; font-size: 9px;}
        #assleft  {width: 562px; margin-top: 605px; font-size: 12px; line-height: 21px; position: absolute; margin-left: 50px; text-align: center;}
        #assright {width: 562px; margin-top: 605px; font-size: 12px; line-height: 21px; position: absolute; margin-left: 512px; text-align: center;}
        #asspngleft {width: 562px; margin-top: 560px; position: absolute; margin-left: 50px; text-align: center;}
        #asspng {width: 562px; margin-top: 560px; position: absolute; margin-left: 512px; text-align: center;}
    </style>
    <page backtop="-1mm" backbottom="-20mm" backleft="-1mm" backright="-4mm">
        <table style="width: 100%;">
            <tr>
                <td>
                    <div id="fundo"><img src="upload/img/certificados/<?= $certificado[0]["imgCertificado"]; ?>" width="1124" height="793" alt=""/></div>
                    <div id="certicamos">We certify that</div>
                    <div id="nome"><strong><span><?= mb_strtoupper($participante[0]["titulacao"] . " " . $participante[0]["Nome"]); ?></span></strong></div>
                    <div id="corpo">
                        <p>attended <?= strip_tags($certificado[0]["TextoPadrao"], '<strong>'); ?>, as <strong><?= $tipo[0]["Alias"]; ?></strong>.</p>
                    </div>
    <?php if (($certificado[0]["cargaHoraria"]) != "") { ?>
                        <div id="carga">
                            <p><strong>Credit hour:</strong> <?= $certificado[0]["cargaHoraria"]; ?></p>
                        </div>
    <?php } ?>
                    <div id="data">
                        <p>Brasil – São Paulo, <?= date('F j, Y', strtotime($certificado[0]["dataEmissao"])); ?></p>
                    </div>

                        <?php if (($dados04[0]["nome"]) != NULL) { ?>
                        <style type="text/css">
                            #ass01  {width: 246px; margin-top: 585px; font-size: 11px; line-height: 18px; position: absolute; margin-left: 60px; text-align: center;}
                            #ass02 {width: 246px; margin-top: 585px; font-size: 11px; line-height: 18px; position: absolute; margin-left: 316px; text-align: center;}
                            #ass03 {width: 246px; margin-top: 585px; font-size: 11px; line-height: 18px; position: absolute; margin-left: 572px; text-align: center;}
                            #ass04 {width: 246px; margin-top: 585px; font-size: 11px; line-height: 18px; position: absolute; margin-left: 818px; text-align: center;}
                            #asspng01 {width: 246px; margin-top: 550px; position: absolute; margin-left: 60px; text-align: center;}
                            #asspng02 {width: 246px; margin-top: 550px; position: absolute; margin-left: 316px; text-align: center;}
                            #asspng03 {width: 246px; margin-top: 550px; position: absolute; margin-left: 572px; text-align: center;}
                            #asspng04 {width: 246px; margin-top: 550px; position: absolute; margin-left: 818px; text-align: center;}
                            #data { width: 800px; position: absolute; margin-top: 460px; padding-left: 162px; font-size: 20px; line-height: 25px; text-align: left;}
                        </style> 
                        <div id="ass01">
                            <br>
                            <?= $dados01[0]["titulacao"]; ?>&nbsp;
                            <?= $dados01[0]["nome"]; ?>
                            <br>
        <?= $dados01[0]["cargo"]; ?>
                        </div>
                        <div id="ass02"> <!-- Principal -->
                            <br>
                            <?= $dados02[0]["titulacao"]; ?>&nbsp;
                            <?= $dados02[0]["nome"]; ?>
                            <br>
        <?= $dados02[0]["cargo"]; ?>
                        </div>
                        <div id="ass03">
                            <br>
        <?= $dados03[0]["nome"]; ?>
                            <br>
        <?= $dados03[0]["cargo"]; ?>
                        </div>
                        <div id="ass04">
                            <br>
        <?= $dados04[0]["titulacao"]; ?>&nbsp;
        <?= $dados04[0]["nome"]; ?>
                            <br>
                        <?= $dados04[0]["cargo"]; ?>
                        </div>

                        <div id="asspng01">
                            <img src="upload/img/certificados/<?= $dados01[0]["PngAssinatura"]; ?>" width="200" />
                        </div>
                        <div id="asspng02">
                            <img src="upload/img/certificados/<?= $dados02[0]["PngAssinatura"]; ?>" width="200" />
                        </div>
                        <div id="asspng03">
                            <img src="upload/img/certificados/<?= $dados03[0]["PngAssinatura"]; ?>" width="200" />
                        </div>
                        <div id="asspng04">
                            <img src="upload/img/certificados/<?= $dados04[0]["PngAssinatura"]; ?>" width="200" />
                        </div>
                        <?php } else if (($dados03[0]["nome"]) != NULL) { ?>
                        <style type="text/css">
                            #ass01  {width: 374px; margin-top: 585px; font-size: 12px; line-height: 21px; position: absolute; margin-left: 50px; text-align: center;}
                            #ass02 {width: 374px; margin-top: 585px; font-size: 12px; line-height: 21px; position: absolute; margin-left: 376px; text-align: center;}
                            #ass03 {width: 374px; margin-top: 585px; font-size: 12px; line-height: 21px; position: absolute; margin-left: 700px; text-align: center;}
                            #asspng01 {width: 374px; margin-top: 540px; position: absolute; margin-left: 50px; text-align: center;}
                            #asspng02 {width: 374px; margin-top: 540px; position: absolute; margin-left: 376px; text-align: center;}
                            #asspng03 {width: 374px; margin-top: 540px; position: absolute; margin-left: 700px; text-align: center;}
                            #data { width: 800px; position: absolute; margin-top: 460px; padding-left: 162px; font-size: 20px; line-height: 25px; text-align: left;}
                        </style> 
                        <div id="ass01">
                            <br>
                            <?= $dados01[0]["titulacao"]; ?>&nbsp;
                            <?= $dados01[0]["nome"]; ?>
                            <br>
                            <?= $dados01[0]["cargo"]; ?>
                        </div>
                        <div id="ass02"> <!-- Principal -->
                            <br>
        <?= $dados02[0]["titulacao"]; ?>&nbsp;
        <?= $dados02[0]["nome"]; ?>
                            <br>
        <?= $dados02[0]["cargo"]; ?>
                        </div>
                        <div id="ass03">
                            <br>
                        <?= $dados03[0]["titulacao"]; ?>&nbsp;
                        <?= $dados03[0]["nome"]; ?>
                            <br>
        <?= $dados03[0]["cargo"]; ?>
                        </div>

                        <div id="asspng01">
                            <img src="upload/img/certificados/<?= $dados01[0]["PngAssinatura"]; ?>" width="240" />
                        </div>
                        <div id="asspng02">
                            <img src="upload/img/certificados/<?= $dados02[0]["PngAssinatura"]; ?>" width="240" />
                        </div>
                        <div id="asspng03">
                            <img src="upload/img/certificados/<?= $dados03[0]["PngAssinatura"]; ?>" width="240" />
                        </div>
                        <?php } else { ?>
                        <style type="text/css">
                            #data { width: 800px; position: absolute; margin-top: 490px; padding-left: 162px; font-size: 20px; line-height: 25px; text-align: left;}
                        </style> 
                        <div id="assleft">
                            <br>
        <?= $dados01[0]["titulacao"]; ?>&nbsp;
        <?= $dados01[0]["nome"]; ?>
                            <br>
        <?= $dados01[0]["cargo"]; ?>
                        </div>
                        <div id="assright">
                            <br>
        <?= $dados02[0]["titulacao"]; ?>&nbsp;
        <?= $dados02[0]["nome"]; ?>
                            <br>
        <?= $dados02[0]["cargo"]; ?>
                        </div>

                        <div id="asspngleft">
                            <img src="upload/img/certificados/<?= $dados01[0]["PngAssinatura"]; ?>" width="240" />
                        </div>
                        <div id="asspng">
                            <img src="upload/img/certificados/<?= $dados02[0]["PngAssinatura"]; ?>" width="240" />
                        </div>
    <?php } ?>
                    <div id="info01">
                        Authentication Number: <?= $apoio[0]["Autenticacao"]; ?>
                    </div>
                    <div id="info02">
                        To check the authenticity of this certificate, visit: www.spdm.org.br/sistemas/certificado
                    </div>

                </td>
            </tr>
        </table>
    </page>

<?php } else if (($apoio[0]["idOrigem"]) == "9") { ?>

    <!-- FPCS Faculdade Paulista -->
    <style type="text/css">
        table {width: 100%; border: none;}
        #fundo { position: relative; z-index: -1000;}
        #certicamos { width: 1124px; position: absolute; margin-top: 240px; padding-left: 0px; padding-right: 110px; font-size: 16px; line-height: 20px; text-align: center;}
        #nome { width: 1124px; position: absolute; margin-top: 275px; padding-left: 0px; font-size: 23px; line-height: 20px; text-align: center;}
        #destaque {color: #cb2027; text-transform: uppercase;}
        #documento{font-size: 16px; line-height: 27px; text-align: center;}
        #corpo { width: 880px; height: 150px; position: absolute; margin-top: 300px; padding-left: 122px; font-size: 19px; line-height: 27px; text-align: center;}
        #carga { width: 880px; position: absolute; margin-top: 500px; padding-left: 102px; font-size: 20px; line-height: 25px; text-align: right;}
        #info01 {width: 500px; margin-top: 710px; position: absolute; margin-left: 60px; font-size: 11px;}
        #info02 {width: 500px; margin-top: 725px; position: absolute; margin-left: 60px; font-size: 9px;}
        #info03 {width: 562px; margin-top: 675px; position: absolute; margin-left: 281px; font-size: 9px; text-align: center;}
        #assleft  {width: 562px; margin-top: 605px; font-size: 12px; line-height: 21px; position: absolute; margin-left: 50px; text-align: center;}
        #asscenter  {width: 562px; margin-top: 605px; font-size: 12px; line-height: 21px; position: absolute; margin-left: 281px; text-align: center;}
        #assright {width: 562px; margin-top: 605px; font-size: 12px; line-height: 21px; position: absolute; margin-left: 512px; text-align: center;}
        #asspngleft {width: 562px; margin-top: 560px; position: absolute; margin-left: 50px; text-align: center;}
        #asspngcenter {width: 562px; margin-top: 560px; position: absolute; margin-left: 281px; text-align: center;}
        #asspng {width: 562px; margin-top: 560px; position: absolute; margin-left: 512px; text-align: center;}
    </style>
    <page backtop="-1mm" backbottom="-20mm" backleft="-1mm" backright="-4mm">
        <table style="width: 100%;">
            <tr>
                <td>
                    <div id="fundo"><img src="upload/img/certificados/<?= $certificado[0]["imgCertificado"]; ?>" width="1124" height="793" alt=""/></div>
                    <div id="certicamos">A Superintendência da Educação, no uso de suas atribuições,<br>confere o presente certificado a</div>
                 
                    <div id="nome">
                        <br><strong><span id="destaque"><?= mb_strtoupper($participante[0]["titulacao"] . " " . $participante[0]["Nome"]); ?></span></strong>
                        <br><br><span id="documento"><?= $participante[0]["Nacionalidade"];?>, nascido(a) em <?= Funcoes::convertDataBr($participante[0]["Nasc"]);?>, portador(a) do documento <?=$doc;?>: <?= $participante[0]['Documento'];?></span>
                    </div>
                    <div id="corpo"><br>
                        <p>
                            <?= $certificado[0]["TextoPadrao"]; ?>
                        </p>
                    </div>
    <?php if (($certificado[0]["cargaHoraria"]) != "") { ?>
                        <div id="carga">
                            <p><strong>Carga horária:</strong> <?= $certificado[0]["cargaHoraria"]; ?></p>
                        </div>
    <?php } ?>
                    <div id="data">
                        <p>São Paulo, <?= strftime('%d de %B de %Y', strtotime($certificado[0]["dataEmissao"])); ?></p>
                    </div>
                        <?php if (($dados04[0]["nome"]) != NULL) { ?>
                        <style type="text/css">
                            #ass01  {width: 246px; margin-top: 615px; font-size: 11px; line-height: 18px; position: absolute; margin-left: 60px; text-align: center;}
                            #ass02 {width: 246px; margin-top: 615px; font-size: 11px; line-height: 18px; position: absolute; margin-left: 316px; text-align: center;}
                            #ass03 {width: 246px; margin-top: 615px; font-size: 11px; line-height: 18px; position: absolute; margin-left: 572px; text-align: center;}
                            #ass04 {width: 246px; margin-top: 615px; font-size: 11px; line-height: 18px; position: absolute; margin-left: 818px; text-align: center;}
                            #asspng01 {width: 246px; margin-top: 580px; position: absolute; margin-left: 60px; text-align: center;}
                            #asspng02 {width: 246px; margin-top: 580px; position: absolute; margin-left: 316px; text-align: center;}
                            #asspng03 {width: 246px; margin-top: 580px; position: absolute; margin-left: 572px; text-align: center;}
                            #asspng04 {width: 246px; margin-top: 580px; position: absolute; margin-left: 818px; text-align: center;}
                            #data { width: 800px; position: absolute; margin-top: 500px; padding-left: 162px; font-size: 20px; line-height: 25px; text-align: left;}
                        </style> 
                        <div id="ass01">
                            <br>
                            <?= $dados01[0]["titulacao"]; ?>&nbsp;<?= $dados01[0]["nome"]; ?>
                            <br>
                            <?= $dados01[0]["cargo"]; ?>
                        </div>
                        <div id="ass02"> <!-- Principal -->
                            <br>
                            <?= $dados02[0]["titulacao"]; ?>&nbsp;<?= $dados02[0]["nome"]; ?>
                            <br>
                            <?= $dados02[0]["cargo"]; ?>
                        </div>
                        <div id="ass03">
                            <br>
                            <?= $dados03[0]["titulacao"]; ?>&nbsp;<?= $dados03[0]["nome"]; ?>
                            <br>
                            <?= $dados03[0]["cargo"]; ?>
                        </div>
                        <div id="ass04">
                            <br>
                            <?= $dados04[0]["titulacao"]; ?>&nbsp;<?= $dados04[0]["nome"]; ?>
                            <br>
                            <?= $dados04[0]["cargo"]; ?>
                        </div>
                        
                        <div id="asspng01">
                            <img src="upload/img/certificados/<?= $dados01[0]["PngAssinatura"]; ?>" width="200" />
                        </div>
                        <div id="asspng02">
                            <img src="upload/img/certificados/<?= $dados02[0]["PngAssinatura"]; ?>" width="200" />
                        </div>
                        <div id="asspng03">
                            <img src="upload/img/certificados/<?= $dados03[0]["PngAssinatura"]; ?>" width="200" />
                        </div>
                        <div id="asspng04">
                            <img src="upload/img/certificados/<?= $dados04[0]["PngAssinatura"]; ?>" width="200" />
                        </div>
<?php } else if (($dados03[0]["nome"]) != NULL) { ?>
                        <style type="text/css">
                            #ass01  {width: 374px; margin-top: 615px; font-size: 12px; line-height: 21px; position: absolute; margin-left: 50px; text-align: center;}
                            #ass02 {width: 374px; margin-top: 615px; font-size: 12px; line-height: 21px; position: absolute; margin-left: 376px; text-align: center;}
                            #ass03 {width: 374px; margin-top: 615px; font-size: 12px; line-height: 21px; position: absolute; margin-left: 700px; text-align: center;}
                            #asspng01 {width: 374px; margin-top: 580px; position: absolute; margin-left: 50px; text-align: center;}
                            #asspng02 {width: 374px; margin-top: 580px; position: absolute; margin-left: 376px; text-align: center;}
                            #asspng03 {width: 374px; margin-top: 580px; position: absolute; margin-left: 700px; text-align: center;}
                            #data { width: 800px; position: absolute; margin-top: 500px; padding-left: 162px; font-size: 20px; line-height: 25px; text-align: left;}
                        </style> 
                        <div id="ass01">
                            <br>
                            <?= $dados01[0]["titulacao"]; ?>&nbsp;<?= $dados01[0]["nome"]; ?>
                            <br>
                            <?= $dados01[0]["cargo"]; ?>
                        </div>
                        <div id="ass02"> <!-- Principal -->
                            <br>
                            <?= $dados02[0]["titulacao"]; ?>&nbsp;<?= $dados02[0]["nome"]; ?>
                            <br>
                            <?= $dados02[0]["cargo"]; ?>
                        </div>
                        <div id="ass03">
                            <br>
                            <?= $dados03[0]["titulacao"]; ?>&nbsp;<?= $dados03[0]["nome"]; ?>
                            <br>
                            <?= $dados03[0]["cargo"]; ?>
                        </div>

                        <div id="asspng01">
                            <img src="upload/img/certificados/<?= $dados01[0]["PngAssinatura"]; ?>" width="240" />
                        </div>
                        <div id="asspng02">
                            <img src="upload/img/certificados/<?= $dados02[0]["PngAssinatura"]; ?>" width="240" />
                        </div>
                        <div id="asspng03">
                            <img src="upload/img/certificados/<?= $dados03[0]["PngAssinatura"]; ?>" width="240" />
                        </div>
<?php } else if (($dados03[0]["nome"]) != NULL) { ?>
                        <style type="text/css">
                            #ass01  {width: 374px; margin-top: 615px; font-size: 12px; line-height: 21px; position: absolute; margin-left: 50px; text-align: center;}
                            #ass02 {width: 374px; margin-top: 615px; font-size: 12px; line-height: 21px; position: absolute; margin-left: 376px; text-align: center;}
                            #ass03 {width: 374px; margin-top: 615px; font-size: 12px; line-height: 21px; position: absolute; margin-left: 700px; text-align: center;}
                            #asspng01 {width: 374px; margin-top: 580px; position: absolute; margin-left: 50px; text-align: center;}
                            #asspng02 {width: 374px; margin-top: 580px; position: absolute; margin-left: 376px; text-align: center;}
                            #asspng03 {width: 374px; margin-top: 580px; position: absolute; margin-left: 700px; text-align: center;}
                            #data { width: 800px; position: absolute; margin-top: 500px; padding-left: 162px; font-size: 20px; line-height: 25px; text-align: left;}
                        </style> 
                        <div id="ass01">
                            <br>
                            <?= $dados01[0]["titulacao"]; ?>&nbsp;<?= $dados01[0]["nome"]; ?>
                            <br>
                            <?= $dados01[0]["cargo"]; ?>
                        </div>
                        <div id="ass02"> <!-- Principal -->
                            <br>
                            <?= $dados02[0]["titulacao"]; ?>&nbsp;<?= $dados02[0]["nome"]; ?>
                            <br>
                            <?= $dados02[0]["cargo"]; ?>
                        </div>
                        <div id="ass03">
                            <br>
                            <?= $dados03[0]["titulacao"]; ?>&nbsp;<?= $dados03[0]["nome"]; ?>
                            <br>
                            <?= $dados03[0]["cargo"]; ?>
                        </div>

                        <div id="asspng01">
                            <img src="upload/img/certificados/<?= $dados01[0]["PngAssinatura"]; ?>" width="240" />
                        </div>
                        <div id="asspng02">
                            <img src="upload/img/certificados/<?= $dados02[0]["PngAssinatura"]; ?>" width="240" />
                        </div>
                        <div id="asspng03">
                            <img src="upload/img/certificados/<?= $dados03[0]["PngAssinatura"]; ?>" width="240" />
                        </div>                        
                        <?php } else { ?>
                        <style type="text/css">
                            #data { width: 800px; position: absolute; margin-top: 500px; padding-left: 162px; font-size: 20px; line-height: 25px; text-align: left;}
                        </style> 
                        <div id="asscenter">
                            <br>
                            <?= $dados01[0]["titulacao"]; ?>&nbsp;<?= $dados01[0]["nome"]; ?>
                            <br>
                            <?= $dados01[0]["cargo"]; ?>
                        </div>
                      

                        <div id="asspngcenter">
                            <img src="upload/img/certificados/<?= $dados01[0]["PngAssinatura"]; ?>" width="240" />
                        </div>                      
         <?php } ?>
                    <div id="info03">
                          SPDM - ASSOCIAÇÃO PAULISTA PARA O DESENVOLVIMENTO DA MEDICINA,<br>mantenedora da FACULDADE PAULISTA DE CIÊNCIAS DA SAÚDE – FPCS
                      </div>
                    <div id="info01">
                        Nº de Autenticação: <?= $apoio[0]["Autenticacao"]; ?>
                    </div>
                    <div id="info02">
                        Para verificar a autenticidade deste certificado, acesse: www.spdm.org.br/sistemas/certificado
                    </div>

                </td>
            </tr>
        </table>
    </page>
<?php } ?>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="<?= URL::base(); ?>/images/favicon.ico" type="image/ico">
<!-- Bootstrap core CSS -->
<link rel="stylesheet" href="<?= URL::base(); ?>/theme/frontend/css/bootstrap.css">
<link rel="stylesheet" href="<?= URL::base(); ?>/theme/frontend/css/custom.css">
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
<style>
    .btn-tr tr
    {
        cursor:pointer;
    }

    label {
        float: right;
        padding-top: 5px;
    }
    .form-group {
        margin-bottom: 5px;
    }
    .leftfalse{
        float: left;
    }
</style>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-20685687-1', 'auto');
    ga('send', 'pageview');

</script>
<!-- Custom Fonts -->
<link href="<?= URL::base(); ?>theme/backend/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<script src="<?= URL::base(); ?>jquery/js/jquery-1.10.2.js"></script>
<script src="<?= URL::base(); ?>theme/frontend/js/bootstrap.min.js"></script>

<!-- Adiciona Toggle -->
<link href="<?= URL::base(); ?>theme/backend/vendor/bootstrap/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="<?= URL::base(); ?>theme/backend/vendor/bootstrap/js/bootstrap-toggle.min.js"></script>
<link href='<?= URL::base(); ?>theme/frontend/fullcalendar/fullcalendar.css' rel='stylesheet' />
<link href='<?= URL::base(); ?>theme/frontend/fullcalendar/fullcalendar.print.css' rel='stylesheet' media='print' />
<link href='<?= URL::base(); ?>theme/frontend/fullcalendar_scheduler/scheduler.css' rel='stylesheet' />
<script src='<?= URL::base(); ?>theme/frontend/fullcalendar/lib/moment.min.js'></script>

<script src='<?= URL::base(); ?>theme/frontend/fullcalendar/lib/jquery-ui.min.js'></script>
<script src='<?= URL::base(); ?>theme/frontend/fullcalendar/fullcalendar.min.js'></script>        
<script src='<?= URL::base(); ?>theme/frontend/fullcalendar/locale/pt-br.js'></script>
<script src='<?= URL::base(); ?>theme/frontend/fullcalendar_scheduler/scheduler.min.js'></script>
<!-- datapicker plugin -->
<link href='<?= URL::base(); ?>theme/frontend/datetimepicker/css/bootstrap-datetimepicker.min.css' rel='stylesheet' />
<script type="text/javascript" src="<?= URL::base(); ?>theme/frontend/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="<?= URL::base(); ?>theme/frontend/datetimepicker/js/moment.js"></script>

<!--<script src="https://www.google.com/recaptcha/api.js" async defer></script> -->
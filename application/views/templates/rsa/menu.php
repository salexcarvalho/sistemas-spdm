<style>
    .nav > li > a {
        padding-right: 45px;
        padding-left: 45px;
    }
</style>
<div id="menu">
    <div class="navbar navbar-default">
        <!-- START Menu mobile -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <!-- END Menu mobile -->
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li id="mn_home"><a href="<?= URL::base(); ?>agenda">Página Inicial</a></li>
                <li id="mn_realizar"><a href="<?= URL::base(); ?>agenda/agendamento/realizar">Realizar Agendamento</a></li>
                <li id="mn_acompanhar"><a href="<?= URL::base(); ?>agenda/agendamento/consultar">Consultar Agendamento</a></li> 
            </ul>
        </div>
    </div>
</div>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="<?= URL::base(); ?>/images/favicon.ico" type="image/ico">
<!-- Bootstrap core CSS -->
<link rel="stylesheet" href="<?= URL::base(); ?>/theme/frontend/css/bootstrap.css">
<link rel="stylesheet" href="<?= URL::base(); ?>/theme/frontend/css/custom.css">
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
<style>
    .btn-tr tr
    {
        cursor:pointer;
    }
</style>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-20685687-1', 'auto');
  ga('send', 'pageview');

</script>

<!-- jQuery Version 1.11.0 -->
<script src="<?= URL::base(); ?>theme/backend/js/jquery-1.11.0.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?= URL::base(); ?>theme/backend/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="<?= URL::base(); ?>theme/backend/js/plugins/metisMenu/metisMenu.min.js"></script>

<!-- DataTables JavaScript -->
<script src="<?= URL::base(); ?>theme/backend/js/plugins/dataTables/jquery.dataTables.js"></script>
<script src="<?= URL::base(); ?>theme/backend/js/plugins/dataTables/dataTables.bootstrap.js"></script>

<!-- Custom Theme JavaScript -->
<script src="<?= URL::base(); ?>theme/backend/js/sb-admin-2.js"></script>

<!-- <script src="//cdn.ckeditor.com/4.5.3/standard/ckeditor.js"></script>-->
<!-- <script src="//cdn.ckeditor.com/4.6.2/full/ckeditor.js"></script>-->
<script src="<?= URL::base(); ?>theme/backend/ckeditor/ckeditor.js"></script>

<link href="<?= URL::base(); ?>theme/backend/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="<?= URL::base(); ?>theme/backend/js/bootstrap-toggle.min.js"></script>

<!-- Adiciona campos JavaScript -->
<script src="<?= URL::base(); ?>theme/backend/js/plugins/jasny/jasny-bootstrap.min.js"></script>

<!-- Autocomplete campos JavaScript -->
<script src="<?= URL::base(); ?>theme/backend/js/plugins/typeahead/jquery.mockjax.js"></script>
<script src="<?= URL::base(); ?>theme/backend/js/plugins/typeahead/bootstrap-typeahead.min.js"></script>
<div id="menu">
    <div class="navbar navbar-default">
        <!-- START Menu mobile -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Menu do site</a>
        </div>
        <!-- END Menu mobile -->
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li id="mn_home"><a href="<?= URL::base(); ?>grc_home/inicio">Home</a></li>
                <li id="mn_curso"><a href="<?= URL::base(); ?>grc_curso/registro">Registro do Curso</a></li>
                <li id="mn_coordenacao"><a href="<?= URL::base(); ?>grc_curso/coordenacao">Coordenação</a></li>
                <li id="mn_inscricao"><a href="<?= URL::base(); ?>grc_curso/inscricao">Inscrição e Seleção</a></li>
                <li id="mn_matricula"><a href="<?= URL::base(); ?>grc_curso/matricula">Matrícula</a></li>
                <li id="mn_financiamento"><a href="<?= URL::base(); ?>grc_curso/financiamento">Financiamento</a></li>
                <li id="mn_pedagogico"><a href="<?= URL::base(); ?>grc_curso/pedagogico">Informações Pedagógicas do Curso</a></li>
            </ul>
        </div>
    </div>
</div>
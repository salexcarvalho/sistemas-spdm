<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="<?= URL::base(); ?>/images/favicon.ico" type="image/ico">

<!-- Bootstrap core CSS -->
<link rel="stylesheet" href="<?= URL::base(); ?>theme/backend/vendor/bootstrap/css/bootstrap.css">
<link rel="stylesheet" href="<?= URL::base(); ?>/theme/frontend/css/custom.css">

<!-- Custom Fonts -->
<link href="<?= URL::base(); ?>theme/backend/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<script src="<?= URL::base(); ?>theme/backend/js/jquery-3.2.1.js"></script>
<script src="<?= URL::base(); ?>theme/backend/js/jquery-migrate-1.4.1.min.js"></script>
<script src="<?= URL::base(); ?>theme/backend/vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Adiciona Toggle -->
<link href="<?= URL::base(); ?>theme/backend/vendor/bootstrap-toggle/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="<?= URL::base(); ?>theme/backend/vendor/bootstrap-toggle/js/bootstrap-toggle.min.js"></script>

<link href='<?= URL::base(); ?>theme/backend/vendor/fullcalendar/fullcalendar.css' rel='stylesheet' />
<link href='<?= URL::base(); ?>theme/backend/vendor/fullcalendar/fullcalendar.print.css' rel='stylesheet' media='print' />
<link href='<?= URL::base(); ?>theme/backend/vendor/fullcalendar_scheduler/scheduler.css' rel='stylesheet' /> 
<script src='<?= URL::base(); ?>theme/backend/vendor/fullcalendar/lib/moment.min.js'></script>

<script src='<?= URL::base(); ?>theme/backend/vendor/fullcalendar/lib/jquery-ui.min.js'></script>
<script src='<?= URL::base(); ?>theme/backend/vendor/fullcalendar/fullcalendar.min.js'></script>        
<script src='<?= URL::base(); ?>theme/backend/vendor/fullcalendar/locale/pt-br.js'></script>
<script src='<?= URL::base(); ?>theme/backend/vendor/fullcalendar_scheduler/scheduler.min.js'></script>
<!-- datapicker plugin -->

<link href='<?= URL::base(); ?>theme/backend/vendor/datetimepicker/css/bootstrap-datetimepicker.min.css' rel='stylesheet' />
<script type="text/javascript" src="<?= URL::base(); ?>theme/backend/vendor/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<!-- eModal Jquery -->
<script type="text/javascript" src="<?= URL::base(); ?>theme/backend/js/eModal.min.js"></script>
<style>
    .btn-tr tr
    {
        cursor:pointer;
    }
    label {
        float: right;
        padding-top: 5px;
    }
    .leftfalse{
        float: left;
    }
</style>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-20685687-1', 'auto');
    ga('send', 'pageview');

</script>

<script src="https://www.google.com/recaptcha/api.js" async defer></script>
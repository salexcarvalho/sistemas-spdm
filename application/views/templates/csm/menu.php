<div id="menu">
    <div class="navbar navbar-default">
        <!-- START Menu mobile -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <!-- END Menu mobile -->
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li id="mn_home"><a href="<?= URL::base(); ?>csm_home/inicio">Home</a></li>
                <li id="mn_conheca"><a href="<?= URL::base(); ?>csm_conheca/detalhes">Conheça a SPDM</a></li>
                <li id="mn_apresentacao"><a href="<?= URL::base(); ?>csm_apresentacao/detalhes">Apresentação</a></li>

                <li id="mn_servicos"><a href="<?= URL::base(); ?>csm_servicos/listar_servicos">Serviços</a></li>
                <li id="mn_profissionais"><a href="<?= URL::base(); ?>csm_profissionais/detalhes">Profissionais</a></li>
                <li id="mn_busca"><a href="<?= URL::base(); ?>csm_busca/avancada">Busca Avançada</a></li>
                <li id="mn_numeros"><a href="<?= URL::base(); ?>csm_numeros/detalhes">Números</a></li>
                <li id="mn_imprensa"><a href="<?= URL::base(); ?>csm_imprensa/detalhes">Imprensa</a></li>                
            </ul>
        </div>
    </div>
</div>
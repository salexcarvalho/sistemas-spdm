<div id="header">
    <div class="row">
        <a href="<?= URL::base(); ?>csm_home/inicio">
            <div id="logo" class="col-md-7">
                <img src="<?= URL::base(); ?>images/logo_csm.png" alt="Logo CSM" width="100%" height="100%">
            </div>
        </a>
        <div id="menu-superior" class="col-md-5 Menu-superior">
            <ul class="nav nav-pills pull-right">
                <li id="mns_acesso-restrito" class="active"><a href="../dashboard/">Acesso Restrito</a></li>
                <li id="mns_trabalhe-conosco"><a href="http://www.curriculum.spdm.org.br/" target="_blank">Trabalhe Conosco</a></li>
                <li id="mns_contato"><a href="<?= URL::base(); ?>csm_recado">Contato</a></li>
            </ul>
        </div>
        <div id="redes-sociais" class="col-md-2 redes-sociais">
            <div id="icon-redes"><a href="http://www.spdm.org.br/blog/" target="_blank"><img src="<?= URL::base(); ?>images/linkedin.png" width="24" height="24" alt="Linkedin"></a></div>
            <div id="icon-redes"><a href="http://www.youtube.com/user/SPDMOficial/videos" target="_blank"><img src="<?= URL::base(); ?>images/youtube.png" width="24" height="24" alt="Youtube"></a></div>
            <div id="icon-redes"><a href="https://www.facebook.com/spdmoficial" target="_blank"><img src="<?= URL::base(); ?>images/facebook.png" width="24" height="24" alt="Facebook"></a></div> 
        </div>
        <div id="pesquisa" class="col-md-3 pesquisa">
            <form class="navbar-form navbar-right">
                <div class="form-group">
                    <input type="search" autofocus placeholder="Pesquisar" class="form-pesquisa">
                </div>
                <button type="submit" class="btn btn-primary">Buscar</button>
            </form>
        </div>
    </div>
</div>
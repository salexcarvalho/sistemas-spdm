<div>
    <div id="footer">
        <div class="container">
            <p class="text-muted credit">Copyright ©2011-<?= date('Y'); ?> SPDM - Associação Paulista para o Desenvolvimento da Medicina. Todos os direitos reservados.
                <br>Rua Dr. Diogo de Faria, 1036 | Vila Clementino | São Paulo, SP | Cep: 04037-003</p>
        </div>
    </div>
</div>
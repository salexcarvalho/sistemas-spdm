<style>
.nav > li > a {
    padding-right: 45px;
    padding-left: 45px;
}
</style>

<div id="menu">
    <div class="navbar navbar-default">
        <!-- START Menu mobile -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Menu do site</a>
        </div>
        <!-- END Menu mobile -->
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li id="mn_home"><a href="<?= URL::base(); ?>canalconfidencial">Página Inicial</a></li>
                <li id="mn_relato"><a href="<?= URL::base(); ?>canalconfidencial/solicitacao">Realizar Relato</a></li>
                <li id="mn_acompanhar"><a href="<?= URL::base(); ?>canalconfidencial/solicitacao/acompanhar_relato">Acompanhar Relato</a></li>
                <!--<li id="mn_codigodeetica"><a href="canalconfidencial/codigodeetica">Código de Ética</a></li>-->
                <li id="mn_contato"><a href="https://www.spdm.org.br/contato/fale-com-o-conselho-administrativo" target="_blank">Contato</a></li>                
            </ul>
        </div>
    </div>
</div>
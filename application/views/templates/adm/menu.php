<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">            
            <li id="dashboard">
                <a href="<?= URL::base(); ?>dashboard/adm/home"><i class="fa fa-dashboard fa-fw"></i>&nbsp;&nbsp;Dashboard</a>
            </li>
            <!-- INICIO DO MENU Canal Confidencial Humanos -->
            <?php if ($_SESSION['AcLiberaMenuRH'] == true) { ?>
            <?php
                //QTDs de Relatos
                $model_relatos = new Model_Scc_Relato('default');

                $quantidade_relatos = $model_relatos->select_quantidade_relatos();
                $concluidos = $model_relatos->select_quantidade_concluidos();
                $abertos = $model_relatos->select_quantidade_abertos();
                $processando = $model_relatos->select_quantidade_processando();
                $encerrados = $model_relatos->select_quantidade_encerrados();
            ?>
                <li id='lista-listagens-rh'>
                    <a href="<?= URL::base(); ?>"><i class="fa fa-envelope"></i>&nbsp;&nbsp;Canal Confidencial<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <?php if ($_SESSION['AcListaMenuRelatRH'] == true) { ?>
                            <li id='lista-relatos'> <a href="<?= URL::base(); ?>dashboard/scc_relatos/home">Relatos <span class="badge pull-right"></span></a> </li>
                        <?php } ?>
                        <?php //if ($_SESSION['AcListaMenuSolicRH'] == true) {  ?>
                        <!--<li id='lista-solicitantes'> <a href="dashboard/scc_solicitantes/home">Solicitantes </a> </li>-->
                        <?php //} ?>
                        <?php if ($_SESSION['AcListaMenuTPRelatRH'] == true) { ?>
                            <li id='lista-tipos-relatos'> <a href="<?= URL::base(); ?>dashboard/scc_tipos/home">Tipos de Relatos </a> </li>
                        <?php } ?>
                        <?php if ($_SESSION['AcListaMenuUnidRH'] == true) { ?>    
                            <li id='lista-unidades-rh'> <a href="<?= URL::base(); ?>dashboard/scc_unidades/home">Unidades RH<span class="badge pull-right"></span></a> </li>
                        <?php } ?>
                        <?php if ($_SESSION['AcListaMenuVincRH'] == true) { ?>    
                            <li id='lista-vinculos'> <a href="<?= URL::base(); ?>dashboard/scc_vinculos/home">Vinculos RH<span class="badge pull-right"></span></a> </li>
                        <?php } ?> 
                        <?php if ($_SESSION['AcListaMenuDsei'] == true) { ?>    
                            <li id='lista-dsei'> <a href="<?= URL::base(); ?>dashboard/scc_dsei/home">DSEI (Saúde Indígena)<span class="badge pull-right"></span></a> </li>
                        <?php } ?>   
                    </ul>
                </li>
            <?php } ?> 
            <!-- FIM DO MENU Canal Confidencial -->

            <!-- INICIO DO MENU AGENDA DE CONTATO -->
            <?php if ($_SESSION['AcLiberaMenuContatos'] == true) { ?>
                <li id='li-contatos'>
                    <a href="#"><i class="fa fa-address-book"></i>&nbsp;&nbsp;Contatos Telefônicos<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <?php if ($_SESSION['AcLiberaMenuConTpLis'] == true) { ?>
                            <li id='li-cad-tipos-contato'> <a href="<?= URL::base(); ?>dashboard/contatos_home/home">Lista de Contatos<span class="badge pull-right"></span></a> </li>
                        <?php } ?>
                        <?php if ($_SESSION['AcLiberaMenuConTpEm'] == true) { ?>
                            <li id='li-cad-tipos-email'> <a href="<?= URL::base(); ?>dashboard/contatos_email/home">Tipos de Emails<span class="badge pull-right"></span></a> </li>
                        <?php } ?>
                        <?php if ($_SESSION['AcLiberaMenuConTpGr'] == true) { ?>
                            <li id='li-cad-tipos-grupos'> <a href="<?= URL::base(); ?>dashboard/contatos_grupo/home">Tipos de Grupos<span class="badge pull-right"></span></a> </li>
                        <?php } ?> 
                        <?php if ($_SESSION['AcLiberaMenuConTpTel'] == true) { ?>
                            <li id='li-cad-tipos-telefone'> <a href="<?= URL::base(); ?>dashboard/contatos_telefone/home">Tipos de Telefones<span class="badge pull-right"></span></a> </li>
                        <?php } ?> 
                        <?php if ($_SESSION['AcLiberaMenuConTpTra'] == true) { ?>
                            <li id='li-cad-tipos-tratamento'> <a href="<?= URL::base(); ?>dashboard/contatos_tratamento/home">Tipos de Tratamentos<span class="badge pull-right"></span></a> </li>
                        <?php } ?> 
                    </ul>
                </li>
            <?php } ?> 
            <!-- FIM DO MENU AGENDA DE CONTATO -->

            <!-- INICIO DO MENU Reserva de Salas -->
            <?php if ($_SESSION['AcLiberaMenuSalas'] == true) { ?>
            <?php
                //QTDs de Eventos E SALAS UTILIZADAS
                $model_reservas = new Model_Rsa_Reserva('default');
                $model_salas = new Model_Rsa_Salas('default');
                $model_areas = new Model_Rsa_Area('default');

                $quantidade_reservas = $model_reservas->select_quantidade_reservas();
                $quantidade_salas = $model_salas->select_quantidade_salas();
                $quantidade_areas = $model_areas->select_quantidade_areas();
            ?>
                <li id='li-salas'>
                    <a href="#"><i class="fa fa-calendar-o"></i>&nbsp;&nbsp;Reserva de Salas<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <?php if ($_SESSION['AcLiberaMenuReser'] == true) { ?>
                            <li id='li-cad-reserva'> <a href="<?= URL::base(); ?>dashboard/rsa_reserva/home">Reservas<span class="badge pull-right"></span></a> </li>
                        <?php } ?>
                        <?php if ($_SESSION['AcLiberaMenuSala'] == true) { ?>     
                            <li id='li-cad-salas'> <a href="<?= URL::base(); ?>dashboard/rsa_salas/home">Salas<span class="badge pull-right"></span></a> </li>
                        <?php } ?>
                        <?php if ($_SESSION['AcLiberaMenuAreas'] == true) { ?> 
                            <li id='li-cad-areas'> <a href="<?= URL::base(); ?>dashboard/rsa_areas/home">Localizações<span class="badge pull-right"></span></a> </li>
                        <?php } ?>   
                        <?php if ($_SESSION['AcLiberaMenuRelat'] == true) { ?> 
                            <li id='li-cad-relatorio'> <a href="<?= URL::base(); ?>dashboard/rsa_relatorio/home">Relátorio<span class="badge pull-right"></span></a> </li>
                        <?php } ?> 
                    </ul>
                </li>
            <?php } ?> 
            <!-- FIM DO MENU SALAS -->

            <!-- INICIO DO MENU Secretaria -->
            <?php if ($_SESSION['AcLiberaMenuSecreta'] == true) { ?>
                <li id='li-gestaoP'>
                    <a href="#"><i class="fa fa-folder-open"></i>&nbsp;&nbsp;Secretaria<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <?php if ($_SESSION['AcLiberaAtas'] == true) { ?>
                            <li id='li-ata'> <a href="<?= URL::base(); ?>dashboard/secretaria_ata/home">Atas<span class="badge pull-right"></span></a> </li>
                        <?php } ?>
                        <?php if ($_SESSION['AcLiberaContrato'] == true) { ?>
                            <li id='li-contrato'> <a href="<?= URL::base(); ?>dashboard/secretaria_contrato/home">Contratos<span class="badge pull-right"></span></a> </li>
                        <?php } ?>    
                        <?php if ($_SESSION['AcLiberaDocumentos'] == true) { ?>
                            <li id='li-documento'> <a href="<?= URL::base(); ?>dashboard/secretaria_documentos/home">Documentos<span class="badge pull-right"></span></a> </li>
                        <?php } ?>
                        <?php if ($_SESSION['AcLiberaProto'] == true) { ?>
                            <li id='li-protocolo'> <a href="<?= URL::base(); ?>dashboard/secretaria_protocolo/home">Protocolos<span class="badge pull-right"></span></a> </li>
                        <?php } ?>
                        <?php if ($_SESSION['AcLiberaOficio'] == true) { ?> 
                            <li id='li-oficio'> <a href="<?= URL::base(); ?>dashboard/secretaria_oficio/home">Ofícios Emitidos<span class="badge pull-right"></span></a> </li>
                        <?php } ?>
                        <?php if ($_SESSION['AcLiberaMemorando'] == true) { ?> 
                            <li id='li-memorando'> <a href="<?= URL::base(); ?>dashboard/secretaria_memorando/home">Memorando Emitidos<span class="badge pull-right"></span></a> </li>
                        <?php } ?>
                        <?php if ($_SESSION['AcLiberaTiposDocs'] == true) { ?> 
                            <li id='li-tipodoc'> <a href="<?= URL::base(); ?>dashboard/secretaria_tiposdedocumentos/home">Tipos de Documento<span class="badge pull-right"></span></a> </li>
                        <?php } ?>
                        <?php if ($_SESSION['AcLiberaModulo'] == true) { ?> 
                            <li id='li-modulo'> <a href="<?= URL::base(); ?>dashboard/secretaria_modulos/home">Módulos do Sistema<span class="badge pull-right"></span></a> </li>
                        <?php } ?>    
                    </ul>
                </li>
            <?php } ?> 
            <!-- FIM DO MENU PROTOCOLOS SITE -->

            <!-- INICIO DO MENU CERTIFICADOS -->
            <?php if ($_SESSION['AcLiberaMenuCert'] == true) { ?>
            <?php
                // QTDs dos certificados
                $model_participantes = new Model_Sec_Participantes('default');
                $model_tparticipantes = new Model_Sec_Tipos('default');
                $model_modelos = new Model_Sec_Certificado('default');
                $model_assinaturas = new Model_Sec_Assinatura('default');

                $quantidade_participantes = $model_participantes->select_quantidade_participantes();
                $quantidade_tipos = $model_tparticipantes->select_quantidade();
                $quantidade_modelos = $model_modelos->select_quantidade_modelos();
                $quantidade_assinaturas = $model_assinaturas->select_quantidade_assinaturas();
            ?>    
                <li id='li-certificados'>
                    <a href="#"><i class="fa fa-plus fa-file-pdf-o"></i>&nbsp;&nbsp;Certificados<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <?php if ($_SESSION['AcLiberaMenuPart'] == true) { ?>
                            <li id='li-cad-participantes'> <a href="<?= URL::base(); ?>dashboard/sec_participantes/home">Participantes/Alunos <span class="badge pull-right"></span></a> </li>
                        <?php } ?>
                        <?php if ($_SESSION['AcLiberaMenuEventos'] == true) { ?>     
                            <li id='li-cad-eventos'> <a href="<?= URL::base(); ?>dashboard/sec_certificados/modelos">Modelo de Certificados <span class="badge pull-right"></span></a> </li>
                        <?php } ?>
                        <?php if ($_SESSION['AcLiberaMenuCertific'] == true) { ?> 
                            <li id='li-cad-certificados'> <a href="<?= URL::base(); ?>dashboard/sec_certificados/home">Certificados Gerados <span class="badge pull-right"></span></a> </li>
                        <?php } ?>
                        <?php if ($_SESSION['AcLiberaMenuTpart'] == true) { ?> 
                            <li id='li-cad-tparticipantes'> <a href="<?= URL::base(); ?>dashboard/sec_tipos/home">Tipos <span class="badge pull-right"></span></a> </li>
                        <?php } ?>
                        <?php if ($_SESSION['AcLiberaMenuOrigens'] == true) { ?> 
                            <li id='li-cad-origem'> <a href="<?= URL::base(); ?>dashboard/sec_origem/home">Origens <span class="badge pull-right"></span></a> </li>
                        <?php } ?>
                        <?php if ($_SESSION['AcLiberaMenuAssinatu'] == true) { ?> 
                            <li id='li-assinatura'> <a href="<?= URL::base(); ?>dashboard/sec_assinaturas/home">Assinaturas <span class="badge pull-right"></span></a> </li>
                        <?php } ?>
                        <?php if ($_SESSION['AcLiberaMenuTdoc'] == true) { ?> 
                            <li id='li-assinatura'> <a href="<?= URL::base(); ?>dashboard/sec_tipoDoc/home">Tipos Documentos <span class="badge pull-right"></span></a> </li>
                        <?php } ?>  
                    </ul>
                </li>
            <?php } ?> 
            <!-- FIM DO MENU CERTIFICADOS -->

            <!-- INICIO DO MENU CSM -->
            <?php if ($_SESSION['AcLiberaMenuCSM'] == true) { ?>
            <?php
                // QTDs do CSM
                $model_funcionario = new Model_Csm_Funcionario('default');
                $model_servico = new Model_Csm_Servico('default');
                $model_contato = new Model_Csm_Contato('default');
                $model_departamento = new Model_Csm_Departamento('default');
                $model_especialidade = new Model_Csm_Especialidade('default');

                $quantidade_funcionarios = $model_funcionario->select_quantidade_funcionarios();
                $quantidade_servicos = $model_servico->select_quantidade_servicos();
                $quantidade_contatos = $model_contato->select_quantidade_contatos();
                $quantidade_departamentos = $model_departamento->select_quantidade_departamentos();
                $quantidade_especialidades = $model_especialidade->select_quantidade_especialidades();
            ?>    
                <li id='lista-listagens'>
                    <a href="<?= URL::base(); ?>"><i class="fa fa-medkit"></i>&nbsp;&nbsp;Catálogo de Serviços (CSM)<span class="fa arrow"></span></a>
                    <ul id="listagens" class="nav nav-second-level">
                        <?php if ($_SESSION['AcListaMenuDepart'] == true) { ?>
                            <li id='lista-departamentos'> <a href="<?= URL::base(); ?>dashboard/csm_departamento/lista">Departamentos <span class="badge pull-right"></span></a> </li>
                        <?php } ?>
                        <?php if ($_SESSION['AcListaMenuEsp'] == true) { ?>
                            <li id='lista-especialidades'> <a href="<?= URL::base(); ?>dashboard/csm_especialidades/lista">Especialidades </a> </li>
                        <?php } ?>
                        <?php if ($_SESSION['AcListaMenuServico'] == true) { ?>
                            <li id='lista-servicos'> <a href="<?= URL::base(); ?>dashboard/csm_servico/lista">Serviços </a> </li>
                        <?php } ?>
                        <?php if ($_SESSION['AcListaMenuFunc'] == true) { ?>    
                            <li id='lista-funcionarios'> <a href="<?= URL::base(); ?>dashboard/csm_funcionario/lista">Funcionários <span class="badge pull-right"></span></a> </li>
                        <?php } ?>
                        <?php if ($_SESSION['AcListaMenuUnidades'] == true) { ?>    
                            <li id='lista-unidades'> <a href="<?= URL::base(); ?>#">Unidades SPDM</a> </li>
                        <?php } ?>    
                    </ul>
                </li>
            <?php } ?> 
            <!-- FIM DO MENU CSM -->

            <!-- INICIO DO MENU INTEGRACAO SITE -->
            <?php if ($_SESSION['AcLiberaMenuIntegrac'] == true) { ?>
                <li id='li-videos'>
                    <a href="#"><i class="fa fa-sitemap"></i>&nbsp;&nbsp;Integrações do Site SPDM<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li id='li-cad-suasaude'> <a href="<?= URL::base(); ?>dashboard/integracoes_home">Sua Saúde na Rede<span class="badge pull-right"></span></a> </li>
                        <li id='li-cad-boaspraticas'> <a href="<?= URL::base(); ?>dashboard/integracoes_home/boaspraticas">Boas Práticas<span class="badge pull-right"></span></a> </li>   
                        <li id='li-cad-dica'> <a href="<?= URL::base(); ?>dashboard/integracoes_home/dicacultural">Dica Cultural<span class="badge pull-right"></span></a> </li>
                        <li id='li-cad-memoria'> <a href="<?= URL::base(); ?>dashboard/integracoes_home/memoriaspdm">Memória SPDM<span class="badge pull-right"></span></a> </li>
                        <li id='li-cad-integracao-suasaude'> <a href="<?= URL::base(); ?>integracoes/galeria" target="_blank">Integração Galeria<span class="badge pull-right"></span></a> </li>
                        <li id='li-cad-integracao-memoria'> <a href="<?= URL::base(); ?>integracoes/galeria_memoria" target="_blank">Integração Memória SPDM<span class="badge pull-right"></span></a> </li>
                        <li id='li-cad-integracao-facebook'> <a href="<?= URL::base(); ?>integracoes/rss" target="_blank">Integração Facebook<span class="badge pull-right"></span></a> </li>
                    </ul>
                </li>
            <?php } ?> 
            <!-- FIM DO MENU INTEGRACAO SITE -->

        </ul>
        <!-- /#side-menu -->
    </div>
    <!-- /.sidebar-collapse -->
</div>    
</nav>
<!-- /.navbar-static-side -->
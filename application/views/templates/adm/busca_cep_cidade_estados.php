<script>
    function CarregaCidade() {
        $('.Cidade').typeahead({
            ajax: '<?= URL::base(); ?>dashboard/apoio/busca_cidade',
            scrollBar: true,
            displayField: 'Cidade'
        });
    }

    function CarregaEstado() {
        $('.Estado').typeahead({
            ajax: '<?= URL::base(); ?>dashboard/apoio/busca_estado',
            scrollBar: true,
            displayField: 'Estado'
        });
    }

    function CarregaCep(val) {
            var iddiv;
            iddiv = $(val).attr('iddiv');
            //alert(iddiv);

            //Nova variável "cep" somente com dígitos.
            var cep = $(val).val().replace(/\D/g, '');

            //Verifica se campo cep possui valor informado.
            if (cep != "") {

                //Expressão regular para validar o CEP.
                var validacep = /^[0-9]{8}$/;

                //Valida o formato do CEP.
                if (validacep.test(cep)) {

                    //Preenche os campos com "..." enquanto consulta webservice.
                    $("#" + iddiv + " .Logradouro").val("...");
                    $("#" + iddiv + " .Bairro").val("...");
                    $("#" + iddiv + " .Cidade").val("...");
                    $("#" + iddiv + " .Estado").val("...");

                    //Consulta o webservice viacep.com.br/
                    $.getJSON("//viacep.com.br/ws/" + cep + "/json/?callback=?", function (dados) {

                        if (!("erro" in dados)) {
                            //Atualiza os campos com os valores da consulta.
                            $("#" + iddiv + " .Logradouro").val(dados.logradouro);
                            $("#" + iddiv + " .Bairro").val(dados.bairro);
                            $("#" + iddiv + " .Cidade").val(dados.localidade);
                            $("#" + iddiv + " .Estado").val(dados.uf);
                        } //end if.
                        else {
                            //CEP pesquisado não foi encontrado.
                            limpa_formulário_cep();
                            alert("CEP não encontrado.");
                        }
                    });
                } //end if.
                else {
                    //cep é inválido.
                    limpa_formulário_cep();
                    alert("Formato de CEP inválido.");
                }
            } //end if.
            else {
                //cep sem valor, limpa formulário.
                limpa_formulário_cep();
            }

            ///Busca Cep
            function limpa_formulário_cep() {
                // Limpa valores do formulário de cep.
                $("#" + iddiv + " .Cep").val("");
                $("#" + iddiv + " .Logradouro").val("");
                $("#" + iddiv + " .Complemento").val("");
                $("#" + iddiv + " .Bairro").val("");
                $("#" + iddiv + " .Cidade").val("");
                $("#" + iddiv + " .Estado").val("");
            }
        }
</script>   
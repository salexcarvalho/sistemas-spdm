<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">        
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="<?= URL::base(); ?>dashboard">
            <img src="<?= URL::base(); ?>upload/config/<?= $_SESSION['logo_admin'] ?>" width="160" alt="Logo CSM">
        </a>

    </div>

    <ul class="nav navbar-top-links navbar-right"> 
        <!--
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="fa fa-envelope fa-fw"></i> <i class="fa fa-caret-down"></i></a>            
            <ul class="dropdown-menu dropdown-messages">
                <li>
                    <a href="#">
                        <div>
                            <strong>John Smith</strong>
                            <span class="pull-right text-muted">
                                <em>Yesterday</em>
                            </span>
                        </div>                        
                        <div>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...
                        </div>
                    </a></li>
                <li class="divider"></li>
                <li>
                    <a href="#">
                        <div><strong>John Smith</strong><span class="pull-right text-muted"><em>Yesterday</em></span></div>
                        <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                    </a>                
                </li>                
                <li class="divider"></li>
                <li>
                    <a href="#">
                        <div><strong>John Smith</strong><span class="pull-right text-muted"><em>Yesterday</em></span></div>
                        <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                    </a>
                </li>
                    <li class="divider"></li>
                <li>                    
                    <a class="text-center" href="#">
                        <strong>Read All Messages</strong><i class="fa fa-angle-right"></i>
                    </a>
                </li>
            </ul>
        </li>
        -->

        <!--
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="fa fa-tasks fa-fw"></i> <i class="fa fa-caret-down"></i>
            </a>            <ul class="dropdown-menu dropdown-tasks">
                <li>
                    <a href="#">
                        <div>
                            <p>
                                <strong>Task 1</strong><span class="pull-right text-muted">40% Complete</span>
                            </p>                            
                            <div class="progress progress-striped active">
                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                    <span class="sr-only">40% Complete (success)</span>                                
                                </div>                            
                            </div>                        
                        </div>                    
                    </a>                
                </li>                
                <li class="divider"></li>                
                <li>                    
                    <a href="#">
                        <div>
                            <p>                                
                                <strong>Task 2</strong>
                                <span class="pull-right text-muted">20% Complete</span>
                            </p>                            
                            <div class="progress progress-striped active">
                                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                                    <span class="sr-only">20% Complete</span>
                                </div>                            
                            </div>                        
                        </div>                    
                    </a>                
                </li>                
                <li class="divider"></li>                
                <li>                    
                    <a href="#">                        
                        <div>                            
                            <p>                                
                                <strong>Task 3</strong>                                
                                <span class="pull-right text-muted">60% Complete</span>                            
                            </p>                            
                            <div class="progress progress-striped active">
                                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                                    <span class="sr-only">60% Complete (warning)</span>                                
                                </div>                            
                            </div>                        
                        </div>                    
                    </a>                
                </li>                
                <li class="divider"></li>                
                <li>                    
                    <a href="#">                        
                        <div>                            
                            <p>                                
                                <strong>Task 4</strong>                                
                                <span class="pull-right text-muted">80% Complete</span>                            
                            </p>                            
                            <div class="progress progress-striped active">                                
                                <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                                    <span class="sr-only">80% Complete (danger)</span>                                
                                </div>                            
                            </div>                        
                        </div>                    
                    </a>                
                </li>                
                <li class="divider"></li>                
                <li>                    
                    <a class="text-center" href="#">                        
                        <strong>See All Tasks</strong>                        
                        <i class="fa fa-angle-right"></i>                    
                    </a>                
                </li>            
            </ul>                   
        </li>             
        -->
        <?php if ($_SESSION['AcLiberaMenuMyUser'] == true) { ?> 
            <li class="dropdown">            
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">                
                    <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>            
                </a>   
                <ul class="dropdown-menu dropdown-user">                
                    <li id='li-edita-conta'>
                        <a href="<?= URL::base(); ?>dashboard/adm/editar_minha_conta?idUsuario=<?php echo $_SESSION['idUsuario']; ?>"><i class="fa fa-edit fa-fw"></i> Editar minha conta</a>
                    </li>
                    <li id='li-minha-conta'>
                        <a href="<?= URL::base(); ?>dashboard/adm/minha_conta?idUsuario=<?php echo $_SESSION['idUsuario']; ?>"><i class="fa fa-user fa-fw"></i> Minha conta</a>
                    </li>                    
                </ul>

            </li>
        <?php } ?> 

        <?php if ($_SESSION['AcLiberaMenuCTR'] == true) { ?> 
            <?php
                // QTDs das ações
                $model_usuario = new Model_Login_Usuario('user');
                $model_acoes = new Model_Login_Acoes('user');
                $model_perfis = new Model_Login_Perfis('user');

                $quantidade_usuarios = $model_usuario->select_quantidade_usuarios();
                $quantidade_acoes = $model_acoes->select_quantidade_acoes();
                $quantidade_perfis = $model_perfis->select_quantidade_perfis();
            ?>    
            <li id='li-cadastro' class="dropdown">            
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">                
                    <i class="fa fa-lock fa-fw"></i> <i class="fa fa-caret-down"></i>
                </a>            
                <ul class="dropdown-menu dropdown-user">
                    <?php if ($_SESSION['AcLiberaMenuUsuarios'] == true) { ?>
                        <li id='li-cad-usuario'> <a href="<?= URL::base(); ?>dashboard/adm/lista_usuario"><i class="fa fa-users fa-fw"></i> Usuários</a> </li>
                    <?php } ?>
                    <?php if ($_SESSION['AcLiberaMenuPerfil'] == true) { ?>    
                        <li id='li-cad-perfil'> <a href="<?= URL::base(); ?>dashboard/perfis/lista_perfis"><i class="fa fa-toggle-on fa-fw"></i> Perfil</a> </li>
                    <?php } ?>
                    <?php if ($_SESSION['AcLiberaMenuAcoes'] == true) { ?>
                        <li id='li-cad-acao'> <a href="<?= URL::base(); ?>dashboard/acoes/lista_acoes"><i class="fa fa-toggle-down fa-fw"></i> Controle de Ações <span class="badge pull-right"></span></a> </li>
                    <?php } ?>
                    <li class="divider"></li>
                    <?php if ($_SESSION['AcLiberaMenuSetor'] == true) { ?>
                        <li id='li-cad-tipos-setor'> <a href="<?= URL::base(); ?>dashboard/setor/home"><i class="fa fa-building-o fa-fw"></i> Setor/Departamentos<span class="badge pull-right"></span></a> </li>
                    <?php } ?>
                </ul>     
            </li>
        <?php } ?>


        <?php if ($_SESSION['AcLiberaMenuCTR'] == true) { ?>
            <li id='li-configuracoes' class="dropdown">            
                <a class="dropdown" href="<?= URL::base(); ?>dashboard/configuracoes">                
                    <i class="fa fa-cogs fa-fw"></i>
                </a>     
            </li>
        <?php } ?> 

        <li>
            <a href="<?= URL::base(); ?>dashboard/adm/logout"><i class="fa fa-sign-out fa-fw"></i> Sair</a>
        </li>    

        <li class="dropdown">            
            <a href="#"><i class="fa fa-code-fork fa-fw"></i> <?= VERSAO ?></a>
        </li>
    </ul>
 
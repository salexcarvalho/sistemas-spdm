<?php Session::instance(); ?>
<?php Funcoes::carrega(); //echo "<pre>"; var_dump($_SESSION['idUsuario']); echo "</pre>";  ?>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="<?= URL::base(); ?>images/favicon.ico" type="image/ico">

<!-- Bootstrap Core CSS -->
<link href="<?= URL::base(); ?>theme/backend/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- MetisMenu CSS -->
<link href="<?= URL::base(); ?>theme/backend/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

<!-- DataTables CSS -->
<link href="<?= URL::base(); ?>theme/backend/vendor/datatables/css/dataTables.bootstrap.css" rel="stylesheet">

<!-- Social Buttons CSS -->
<link href="<?= URL::base(); ?>theme/backend/vendor/bootstrap-social/bootstrap-social.css" rel="stylesheet">	

<!-- SbAdmin CSS -->
<link href="<?= URL::base(); ?>theme/backend/dist/css/sb-admin-2.css" rel="stylesheet">

<!-- Morris Charts CSS -->
<link href="<?= URL::base(); ?>theme/backend/vendor/morrisjs/morris.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="<?= URL::base(); ?>theme/backend/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

<!-- Custom CSS -->
<link href="<?= URL::base(); ?>theme/backend/css/custom.css" rel="stylesheet">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

<!-- jQuery Version 1.11.0 
<script src="<?= URL::base(); ?>theme/backend/js/jquery-1.11.0.js"></script>-->

<!-- jQuery Version 3.2.1 -->

<script src="<?= URL::base(); ?>theme/backend/js/jquery-3.2.1.js"></script>
<script src="<?= URL::base(); ?>theme/backend/js/jquery-migrate-1.4.1.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?= URL::base(); ?>theme/backend/vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="<?= URL::base(); ?>theme/backend/vendor/metisMenu/metisMenu.min.js"></script>

<!-- DataTables JavaScript -->
<script src="<?= URL::base(); ?>theme/backend/vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="<?= URL::base(); ?>theme/backend/vendor/datatables/js/dataTables.bootstrap.js"></script>

<!-- Custom Theme JavaScript -->
<script src="<?= URL::base(); ?>theme/backend/dist/js/sb-admin-2.js"></script>

<!-- CK Editor -->
<script src="<?= URL::base(); ?>theme/backend/ckeditor/ckeditor.js"></script>

<!-- Adiciona Toggle -->
<link href="<?= URL::base(); ?>theme/backend/vendor/bootstrap-toggle/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="<?= URL::base(); ?>theme/backend/vendor/bootstrap-toggle/js/bootstrap-toggle.min.js"></script>

<!-- Adiciona campos JavaScript -->
<script src="<?= URL::base(); ?>theme/backend/js/plugins/jasny/jasny-bootstrap.min.js"></script>

<!-- Autocomplete campos JavaScript -->
<script src="<?= URL::base(); ?>theme/backend/js/plugins/typeahead/jquery.mockjax.js"></script>
<script src="<?= URL::base(); ?>theme/backend/js/plugins/typeahead/bootstrap-typeahead.min.js"></script>

<!-- Morris JavaScript -->
<script src="<?= URL::base(); ?>theme/backend/vendor/raphael/raphael.min.js"></script>
<script src="<?= URL::base(); ?>theme/backend/vendor/morrisjs/morris.min.js"></script>

<!-- fullcallendar plugin -->
<link href='<?= URL::base(); ?>theme/backend/vendor/fullcalendar/fullcalendar.css' rel='stylesheet' />
<link href='<?= URL::base(); ?>theme/backend/vendor/fullcalendar/fullcalendar.print.css' rel='stylesheet' media='print' />
<link href='<?= URL::base(); ?>theme/backend/vendor/fullcalendar_scheduler/scheduler.css' rel='stylesheet' /> 
<script src='<?= URL::base(); ?>theme/backend/vendor/fullcalendar/lib/moment.min.js'></script>

<script src='<?= URL::base(); ?>theme/backend/vendor/fullcalendar/lib/jquery-ui.min.js'></script>
<script src='<?= URL::base(); ?>theme/backend/vendor/fullcalendar/fullcalendar.min.js'></script>        
<script src='<?= URL::base(); ?>theme/backend/vendor/fullcalendar/locale/pt-br.js'></script>
<script src='<?= URL::base(); ?>theme/backend/vendor/fullcalendar_scheduler/scheduler.min.js'></script>

<!-- datapicker plugin -->
<link href='<?= URL::base(); ?>theme/backend/vendor/datetimepicker/css/bootstrap-datetimepicker.min.css' rel='stylesheet' />
<script type="text/javascript" src="<?= URL::base(); ?>theme/backend/vendor/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>

<!-- eModal Jquery -->
<script type="text/javascript" src="<?= URL::base(); ?>theme/backend/js/eModal.min.js"></script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-20685687-5"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-20685687-5');
</script>
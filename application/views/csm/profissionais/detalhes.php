<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?= SISTEMA; ?> - Profissionais</title>
        <?php include(kohana::find_file('views/templates/csm', 'init', 'php')) ?>
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
        <script type="text/javascript" src="<?= URL::base(); ?>jquery/js/gmaps.js"></script>

        <script>
            jQuery(document).ready(function () {
                // Remove seleção de ativo no menu.
                $('.navbar-nav li').removeClass('active');
                $('#menu-superior li').removeClass('active');
                // Ativa botão no menu.
                $('.navbar-nav #mn_profissionais').addClass('active');
            });
        </script>
    </head>
    <body>
        <div class="content">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/csm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/csm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <!-- Inicio do Conteudo -->
            <div class="row">
                <div class="col-md-8">
                    <?php
                    foreach ($artigos as $artigo) {
                        if ($artigo['publicado'] == 'S') {
                            // div de com classe destaque não possui css definido.
                            ?>
                            <div <?= ($artigo['destaque'] == 'S') ? 'class="destaque"' : ''; ?>>
                                <h4 class="bold uppercase azul"><?= $artigo['tituloArtigo']; ?></h4>
        <?php
        if ($artigo['caminhoImagem'] != NULL) {
            ?>
                                    <img src="<?= $artigo['caminhoImagem'] . $artigo['nmImagem']; ?>" class="img-thumbnail img-responsive img-left">
                                    <?php
                                }
                                ?>
                                <?= ($artigo['aliasArtigo'] != NULL) ? '<p class="marginleft">' . $artigo['aliasArtigo'] . '</p>' : ''; ?>
                                <?= ($artigo['textoIntro'] != NULL) ? '<p class="marginleft">' . $artigo['aliasArtigo'] . '</p>' : ''; ?>
                                <?php
                                // A formatação do texto do artigo deve ser feita em HTML direto no banco de dados.
                                ?>
                                <?= $artigo['textoCompleto']; ?>
                            </div>
                                <?php
                            }
                        }
                        ?>
                </div>
                <div class="col-md-4">
                    <h4 class="bold uppercase azul"><strong>Serviços mais procurados</strong></h4>
                    <div id="tabela">
                        <table id="tb_especialidades" class="table table-striped table-hover">
                            <thead></thead>
                            <tbody class="btn-tr">
<?php
foreach ($servicos as $servico) {
    ?>
                                    <tr onclick="window.location = '../servico/lista_servicos_acessos?idServico=<?= $servico['idServico']; ?>'">
                                        <td><?= $servico['nmServico']; ?></td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <br>    
            <!-- Fim do Conteudo -->
        </div>
        <!-- Inicio do Rodadpé-->
<?php include(kohana::find_file('views/templates/csm', 'footer', 'php')) ?>
        <!-- Fim do Rodadpé-->
    </body>
</html>


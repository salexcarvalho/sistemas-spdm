<!DOCTYPE html>
<html lang="pt_br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title><?= SISTEMA; ?> - Contato</title>
        <?php include(kohana::find_file('views/templates/csm', 'init', 'php')) ?>
        <script>
            jQuery(document).ready(function ()
            {
                // Remove seleção de ativo no menu.
                $('.navbar-nav li').removeClass('active');
                $('#menu-superior li').removeClass('active');
                // Ativa botão no menu.
                $('#menu-superior #mns_contato').addClass('active');
            });
        </script>
        <style>
            .centered{
                margin : auto;
            }
        </style>
    </head>
    <body>
        <div class="content">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/csm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/csm', 'menu', 'php')) ?>
            <div id="div_content" class="row">
                <div class="col-md-12">
                    <h4 class="bold uppercase azul">Contato</h4>
                    <form class="form-horizontal" role="form" method="POST" action="enviar_contato">
                        <div class="form-group">
                            <label for="txtNome" class="col-sm-offset-3 col-sm-1 control-label">Nome</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" id="txtNome" name="txtNome" placeholder="" required value="<?= (isset($post['txtNome'])) ? $post['txtNome'] : '' ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="txtEmail" class="col-sm-offset-3 col-sm-1 control-label">Email</label>
                            <div class="col-sm-4">
                                <input type="email" class="form-control" id="txtEmail" name="txtEmail" placeholder="" required value="<?= (isset($post['txtEmail'])) ? $post['txtEmail'] : '' ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="txtMensagem" class="col-sm-offset-3 col-sm-1 control-label">Mensagem</label>
                            <div class="col-sm-4">
                                <textarea style="resize: vertical;" class="form-control" rows="10" name="txtMensagem" id="txtMensagem" placeholder="" required><?= (isset($post['txtMensagem'])) ? $post['txtMensagem'] : '' ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-4">
                                <button type="submit" class="btn btn-primary">Enviar</button>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-4">
                                <?php if (isset($warning)) : ?>
                                    <p class="message"><?= $warning; ?></p>
                                <?php endif ?>
                                <?php if (isset($errors)) : ?>
                                    <p class="message">Alguns erros foram encontrados.</p>
                                    <p class="message">Por favor confira os dados que você inseriu.</p>
                                    <ul class="errors">
                                        <?php foreach ($errors as $message): ?>
                                            <li><?php echo $message; ?></li>
                                        <?php endforeach ?>
                                    </ul>
                                <?php endif ?>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <?php include(kohana::find_file('views/templates/csm', 'footer', 'php')) ?>
    </body>
</html>
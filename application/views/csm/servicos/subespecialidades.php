<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= SISTEMA; ?> - Serviços</title>
        <?php include(kohana::find_file('views/templates/csm', 'init', 'php')) ?>
        <script>
            jQuery(document).ready(function()
            {
                // Remove seleção de ativo no menu.
                $('.navbar-nav li').removeClass('active');
                $('#menu-superior li').removeClass('active');
                // Ativa botão no menu.
                $('.navbar-nav #mn_servicos').addClass('active');
            });
        </script>
    </head>
    <body>
        <div class="content">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/csm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/csm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <!-- Inicio do Conteudo -->
            <div id="div_content" class="table-responsive">
                <table id="tb_subespecialidades" class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>SUBESPECIALIDADE</th>

                            <th>
                                CENTRO CUSTO
                            </th>
                            <th>
                                LOGRADOURO
                            </th>
                            <th>
                                NUMERO LOGRADOURO
                            </th>
                            <th>
                                COMPLEMENTO LOGRADOURO
                            </th>
                            <th>
                                CEP LOGRADOURO
                            </th>
                            <th>
                                BAIRRO
                            </th>
                            <th>
                                UF
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($subespecialidades as $subespecialidade)
                        {
                            ?>
                            <tr idespecialidade="<?= $subespecialidade['idEspecialidade']; ?>" idsubespecialidade="<?= $subespecialidade['idSubEspecialidade']; ?>">
                                <td><?= $subespecialidade['nmSubEspecialidade']; ?></td>
                                <td><?= $subespecialidade['idCentroCusto']; ?></td>
                                <td><?= $subespecialidade['logradouro']; ?></td>
                                <td><?= $subespecialidade['nuLogradouro']; ?></td>
                                <td><?= $subespecialidade['complLogradouro']; ?></td>
                                <td><?= $subespecialidade['cepLogradouro']; ?></td>
                                <td><?= $subespecialidade['nmBairro']; ?></td>
                                <td><?= $subespecialidade['UF']; ?></td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
            <br>
            <!-- Fim do Conteudo -->
        </div>
        <!-- Inicio do Rodadpé-->
        <?php include(kohana::find_file('views/templates/csm', 'footer', 'php')) ?>
        <!-- Fim do Rodadpé-->
    </body>
</html>
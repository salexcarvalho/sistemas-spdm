<!DOCTYPE html>
<html lang="pt-br">

    <head>
        <title><?= SISTEMA; ?> - Serviços</title>
        <?php include_once(kohana::find_file('views/templates/csm', 'init', 'php')) ?>
        <script>
            jQuery(document).ready(function()
            {
                // Remove seleção de ativo no menu.
                $('.navbar-nav li').removeClass('active');
                $('#menu-superior li').removeClass('active');
                // Ativa botão no menu.
                $('.navbar-nav #mn_servicos').addClass('active');

                // Evento de clique para tabela de especialidades.
                $(document).on("click", "#tb_especialidades tbody tr", function()
                {
                    // Muda cursor para modo de espera.
                    $("body").css("cursor", "wait");
                    $.get("lista_subespecialidades",
                            {
                                idespecialidade: $(this).attr("idespecialidade"),
                                nmespecialidade: $(this).children(":first").text().trim()
                            })
                            .done(function(data)
                            {
                                $("body").html(data);
                            })
                            .fail(function()
                            {
                                alert("Ocorreu um erro na sua requisição.");
                            })
                            .always(function()
                            {
                                // Muda cursor para normal após obter resposta do AJAX.
                                $("body").css("cursor", "default");
                            });
                });
            });
        </script>
    </head>
    <body>
        <div class="content">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/csm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/csm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <!-- Inicio do Conteudo -->
            <div id="div_content" class="table-responsive">
                <table id="tb_especialidades" class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>
                                AMBULATORIO / ESPECIALIDADE
                            </th>
                            <th>
                                QTD SUBS
                            </th>
                            <th>
                                CENTRO CUSTO
                            </th>
                            <th>
                                LOGRADOURO
                            </th>
                            <th>
                                NUMERO LOGRADOURO
                            </th>
                            <th>
                                COMPLEMENTO LOGRADOURO
                            </th>
                            <th>
                                CEP LOGRADOURO
                            </th>
                            <th>
                                BAIRRO
                            </th>
                            <th>
                                UF
                            </th>
                        </tr>
                    </thead>
                    <tbody class="btn-tr">
                        <?php
                        /*foreach ($especialidades as $especialidade)
                        {
                            ?>
                            <tr idespecialidade="<?= $especialidade['idEspecialidade'] ?>">
                                <td><?= ($especialidade['nmCentroCusto']) ? $especialidade['nmCentroCusto'] : 'Nome Centro de Custo ausente';?><?= ' / '?><?= $especialidade['nmEspecialidade'] ?></td>
                                <td><?= $especialidade['qtdSub'] ?></td>
                                <td><?= ($especialidade['idCentroCusto']) ? $especialidade['idCentroCusto'] : ''; ?></td>
                                <td><?= ($especialidade['logradouro']) ? $especialidade['logradouro'] : ''; ?></td>
                                <td><?= ($especialidade['nuLogradouro']) ? $especialidade['nuLogradouro'] : ''; ?></td>
                                <td><?= ($especialidade['complLogradouro']) ? $especialidade['complLogradouro'] : ''; ?></td>
                                <td><?= ($especialidade['cepLogradouro']) ? $especialidade['cepLogradouro'] : ''; ?></td>
                                <td><?= ($especialidade['nmBairro']) ? $especialidade['nmBairro'] : ''; ?></td>
                                <td><?= ($especialidade['UF']) ? $especialidade['UF'] : ''; ?></td>
                            </tr>
                            <?php
                        }*/
                        ?>
                    </tbody>
                </table>
            </div>
            <br>
            <!-- Fim do Conteudo -->
        </div>
        <!-- Inicio do Rodadpé-->
        <?php include(kohana::find_file('views/templates/csm', 'footer', 'php')) ?>
        <!-- Fim do Rodadpé-->
    </body>
</html>
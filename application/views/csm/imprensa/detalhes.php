<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= SISTEMA; ?> - Imprensa</title>
        <?php include(kohana::find_file('views/templates/csm', 'init', 'php')) ?>
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
        <script type="text/javascript" src="<?= URL::base(); ?>jquery/js/gmaps.js"></script>

        <script>
            jQuery(document).ready(function () {
                // Remove seleção de ativo no menu.
                $('.navbar-nav li').removeClass('active');
                $('#menu-superior li').removeClass('active');
                // Ativa botão no menu.
                $('.navbar-nav #mn_imprensa').addClass('active');
            });
        </script>
        <script type="text/javascript">
            var map;
            $(document).ready(function () {
                map = new GMaps({
                    el: '#mappq',
                    zoom: 16,
                    lat: -23.597940,
                    lng: -46.643960,
                    zoomControl: true,
                    zoomControlOpt: {
                        style: 'SMALL',
                        position: 'TOP_LEFT'
                    },
                    panControl: false,
                    streetViewControl: false,
                    mapTypeControl: false,
                    overviewMapControl: false
                });
                map.addMarker({
                    lat: -23.597940,
                    lng: -46.643960,
                    title: 'Hospital São Paulo',
                    infoWindow: {
                        content: '<p>Teste</p>'
                    }
                });
            });
        </script>
    </head>
    <body>
        <div class="content">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/csm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/csm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <!-- Inicio do Conteudo -->
            <div class="row">
                <div class="col-md-8">
                    <h4 class="bold uppercase azul">Imprensa</h4>
                    <img src="<?= URL::base(); ?>images/oftalmo.jpg" class="img-thumbnail img-responsive img-left" width="274">
                    <p class="marginleft">Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam id dolor id nibh ultricies vehicula.</p>
                    <p class="marginleft">Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec ullamcorper nulla non metus auctor fringilla. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Donec ullamcorper nulla non metus auctor fringilla.</p>
                    <p class="marginleft">Maecenas sed diam eget risus varius blandit sit amet non magna. Donec id elit non mi porta gravida at eget metus. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.</p>
                    <p class="marginleft">Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam id dolor id nibh ultricies vehicula.</p>

                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="bold azul">Diagnóstico e Tratamento</h4>
                            <p class="marginleft">Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam id dolor id nibh ultricies vehicula.</p>
                            <p class="marginleft">Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec ullamcorper nulla non metus auctor fringilla. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Donec ullamcorper nulla non metus auctor fringilla.</p>
                            <p class="marginleft">Maecenas sed diam eget risus varius blandit sit amet non magna. Donec id elit non mi porta gravida at eget metus.</p>
                            <button type="button" class="btn btn-info marginleft">SAIBA MAIS</button>
                        </div>
                        <div class="col-md-6">
                            <h4 class="bold azul">Vídeo Informativo</h4>
                            <video width="375" height="262" class="img-thumbnail" controls>
                                <source src="<?= URL::base(); ?>video/big_buck_bunny_720p_surround.avi" type="video/mp4">
                                Your browser does not support the video tag.
                            </video>
                        </div>
                    </div>

                </div>
                <div class="col-md-4">
                    <div class="well well-sm">
                        <h5 class="bold uppercase azul">Como Ser Atendido?</h5>
                        <p>Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam id dolor id nibh ultricies vehicula.</p>
                    </div>

                    <h5 class="bold uppercase azul">Localização</h5>
                    <div id="mappq" class="img-thumbnail"></div>

                </div>
            </div>
            <br>    
            <!-- Fim do Conteudo -->
        </div>
        <!-- Inicio do Rodadpé-->
        <?php include(kohana::find_file('views/templates/csm', 'footer', 'php')) ?>
        <!-- Fim do Rodadpé-->
    </body>
</html>


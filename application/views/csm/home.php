<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= SISTEMA; ?> - Home</title>
        <?php include(kohana::find_file('views/templates/csm', 'init', 'php')) ?>
        <script>
            jQuery(document).ready(function ()
            {
                // Remove seleção de ativo no menu.
                $('.navbar-nav li').removeClass('active');
                $('#menu-superior li').removeClass('active');
                // Ativa botão no menu.
                $('.navbar-nav #mn_home').addClass('active');
            });
        </script>
    </head>
    <body>
        <div class="content">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/csm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/csm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <!-- Inicio do Conteudo -->
            <div id="div_content" class="row">
                <div class="col-md-8">
                    <h4 class="bold uppercase azul">O que é o Catálogo de Serviços Médicos?</h4>
                    <p>Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam id dolor id nibh ultricies vehicula.</p>

                    <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec ullamcorper nulla non metus auctor fringilla. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Donec ullamcorper nulla non metus auctor fringilla.</p>

                    <p>Maecenas sed diam eget risus varius blandit sit amet non magna. Donec id elit non mi porta gravida at eget metus. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.</p>

                    <p>Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam id dolor id nibh ultricies vehicula.</p>

                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="uppercase">Onde estamos?</h4>
                            <img src="<?= URL::base(); ?>images/catalogo.jpg" class="img-thumbnail" style="width: 100%; height: 207px">
                        </div>
                        <div class="col-md-6">
                            <h4 class="uppercase">Como posso usar o serviço?</h4>
                            <video class="img-thumbnail" controls style="width: 100%">
                                <source src="<?= URL::base(); ?>videos/Encerramento.mp4" type="video/mp4">
                                Seu navegador n&atilde;o suporta recursos de video, recomendamos o Google Chrome.
                            </video>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <h4 class="bold uppercase azul"><strong>Serviços mais procurados</strong></h4>
                    <div id="tabela">
                        <table id="tb_especialidades" class="table table-striped table-hover">
                            <thead></thead>
                            <tbody class="btn-tr">
                                <?php
                                foreach ($servicos as $servico) {
                                    ?>
                                    <tr onclick="window.location = '../servico/lista_servicos_acessos?idServico=<?= $servico['idServico']; ?>'">
                                        <td><?= $servico['nmServico']; ?></td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <br>    
            <!-- Fim do Conteudo -->
        </div>
        <!-- Inicio do Rodapé-->
        <?php include(kohana::find_file('views/templates/csm', 'footer', 'php')) ?>
        <!-- Fim do Rodapé-->
    </body>
</html>
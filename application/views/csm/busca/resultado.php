<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?= SISTEMA; ?> - Home</title>
        <?php include(kohana::find_file('views/templates/csm', 'init', 'php')) ?>
        <script>
            jQuery(document).ready(function () {
                // Remove seleção de ativo no menu.
                $('.navbar-nav li').removeClass('active');
                $('#menu-superior li').removeClass('active');
                // Ativa botão no menu.
                $('.navbar-nav #mn_busca').addClass('active');
            });
        </script>
    </head>
    <body>
        <div class="content">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/csm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/csm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <!-- Inicio do Conteudo -->
            <div class="row">
                <div class="col-md-8">
                    <h4 class="bold uppercase azul">Resultado da busca</h4>
                    <p class="marginleft">Foram encontradas <span class="text-success">03 resultados</span>, com o termo <em class="text-danger">"DOR DE BARRIGA"</em>.</p>

                    <div class="results">
                        <section class="result-part">
                            <h5><a href="#" class="text-danger bold uppercase">Dor de barriga</a>  |  <span class="text-info small uppercase"><strong>Especialidade:</strong> Gastrocliníca</span></h5>
                            <p class="marginleft">Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec ullamcorper nulla non metus auctor fringilla. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Donec ullamcorper nulla non metus auctor fringilla.</p>
                        </section>
                    </div>

                    <div id="paginacao" class="center">
                        <ul class="pagination">
                            <li><a href="#">&laquo;</a></li>
                            <?php
                            if (isset($pag)) {
                                for ($i = 0; $i < count($pag); $i++) {
                                    ?>
                                    <li <?= ($pag == $i) ? 'class="active"' : ''; ?> class="active"><a href="#">1</a></li>
                                    <?php
                                }
                            }
                            ?>
                            <li><a href="#">&raquo;</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4">
                    <h4 class="bold uppercase">Serviços mais procurados</h4>
                    <div id="tabela">
                        <table class="table table-striped table-hover">
                            <thead></thead>
                            <tbody class="btn-tr">
                                <tr>
                                    <td>Otorrinolaringologia</td>
                                </tr>
                                <tr>
                                    <td>Cirurgia Vascular</td>
                                </tr>
                                <tr>
                                    <td>Neurologia</td>
                                </tr>
                                <tr>
                                    <td>Oftalmologia</td>
                                </tr>
                                <tr>
                                    <td>Otorrinolaringologia</td>
                                </tr>
                                <tr>
                                    <td>Cirurgia Vascular</td>
                                </tr>
                                <tr>
                                    <td>Neurologia</td>
                                </tr>
                                <tr>
                                    <td>Oftalmologia</td>
                                </tr>
                                <tr>
                                    <td>Otorrinolaringologia</td>
                                </tr>
                                <tr>
                                    <td>Cirurgia Vascular</td>
                                </tr>
                                <tr>
                                    <td>Neurologia</td>
                                </tr>
                                <tr>
                                    <td>Oftalmologia</td>
                                </tr>
                                <tr>
                                    <td>Otorrinolaringologia</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
            <br>    
            <!-- Fim do Conteudo -->
        </div>
        <!-- Inicio do Rodadpé-->
<?php include(kohana::find_file('views/templates/csm', 'footer', 'php')) ?>
        <!-- Fim do Rodadpé-->

    </body>
</html>


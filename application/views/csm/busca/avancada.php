<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?= SISTEMA; ?> - Busca Avançada</title>
        <?php include(kohana::find_file('views/templates/csm', 'init', 'php')) ?>
        <script>
            jQuery(document).ready(function () {
                // Remove seleção de ativo no menu.
                $('.navbar-nav li').removeClass('active');
                $('#menu-superior li').removeClass('active');
                // Ativa botão no menu.
                $('.navbar-nav #mn_busca').addClass('active');
            });
        </script>
    </head>
    <body>
        <div class="content">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/csm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/csm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <!-- Inicio do Conteudo -->
            <div class="row">
                <div class="col-md-8">
                    <h4 class="bold uppercase azul">Busca Avançada</h4>
                    <p class="marginleft">Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam id dolor id nibh ultricies vehicula.</p>
                    <form class="form-horizontal marginleft" role="form">
                        <div class="form-group col-md-12">
                            <input type="search" class="form-control" placeholder="Digite aqui o termo da sua pesquisa...">
                            <div class="row">
                                <div class="col-md-5">
                                    <select class="form-control" id='especialidade'>
                                        <option selected='selected'>Selecione uma Especialidade</option>
                                        <option>CARDIOLOGIA</option>
                                        <option>CASA DA MÃO</option>
                                        <option>CATARATA</option>
                                        <option>GINECOLOGIA</option>
                                    </select>
                                </div>
                                <div class="col-md-7">
                                    <select class="form-control" id='subespecialidade'>
                                        <option selected='selected'>Selecione uma Subespecialidade</option>
                                        <option>CARDIOLOGIA</option>
                                        <option>CASA DA MÃO</option>
                                        <option>CATARATA</option>
                                        <option>GINECOLOGIA</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5">
                                    <select class="form-control" id='Estado'>
                                        <option selected='selected'>Selecione um Estado</option>
                                        <option>SÃO PAULO</option>
                                        <option>MINAS GERAIS</option>
                                        <option>SANTA CATARINA</option>
                                        <option>RIO DE JANEIRO</option>
                                    </select>
                                </div>
                                <div class="col-md-7">
                                    <select class="form-control" id='cidade'>
                                        <option selected='selected'>Selecione uma Cidade</option>
                                        <option>SÃO PAULO</option>
                                        <option>SÃO JOSÉ DOS CAMPOS</option>
                                        <option>SANTOS</option>
                                        <option>UBATUBA</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <select class="form-control" id='TIPO'>
                                        <option selected='selected'>Selecione um tipo de atendimento</option>
                                        <option>HOSPITALAR</option>
                                        <option>AMBULATORIAL</option>
                                        <option>CLINICO</option>
                                        <option>URGÊNCIA</option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <button type="submit" class="btn btn-success">BUSCAR</button>
                                    <button type="reset" class="btn btn-success">LIMPAR</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-4">
                    <h4 class="bold uppercase azul"><strong>Serviços mais procurados</strong></h4>
                    <div id="tabela">
                        <table id="tb_especialidades" class="table table-striped table-hover">
                            <thead></thead>
                            <tbody class="btn-tr">
                                <?php
                                foreach ($servicos as $servico) {
                                    ?>
                                    <tr onclick="window.location = '../servico/lista_servicos_acessos?idServico=<?= $servico['idServico']; ?>'">
                                        <td><?= $servico['nmServico']; ?></td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <br>    
            <!-- Fim do Conteudo -->
        </div>
        <!-- Inicio do Rodadpé-->
        <?php include(kohana::find_file('views/templates/csm', 'footer', 'php')) ?>
        <!-- Fim do Rodadpé-->

    </body>
</html>


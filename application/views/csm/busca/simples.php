<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= SISTEMA; ?> - Busca Simples</title>
        <?php include(kohana::find_file('views/templates/csm', 'init', 'php')) ?>
        <script>
            jQuery(document).ready(function () {
                // Remove seleção de ativo no menu.
                $('.navbar-nav li').removeClass('active');
                $('#menu-superior li').removeClass('active');
                // Ativa botão no menu.
                $('.navbar-nav #mn_busca').addClass('active');
            });
        </script>
    </head>
    <body>
        <div class="content">
            <!-- Inicio do Header -->
            <?php include(kohana::find_file('views/templates/csm', 'header', 'php')) ?>
            <!-- Fim do Header -->
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/csm', 'menu', 'php')) ?>
            <!-- Fim do Menu -->
            <!-- Inicio do Conteudo -->
            <div class="row">
                <div class="col-md-8">
                    <h4 class="bold uppercase azul">Busca Simples</h4>
                    <p class="marginleft">Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam id dolor id nibh ultricies vehicula.</p>
                    <form class="form-horizontal marginleft" role="form">
                        <div class="form-group col-md-12">
                            <div class="row">
                                <div class="col-md-9">
                                    <input type="search" class="form-control" placeholder="Digite aqui o termo da sua pesquisa...">
                                </div>
                                <div class="col-md-3">
                                    <button type="submit" class="btn btn-success">BUSCAR</button>
                                    <button type="reset" class="btn btn-success">LIMPAR</button>
                                </div>
                            </div>
                        </div>
                    </form>  
                </div>
                <div class="col-md-4">
                    <h4 class="bold uppercase">Serviços mais procurados</h4>
                    <div id="tabela">
                        <table class="table table-striped table-hover">
                            <thead></thead>
                            <tbody class="btn-tr">
                                <tr>
                                    <td>Otorrinolaringologia</td>
                                </tr>
                                <tr>
                                    <td>Cirurgia Vascular</td>
                                </tr>
                                <tr>
                                    <td>Neurologia</td>
                                </tr>
                                <tr>
                                    <td>Oftalmologia</td>
                                </tr>
                                <tr>
                                    <td>Otorrinolaringologia</td>
                                </tr>
                                <tr>
                                    <td>Cirurgia Vascular</td>
                                </tr>
                                <tr>
                                    <td>Neurologia</td>
                                </tr>
                                <tr>
                                    <td>Oftalmologia</td>
                                </tr>
                                <tr>
                                    <td>Otorrinolaringologia</td>
                                </tr>
                                <tr>
                                    <td>Cirurgia Vascular</td>
                                </tr>
                                <tr>
                                    <td>Neurologia</td>
                                </tr>
                                <tr>
                                    <td>Oftalmologia</td>
                                </tr>
                                <tr>
                                    <td>Otorrinolaringologia</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
            <br>    
            <!-- Fim do Conteudo -->
        </div>
        <!-- Inicio do Rodadpé-->
        <?php include(kohana::find_file('views/templates/csm', 'footer', 'php')) ?>
        <!-- Fim do Rodadpé-->

    </body>
</html>


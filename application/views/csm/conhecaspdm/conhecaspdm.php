<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= SISTEMA; ?> - Conheça a SPDM</title>
        <?php include(kohana::find_file('views/templates/csm', 'init', 'php')) ?>
        <script>
            jQuery(document).ready(function ()
            {
                // Remove seleção de ativo no menu.
                $('.navbar-nav li').removeClass('active');
                $('#menu-superior li').removeClass('active');
                // Ativa botão no menu.
                $('.navbar-nav #mn_conheca').addClass('active');
            });
        </script>
    </head>
    <body>
        <div class="content">
            <?php include(kohana::find_file('views/templates/csm', 'header', 'php')) ?>
            <?php include(kohana::find_file('views/templates/csm', 'menu', 'php')) ?>
            <div id="div_content" class="row">
                <div class="col-md-8">
                    <?php
                    foreach ($artigos as $artigo) {
                        if ($artigo['publicado'] == 'S') {
                            // div de com classe destaque não possui css definido.
                            ?>
                            <div <?= ($artigo['destaque'] == 'S') ? 'class="destaque"' : ''; ?>>
                                <h4 class="bold uppercase azul"><?= $artigo['tituloArtigo']; ?></h4>
        <?php
        if ($artigo['caminhoImagem'] != NULL) {
            ?>
                                    <img src="<?= $artigo['caminhoImagem'] . $artigo['nmImagem']; ?>" class="img-thumbnail img-responsive img-left">
                                    <?php
                                }
                                ?>
                                <?= ($artigo['aliasArtigo'] != NULL) ? '<p class="marginleft">' . $artigo['aliasArtigo'] . '</p>' : ''; ?>
                                <?= ($artigo['textoIntro'] != NULL) ? '<p class="marginleft">' . $artigo['aliasArtigo'] . '</p>' : ''; ?>
                                <?php
                                // A formatação do texto do artigo deve ser feita em HTML direto no banco de dados.
                                ?>
                                <?= $artigo['textoCompleto']; ?>
                            </div>
                                <?php
                            }
                        }
                        ?>
                </div>
                <div class="col-md-4">
                    <h4 class="bold uppercase azul"><strong>Serviços mais procurados</strong></h4>
                    <div id="tabela">
                        <table id="tb_especialidades" class="table table-striped table-hover">
                            <thead></thead>
                            <tbody class="btn-tr">
<?php
foreach ($servicos as $servico) {
    ?>
                                    <tr onclick="window.location = '../servico/lista_servicos_acessos?idServico=<?= $servico['idServico']; ?>'">
                                        <td><?= $servico['nmServico']; ?></td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <br>
        </div>
<?php include(kohana::find_file('views/templates/csm', 'footer', 'php')) ?>
    </body>
</html>
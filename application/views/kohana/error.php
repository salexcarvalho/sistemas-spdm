<?php Session::instance(); ?>
<?php Funcoes::carrega(); //echo "<pre>"; var_dump($_SESSION['idUsuario']); echo "</pre>";   ?>
<?php
// Unique error identifier
$error_id = uniqid('error');
?>
<!DOCTYPE html>
<html lang="nl" dir="ltr">
    <head>

        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="noindex, nofollow" />

        <title>SPDM - Associação Paulista para o Desenvolvimento da Medicina</title>

        <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" />
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" />
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/1.4.1/css/ionicons.min.css">

        <style>
            body {
                background: #fbfbfb;
                font-family: 'Source Sans Pro', sans-serif;
            }
            .well {
                margin: 50px auto;
                text-align: center;
                padding: 25px;
                max-width: 600px;
            }
            h1, h2, h3, p {
                margin: 0;
            }
            p {
                font-size: 17px;
                margin-top: 25px;
            }
            p a.btn {
                margin: 0 5px;
            }
            h1 .ion {
                vertical-align: -5%;
                margin-right: 5px;
            }
        </style>

    </head>
    <body>
        <div class="container">
            <br><br><br>
            <div class="well">
                <div class="row">
                    <center><img style="-webkit-user-select: none" src="<?= URL::base(); ?>upload/config/<?= $_SESSION['logo_admin'] ?>"></center>
                </div>
                <br>
                <br>
                <?php if ($code == 400) { ?>                
                    <h1><div class="ion ion-alert-circled"></div> 400 - Solicitação incorreta</h1>
                <?php } else if ($code == 401) { ?>
                    <h1><div class="ion ion-alert-circled"></div> 401 - Autorização necessária</h1>
                <?php } else if ($code == 403) { ?>
                    <h1><div class="ion ion-alert-circled"></div> 403 - Acesso restrito</h1>
                    <p>Está area do site possui acesso restrito.</p>
                <?php } else if ($code == 404) { ?>
                    <h1><div class="ion ion-alert-circled"></div> 404 - Página não encontrada</h1>
                    <p>Infelizmente houve um erro e não podemos processar a sua solicitação.</p>
                <?php } else if ($code == 500) { ?>
                    <h1><div class="ion ion-alert-circled"></div> 500 - Erro interno do servidor</h1>
                    <p>Houve um erro interno do servidor, comunique o administrador.</p>
                <?php } else { ?>
                    <h1><div class="ion ion-alert-circled"></div> Erro: <?php echo $code; ?> </h1>
                <?php } ?>  

                <p>
                    <a class="btn btn-default btn-lg" href="#" onclick="history.back(); return false;">Voltar</a>
                    <a class="btn btn-default btn-lg" href="<?= URL::base(); ?>dashboad" >Ir para Home</a>
                </p>
            </div>
        </div>

    </body>
</html>

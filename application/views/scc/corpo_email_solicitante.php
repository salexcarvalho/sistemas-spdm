<html>
    <head>
    </head>
    <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" yahoo="fix" spellcheck="false">
        <table width="660" border="0" cellpadding="0" cellspacing="0" align="center" class="full" bgcolor="#f5f5f5" style="border-color: rgb(247, 247, 247); font-weight: bold; font-size: 18px;">
            <tbody>
                <tr>
                    <td width="660" valign="top" align="center">
                        <table width="660" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
                            <tbody>
                                <tr>
                                    <td width="660" height="140" align="center">
                                        <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="full">
                                            <tbody>
                                                <tr>
                                                    <td width="325" align="right" valign="middle" class="logo"><table width="125" border="0" cellpadding="0" cellspacing="0" align="left" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
                                                            <tbody>
                                                                <tr>
                                                                    <td height="55" valign="middle" align="center" width="660" class="fullCenter"><img src="<?= LOGO ?>" alt="logo" height="80" class="hover" /></a></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                    <td align="right" valign="middle" style="font-size: 22px; font-family: Verdana; font-weight: bold;">
                                                        Canal Confidencial
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
        <table width="660" border="0" cellpadding="0" cellspacing="0" align="center" class="full" bgcolor="#ffffff" style="background-color: rgb(255, 255, 255);">
            <tbody>
                <tr>
                    <td width="660" align="center" valign="top" bgcolor="#f5f5f5">
                        <table width="660" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
                            <tbody>
                                <tr>
                                    <td align="center" bgcolor="#f5f5f5">
                                        <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" style="font-family: Verdana; font-size: 12px;">
                                            <tbody>
                                                <tr>
                                                    <td height="20" bgcolor="#f5f5f5"><h2>Relato ID: <span style="color:#f00;"><?= $Protocolo; ?></span></h2></td>
                                                </tr>
                                                <tr>
                                                    <td height="10" bgcolor="#f5f5f5">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td height="25" bgcolor="#f5f5f5">

                                                        <h3>Dados do Relato</h3>
                                                        <p>
                                                            <strong>Nome:</strong> <?= $vars['nome_solicitante']; ?><br /> 
                                                            <strong>E-mail:</strong> <?= $vars['email_solicitante']; ?><br />
                                                            <strong>Cargo/Fun&ccedil;&atilde;o :</strong> <?= $vars['cargo_solicitante']; ?><br /> 
                                                            <strong>V&iacute;nculo: </strong><?= $vars['vinculo_name']; ?></p>

                                                        <h3 style="color: #000000;"> Informa&ccedil;&otilde;es sobre o Relato</h3>
                                                        <p>
                                                            <strong>Tipo do relato:</strong> <?= $vars['tipo_name']; ?><br />
                                                            <strong>Unidade do relato:</strong> <?= $vars['unidade_name']; ?><br />
                                                            <strong>Distritos (DSEI) conveniados: </strong><?= $vars['dsei_name']; ?><br />
                                                            <strong>Quando ocorreu: </strong><?= $vars['quando_relato']; ?><br />
                                                            <strong>Onde: </strong><?= $vars['onde_relato']; ?><br />
                                                            <strong>Nome dos Envolvidos: </strong><?= $vars['nome_envolvidos']; ?></p>

                                                        <h3 style="color: #000000;">Descri&ccedil;&atilde;o do Relato</h3>

                                                        <p><?= $vars['descricao_da_situacao']; ?></p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td height="25" bgcolor="#f5f5f5"></td>
                                                </tr>
                                                <tr>
                                                    <td height="25" align="center" valign="middle" bgcolor="#f5f5f5"><span class="fullCenter">
                                                            <a href="http://<?= $_SERVER['SERVER_NAME'] ?><?= URL::base(); ?>canalconfidencial/solicitacao/visualizar_relato?idProtocolo=<?= $Protocolo; ?>" target="_blank">
                                                                <img src="http://<?= $_SERVER['SERVER_NAME'] ?><?= URL::base(); ?>images/scc/btn_acao_acompanhar.jpg" class="hover" />
                                                            </a>
                                                        </span></td>
                                                </tr>
                                                <tr>
                                                    <td height="25" bgcolor="#f5f5f5"></td>
                                                </tr>
                                            </tbody>
                                        </table></td>
                                </tr>
                                <tr>
                                    <td height="35" align="center" bgcolor="#f5f5f5">&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
        <table width="660" border="0" cellpadding="0" cellspacing="0" align="center" class="full" bgcolor="#366829" style="background-color: rgb(41, 128, 185); border-color: rgb(41, 128, 185); color: rgba(255,255,255,1);">
            <tbody>
                <tr>
                    <td width="660" align="center" valign="top">
                        <table width="660" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile">
                            <tbody>
                                <tr>
                                    <td valign="top" align="center">
                                        <table width="660" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; color: #FFFFFF;" class="fullCenter">
                                            <tbody>
                                                <tr>
                                                    <td width="660" height="60" align="left" valign="middle" style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 14px; color: #000; line-height: 22px;">
                                                        <p style="color: #FFFFFF; font-size: 12px;" class="underline"><span style="font-family: 'proxima_nova_rgregular', Helvetica; font-weight: bold; text-align: left; font-size: 20px; color: #FFFFFF; ">              <!--<![endif]--></span><strong>Mensagem enviada atravês do Canal Confidencial - SPDM</strong></p>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                    </td>
                </tr>
            </tbody>
        </table>
        <style type="text/css">
            .pic185 {width:185px;height:175px;overflow:hidden;}
            .grow185 img{width:185px;height:175px;
                         -webkit-transition:all 1.5s ease;-moz-transition:all 1s ease;-o-transition:all 1s ease;-ms-transition:all 1s ease;transition:all 1s ease;}
            .grow185 img:hover{width:230px;height:210px;}

            .pic600 {width:600px;height:300px;overflow:hidden;}
            .grow600 img{width:600px;height:300px;
                         -webkit-transition:all 1.5s ease;-moz-transition:all 1s ease;-o-transition:all 1s ease;-ms-transition:all 1s ease;transition:all 1s ease;}
            .grow600 img:hover{width:660px;height:380px;}

            .pic280 {width:280px;height:200px;overflow:hidden;}
            .grow280 img{width:280px;height:200px;
                         -webkit-transition:all 1.5s ease;-moz-transition:all 1s ease;-o-transition:all 1s ease;-ms-transition:all 1s ease;transition:all 1s ease;}
            .grow280 img:hover{width:340px;height:260px;}

            .pic298 {width:298px;height:402px;overflow:hidden;}
            .grow298 img{width:298px;height:402px;
                         -webkit-transition:all 1.5s ease;-moz-transition:all 1s ease;-o-transition:all 1s ease;-ms-transition:all 1s ease;transition:all 1s ease;}
            .grow298 img:hover{width:370px;height:470px;}
        </style>
    </body>
</html>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?= SISTEMA; ?> - Canal Confidencial</title>
        <?php include(kohana::find_file('views/templates/scc', 'init', 'php')) ?>
        <script>
            jQuery(document).ready(function ()
            {
                // Remove seleção de ativo no menu.
                $('.navbar-nav li').removeClass('active');
                $('#menu-superior li').removeClass('active');
                // Ativa botão no menu.
                $('.navbar-nav #mn_acompanhar').addClass('active');
            });
        </script>
    </head>
    <body>
        <div class="content">
            <?php include(kohana::find_file('views/templates/scc', 'header', 'php')) ?>
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/scc', 'menu', 'php')) ?>
            <!-- Fim do Menu -->

            <div class="container" >
                <div class="row">
                    <div class="col-lg-12">
                        <h4 class="bold uppercase azul">Acompanhar Relato</h4>
                        <hr style="width: 98%;">
                    </div>
                </div>            
                <div class="row col-lg-12">
                    <?php if (isset($warning)) : ?>
                        <div id="msg" class="alert alert-danger alert-dismissible" role="alert" >
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <strong><?= $warning; ?></strong>
                        </div>
                    <?php endif ?>
                    <p>Para acompanhar o andamento do seu relato, por favor digite o ID do seu protocolo no campo a abaixo:</p>
                    <br>
                </div>
                <form class="form-horizontal" role="form" method="POST" action="visualizar_relato">
                    <div class="form-group col-lg-12">
                        <div class="col-lg-3">
                            <label>Informe o ID do protocolo<span id="obrigatorio" style="color: #ff0000;">*</span></label>
                        </div>    
                        <div class="col-lg-3">
                            <input type="text" name="idProtocolo" id="idProtocolo" class="form-control" maxlength="16" size="16" required>
                        </div>  
                    </div>
                    <div class="form-group col-lg-12">
                        <div class="col-lg-3"></div>
                        <div class="col-lg-5">
                            <button class="btn btn-md btn-primary uppercase" type="submit" name="submit" id="submit" value="">Consultar</button>
                            <button class="btn btn-md btn-info uppercase" type="reset" name="reset" id="reset" value="">Cancelar</button>
                        </div> 
                    </div> 
                </form>    
                <div class="pager" style="height: 180px;"></div>
            </div>

        </div>
    </div>
    <?php include(kohana::find_file('views/templates/scc', 'footer', 'php')) ?>

    <script>

        jQuery(document).ready(function ()
        {
            // Verifica se tem o certificado
            $("body").on("click", "button[name=btnValidacao]", function ()
            {
                var autenticacao = $('#autenticacao').val();
                $('button#btnValidacao').text("Processando");
                $.ajax("gerapdf",
                        {
                            type: "get",
                            data:
                                    {
                                        autenticacao: autenticacao
                                    },
                            success: function () {
                                $('#sucesso').addClass('visible-lg');
                                $('#sucesso').removeClass('hidden');

                                var counter = 5;
                                setInterval(function () {
                                    counter--;
                                    if (counter >= 0) {
                                        span = document.getElementById("count");
                                        span.innerHTML = counter;
                                    }
                                    // Display 'counter' wherever you want to display it.
                                    if (counter === 0) {
                                        window.open('gerapdf?autenticacao=' + autenticacao, '_blank');
                                        window.location.replace("home");
                                        clearInterval(counter);
                                    }

                                }, 1000);
                                $('button#btnValidacao').text("Validar");

                            },
                            error: function (data) {
                                $('button#btnValidacao').text("Validar");
                                $('#erro').addClass('visible-lg');
                                $('#erro').removeClass('hidden');
                                var counter = 5;
                                setInterval(function () {
                                    counter--;
                                    if (counter >= 0) {
                                        span = document.getElementById("counterro");
                                        span.innerHTML = counter;
                                    }
                                    if (counter === 0) {
                                        window.location.replace("home");
                                        clearInterval(counter);
                                    }

                                }, 1000);
                            }
                        });
            });
        });

    </script>

</body>
</html>
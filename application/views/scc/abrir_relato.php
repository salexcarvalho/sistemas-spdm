<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?= SISTEMA; ?> - Canal Confidencial</title>
        <?php include(kohana::find_file('views/templates/scc', 'init', 'php')) ?>
        <script>
            jQuery(document).ready(function ()
            {
                // Remove seleção de ativo no menu.
                $('.navbar-nav li').removeClass('active');
                $('#menu-superior li').removeClass('active');
                // Ativa botão no menu.
                $('.navbar-nav #mn_relato').addClass('active');
            });
        </script>
    </head>
    <body>
        <div class="content">
            <?php include(kohana::find_file('views/templates/scc', 'header', 'php')) ?>
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/scc', 'menu', 'php')) ?>
            <!-- Fim do Menu -->

            <div class="container" >
                <div class="row">
                    <div class="col-lg-12">
                        <h4 class="bold uppercase azul">Realizar relato</h4>
                        <hr style="width: 98%;">
                    </div>
                </div>                
                <div class="row col-lg-12">
                    <p>Você pode fazer um registro anônimo ou identificado.</p>                   
                    <p>A opção identificada é voltada para situações onde o relator se disponibiliza a ser contatado para esclarecimento de possíveis dúvidas sobre o relato fornecido.</p>
                    <p>Lembrando que a veracidade das informações relatadas é de responsabilidade do relator.</p>
                    <p>Registros identificados são muito importantes para um processo de averiguação rápido e objetivo. Todas as informações são tratadas de forma sigilosa e sua identidade estará protegida.</p>
                </div>
                <div class="row col-lg-12" style="margin-top: 15px; margin-bottom: 15px; margin-left: 30px;">

                    <p class="col-lg-2">Você deseja se identificar?</p>
                    <div class="form-group col-lg-8">
                        <button class="btn btn-md btn-primary uppercase" type="button" name="sim" id="sim" value="true">Sim</button>&nbsp;
                        <button class="btn btn-md btn-danger uppercase" type="button" name="nao" id="nao" value="false">Não</button>
                    </div>

                </div>
                <div class="row container">
                    <form class="form-horizontal" role="form" method="POST" action="dados_relato">
                        <div id="nome_solicitante_grupo" class="form-group col-lg-12" style="display: none;">
                            <div class="col-lg-2">
                                <label id="nome_solicitante_label">Nome</label>
                            </div>    
                            <div class="col-lg-9">
                                <input type="text" name="nome_solicitante" id="nome_solicitante" class="form-control" placeholder="Digite aqui o seu nome">
                            </div>    
                        </div>
                        <div id="email_solicitante_grupo" class="form-group col-lg-12" style="display: none;">
                            <div class="col-lg-2">
                                <label>Email</label>
                            </div>    
                            <div class="col-lg-9">
                                <input type="text" name="email_solicitante" id="email_solicitante" class="form-control" placeholder="Digite aqui o seu e-mail">
                            </div>    
                        </div>
                        <div class="form-group col-lg-12">
                            <div id="cargo_solicitante_grupo" style="display: none;">
                                <div class="col-lg-2">
                                    <label>Cargo/Função<span id="obrigatorio" style="color: #ff0000;">*</span></label>
                                </div>    
                                <div class="col-lg-9">
                                    <input type="text" name="cargo_solicitante" id="cargo_solicitante" class="form-control" placeholder="Digite aqui o seu cargo/função">
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-lg-12">
                            <div id="vinculo_solicitante_grupo"  style="display: none;">
                                <div class="col-lg-2">
                                    <label>Vínculo<span id="obrigatorio" style="color: #ff0000;">*</span></label>
                                </div>    
                                <div class="col-lg-9">
                                    <select class="form-control" id='vinculo_solicitante' name='vinculo_solicitante' required="">
                                        <option value="" selected='selected'>Selecione uma Opção</option>
                                        <?php foreach ($vinculos as $vinculo) { ?>
                                            <option value="<?= $vinculo['idVinculo']; ?>"><?= $vinculo['Nome_Vinculo']; ?></option>
                                        <?php } ?>
                                    </select>
                                    <input type="hidden" id="vinculo_name" name="vinculo_name" value="">
                                </div>
                            </div>    
                        </div>
                        <div id="tel_contatos" class="form-group col-lg-12" style="display: none;">
                            <div class="col-lg-2">
                                <label>Telefone</label>
                            </div>    
                            <div class="col-lg-4">
                                <input type="text" name="telefone_solicitante" id="telefone_solicitante" class="form-control" placeholder="Digite aqui o seu telefone">
                            </div> 
                            <div class="col-lg-1">
                                <label>Celular</label>
                            </div>    
                            <div class="col-lg-4">
                                <input type="text" name="celular_solicitante" id="celular_solicitante" class="form-control" placeholder="Digite aqui o seu celular">
                            </div>
                        </div>
                        <div id="btn_prosseguir" class="form-group col-lg-12" style="display: none;">
                            <div class="col-lg-2"></div>
                            <div class="col-lg-5">
                                <button class="btn btn-md btn-primary uppercase" type="submit" name="dados_relato" id="dados_relato" value="">Prosseguir</button>
                            </div> 
                        </div>    
                    </form>
                </div>

            </div>

            <div class="row" style="height: 100px;"></div>
        </div>

    </div>
</div>
<?php include(kohana::find_file('views/templates/scc', 'footer', 'php')) ?>

<script>
    jQuery(document).ready(function ()
    {
        // Função para habilitar os campos sim/nao, "Você deseja se identificar?" 
        $('#sim').click(function () {
            $('#nome_solicitante_grupo').show();
            $('#email_solicitante_grupo').show();
            $('#vinculo_solicitante_grupo').show();
            $('#cargo_solicitante_grupo').show();
            $('#tel_contatos').show();
            $('#btn_prosseguir').show();
            $('#cargo_solicitante').attr("required", "true");
        })

        $('#nao').click(function () {
            $('#nome_solicitante_grupo').hide();
            $('#email_solicitante_grupo').show();
            $('#vinculo_solicitante_grupo').show();
            $('#cargo_solicitante_grupo').hide();
            $('#tel_contatos').hide();
            $('#btn_prosseguir').show();
            $('#cargo_solicitante').removeAttr("required");
        })

        $("#vinculo_solicitante").change(function () {
            var vinculon = $(this).find("option:selected").text();
            $("#vinculo_name").val(vinculon);
        });

    });
</script>

</body>
</html>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?= SISTEMA; ?> - Canal Confidencial</title>
        <?php include(kohana::find_file('views/templates/scc', 'init', 'php')) ?>
        <script>
            jQuery(document).ready(function ()
            {
                // Remove seleção de ativo no menu.
                $('.navbar-nav li').removeClass('active');
                $('#menu-superior li').removeClass('active');
                // Ativa botão no menu.
                $('.navbar-nav #mn_relato').addClass('active');
            });
        </script>
    </head>
    <body>
        <div class="content">
            <?php include(kohana::find_file('views/templates/scc', 'header', 'php')) ?>
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/scc', 'menu', 'php')) ?>
            <!-- Fim do Menu -->

            <div class="container" >
                <div class="row">
                    <div class="col-lg-12">
                        <h4 class="bold uppercase azul">Realizar relato</h4>
                        <hr style="width: 98%;">
                    </div>
                </div>            
                <div class="row col-lg-12">
                    <p>As informações aqui registradas serão recebidas por um comitê, assegurando sigilo absoluto e o tratamento adequado de cada situação pela alta administração da <strong>SPDM</strong>, sem conflitos de interesses.</p>
                    <p>A veracidade das informações providas é uma responsabilidade do relator. </p>
                    <p>Todas as informações serão verificadas durante o processo de averiguação, e as ações decorrentes serão tomadas a critério exclusivo da <strong>SPDM</strong>.</p>
                </div>
                <div class="row col-lg-12" style="margin-left: 0px; margin-top: 20px;">
                    <a href="<?= URL::base(); ?>canalconfidencial/solicitacao/abrir_relato">
                        <button class="btn btn-md btn-primary uppercase" type="submit" name="abrir_relato" id="abrir_relato" value="">Concordo com os termos</button>&nbsp;&nbsp;
                    </a>
                    <a href="<?= URL::base(); ?>canalconfidencial/solicitacao/nao_concordo">
                        <button class="btn btn-md btn-danger uppercase" type="submit" name="nao-concorda" id="nao-concorda" value="">Não concordo com os termos</button>
                    </a>
                </div>
                <div class="pager" style="height: 180px;"></div>
            </div>

        </div>
    </div>
    <?php include(kohana::find_file('views/templates/scc', 'footer', 'php')) ?>

    <script>

        jQuery(document).ready(function ()
        {
            // Verifica se tem o certificado
            $("body").on("click", "button[name=btnValidacao]", function ()
            {
                var autenticacao = $('#autenticacao').val();
                $('button#btnValidacao').text("Processando");
                $.ajax("gerapdf",
                        {
                            type: "get",
                            data:
                                    {
                                        autenticacao: autenticacao
                                    },
                            success: function () {
                                $('#sucesso').addClass('visible-lg');
                                $('#sucesso').removeClass('hidden');

                                var counter = 5;
                                setInterval(function () {
                                    counter--;
                                    if (counter >= 0) {
                                        span = document.getElementById("count");
                                        span.innerHTML = counter;
                                    }
                                    // Display 'counter' wherever you want to display it.
                                    if (counter === 0) {
                                        window.open('gerapdf?autenticacao=' + autenticacao, '_blank');
                                        window.location.replace("home");
                                        clearInterval(counter);
                                    }

                                }, 1000);
                                $('button#btnValidacao').text("Validar");

                            },
                            error: function (data) {
                                $('button#btnValidacao').text("Validar");
                                $('#erro').addClass('visible-lg');
                                $('#erro').removeClass('hidden');
                                var counter = 5;
                                setInterval(function () {
                                    counter--;
                                    if (counter >= 0) {
                                        span = document.getElementById("counterro");
                                        span.innerHTML = counter;
                                    }
                                    if (counter === 0) {
                                        window.location.replace("home");
                                        clearInterval(counter);
                                    }

                                }, 1000);
                            }
                        });
            });
        });

    </script>

</body>
</html>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?= SISTEMA; ?> - Canal Confidencial</title>
        <?php include(kohana::find_file('views/templates/scc', 'init', 'php')) ?>
        <script>
            jQuery(document).ready(function ()
            {
                // Remove seleção de ativo no menu.
                $('.navbar-nav li').removeClass('active');
                $('#menu-superior li').removeClass('active');
                // Ativa botão no menu.
                $('.navbar-nav #mn_relato').addClass('active');
            });
        </script>
    </head>
    <body>
        <div class="content">
            <?php include(kohana::find_file('views/templates/scc', 'header', 'php')) ?>
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/scc', 'menu', 'php')) ?>
            <!-- Fim do Menu -->

            <div class="container" >
                <div class="row">
                    <div class="col-lg-12">
                        <h4 class="bold uppercase azul">Descreva o relato</h4>
                        <hr style="width: 97%;">
                    </div>
                </div>                
                <div class="row container">
                    <p>Por favor, descreva a situação que o motiva a procurar este canal. É importante que seu relato seja completo e detalhado.</p>                   
                    <p>Para acompanhar o andamento de seu relato, você receberá um número de protocolo que lhe será fornecido após o registro do relato.</p>
                    <p>Agradecemos sua iniciativa e confiança.</p>
                </div>

                <div class="row container" style="margin-top: 15px; margin-bottom: 15px;">
                    <form class="form-horizontal" role="form" method="POST" action="gravar_relato" enctype="multipart/form-data">

                        <!-- Dados transportados da pagina abrir relato -->
                        <input type="hidden" name="nome_solicitante" id="nome_solicitante" class="form-control" value="<?= $vars['nome_solicitante']; ?>">
                        <input type="hidden" name="email_solicitante" id="email_solicitante" class="form-control" value="<?= $vars['email_solicitante']; ?>">
                        <input type="hidden" name="cargo_solicitante" id="cargo_solicitante" class="form-control" value="<?= $vars['cargo_solicitante']; ?>">
                        <input type="hidden" name="vinculo_solicitante" id="vinculo_solicitante" class="form-control" value="<?= $vars['vinculo_solicitante']; ?>">
                        <input type="hidden" name="telefone_solicitante" id="telefone_solicitante" class="form-control" value="<?= $vars['telefone_solicitante']; ?>">
                        <input type="hidden" name="celular_solicitante" id="celular_solicitante" class="form-control" value="<?= $vars['celular_solicitante']; ?>">
                        <input type="hidden" name="vinculo_name" id="vinculo_name" class="form-control" value="<?= $vars['vinculo_name']; ?>">
                        <!-- Dados transportados da pagina abrir relato -->

                        <div class="form-group col-lg-12">

                            <div class="col-lg-2">
                                <label>Tipo do Relato<span id="obrigatorio" style="color: #ff0000;">*</span></label>
                            </div>    
                            <div class="col-lg-9">
                                <select class="form-control" id='tipo_relato' name='tipo_relato' required>
                                    <option value="" selected='selected'>Selecione uma Opção</option>
                                    <?php foreach ($tipos as $tipo) { ?>
                                        <option value="<?= $tipo['idTipoRelato']; ?>"><?= $tipo['Nome_TipoRelato']; ?></option>
                                    <?php } ?>
                                </select>
                                <input type="hidden" id="tipo_name" name="tipo_name" value="">
                            </div>

                        </div>
                        <div class="form-group col-lg-12">
                            <div id="vinculo_solicitante_grupo"  >
                                <div class="col-lg-2">
                                    <label>Unidade do Relato<span id="obrigatorio" style="color: #ff0000;">*</span></label>
                                </div>    
                                <div class="col-lg-9">
                                    <select class="form-control" id='unidade_relato' name='unidade_relato' required>
                                        <option value="" selected='selected'>Selecione uma Opção</option>
                                        <?php foreach ($unidades as $unidade) { ?>
                                            <option value="<?= $unidade['idUnidade']; ?>"><?= $unidade['Nome_Unidade']; ?></option>
                                        <?php } ?>
                                    </select>
                                    <input type="hidden" id="unidade_name" name="unidade_name" value="">
                                </div>
                            </div>    
                        </div>
                        <div class="form-group col-lg-12" id="dsei_grupo" style="display: none;">
                            <div class="col-lg-2">
                                <label>DSEI (Saúde Indígena)</label>
                            </div>    
                            <div class="col-lg-9">
                                <select class="form-control" id='dsei' name='dsei'>
                                    <option value="null" selected='selected'>Selecione uma Opção</option>
                                    <?php foreach ($dseis as $dsei) { ?>
                                        <option value="<?= $dsei['idDsei']; ?>"><?= $dsei['Nome_Dsei']; ?></option>
                                    <?php } ?>
                                </select>
                                <input type="hidden" id="dsei_name" name="dsei_name" value="">
                            </div>   
                        </div>
                        <div class="form-group col-lg-12" >
                            <div class="col-lg-2">
                                <label>Quando</label>
                            </div>    
                            <div class="col-lg-4">
                                <input type="date" name="quando_relato" id="quando_relato" class="form-control" placeholder="Data em que aconteceu a situação">
                            </div> 
                            <div class="col-lg-1">
                                <label>Onde</label>
                            </div>    
                            <div class="col-lg-4">
                                <input type="text" name="onde_relato" id="onde_relato" class="form-control" placeholder="Onde aconteceu a situação">
                            </div>
                        </div>
                        <div class="form-group col-lg-12" >
                            <div class="col-lg-2">
                                <label>Nome dos Envolvidos</label>
                            </div>    
                            <div class="col-lg-9">
                                <input type="text" name="nome_envolvidos" id="nome_envolvidos" class="form-control" placeholder="Quem (nome das pessoas envolvidas, inclusive testemunhas)">
                            </div>    
                        </div>
                        <div class="form-group col-lg-12" >
                            <div class="col-lg-2">
                                <label>O quê (descrição da situação)</label>
                            </div>    
                            <div class="col-lg-9">
                                <textarea rows="5" class="form-control" name="descricao_da_situacao" id="descricao_da_situacao" value="" placeholder="" type="textarea"></textarea>  
                            </div>    
                        </div>
                        <div class="form-group col-lg-12">
                            <div class="col-lg-3">
                                <label>Este relato está ligado a outro registro?</label>
                            </div>    
                            <div class="col-lg-2">
                                <label class="leftfalse">
                                    <input type="radio" name="registro_anterior" id="registro_sim" value="1">&nbsp;Sim
                                </label>
                                <label class="leftfalse">
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="registro_anterior" id="registro_nao" value="0">&nbsp;Não
                                </label>
                            </div>
                        </div>
                        <div id="numero_do_protocolo_anterior_grupo" class="form-group col-lg-12" style="display: none;">
                            <div class="col-lg-2">
                                <label>Informe o número</label>
                            </div>    
                            <div class="col-lg-9">
                                <input type="text" name="numero_do_protocolo_anterior" id="numero_do_protocolo_anterior" class="form-control" placeholder="Digite o número de protocolo anterior">  
                            </div>    
                        </div>

                        <div class="form-group col-lg-12" >
                            <div class="col-lg-2"></div>
                            <div class="col-lg-9">
                                <label class="leftfalse">Se você quiser anexar arquivos como fotos e documentos, adicione-os aqui. O tamanho máximo dos arquivos é de 30 MB.</label>
                            </div>
                            <div class="col-lg-2"></div>
                            <div class="col-lg-4">
                                <input type="file" class="btn btn-default" id="anexo" name="anexo[]" multiple="" onchange="makeFileList();">
                            </div>
                            <div class="col-lg-5">
                                <ol id="fileList" style="margin-top: 10px; margin-bottom: 10px;"></ol>

                            </div>    
                        </div>                        
                        <div class="form-group col-lg-12">
                            <div class="col-lg-2"></div>
                            <div class="col-lg-5">
                                <button class="btn btn-md btn-primary uppercase" type="submit" name="submit" id="submit" value="">Enviar</button>
                                <button class="btn btn-md btn-info uppercase" type="reset" name="reset" id="reset" value="">Limpar</button>
                            </div> 
                        </div>    
                    </form>
                </div>

            </div>

            <div class="row" style="height: 100px;"></div>
        </div>

    </div>
</div>
<?php include(kohana::find_file('views/templates/scc', 'footer', 'php')) ?>

<script>
    jQuery(document).ready(function ()
    {
        // Função para habilitar os campos sim/nao, "Você deseja se identificar?" 
        $('#registro_sim').click(function () {
            $('#numero_do_protocolo_anterior_grupo').show();
        });

        $('#registro_nao').click(function () {
            $('#numero_do_protocolo_anterior_grupo').hide();
        });


        $('#unidade_relato').change(function () {
            var idUnidade = $(this).val();
            if (idUnidade === '2') {
                $('#dsei_grupo').show();
            } else {
                $('#dsei_grupo').hide();
            }
        });

        $("#unidade_relato").change(function () {
            var unidaden = $(this).find("option:selected").text();
            $("#unidade_name").val(unidaden);
        });

        $("#tipo_relato").change(function () {
            var tipon = $(this).find("option:selected").text();
            $("#tipo_name").val(tipon);
        });

        $("#dsei").change(function () {
            var dsein = $(this).find("option:selected").text();
            $("#dsei_name").val(dsein);
        });

    });

    function makeFileList() {
        var input = document.getElementById("anexo");
        var ol = document.getElementById("fileList");
        while (ol.hasChildNodes()) {
            ol.removeChild(ol.firstChild);
        }
        for (var i = 0; i < input.files.length; i++) {
            var li = document.createElement("li");
            li.innerHTML = input.files[i].name;
            ol.appendChild(li);
        }
        if (!ol.hasChildNodes()) {
            var li = document.createElement("li");
            li.innerHTML = 'Nenhum arquivo selecionado';
            ol.appendChild(li);
        }
    }
</script>

</body>
</html>
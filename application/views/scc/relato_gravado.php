<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?= SISTEMA; ?> - Canal Confidencial</title>
        <?php include(kohana::find_file('views/templates/scc', 'init', 'php')) ?>
        <script>
            jQuery(document).ready(function ()
            {
                // Remove seleção de ativo no menu.
                $('.navbar-nav li').removeClass('active');
                $('#menu-superior li').removeClass('active');
                // Ativa botão no menu.
                $('.navbar-nav #mn_relato').addClass('active');
            });
        </script>
    </head>
    <body>
        <div class="content">
            <?php include(kohana::find_file('views/templates/scc', 'header', 'php')) ?>
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/scc', 'menu', 'php')) ?>
            <!-- Fim do Menu -->

            <div class="container" >
                <div class="row">
                    <div class="col-lg-12">
                        <h4 class="bold uppercase azul">Relato Recebido</h4>
                        <hr style="width: 98%;">
                    </div>
                </div>
                <div class="row col-lg-12">
                    <h4>
                        Seu Relato foi recebido com sucesso!
                    </h4>
                    <br>
                    <p style="font-size: 16px; margin-left: 10px;">
                        <strong>Obrigado, nós recebemos a sua mensagem e responderemos o mais breve possível!</strong>
                        <br><br>
                        Caso você tenha informado um e-mail, receberá uma cópia de sua solicitação em sua caixa postal.
                    </p>
                    <p style="font-size: 16px; margin-left: 10px;">Seu numero de protocolo é: <strong style="color: #ff0000;"><?= $Protocolo; ?></strong></p>

                </div>


                <div class="pager" style="height: 180px;"></div>
            </div>

        </div>
    </div>
    <?php include(kohana::find_file('views/templates/scc', 'footer', 'php')) ?>

    <script>

        jQuery(document).ready(function ()
        {
            // Verifica se tem o certificado
            $("body").on("click", "button[name=btnValidacao]", function ()
            {
                var autenticacao = $('#autenticacao').val();
                $('button#btnValidacao').text("Processando");
                $.ajax("gerapdf",
                        {
                            type: "get",
                            data:
                                    {
                                        autenticacao: autenticacao
                                    },
                            success: function () {
                                $('#sucesso').addClass('visible-lg');
                                $('#sucesso').removeClass('hidden');

                                var counter = 5;
                                setInterval(function () {
                                    counter--;
                                    if (counter >= 0) {
                                        span = document.getElementById("count");
                                        span.innerHTML = counter;
                                    }
                                    // Display 'counter' wherever you want to display it.
                                    if (counter === 0) {
                                        window.open('gerapdf?autenticacao=' + autenticacao, '_blank');
                                        window.location.replace("home");
                                        clearInterval(counter);
                                    }

                                }, 1000);
                                $('button#btnValidacao').text("Validar");

                            },
                            error: function (data) {
                                $('button#btnValidacao').text("Validar");
                                $('#erro').addClass('visible-lg');
                                $('#erro').removeClass('hidden');
                                var counter = 5;
                                setInterval(function () {
                                    counter--;
                                    if (counter >= 0) {
                                        span = document.getElementById("counterro");
                                        span.innerHTML = counter;
                                    }
                                    if (counter === 0) {
                                        window.location.replace("home");
                                        clearInterval(counter);
                                    }

                                }, 1000);
                            }
                        });
            });
        });

    </script>

</body>
</html>
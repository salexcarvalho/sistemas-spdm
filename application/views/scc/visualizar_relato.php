<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?= SISTEMA; ?> - Canal Confidencial</title>
        <?php include(kohana::find_file('views/templates/scc', 'init', 'php')) ?>
        <script>
            jQuery(document).ready(function ()
            {
                // Remove seleção de ativo no menu.
                $('.navbar-nav li').removeClass('active');
                $('#menu-superior li').removeClass('active');
                // Ativa botão no menu.
                $('.navbar-nav #mn_acompanhar').addClass('active');
            });
        </script>

        <style>
            .table {
                margin-bottom: -10px;
                margin-top: -5px;
            }
            .table > thead > tr > th,
            .table > tbody > tr > th,
            .table > tfoot > tr > th,
            .table > thead > tr > td,
            .table > tbody > tr > td,
            .table > tfoot > tr > td {
                padding: 4px;
                padding-bottom: 4px;
                line-height: 1.42857143;
                vertical-align: bottom;
                border-bottom: 1px solid #ddd;
                border-top: 0px solid #ddd;
            }
            label {
                float: left;
                padding-top: 0px;
                margin-right: 5px;
            }
        </style>
    </head>
    <body>
        <div class="content">
            <?php include(kohana::find_file('views/templates/scc', 'header', 'php')) ?>
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/scc', 'menu', 'php')) ?>
            <!-- Fim do Menu -->

            <div class="container" >
                <div class="row">
                    <div class="col-lg-12">
                        <h4 class="bold uppercase azul">Visualizar Dados do Relato - <span id="obrigatorio" style="color: #ff0000;">ID de Protocolo: <?= $idProtocolo; ?></span></h4>
                        <hr style="width: 98%;">
                        <?php if (isset($warning)) : ?>
                            <div id="msg" class="alert alert-success alert-dismissible" role="alert" style="width:98%;">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong><?= $warning; ?></strong>
                            </div>
                        <?php endif ?>
                    </div>
                </div>            
                <div class="row" style="margin-right:20px;">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <span><strong>SOLICITANTE</strong></span>
                                <div class="pull-right">
                                    <?php if ($relato[0]['Atendimento'] == "4") { ?>
                                        <span>Status do Atendimento:&nbsp; </span><span class="btn btn-xs btn-danger" style="width: 90px;">Encerrado</span>
                                    <?php } elseif ($relato[0]['Atendimento'] == "2") { ?>
                                        <span>Status do Atendimento:&nbsp; </span><span class="btn btn-xs btn-info" style="width: 90px;">Processando</span>
                                    <?php } else if ($relato[0]['Atendimento'] == "3") { ?>
                                        <span>Status do Atendimento:&nbsp; </span><span class="btn btn-xs btn-success" style="width: 90px;">Concluído</span>
                                    <?php } else { ?>
                                        <span>Status do Atendimento:&nbsp; </span><span class="btn btn-xs btn-warning" style="width: 90px;">Aberto</span>
                                    <?php } ?>
                                </div>    
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <table border="0" class="table table-condensed">
                                        <tbody>
                                            <tr>
                                                <td><label class="control-label">Nome:</label> <?php if ($relato[0]['Nome_Solicitante'] != "") { ?><?= $relato[0]['Nome_Solicitante']; ?><?php } else { ?><span style="color:#f00;">Anônimo</span><?php } ?></td>
                                                <td><label class="control-label">Email:</label> <?php if ($relato[0]['Email_Solicitante'] != "") { ?><?= $relato[0]['Email_Solicitante']; ?><?php } else { ?><span style="color:#f00;">Anônimo</span><?php } ?></td>
                                                <td><label class="control-label">Cargo:</label> <?= $relato[0]['Cargo_Solicitante']; ?></td>

                                            </tr>
                                            <tr>
                                                <td><label class="control-label">Telefone:</label> <?= $relato[0]['Tel_Solicitante']; ?></td>
                                                <td><label class="control-label">Celular:</label> <?= $relato[0]['Cel_Solicitante']; ?></td>
                                                <td><label class="control-label">Data da Solitação:</label> <?= strftime('%d/%m/%Y', strtotime($relato[0]['dtCadastro'])); ?></td>
                                            </tr>
                                            <tr>
                                                <td><label class="control-label">Vínculo do RH:</label> <?= $relato[0]['Nome_Vinculo']; ?></td>
                                                <td colspan="2"><label class="control-label">Unidade que ocorreu o Relato:</label> <?= $relato[0]['Nome_Unidade']; ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="panel-heading" style="border-top: 1px solid; border-top-color: #ddd; border-top-left-radius: 0px; border-top-right-radius: 0px; ">
                                <span><strong>RELATO</strong></span>   
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <table border="0" class="table table-condensed">
                                        <tbody>
                                            <tr>
                                                <td width="18%"><label class="control-label">Quando ocorreu:</label> <?= strftime('%d/%m/%Y', strtotime($relato[0]['dtQuando'])); ?></td>
                                                <td width="38%"><label class="control-label">Local do Relato:</label> <?= $relato[0]['Onde']; ?></td>

                                            </tr>
                                            <tr>
                                                <td><label class="control-label">DSEI:</label> <?= $relato[0]['Nome_Dsei']; ?></td>
                                                <td><label class="control-label">Número do Protocolo Anterior:</label> <?= $relato[0]['Numero_Protocolo_Anterior']; ?></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2"><label class="control-label">Nome dos envolvidos:</label> <?= $relato[0]['Nome_Envolvidos']; ?></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <h5><strong>Descrição do Relato:</strong></h5>
                                                    <p><?= $relato[0]['Descricao_Relato']; ?></p></td>
                                            </tr>
                                            <?php if (!empty($anexo[0])) { ?>
                                                <tr>
                                                    <td colspan="3">
                                                        <h5><strong>Anexos:</strong></h5>
                                                        <?php
                                                        $i = 1;
                                                        foreach ($anexo as $anexos) {
                                                            ?>
                                                            <?php if ($anexos['idResposta'] == null or $anexos['idResposta'] == "") { ?>
                                                                <a href="<?= URL::base(); ?>upload/scc/<?= $anexos['Caminho']; ?>" class="btn btn-default btn-md" style="width: 120px; margin-right: 0px; margin-bottom: 10px;" target="_blank" download>ANEXO_<?= $i; ?></a>
                                                            <?php } ?>   
                                                            <?php
                                                            $i++;
                                                        }
                                                        ?>
                                                    </td>
                                                </tr>
<?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>


                        <?php
                        $numresp = 1;
                        foreach ($resposta as $respostas) {
                            ?>
                            <div class="panel panel-<?php if ($respostas['Envio'] == 'reencaminhar_rh') { ?>info<?php } else if ($respostas['Envio'] == 'resposta_usuario') { ?>warning<?php } else { ?>danger<?php } ?>">
                                <div class="panel-heading">
                                    <span><strong><?php if ($respostas['Envio'] == 'reencaminhar_rh') { ?> Encaminhado pelo RH <?php } else if ($respostas['Envio'] == 'resposta_usuario') { ?> Resposta do Solicitante <?php } else { ?> Resposta do RH <?php } ?> - <span style="color:#f00;">Interação nº <?= $numresp; ?></span></strong></span>
                                </div>
                                <div class="panel-body">
                                    <div class="form-group" >
                                        <table border="0" class="table table-condensed">
                                            <tbody>
                                                <tr>
                                                    <?php if ($respostas['Envio'] == 'reencaminhar_rh') { ?>
                                                        <td><label class="control-label">Encaminhado em:</label> <?= strftime('%d/%m/%Y', strtotime($respostas['dtResposta'])); ?></td>
                                                        <td><label class="control-label">Encaminhado por:</label> <?= $respostas['Nome_Usuario']; ?></td>
                                                    <?php } else { ?>
                                                        <td><label class="control-label">Respondido em:</label> <?= strftime('%d/%m/%Y', strtotime($respostas['dtResposta'])); ?></td>
                                                        <td><label class="control-label">Respondido por:</label> <?php if ($respostas['Nome_Usuario'] != '') { ?><?= $respostas['Nome_Usuario']; ?> <?php } else { ?> <?= $relato[0]['Nome_Solicitante']; ?> <?php } ?></td>
    <?php } ?>                                                                
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <h5><strong>Descrição do Resposta:</strong></h5>
                                                        <p><?= $respostas['Resposta']; ?></p></td>
                                                </tr>

                                                <?php
                                                $model_relatos = new Model_Scc_Relato('default');
                                                $anexor = $model_relatos->select_anexos($relato[0]['idProtocolo'], $respostas['idResposta']);

                                                if ($anexor[0] != "") {
                                                    ?>

                                                    <tr>
                                                        <td colspan="3">
                                                            <label class="control-label">Anexos:</label>
                                                            <?php
                                                            $i = 1;
                                                            foreach ($anexor as $anexosr) {
                                                                ?>
                                                                <a href="<?= URL::base(); ?>upload/scc/<?= $anexosr['Caminho']; ?>" class="btn btn-default btn-md" style="width: 120px; margin-right: 0px; margin-bottom: 10px;" target="_blank" download>ANEXO_ADM_<?= $i; ?></a>
                                                        <?php
                                                        $i++;
                                                    }
                                                    ?>
                                                        </td>
                                                    </tr>
    <?php } ?>

                                            </tbody>
                                        </table>   
                                    </div>


                                </div>
                            </div>
    <?php
    $numresp++;
}
?>
<?php if ($relato[0]['Atendimento'] != "4") { ?>
                            <form class="form-horizontal" role="form" method="POST" action="responder_relato" enctype="multipart/form-data">
                                <input type="hidden" id="idRelato" name="idRelato" value="<?= $relato[0]['idRelato']; ?>">
                                <input type="hidden" id="idProtocolo" name="idProtocolo" value="<?= $relato[0]['idProtocolo']; ?>">
                                <input type="hidden" id="idSolicitante" name="idSolicitante" value="<?= $relato[0]['idSolicitante']; ?>">
                                <input type="hidden" id="email_solicitante" name="email_solicitante" value="<?= $relato[0]['Email_Solicitante']; ?>">
                                <input type="hidden" name="nome_solicitante" id="nome_solicitante" class="form-control" value="<?= $relato[0]['Nome_Solicitante']; ?>">
                                <input type="hidden" id="vinculo_solicitante" name="vinculo_solicitante" value="<?= $relato[0]['idVinculo']; ?>">

                                <div class="panel panel-default" id="responderorelato">
                                    <div class="panel-heading">
                                        <span><strong>INTERAGIR COM RELATO</strong></span>
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-group" >
                                            <div class="col-lg-2">
                                                <label>Informe a sua Interação</label>
                                            </div>    
                                            <div class="col-lg-9">
                                                <textarea rows="6" class="form-control" name="resposta_usuario" id="resposta_usuario" value="" placeholder="" type="textarea"></textarea>  
                                            </div>    
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" id="acaobtn">
                                    <div class="col-lg-10">
                                        <button type="submit" class="btn btn-primary">Salvar e Gravar</button>
                                    </div>
                                </div>
                            </form>
<?php } ?>
                    </div>
                </div>

                <div class="pager" style="height: 180px;"></div>
            </div>

        </div>
    </div>
<?php include(kohana::find_file('views/templates/scc', 'footer', 'php')) ?>



</body>
</html>
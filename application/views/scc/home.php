<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?= SISTEMA; ?> - Canal Confidencial</title>
        <?php include(kohana::find_file('views/templates/scc', 'init', 'php')) ?>
        <script>
            jQuery(document).ready(function ()
            {
                // Remove seleção de ativo no menu.
                $('.navbar-nav li').removeClass('active');
                $('#menu-superior li').removeClass('active');
                // Ativa botão no menu.
                $('.navbar-nav #mn_home').addClass('active');
            });
        </script>
    </head>
    <body>
        <div class="content">
            <?php include(kohana::find_file('views/templates/scc', 'header', 'php')) ?>
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/scc', 'menu', 'php')) ?>
            <!-- Fim do Menu -->

            <div class="container" >
                <div class="row">
                    <div class="col-lg-12">
                        <h4 class="bold uppercase azul">Página Inicial</h4>
                        <hr style="width: 97%;">
                    </div>
                </div>
                <div class="row container">
                    <p>Este é um canal exclusivo da <strong>SPDM - Associação Paulista para o Desenvolvimento da Medicina</strong> para comunicação segura e, se desejada, anônima de condutas que violem o <strong>Manual de Funcionários</strong> ou a legislação vigente.</p>
                    <p>Se desejar, seu relato pode ser feito também pelo e-mail: <span style="color: #ff0000;">combateaoassedio@spdm.org.br</span>.</p>
                    <p>Para esclarecimentos sobre nossos serviços, elogios, sugestões ou reclamações, por favor acesse: <a href="https://www.spdm.org.br/contato" target="_blank">https://www.spdm.org.br/contato</a></p>
                </div>
                <div class="row col-lg-12">
                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" class="">O que é Assédio Moral</a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in" aria-expanded="true" style="">
                                <div class="panel-body">
                                    <p>É a exposição de um ou mais empregados a situações humilhantes, constrangedoras, em más condições de trabalho; ridicularizando, inferiorizando e ofendendo de forma constante e prolongada. Tais situações tem o intuito de desestabilizar emocionalmente e fisicamente esses empregados, podendo ocasionar danos a sua saúde, bem como ao ambiente de trabalho.</p>
                                    <p>A vítima escolhida é isolada do grupo sem explicações, passando a ser hostilizada, ridicularizada, inferiorizada, culpabilizada e desacreditada diante dos pares. Estes, por medo do desemprego e a vergonha de serem também humilhados, associados ao estímulo constante à competitividade, rompem os laços afetivos com a vítima e, frequentemente, reproduzem e reatualizam ações e atos do agressor no ambiente de trabalho, instaurando o pacto da tolerância e do silêncio no coletivo, enquanto a vitima vai gradativamente se desestabilizando e fragilizando, perdendo sua autoestima.</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="collapsed" aria-expanded="false">Objetivo do Assédio Moral</a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <p>- Desestabilizar emocionalmente e profissionalmente o empregado;<br>
                                        - Forçar sua transferência a outro setor/filial;<br>
                                        - Forçar o empregado a pedir demissão;<br>
                                        - Sujeitar o empregado a conviver com tal violência passivamente, por medo do desemprego.</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" class="collapsed" aria-expanded="false">O Que Não é Assédio Moral?</a>
                                </h4>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <p>É importante diferenciar entre o que é assédio moral e o que não é.</p>
                                    <p>Não é considerado assédio:</p>
                                    <p>- Conflitos de idéias, opiniões, interesses, quando há igualdade entre os debatedores;<br>
                                        - Estresse profissional provocado por eventuais picos de trabalho;<br>
                                        - Más condições de trabalho, excetuando-se quando forem direcionadas a um único trabalhador;<br>
                                        - Mudanças ou transferência de função, desde que não tenham caráter punitivo ou configurem perseguição;<br>
                                        - Críticas ou avaliações sobre o trabalho executado desde que sejam fundamentadas e comunicadas de forma construtiva e respeitosa;<br>
                                        - Exigência de produtividade, dentro dos parâmetros da razoabilidade;<br>
                                        - Controle administrativo dos chefes sobre os empregados, desde que este poder disciplinar do superior hierárquico seja exercido de maneira adequada.</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour" class="collapsed" aria-expanded="false">Assita o vídeo explicativo sobre "Assédio Moral no Ambiente de Trabalho"</a>
                                </h4>
                            </div>
                            <div id="collapseFour" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                    <iframe width="100%" height="604" src="https://www.youtube.com/embed/DerFxhn12Kw" frameborder="0" allowfullscreen></iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row container" style="margin-left: 0px; margin-top: 20px;">
                    <a href="<?= URL::base(); ?>canalconfidencial/solicitacao">
                        <button class="btn btn-md btn-primary uppercase" type="submit" name="realizar_relato" id="realizar_relato" value="">Realizar Relato</button>&nbsp;&nbsp;
                    </a>
                    <a href="<?= URL::base(); ?>canalconfidencial/solicitacao/acompanhar_relato">
                        <button class="btn btn-md btn-danger uppercase" type="submit" name="acompanhar_relato" id="acompanhar_relato" value="">Acompanhar Relato</button>
                    </a>
                </div>
                <div class="pager" style="height: 20px;"></div>
            </div>

        </div>
    </div>
    <?php include(kohana::find_file('views/templates/scc', 'footer', 'php')) ?>

    <script>

        jQuery(document).ready(function ()
        {
            // Verifica se tem o certificado
            $("body").on("click", "button[name=btnValidacao]", function ()
            {
                var autenticacao = $('#autenticacao').val();
                $('button#btnValidacao').text("Processando");
                $.ajax("gerapdf",
                        {
                            type: "get",
                            data:
                                    {
                                        autenticacao: autenticacao
                                    },
                            success: function () {
                                $('#sucesso').addClass('visible-lg');
                                $('#sucesso').removeClass('hidden');

                                var counter = 5;
                                setInterval(function () {
                                    counter--;
                                    if (counter >= 0) {
                                        span = document.getElementById("count");
                                        span.innerHTML = counter;
                                    }
                                    // Display 'counter' wherever you want to display it.
                                    if (counter === 0) {
                                        window.open('gerapdf?autenticacao=' + autenticacao, '_blank');
                                        window.location.replace("home");
                                        clearInterval(counter);
                                    }

                                }, 1000);
                                $('button#btnValidacao').text("Validar");

                            },
                            error: function (data) {
                                $('button#btnValidacao').text("Validar");
                                $('#erro').addClass('visible-lg');
                                $('#erro').removeClass('hidden');
                                var counter = 5;
                                setInterval(function () {
                                    counter--;
                                    if (counter >= 0) {
                                        span = document.getElementById("counterro");
                                        span.innerHTML = counter;
                                    }
                                    if (counter === 0) {
                                        window.location.replace("home");
                                        clearInterval(counter);
                                    }

                                }, 1000);
                            }
                        });
            });
        });

    </script>

</body>
</html>
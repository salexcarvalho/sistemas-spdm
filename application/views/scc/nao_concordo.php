<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?= SISTEMA; ?> - Canal Confidencial</title>
        <?php include(kohana::find_file('views/templates/scc', 'init', 'php')) ?>
        <script>
            jQuery(document).ready(function ()
            {
                // Remove seleção de ativo no menu.
                $('.navbar-nav li').removeClass('active');
                $('#menu-superior li').removeClass('active');
                // Ativa botão no menu.
                $('.navbar-nav #mn_relato').addClass('active');
            });
        </script>
    </head>
    <body>
        <div class="content">
            <?php include(kohana::find_file('views/templates/scc', 'header', 'php')) ?>
            <!-- Inicio do Menu -->
            <?php include(kohana::find_file('views/templates/scc', 'menu', 'php')) ?>
            <!-- Fim do Menu -->

            <div class="container" >
                <div class="row">
                    <div class="col-lg-12">
                        <h4 class="bold uppercase azul">Termo Rejeitado</h4>
                        <hr style="width: 98%;">
                    </div>
                </div>
                <div class="row col-lg-12">
                    <p>Agradecemos seu contato, mas como você não concorda com os termos, não poderemos prosseguir.</p>
                </div>
                <div class="row col-lg-12" style="margin-left: 0px; margin-top: 20px;">
                    <a href="https://www.spdm.org.br/">
                        <button class="btn btn-md btn-warning uppercase" type="submit" name="site" id="site" value="">Voltar ao Portal da SPDM</button>&nbsp;&nbsp;
                    </a>
                    <a href="<?= URL::base(); ?>canalconfidencial/solicitacao/abrir_relato">
                        <button class="btn btn-md btn-primary uppercase" type="submit" name="abrir_relato" id="abrir_relato" value="">Concordo com os termos e desejo preencher a solicitação.</button>
                    </a>
                </div>
                <div class="pager" style="height: 180px;"></div>
            </div>

        </div>
    </div>
    <?php include(kohana::find_file('views/templates/scc', 'footer', 'php')) ?>

    <script>

        jQuery(document).ready(function ()
        {
            // Verifica se tem o certificado
            $("body").on("click", "button[name=btnValidacao]", function ()
            {
                var autenticacao = $('#autenticacao').val();
                $('button#btnValidacao').text("Processando");
                $.ajax("gerapdf",
                        {
                            type: "get",
                            data:
                                    {
                                        autenticacao: autenticacao
                                    },
                            success: function () {
                                $('#sucesso').addClass('visible-lg');
                                $('#sucesso').removeClass('hidden');

                                var counter = 5;
                                setInterval(function () {
                                    counter--;
                                    if (counter >= 0) {
                                        span = document.getElementById("count");
                                        span.innerHTML = counter;
                                    }
                                    // Display 'counter' wherever you want to display it.
                                    if (counter === 0) {
                                        window.open('gerapdf?autenticacao=' + autenticacao, '_blank');
                                        window.location.replace("home");
                                        clearInterval(counter);
                                    }

                                }, 1000);
                                $('button#btnValidacao').text("Validar");

                            },
                            error: function (data) {
                                $('button#btnValidacao').text("Validar");
                                $('#erro').addClass('visible-lg');
                                $('#erro').removeClass('hidden');
                                var counter = 5;
                                setInterval(function () {
                                    counter--;
                                    if (counter >= 0) {
                                        span = document.getElementById("counterro");
                                        span.innerHTML = counter;
                                    }
                                    if (counter === 0) {
                                        window.location.replace("home");
                                        clearInterval(counter);
                                    }

                                }, 1000);
                            }
                        });
            });
        });

    </script>

</body>
</html>
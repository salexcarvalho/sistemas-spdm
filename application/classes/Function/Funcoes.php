<?php

defined('SYSPATH') OR die('No direct script access.');

class Function_Funcoes {

    public static function dump($var) {
        echo "<pre>";
        var_dump($var);
        echo "</pre>";
        exit;
    }
    
    public static function reArrayFiles($file) {
        $file_ary = array();
        $file_count = count($file['name']);
        $file_key = array_keys($file);

        for ($i = 0; $i < $file_count; $i++) {
            foreach ($file_key as $val) {
                $file_ary[$i][$val] = $file[$val][$i];
            }
        }
        return $file_ary;
    }
    
    
    public static function verifica_login() {
        if (Auth::instance()->logged_in() == 0) {            
            $pag = base64_encode(str_replace(URL::base(),'',$_SERVER['REQUEST_URI']));
            Controller_Dashboard_Adm::redirect('dashboard/adm/login?&pag='.$pag);
        }
    }
    
    public static function carrega() {
        
        $model_config = new Model_Login_Configuracoes('default');
        $config = $model_config->select_config();
        
        $_SESSION['company_name'] = $config[0]['company_name'];
        $_SESSION['company_legal_name'] = $config[0]['company_legal_name'];
        $_SESSION['logo_admin'] = $config[0]['logo_admin'];
    }

    public static function rgb() {
        $randomString = md5(time() . rand(0, 999)); //like "d73a6ef90dc6a ..."
        $r = substr($randomString, 0, 2); //1. and 2.
        $g = substr($randomString, 2, 2); //3. and 4.
        $b = substr($randomString, 4, 2); //5. and 6.
        return $r . $g . $b;
    }

    public static function formataTelefone($numero) {
        if (strlen($numero) == 10) {
            $novo = substr_replace($numero, '(', 0, 0);
            $novo = substr_replace($novo, ')', 3, 0);
            $novo = substr_replace($novo, ' ', 4, 0);
            $novo = substr_replace($novo, '-', 9, 0);
        } elseif (strlen($numero) == 11) {
            $novo = substr_replace($numero, '(', 0, 0);
            $novo = substr_replace($novo, ')', 3, 0);
            $novo = substr_replace($novo, ' ', 4, 0);
            $novo = substr_replace($novo, '-', 10, 0);
        } else {
            $novo = $numero;
        }
        return $novo;
    }

    public static function formataCep($numero) {
        if (strlen($numero) == 8) {
            $novo = substr_replace($numero, '-', 5, 0);
        } else {
            $novo = $numero;
        }
        return $novo;
    }

    public static function convertData($data) {
        $dd = explode(" ", $data);
        if (count($dd)> 1):
            $data = $dd[0];
            $dataAux = explode("/", $data);
            $data = $dataAux[2] . "-" . $dataAux[1] . "-" . $dataAux[0] . " " . $dd[1];
        else:
            $data = $dd[0];
            $dataAux = explode("/", $data);           
            $data = $dataAux[2] . "-" . $dataAux[1] . "-" . $dataAux[0];
        endif;
       return $data;
    }

    public static function convertDataBr($data) {
        $dd = explode(" ", $data);
        $data = $dd[0];
        $dataAux = explode("-", $data);
        $data = $dataAux[2] . "/" . $dataAux[1] . "/" . $dataAux[0];
        return $data;
    }
    public static function mes($data) {
        $m = date('m', strtotime($data));
        switch ($m){
          case '01':
              $mes = "Janeiro";
              break;
          case '02':
              $mes = "Janeiro";
              break;
          case '03':
              $mes = "Março";
              break;
          case '04':
              $mes = "Abril";
              break;
          case '05':
              $mes = "Maio";
              break;
          case '06':
              $mes = "Junho";
              break;
          case '07':
              $mes = "Julho";
              break;
          case '08':
              $mes = "Agosto";
              break;
          case '09':
              $mes = "Setembro";
              break;
          case '10':
              $mes = "Outubro";
              break;
          case '11':
              $mes = "Novembro";
              break;
          case '12':
             $mes = "Dezembro";
              break;
        }
        return $mes;
    }
    public static function ReservaRelat($data1, $data2){
        $dd1 = explode(" ", $data1);
        $data1 = $dd1[0];
        $dataAux = explode("-", $data1);
        $data1 = $dataAux[2] . "/" . $dataAux[1] . "/" . $dataAux[0];
        $hora1 = explode(":",$dd1[1]);
        $haux = $hora1[0].":".$hora1[1];
        $hora1 = $haux;
        
        $dd2 = explode(" ", $data2);
        $data2 = $dd2[0];
        $dataAux = explode("-", $data2);
        $data2 = $dataAux[2] . "/" . $dataAux[1] . "/" . $dataAux[0];
        $hora2 = explode(":",$dd2[1]);
        $haux = $hora2[0].":".$hora2[1];
        $hora2 = $haux;
        
        if($data1 == $data2){
            $resp = "Evento ocorrerá no dia ".$data1." das ".$hora1." as ".$hora2;
        }else{
            $resp = "Evento ocorrerá no periodo de ".$data1." a ".$data2." das ".$hora1." as ".$hora2;
        }
        
        return $resp; 
    }
    public static function Reservar($idReserva) {
        
        $idReserva = $idReserva;
        $idMensagem = 3;        
        
        $cc = "";
        $bcc = "";
        $to = '';
        $from = array(''=>'');
           
       
        $model_mensagem = new Model_Msg_Mensagempadrao('default');
        $mensagem = $model_mensagem->select_mensagem($idMensagem);
        
        $model_reserva = new Model_Rsa_Reserva('default');
        $complemento = $model_reserva->select_reservas_relatorio_id($idReserva);
        
        $model_email = new Model_Msg_Email('default');
        $verificar = $model_email->verificar_emails_enviados($idMensagem, $to, date('Y-m-d'));
           if(isset($verificar[0]['idEnviados'])){
                $resp = $model_email->select_emails_enviados_id($verificar[0]['idEnviados']);
                $email = $resp[0]['idEnviados'];
           }else{
                $resp = $model_email->insert_emails_enviados($idMensagem, $to, date('Y-m-d'));
                $email = $resp[0];
           }
           $view = View::factory('adm/msg/montarMensagem')
                ->set('mensagem', $mensagem)
                ->set('complemento', $complemento)
                ->set('email', $email)
                ->set('para',$to)
                ->render(); 
          $this->response->body($view);          
          /* $emailSend = Email::factory()
                   ->to($to)
                   ->from($from)               
                   ->subject($mensagem[0]['assunto'])
                   ->message($view, 'text/html')
                   ->send();
           return $emailSend;*/
        }
    public static function hora($valor){        
        $dd1 = explode(" ", $valor);      
        return $dd1[1];        
    }
    public static function data($valor){        
        $dd1 = explode(" ", $valor);      
        return $dd1[0];        
    }  
    
    public static function gerar_senha($tamanho, $maiusculas = true, $minusculas = true, $numeros = true, $simbolos = true){
      $ma = "ABCDEFGHIJKLMNOPQRSTUVYXWZ"; // $ma contem as letras maiusculas
      $mi = "abcdefghijklmnopqrstuvyxwz"; // $mi contem as letras minusculas
      $nu = "0123456789"; // $nu contem os numeros
      $si = "!@#$%¨&*()_+="; // $si contem os sibolos
      $senha = "";                                  
      if ($maiusculas){
            // se $maiusculas for "true", a variavel $ma é embaralhada e adicionada para a variavel $senha
            $senha .= str_shuffle($ma);
      }
        
        if ($minusculas){
            // se $minusculas for "true", a variavel $mi é embaralhada e adicionada para a variavel $senha
            $senha .= str_shuffle($mi);
        }
        
        if ($numeros){
            // se $numeros for "true", a variavel $nu é embaralhada e adicionada para a variavel $senha
            $senha .= str_shuffle($nu);
        }
        
        if ($simbolos){
            // se $simbolos for "true", a variavel $si é embaralhada e adicionada para a variavel $senha
            $senha .= str_shuffle($si);
        }
        
        // retorna a senha embaralhada com "str_shuffle" com o tamanho definido pela variavel $tamanho
        return substr(str_shuffle($senha),0,$tamanho);
    }

}

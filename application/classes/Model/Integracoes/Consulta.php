<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Integracoes_Consulta extends Model
{

    /**
     * Construtor da classe, já seta a conexao com o banco de dados
     *
     * @param string $con_type
     */
    public function __construct($db = NULL)
    {
        if ($db != NULL)
        {
            // Set the instance or name
            $this->_db = $db;
        }
        elseif (!$this->_db)
        {
            // Use the default name
            $this->_db = Database::$default;
        }

        if (is_string($this->_db))
        {
            // Load the database
            $this->_db = Database::instance($this->_db);
        }
    }
    
    public function select_videos_suasaude()
    {
          $videos = DB::select('rz6s4_k2_items.id', 'rz6s4_k2_items.title', 'rz6s4_k2_items.alias', 'rz6s4_k2_items.catid',
                'rz6s4_k2_items.published', 'rz6s4_k2_items.introtext', 'rz6s4_k2_items.created', 'rz6s4_k2_items.video', 
                 array('rz6s4_k2_categories.name', 'nmCategoria'))
                ->from('rz6s4_k2_items')
                ->join('rz6s4_k2_categories', 'LEFT')
                ->on('rz6s4_k2_categories.id', '=', 'rz6s4_k2_items.catid')  
                ->where('rz6s4_k2_categories.parent', '=', '63')
                ->and_where('rz6s4_k2_items.published', '=', '1')
                ->and_where('rz6s4_k2_categories.id', '!=', '78')  
                ->and_where('rz6s4_k2_categories.id', '!=', '105')
                ->order_by('rz6s4_k2_items.id', 'Desc')  
                ->execute($this->_db);
            return $videos;
    }
    
    public function select_videos_dica()
    {
          $videos = DB::select('rz6s4_k2_items.id', 'rz6s4_k2_items.title', 'rz6s4_k2_items.alias', 'rz6s4_k2_items.catid',
                'rz6s4_k2_items.published', 'rz6s4_k2_items.introtext', 'rz6s4_k2_items.created', 'rz6s4_k2_items.video', 
                 array('rz6s4_k2_categories.name', 'nmCategoria'))
                ->from('rz6s4_k2_items')
                ->join('rz6s4_k2_categories', 'LEFT')
                ->on('rz6s4_k2_categories.id', '=', 'rz6s4_k2_items.catid')  
                ->where('rz6s4_k2_categories.id', '=', '103')
                ->and_where('rz6s4_k2_items.published', '=', '1')
                ->order_by('rz6s4_k2_items.id', 'Desc')  
                ->execute($this->_db);
            return $videos;
    }
    
    public function select_videos_memoria()
    {
          $videos = DB::select('rz6s4_k2_items.id', 'rz6s4_k2_items.title', 'rz6s4_k2_items.alias', 'rz6s4_k2_items.catid',
                'rz6s4_k2_items.published', 'rz6s4_k2_items.introtext', 'rz6s4_k2_items.created', 'rz6s4_k2_items.video', 
                 array('rz6s4_k2_categories.name', 'nmCategoria'))
                ->from('rz6s4_k2_items')
                ->join('rz6s4_k2_categories', 'LEFT')
                ->on('rz6s4_k2_categories.id', '=', 'rz6s4_k2_items.catid')  
                ->where('rz6s4_k2_categories.id', '=', '105')
                ->and_where('rz6s4_k2_items.published', '=', '1')
                ->order_by('rz6s4_k2_items.id', 'Desc')  
                ->execute($this->_db);
            return $videos;
    }
    
    public function select_videos_boaspraticas()
    {
          $videos = DB::select('rz6s4_k2_items.id', 'rz6s4_k2_items.title', 'rz6s4_k2_items.alias', 'rz6s4_k2_items.catid',
                'rz6s4_k2_items.published', 'rz6s4_k2_items.introtext', 'rz6s4_k2_items.created', 'rz6s4_k2_items.video', 
                 array('rz6s4_k2_categories.name', 'nmCategoria'))
                ->from('rz6s4_k2_items')
                ->join('rz6s4_k2_categories', 'LEFT')
                ->on('rz6s4_k2_categories.id', '=', 'rz6s4_k2_items.catid')  
                ->where('rz6s4_k2_categories.id', '=', '78')
                ->and_where('rz6s4_k2_items.published', '=', '1')
                ->order_by('rz6s4_k2_items.id', 'Desc')  
                ->execute($this->_db);
            return $videos;
    }    
    
    public function select_video($id)
    {
          $videos = DB::select('rz6s4_k2_items.id', 'rz6s4_k2_items.title', 'rz6s4_k2_items.alias', 'rz6s4_k2_items.catid',
                'rz6s4_k2_items.published', 'rz6s4_k2_items.introtext', 'rz6s4_k2_items.created', 'rz6s4_k2_items.video')
                ->from('rz6s4_k2_items')
                ->where('rz6s4_k2_items.id', '=', $id)
                ->execute($this->_db);
            return $videos;
    }
    
    public function select_conteudo()
    {
          $videos = DB::select('rz6s4_k2_items.id', 'rz6s4_k2_items.title', 'rz6s4_k2_items.alias', 'rz6s4_k2_items.catid',
                'rz6s4_k2_items.published', 'rz6s4_k2_items.introtext', 'rz6s4_k2_items.created', 'rz6s4_k2_items.modified', 'rz6s4_k2_items.video', 
                 array('rz6s4_k2_categories.name', 'nmCategoria'))
                ->from('rz6s4_k2_items')
                ->join('rz6s4_k2_categories', 'LEFT')
                ->on('rz6s4_k2_categories.id', '=', 'rz6s4_k2_items.catid')  
                ->where('rz6s4_k2_items.published', '=', '1')
                ->or_where('rz6s4_k2_categories.parent', '=', '44')
                ->or_where('rz6s4_k2_categories.parent', '=', '59')
                ->or_where('rz6s4_k2_categories.id', '=', '103')
                ->order_by('rz6s4_k2_items.id', 'Desc')  
                ->limit('25')  
                ->execute($this->_db);
            return $videos;
    }
}


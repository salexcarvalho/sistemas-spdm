<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Login_Perfis extends Model {

    /**
     * Construtor da classe, já seta a conexao com o banco de dados
     *
     * @param string $con_type
     */
    public function __construct($db = NULL) {
        if ($db != NULL) {
            // Set the instance or name
            $this->_db = $db;
        } else {
            // Use the default name
            $this->_db = Database::$default;
        }

        if (is_string($this->_db)) {
            // Load the database
            $this->_db = Database::instance($this->_db);
        }
    }

    
    public function select_perfis() {
        $acoes = DB::select('LOGIN_Perfil.idPerfil', 'LOGIN_Perfil.nmPerfil', 'LOGIN_Perfil.dsPerfil', 'LOGIN_Perfil.ativo')
                ->from('LOGIN_Perfil')
                ->execute($this->_db)
        ;

        return $acoes;
    }

    
    public function select_perfil_nm($nmPerfil) {
        $perfil = DB::select()
                ->from('LOGIN_Perfil')
                ->where('LOGIN_Perfil.nmPerfil', '=', $nmPerfil)
                ->execute($this->_db)
        ;

        return $perfil;
    }
    
    public function select_quantidade_perfis() {
        return $this->select_perfis()->count();
    }
    
    public function select_perfil($idPerfil) {
        $perfil = DB::select()
                ->from('LOGIN_Perfil')
                ->where('LOGIN_Perfil.idPerfil', '=', $idPerfil)
                ->execute($this->_db)
        ;

        return $perfil;
    }
    
    
    public function select_perfil_acao($idPerfil) {
        $perfiltemacao = DB::select('LOGIN_PerfilTemAcao.idPerfil', 'LOGIN_PerfilTemAcao.idAcao', 'LOGIN_PerfilTemAcao.Status',
                array('LOGIN_Acao.idAcao', 'Acao'), array('LOGIN_Acao.dsAcao', 'dsAcao'), array('LOGIN_Acao.cdController', 'cdController'))
                ->from('LOGIN_PerfilTemAcao')
                ->join('LOGIN_Acao', 'LEFT')
                ->on('LOGIN_Acao.idAcao', '=', 'LOGIN_PerfilTemAcao.idAcao')
                ->where('LOGIN_PerfilTemAcao.idPerfil', '=', $idPerfil)
                ->execute($this->_db)
        ;
        //echo "<pre>"; var_dump($perfiltemacao); echo "</pre>"; exit;
        return $perfiltemacao;
    }
    
    
    public function select_perfil_acao_usuario($idPerfil) {
        $perfiltemacao = DB::select('LOGIN_PerfilTemAcao.Status', array('LOGIN_Acao.nmAcao', 'nmAcao'), array('LOGIN_Acao.idAcao', 'idAcao'), 
                array('LOGIN_Acao.idCurto', 'idCurto'))
                ->from('LOGIN_PerfilTemAcao')
                ->join('LOGIN_Acao', 'LEFT')
                ->on('LOGIN_Acao.idAcao', '=', 'LOGIN_PerfilTemAcao.idAcao')
                ->where('LOGIN_PerfilTemAcao.idPerfil', '=', $idPerfil)
                ->execute($this->_db)
        ;
        //echo "<pre>"; var_dump($perfiltemacao); echo "</pre>"; exit;
        return $perfiltemacao;
    }
    
    
    public function select_controller() {
        $controller = DB::select()
                ->from('LOGIN_Controller')
                ->where('LOGIN_Controller.nmController', 'NOT LIKE', '%menu%')                
                ->execute($this->_db)
        ;

        return $controller;
    }
    
    
    public function select_controller_menu() {
        $controller = DB::select()
                ->from('LOGIN_Controller')
                ->where('LOGIN_Controller.nmController', 'LIKE', '%menu%')
                ->execute($this->_db)
        ;

        return $controller;
    }
    
    
    public function select_controller_nm($nmController) {
        $controller = DB::select()
                ->from('LOGIN_Controller')
                ->where('LOGIN_Controller.nmController', '=', $nmController)
                ->execute($this->_db)
        ;

        return $controller;
    }

    
    public function select_acao($idAcao) {
        $perfil = DB::select('LOGIN_Acao.idAcao', 'LOGIN_Acao.nmAcao', 'LOGIN_Acao.dsAcao', 'LOGIN_Acao.cdController', 'LOGIN_Acao.ativo',
                array('LOGIN_Controller.nmController', 'Controller'))
                ->from('LOGIN_Acao')
                ->join('LOGIN_Controller', 'LEFT')
                ->on('LOGIN_Controller.nmController', '=', 'LOGIN_Acao.cdController')
                ->where('LOGIN_Acao.idAcao', '=', $idAcao)
                ->execute($this->_db)
        ;

        return $perfil;
    }

    
    public function insert_perfil($nmPerfil, $dsPerfil, $status) {
        $colunas = array(
            'nmPerfil',
            'dsPerfil',
            'ativo',
        );

        $values = array(
            trim($nmPerfil),
            trim($dsPerfil),
            trim($status),
        );

        $perfil = DB::insert('LOGIN_Perfil', $colunas)
                ->values($values)
                ->execute($this->_db)
        ;

        return $perfil;
    }
    
    
    public function insert_perfil_acao($idPerfil, $idAcao, $Status) {
        $colunas = array(
            'idPerfil',
            'idAcao',
            'Status',
        );

        $values = array(
            trim($idPerfil),
            trim($idAcao),
            trim($Status),
        );

        $perfis = DB::insert('LOGIN_PerfilTemAcao', $colunas)
                ->values($values)
                ->execute($this->_db)
        ;

        return $perfis;
    }
    
    
    public function delete_perfil($idPerfil) {
        $perfil = DB::delete('LOGIN_Perfil')
                ->where('idPerfil', '=', $idPerfil)
                ->execute($this->_db)
        ;

        return $perfil;
    }
    
    
    public function delete_acao_perfil($idPerfil) {
        $perfil = DB::delete('LOGIN_PerfilTemAcao')
                ->where('idPerfil', '=', $idPerfil)
                ->execute($this->_db)
        ;

        return $perfil;
    }
    

    public function update_perfil($idPerfil, $nmPerfil, $dsPerfil) {
        $perfil = DB::update('LOGIN_Perfil')
                ->set(array('nmPerfil' => $nmPerfil))
                ->set(array('dsPerfil' => $dsPerfil))
                ->where('idPerfil', '=', $idPerfil)
                ->execute($this->_db)
        ;

        return $perfil;
    }
    
    
    public function update_perfil_acao($idPerfilAt, $idAcao, $Status) {
        $perfil = DB::update('LOGIN_PerfilTemAcao')
                ->set(array('idAcao' => $idAcao))
                ->set(array('Status' => $Status))
                ->where('idPerfil', '=', $idPerfilAt)
                ->and_where('idAcao', '=', $idAcao)
                ->execute($this->_db)
        ;

        return $perfil;
    }
    
    
    public function update_ativar_perfil($idPerfil) {
        $perfil = DB::select('LOGIN_Perfil.ativo')
                ->from('LOGIN_Perfil')
                ->where('idPerfil', '=', $idPerfil)
                ->execute($this->_db)
        ;

        if ($perfil[0]['ativo'] == 'S') {
            $pairs = array(
                'ativo' => 'N',
            );
        } else {
            $pairs = array(
                'ativo' => 'S',
            );
        }

        $state = DB::update('LOGIN_Perfil')
                ->set($pairs)
                ->where('idPerfil', '=', $idPerfil)
                ->execute($this->_db)
        ;
        return $state;
    }

}

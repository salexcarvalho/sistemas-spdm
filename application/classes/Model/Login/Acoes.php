<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Login_Acoes extends Model {

    /**
     * Construtor da classe, já seta a conexao com o banco de dados
     *
     * @param string $con_type
     */
    public function __construct($db = NULL) {
        if ($db != NULL) {
            // Set the instance or name
            $this->_db = $db;
        } else {
            // Use the default name
            $this->_db = Database::$default;
        }

        if (is_string($this->_db)) {
            // Load the database
            $this->_db = Database::instance($this->_db);
        }
    }

    
    public function select_acoes() {
        $acoes = DB::select('LOGIN_Acao.idAcao', 'LOGIN_Acao.nmAcao', 'LOGIN_Acao.dsAcao', 'LOGIN_Acao.idCurto', 'LOGIN_Acao.cdController', 'LOGIN_Acao.ativo',
                array('LOGIN_Controller.nmController', 'Controller') , array('LOGIN_Controller.dsController', 'dsController'))
                ->from('LOGIN_Acao')
                ->join('LOGIN_Controller', 'LEFT')
                ->on('LOGIN_Controller.nmController', '=', 'LOGIN_Acao.cdController')
                ->execute($this->_db)
        ;

        return $acoes;
    }

    
    public function select_controller() {
        $controller = DB::select()
                ->from('LOGIN_Controller')
                ->execute($this->_db)
        ;

        return $controller;
    }
    
    
    public function select_controller_nm($nmController) {
        $controller = DB::select()
                ->from('LOGIN_Controller')
                ->where('LOGIN_Controller.nmController', '=', $nmController)
                ->execute($this->_db)
        ;

        return $controller;
    }

    
    public function select_acao($idAcao) {
        $acao = DB::select('LOGIN_Acao.idAcao', 'LOGIN_Acao.nmAcao', 'LOGIN_Acao.dsAcao', 'LOGIN_Acao.idCurto', 'LOGIN_Acao.cdController', 'LOGIN_Acao.ativo',
                array('LOGIN_Controller.nmController', 'Controller'))
                ->from('LOGIN_Acao')
                ->join('LOGIN_Controller', 'LEFT')
                ->on('LOGIN_Controller.nmController', '=', 'LOGIN_Acao.cdController')
                ->where('LOGIN_Acao.idAcao', '=', $idAcao)
                ->execute($this->_db)
        ;

        return $acao;
    }
    
    public function select_acao_nm($nmAcao) {
        $controller = DB::select()
                ->from('LOGIN_Acao')
                ->where('LOGIN_Acao.nmAcao', '=', $nmAcao)
                ->execute($this->_db)
        ;

        return $controller;
    }
    
    
    public function select_acao_idcurto($idCurto) {
        $controller = DB::select()
                ->from('LOGIN_Acao')
                ->where('LOGIN_Acao.idCurto', '=', $idCurto)
                ->execute($this->_db)
        ;

        return $controller;
    }

    
    public function insert_acao($nmAcao, $dsAcao, $idCurto, $nmController) {
        $colunas = array(
            'nmAcao',
            'dsAcao',
            'idCurto',
            'cdController',
        );

        $values = array(
            trim($nmAcao),
            trim($dsAcao),
            trim($idCurto),
            trim($nmController),
        );

        $usuario = DB::insert('LOGIN_Acao', $colunas)
                ->values($values)
                ->execute($this->_db)
        ;

        return $usuario;
    }
    
    
    public function insert_controller($nmController, $dsController) {
        $colunas = array(
            'nmController',
            'dsController',
        );

        $values = array(
            trim($nmController),
            trim($dsController),
        );

        $Controller = DB::insert('LOGIN_Controller', $colunas)
                ->values($values)
                ->execute($this->_db)
        ;

        return $Controller;
    }
    
    
    public function delete_acao($idAcao) {
        $acao = DB::delete('LOGIN_Acao')
                ->where('idAcao', '=', $idAcao)
                ->execute($this->_db)
        ;

        return $acao;
    }
    
    
    public function delete_perfil_acao($idAcao) {
        $acao = DB::delete('LOGIN_PerfilTemAcao')
                ->where('idAcao', '=', $idAcao)
                ->execute($this->_db)
        ;

        return $acao;
    }
    

    public function update_acao($idAcao, $nmAcao, $dsAcao, $idCurto, $nmController) {
        $acao = DB::update('LOGIN_Acao')
                ->set(array('nmAcao' => $nmAcao))
                ->set(array('dsAcao' => $dsAcao))
                ->set(array('idCurto' => $idCurto))
                ->set(array('cdController' => $nmController))
                ->where('idAcao', '=', $idAcao)
                ->execute($this->_db)
        ;

        return $acao;
    }
    
    
    public function update_ativar_acao($idAcao) {
        $acao = DB::select('LOGIN_Acao.ativo')
                ->from('LOGIN_Acao')
                ->where('idAcao', '=', $idAcao)
                ->execute($this->_db)
        ;

        if ($acao[0]['ativo'] == 'S') {
            $pairs = array(
                'ativo' => 'N',
            );
        } else {
            $pairs = array(
                'ativo' => 'S',
            );
        }

        $state = DB::update('LOGIN_Acao')
                ->set($pairs)
                ->where('idAcao', '=', $idAcao)
                ->execute($this->_db)
        ;
        return $state;
    }
    
    public function select_quantidade_acoes() {
        return $this->select_acoes()->count();
    }

}

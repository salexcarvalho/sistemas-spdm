<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Login_Usuario extends Model {

    /**
     * Construtor da classe, já seta a conexao com o banco de dados
     *
     * @param string $con_type
     */
    public function __construct($db = NULL) {
         if ($db != NULL) {
            // Set the instance or name
            $this->_db = $db;
        } else {
            // Use the default name
            $this->_db = Database::$default;
        }

        if (is_string($this->_db)) {
            // Load the database
            $this->_db = Database::instance($this->_db);
        }
    }

    public function select_usuarios() {
        $usuarios = DB::select('LOGIN_Usuario.idUsuario', 'LOGIN_Usuario.nmNome', 'LOGIN_Usuario.nmUsuario', 'LOGIN_Usuario.idTipoSetor', 'LOGIN_Usuario.email', 'LOGIN_Usuario.logins', 
                'LOGIN_Usuario.dtCadastro', 'LOGIN_Usuario.ativo', array('LOGIN_Perfil.nmPerfil', 'perfil'))
                ->from('LOGIN_Usuario')
                ->join('LOGIN_UsuarioTemPerfil', 'LEFT')
                ->on('LOGIN_UsuarioTemPerfil.idUsuario', '=', 'LOGIN_Usuario.idUsuario')
                ->join('LOGIN_Perfil', 'LEFT')
                ->on('LOGIN_Perfil.idPerfil', '=', 'LOGIN_UsuarioTemPerfil.idPerfil')
                ->execute($this->_db)
        ;

        return $usuarios;
    }

    public function select_perfis() {
        $usuario = DB::select()
                ->from('LOGIN_Perfil')
                ->execute($this->_db)
        ;

        return $usuario;
    }

    public function select_usuario($idUsuario) {
        $usuario = DB::select('LOGIN_Usuario.idUsuario', 'LOGIN_Usuario.nmNome', 'LOGIN_Usuario.nmUsuario', 'LOGIN_Usuario.idTipoSetor', 'LOGIN_Usuario.email', 
                'LOGIN_Usuario.logins', 'LOGIN_Usuario.dtCadastro', 'LOGIN_Usuario.ultimoLogin', 'LOGIN_Usuario.ativo', 
                array('LOGIN_Perfil.nmPerfil', 'perfil'), array('LOGIN_UsuarioTemPerfil.idPerfil', 'perfil'))
                ->from('LOGIN_Usuario')
                ->join('LOGIN_UsuarioTemPerfil', 'LEFT')
                ->on('LOGIN_UsuarioTemPerfil.idUsuario', '=', 'LOGIN_Usuario.idUsuario')
                ->join('LOGIN_Perfil', 'LEFT')
                ->on('LOGIN_Perfil.idPerfil', '=', 'LOGIN_UsuarioTemPerfil.idPerfil')
                ->where('LOGIN_Usuario.idUsuario', '=', $idUsuario)
                ->execute($this->_db)
        ;

        return $usuario;
    }
    
    public function select_usuarios_setor($idTipoSetor) {
        $usuarios = DB::select('LOGIN_Usuario.idUsuario', 'LOGIN_Usuario.nmNome', 'LOGIN_Usuario.nmUsuario', 'LOGIN_Usuario.idTipoSetor', 'LOGIN_Usuario.email', 
                'LOGIN_Usuario.logins', 'LOGIN_Usuario.dtCadastro', 'LOGIN_Usuario.ultimoLogin', 'LOGIN_Usuario.ativo', 
                array('LOGIN_Perfil.nmPerfil', 'perfil'), array('LOGIN_UsuarioTemPerfil.idPerfil', 'perfil'))
                ->from('LOGIN_Usuario')
                ->join('LOGIN_UsuarioTemPerfil', 'LEFT')
                ->on('LOGIN_UsuarioTemPerfil.idUsuario', '=', 'LOGIN_Usuario.idUsuario')
                ->join('LOGIN_Perfil', 'LEFT')
                ->on('LOGIN_Perfil.idPerfil', '=', 'LOGIN_UsuarioTemPerfil.idPerfil')
                ->where('LOGIN_Usuario.idTipoSetor', '=', $idTipoSetor)
                ->execute($this->_db)
        ;

        return $usuarios;
    }

    public function select_usuario_pornome($nmUsuario) {
        $usuario = DB::select()
                ->from('LOGIN_Usuario')
                ->where('nmUsuario', '=', $nmUsuario)
                ->and_where('ativo', '=', 'S')
                ->execute($this->_db)
        ;
        return $usuario;
    }
    
    public function select_usuario_pornome_ins($nmUsuario) {
        $usuario = DB::select()
                ->from('LOGIN_Usuario')
                ->where('nmUsuario', '=', $nmUsuario)
                ->execute($this->_db)
        ;
        return $usuario;
    }
    
    public function select_usuario_redrfinir($nmUsuarioEmail) {
        $usuario = DB::select()
                ->from('LOGIN_Usuario')
                ->where('ativo', '=', 'S')
                ->and_where_open()
                ->and_where('nmUsuario', '=', $nmUsuarioEmail)
                ->or_where('email', '=', $nmUsuarioEmail)
                ->and_where_close()
                ->execute($this->_db)
        ;
        return $usuario;
    }
    
    public function select_usuario_perfil($nmUsuario) {
        $usuario = DB::select('LOGIN_Usuario.idUsuario', 'LOGIN_Usuario.nmUsuario', 'LOGIN_Usuario.nmNome', 'LOGIN_Usuario.email', 'LOGIN_Usuario.idTipoSetor', array('LOGIN_Perfil.nmPerfil', 'nmPerfil'), 
                array('LOGIN_UsuarioTemPerfil.idPerfil', 'perfil'), array('LOGIN_TipoSetor.Setor', 'nmSetor'), array('LOGIN_TipoSetor.idTipoSetor', 'idSetor'))
                ->from('LOGIN_Usuario')
                ->join('LOGIN_UsuarioTemPerfil', 'LEFT')
                ->on('LOGIN_UsuarioTemPerfil.idUsuario', '=', 'LOGIN_Usuario.idUsuario')
                ->join('LOGIN_Perfil', 'LEFT')
                ->on('LOGIN_Perfil.idPerfil', '=', 'LOGIN_UsuarioTemPerfil.idPerfil')
                ->join('LOGIN_TipoSetor', 'LEFT')
                ->on('LOGIN_TipoSetor.idTipoSetor', '=', 'LOGIN_Usuario.idTipoSetor')
                ->where('nmUsuario', '=', $nmUsuario)
                ->and_where('LOGIN_Usuario.ativo', '=', 'S')
                ->execute($this->_db)
        ;
        return $usuario;
    }
    

    public function select_quantidade_usuarios() {
        return $this->select_usuarios()->count();
    }

    public function update_login_usuario($nmUsuario) {
        $pairs = array(
            'logins' => DB::expr('logins + 1'),
            'ultimoLogin' => DB::expr('NOW()')
        );

        $state = DB::update('LOGIN_Usuario')
                ->set($pairs)
                ->where('nmUsuario', '=', $nmUsuario)
                ->execute($this->_db)
        ;
        return $state;
    }

    public function insert_usuario($nmNome, $nmUsuario, $idTipoSetor, $senha, $email) {
        $colunas = array(
            'nmNome',
            'nmUsuario',
            'idTipoSetor',
            'senha',
            'email',
            'dtCadastro',
        );

        $values = array(
            trim($nmNome),
            trim($nmUsuario),
            trim($idTipoSetor),
            trim($senha),
            trim($email),
            DB::expr('NOW()'),
        );

        $usuario = DB::insert('LOGIN_Usuario', $colunas)
                ->values($values)
                ->execute($this->_db)
        ;

        return $usuario;
    }

    public function insert_usuario_perfil($idUsuario, $idPerfil) {
        $colunas = array(
            'idUsuario',
            'idPerfil',
        );

        $values = array(
            trim($idUsuario),
            trim($idPerfil),
        );

        $usuario = DB::insert('LOGIN_UsuarioTemPerfil', $colunas)
                ->values($values)
                ->execute($this->_db)
        ;

        return $usuario;
    }
    
    
    public function delete_usuario($idUsuario) {
        $usuario = DB::delete('LOGIN_Usuario')
                ->where('idUsuario', '=', $idUsuario)
                ->execute($this->_db)
        ;

        return $usuario;
    }
    
    
    public function delete_perfil_usuario($idUsuario) {
        $usuario = DB::delete('LOGIN_UsuarioTemPerfil')
                ->where('idUsuario', '=', $idUsuario)
                ->execute($this->_db)
        ;

        return $usuario;
    }
    

    public function update_usuario($idUsuario, $nmNome, $nmUsuario, $idTipoSetor, $hash, $email) {
        $usuario = DB::update('LOGIN_Usuario')
                ->set(array('nmNome' => $nmNome))
                ->set(array('nmUsuario' => $nmUsuario))
                ->set(array('idTipoSetor' => $idTipoSetor))
                ->set(array('senha' => $hash))
                ->set(array('email' => $email))
                ->where('idUsuario', '=', $idUsuario)
                ->execute($this->_db)
        ;

        return $usuario;
    }
    
    public function update_senha($Token, $Email, $hash) {
        $usuario = DB::update('LOGIN_Usuario')
                ->set(array('senha' => $hash))
                ->where('senha', '=', $Token)
                ->and_where('email', '=', $Email)
                ->execute($this->_db)
        ;

        return $usuario;
    }


    public function update_usuario_simples($idUsuario, $nmNome, $nmUsuario, $idTipoSetor, $email) {
        $usuario = DB::update('LOGIN_Usuario')
                ->set(array('nmNome' => $nmNome))
                ->set(array('nmUsuario' => $nmUsuario))
                ->set(array('idTipoSetor' => $idTipoSetor))
                ->set(array('email' => $email))
                ->where('idUsuario', '=', $idUsuario)
                ->execute($this->_db)
        ;

        return $usuario;
    }
    
    public function update_perfil_usuario($idUsuario, $idPerfil) {
        $usuario = DB::update('LOGIN_UsuarioTemPerfil')
                ->set(array('idUsuario' => $idUsuario))
                ->set(array('idPerfil' => $idPerfil))
                ->where('idUsuario', '=', $idUsuario)
                ->execute($this->_db)
        ;

        return $usuario;
    }
    
    
    public function update_ativar_usuario($idUsuario) {
        $usuario = DB::select('LOGIN_Usuario.ativo')
                ->from('LOGIN_Usuario')
                ->where('idUsuario', '=', $idUsuario)
                ->execute($this->_db)
        ;

        if ($usuario[0]['ativo'] == 'S') {
            $pairs = array(
                'ativo' => 'N',
            );
        } else {
            $pairs = array(
                'ativo' => 'S',
            );
        }

        $state = DB::update('LOGIN_Usuario')
                ->set($pairs)
                ->where('idUsuario', '=', $idUsuario)
                ->execute($this->_db)
        ;
        return $state;
    }
    
    public function select_campos(){
        $colunas = DB::query(Database::SELECT, 'SHOW FULL COLUMNS FROM LOGIN_Usuario');
        return $colunas->execute($this->_db);
    }
}

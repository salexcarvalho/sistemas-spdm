<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Login_Setor extends Model {

    /**
     * Construtor da classe, já seta a conexao com o banco de dados
     *
     * @param string $con_type
     */
    public function __construct($db = NULL) {
        if ($db != NULL) {
            // Set the instance or name
            $this->_db = $db;
        } else {
            // Use the default name
            $this->_db = Database::$default;
        }

        if (is_string($this->_db)) {
            // Load the database
            $this->_db = Database::instance($this->_db);
        }
    }

    // Select para tipos de grupos
    public function select_tipo() {
        $tiposetor = DB::select('idTipoSetor', 'Setor', 'Descricao', 'dtCadastro', 'dtAlteracao', 'Status')
                ->from('LOGIN_TipoSetor')
                ->execute($this->_db)
        ;
        return $tiposetor;
    }
    
    // Select para tipos de grupos por id
    public function select_tipo_id($idTipoSetor) {
        $tiposetor = DB::select('idTipoSetor', 'Setor', 'Descricao', 'dtCadastro', 'dtAlteracao', 'Status')
                ->from('LOGIN_TipoSetor')
                ->where('idTipoSetor', '=', $idTipoSetor)
                ->execute($this->_db)
        ;
        return $tiposetor;
    }

    // Insert para tipos de grupos
    public function insert_tipo($Setor, $Descricao, $status) {
        $colunas = array(
            'Setor',
            'Descricao',
            'dtCadastro',
            'Status',
            'idUsuario'
        );
        $values = array(
            $Setor,
            $Descricao,
            date('yy-m-d'),
            $status,
            $_SESSION['idUsuario']
        );
        $tiposetor = DB::insert("LOGIN_TipoSetor", $colunas)
                ->values($values)
                ->execute($this->_db)
        ;
        return $tiposetor;
    }

    // Update para tipos de grupos
    public function update_tipo($idTipoSetor, $Setor, $Descricao, $status) {
        
        $tiposetor = DB::update('LOGIN_TipoSetor')
                ->set(array('Setor' => $Setor))
                ->set(array('Descricao' => $Descricao))
                ->set(array('Status' => $status))
                ->set(array('idUsuario' => $_SESSION['idUsuario']))
                ->set(array('dtAlteracao' => date('yy-m-d')))
                ->where('idTipoSetor', '=', $idTipoSetor)
                ->execute($this->_db)
        ;
        return $tiposetor;
    }
    
    // Deleta para tipos de grupos
    public function delete_tipo($idTipoSetor){
        $tiposetor = DB::delete('LOGIN_TipoSetor')
                       ->where('idTipoSetor', '=', $idTipoSetor)
                       ->execute($this->_db);
        return $tiposetor;
    }
    
    public function update_ativar_tipo($idTipoSetor){
        $tiposetor = DB::select('LOGIN_TipoSetor.Status')
                ->from('LOGIN_TipoSetor')
                ->where('idTipoSetor', '=', $idTipoSetor)
                ->execute($this->_db)
        ;

        if ($tiposetor[0]['Status'] == '1')
        {
            $pairs = array('Status' => '0',);
        }
        else
        {
            $pairs = array('Status' => '1',);
        }

        $state = DB::update('LOGIN_TipoSetor')
                ->set($pairs)
                ->where('idTipoSetor', '=', $idTipoSetor)
                ->execute($this->_db)
        ;
        return $state;
    }

}

<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Login_Configuracoes extends Model {

    /**
     * Construtor da classe, já seta a conexao com o banco de dados
     *
     * @param string $con_type
     */
    public function __construct($db = NULL) {
        if ($db != NULL) {
            // Set the instance or name
            $this->_db = $db;
        } else {
            // Use the default name
            $this->_db = Database::$default;
        }

        if (is_string($this->_db)) {
            // Load the database
            $this->_db = Database::instance($this->_db);
        }
    }

    public function select_config() {
        $config = DB::select('SIS_Config.idConfig', 'SIS_Config.company_name', 'SIS_Config.company_legal_name', 'SIS_Config.contact_person', 'SIS_Config.company_address',
                'SIS_Config.company_cep', 'SIS_Config.company_city', 'SIS_Config.company_state', 'SIS_Config.company_email', 'SIS_Config.company_phone',
                'SIS_Config.company_cellphone', 'SIS_Config.company_url', 'SIS_Config.logo_admin')
                ->from('SIS_Config')
                ->where('SIS_Config.idConfig', '=', '1')
                ->execute($this->_db)
        ;

        return $config;
    }
    
    public function update_config($vars, $img) {
        $state = DB::update('SIS_Config')
                ->set(array('company_name' => $vars['company_name']))
                ->set(array('company_legal_name' => $vars['company_legal_name']))
                ->set(array('contact_person' => $vars['contact_person']))
                ->set(array('company_address' => $vars['company_address']))
                ->set(array('company_cep' => $vars['company_cep']))
                ->set(array('company_city' => $vars['company_city']))
                ->set(array('company_state' => $vars['company_state']))
                ->set(array('company_email' => $vars['company_email']))
                ->set(array('company_phone' => $vars['company_phone']))
                ->set(array('company_cellphone' => $vars['company_cellphone']))
                ->set(array('company_url' => $vars['company_url']))
                ->set(array('logo_admin' => $img))
                ->where('SIS_Config.idConfig', '=', '1')
                ->execute($this->_db)
        ;
        return $state;
    }   
}

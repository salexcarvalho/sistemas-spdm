<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Rsa_Area extends Model {

    /**
     * Construtor da classe, já seta a conexao com o banco de dados
     *
     * @param string $con_type
     */
    public function __construct($db = NULL) {
        if ($db != NULL) {
            // Set the instance or name
            $this->_db = $db;
        } else {
            // Use the default name
            $this->_db = Database::$default;
        }

        if (is_string($this->_db)) {
            // Load the database
            $this->_db = Database::instance($this->_db);
        }
    }

    public function select_areas() {
        $area = DB::select("idArea, nome, local, status, criado_em, atualizado_em, idUsuario")
                ->from("RSA_Area")
                ->where('excluido', '=', '0')
                ->order_by('nome', 'ASC')
                ->execute($this->_db);
        return $area;
    }

    public function select_areas_filtro($nome) {
        $area = DB::select("idArea, nome, local, status, criado_em, atualizado_em, idUsuario")
                ->from("RSA_Area")
                ->where('excluido', '=', '0')
                ->and_where("nome", '=', $nome)
                ->execute($this->_db);
        return $area;
    }
    
    public function select_areas_filtro_id($idArea) {
        $area = DB::select("idArea, nome, local, status, criado_em, atualizado_em, idUsuario")
                ->from("RSA_Area")
                ->where('excluido', '=', '0')
                ->and_where("idArea", '=', $idArea)
                ->execute($this->_db);
        return $area;
    }

    public function select_areas_id($idArea) {
        $area = DB::select("idArea")
                ->from("RSA_Area")
                ->where('excluido', '=', '0')
                ->and_where("idArea", '=', $idArea)
                ->execute($this->_db);
        if (count($area) > 0):
            for ($i = 0; $i < count($area); $i++):
                $ids[] = $area[$i]['idArea'];
            endfor;
        else:
            $ids = null;
        endif;

        return $ids;
    }

    public function select_quantidade_areas() {
        return $this->select_areas()->count();
    }

    public function insert_area($nome, $endereco, $status) {

        $area = DB::insert("RSA_Area", array("nome", "local", "status", "criado_em", "idUsuario"))
                ->values(array($nome, $endereco, $status, date('yy-m-d'), $_SESSION['idUsuario']))
                ->execute($this->_db);
        return $area;
    }

    public function update_area($idArea, $nome, $local, $status) {
        $area = DB::update('RSA_Area')
                ->set(array('nome' => $nome))
                ->set(array('local' => $local))
                ->set(array('status' => $status))
                ->set(array('atualizado_em' => date('yy-m-d')))
                ->set(array('idUsuario' => $_SESSION['idUsuario']))
                ->where('idArea', '=', $idArea)
                ->execute($this->_db)
        ;

        return $area;
    }

    public function update_ativar_area($idArea) {
        $area = DB::select('status')
                ->from('RSA_Area')
                ->where('idArea', '=', $idArea)
                ->and_where('excluido', '=', '0')
                ->execute($this->_db);
        $model_salas = new Model_Rsa_Salas('default');        
        $salas = $model_salas->select_salas_ids($idArea);
 
        if(count($salas)>0){
            for($i=0;$i<count($salas);$i++):
                $model_salas->update_ativar_sala($salas[$i]);
            endfor;
        }

        if ($area[0]['status'] == '1') {
            $pairs = array('status' => '0',);
        } else {
            $pairs = array('status' => '1',);
        }
        
        $state = DB::update('RSA_Area')
                ->set($pairs)
                ->where('idArea', '=', $idArea)
                ->execute($this->_db);
        return $state;
    }

    public function delete_area($idArea) {
        $returned_id = DB::delete('RSA_Area')
                ->where('idArea', '=', $idArea)
                ->execute($this->_db);
        return $returned_id;
    }

    public function excluir_area($idArea) {
        $state = DB::update('RSA_Area')
                ->set(array('excluido' => '1'))
                ->where('idArea', '=', $idArea)
                ->execute($this->_db);
        return $state;
    }

}

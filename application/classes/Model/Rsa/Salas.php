<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Rsa_Salas extends Model {
    /**
     * Construtor da classe, já seta a conexao com o banco de dados
     *
     * @param string $con_type
     */
    public function __construct($db = NULL) {
         if ($db != NULL) {
            // Set the instance or name
            $this->_db = $db;
        } else {
            // Use the default name
            $this->_db = Database::$default;
        }

        if (is_string($this->_db)) {
            // Load the database
            $this->_db = Database::instance($this->_db);
        }
    }
    
    public function select_salas_reservar(){
        $salas = DB::select('RSA_Salas.idSalas', 'RSA_Salas.nome', 'RSA_Salas.capacidade', 'RSA_Salas.extras', 'RSA_Salas.rgb', 'RSA_Salas.area_id', 'RSA_Salas.status',
                'RSA_Salas.criado_em', 'RSA_Salas.atualizado_em', 'RSA_Salas.idUsuario',  'RSA_Salas.idTipoSetor',  'RSA_Salas.idUsuario_at', array('RSA_Area.alias', 'Alias'),
                array('RSA_Area.nome', 'NomeArea'))
                ->from('RSA_Salas')
                ->join('RSA_Area', 'LEFT')
                ->on('RSA_Area.idArea', '=', 'RSA_Salas.area_id')
                ->where('RSA_Salas.excluido', '=', '0');
                if ($_SESSION['Perfil'] !== 'Administrador') {
                    $salas->and_where('RSA_Salas.idTipoSetor', '=', $_SESSION['idSetor']);
                }
                return $salas->order_by('Alias', 'DESC')->execute($this->_db); 
    }
    
    public function select_salas_publico(){
        $salas = DB::select('RSA_Salas.idSalas', 'RSA_Salas.nome', 'RSA_Salas.capacidade', 'RSA_Salas.extras', 'RSA_Salas.rgb', 'RSA_Salas.area_id', 'RSA_Salas.status',
                'RSA_Salas.criado_em', 'RSA_Salas.atualizado_em', 'RSA_Salas.idUsuario',  'RSA_Salas.idTipoSetor',  'RSA_Salas.idUsuario_at', array('RSA_Area.alias', 'Alias'),
                array('RSA_Area.nome', 'NomeArea'))
                ->from('RSA_Salas')
                ->join('RSA_Area', 'LEFT')
                ->on('RSA_Area.idArea', '=', 'RSA_Salas.area_id')
                ->where('RSA_Salas.excluido', '=', '0')
                ->and_where('RSA_Salas.status', '=', '1')
                ->order_by('Alias', 'DESC')
                ->execute($this->_db); 
        return $salas;
    }
    
    public function select_salas(){
        $salas = DB::select('RSA_Salas.idSalas', 'RSA_Salas.nome', 'RSA_Salas.capacidade', 'RSA_Salas.extras', 'RSA_Salas.rgb', 'RSA_Salas.area_id', 'RSA_Salas.status',
                'RSA_Salas.criado_em', 'RSA_Salas.atualizado_em', 'RSA_Salas.idUsuario',  'RSA_Salas.idTipoSetor',  'RSA_Salas.idUsuario_at', array('RSA_Area.alias', 'Alias'))
                ->from('RSA_Salas')
                ->join('RSA_Area', 'LEFT')
                ->on('RSA_Area.idArea', '=', 'RSA_Salas.area_id')
                ->where('RSA_Salas.excluido', '=', '0')
                ->and_where('RSA_Salas.status', '=', '1')
                ->order_by('Alias', 'DESC')
                ->execute($this->_db); 
        return $salas; 
    }
    
    public function select_salas_filtro($nome, $cap){
        $salas = DB::select('RSA_Salas.idSalas', 'RSA_Salas.nome', 'RSA_Salas.capacidade', 'RSA_Salas.extras', 'RSA_Salas.rgb', 'RSA_Salas.area_id', 'RSA_Salas.status',
                'RSA_Salas.criado_em', 'RSA_Salas.atualizado_em', 'RSA_Salas.idUsuario',  'RSA_Salas.idTipoSetor',  'RSA_Salas.idUsuario_at')
                ->from('RSA_Salas')
                ->where('RSA_Salas.capacidade', '=', $cap)
                ->or_where('RSA_Salas.nome', '=', $nome)
                ->and_where('RSA_Salas.excluido', '=', '0')
                ->order_by('RSA_Salas.nome', 'ASC')
                ->execute($this->_db);
        return $salas; 
    }
    
    public function select_salas_id($idSalas){
        $salas = DB::select('RSA_Salas.idSalas', 'RSA_Salas.nome', 'RSA_Salas.capacidade', 'RSA_Salas.extras', 'RSA_Salas.rgb', 'RSA_Salas.area_id', 'RSA_Salas.status',
                'RSA_Salas.criado_em', 'RSA_Salas.atualizado_em', 'RSA_Salas.idUsuario',  'RSA_Salas.idTipoSetor',  'RSA_Salas.idUsuario_at')
                ->from('RSA_Salas')                
                ->where('RSA_Salas.idSalas', '=', $idSalas)
                ->and_where('RSA_Salas.excluido', '=', '0')
                ->order_by('RSA_Salas.nome', 'ASC')
                ->execute($this->_db);
        return $salas; 
    }
    
    public function select_salas_rgb($idSalas){
        $salas = DB::select('RSA_Salas.rgb', 'RSA_Salas.nome')
                ->from('RSA_Salas')                
                ->where('RSA_Salas.idSalas', '=', $idSalas)
                ->and_where('RSA_Salas.excluido', '=', '0')
                ->execute($this->_db);
        return $salas; 
    }
    
    public function select_salas_area($area_id){
        $salas = DB::select('RSA_Salas.idSalas', 'RSA_Salas.nome', 'RSA_Salas.capacidade', 'RSA_Salas.extras', 'RSA_Salas.rgb', 'RSA_Salas.area_id', 'RSA_Salas.status',
                'RSA_Salas.criado_em', 'RSA_Salas.atualizado_em', 'RSA_Salas.idUsuario',  'RSA_Salas.idTipoSetor',  'RSA_Salas.idUsuario_at', array('RSA_Area.nome', 'Area'))
                ->from('RSA_Salas')
                ->join('RSA_Area')
                ->on('RSA_Area.idArea', '=', 'RSA_Salas.area_id')
                ->where('RSA_Salas.area_id', '=', $area_id)
                ->and_where('RSA_Salas.excluido', '=', '0')
                ->and_where('RSA_Salas.status', '=', '1')
                ->order_by('RSA_Salas.nome', 'ASC')
                ->as_assoc()
                ->execute($this->_db);
        return $salas; 
    }
    
    public function select_salas_ids($area_id){
        $salas = DB::select('RSA_Salas.idSalas')
                ->from('RSA_Salas')
                ->where('RSA_Salas.area_id', '=', $area_id)           
                ->execute($this->_db);
        if (count($salas) > 0):
            for($i=0;$i<count($salas);$i++):
                  $ids[] =  $salas[$i]['RSA_Salas.idSalas'];  
            endfor;
        else:
            $ids = null;
        endif;

        return $ids; 
    }
    
    public function select_quantidade_salas() {
        return $this->select_salas()->count();
    }
    
    public function select_reservas_area_qtde($idArea) {
        return $this->select_salas_area($idArea)->count();
    }
    
    public function insert_sala($nome, $capacidade, $extra, $rgb, $area_id, $status){
         $salas = DB::insert('RSA_Salas',array('RSA_Salas.nome', 'RSA_Salas.capacidade', 'RSA_Salas.extras', 'RSA_Salas.rgb', 'RSA_Salas.area_id', 'RSA_Salas.status',
             'RSA_Salas.criado_em', 'RSA_Salas.idUsuario',  'RSA_Salas.idTipoSetor'))
                ->values(array($nome, $capacidade, $extra, $rgb, $area_id, $status, date('yy-m-d'), $_SESSION['idUsuario'], $_SESSION['idSetor']))
                ->execute($this->_db);
        return $salas;
    }
    
    public function update_sala($idSalas, $nome, $capacidade, $extra, $area_id, $status) {
        $salas = DB::update('RSA_Salas')
                ->set(array('RSA_Salas.nome' => $nome))
                ->set(array('RSA_Salas.capacidade' => $capacidade))
                ->set(array('RSA_Salas.extras' => $extra))
                ->set(array('RSA_Salas.area_id' => $area_id))
                ->set(array('RSA_Salas.status' => $status))
                ->set(array('RSA_Salas.atualizado_em' => date('yy-m-d')))
                ->set(array('RSA_Salas.idUsuario_at' => $_SESSION['idUsuario']))
                ->where('RSA_Salas.idSalas', '=', $idSalas)
                ->execute($this->_db);
        return $salas;
    }
     
    public function update_ativar_sala($idSalas) {
        $salas = DB::select('status')
                ->from('RSA_Salas')
                ->where('idSalas', '=', $idSalas)
                ->execute($this->_db);
        
        if ($salas[0]['status'] == '1') {
            $pairs = array('status' => '0',);
        } else {
            $pairs = array('status' => '1',);
        }

        $state = DB::update('RSA_Salas')
                ->set($pairs)
                ->where('idSalas', '=', $idSalas)
                ->execute($this->_db)
        ;
        return $state;
    }
    
    public function delete_sala($idSalas)    {              
        $returned_id = DB::delete('RSA_Salas')
                       ->where('RSA_Salas.idSalas', 'in', $idSalas)
                       ->and_where('RSA_Salas.excluido', '=', '0')
                       ->execute($this->_db);
        return $returned_id;
    }
    
    public function excluir_salas($idSalas) {
        $state = DB::update('RSA_Salas')
                ->set(array('excluido' => '1'))
                ->where('RSA_Salas.idSalas', 'in', $idSalas)
                ->execute($this->_db);        
        return $state;
    }
}
<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Rsa_Reserva extends Model {

    /**
     * Construtor da classe, já seta a conexao com o banco de dados
     *
     * @param string $con_type
     */
    public function __construct($db = NULL) {
       if ($db != NULL) {
            // Set the instance or name
            $this->_db = $db;
        } else {
            // Use the default name
            $this->_db = Database::$default;
        }

        if (is_string($this->_db)) {
            // Load the database
            $this->_db = Database::instance($this->_db);
        }
    }

    public function select_reservas() {
        $reserva = DB::select('RSA_Reserva.idReserva', 'RSA_Reserva.titulo', 'RSA_Reserva.start_data', 'RSA_Reserva.end_data', 'RSA_Reserva.responsavel', 'RSA_Reserva.sala_id',
                'RSA_Reserva.criado_em', 'RSA_Reserva.atualizado_em', 'RSA_Reserva.idSerie', 'RSA_Reserva.dow', 'RSA_Reserva.obs', 'RSA_Reserva.inicioSerie', 'RSA_Reserva.idUsuario',
                'RSA_Reserva.idUsuario_at')
                ->from('RSA_Reserva')
                ->where('excluido', '=', '0')
                ->order_by('RSA_Reserva.titulo', 'ASC')
                ->execute($this->_db);
        return $reserva;
    }
    
    public function select_reserva_inicio($idSerie) {
        $reserva = DB::select('RSA_Reserva.idReserva', 'RSA_Reserva.start_data', 'RSA_Reserva.end_data', 'RSA_Reserva.idSerie', 'RSA_Reserva.inicioSerie')
                ->from('RSA_Reserva')
                ->where('RSA_Reserva.idSerie', '=', $idSerie)
                ->order_by('RSA_Reserva.idReserva', 'ASC')
                ->execute($this->_db);
        return $reserva;
    }
    
    public function select_reserva_fim($idSerie) {
        $reserva = DB::select('RSA_Reserva.idReserva', 'RSA_Reserva.start_data', 'RSA_Reserva.end_data', 'RSA_Reserva.idSerie', 'RSA_Reserva.inicioSerie')
                ->from('RSA_Reserva')
                ->where('RSA_Reserva.idSerie', '=', $idSerie)
                ->order_by('RSA_Reserva.idReserva', 'DESC')
                ->execute($this->_db);
        return $reserva;
    }
    
    public function select_reserva_serie($idSerie) {
        $reserva = DB::select('RSA_Reserva.idReserva', 'RSA_Reserva.start_data', 'RSA_Reserva.end_data', 'RSA_Reserva.idSerie', 'RSA_Reserva.inicioSerie')
                ->from('RSA_Reserva')
                ->where('RSA_Reserva.idSerie', '=', $idSerie)
                ->order_by('RSA_Reserva.idReserva', 'DESC')
                ->execute($this->_db);
        return $reserva;
    }

    public function select_reserva($idReserva) {
        $reserva = DB::select('RSA_Reserva.idReserva', 'RSA_Reserva.titulo', 'RSA_Reserva.start_data', 'RSA_Reserva.end_data', 'RSA_Reserva.responsavel', 'RSA_Reserva.sala_id',
                'RSA_Reserva.criado_em', 'RSA_Reserva.atualizado_em', 'RSA_Reserva.idSerie', 'RSA_Reserva.dow', 'RSA_Reserva.obs', 'RSA_Reserva.inicioSerie', 'RSA_Reserva.idUsuario',
                'RSA_Reserva.idUsuario_at', PREFIX.TABLELOGIN.'LOGIN_Usuario.nmNome')
                ->from('RSA_Reserva')
                ->join(PREFIX.TABLELOGIN.'LOGIN_Usuario', 'LEFT')
                ->on(PREFIX.TABLELOGIN.'LOGIN_Usuario.idUsuario', '=', 'RSA_Reserva.idUsuario')
                ->where('RSA_Reserva.idReserva', '=', $idReserva)
                ->execute($this->_db);
        return $reserva;
    }
    
    public function select_reservas_mes($mes) {
        $reserva = DB::select('RSA_Reserva.idReserva', 'RSA_Reserva.titulo', 'RSA_Reserva.start_data', 'RSA_Reserva.end_data', 'RSA_Reserva.responsavel', 'RSA_Reserva.sala_id',
                'RSA_Reserva.criado_em', 'RSA_Reserva.atualizado_em', 'RSA_Reserva.idSerie', 'RSA_Reserva.dow', 'RSA_Reserva.obs', 'RSA_Reserva.inicioSerie', 'RSA_Reserva.idUsuario')
                ->from('RSA_Reserva')
                ->where('MONTH(RSA_Reserva.start_data)', '=', $mes)
                ->order_by('RSA_Reserva.titulo', 'ASC')
                ->execute($this->_db);
        return $reserva;
    }

    public function select_reservas_salas($idSala) {
        $reserva = DB::select('RSA_Reserva.idReserva', 'RSA_Reserva.titulo', 'RSA_Reserva.start_data', 'RSA_Reserva.end_data', 'RSA_Reserva.responsavel', 'RSA_Reserva.sala_id',
                'RSA_Reserva.criado_em', 'RSA_Reserva.atualizado_em', 'RSA_Reserva.idSerie', 'RSA_Reserva.dow', 'RSA_Reserva.obs', 'RSA_Reserva.inicioSerie', 'RSA_Reserva.idUsuario')
                ->from('RSA_Reserva')
                ->where('RSA_Reserva.sala_id', '=', $idSala)
                ->order_by('RSA_Reserva.titulo', 'ASC')
                ->execute($this->_db);
        return $reserva;
    }

    public function select_reservas_salas_qtde($idSala) {
        return $this->select_reservas_salas($idSala)->count();
    }

    public function select_reservas_mes_qtde($mes) {
        return $this->select_reservas_mes($mes)->count();
    }


    public function select_reservas_data() {
        $reserva = DB::select('RSA_Reserva.idReserva', 'RSA_Reserva.titulo', 'RSA_Reserva.start_data', 'RSA_Reserva.end_data', 'RSA_Reserva.responsavel', 'RSA_Reserva.sala_id',
                'RSA_Reserva.idUsuario', 'RSA_Reserva.criado_em', 'RSA_Reserva.idUsuario_at', 'RSA_Reserva.atualizado_em', 'RSA_Reserva.idSerie', 'RSA_Reserva.dow', 'RSA_Reserva.obs',
                'RSA_Reserva.inicioSerie',
                array('RSA_Salas.nome','NomeSala'))
                ->from('RSA_Reserva')
                ->join('RSA_Salas', 'LEFT')
                ->on('RSA_Salas.idSalas', '=', 'RSA_Reserva.sala_id')
                ->where('RSA_Reserva.excluido', '=', '0')
                ->execute($this->_db);
        return $reserva;
    }

    public function select_reservas_relatorio($start, $end) {
        $reserva = DB::select('RSA_Reserva.idReserva', 'RSA_Reserva.titulo', 'RSA_Reserva.start_data', 'RSA_Reserva.end_data', 'RSA_Reserva.responsavel', 'RSA_Reserva.sala_id',
                'RSA_Salas.nome as Sala', 'RSA_Area.nome as Area', 'RSA_Area.local', 'RSA_Reserva.dow', 'RSA_Reserva.obs')
                ->from('RSA_Reserva')
                ->join('RSA_Salas', 'INNER')
                ->on('RSA_Reserva.sala_id', '=', 'idSalas')
                ->join('RSA_Area', 'INNER')
                ->on('area_id', '=', 'idArea')
                ->where('RSA_Reserva.start_data', '>=', $start)
                ->and_where('RSA_Reserva.start_data', '<=', $end)
                ->and_where('RSA_Reserva.excluido', '=', '0')
                ->order_by('RSA_Reserva.sala_id', 'ASC')
                ->execute($this->_db);
        return $reserva;
    }
    
    public function select_reservas_relatorio2($dataini, $datafim, $sala) {
        $reserva = DB::select('RSA_Reserva.idReserva', 'RSA_Reserva.titulo', 'RSA_Reserva.start_data', 'RSA_Reserva.end_data', 'RSA_Reserva.responsavel', 'RSA_Reserva.sala_id',
                'RSA_Reserva.allday', 'RSA_Salas.nome as Sala', 'RSA_Area.nome as Area', 'RSA_Salas.rgb', 'RSA_Reserva.dow', 'RSA_Reserva.obs', 'RSA_Reserva.inicioSerie', 'RSA_Reserva.idSerie')
                ->from("RSA_Reserva")
                ->join("RSA_Salas", "INNER")
                ->on('sala_id', '=', 'idSalas')
                ->join("RSA_Area", "INNER")
                ->on('area_id', '=', 'idArea')
                ->where("sala_id", '=', $sala)
                ->and_where('start_data', '>=', $dataini.' 00:00')
                ->and_where("end_data", '<=', $datafim.' 23:59')
                ->order_by('start_data', 'Asc')
                ->execute($this->_db);
        //Funcoes::dump($reserva);
        return $reserva;
    }
    
    public function select_reservas_relatorio3($data, $inicio, $end, $sala) {
        $reserva = DB::select('idReserva', 'titulo', 'start_data', 'end_data', 'responsavel', 'sala_id', 'allday', 'RSA_Salas.nome as Sala', 'RSA_Area.nome as Area', 'rgb', 'dow', 'obs')
                ->from("RSA_Reserva")
                ->join("RSA_Salas", "INNER")
                ->on('sala_id', '=', 'idSalas')
                ->join("RSA_Area", "INNER")
                ->on('area_id', '=', 'idArea')
                ->where("date_format(`start_data`, '%Y-%m-%d')", '=', $data)
                ->and_where("date_format(`start_data`, '%H:%i:%s')", '>=', $inicio)
                ->and_where("sala_id", '=', $sala)               
                ->execute($this->_db);        
        return $reserva;
    }

    public function select_reservas_relatorio_id($idReserva) {
        $reserva = DB::select('RSA_Reserva.idReserva', 'RSA_Reserva.titulo', 'RSA_Reserva.start_data', 'RSA_Reserva.end_data', 'RSA_Reserva.responsavel', 'RSA_Reserva.sala_id',
                'RSA_Salas.nome as Sala', 'RSA_Area.nome as Area', 'RSA_Area.local', 'RSA_Reserva.dow', 'RSA_Reserva.obs')
                ->from('RSA_Reserva')
                ->join('RSA_Salas', 'INNER')
                ->on('RSA_Reserva.sala_id', '=', 'idSalas')
                ->join('RSA_Area', 'INNER')
                ->on('area_id', '=', 'idArea')
                ->where('RSA_Reserva.idReserva', '=', $idReserva)
                ->and_where('RSA_Reserva.excluido', '=', '0')
                ->execute($this->_db);
        return $reserva;
    }

    public function select_reservas_relatorio_by_id($idReserva) {
        $reserva = DB::select('RSA_Reserva.idReserva', 'RSA_Reserva.titulo', 'RSA_Reserva.start_data', 'RSA_Reserva.end_data', 'RSA_Reserva.responsavel', 'RSA_Reserva.sala_id',
                'RSA_Reserva.idUsuario', 'RSA_Reserva.criado_em', 'RSA_Reserva.idUsuario_at', 'RSA_Reserva.atualizado_em', 'RSA_Reserva.dow', 'RSA_Reserva.obs')
                ->from('RSA_Reserva')
                ->where('RSA_Reserva.idReserva', '=', $idReserva)
                ->and_where('excluido', '=', '0')
                ->execute($this->_db);
        return $reserva;
    }

    public function select_reservas_datas() {
        $reserva = DB::select("DATE('RSA_Reserva.start_data') as dataR", "count('RSA_Reserva.idReserva') as total")
                ->group_by(DATE('RSA_Reserva.start_data'))
                ->from('RSA_Reserva')
                ->execute($this->_db);
        return $reserva;
    }

    public function select_reservas_ids($idSalas) {
        $reserva = DB::select('RSA_Reserva.idReserva')
                ->from('RSA_Reserva')
                ->where('RSA_Reserva.sala_id', 'in', $idSalas)
                ->execute($this->_db);

        if (count($reserva) > 0):
            for ($i = 0; $i < count($reserva); $i++):
                $ids[] = $reserva[$i]['RSA_Reserva.idReserva'];
            endfor;
        else:
            $ids = null;
        endif;

        return $ids;
    }

    public function select_quantidade_reservas() {
        return $this->select_reservas()->count();
    }

    public function insert_reserva($titulo, $start_data, $end_data, $responsavel, $sala_id, $idSerie = NULL, $inicioSerie = 0, $dow = NULL, $obs) {
        $reserva = DB::insert('RSA_Reserva', array('RSA_Reserva.titulo', 'RSA_Reserva.start_data', 'RSA_Reserva.end_data', 'RSA_Reserva.responsavel', 'RSA_Reserva.sala_id',
            'RSA_Reserva.idSerie', 'RSA_Reserva.inicioSerie', 'RSA_Reserva.dow', 'RSA_Reserva.obs', 'RSA_Reserva.criado_em', 'RSA_Reserva.idUsuario'))
                ->values(array($titulo, $start_data, $end_data, $responsavel, $sala_id, $idSerie, $inicioSerie, $dow, $obs, date('Y-m-d'), $_SESSION['idUsuario']))
                ->execute($this->_db);
        return $reserva;
    }

    public function update_reserva($idReserva, $titulo, $start_data, $end_data, $responsavel, $sala_id, $idSerie = NULL, $inicioSerie = 0, $dow, $obs) {
        $reserva = DB::update('RSA_Reserva')
                ->set(array('RSA_Reserva.titulo' => $titulo))
                ->set(array('RSA_Reserva.start_data' => $start_data))
                ->set(array('RSA_Reserva.end_data' => $end_data))
                ->set(array('RSA_Reserva.responsavel' => $responsavel))
                ->set(array('RSA_Reserva.sala_id' => $sala_id))
                ->set(array('RSA_Reserva.inicioSerie' => $inicioSerie))
                ->set(array('RSA_Reserva.dow' => $dow))
                ->set(array('RSA_Reserva.idSerie' => $idSerie))
                ->set(array('RSA_Reserva.obs' => $obs))
                ->set(array('RSA_Reserva.atualizado_em' => date('Y-m-d')))
                ->set(array('RSA_Reserva.idUsuario_at' => $_SESSION['idUsuario']))
                ->where('RSA_Reserva.idReserva', '=', $idReserva)
                ->execute($this->_db);
        return $reserva;
    }
    
    public function change_reserva($idReserva, $titulo, $start_data, $end_data, $responsavel, $sala_id) {
        $reserva = DB::update('RSA_Reserva')
                ->set(array('RSA_Reserva.titulo' => $titulo))
                ->set(array('RSA_Reserva.start_data' => $start_data))
                ->set(array('RSA_Reserva.end_data' => $end_data))
                ->set(array('RSA_Reserva.responsavel' => $responsavel))
                ->set(array('RSA_Reserva.sala_id' => $sala_id))
                ->set(array('RSA_Reserva.atualizado_em' => date('Y-m-d')))
                ->set(array('RSA_Reserva.idUsuario_at' => $_SESSION['idUsuario']))
                ->where('RSA_Reserva.idReserva', '=', $idReserva)
                ->execute($this->_db);
        return $reserva;
    }

    public function excluir_reserva($idReserva) {
        $returned_id = DB::update('RSA_Reserva')
                ->set(array('excluido' => '1'))
                ->where('RSA_Reserva.idReserva', 'in', $idReserva)
                ->execute($this->_db);
        return $returned_id;
    }
    
    public function excluir_reserva_p($idReserva) {
        $returned_id = DB::delete('RSA_Reserva')
                ->where('RSA_Reserva.idReserva', '=', $idReserva)
                ->execute($this->_db);
        return $returned_id;
    }
    
    public function excluir_reserva_serie($idSerie) {
        $returned_id = DB::delete('RSA_Reserva')
                ->where('RSA_Reserva.idSerie', '=', $idSerie)
                ->execute($this->_db);
        return $returned_id;
    }

    public function update_ativar_reserva($idReserva) {
        $salas = DB::select('status')
                ->from('RSA_Reserva')
                ->where('RSA_Reserva.idReserva', '=', $idReserva)
                ->execute($this->_db);

        if ($salas[0]['status'] == '1') {
            $pairs = array('status' => '0',);
        } else {
            $pairs = array('status' => '1',);
        }

        $state = DB::update('RSA_Reserva')
                ->set($pairs)
                ->where('RSA_Reserva.idReserva', '=', $idReserva)
                ->execute($this->_db)
        ;
        return $state;
    }
    public function disponibilidade($idSala, $start_data, $end_data){
          $reserva = DB::select('RSA_Reserva.idReserva')
                ->from('RSA_Reserva')
                ->where('excluido', '=', '0')
                ->and_where_open()
                ->where('start_data', '>=', $start_data)
                ->and_where('end_data', '<=', $end_data)
                ->and_where_close()
                ->and_where('sala_id', '=', $idSala)
                ->execute($this->_db);
        return $reserva;
       
    }

}

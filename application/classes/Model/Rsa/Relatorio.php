<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Rsa_Relatorio extends Model {
    /**
     * Construtor da classe, já seta a conexao com o banco de dados
     *
     * @param string $con_type
     */
    public function __construct($db = NULL) {
        if ($db != NULL) {
            // Set the instance or name
            $this->_db = $db;
        } else {
            // Use the default name
            $this->_db = Database::$default;
        }

        if (is_string($this->_db)) {
            // Load the database
            $this->_db = Database::instance($this->_db);
        }
    }
    public function select_areas(){
        $area = DB::select("idArea, nome, local, status, excluido")
                ->from("RSA_Area")
                ->order_by('nome', 'ASC')
                ->execute($this->_db);
        return $area; 
    }    
    public function select_salas(){
        $salas = DB::select("idSalas, nome, capacidade, extras, rgb, area_id, status, excluido")
                ->from("RSA_Salas")
                ->order_by('nome', 'ASC')
                ->execute($this->_db);
        return $salas; 
    }
    public function select_reservas() {
        $reserva = DB::select("idReserva, titulo, start_data, end_data, sala_id, allday, status, excluido")
                ->from("RSA_Reserva")
                ->order_by('titulo', 'ASC')
                ->execute($this->_db);
        return $reserva;
    }
     public function select_salas_area($area_id){
        $salas = DB::select("idSalas, nome, capacidade, extras, rgb, area_id, status, excluido")
                ->from("RSA_Salas")
                ->where("area_id","=",$area_id)
                ->execute($this->_db);
        return $salas; 
    }
    public function select_reservas_salas($sala_id) {
        $reserva = DB::select("idReserva, titulo, start_data, end_data, sala_id, allday, status, excluido")
                ->from("RSA_Reserva")
                ->where("sala_id","=",$sala_id)
                ->execute($this->_db);
        return $reserva;
    }
    public function select_qtde_areas() {
          return $this->select_areas()->count();
    }
    public function select_qtde_salas() {
          return $this->select_salas()->count();
    }
    public function select_qtde_reservas() {
          return $this->select_reservas()->count();
    }
}
<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Rsa_Agendamento extends Model {

    /**
     * Construtor da classe, já seta a conexao com o banco de dados
     *
     * @param string $con_type
     */
    public function __construct($db = NULL) {
        if ($db != NULL) {
            // Set the instance or name
            $this->_db = $db;
        } else {
            // Use the default name
            $this->_db = Database::$default;
        }

        if (is_string($this->_db)) {
            // Load the database
            $this->_db = Database::instance($this->_db);
        }
    }
    public function select_agendamentos(){
        $agenda = DB::select('RSA_Agendamento.idAgendamento', 'RSA_Agendamento.titulo', 'RSA_Agendamento.start_data', 'RSA_Agendamento.end_data', 'RSA_Agendamento.sala_id', 'RSA_Agendamento.resp_nome', 'RSA_Agendamento.resp_email', 'RSA_Agendamento.resp_telefone', 'RSA_Agendamento.resp_celular', 'RSA_Agendamento.idSerie', 'RSA_Agendamento.inicioSerie', 'RSA_Agendamento.resp_nome_ag', 'RSA_Agendamento.resp_email_ag', 'RSA_Agendamento.resp_telefone_ag', 'RSA_Agendamento.resp_celular_ag', 'RSA_Agendamento.status', 'RSA_Agendamento.dtCadastro', 'RSA_Agendamento.dtConfirmacao', 'RSA_Agendamento.dtCadastro', 'RSA_Agendamento.idUsuario')
                ->from("RSA_Agendamento")
                ->where('RSA_Agendamento.status','=','0')
                ->order_by('RSA_Agendamento.resp_email', 'ASC')
                ->execute($this->_db);
        return $agenda;
    }
    public function select_agendamento($idAgendamento){
       $agenda = DB::select('RSA_Agendamento.idAgendamento', 'RSA_Agendamento.titulo', 'RSA_Agendamento.start_data', 'RSA_Agendamento.end_data', 'RSA_Agendamento.sala_id', 'RSA_Agendamento.resp_nome', 'RSA_Agendamento.resp_email', 'RSA_Agendamento.resp_telefone', 'RSA_Agendamento.resp_celular', 'RSA_Agendamento.idSerie', 'RSA_Agendamento.inicioSerie', 'RSA_Agendamento.resp_nome_ag', 'RSA_Agendamento.resp_email_ag', 'RSA_Agendamento.resp_telefone_ag', 'RSA_Agendamento.resp_celular_ag', 'RSA_Agendamento.status', 'RSA_Agendamento.dtCadastro', 'RSA_Agendamento.dtConfirmacao', 'RSA_Agendamento.dtCadastro', 'RSA_Agendamento.idUsuario')
                ->from("RSA_Agendamento")
                ->where('RSA_Agendamento.idAgendamento','=',$idAgendamento)
                ->execute($this->_db);
        return $agenda;
    }
    public function insert_agendamento($titulo, $start_data, $end_data, $sala_id, $resp_nome, $resp_email, $resp_telefone, $resp_celular, $idSerie, $inicioSerie, $resp_nome_ag, $resp_email_ag, $resp_telefone_ag, $resp_celular_ag){
        $agenda = DB::insert("RSA_Agendamento",array('RSA_Agendamento.idAgendamento', 'RSA_Agendamento.titulo', 'RSA_Agendamento.start_data', 'RSA_Agendamento.end_data', 'RSA_Agendamento.sala_id', 'RSA_Agendamento.resp_nome', 'RSA_Agendamento.resp_email', 'RSA_Agendamento.resp_telefone', 'RSA_Agendamento.resp_celular', 'RSA_Agendamento.idSerie', 'RSA_Agendamento.inicioSerie', 'RSA_Agendamento.resp_nome_ag', 'RSA_Agendamento.resp_email_ag', 'RSA_Agendamento.resp_telefone_ag', 'RSA_Agendamento.resp_celular_ag', 'RSA_Agendamento.dtCadastro'))
               ->values(array($titulo, $start_data, $end_data, $sala_id, $resp_nome, $resp_email, $resp_telefone, $resp_celular, $resp_nome_ag, $resp_email_ag, $resp_telefone_ag, $resp_celular_ag, date('Y-m-d')))
                ->execute($this->_db);
        return $agenda;
    }
    public function change_status($idAgendamento, $status){
       $agenda = DB::update('RSA_Agendamento')                
                ->set(array('RSA_Agendamento.status' => $status))
                ->set(array('RSA_Agendamento.idUsuario' => $_SESSION['idUsuario']))
                ->set(array('RSA_Agendamento.dtConfirmacao' => date('Y-m-d')))
                ->where('RSA_Agendamento.idAgendamento', '=', $idAgendamento)
                ->execute($this->_db);
        return $agenda;
    }
    public function select_visualizar($idAgendamento){
        $agenda = DB::select('RSA_Agendamento.idAgendamento', 'RSA_Agendamento.titulo', 'RSA_Agendamento.start_data', 'RSA_Agendamento.end_data', 'RSA_Agendamento.sala_id', 'RSA_Agendamento.resp_nome', 'RSA_Agendamento.resp_email', 'RSA_Agendamento.resp_telefone', 'RSA_Agendamento.resp_celular', 'RSA_Agendamento.resp_nome_ag', 'RSA_Agendamento.resp_email_ag', 'RSA_Agendamento.resp_telefone_ag', 'RSA_Agendamento.resp_celular_ag', 'RSA_Agendamento.status', 'RSA_Agendamento.dtCadastro', 'RSA_Agendamento.dtConfirmacao', 'RSA_Agendamento.idUsuario', array('RSA_Salas.nome', 'sala'),array('RSA_Area.nome', 'local'))
                ->from("RSA_Agendamento")
                ->join('RSA_Salas')
                ->on('RSA_Agendamento.sala_id', '=','RSA_Salas.idSalas')
                ->join('RSA_Area')
                ->on('RSA_Salas.area_id', '=','RSA_Area.idArea')
                ->where('RSA_Agendamento.idAgendamento','=',$idAgendamento)
                ->execute($this->_db);       
        return $agenda;
    }
}
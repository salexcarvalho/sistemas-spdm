<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Secretaria_Ata extends Model {

    /**
     * Construtor da classe, já seta a conexao com o banco de dados
     *
     * @param string $con_type
     */
    public function __construct($db = NULL) {
        if ($db != NULL) {
            // Set the instance or name
            $this->_db = $db;
        } else {
            // Use the default name
            $this->_db = Database::$default;
        }

        if (is_string($this->_db)) {
            // Load the database
            $this->_db = Database::instance($this->_db);
        }
    }

    public function select_atas($Pesquisa) {
        $ata = DB::select(
                'SEP_Ata.idAta', 'SEP_Ata.idTipoDoc', 'SEP_Ata.idSetor', 'SEP_Ata.Ata', 'SEP_Ata.Assunto',
                'SEP_Ata.dtReuniao', 'SEP_Ata.NRegistro', 'SEP_Ata.Obs',
                'SEP_Ata.idUsuario', 'SEP_Ata.dtCadastro', 'SEP_Ata.dtAlteracao', 'SEP_Ata.Status', PREFIX.TABLELOGIN.'LOGIN_TipoSetor.Setor', 'SEP_TipoDoc.Nome')
                ->from('SEP_Ata')
                ->join(PREFIX.TABLELOGIN.'LOGIN_TipoSetor', 'LEFT')
                ->on(PREFIX.TABLELOGIN.'LOGIN_TipoSetor.idTipoSetor', '=', 'SEP_Ata.idSetor')
                ->join('SEP_TipoDoc', 'LEFT')
                ->on('SEP_TipoDoc.idTipoDoc', '=', 'SEP_Ata.idTipoDoc');
                if ($_SESSION['Perfil'] !== 'Administrador') {
                    $ata->where('SEP_Ata.idTipoSetor', '=', $_SESSION['idSetor']);
                }
                if ($Pesquisa !== '') {   
                    $ata->and_where('SEP_Ata.Assunto', 'LIKE', '%'.$Pesquisa.'%')->execute($this->_db);                            
                }
                return $ata->order_by('SEP_Ata.ata', 'ASC')->execute($this->_db);
    }

    public function select_ata($idAta) {
        $ata = DB::select(
                'SEP_Ata.idAta', 'SEP_Ata.idTipoDoc',  'SEP_Ata.idSetor', 'SEP_Ata.Ata', 'SEP_Ata.Assunto',
                'SEP_Ata.dtReuniao', 'SEP_Ata.NRegistro', 'SEP_Ata.Obs',
                'SEP_Ata.idUsuario', 'SEP_Ata.dtCadastro', 'SEP_Ata.dtAlteracao', 'SEP_Ata.Status', 'SEP_TipoDoc.Nome')
                ->from('SEP_Ata')
                ->join('SEP_TipoDoc', 'LEFT')
                ->on('SEP_TipoDoc.idTipoDoc', '=', 'SEP_Ata.idTipoDoc')
                ->where('SEP_Ata.idAta', '=', $idAta)
                ->execute($this->_db);
        return $ata;
    }

    public function insert_ata($vars, $dtReuniao) {
        $colunas = array(
            'idSetor',
            'idTipoDoc',
            'Ata',
            'Assunto',
            'dtReuniao',
            'NRegistro',
            'Obs',
            'idUsuario',
            'idTipoSetor',
            'dtCadastro',
            'Status'
        );
        $values = array(
            $vars['idSetor'],
            $vars['idTipoDoc'],
            $vars['Ata'],
            $vars['Assunto'],
            $dtReuniao,
            $vars['NRegistro'],
            $vars['Obs'],
            $_SESSION['idUsuario'],
            $_SESSION['idSetor'],
            date('yy-m-d'),
            $vars['Status']
        );
        $returned_id = DB::insert("SEP_Ata", $colunas)
                ->values($values)
                ->execute($this->_db)
        ;
        return $returned_id;
    }
    
    public function update_ata($idAta, $vars, $dtReuniao) {
        $state = DB::update('SEP_Ata')
                ->set(array('idSetor' => $vars['idSetor']))
                ->set(array('idTipoDoc' => $vars['idTipoDoc']))
                ->set(array('Ata' => $vars['Ata']))
                ->set(array('Assunto' => $vars['Assunto']))
                ->set(array('dtReuniao' => $dtReuniao))
                ->set(array('NRegistro' => $vars['NRegistro']))
                ->set(array('Obs' => $vars['Obs']))
                ->set(array('idUsuario' => $_SESSION['idUsuario']))
                ->set(array('idTipoSetor' => $_SESSION['idSetor']))
                ->set(array('dtAlteracao' => date('yy-m-d')))
                ->set(array('Status' => $vars['Status']))
                ->where('idAta', '=', $idAta)
                ->execute($this->_db)
        ;
        return $state;
    }

    public function delete_ata($idAta) {
        $Ata = DB::delete('SEP_Ata')
                ->where('idAta', '=', $idAta)
                ->execute($this->_db);
        return $Ata;
    }

    public function update_ativar_ata($idAta) {

        $atas = DB::select('SEP_Ata.Status')
                ->from('SEP_Ata')
                ->where('idAta', '=', $idAta)
                ->execute($this->_db)
        ;

        if ($atas[0]['Status'] == '1') {
            $pairs = array('Status' => '0',);
        } else {
            $pairs = array('Status' => '1',);
        }

        $state = DB::update('SEP_Ata')
                ->set($pairs)
                ->where('SEP_Ata.idAta', '=', $idAta)
                ->execute($this->_db)
        ;
        return $state;
    }

}

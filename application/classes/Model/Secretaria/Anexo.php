<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Secretaria_Anexo extends Model {

    /**
     * Construtor da classe, já seta a conexao com o banco de dados
     *
     * @param string $con_type
     */
    public function __construct($db = NULL) {
        if ($db != NULL) {
            // Set the instance or name
            $this->_db = $db;
        } else {
            // Use the default name
            $this->_db = Database::$default;
        }

        if (is_string($this->_db)) {
            // Load the database
            $this->_db = Database::instance($this->_db);
        }
    }
    
    public function select_anexo_ata($idAta) {
        $protocolo = DB::select
                        ('SEP_Anexo.idAnexo', 'SEP_Anexo.idAta', 'SEP_Anexo.Caminho')
                    ->from('SEP_Anexo')
                    ->where('SEP_Anexo.idAta', '=', $idAta)
                    ->and_where('SEP_Anexo.idAta', '!=', NULL)
                    ->order_by('SEP_Anexo.idAnexo', 'DESC')
                    ->execute($this->_db);        
        return $protocolo;
    }
    
    public function insert_anexo_ata($idAta, $newname) {
        $colunas = array(
            'idAta',
            'Caminho',
            'idUsuario',
            'dtCadastro'
        );
        $values = array(
            $idAta,
            $newname,
            $_SESSION['idUsuario'],
            date('yy-m-d')
        );
        $anexo = DB::insert("SEP_Anexo", $colunas)
                ->values($values)
                ->execute($this->_db)
        ;
        return $anexo;
    }
    
    public function delete_anexo_ata($idAta) {
        $Protocolo = DB::delete('SEP_Anexo')
                ->where('idAta', '=', $idAta)
                ->execute($this->_db);
        return $Protocolo;
    }
    
    public function select_anexo_documento($idDocumentos) {
        $protocolo = DB::select
                        ('SEP_Anexo.idAnexo', 'SEP_Anexo.idDocumentos', 'SEP_Anexo.Caminho')
                     ->from('SEP_Anexo')
                     ->where('SEP_Anexo.idDocumentos', '=', $idDocumentos)
                     ->and_where('SEP_Anexo.idDocumentos', '!=', NULL)
                     ->execute($this->_db);        
        return $protocolo;
    }
    
    public function insert_anexo_documento($idDocumentos, $newname) {
        $colunas = array(
            'idDocumentos',
            'Caminho',
            'idUsuario',
            'dtCadastro'
        );
        $values = array(
            $idDocumentos,
            $newname,
            $_SESSION['idUsuario'],
            date('yy-m-d')
        );
        $anexo = DB::insert("SEP_Anexo", $colunas)
                ->values($values)
                ->execute($this->_db)
        ;
        return $anexo;
    }
    
    public function delete_anexo_documento($idDocumentos) {
        $Protocolo = DB::delete('SEP_Anexo')
                ->where('idDocumentos', '=', $idDocumentos)
                ->execute($this->_db);
        return $Protocolo;
    }
    
    public function select_anexo_contrato($idContrato) {
        $protocolo = DB::select
                        ('SEP_Anexo.idAnexo', 'SEP_Anexo.idContrato', 'SEP_Anexo.Caminho')
                     ->from('SEP_Anexo')
                     ->where('SEP_Anexo.idContrato', '=', $idContrato)
                     ->and_where('SEP_Anexo.idContrato', '!=', NULL)
                     ->execute($this->_db);        
        return $protocolo;
    }
    
    public function insert_anexo_contrato($idContrato, $newname) {
        $colunas = array(
            'idContrato',
            'Caminho',
            'idUsuario',
            'dtCadastro'
        );
        $values = array(
            $idContrato,
            $newname,
            $_SESSION['idUsuario'],
            date('yy-m-d')
        );
        $anexo = DB::insert("SEP_Anexo", $colunas)
                ->values($values)
                ->execute($this->_db)
        ;
        return $anexo;
    }
    
    public function delete_anexo_contrato($idContrato) {
        $Protocolo = DB::delete('SEP_Anexo')
                ->where('idContrato', '=', $idContrato)
                ->execute($this->_db);
        return $Protocolo;
    }
    
    public function select_anexo_protocolo($idProtocolo) {
        $protocolo = DB::select
                        ('SEP_Anexo.idAnexo', 'SEP_Anexo.idProtocolo', 'SEP_Anexo.Caminho')
                     ->from('SEP_Anexo')
                     ->where('SEP_Anexo.idProtocolo', '=', $idProtocolo)
                     ->and_where('SEP_Anexo.idProtocolo', '!=', NULL)
                     ->execute($this->_db);        
        return $protocolo;
    }
    
    public function insert_anexo_protocolo($idProtocolo, $newname) {
        $colunas = array(
            'idProtocolo',
            'Caminho',
            'idUsuario',
            'dtCadastro'
        );
        $values = array(
            $idProtocolo,
            $newname,
            $_SESSION['idUsuario'],
            date('yy-m-d')
        );
        $anexo = DB::insert("SEP_Anexo", $colunas)
                ->values($values)
                ->execute($this->_db)
        ;
        return $anexo;
    }
    
    public function delete_anexo_protocolo($idProtocolo) {
        $Protocolo = DB::delete('SEP_Anexo')
                ->where('idProtocolo', '=', $idProtocolo)
                ->execute($this->_db);
        return $Protocolo;
    }
    
    public function select_anexo_memorando($idMemorando) {
        $protocolo = DB::select
                        ('SEP_Anexo.idAnexo', 'SEP_Anexo.idMemorando', 'SEP_Anexo.Caminho')
                     ->from('SEP_Anexo')
                     ->where('SEP_Anexo.idMemorando', '=', $idMemorando)
                     ->and_where('SEP_Anexo.idMemorando', '!=', NULL)
                     ->execute($this->_db);        
        return $protocolo;
    }
    
    public function insert_anexo_memorando($idMemorando, $newname) {
        $colunas = array(
            'idMemorando',
            'Caminho',
            'idUsuario',
            'dtCadastro'
        );
        $values = array(
            $idMemorando,
            $newname,
            $_SESSION['idUsuario'],
            date('yy-m-d')
        );
        $anexo = DB::insert("SEP_Anexo", $colunas)
                ->values($values)
                ->execute($this->_db)
        ;
        return $anexo;
    }
    
    public function delete_anexo_memorando($idMemorando) {
        $Protocolo = DB::delete('SEP_Anexo')
                ->where('idMemorando', '=', $idMemorando)
                ->execute($this->_db);
        return $Protocolo;
    }
    
    public function select_anexo_oficio($idOficio) {
        $oficio= DB::select
                        ('SEP_Anexo.idAnexo', 'SEP_Anexo.idOficio', 'SEP_Anexo.Caminho')
                     ->from('SEP_Anexo')
                     ->where('SEP_Anexo.idOficio', '=', $idOficio)
                     ->and_where('SEP_Anexo.idOficio', '!=', NULL)
                     ->execute($this->_db);        
        return $oficio;
    }
    
    public function insert_anexo_oficio($idOficio, $newname) {
        $colunas = array(
            'idOficio',
            'Caminho',
            'idUsuario',
            'dtCadastro'
        );
        $values = array(
            $idOficio,
            $newname,
            $_SESSION['idUsuario'],
            date('yy-m-d')
        );
        $anexo = DB::insert("SEP_Anexo", $colunas)
                ->values($values)
                ->execute($this->_db)
        ;
        return $anexo;
    }
    
    public function delete_anexo_oficio($idOficio) {
        $Protocolo = DB::delete('SEP_Anexo')
                ->where('idOficio', '=', $idOficio)
                ->execute($this->_db);
        return $Protocolo;
    }
    
    public function select_anexo_movimentacao($idMovimentacao) {
        $protocolo = DB::select
                        ('SEP_Anexo.idAnexo', 'SEP_Anexo.idMovimentacao', 'SEP_Anexo.Caminho')
                     ->from('SEP_Anexo')
                     ->where('SEP_Anexo.idMovimentacao', '=', $idMovimentacao)
                     ->and_where('SEP_Anexo.idMovimentacao', '!=', NULL)
                     ->execute($this->_db);        
        return $protocolo;
    }
    
    public function insert_anexo_movimentacao($idMovimentacao, $newname) {
        $colunas = array(
            'idMovimentacao',
            'Caminho',
            'idUsuario',
            'dtCadastro'
        );
        $values = array(
            $idMovimentacao,
            $newname,
            $_SESSION['idUsuario'],
            date('yy-m-d')
        );
        $anexo = DB::insert("SEP_Anexo", $colunas)
                ->values($values)
                ->execute($this->_db)
        ;
        return $anexo;
    }
    
    public function delete_anexo_movimentacao($idMovimentacao) {
        $Protocolo = DB::delete('SEP_Anexo')
                ->where('idMovimentacao', '=', $idMovimentacao)
                ->execute($this->_db);
        return $Protocolo;
    }
    
    public function delete_anexo($idAnexo) {
        $returned_id = DB::delete('SEP_Anexo')
                ->where('idAnexo', '=', $idAnexo)
                ->execute($this->_db);
        return $returned_id;
    }
}
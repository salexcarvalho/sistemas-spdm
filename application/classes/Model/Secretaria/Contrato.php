<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Secretaria_Contrato extends Model {

    /**
     * Construtor da classe, já seta a conexao com o banco de dados
     *
     * @param string $con_type
     */
    public function __construct($db = NULL) {
        if ($db != NULL) {
            // Set the instance or name
            $this->_db = $db;
        } else {
            // Use the default name
            $this->_db = Database::$default;
        }

        if (is_string($this->_db)) {
            // Load the database
            $this->_db = Database::instance($this->_db);
        }
    }

    public function select_contratos($Pesquisa) {
        $contrato = DB::select(
                'SEP_Contrato.idContrato', 'SEP_Contrato.idTipoDoc', 'SEP_Contrato.idSetor', 'SEP_Contrato.Contrato', 'SEP_Contrato.Assunto',
                'SEP_Contrato.dtAssinatura', 'SEP_Contrato.NRegistro', 'SEP_Contrato.Obs', 'SEP_Contrato.idTipoSetor',
                'SEP_Contrato.idUsuario', 'SEP_Contrato.dtCadastro', 'SEP_Contrato.dtAlteracao', 'SEP_Contrato.Status', PREFIX.TABLELOGIN.'LOGIN_TipoSetor.Setor', 'SEP_TipoDoc.Nome')
                ->from('SEP_Contrato')
                ->join(PREFIX.TABLELOGIN.'LOGIN_TipoSetor', 'LEFT')
                ->on(PREFIX.TABLELOGIN.'LOGIN_TipoSetor.idTipoSetor', '=', 'SEP_Contrato.idSetor')
                ->join('SEP_TipoDoc', 'LEFT')
                ->on('SEP_TipoDoc.idTipoDoc', '=', 'SEP_Contrato.idTipoDoc');
                if ($_SESSION['Perfil'] !== 'Administrador') {
                    $contrato->where('SEP_Contrato.idTipoSetor', '=', $_SESSION['idSetor']);
                }
                if ($Pesquisa !== '') {   
                $contrato->and_where('SEP_Contrato.Assunto', 'LIKE', '%'.$Pesquisa.'%')->execute($this->_db);                            
                }
                return $contrato->order_by('SEP_Contrato.contrato', 'ASC')->execute($this->_db);
    }

    public function select_contrato($idContrato) {
        $contrato = DB::select(
                'SEP_Contrato.idContrato', 'SEP_Contrato.idTipoDoc', 'SEP_Contrato.idSetor', 'SEP_Contrato.Contrato', 'SEP_Contrato.Assunto',
                'SEP_Contrato.dtAssinatura', 'SEP_Contrato.NRegistro', 'SEP_Contrato.Obs',
                'SEP_Contrato.idUsuario', 'SEP_Contrato.dtCadastro', 'SEP_Contrato.dtAlteracao', 'SEP_Contrato.Status', 'SEP_TipoDoc.Nome')
                ->from('SEP_Contrato')
                ->join('SEP_TipoDoc', 'LEFT')
                ->on('SEP_TipoDoc.idTipoDoc', '=', 'SEP_Contrato.idTipoDoc')
                ->where('SEP_Contrato.idContrato', '=', $idContrato)
                ->execute($this->_db);
        return $contrato;
    }

    public function insert_contrato($vars, $dtAssinatura) {
        $colunas = array(
            'idSetor',
            'idTipoDoc',
            'Contrato',
            'Assunto',
            'dtAssinatura',
            'NRegistro',
            'Obs',
            'idUsuario',
            'idTipoSetor',
            'dtCadastro',
            'Status'
        );
        $values = array(
            $vars['idSetor'],
            $vars['idTipoDoc'],
            $vars['Contrato'],
            $vars['Assunto'],
            $dtAssinatura,
            $vars['NRegistro'],
            $vars['Obs'],
            $_SESSION['idUsuario'],
            $_SESSION['idSetor'],
            date('yy-m-d'),
            $vars['Status']
        );
        $returned_id = DB::insert("SEP_Contrato", $colunas)
                ->values($values)
                ->execute($this->_db)
        ;
        return $returned_id;
    }
    
    public function update_contrato($idContrato, $vars, $dtAssinatura) {
        $state = DB::update('SEP_Contrato')
                ->set(array('idSetor' => $vars['idSetor']))
                ->set(array('idTipoDoc' => $vars['idTipoDoc']))
                ->set(array('Contrato' => $vars['Contrato']))
                ->set(array('Assunto' => $vars['Assunto']))
                ->set(array('dtAssinatura' => $dtAssinatura))
                ->set(array('NRegistro' => $vars['NRegistro']))
                ->set(array('Obs' => $vars['Obs']))
                ->set(array('idUsuario' => $_SESSION['idUsuario']))
                ->set(array('idTipoSetor' => $_SESSION['idSetor']))
                ->set(array('dtAlteracao' => date('yy-m-d')))
                ->set(array('Status' => $vars['Status']))
                ->where('idContrato', '=', $idContrato)
                ->execute($this->_db)
        ;
        return $state;
    }

    public function delete_contrato($idContrato) {
        $Contrato = DB::delete('SEP_Contrato')
                ->where('idContrato', '=', $idContrato)
                ->execute($this->_db);
        return $Contrato;
    }

    public function update_ativar_contrato($idContrato) {

        $contratos = DB::select('SEP_Contrato.Status')
                ->from('SEP_Contrato')
                ->where('idContrato', '=', $idContrato)
                ->execute($this->_db)
        ;

        if ($contratos[0]['Status'] == '1') {
            $pairs = array('Status' => '0',);
        } else {
            $pairs = array('Status' => '1',);
        }

        $state = DB::update('SEP_Contrato')
                ->set($pairs)
                ->where('SEP_Contrato.idContrato', '=', $idContrato)
                ->execute($this->_db)
        ;
        return $state;
    }

}

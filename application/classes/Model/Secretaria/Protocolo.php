<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Secretaria_Protocolo extends Model {

    /**
     * Construtor da classe, já seta a conexao com o banco de dados
     *
     * @param string $con_type
     */
    public function __construct($db = NULL) {
        if ($db != NULL) {
            // Set the instance or name
            $this->_db = $db;
        } else {
            // Use the default name
            $this->_db = Database::$default;
        }

        if (is_string($this->_db)) {
            // Load the database
            $this->_db = Database::instance($this->_db);
        }
    }

    public function select_protocolos($Pesquisa) {
        $protocolo = DB::select
                        ('SEP_Protocolo.idProtocolo', 'SEP_Protocolo.idSetor', 'SEP_Protocolo.idTipoDoc', 'SEP_Protocolo.Protocolo', 'SEP_Protocolo.Assunto',
                'SEP_Protocolo.dtEntrada', 'SEP_Protocolo.dtSaida', 'SEP_Protocolo.Retirada', 'SEP_Protocolo.Entregue', 'SEP_Protocolo.NProcuracao', 'SEP_Protocolo.Obs',
                'SEP_Protocolo.idUsuario', 'SEP_Protocolo.dtCadastro', 'SEP_Protocolo.dtAlteracao', 'SEP_Protocolo.Status', PREFIX.TABLELOGIN.'LOGIN_TipoSetor.Setor')
                ->from('SEP_Protocolo')
                ->join(PREFIX.TABLELOGIN.'LOGIN_TipoSetor', 'LEFT')
                ->on(PREFIX.TABLELOGIN.'LOGIN_TipoSetor.idTipoSetor', '=', 'SEP_Protocolo.idSetor');
                if ($_SESSION['Perfil'] !== 'Administrador') {
                    $protocolo->where('SEP_Protocolo.idTipoSetor', '=', $_SESSION['idSetor']);
                }
                if ($Pesquisa !== '') {   
                    $protocolo->and_where('SEP_Protocolo.Assunto', 'LIKE', '%'.$Pesquisa.'%')->execute($this->_db);                            
                }
                return $protocolo->order_by('SEP_Protocolo.protocolo', 'ASC')->execute($this->_db);
    }

    public function select_protocolo($idProtocolo) {
        $protocolo = DB::select
                        ('SEP_Protocolo.idProtocolo', 'SEP_Protocolo.idSetor', 'SEP_Protocolo.idTipoDoc', 'SEP_Protocolo.Protocolo', 'SEP_Protocolo.Assunto',
                'SEP_Protocolo.dtEntrada', 'SEP_Protocolo.dtSaida', 'SEP_Protocolo.Retirada', 'SEP_Protocolo.Entregue', 'SEP_Protocolo.NProcuracao', 'SEP_Protocolo.Obs',
                'SEP_Protocolo.idUsuario', 'SEP_Protocolo.dtCadastro', 'SEP_Protocolo.dtAlteracao', 'SEP_Protocolo.Status')
                ->from('SEP_Protocolo')
                ->where('SEP_Protocolo.idProtocolo', '=', $idProtocolo)
                ->execute($this->_db);
        return $protocolo;
    }

    public function insert_protocolo($vars, $Entrada, $Saida) {
        $colunas = array(
            'idSetor',
            'idTipoDoc',
            'Protocolo',
            'Assunto',
            'dtEntrada',
            'dtSaida',
            'Retirada',
            'Entregue',
            'NProcuracao',
            'Obs',
            'idUsuario',
            'idTipoSetor',
            'dtCadastro',
            'Status'
        );
        $values = array(
            $vars['idSetor'],
            $vars['idTipoDoc'],
            $vars['Protocolo'],
            $vars['Assunto'],
            $Entrada,
            $Saida,
            $vars['Retirada'],
            $vars['Entregue'],
            $vars['NProcuracao'],
            $vars['Obs'],
            $_SESSION['idUsuario'],
            $_SESSION['idSetor'],
            date('yy-m-d'),
            $vars['Status']
        );
        $returned_id = DB::insert("SEP_Protocolo", $colunas)
                ->values($values)
                ->execute($this->_db)
        ;
        return $returned_id;
    }
    
    public function update_protocolo($idProtocolo, $vars, $Entrada, $Saida) {
        $state = DB::update('SEP_Protocolo')
                ->set(array('idSetor' => $vars['idSetor']))
                ->set(array('idTipoDoc' => $vars['idTipoDoc']))
                ->set(array('Protocolo' => $vars['Protocolo']))
                ->set(array('Assunto' => $vars['Assunto']))
                ->set(array('dtEntrada' => $Entrada))
                ->set(array('dtSaida' => $Saida))
                ->set(array('Retirada' => $vars['Retirada']))
                ->set(array('Entregue' => $vars['Entregue']))
                ->set(array('NProcuracao' => $vars['NProcuracao']))
                ->set(array('Obs' => $vars['Obs']))
                ->set(array('idUsuario' => $_SESSION['idUsuario']))
                ->set(array('idTipoSetor' => $_SESSION['idSetor']))
                ->set(array('dtAlteracao' => date('yy-m-d')))
                ->set(array('Status' => $vars['Status']))
                ->where('idProtocolo', '=', $idProtocolo)
                ->execute($this->_db)
        ;
        return $state;
    }

    public function delete_protocolo($idProtocolo) {
        $Protocolo = DB::delete('SEP_Protocolo')
                ->where('idProtocolo', '=', $idProtocolo)
                ->execute($this->_db);
        return $Protocolo;
    }

    public function update_ativar_protocolo($idProtocolo) {

        $protocolos = DB::select('SEP_Protocolo.Status')
                ->from('SEP_Protocolo')
                ->where('idProtocolo', '=', $idProtocolo)
                ->execute($this->_db)
        ;

        if ($protocolos[0]['Status'] == '1') {
            $pairs = array('Status' => '0',);
        } else {
            $pairs = array('Status' => '1',);
        }

        $state = DB::update('SEP_Protocolo')
                ->set($pairs)
                ->where('SEP_Protocolo.idProtocolo', '=', $idProtocolo)
                ->execute($this->_db)
        ;
        return $state;
    }

}

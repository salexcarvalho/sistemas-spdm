<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Secretaria_Documentos extends Model {

    /**
     * Construtor da classe, já seta a conexao com o banco de dados
     *
     * @param string $con_type
     */
    public function __construct($db = NULL) {
        if ($db != NULL) {
            // Set the instance or name
            $this->_db = $db;
        } else {
            // Use the default name
            $this->_db = Database::$default;
        }

        if (is_string($this->_db)) {
            // Load the database
            $this->_db = Database::instance($this->_db);
        }
    }

    public function select_documentos($Pesquisa) {
        $documento = DB::select(
                'SEP_Documentos.idDocumentos', 'SEP_Documentos.idTipoDoc', 'SEP_Documentos.idSetor', 'SEP_Documentos.Documentos', 'SEP_Documentos.Assunto',
                'SEP_Documentos.dtArquivo', 'SEP_Documentos.NRegistro', 'SEP_Documentos.Obs', 'SEP_Documentos.idUsuario', 'SEP_Documentos.dtCadastro',
                'SEP_Documentos.dtAlteracao', 'SEP_Documentos.Status', PREFIX.TABLELOGIN.'LOGIN_TipoSetor.Setor', 'SEP_TipoDoc.Nome')
                ->from('SEP_Documentos')
                ->join(PREFIX.TABLELOGIN.'LOGIN_TipoSetor', 'LEFT')
                ->on(PREFIX.TABLELOGIN.'LOGIN_TipoSetor.idTipoSetor', '=', 'SEP_Documentos.idSetor')
                ->join('SEP_TipoDoc', 'LEFT')
                ->on('SEP_TipoDoc.idTipoDoc', '=', 'SEP_Documentos.idTipoDoc');
                if ($_SESSION['Perfil'] !== 'Administrador') {
                    $documento->where('SEP_Documentos.idTipoSetor', '=', $_SESSION['idSetor']);
                }
                /*if ($TpDoc !== '') {   
                    $documento->and_where('SEP_Documentos.idTipoDoc', 'LIKE', '%'.$TpDoc.'%');                            
                }
                if ($TpSetor !== '') {   
                    $documento->and_where('SEP_Documentos.idSetor', 'LIKE', '%'.$TpSetor.'%');                            
                }*/
                if ($Pesquisa !== '') {   
                    $documento->and_where('SEP_Documentos.Assunto', 'LIKE', '%'.$Pesquisa.'%')->execute($this->_db);                            
                }
                return $documento->order_by('SEP_Documentos.idDocumentos', 'DESC')->execute($this->_db);
    }

    public function select_documento($idDocumentos) {
        $documento = DB::select(
                'SEP_Documentos.idDocumentos', 'SEP_Documentos.idTipoDoc', 'SEP_Documentos.idSetor', 'SEP_Documentos.Documentos', 'SEP_Documentos.Assunto',
                'SEP_Documentos.dtArquivo', 'SEP_Documentos.NRegistro', 'SEP_Documentos.Obs', 'SEP_Documentos.idUsuario', 'SEP_Documentos.dtCadastro',
                'SEP_Documentos.dtAlteracao', 'SEP_Documentos.Status', 'SEP_TipoDoc.Nome')
                ->from('SEP_Documentos')
                ->join('SEP_TipoDoc', 'LEFT')
                ->on('SEP_TipoDoc.idTipoDoc', '=', 'SEP_Documentos.idTipoDoc')
                ->where('SEP_Documentos.idDocumentos', '=', $idDocumentos)
                ->execute($this->_db);
        return $documento;
    }

    public function insert_documento($vars, $dtArquivo) {
        $colunas = array(
            'idSetor',
            'idTipoDoc',
            'Documentos',
            'Assunto',
            'dtArquivo',
            'NRegistro',
            'Obs',
            'idUsuario',
            'idTipoSetor',
            'dtCadastro',
            'Status'
        );
        $values = array(
            $vars['idSetor'],
            $vars['idTipoDoc'],
            $vars['Documentos'],
            $vars['Assunto'],
            $dtArquivo,
            $vars['NRegistro'],
            $vars['Obs'],
            $_SESSION['idUsuario'],
            $_SESSION['idSetor'],
            date('yy-m-d'),
            $vars['Status']
        );
        $returned_id = DB::insert("SEP_Documentos", $colunas)
                ->values($values)
                ->execute($this->_db)
        ;
        return $returned_id;
    }
    
    public function update_documento($idDocumentos, $vars, $dtArquivo) {
        $state = DB::update('SEP_Documentos')
                ->set(array('idSetor' => $vars['idSetor']))
                ->set(array('idTipoDoc' => $vars['idTipoDoc']))
                ->set(array('Documentos' => $vars['Documentos']))
                ->set(array('Assunto' => $vars['Assunto']))
                ->set(array('dtArquivo' => $dtArquivo))
                ->set(array('NRegistro' => $vars['NRegistro']))
                ->set(array('Obs' => $vars['Obs']))
                ->set(array('idUsuario' => $_SESSION['idUsuario']))
                ->set(array('idTipoSetor' => $_SESSION['idSetor']))
                ->set(array('dtAlteracao' => date('yy-m-d')))
                ->set(array('Status' => $vars['Status']))
                ->where('idDocumentos', '=', $idDocumentos)
                ->execute($this->_db)
        ;
        return $state;
    }

    public function delete_documento($idDocumentos) {
        $Documentos = DB::delete('SEP_Documentos')
                ->where('idDocumentos', '=', $idDocumentos)
                ->execute($this->_db);
        return $Documentos;
    }

    public function update_ativar_documento($idDocumentos) {

        $documentos = DB::select('SEP_Documentos.Status')
                ->from('SEP_Documentos')
                ->where('idDocumentos', '=', $idDocumentos)
                ->execute($this->_db)
        ;

        if ($documentos[0]['Status'] == '1') {
            $pairs = array('Status' => '0',);
        } else {
            $pairs = array('Status' => '1',);
        }

        $state = DB::update('SEP_Documentos')
                ->set($pairs)
                ->where('SEP_Documentos.idDocumentos', '=', $idDocumentos)
                ->execute($this->_db)
        ;
        return $state;
    }

}

<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Secretaria_Tipodocumentos extends Model {

    /**
     * Construtor da classe, já seta a conexao com o banco de dados
     *
     * @param string $con_type
     */
    public function __construct($db = NULL) {
        if ($db != NULL) {
            // Set the instance or name
            $this->_db = $db;
        } else {
            // Use the default name
            $this->_db = Database::$default;
        }

        if (is_string($this->_db)) {
            // Load the database
            $this->_db = Database::instance($this->_db);
        }
    }

    public function select_tipo($idTipoDoc) {
        $tipo = DB::select("idTipoDoc", "idModulo", "Nome", "Alias", "Status", "idUsuario", "dtCadastro", "dtAlteracao")
                ->from('SEP_TipoDoc')
                ->where('SEP_TipoDoc.idTipoDoc', '=', $idTipoDoc)
                ->execute($this->_db)
        ;
        return $tipo;
    }
    
    public function select_tipo_alias($Alias) {

        $tipo = DB::select("idTipoDoc", "idModulo", "Nome", "Alias", "Status", "idUsuario", "dtCadastro", "dtAlteracao")
                ->from('SEP_TipoDoc')
                ->where('SEP_TipoDoc.Alias', '=', $Alias)
                ->execute($this->_db)
        ;
        return $tipo;
    }

    public function select_tipos($Pesquisa) {
        $tipo = DB::select("SEP_TipoDoc.idTipoDoc", "SEP_TipoDoc.idModulo", "SEP_TipoDoc.Nome", "SEP_TipoDoc.Alias", "SEP_TipoDoc.Status", "SEP_TipoDoc.idUsuario", "SEP_TipoDoc.dtCadastro", "SEP_TipoDoc.dtAlteracao", array('SEP_Modulo.Nome', 'nmModulo'))
                ->from('SEP_TipoDoc')
                ->join('SEP_Modulo', 'LEFT')
                ->on('SEP_Modulo.idModulo', '=', 'SEP_TipoDoc.idModulo');
                if ($_SESSION['Perfil'] !== 'Administrador') {
                    $tipo->and_where('SEP_TipoDoc.idTipoSetor', '=', $_SESSION['idSetor']);
                }
                if ($Pesquisa !== '') {   
                    $tipo->and_where('SEP_TipoDoc.Nome', 'LIKE', '%'.$Pesquisa.'%')->execute($this->_db);                            
                }
                return $tipo->order_by('SEP_TipoDoc.idTipoDoc', 'DESC')->execute($this->_db);
    }
    
    public function select_tipos_modulo($idModulo) {
        $tipo = DB::select("SEP_TipoDoc.idTipoDoc", "SEP_TipoDoc.idModulo", "SEP_TipoDoc.Nome", "SEP_TipoDoc.Alias", "SEP_TipoDoc.Status", "SEP_TipoDoc.idUsuario", "SEP_TipoDoc.dtCadastro", "SEP_TipoDoc.dtAlteracao", array('SEP_Modulo.Nome', 'nmModulo'))
                ->from('SEP_TipoDoc')
                ->join('SEP_Modulo', 'LEFT')
                ->on('SEP_Modulo.idModulo', '=', 'SEP_TipoDoc.idModulo')
                ->where('SEP_TipoDoc.idModulo', '=', $idModulo);
                if ($_SESSION['Perfil'] !== 'Administrador') {
                    $tipo->and_where('SEP_TipoDoc.idTipoSetor', '=', $_SESSION['idSetor']);
                }
                return $tipo->order_by('SEP_TipoDoc.Nome', 'ASC')->execute($this->_db);
    }
    
    public function select_tipos_ativos($Status) {
        $tipo = DB::select("idTipoDoc", "idModulo", "Nome", "Alias", "Status", "idUsuario", "dtCadastro", "dtAlteracao")
                ->from('SEP_TipoDoc')
                ->where('SEP_TipoDoc.Status', '=', $Status);
                if ($_SESSION['Perfil'] !== 'Administrador') {
                    $tipo->where('SEP_TipoDoc.idTipoSetor', '=', $_SESSION['idSetor']);
                }
                return $tipo->order_by('SEP_TipoDoc.Nome', 'ASC')->execute($this->_db);
    }

    public function insert_tipo($idModulo, $Nome, $Alias, $Status) {
        $colunas = array(
            'idModulo',
            'Nome',
            'Alias',
            'dtCadastro',
            'Status',
            'idUsuario',
            'idTipoSetor'
        );
        $values = array(
            $idModulo,
            $Nome,
            $Alias,
            date('yy-m-d'),
            $Status,
            $_SESSION['idUsuario'],
            $_SESSION['idSetor']
        );
        $returned_id = DB::insert("SEP_TipoDoc", $colunas)
                ->values($values)
                ->execute($this->_db)
        ;
        return $returned_id;
    }

    public function update_tipo($idTipoDoc, $idModulo, $Nome, $Alias, $Status) {
        $state = DB::update('SEP_TipoDoc')
                ->set(array('idModulo' => $idModulo))
                ->set(array('Nome' => $Nome))
                ->set(array('Alias' => $Alias))
                ->set(array('Status' => $Status))
                ->set(array('idUsuario' => $_SESSION['idUsuario']))
                ->set(array('dtAlteracao' => date('yy-m-d')))
                ->where('idTipoDoc', '=', $idTipoDoc)
                ->execute($this->_db)
        ;
        return $state;
    }

    public function delete_tipo($idTipoDoc) {
        $returned_id = DB::delete('SEP_TipoDoc')
                ->where('idTipoDoc', '=', $idTipoDoc)
                ->execute($this->_db);
        return $returned_id;
    }

    public function update_ativar_tipo($idTipoDoc) {

        $tipos = DB::select('SEP_TipoDoc.Status')
                ->from('SEP_TipoDoc')
                ->where('idTipoDoc', '=', $idTipoDoc)
                ->execute($this->_db)
        ;

        if ($tipos[0]['Status'] == '1') {
            $pairs = array('Status' => '0',);
        } else {
            $pairs = array('Status' => '1',);
        }

        $state = DB::update('SEP_TipoDoc')
                ->set($pairs)
                ->where('idTipoDoc', '=', $idTipoDoc)
                ->execute($this->_db)
        ;
        return $state;
    }
    
}
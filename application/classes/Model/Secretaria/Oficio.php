<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Secretaria_Oficio extends Model {

    /**
     * Construtor da classe, já seta a conexao com o banco de dados
     *
     * @param string $con_type
     */
    public function __construct($db = NULL) {
        if ($db != NULL) {
            // Set the instance or name
            $this->_db = $db;
        } else {
            // Use the default name
            $this->_db = Database::$default;
        }

        if (is_string($this->_db)) {
            // Load the database
            $this->_db = Database::instance($this->_db);
        }
    }

    public function select_oficios($Pesquisa) {
        $oficio = DB::select
                        ('SEP_Oficio.idOficio', 'SEP_Oficio.idSetor', 'SEP_Oficio.Numero', 'SEP_Oficio.AnoCorrente', 'SEP_Oficio.Assunto', 'SEP_Oficio.dtEmissao',
                'SEP_Oficio.Obs', 'SEP_Oficio.idUsuario', 'SEP_Oficio.dtCadastro', 'SEP_Oficio.dtAlteracao', 'SEP_Oficio.Status', PREFIX.TABLELOGIN.'LOGIN_TipoSetor.Setor')
                ->from('SEP_Oficio')
                ->join(PREFIX.TABLELOGIN.'LOGIN_TipoSetor', 'LEFT')
                ->on(PREFIX.TABLELOGIN.'LOGIN_TipoSetor.idTipoSetor', '=', 'SEP_Oficio.idSetor');
                if ($_SESSION['Perfil'] !== 'Administrador') {
                    $oficio->where('SEP_Oficio.idTipoSetor', '=', $_SESSION['idSetor']);
                }
                if ($Pesquisa !== '') {   
                    $oficio->and_where('SEP_Oficio.Assunto', 'LIKE', '%'.$Pesquisa.'%')->execute($this->_db);                            
                }
                return $oficio->order_by('SEP_Oficio.idOficio', 'DESC')->execute($this->_db);
    }

    public function select_oficio($idOficio) {
        $oficio = DB::select
                        ('SEP_Oficio.idOficio', 'SEP_Oficio.idSetor', 'SEP_Oficio.Numero', 'SEP_Oficio.AnoCorrente', 'SEP_Oficio.Assunto', 'SEP_Oficio.dtEmissao',
                'SEP_Oficio.Obs', 'SEP_Oficio.idUsuario', 'SEP_Oficio.dtCadastro', 'SEP_Oficio.dtAlteracao', 'SEP_Oficio.Status')
                ->from('SEP_Oficio')
                ->where('SEP_Oficio.idOficio', '=', $idOficio)
                ->execute($this->_db);
        return $oficio;
    }
    
    public function select_oficios_ano($ano) {
        $oficio = DB::select
                        ('SEP_Oficio.idOficio', 'SEP_Oficio.idSetor', 'SEP_Oficio.Numero', 'SEP_Oficio.AnoCorrente', 'SEP_Oficio.Assunto', 'SEP_Oficio.dtEmissao',
                'SEP_Oficio.Obs', 'SEP_Oficio.idUsuario', 'SEP_Oficio.dtCadastro', 'SEP_Oficio.dtAlteracao', 'SEP_Oficio.Status', PREFIX.TABLELOGIN.'LOGIN_TipoSetor.Setor')
                ->from('SEP_Oficio')
                ->join(PREFIX.TABLELOGIN.'LOGIN_TipoSetor', 'LEFT')
                ->on(PREFIX.TABLELOGIN.'LOGIN_TipoSetor.idTipoSetor', '=', 'SEP_Oficio.idSetor')
                ->where('SEP_Oficio.AnoCorrente', '=', $ano);
                if ($_SESSION['Perfil'] !== 'Administrador') {
                    $oficio->where('SEP_Oficio.idTipoSetor', '=', $_SESSION['idSetor']);
                }
                return $oficio->order_by('SEP_Oficio.idOficio', 'DESC')->execute($this->_db);
    }

    public function insert_oficio($vars, $Emissao) {
        $colunas = array(
            'idSetor',
            'Numero',
            'AnoCorrente',
            'Assunto',
            'dtEmissao',
            'Obs',
            'idUsuario',
            'idTipoSetor',
            'dtCadastro',
            'Status'
        );
        $values = array(
            $vars['idSetor'],
            $vars['Numero'],
            $vars['AnoCorrente'],
            $vars['Assunto'],
            $Emissao,
            $vars['Obs'],
            $_SESSION['idUsuario'],
            $_SESSION['idSetor'],
            date('yy-m-d'),
            $vars['Status']
        );
        $returned_id = DB::insert("SEP_Oficio", $colunas)
                ->values($values)
                ->execute($this->_db)
        ;
        return $returned_id;
    }
    
    public function update_oficio($idOficio, $vars, $Emissao) {
        $state = DB::update('SEP_Oficio')
                ->set(array('idSetor' => $vars['idSetor']))
                ->set(array('Numero' => $vars['Numero']))
                ->set(array('AnoCorrente' => $vars['AnoCorrente']))
                ->set(array('Assunto' => $vars['Assunto']))
                ->set(array('dtEmissao' => $Emissao))
                ->set(array('Obs' => $vars['Obs']))
                ->set(array('idUsuario' => $_SESSION['idUsuario']))
                ->set(array('idTipoSetor' => $_SESSION['idSetor']))
                ->set(array('dtAlteracao' => date('yy-m-d')))
                ->set(array('Status' => $vars['Status']))
                ->where('idOficio', '=', $idOficio)
                ->execute($this->_db)
        ;
        return $state;
    }

    public function delete_oficio($idOficio) {
        $Oficio = DB::delete('SEP_Oficio')
                ->where('idOficio', '=', $idOficio)
                ->execute($this->_db);
        return $Oficio;
    }

    public function update_ativar_oficio($idOficio) {
        $oficios = DB::select('SEP_Oficio.Status')
                ->from('SEP_Oficio')
                ->where('idOficio', '=', $idOficio)
                ->execute($this->_db)
        ;

        if ($oficios[0]['Status'] == '1') {
            $pairs = array('Status' => '0',);
        } else {
            $pairs = array('Status' => '1',);
        }

        $state = DB::update('SEP_Oficio')
                ->set($pairs)
                ->where('SEP_Oficio.idOficio', '=', $idOficio)
                ->execute($this->_db)
        ;
        return $state;
    }

}

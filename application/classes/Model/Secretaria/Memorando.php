<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Secretaria_Memorando extends Model {

    /**
     * Construtor da classe, já seta a conexao com o banco de dados
     *
     * @param string $con_type
     */
    public function __construct($db = NULL) {
        if ($db != NULL) {
            // Set the instance or name
            $this->_db = $db;
        } else {
            // Use the default name
            $this->_db = Database::$default;
        }

        if (is_string($this->_db)) {
            // Load the database
            $this->_db = Database::instance($this->_db);
        }
    }

    public function select_memorandos($Pesquisa) {
        $memorando = DB::select
                        ('SEP_Memorando.idMemorando', 'SEP_Memorando.idSetor', 'SEP_Memorando.Numero', 'SEP_Memorando.AnoCorrente', 'SEP_Memorando.Assunto', 'SEP_Memorando.dtEmissao',
                'SEP_Memorando.Obs', 'SEP_Memorando.idUsuario', 'SEP_Memorando.dtCadastro', 'SEP_Memorando.dtAlteracao', 'SEP_Memorando.Status', PREFIX.TABLELOGIN.'LOGIN_TipoSetor.Setor')
                ->from('SEP_Memorando')
                ->join(PREFIX.TABLELOGIN.'LOGIN_TipoSetor', 'LEFT')
                ->on(PREFIX.TABLELOGIN.'LOGIN_TipoSetor.idTipoSetor', '=', 'SEP_Memorando.idSetor');
                if ($_SESSION['Perfil'] !== 'Administrador') {
                    $memorando->where('SEP_Memorando.idTipoSetor', '=', $_SESSION['idSetor']);
                }
                if ($Pesquisa !== '') {   
                    $memorando->and_where('SEP_Memorando.Assunto', 'LIKE', '%'.$Pesquisa.'%')->execute($this->_db);                            
                }
                return $memorando->order_by('SEP_Memorando.idMemorando', 'DESC')->execute($this->_db);
    }

    public function select_memorando($idMemorando) {
        $memorando = DB::select
                        ('SEP_Memorando.idMemorando', 'SEP_Memorando.idSetor', 'SEP_Memorando.Numero', 'SEP_Memorando.AnoCorrente', 'SEP_Memorando.Assunto', 'SEP_Memorando.dtEmissao',
                'SEP_Memorando.Obs', 'SEP_Memorando.idUsuario', 'SEP_Memorando.dtCadastro', 'SEP_Memorando.dtAlteracao', 'SEP_Memorando.Status')
                ->from('SEP_Memorando')
                ->where('SEP_Memorando.idMemorando', '=', $idMemorando)
                ->execute($this->_db);
        return $memorando;
    }
    
    public function select_memorandos_ano($ano) {
        $memorando = DB::select
                        ('SEP_Memorando.idMemorando', 'SEP_Memorando.idSetor', 'SEP_Memorando.Numero', 'SEP_Memorando.AnoCorrente', 'SEP_Memorando.Assunto', 'SEP_Memorando.dtEmissao',
                'SEP_Memorando.Obs', 'SEP_Memorando.idUsuario', 'SEP_Memorando.dtCadastro', 'SEP_Memorando.dtAlteracao', 'SEP_Memorando.Status', PREFIX.TABLELOGIN.'LOGIN_TipoSetor.Setor')
                ->from('SEP_Memorando')
                ->join(PREFIX.TABLELOGIN.'LOGIN_TipoSetor', 'LEFT')
                ->on(PREFIX.TABLELOGIN.'LOGIN_TipoSetor.idTipoSetor', '=', 'SEP_Memorando.idSetor')
                ->where('SEP_Memorando.AnoCorrente', '=', $ano);
                if ($_SESSION['Perfil'] !== 'Administrador') {
                    $memorando->where('SEP_Memorando.idTipoSetor', '=', $_SESSION['idSetor']);
                }
                return $memorando->order_by('SEP_Memorando.idMemorando', 'DESC')->execute($this->_db);
    }

    public function insert_memorando($vars, $Emissao) {
        $colunas = array(
            'idSetor',
            'Numero',
            'AnoCorrente',
            'Assunto',
            'dtEmissao',
            'Obs',
            'idUsuario',
            'idTipoSetor',
            'dtCadastro',
            'Status'
        );
        $values = array(
            $vars['idSetor'],
            $vars['Numero'],
            $vars['AnoCorrente'],
            $vars['Assunto'],
            $Emissao,
            $vars['Obs'],
            $_SESSION['idUsuario'],
            $_SESSION['idSetor'],
            date('yy-m-d'),
            $vars['Status']
        );
        $returned_id = DB::insert("SEP_Memorando", $colunas)
                ->values($values)
                ->execute($this->_db)
        ;
        return $returned_id;
    }
    
    public function update_memorando($idMemorando, $vars, $Emissao) {
        $state = DB::update('SEP_Memorando')
                ->set(array('idSetor' => $vars['idSetor']))
                ->set(array('Numero' => $vars['Numero']))
                ->set(array('AnoCorrente' => $vars['AnoCorrente']))
                ->set(array('Assunto' => $vars['Assunto']))
                ->set(array('dtEmissao' => $Emissao))
                ->set(array('Obs' => $vars['Obs']))
                ->set(array('idUsuario' => $_SESSION['idUsuario']))
                ->set(array('idTipoSetor' => $_SESSION['idSetor']))
                ->set(array('dtAlteracao' => date('yy-m-d')))
                ->set(array('Status' => $vars['Status']))
                ->where('idMemorando', '=', $idMemorando)
                ->execute($this->_db)
        ;
        return $state;
    }

    public function delete_memorando($idMemorando) {
        $Memorando = DB::delete('SEP_Memorando')
                ->where('idMemorando', '=', $idMemorando)
                ->execute($this->_db);
        return $Memorando;
    }

    public function update_ativar_memorando($idMemorando) {
        $memorandos = DB::select('SEP_Memorando.Status')
                ->from('SEP_Memorando')
                ->where('idMemorando', '=', $idMemorando)
                ->execute($this->_db)
        ;

        if ($memorandos[0]['Status'] == '1') {
            $pairs = array('Status' => '0',);
        } else {
            $pairs = array('Status' => '1',);
        }

        $state = DB::update('SEP_Memorando')
                ->set($pairs)
                ->where('SEP_Memorando.idMemorando', '=', $idMemorando)
                ->execute($this->_db)
        ;
        return $state;
    }

}

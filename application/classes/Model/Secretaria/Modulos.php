<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Secretaria_Modulos extends Model {

    /**
     * Construtor da classe, já seta a conexao com o banco de dados
     *
     * @param string $con_type
     */
    public function __construct($db = NULL) {
        if ($db != NULL) {
            // Set the instance or name
            $this->_db = $db;
        } else {
            // Use the default name
            $this->_db = Database::$default;
        }

        if (is_string($this->_db)) {
            // Load the database
            $this->_db = Database::instance($this->_db);
        }
    }

    public function select_modulo($idModulo) {
        $modulo = DB::select("idModulo", "Nome", "Alias", "Status", "idUsuario", "dtCadastro", "dtAlteracao")
                ->from('SEP_Modulo')
                ->where('SEP_Modulo.idModulo', '=', $idModulo)                
                ->execute($this->_db)
        ;
        return $modulo;
    }
    
    public function select_modulo_alias($Alias) {

        $modulo = DB::select("idModulo", "Nome", "Alias", "Status", "idUsuario", "dtCadastro", "dtAlteracao")
                ->from('SEP_Modulo')
                ->where('SEP_Modulo.Alias', '=', $Alias)
                ->execute($this->_db)
        ;
        return $modulo;
    }

    public function select_modulos() {
        $modulo = DB::select("idModulo", "Nome", "Alias", "Status", "idUsuario", "dtCadastro", "dtAlteracao")
                ->from('SEP_Modulo');
                if ($_SESSION['Perfil'] !== 'Administrador') {
                    $modulo->and_where('idTipoSetor', '=', $_SESSION['idSetor']);
                }
                return $modulo->order_by('idModulo', 'DESC')->execute($this->_db);
    }
    
    public function select_modulos_ativos($Status) {
        $modulo = DB::select("idModulo", "Nome", "Alias", "Status", "idUsuario", "dtCadastro", "dtAlteracao")
                ->from('SEP_Modulo')
                ->where('SEP_Modulo.Status', '=', $Status);
                if ($_SESSION['Perfil'] !== 'Administrador') {
                    $modulo->where('SEP_Modulo.idTipoSetor', '=', $_SESSION['idSetor']);
                }
                return $modulo->order_by('SEP_Modulo.Nome', 'ASC')->execute($this->_db);
    }

    public function insert_modulo($Nome, $Alias, $Status) {
        $colunas = array(
            'Nome',
            'Alias',
            'dtCadastro',
            'Status',
            'idUsuario',
            'idTipoSetor'
        );
        $values = array(
            $Nome,
            $Alias,
            date('yy-m-d'),
            $Status,
            $_SESSION['idUsuario'],
            $_SESSION['idSetor']
        );
        $returned_id = DB::insert("SEP_Modulo", $colunas)
                ->values($values)
                ->execute($this->_db)
        ;
        return $returned_id;
    }

    public function update_modulo($idModulo, $Nome, $Alias, $Status) {
        $state = DB::update('SEP_Modulo')
                ->set(array('Nome' => $Nome))
                ->set(array('Alias' => $Alias))
                ->set(array('Status' => $Status))
                ->set(array('idUsuario' => $_SESSION['idUsuario']))
                ->set(array('dtAlteracao' => date('yy-m-d')))
                ->where('idModulo', '=', $idModulo)
                ->execute($this->_db)
        ;
        return $state;
    }

    public function delete_modulo($idModulo) {
        $returned_id = DB::delete('SEP_Modulo')
                ->where('idModulo', '=', $idModulo)
                ->execute($this->_db);
        return $returned_id;
    }

    public function update_ativar_modulo($idModulo) {

        $modulos = DB::select('SEP_Modulo.Status')
                ->from('SEP_Modulo')
                ->where('idModulo', '=', $idModulo)
                ->execute($this->_db)
        ;

        if ($modulos[0]['Status'] == '1') {
            $pairs = array('Status' => '0',);
        } else {
            $pairs = array('Status' => '1',);
        }

        $state = DB::update('SEP_Modulo')
                ->set($pairs)
                ->where('idModulo', '=', $idModulo)
                ->execute($this->_db)
        ;
        return $state;
    }
    
}
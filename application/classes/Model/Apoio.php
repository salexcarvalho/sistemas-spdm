<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Apoio extends Model {

    /**
     * Construtor da classe, já seta a conexao com o banco de dados
     *
     * @param string $con_type
     */
    public function __construct($db = NULL) {
        if ($db != NULL) {
            // Set the instance or name
            $this->_db = $db;
        } else {
            // Use the default name
            $this->_db = Database::$default;
        }

        if (is_string($this->_db)) {
            // Load the database
            $this->_db = Database::instance($this->_db);
        }
    }

    public function select_estado() {
        $estado = DB::select('id', 'nome', 'uf')
                ->from('APOIO_Estado')
                ->execute($this->_db)
        ;
        return $estado;
    }
    
    public function select_cidade() {
        $cidade = DB::select('id', 'nome', 'estado')
                ->from('APOIO_Cidade')
                ->execute($this->_db)
        ;
        return $cidade;
    }
    
    public function select_busca_cidade($query) {
        $cidade = DB::select('id', 'nome', 'estado')
                ->from('APOIO_Cidade')
                ->where('nome', 'like' , '%'.$query.'%')
                ->execute($this->_db)
        ;
        return $cidade;
    }
    
    public function select_busca_estado($query) {
        $estado = DB::select('id', 'nome', 'uf')
                ->from('APOIO_Estado')
                ->where('nome', 'like' , '%'.$query.'%')
                ->execute($this->_db)
        ;
        return $estado;
    }

}

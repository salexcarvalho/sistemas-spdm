<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Sec_Assinatura extends Model {

    /**
     * Construtor da classe, já seta a conexao com o banco de dados
     *
     * @param string $con_type
     */
    public function __construct($db = NULL) {
        if ($db != NULL) {
            // Set the instance or name
            $this->_db = $db;
        } else {
            // Use the default name
            $this->_db = Database::$default;
        }

        if (is_string($this->_db)) {
            // Load the database
            $this->_db = Database::instance($this->_db);
        }
    }

    public function select_assinaturas() {
        $assinaturas = DB::select('SEC_Assinaturas.idAssinaturas', 'SEC_Assinaturas.titulacao', 'SEC_Assinaturas.nome', 'SEC_Assinaturas.cargo', 'SEC_Assinaturas.PngAssinatura', 'SEC_Assinaturas.status')
                ->from('SEC_Assinaturas');
                if ($_SESSION['Perfil'] !== 'Administrador') {
                    $assinaturas->where('SEC_Assinaturas.idTipoSetor', '=', $_SESSION['idSetor'])
                            ->and_where('status', '=', 1);                    
                }
                return $assinaturas->order_by('SEC_Assinaturas.idAssinaturas', 'DESC')->execute($this->_db);
    }

    public function select_assinatura($idAssinaturas) {
        $assinatura = DB::select('SEC_Assinaturas.idAssinaturas', 'SEC_Assinaturas.titulacao', 'SEC_Assinaturas.nome', 'SEC_Assinaturas.cargo', 'SEC_Assinaturas.PngAssinatura', 'SEC_Assinaturas.status')
                ->from('SEC_Assinaturas')
                ->where('SEC_Assinaturas.idAssinaturas', '=', $idAssinaturas)
                ->execute($this->_db);
        return $assinatura;
    }

    public function select_quantidade_assinaturas() {
        return $this->select_assinaturas()->count();
    }

    public function insert_assinatura($titulacao, $nome, $cargo, $PngAssinatura_name, $status) {
        $returned_id = DB::insert("SEC_Assinaturas", array("titulacao", "nome", "cargo", "PngAssinatura", "status", "idUsuario", "idTipoSetor", "dtCriacao"))
                ->values(array($titulacao, $nome, $cargo, $PngAssinatura_name, $status, $_SESSION['idUsuario'], $_SESSION['idSetor'], date('Y-m-d')))
                ->execute($this->_db)
        ;
        return $returned_id;
    }

    public function update_assinatura($idAssinaturas, $titulacao, $nome, $cargo, $PngAssinatura_hidenn, $PngAssinatura_name, $status) {
        if ($PngAssinatura_name == date('dmY') . '_' || empty($PngAssinatura_name)) {
            $PngAssinatura_name = $PngAssinatura_hidenn;
        }

        $returned_id = DB::update('SEC_Assinaturas')
                ->set(array('titulacao' => $titulacao))
                ->set(array('nome' => $nome))
                ->set(array('cargo' => $cargo))
                ->set(array('PngAssinatura' => $PngAssinatura_name))
                ->set(array('status' => $status))
                ->set(array('idUsuario' => $_SESSION['idUsuario']))
                ->set(array('dtAlteracao' => date('Y-m-d')))
                ->where('idAssinaturas', '=', $idAssinaturas)
                ->execute($this->_db)
        ;
        return $returned_id;
    }

    public function delete_assinatura($idAssinaturas) {
        $Assinatura = DB::delete('SEC_Assinaturas')
                ->where('idAssinaturas', '=', $idAssinaturas)
                ->execute($this->_db)
        ;
        return $Assinatura;
    }

    public function update_ativar_assinatura($idAssinaturas) {
        $assinatura = DB::select('SEC_Assinaturas.status')
                ->from('SEC_Assinaturas')
                ->where('idAssinaturas', '=', $idAssinaturas)
                ->execute($this->_db)
        ;

        if ($assinatura[0]['status'] == '1') {
            $pairs = array('status' => '0',);
        } else {
            $pairs = array('status' => '1',);
        }

        $state = DB::update('SEC_Assinaturas')
                ->set($pairs)
                ->where('idAssinaturas', '=', $idAssinaturas)
                ->execute($this->_db)
        ;
        return $state;
    }

}

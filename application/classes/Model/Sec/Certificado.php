<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Sec_Certificado extends Model {

    /**
     * Construtor da classe, já seta a conexao com o banco de dados
     *
     * @param string $con_type
     */
    public function __construct($db = NULL) {
        if ($db != NULL) {
            // Set the instance or name
            $this->_db = $db;
        } else {
            // Use the default name
            $this->_db = Database::$default;
        }

        if (is_string($this->_db)) {
            // Load the database
            $this->_db = Database::instance($this->_db);
        }
    }

    public function select_certificados() {
        $certificados = DB::select('idCertificados', 'idOrigem', 'fk_idAssinatura_1', 'fk_idAssinatura_2', 'fk_idAssinatura_3', 'fk_idAssinatura_4', 'status', 'eventoNome', 'localEvento', 'dataEmissao', 'horaEmissao', 'cargaHoraria', 'TextoPadrao', 'imgCertificado', 'idUsuario', 'dtCriacao ', 'dtAlteracao')
                ->from('SEC_Certificados');
                if ($_SESSION['Perfil'] !== 'Administrador') {
                    $certificados->where('SEC_Certificados.idTipoSetor', '=', $_SESSION['idSetor']);
                }
                return $certificados->order_by('idCertificados', 'DESC')->execute($this->_db);
    }

    public function select_certificado($idCertificados) {
        $certificado = DB::select('idCertificados', 'idOrigem', 'fk_idAssinatura_1', 'fk_idAssinatura_2', 'fk_idAssinatura_3', 'fk_idAssinatura_4', 'status', 'eventoNome', 'localEvento', 'dataEmissao', 'horaEmissao', 'cargaHoraria', 'TextoPadrao', 'imgCertificado', 'idUsuario', 'dtCriacao ', 'dtAlteracao')
                ->from('SEC_Certificados')
                ->where('idCertificados', '=', $idCertificados)
                ->execute($this->_db);

        return $certificado;
    }

    public function select_certificado_auth($eventoNome) {

        $certificado = DB::select('idCertificados', 'idOrigem', 'fk_idAssinatura_1', 'fk_idAssinatura_2', 'fk_idAssinatura_3', 'fk_idAssinatura_4', 'status', 'eventoNome', 'localEvento', 'dataEmissao', 'horaEmissao', 'cargaHoraria', 'TextoPadrao', 'imgCertificado', 'idUsuario', 'dtCriacao ', 'dtAlteracao')
                ->from('SEC_Certificados')
                ->where('auth', '=', $eventoNome)
                ->execute($this->_db);
        return $certificado;
    }

    public function select_quantidade_modelos() {
        return $this->select_certificados()->count();
    }

    public function select_tipocertificados() {
        $certificados = DB::select('idTipoCertificado', 'Nome_Certificado')
                ->from('SEC_TipoCertificado')
                ->execute($this->_db)
        ;

        return $certificados;
    }

    public function update_ativar_certificado($idCertificados) {
        $certificado = DB::select('Status')
                ->from('SEC_Certificados')
                ->where('idCertificados', '=', $idCertificados)
                ->execute($this->_db)
        ;

        if ($certificado[0]['Status'] == '1') {
            $pairs = array(
                'Status' => '0',
            );
        } else {
            $pairs = array(
                'Status' => '1',
            );
        }

        $state = DB::update('SEC_Certificados')
                ->set($pairs)
                ->where('idCertificados', '=', $idCertificados)
                ->execute($this->_db)
        ;
        return $state;
    }

    public function update_imprime_certificado($idCertificados) {
        $pairs = array('Impresso' => '1',);

        $state = DB::update('SEC_Apoio')
                ->set($pairs)
                ->where('idCertificado', '=', $idCertificados)
                ->execute($this->_db)
        ;
        return $state;
    }

    public function gerar_certificado($idApoio, $idCertificados, $Autenticacao) {
        $gerarCertificado = DB::update('SEC_Apoio')
                ->set(array('idCertificado' => $idCertificados))
                ->set(array('Autenticacao' => $Autenticacao))
                ->set(array('idUsuario' => $_SESSION['idUsuario']))
                ->set(array('dtAlteracao' => date('yy-m-d')))
                ->where('idApoio', '=', $idApoio)
                ->execute($this->_db);
        return $gerarCertificado;
    }

    public function excluir_certificado($idApoio) {
        $excluirCertificado = DB::update('SEC_Apoio')
                ->set(array('idCertificado' => null))
                ->set(array('Autenticacao' => null))
                ->set(array('idUsuario' => $_SESSION['idUsuario']))
                ->set(array('dtAlteracao' => date('yy-m-d')))
                ->where('idApoio', '=', $idApoio)
                ->execute($this->_db);
        return $excluirCertificado;
    }

    public function verificar_status($autenticacao) {
        $status = DB::select('status')
                ->from('SEC_Apoio')
                ->where('Autenticacao', '=', $autenticacao)
                ->execute($this->_db);
        return $status;
    }

    public function insert_modelo($idOrigem, $idAssinatura1, $idAssinatura2, $idAssinatura3, $idAssinatura4, $nomeEvento, $local, $data, $hora, $c_horaria, $msg_cert, $img_Cert) {
        if ($img_Cert == "") {
            if($idOrigem==9){
                $img_Cert = "certificados_padrao.png";
            }else{
                $img_Cert = "200319_padrao_fpcs_2019.png";
            }
        }
        $returned_id = DB::insert("SEC_Certificados", array("idOrigem", "fk_idAssinatura_1", "fk_idAssinatura_2", "fk_idAssinatura_3", "fk_idAssinatura_4", "eventoNome", "localEvento", "dataEmissao", "horaEmissao", "cargaHoraria", "TextoPadrao", "imgCertificado", "idUsuario", "idTipoSetor", "auth"))
                ->values(array($idOrigem, $idAssinatura1, $idAssinatura2, $idAssinatura3, $idAssinatura4, $nomeEvento, $local, $data, $hora, $c_horaria, $msg_cert, $img_Cert, $_SESSION['idUsuario'], $_SESSION['idSetor'], md5($nomeEvento)))
                ->execute($this->_db)
        ;
        return $returned_id;
    }

    public function update_modelo($idCertificados, $idOrigem, $idAssinatura1, $idAssinatura2, $idAssinatura3, $idAssinatura4, $nmEvento, $Local, $dtEmissao, $Hora, $c_horaria, $msg_cert, $img_Cert_hidenn, $img_Cert_name) {
        if ($img_Cert_name == NULL || empty($img_Cert_name)) {
            $img_Cert_name = $img_Cert_hidenn;
        }

        $returned_id = DB::update('SEC_Certificados')
                ->set(array('idOrigem' => $idOrigem))
                ->set(array('fk_idAssinatura_1' => $idAssinatura1))
                ->set(array('fk_idAssinatura_2' => $idAssinatura2))
                ->set(array('fk_idAssinatura_3' => $idAssinatura3))
                ->set(array('fk_idAssinatura_4' => $idAssinatura4))
                ->set(array('eventoNome' => $nmEvento))
                ->set(array('localEvento' => $Local))
                ->set(array('dataEmissao' => $dtEmissao))
                ->set(array('horaEmissao' => $Hora))
                ->set(array('cargaHoraria' => $c_horaria))
                ->set(array('TextoPadrao' => $msg_cert))
                ->set(array('imgCertificado' => $img_Cert_name))
                ->set(array('idUsuario' => $_SESSION['idUsuario']))
                ->set(array('dtAlteracao' => date('yy-m-d')))
                ->set(array('auth' => md5($nmEvento)))
                ->where('idCertificados', '=', $idCertificados)
                ->execute($this->_db);
        return $returned_id;
    }

    public function delete_modelo($idCertificados) {

        $apoio = DB::delete('SEC_Apoio')
                ->where('idCertificado', '=', $idCertificados)
                ->execute($this->_db);

        $modelo = DB::delete('SEC_Certificados')
                ->where('idCertificados', '=', $idCertificados)
                ->execute($this->_db);

        return $modelo;
    }

}

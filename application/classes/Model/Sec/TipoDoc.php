<?php
defined('SYSPATH') or die('No direct script access.');

class Model_Sec_TipoDoc extends Model {

    /**
     * Construtor da classe, já seta a conexao com o banco de dados
     *
     * @param string $con_type
     */
    public function __construct($db = NULL) {
        if ($db != NULL) {
            // Set the instance or name
            $this->_db = $db;
        } else {
            // Use the default name
            $this->_db = Database::$default;
        }

        if (is_string($this->_db)) {
            // Load the database
            $this->_db = Database::instance($this->_db);
        }
    }

    public function select_tipoDoc($idTipoDoc) {
        $tipoDoc = DB::select("idTipoDoc", "Nome", "Alias", "Status", "idUsuario", "dtCriacao", "dtAlteracao")
                ->from('SEC_TipoDoc')
                ->where('SEC_TipoDoc.idTipoDoc', '=', $idTipoDoc)                
                ->execute($this->_db)
        ;
        return $tipoDoc;
    }

    public function select_tipoDoc_alias($Alias) {

        $tipoDoc = DB::select("idTipoDoc", "Nome", "Alias", "Status", "idUsuario", "dtCriacao", "dtAlteracao")
                ->from('SEC_TipoDoc')
                ->where('SEC_TipoDoc.Alias', '=', $Alias)
                ->execute($this->_db)
        ;

        return $tipoDoc;
    }

    public function select_tipoDocs() {
        $tipoDoc = DB::select("idTipoDoc", "Nome", "Alias", "Status", "idUsuario", "dtCriacao", "dtAlteracao")
                ->from('SEC_TipoDoc')
                ->order_by('SEC_TipoDoc.idTipoDoc', 'DESC')
                ->execute($this->_db)
        ;
        return $tipoDoc;
    }

    public function select_quantidade() {
        return $this->select_tipoDocs()->count();
    }

    public function insert_tipoDoc($Nome, $alias, $status) {
        $colunas = array(
            'Nome',
            'Alias',
            'Status',
            'idUsuario'
        );
        $values = array(
            $Nome,
            $alias,
            $status,
            $_SESSION['idUsuario']
        );
        $returned_id = DB::insert("SEC_TipoDoc", $colunas)
                ->values($values)
                ->execute($this->_db)
        ;
        return $returned_id;
    }

    public function update_tipoDoc($idTipoDoc, $nmTipo, $alias, $Status) {
        $state = DB::update('SEC_TipoDoc')
                ->set(array('nome' => $nmTipo))
                ->set(array('Alias' => $alias))
                ->set(array('Status' => $Status))
                ->set(array('idUsuario' => $_SESSION['idUsuario']))
                ->set(array('dtAlteracao' => date('yy-m-d')))
                ->where('idTipoDoc', '=', $idTipoDoc)
                ->execute($this->_db)
        ;
        return $state;
    }

    public function delete_tipoDoc($idTipoDoc) {
        $returned_id = DB::delete('SEC_TipoDoc')
                ->where('idTipoDoc', '=', $idTipoDoc)
                ->execute($this->_db);
        return $returned_id;
    }

    public function update_ativar_tipoDoc($idTipoDoc) {

        $tipoDoc = DB::select('SEC_TipoDoc.Status')
                ->from('SEC_TipoDoc')
                ->where('idTipoDoc', '=', $idTipoDoc)
                ->execute($this->_db)
        ;

        if ($tipoDoc[0]['Status'] == '1') {
            $pairs = array('Status' => '0',);
        } else {
            $pairs = array('Status' => '1',);
        }

        $state = DB::update('SEC_TipoDoc')
                ->set($pairs)
                ->where('idTipoDoc', '=', $idTipoDoc)
                ->execute($this->_db)
        ;
        return $state;
    }

}

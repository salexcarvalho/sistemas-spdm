<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Sec_Origem extends Model {

    /**
     * Construtor da classe, já seta a conexao com o banco de dados
     *
     * @param string $con_type
     */
    public function __construct($db = NULL) {
        if ($db != NULL) {
            // Set the instance or name
            $this->_db = $db;
        } else {
            // Use the default name
            $this->_db = Database::$default;
        }

        if (is_string($this->_db)) {
            // Load the database
        }
        $this->_db = Database::instance($this->_db);
    }

    public function select_origens() {
        $origem = DB::select('SEC_Origem.idOrigem', 'SEC_Origem.Nome', 'SEC_Origem.Alias', 'SEC_Origem.Status')
                ->from('SEC_Origem');
                if ($_SESSION['Perfil'] !== 'Administrador') {
                    $origem->where('SEC_Origem.idTipoSetor', '=', $_SESSION['idSetor']);
                }
                return $origem->order_by('SEC_Origem.idOrigem', 'DESC')->execute($this->_db);
    }

    public function select_origem($idOrigem) {
        $origem = DB::select('SEC_Origem.idOrigem', 'SEC_Origem.Nome', 'SEC_Origem.Alias', 'SEC_Origem.Status')
                ->from('SEC_Origem')
                ->where('SEC_Origem.idOrigem', '=', $idOrigem)
                ->execute($this->_db)
        ;
        return $origem;
    }

    public function select_origem_nome($nome) {
        $origem = DB::select('SEC_Origem.idOrigem', 'SEC_Origem.Nome', 'SEC_Origem.Alias', 'SEC_Origem.Status')
                ->from('SEC_Origem')
                ->where('SEC_Origem.Nome', '=', $nome)
                ->execute($this->_db)
        ;
        return $origem;
    }

    public function insert_origem($nmOrigem, $aliasOrigem, $status) {
        $returned_id = DB::insert("SEC_Origem", array("Nome", 'Alias', 'Status', "idUsuario", "idTipoSetor"))
                ->values(array($nmOrigem, $aliasOrigem, $status, $_SESSION['idUsuario'], $_SESSION['idSetor']))
                ->execute($this->_db)
        ;
        return $returned_id;
    }

    public function update_ativarOrigem($idOrigem) {
        $participante = DB::select('SEC_Origem.Status')
                ->from('SEC_Origem')
                ->where('idOrigem', '=', $idOrigem)
                ->execute($this->_db)
        ;

        if ($participante[0]['Status'] == '1') {
            $pairs = array('Status' => '0',);
        } else {
            $pairs = array('Status' => '1',);
        }

        $state = DB::update('SEC_Origem')
                ->set($pairs)
                ->where('idOrigem', '=', $idOrigem)
                ->execute($this->_db)
        ;
        return $state;
    }

    public function update_origem($idOrigem, $nmOrigem, $status, $aliasOrigem) {
        $state = DB::update('SEC_Origem')
                ->set(array('Nome' => $nmOrigem))
                ->set(array('Alias' => $aliasOrigem))
                ->set(array('Status' => $status))
                ->set(array('idUsuario' => $_SESSION['idUsuario']))
                ->set(array('dtAlteracao' => date('yy-m-d')))
                ->where('idOrigem', '=', $idOrigem)
                ->execute($this->_db)
        ;
        return $state;
    }

    public function delete_origem($idOrigem) {
        $origem = DB::delete('SEC_Origem')
                ->where('idOrigem', '=', $idOrigem)
                ->execute($this->_db)
        ;
        return $origem;
    }

}

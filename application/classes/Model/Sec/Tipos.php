<?php
defined('SYSPATH') or die('No direct script access.');

class Model_Sec_Tipos extends Model {

    /**
     * Construtor da classe, já seta a conexao com o banco de dados
     *
     * @param string $con_type
     */
    public function __construct($db = NULL) {
        if ($db != NULL) {
            // Set the instance or name
            $this->_db = $db;
        } else {
            // Use the default name
            $this->_db = Database::$default;
        }

        if (is_string($this->_db)) {
            // Load the database
            $this->_db = Database::instance($this->_db);
        }
    }

    public function select_tipo($idTipos) {
        $tipo = DB::select("idTipos", "Nome", "Alias", "Status", "idUsuario", "dtCriacao", "dtAlteracao")
                ->from('SEC_Tipos')
                ->where('SEC_Tipos.idTipos', '=', $idTipos)                
                ->execute($this->_db)
        ;
        return $tipo;
    }

    public function select_tipo_alias($Alias) {

        $tipo = DB::select("idTipos", "Nome", "Alias", "Status", "idUsuario", "dtCriacao", "dtAlteracao")
                ->from('SEC_Tipos')
                ->where('SEC_Tipos.Alias', '=', $Alias)
                ->execute($this->_db)
        ;

        return $tipo;
    }

    public function select_tipos() {
        $tipo = DB::select("idTipos", "Nome", "Alias", "Status", "idUsuario", "dtCriacao", "dtAlteracao")
                ->from('SEC_Tipos')
                ->order_by('SEC_Tipos.idTipos', 'DESC')
                ->execute($this->_db)
        ;
        return $tipo;
    }

    public function select_quantidade() {
        return $this->select_tipos()->count();
    }

    public function insert_tipo($Nome, $alias, $status) {
        $colunas = array(
            'Nome',
            'Alias',
            'Status',
            'idUsuario'
        );
        $values = array(
            $Nome,
            $alias,
            $status,
            $_SESSION['idUsuario']
        );
        $returned_id = DB::insert("SEC_Tipos", $colunas)
                ->values($values)
                ->execute($this->_db)
        ;
        return $returned_id;
    }

    public function update_tipo($idTipos, $nmTipo, $alias, $Status) {
        $state = DB::update('SEC_Tipos')
                ->set(array('nome' => $nmTipo))
                ->set(array('Alias' => $alias))
                ->set(array('Status' => $Status))
                ->set(array('idUsuario' => $_SESSION['idUsuario']))
                ->set(array('dtAlteracao' => date('yy-m-d')))
                ->where('idTipos', '=', $idTipos)
                ->execute($this->_db)
        ;
        return $state;
    }

    public function delete_tipo($idTipos) {
        $returned_id = DB::delete('SEC_Tipos')
                ->where('idTipos', '=', $idTipos)
                ->execute($this->_db);
        return $returned_id;
    }

    public function update_ativar_tipo($idTipos) {

        $tipos = DB::select('SEC_Tipos.Status')
                ->from('SEC_Tipos')
                ->where('idTipos', '=', $idTipos)
                ->execute($this->_db)
        ;

        if ($tipos[0]['Status'] == '1') {
            $pairs = array('Status' => '0',);
        } else {
            $pairs = array('Status' => '1',);
        }

        $state = DB::update('SEC_Tipos')
                ->set($pairs)
                ->where('idTipos', '=', $idTipos)
                ->execute($this->_db)
        ;
        return $state;
    }

}

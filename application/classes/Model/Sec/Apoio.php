<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Sec_Apoio extends Model {

    /**
     * Construtor da classe, já seta a conexao com o banco de dados
     *
     * @param string $con_type
     */
    public function __construct($db = NULL) {
        if ($db != NULL) {
            // Set the instance or name
            $this->_db = $db;
        } else {
            // Use the default name
            $this->_db = Database::$default;
        }

        if (is_string($this->_db)) {
            // Load the database
            $this->_db = Database::instance($this->_db);
        }
    }

    public function select_apoios() {
        $apoio = DB::select("idApoio, idParticipante, idCertificado, idOrigem, idTipo,modulo, tema, data, livre, Autenticacao, Impresso, Status, idUsuario, dtCriacao, dtAlteracao")
                ->from('SEC_Apoio')
                ->order_by('idApoio', 'DESC')
                ->execute($this->_db);
        return $apoio;
    }

    public function select_apoio_import($idParticipante, $idTipo, $idOrigem) {
        $apoio = DB::select("idApoio, idParticipante, idCertificado, idOrigem, idTipo,modulo, tema, data, livre, Autenticacao, Impresso, Status, idUsuario, dtCriacao, dtAlteracao")
                ->from('SEC_Apoio')
                ->where('idParticipante', '=', $idParticipante)
                ->and_where('idTipo', '=', $idTipo)
                ->and_where('idOrigem', '=', $idOrigem)
                ->and_where('idCertificado', 'IS', NULL)
                ->execute($this->_db);
        return $apoio;
    }

    public function select_apoio_certificados() {
        $apoio = DB::select("idApoio, idParticipante, idCertificado, idOrigem, idTipo,modulo, tema, data, livre, Autenticacao, Impresso, Status, idUsuario, dtCriacao, dtAlteracao")
                ->from('SEC_Apoio')
                ->where('idCertificado', 'IS NOT', NULL)
                ->and_where('idCertificado', '>', 0)
                ->order_by('idApoio', 'DESC')
                ->execute($this->_db);        
        return $apoio;
    }

    public function select_apoio($idApoio) {
        $apoio = DB::select("idApoio, idParticipante, idCertificado, idOrigem, idTipo,modulo, tema, data, livre, Autenticacao, Impresso, Status, idUsuario, dtCriacao, dtAlteracao")
                ->from('SEC_Apoio')
                ->where('idApoio', '=', $idApoio)
                ->execute($this->_db);
        return $apoio;
    }
    
    public function select_apoio_email($idApoio) {
        $apoio = DB::select("SEC_Apoio.autenticacao, SEC_Participantes.Nome, SEC_Participantes.Email, SEC_Certificados.eventoNome")
                ->from('SEC_Apoio')
                ->join('SEC_Participantes')
                ->on('SEC_Apoio.idParticipante', '=', 'SEC_Participantes.idParticipante')
                ->join('SEC_Certificados')
                ->on('SEC_Apoio.idCertificado', '=', 'SEC_Certificados.idCertificados')
                ->where('SEC_Apoio.idApoio', '=', $idApoio)
                ->and_where('SEC_Participantes.status', '=', '1')
                ->execute($this->_db);
        return $apoio;
    }

    public function select_apoio_participante($idParticipante) {
        $apoio = DB::select("idApoio, idParticipante, idCertificado, idOrigem, idTipo, modulo, tema, data, livre, Autenticacao, Impresso, Status, idUsuario, dtCriacao, dtAlteracao")
                ->from('SEC_Apoio')
                ->where('idParticipante', '=', $idParticipante)
                ->execute($this->_db)->as_array();
        return $apoio;
    }

    public function select_apoio_tipo($idTipo) {
        $apoio = DB::select("idApoio, idParticipante, idCertificado, idOrigem, idTipo,modulo, tema, data, livre, Autenticacao, Impresso, Status, idUsuario, dtCriacao, dtAlteracao")
                ->from('SEC_Apoio')
                ->where('idTipo', '=', $idTipo)
                ->execute($this->_db);
        return $apoio;
    }

    public function select_apoio_origem($idOrigem) {
        $apoio = DB::select("idApoio, idParticipante, idCertificado, idOrigem, idTipo,modulo, tema, data, livre, Autenticacao, Impresso, Status, idUsuario, dtCriacao, dtAlteracao")
                ->from('SEC_Apoio')
                ->where('idOrigem', '=', $idOrigem)
                ->execute($this->_db);
        return $apoio;
    }

    public function select_apoio_ceritificado($idApoio, $autenticacao) {
        $apoio = DB::select("idApoio, idParticipante, idCertificado, idOrigem, idTipo,modulo, tema, data, livre, Autenticacao, Impresso, Status, idUsuario, dtCriacao, dtAlteracao")
                ->from('SEC_Apoio')
                ->where('idApoio', '=', $idApoio)
                ->and_where('Autenticacao', '=', $autenticacao)
                ->execute($this->_db);
        return $apoio;
    }

    public function select_apoio_ceritificado_auth($autenticacao) {
        $apoio = DB::select("idApoio, idParticipante, idCertificado, idOrigem, idTipo,modulo, tema, data, livre, Autenticacao, Impresso, Status, idUsuario, dtCriacao, dtAlteracao")
                ->from('SEC_Apoio')
                ->where('Autenticacao', '=', $autenticacao)
                ->execute($this->_db);
        return $apoio;
    }
    public function select_apoio_participante_evento($idCertificado){
        $apoio = DB::select("idParticipante")
                ->from('SEC_Apoio')
                ->where('idCertificado', '=', $idCertificado)
                ->execute($this->_db);        
        return $apoio;
    }

    public function select_apoio_ceritificado_lote($idCertificado) {
        $apoio = DB::select("Autenticacao","idParticipante")
                ->from('SEC_Apoio')
                ->where('idCertificado', '=', $idCertificado)
                //->limit(30)->offset(0)
                ->execute($this->_db);
        return $apoio;
    }
    
     public function select_apoio_ceritificado_lote_full($idCertificado) {
        $apoio = DB::select("idApoio, idParticipante, idCertificado, idOrigem, idTipo,modulo, tema, data, livre, Autenticacao, Impresso, Status, idUsuario, dtCriacao, dtAlteracao")
                ->from('SEC_Apoio')
                ->where('idCertificado', '=', $idCertificado)
                ->execute($this->_db);
        return $apoio;
    }

    public function select_quantidade_apoio() {
        return $this->select_apoios()->count();
    }

    public function insert_apoio($idParticipante, $idCertificado, $idTipo, $idOrigem, $modulo, $tema, $data, $livre) {
        $returned_id = DB::insert("SEC_Apoio", array("idParticipante, idCertificado, idOrigem, idTipo, modulo, tema, data, livre, idUsuario"))
                ->values(array($idParticipante, $idCertificado, $idOrigem, $idTipo, $modulo, $tema, $data, $livre, $_SESSION['idUsuario']))
                ->execute($this->_db)
        ;
        return $returned_id;
    }

    public function update_apoio_certificado($idApoio, $idCertificado) {
        $returned_id = DB::update('SEC_Apoio')
                ->set(array('idCertificado' => $idCertificado))
                ->set(array('idUsuario' => $_SESSION['idUsuario']))
                ->set(array('dtAlteracao' => date('yy-m-d')))
                ->where('idApoio', '=', $idApoio)
                ->execute($this->_db);
        return $returned_id;
    }

    public function update_apoio($idApoio, $idParticipante, $idCertificado, $idTipo, $idOrigem, $modulo, $tema, $data, $livre) {
        $returned_id = DB::update('SEC_Apoio')
                ->set(array('idParticipante' => $idParticipante))
                ->set(array('idCertificado' => $idCertificado))
                ->set(array('idTipo' => $idTipo))
                ->set(array('idOrigem' => $idOrigem))
                ->set(array('modulo' => $modulo))
                ->set(array('tema' => $tema))
                ->set(array('livre' => $livre))
                ->set(array('idUsuario' => $_SESSION['idUsuario']))
                ->set(array('dtAlteracao' => date('yy-m-d')))
                ->where('idApoio', '=', $idApoio)
                ->execute($this->_db);
        return $returned_id;
    }

    public function update_apoio_autenticacao($idApoio, $Autenticacao) {

        $returned_id = DB::update('SEC_Apoio')
                ->set(array('Autenticacao' => $Autenticacao))
                ->set(array('idUsuario' => $_SESSION['idUsuario']))
                ->set(array('dtAlteracao' => date('yy-m-d')))
                ->where('idApoio', '=', $idApoio)
                ->execute($this->_db);
        return $returned_id;
    }

    public function update_apoio_impresso($idApoio, $impresso) {

        $returned_id = DB::update('SEC_Apoio')
                ->set(array('Impresso' => $Impresso))
                ->set(array('idUsuario' => $_SESSION['idUsuario']))
                ->set(array('dtAlteracao' => date('yy-m-d')))
                ->where('idApoio', '=', $idApoio)
                ->execute($this->_db);
        return $returned_id;
    }

    public function delete_apoio($idApoio) {
        $apoio = DB::delete('SEC_Apoio')
                ->where('idApoio', '=', $idApoio)
                ->execute($this->_db)
        ;
        return $apoio;
    }

    public function update_ativar_apoio($idApoio) {
        $apoio = DB::select('Status')
                ->from('SEC_Apoio')
                ->where('idApoio', '=', $idApoio)
                ->execute($this->_db)
        ;

        if ($apoio[0]['Status'] == '1') {
            $pairs = array('Status' => '0',);
        } else {
            $pairs = array('Status' => '1',);
        }

        $state = DB::update('SEC_Apoio')
                ->set($pairs)
                ->where('idApoio', '=', $idApoio)
                ->execute($this->_db)
        ;
        return $state;
    }

    public function select_campos($table) {
        $campos = DB::select('COLUMN_NAME')
                ->from('information_schema.COLUMNS')
                ->where('TABLE_SCHEMA', '=', 'cms_2016')
                ->and_where('TABLE_NAME', '=', $table)
                ->execute($this->_db);
        return $campos;
    }

}

<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Sec_Participantes extends Model {

    /**
     * Construtor da classe, já seta a conexao com o banco de dados
     *
     * @param string $con_type
     */
    public function __construct($db = NULL) {
        if ($db != NULL) {
            // Set the instance or name
            $this->_db = $db;
        } else {
            // Use the default name
            $this->_db = Database::$default;
        }

        if (is_string($this->_db)) {
            // Load the database
            $this->_db = Database::instance($this->_db);
        }
    }

    public function select_participantes() {
        $participantes = DB::select('idParticipante', 'titulacao', 'Nome', 'Nasc', 'Nacionalidade', 'Documento', 'TipoDoc', 'Email', 'Status', 'dtCriacao', 'dtAlteracao')
                ->from('SEC_Participantes');
                if ($_SESSION['Perfil'] !== 'Administrador') {
                    $participantes->where('SEC_Participantes.idTipoSetor', '=', $_SESSION['idSetor']);
                }
                return $participantes->order_by('idParticipante', 'Desc')->execute($this->_db);
    }
      public function select_participantes_id($idParticipante) {
        $participantes = DB::select('idParticipante', 'titulacao', 'Nome', 'Nasc', 'Nacionalidade', 'Documento', 'TipoDoc', 'Email', 'Status', 'dtCriacao', 'dtAlteracao')
                ->from('SEC_Participantes')
                ->where('idParticipante', '=', $idParticipante)
                ->and_where('Status', '=', '1');
                return $participantes->order_by('idParticipante', 'Desc')->execute($this->_db);
    }

    public function select_participantes_pesquisa($Pesquisa) {
        $participantes = DB::select('idParticipante', 'titulacao', 'Nome', 'Nasc', 'Nacionalidade', 'Documento', 'TipoDoc', 'Email', 'Status', 'idParticipante', 'dtCriacao', 'dtAlteracao')
                ->from('SEC_Participantes');
                if (is_numeric($Pesquisa)) {
                    $participantes->where('idParticipante', '=', $Pesquisa);
                } else {
                    $participantes->where('Nome', 'LIKE', '%' . $Pesquisa . '%');
                }
                if ($_SESSION['Perfil'] !== 'Administrador') {
                    $participantes->where('SEC_Participantes.idTipoSetor', '=', $_SESSION['idSetor']);
                }
                return $participantes->order_by('idParticipante', 'DESC')->execute($this->_db);
    }

    public function select_participantes_count() {
        return $this->select_participantes();
    }

    public function select_participante($idParticipante) {
        $participantes = DB::select('idParticipante', 'titulacao', 'Nome', 'Nasc', 'Nacionalidade', 'Documento', 'TipoDoc', 'Email', 'Status', 'dtCriacao', 'dtAlteracao')
                ->from('SEC_Participantes')
                ->where('SEC_Participantes.idParticipante', '=', $idParticipante)
                ->execute($this->_db)
        ;
        return $participantes;
    }
    
    public function select_participantes_in($ids, $Pesquisa) {
        $participantes = DB::select('idParticipante', 'titulacao', 'Nome', 'Nasc', 'Nacionalidade', 'Documento', 'TipoDoc', 'Email', 'Status', 'dtCriacao', 'dtAlteracao')
                ->from('SEC_Participantes')
                ->where('idParticipante', 'in', $ids)
                ->and_where('Nome', 'LIKE', '%' . $Pesquisa . '%')
                ->execute($this->_db);             
       
        return $participantes;
    }

    public function select_participante_nome($nome) {
        $participantes = DB::select('idParticipante', 'titulacao', 'Nome', 'Nasc', 'Nacionalidade', 'Documento', 'TipoDoc', 'Email', 'Status')
                ->from('SEC_Participantes')
                ->where('Nome', '=', $nome)
                ->execute($this->_db)
        ;
        return $participantes;
    }
     public function select_participante_nome_email($nome, $email) {
        $participantes = DB::select('idParticipante', 'titulacao', 'Nome', 'Nasc', 'Nacionalidade', 'Documento', 'TipoDoc', 'Email', 'Status')
                ->from('SEC_Participantes')
                ->where('Nome', '=', $nome)
                ->or_where('Email', '=', $email)             
                ->execute($this->_db);
       // Funcoes::dump($participantes);
        return $participantes;
    }

    public function select_quantidade_participantes() {
        return $this->select_participantes_count()->count();
    }

    public function update_ativar_participante($idParticipante) {
        $participante = DB::select('Status')
                ->from('SEC_Participantes')
                ->where('idParticipante', '=', $idParticipante)
                ->execute($this->_db)
        ;

        if ($participante[0]['Status'] == '1') {
            $pairs = array('Status' => '0',);
        } else {
            $pairs = array('Status' => '1',);
        }

        $state = DB::update('SEC_Participantes')
                ->set($pairs)
                ->where('idParticipante', '=', $idParticipante)
                ->execute($this->_db)
        ;
        return $state;
    }

    public function insert_participante($titulacao, $nmParticipante, $nasc, $nacionalidade, $Doc, $TDoc, $email, $idSetor=null) {
        
        if($idSetor==null){
           $idSetor = $_SESSION['idSetor'];
        }
        $returned_id = DB::insert("SEC_Participantes", array("titulacao", "Nome", "Nasc", "Nacionalidade", "Documento", "TipoDoc", "Email", 'Status', "idUsuario", "idTipoSetor"))
            ->values(array($titulacao, $nmParticipante, $nasc, $nacionalidade, $Doc, $TDoc, $email, 1, $_SESSION['idUsuario'], $idSetor))
            ->execute($this->_db);
        return $returned_id;
    }

    public function delete_participante($idParticipante) {
        $model_apoio = new Model_Sec_Apoio('default');
        $verifica = $model_apoio->select_apoio_participante($idParticipante); 
        if (sizeof($verifica)>0) {
          $apoio = DB::delete('SEC_Apoio')
                    ->where('idParticipante', '=', $idParticipante)
                    ->execute($this->_db);
        }
        $participante = DB::delete('SEC_Participantes')
                ->where('idParticipante', '=', $idParticipante)
                ->execute($this->_db);

        return $participante;
    }

    public function excluir_participante($idParticipante) {
        $certificado = new Model_SEC_Certificado('default');
        $dados = $certificado->select_certificado_excluir($idParticipante);

        for ($i = 0; $i < cont($dados); $i++) {
            $certificado->excluir_certificado($dados[$i][''], $dados[$i]['']);
        }
        $participante = DB::delete('SEC_Participantes')
                ->where('idParticipante', '=', $idParticipante)
                ->execute($this->_db)
        ;

        //    return $participante;
    }

    public function update_participante($idParticipante, $titulacao, $Nome, $nasc, $nacionalidade, $Doc, $TDoc, $email) {
        $participante = DB::update('SEC_Participantes')
                ->set(array('titulacao' => $titulacao))
                ->set(array('Nome' => $Nome))
                ->set(array('Nasc' => $nasc))
                ->set(array('Nacionalidade' => $nacionalidade))
                ->set(array('TipoDoc' => $TDoc))
                ->set(array('Documento' => $Doc))
                ->set(array('Email' => $email))
                ->set(array('idUsuario' => $_SESSION['idUsuario']))
                ->set(array('dtAlteracao' => date('yy-m-d')))
                ->where('idParticipante', '=', $idParticipante)
                ->execute($this->_db)
        ;
        return $participante;
    }

    public function validar_email($busca) {
        $email = DB::select('email')
                ->from('SEC_Participantes')
                ->where('email', '=', $busca)
                ->execute($this->_db);

        if ($email->count() > 0)
            echo json_encode(array('email' => 'Já existe um usuário cadastrado com este email'));
        else
            echo json_encode(array('email' => 'Usuário valido.'));
    }

}

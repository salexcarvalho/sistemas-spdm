<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Contatos_Endereco extends Model {

    /**
     * Construtor da classe, já seta a conexao com o banco de dados
     *
     * @param string $con_type
     */
    public function __construct($db = NULL) {
        if ($db != NULL) {
            // Set the instance or name
            $this->_db = $db;
        } else {
            // Use the default name
            $this->_db = Database::$default;
        }

        if (is_string($this->_db)) {
            // Load the database
            $this->_db = Database::instance($this->_db);
        }
    }

    // Select para endereços
    public function select_endereco() {
        $endereco = DB::select('idEndereco', 'idContato', 'Local_Endereco', 'Endereco_Logradouro', 'Endereco_Numero', 'Endereco_Complemento', 'Endereco_Cep',
                'Endereco_Bairro', 'Endereco_Cidade', 'Endereco_Estado', 'Status')
                ->from('AGE_Endereco')
                ->execute($this->_db)
        ;
        return $endereco;
    }
    
    // Select para endereços por idContato
    public function select_endereco_id($idContato) {
        $endereco = DB::select('idEndereco', 'idContato', 'Local_Endereco', 'Endereco_Logradouro', 'Endereco_Numero', 'Endereco_Complemento', 'Endereco_Cep',
                'Endereco_Bairro', 'Endereco_Cidade', 'Endereco_Estado', 'Status')
                ->from('AGE_Endereco')
                ->where('idContato', '=', $idContato)
                ->execute($this->_db)
        ;
        return $endereco;
    }
    
    // Insert para endereços
    public function insert_enderecos_adicionais($vars, $idContato) {
        for ($i=0; $i<count($vars['Endereco_Logradouro']); $i++) {
            $colunas = array(
                'idContato',
                'Local_Endereco',
                'Endereco_Cep',
                'Endereco_Logradouro',
                'Endereco_Numero',
                'Endereco_Complemento',                
                'Endereco_Bairro',
                'Endereco_Cidade',
                'Endereco_Estado',
                'idUsuario',
                'dtCadastro',
                'Status'
            );
            $values = array(
                $idContato,
                $vars['Local_Endereco'][$i],
                str_replace('-', '', $vars['Endereco_Cep'][$i]),
                $vars['Endereco_Logradouro'][$i],
                $vars['Endereco_Numero'][$i],
                $vars['Endereco_Complemento'][$i],
                $vars['Endereco_Bairro'][$i],
                $vars['Endereco_Cidade'][$i],
                $vars['Endereco_Estado'][$i],
                $_SESSION['idUsuario'],
                date('yy-m-d'),
                $vars['status']
            );
            $enderecos = DB::insert("AGE_Endereco", $colunas)
                    ->values($values)
                    ->execute($this->_db)
            ;
        }
        //return $enderecos;
    }

    // Update para enderecos
    public function update_endereco($vars) {
        for ($i=0; $i<count($vars['Endereco_Logradouro']); $i++) {
            $endereco = DB::update('AGE_Endereco')
                    ->set(array('idContato' => $vars['idContato']))
                    ->set(array('Local_Endereco' => $vars['Local_Endereco'][$i]))
                    ->set(array('Endereco_Cep' => str_replace('-', '', $vars['Endereco_Cep'][$i])))
                    ->set(array('Endereco_Logradouro' => $vars['Endereco_Logradouro'][$i]))
                    ->set(array('Endereco_Numero' => $vars['Endereco_Numero'][$i]))
                    ->set(array('Endereco_Complemento' => $vars['Endereco_Complemento'][$i]))
                    ->set(array('Endereco_Bairro' => $vars['Endereco_Bairro'][$i]))
                    ->set(array('Endereco_Cidade' => $vars['Endereco_Cidade'][$i]))
                    ->set(array('Endereco_Estado' => $vars['Endereco_Estado'][$i]))
                    ->set(array('Status' => $vars['status']))
                    ->set(array('idUsuario' => $_SESSION['idUsuario']))
                    ->set(array('dtAlteracao' => date('yy-m-d')))
                    ->where('idEndereco', '=', $vars['idEndereco'][$i])
                    ->execute($this->_db)
            ;
        }
        return $endereco;
    }

    // Deleta para endereços
    public function delete_endereco($idContato){
        $endereco = DB::delete('AGE_Endereco')
                       ->where('idContato', '=', $idContato)
                       ->execute($this->_db);
        return $endereco;
    }
    
    // Deleta para endereços
    public function delete_endereco_individual($idEndereco){
        $endereco = DB::delete('AGE_Endereco')
                       ->where('idEndereco', '=', $idEndereco)
                       ->execute($this->_db);
        return $endereco;
    }
    
    // Altera o Status para endereços
    public function update_ativar_endereco($idContato){
        $endereco = DB::select('AGE_Endereco.Status')
                ->from('AGE_Endereco')
                ->where('idContato', '=', $idContato)
                ->execute($this->_db)
        ;

        if ($endereco[0]['Status'] == '1')
        {
            $pairs = array('Status' => '0',);
        }
        else
        {
            $pairs = array('Status' => '1',);
        }

        $state = DB::update('AGE_Endereco')
                ->set($pairs)
                ->where('idContato', '=', $idContato)
                ->execute($this->_db)
        ;
        return $state;
    }

}

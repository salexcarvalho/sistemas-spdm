<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Contatos_Telefone extends Model {

    /**
     * Construtor da classe, já seta a conexao com o banco de dados
     *
     * @param string $con_type
     */
    public function __construct($db = NULL) {
        if ($db != NULL) {
            // Set the instance or name
            $this->_db = $db;
        } else {
            // Use the default name
            $this->_db = Database::$default;
        }

        if (is_string($this->_db)) {
            // Load the database
            $this->_db = Database::instance($this->_db);
        }
    }
    
    // Select para telefones
    public function select_telefones() {
        $telefone = DB::select('idTelefone', 'idContato', 'Numero', 'Observacao', 'idTipoTel', 'Status')
                ->from('AGE_Telefone')
                ->execute($this->_db)
        ;
        return $telefone;
    }
    
    // Select para tipos de telefones
    public function select_tipo() {
        $telefone = DB::select('idTipoTel', 'Descricao', 'dtCadastro', 'dtAlteracao', 'Status')
                ->from('AGE_TipoTel');
                if ($_SESSION['Perfil'] !== 'Administrador') {
                    $telefone->where('AGE_TipoTel.idTipoSetor', '=', $_SESSION['idSetor']);
                }
                return $telefone->execute($this->_db);
    }
    
    // Select para telefones por idContato
    public function select_telefone_id($idContato) {
        $telefone = DB::select('AGE_Telefone.idTelefone', 'AGE_Telefone.idContato', 'AGE_Telefone.Numero', 'AGE_Telefone.Observacao', 'AGE_Telefone.idTipoTel', 'AGE_Telefone.Status', 'AGE_TipoTel.Descricao')
                ->from('AGE_Telefone')
                ->join('AGE_TipoTel', 'LEFT')
                ->on('AGE_TipoTel.idTipoTel', '=', 'AGE_Telefone.idTipoTel')
                ->where('idContato', '=', $idContato)
                ->execute($this->_db)
        ;
        return $telefone;
    }
    
    // Select para tipos de telefones por id
    public function select_tipo_id($idTipoTel) {
        $telefone = DB::select('idTipoTel', 'Descricao', 'dtCadastro', 'dtAlteracao', 'Status')
                ->from('AGE_TipoTel')
                ->where('idTipoTel', '=', $idTipoTel)
                ->execute($this->_db)
        ;
        return $telefone;
    }
    
    // Insert para telefones
    public function insert_telefones_adicionais($vars, $idContato) {
        for ($i=0; $i<count($vars['idTipoTel']); $i++) {  
            $colunas = array(
                'idContato', 
                'Numero',
                'Observacao',
                'idTipoTel',
                'idUsuario',
                'dtCadastro',
                'Status'
            );
            $values = array(
                $idContato,
                preg_replace(array('/\s/', '/-/', '/\(|\)/'), '', $vars['Tel_Add'][$i]),
                $vars['Obs_Tel_Add'][$i],
                $vars['idTipoTel'][$i],
                $_SESSION['idUsuario'],
                date('yy-m-d'),
                $vars['status']
            );
            $telefones = DB::insert("AGE_Telefone", $colunas)
                    ->values($values)
                    ->execute($this->_db)
            ;
        }
        return $telefones;
    }    

    // Insert para tipos de telefones
    public function insert_tipo($Descricao, $status) {
        $colunas = array(
            'Descricao',
            'dtCadastro',
            'Status',
            'idUsuario',
            'idTipoSetor'
        );
        $values = array(
            $Descricao,
            date('yy-m-d'),
            $status,
            $_SESSION['idUsuario'],
            $_SESSION['idSetor']
        );
        $telefone = DB::insert("AGE_TipoTel", $colunas)
                ->values($values)
                ->execute($this->_db)
        ;
        return $telefone;
    }

    // Update para tipos de telefones
    public function update_tipo($idTipoTel, $Descricao, $status) {
        $telefone = DB::update('AGE_TipoTel')
                ->set(array('Descricao' => $Descricao))
                ->set(array('Status' => $status))
                ->set(array('idUsuario' => $_SESSION['idUsuario']))
                ->set(array('dtAlteracao' => date('yy-m-d')))
                ->where('idTipoTel', '=', $idTipoTel)
                ->execute($this->_db)
        ;
        return $telefone;
    }
    
    // Update para telefones
    public function update_telefones($vars) {
        for ($i=0; $i<count($vars['idTipoTel']); $i++) {
            $emails = DB::update('AGE_Telefone')
                    ->set(array('Numero' => preg_replace(array('/\s/', '/-/', '/\(|\)/'), '', $vars['Tel_Add'][$i])))
                    ->set(array('Observacao' => $vars['Obs_Tel_Add'][$i]))
                    ->set(array('idTipoTel' => $vars['idTipoTel'][$i]))
                    ->set(array('Status' => $vars['status']))
                    ->set(array('idUsuario' => $_SESSION['idUsuario']))
                    ->set(array('dtAlteracao' => date('yy-m-d')))
                    ->where('idTelefone', '=', $vars['idTelefone'][$i])
                    ->execute($this->_db)
            ;
        }
        return $emails;
    }
    
    // Deleta para telefones
    public function delete_telefone($idContato){
        $telefone = DB::delete('AGE_Telefone')
                       ->where('idContato', '=', $idContato)
                       ->execute($this->_db);
        return $telefone;
    }
    
    // Deleta para telefones
    public function delete_telefone_individual($idTelefone){
        $telefone = DB::delete('AGE_Telefone')
                       ->where('idTelefone', '=', $idTelefone)
                       ->execute($this->_db);
        return $telefone;
    }
    
    // Deleta para tipos de telefones
    public function delete_tipo($idTipoTel){
        $telefone = DB::delete('AGE_TipoTel')
                       ->where('idTipoTel', '=', $idTipoTel)
                       ->execute($this->_db);
        return $telefone;
    }
    
    // Altera o Status para telefones
    public function update_ativar_telefone($idContato){
        $telefone = DB::select('AGE_Telefone.Status')
                ->from('AGE_Telefone')
                ->where('idContato', '=', $idContato)
                ->execute($this->_db)
        ;

        if ($telefone[0]['Status'] == '1')
        {
            $pairs = array('Status' => '0',);
        }
        else
        {
            $pairs = array('Status' => '1',);
        }

        $state = DB::update('AGE_Telefone')
                ->set($pairs)
                ->where('idContato', '=', $idContato)
                ->execute($this->_db)
        ;
        return $state;
    }
    
    // Altera o Status para tipos de telefones
    public function update_ativar_tipo($idTipoTel){
        $telefone = DB::select('AGE_TipoTel.Status')
                ->from('AGE_TipoTel')
                ->where('idTipoTel', '=', $idTipoTel)
                ->execute($this->_db)
        ;

        if ($telefone[0]['Status'] == '1')
        {
            $pairs = array('Status' => '0',);
        }
        else
        {
            $pairs = array('Status' => '1',);
        }

        $state = DB::update('AGE_TipoTel')
                ->set($pairs)
                ->where('idTipoTel', '=', $idTipoTel)
                ->execute($this->_db)
        ;
        return $state;
    }

}

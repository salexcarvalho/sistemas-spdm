<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Contatos_Home extends Model {

    /**
     * Construtor da classe, já seta a conexao com o banco de dados
     *
     * @param string $con_type
     */
    public function __construct($db = NULL) {
        if ($db != NULL) {
            // Set the instance or name
            $this->_db = $db;
        } else {
            // Use the default name
            $this->_db = Database::$default;
        }

        if (is_string($this->_db)) {
            // Load the database
            $this->_db = Database::instance($this->_db);
        }
    }

    // Select para tipos de contatos
    public function select_contato($Pesquisa) {
        $contato = DB::select('AGE_Contato.idContato', 'AGE_Contato.idTratamento', 'AGE_Contato.Nome', 'AGE_Contato.idTipoSetor', 'AGE_Contato.EmailP',
                'AGE_Contato.Telefone_Escritorio', 'AGE_Contato.Celular_Principal', 'AGE_Contato.Obs', PREFIX.TABLELOGIN.'LOGIN_TipoSetor.Setor', 'AGE_Contato.Status',
                'AGE_Contato.Departamento', array('AGE_TipoTratamento.Descricao', 'Tratamento'))
                ->from('AGE_Contato')
                ->join(PREFIX.TABLELOGIN.'LOGIN_TipoSetor', 'LEFT')
                ->on(PREFIX.TABLELOGIN.'LOGIN_TipoSetor.idTipoSetor', '=', 'AGE_Contato.idTipoSetor')
                ->join('AGE_TipoTratamento', 'LEFT')
                ->on('AGE_TipoTratamento.idTipoTratamento', '=', 'AGE_Contato.idTratamento');
                if ($_SESSION['Perfil'] !== 'Administrador') {
                    $contato->where('AGE_Contato.idTipoSetor', '=', $_SESSION['idSetor']);
                }
                if ($Pesquisa !== '') {   
                $contato->and_where('AGE_Contato.Nome', 'LIKE', '%'.$Pesquisa.'%')->execute($this->_db);                            
                } 
                return $contato->order_by('AGE_Contato.idContato', 'DESC')->execute($this->_db);
                
    }
    
    // Select para tipos de contatos por id
    public function select_contato_id($idContato) {
        $contato = DB::select('AGE_Contato.idContato', 'AGE_Contato.idTratamento', 'AGE_Contato.Nome', 'AGE_Contato.dtNascimento', 'AGE_Contato.Cargo',
                'AGE_Contato.idTipoSetor', 'AGE_Contato.Departamento', 'AGE_Contato.idGrupo', 'AGE_Contato.EmailP', 'AGE_Contato.Telefone_Escritorio',
                'AGE_Contato.Celular_Principal', 'AGE_Contato.Obs', 'AGE_Contato.Endereco_Logradouro', 'AGE_Contato.Endereco_Numero', 'AGE_Contato.Endereco_Complemento',
                'AGE_Contato.Endereco_Cep', 'AGE_Contato.Endereco_Bairro', 'AGE_Contato.Endereco_Cidade', 'AGE_Contato.Endereco_Estado', 'AGE_Contato.idUsuario',
                'AGE_Contato.Status', PREFIX.TABLELOGIN.'LOGIN_TipoSetor.Setor', 'AGE_TipoTratamento.Descricao', 'AGE_TipoGrupo.Grupo')
                ->from('AGE_Contato')
                ->join(PREFIX.TABLELOGIN.'LOGIN_TipoSetor', 'LEFT')
                ->on(PREFIX.TABLELOGIN.'LOGIN_TipoSetor.idTipoSetor', '=', 'AGE_Contato.idTipoSetor')
                ->join('AGE_TipoTratamento', 'LEFT')
                ->on('AGE_TipoTratamento.idTipoTratamento', '=', 'AGE_Contato.idTratamento')
                ->join('AGE_TipoGrupo', 'LEFT')
                ->on('AGE_TipoGrupo.idTipoGrupo', '=', 'AGE_Contato.idGrupo')
                ->where('idContato', '=', $idContato)
                ->execute($this->_db)
        ;
        return $contato;
    }

    // Insert para contatos
    public function insert_contato($vars) {
        $colunas = array(
            'idTratamento', 
            'Nome',
            //'dtNascimento',
            'Cargo',
            'Departamento',
            'idTipoSetor',
            'idGrupo',
            'EmailP',
            'Telefone_Escritorio',
            'Celular_Principal',
            'Obs',
            'Endereco_Cep',
            'Endereco_Logradouro',
            'Endereco_Numero',
            'Endereco_Complemento',
            'Endereco_Bairro',
            'Endereco_Cidade',
            'Endereco_Estado',
            'idUsuario',
            'dtCadastro',
            'Status'
        );
        $values = array(
            $vars['idTipoTratamento'],
            $vars['Nome'],
            //$vars['dtNascimento'],
            $vars['Cargo'],
            $vars['Departamento'],
            $_SESSION['idSetor'],
            $vars['idTipoGrupo'],
            $vars['EmailP'],
            preg_replace(array('/\s/', '/-/', '/\(|\)/'), '', $vars['Telefone_Escritorio']),
            preg_replace(array('/\s/', '/-/', '/\(|\)/'), '', $vars['Celular_Principal']),
            $vars['Obs'],
            str_replace('-', '', $vars['Endereco_Cep_P']),
            $vars['Endereco_Logradouro_P'],
            $vars['Endereco_Numero_P'],
            $vars['Endereco_Complemento_P'],
            $vars['Endereco_Bairro_P'],
            $vars['Endereco_Cidade_P'],
            $vars['Endereco_Estado_P'],
            $_SESSION['idUsuario'],
            date('yy-m-d'),
            $vars['status']
        );
        $contato = DB::insert("AGE_Contato", $colunas)
                ->values($values)
                ->execute($this->_db)
        ;
        return $contato;
    }

    // Update para contatos
    public function update_contato($vars) {
        $contato = DB::update('AGE_Contato')
                ->set(array('idTratamento' => $vars['idTipoTratamento']))
                ->set(array('Nome' => $vars['Nome']))
                //->set(array('dtNascimento' => $vars['dtNascimento']))
                ->set(array('Cargo' => $vars['Cargo']))
                ->set(array('Departamento' => $vars['Departamento']))
                ->set(array('idGrupo' => $vars['idTipoGrupo']))
                ->set(array('EmailP' => $vars['EmailP']))
                ->set(array('Telefone_Escritorio' => preg_replace(array('/\s/', '/-/', '/\(|\)/'), '', $vars['Telefone_Escritorio'])))
                ->set(array('Celular_Principal' => preg_replace(array('/\s/', '/-/', '/\(|\)/'), '', $vars['Celular_Principal'])))
                ->set(array('Obs' => $vars['Obs']))
                ->set(array('Endereco_Cep' => str_replace('-', '', $vars['Endereco_Cep_P'])))
                ->set(array('Endereco_Logradouro' => $vars['Endereco_Logradouro_P']))
                ->set(array('Endereco_Numero' => $vars['Endereco_Numero_P']))
                ->set(array('Endereco_Complemento' => $vars['Endereco_Complemento_P']))
                ->set(array('Endereco_Bairro' => $vars['Endereco_Bairro_P']))
                ->set(array('Endereco_Cidade' => $vars['Endereco_Cidade_P']))
                ->set(array('Endereco_Estado' => $vars['Endereco_Estado_P']))
                ->set(array('Status' => $vars['status']))
                ->set(array('idUsuario' => $_SESSION['idUsuario']))
                ->set(array('dtAlteracao' => date('yy-m-d')))
                ->where('idContato', '=', $vars['idContato'])
                ->execute($this->_db)
        ;
        return $contato;
    }
    
    // Deleta para tipos de contatos
    public function delete_contato($idContato){
        $contato = DB::delete('AGE_Contato')
                       ->where('idContato', '=', $idContato)
                       ->execute($this->_db);
        return $contato;
    }
    
    // Altera o Status para tipos de contatos
    public function update_ativar_contato($idContato){
        $contato = DB::select('AGE_Contato.Status')
                ->from('AGE_Contato')
                ->where('idContato', '=', $idContato)
                ->execute($this->_db)
        ;

        if ($contato[0]['Status'] == '1')
        {
            $pairs = array('Status' => '0',);
        }
        else
        {
            $pairs = array('Status' => '1',);
        }

        $state = DB::update('AGE_Contato')
                ->set($pairs)
                ->where('idContato', '=', $idContato)
                ->execute($this->_db)
        ;
        return $state;
    }

}

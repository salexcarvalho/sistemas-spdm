<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Contatos_Tratamento extends Model {

    /**
     * Construtor da classe, já seta a conexao com o banco de dados
     *
     * @param string $con_type
     */
    public function __construct($db = NULL) {
        if ($db != NULL) {
            // Set the instance or name
            $this->_db = $db;
        } else {
            // Use the default name
            $this->_db = Database::$default;
        }

        if (is_string($this->_db)) {
            // Load the database
            $this->_db = Database::instance($this->_db);
        }
    }

    // Select para tipos de tratamentos
    public function select_tipo() {
        $tipoemail = DB::select('idTipoTratamento', 'Descricao', 'dtCadastro', 'dtAlteracao', 'Status')
                ->from('AGE_TipoTratamento');
                if ($_SESSION['Perfil'] !== 'Administrador') {
                    $tipoemail->where('AGE_TipoTratamento.idTipoSetor', '=', $_SESSION['idSetor']);
                }
                return $tipoemail->order_by('idTipoTratamento', 'Asc')->execute($this->_db);
    }
    
    // Select para tipos de tratamentos por id
    public function select_tipo_id($idTipoTratamento) {
        $tipoemail = DB::select('idTipoTratamento', 'Descricao', 'dtCadastro', 'dtAlteracao', 'Status')
                ->from('AGE_TipoTratamento')
                ->where('idTipoTratamento', '=', $idTipoTratamento)
                ->execute($this->_db)
        ;
        return $tipoemail;
    }

    // Insert para tipos de tratamentos
    public function insert_tipo($Descricao, $status) {
        $colunas = array(
            'Descricao',
            'dtCadastro',
            'Status',
            'idUsuario',
            'idTipoSetor'
        );
        $values = array(
            $Descricao,
            date('yy-m-d'),
            $status,
            $_SESSION['idUsuario'],
            $_SESSION['idSetor']
        );
        $tipoemail = DB::insert("AGE_TipoTratamento", $colunas)
                ->values($values)
                ->execute($this->_db)
        ;
        return $tipoemail;
    }

    // Update para tipos de tratamentos
    public function update_tipo($idTipoTratamento, $Descricao, $status) {
        $tipoemail = DB::update('AGE_TipoTratamento')
                ->set(array('Descricao' => $Descricao))
                ->set(array('Status' => $status))
                ->set(array('idUsuario' => $_SESSION['idUsuario']))
                ->set(array('dtAlteracao' => date('yy-m-d')))
                ->where('idTipoTratamento', '=', $idTipoTratamento)
                ->execute($this->_db)
        ;
        return $tipoemail;
    }
    
    // Deleta para tipos de tratamentos
    public function delete_tipo($idTipoTratamento){
        $tipoemail = DB::delete('AGE_TipoTratamento')
                       ->where('idTipoTratamento', '=', $idTipoTratamento)
                       ->execute($this->_db);
        return $tipoemail;
    }
    
    // Altera o Status para tipos de tratamentos
    public function update_ativar_tipo($idTipoTratamento){
        $tipoemail = DB::select('AGE_TipoTratamento.Status')
                ->from('AGE_TipoTratamento')
                ->where('idTipoTratamento', '=', $idTipoTratamento)
                ->execute($this->_db)
        ;

        if ($tipoemail[0]['Status'] == '1')
        {
            $pairs = array('Status' => '0',);
        }
        else
        {
            $pairs = array('Status' => '1',);
        }

        $state = DB::update('AGE_TipoTratamento')
                ->set($pairs)
                ->where('idTipoTratamento', '=', $idTipoTratamento)
                ->execute($this->_db)
        ;
        return $state;
    }

}

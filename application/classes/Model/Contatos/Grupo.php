<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Contatos_Grupo extends Model {

    /**
     * Construtor da classe, já seta a conexao com o banco de dados
     *
     * @param string $con_type
     */
    public function __construct($db = NULL) {
        if ($db != NULL) {
            // Set the instance or name
            $this->_db = $db;
        } else {
            // Use the default name
            $this->_db = Database::$default;
        }

        if (is_string($this->_db)) {
            // Load the database
            $this->_db = Database::instance($this->_db);
        }
    }

    // Select para tipos de grupos
    public function select_tipo() {
        $tipoemail = DB::select('idTipoGrupo', 'Grupo', 'Descricao', 'dtCadastro', 'dtAlteracao', 'Status')
                ->from('AGE_TipoGrupo');
                if ($_SESSION['Perfil'] !== 'Administrador') {
                    $tipoemail->where('AGE_TipoGrupo.idTipoSetor', '=', $_SESSION['idSetor']);
                }
                return $tipoemail->execute($this->_db);
    }
    
    // Select para tipos de grupos por id
    public function select_tipo_id($idTipoGrupo) {
        $tipoemail = DB::select('idTipoGrupo', 'Grupo', 'Descricao', 'dtCadastro', 'dtAlteracao', 'Status')
                ->from('AGE_TipoGrupo')
                ->where('idTipoGrupo', '=', $idTipoGrupo)
                ->execute($this->_db)
        ;
        return $tipoemail;
    }

    // Insert para tipos de grupos
    public function insert_tipo($Grupo, $Descricao, $status) {
        $colunas = array(
            'Grupo',
            'Descricao',
            'dtCadastro',
            'Status',
            'idUsuario',
            'idTipoSetor'
        );
        $values = array(
            $Grupo,
            $Descricao,
            date('yy-m-d'),
            $status,
            $_SESSION['idUsuario'],
            $_SESSION['idSetor']
        );
        $tipoemail = DB::insert("AGE_TipoGrupo", $colunas)
                ->values($values)
                ->execute($this->_db)
        ;
        return $tipoemail;
    }

    // Update para tipos de grupos
    public function update_tipo($idTipoGrupo, $Grupo, $Descricao, $status) {
        
        $tipoemail = DB::update('AGE_TipoGrupo')
                ->set(array('Grupo' => $Grupo))
                ->set(array('Descricao' => $Descricao))
                ->set(array('Status' => $status))
                ->set(array('idUsuario' => $_SESSION['idUsuario']))
                ->set(array('dtAlteracao' => date('yy-m-d')))
                ->where('idTipoGrupo', '=', $idTipoGrupo)
                ->execute($this->_db)
        ;
        return $tipoemail;
    }
    
    // Deleta para tipos de grupos
    public function delete_tipo($idTipoGrupo){
        $tipoemail = DB::delete('AGE_TipoGrupo')
                       ->where('idTipoGrupo', '=', $idTipoGrupo)
                       ->execute($this->_db);
        return $tipoemail;
    }
    
    // Altera o Status para tipos de grupos
    public function update_ativar_tipo($idTipoGrupo){
        $tipoemail = DB::select('AGE_TipoGrupo.Status')
                ->from('AGE_TipoGrupo')
                ->where('idTipoGrupo', '=', $idTipoGrupo)
                ->execute($this->_db)
        ;

        if ($tipoemail[0]['Status'] == '1')
        {
            $pairs = array('Status' => '0',);
        }
        else
        {
            $pairs = array('Status' => '1',);
        }

        $state = DB::update('AGE_TipoGrupo')
                ->set($pairs)
                ->where('idTipoGrupo', '=', $idTipoGrupo)
                ->execute($this->_db)
        ;
        return $state;
    }

}

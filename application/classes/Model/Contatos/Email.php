<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Contatos_Email extends Model {

    /**
     * Construtor da classe, já seta a conexao com o banco de dados
     *
     * @param string $con_type
     */
    public function __construct($db = NULL) {
        if ($db != NULL) {
            // Set the instance or name
            $this->_db = $db;
        } else {
            // Use the default name
            $this->_db = Database::$default;
        }

        if (is_string($this->_db)) {
            // Load the database
            $this->_db = Database::instance($this->_db);
        }
    }

    // Select para emails adicionais
    public function select_emails() {
        $email = DB::select('idEmail', 'idContato', 'Email', 'Observacao', 'idTipoEmail', 'Status')
                ->from('AGE_Email')
                ->execute($this->_db)
        ;
        return $email;
    }

    // Select para tipos de emails
    public function select_tipo() {
        $tipoemail = DB::select('idTipoEmail', 'Descricao', 'dtCadastro', 'dtAlteracao', 'Status')
                ->from('AGE_TipoEmail');
                if ($_SESSION['Perfil'] !== 'Administrador') {
                    $tipoemail->where('AGE_TipoEmail.idTipoSetor', '=', $_SESSION['idSetor']);
                }
                return $tipoemail->execute($this->_db);
    }
    
    // Select para emails por idContato
    public function select_email_id($idContato) {
        $email = DB::select('AGE_Email.idEmail', 'AGE_Email.idContato', 'AGE_Email.Email', 'AGE_Email.Observacao', 'AGE_Email.idTipoEmail',
                'AGE_Email.Status', 'AGE_TipoEmail.Descricao')
                ->from('AGE_Email')
                ->join('AGE_TipoEmail', 'LEFT')
                ->on('AGE_TipoEmail.idTipoEmail', '=', 'AGE_Email.idTipoEmail')
                ->where('idContato', '=', $idContato)
                ->execute($this->_db)
        ;
        return $email;
    }
    
    // Select para tipos de emails por id
    public function select_tipo_id($idTipoEmail) {
        $tipoemail = DB::select('idTipoEmail', 'Descricao', 'dtCadastro', 'dtAlteracao', 'Status')
                ->from('AGE_TipoEmail')
                ->where('idTipoEmail', '=', $idTipoEmail)
                ->execute($this->_db)
        ;
        return $tipoemail;
    }

    // Insert para tipos de emails
    public function insert_tipo($Descricao, $status) {
        $colunas = array(
            'Descricao',
            'dtCadastro',
            'Status',
            'idUsuario',
            'idTipoSetor'
        );
        $values = array(
            $Descricao,
            date('yy-m-d'),
            $status,
            $_SESSION['idUsuario'],
            $_SESSION['idSetor']
        );
        $tipoemail = DB::insert("AGE_TipoEmail", $colunas)
                ->values($values)
                ->execute($this->_db)
        ;
        return $tipoemail;
    }
    
    // Insert para emails
    public function insert_emails_adicionais($vars, $idContato) {
        for ($i=0; $i<count($vars['Email_Add']); $i++) {
            $colunas = array(
                'idContato', 
                'Email',
                'Observacao',
                'idTipoEmail',
                'idUsuario',
                'dtCadastro',
                'Status'
            );
            $values = array(
                $idContato,
                $vars['Email_Add'][$i],
                $vars['Obs_Email_Add'][$i],
                $vars['idTipoEmail'][$i],
                $_SESSION['idUsuario'],
                date('yy-m-d'),
                $vars['status']
            );
            $emails = DB::insert("AGE_Email", $colunas)
                    ->values($values)
                    ->execute($this->_db)
            ;
        }
        return $emails;
    }   
    
    // Update para emails
    public function update_emails($vars) {
        for ($i=0; $i<count($vars['Email_Add']); $i++) {
            $emails = DB::update('AGE_Email')
                    ->set(array('Email' => $vars['Email_Add'][$i]))
                    ->set(array('Observacao' => $vars['Obs_Email_Add'][$i]))
                    ->set(array('idTipoEmail' => $vars['idTipoEmail'][$i]))
                    ->set(array('Status' => $vars['status']))
                    ->set(array('idUsuario' => $_SESSION['idUsuario']))
                    ->set(array('dtAlteracao' => date('yy-m-d')))
                    ->where('idEmail', '=', $vars['idEmail'][$i])
                    ->execute($this->_db)
            ;
        }
        return $emails;
    }
    
    // Update para tipos de emails
    public function update_tipo($idTipoEmail, $Descricao, $status) {
        $tipoemail = DB::update('AGE_TipoEmail')
                ->set(array('Descricao' => $Descricao))
                ->set(array('Status' => $status))
                ->set(array('idUsuario' => $_SESSION['idUsuario']))
                ->set(array('dtAlteracao' => date('yy-m-d')))
                ->where('idTipoEmail', '=', $idTipoEmail)
                ->execute($this->_db)
        ;
        return $tipoemail;
    }
    
    // Delete para emails
    public function delete_email($idContato){
        $email = DB::delete('AGE_Email')
                       ->where('idContato', '=', $idContato)
                       ->execute($this->_db);
        return $email;
    }
    
    // Delete para emails
    public function delete_email_individual($idEmail){
        $email = DB::delete('AGE_Email')
                       ->where('idEmail', '=', $idEmail)
                       ->execute($this->_db);
        return $email;
    }
    
    // Deleta para tipos de emails
    public function delete_tipo($idTipoEmail){
        $tipoemail = DB::delete('AGE_TipoEmail')
                       ->where('idTipoEmail', '=', $idTipoEmail)
                       ->execute($this->_db);
        return $tipoemail;
    }
    
    // Altera o Status para emails
    public function update_ativar_email($idContato){
        $email = DB::select('AGE_Email.Status')
                ->from('AGE_Email')
                ->where('idContato', '=', $idContato)
                ->execute($this->_db)
        ;

        if ($email[0]['Status'] == '1')
        {
            $pairs = array('Status' => '0',);
        }
        else
        {
            $pairs = array('Status' => '1',);
        }

        $state = DB::update('AGE_Email')
                ->set($pairs)
                ->where('idContato', '=', $idContato)
                ->execute($this->_db)
        ;
        return $state;
    }
    
    // Altera o Status para tipos de emails
    public function update_ativar_tipo($idTipoEmail){
        $tipoemail = DB::select('AGE_TipoEmail.Status')
                ->from('AGE_TipoEmail')
                ->where('idTipoEmail', '=', $idTipoEmail)
                ->execute($this->_db)
        ;

        if ($tipoemail[0]['Status'] == '1')
        {
            $pairs = array('Status' => '0',);
        }
        else
        {
            $pairs = array('Status' => '1',);
        }

        $state = DB::update('AGE_TipoEmail')
                ->set($pairs)
                ->where('idTipoEmail', '=', $idTipoEmail)
                ->execute($this->_db)
        ;
        return $state;
    }

}

<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Csm_Funcionario extends Model
{

    /**
     * Construtor da classe, já seta a conexao com o banco de dados
     *
     * @param string $con_type
     */
    public function __construct($db = NULL)
    {
        if ($db != NULL)
        {
            // Set the instance or name
            $this->_db = $db;
        }
        elseif (!$this->_db)
        {
            // Use the default name
            $this->_db = Database::$default;
        }

        if (is_string($this->_db))
        {
            // Load the database
            $this->_db = Database::instance($this->_db);
        }
    }

    public function select_funcionarios()
    {
        $funcionarios = DB::select('CSM_Funcionario.idFuncionario', 'CSM_Funcionario.nmFuncionario', 'CSM_Funcionario.sexo', 'CSM_Funcionario.dtNascimento',
                'CSM_Funcionario.cdRegistroFuncional', 'CSM_Funcionario.nuConselho', 'CSM_Funcionario.miniCurriculum', 'CSM_Funcionario.idTipoFuncionario',
                'CSM_Funcionario.idTipoFuncionario', 'CSM_Funcionario.idContato', 'CSM_Funcionario.ativo', 'CSM_TipoFuncionario.nmTipoFuncionario',
                'CSM_Contato.idContato')
                ->from('CSM_Funcionario')
                ->join('CSM_TipoFuncionario', 'LEFT')
                ->on('CSM_Funcionario.idTipoFuncionario', '=', 'CSM_TipoFuncionario.idTipoFuncionario')
                ->join('CSM_Contato', 'LEFT')
                ->on('CSM_Funcionario.idContato', '=', 'CSM_Contato.idContato')
                ->order_by('CSM_Funcionario.nmFuncionario', 'ASC')
                ->execute($this->_db)
        ;


        return $funcionarios;
    }
    
    public function select_nmfuncionarios()
    {
        $funcionarios = DB::select('CSM_Funcionario.idFuncionario', 'CSM_Funcionario.nmFuncionario')
                ->from('Funcionario')
                ->order_by('CSM_Funcionario.nmFuncionario', 'ASC')
                ->execute($this->_db)
        ;


        return $funcionarios;
    }


    public function select_funcionario($vars)
    {
        $funcionario = DB::select('CSM_Funcionario.idFuncionario', 'CSM_Funcionario.nmFuncionario', 'CSM_Funcionario.sexo', 'CSM_Funcionario.dtNascimento',
                'CSM_Funcionario.cdRegistroFuncional', 'CSM_Funcionario.nuConselho', 'CSM_Funcionario.miniCurriculum', 'CSM_Funcionario.idTipoFuncionario',
                'CSM_Funcionario.idTipoFuncionario', 'CSM_Funcionario.idContato', 'CSM_Funcionario.ativo', 'CSM_TipoFuncionario.nmTipoFuncionario',
                'CSM_Contato.idContato', 'CSM_Contato.mostrarContatoNoSite', 'CSM_Contato.telefone', 'CSM_Contato.nuVOIP', 'CSM_Contato.email',
                'CSM_Contato.site', 'CSM_Contato.facebook', 'CSM_Contato.twitter', 'CSM_Contato.googleplus', 'CSM_Contato.linkedIn',
                'CSM_Contato.tipoContato', 'CSM_Imagem.caminhoImagem', 'CSM_Imagem.nmImagem', 'CSM_Imagem.dsImagem')
                ->from('CSM_Funcionario')
                ->join('CSM_TipoFuncionario', 'LEFT')
                ->on('CSM_Funcionario.idTipoFuncionario', '=', 'CSM_TipoFuncionario.idTipoFuncionario')
                ->join('CSM_Contato', 'LEFT')
                ->on('CSM_Funcionario.idContato', '=', 'CSM_Contato.idContato')
                ->join('CSM_Imagem', 'LEFT')
                ->on('CSM_Funcionario.idFuncionario', '=', 'CSM_Imagem.idFuncionario')
                ->where('CSM_Funcionario.idFuncionario', '=', $vars['idFuncionario'])
                ->execute($this->_db)
        ;


        return $funcionario;
    }

    public function select_quantidade_funcionarios()
    {
        return $this->select_funcionarios()->count();
    }

    private function select_existe_funcionario($vars)
    {
        $existe_funcionario = DB::select('idFuncionario')
                ->from('CSM_Funcionario')
                ->where('nmFuncionario', '=', $vars['nmFuncionario'])
                ->execute($this->_db)
        ;

        if ($existe_funcionario->count() >= 1)
        {
            return $existe_funcionario[0]['idFuncionario'];
        }
        return FALSE;
    }

    public function insert_funcionario($vars)
    {
        if ($vars['nmFuncionario'] == 'NULL' || empty($vars['nmFuncionario']))
        {
            $vars['nmFuncionario'] = NULL;
            return NULL;
        }

        if ($vars['sexo'] == 'NULL' || empty($vars['sexo']))
        {
            $vars['sexo'] = NULL;
            return NULL;
        }

        if ($vars['dtNascimento'] == 'NULL' || empty($vars['dtNascimento']))
        {
            $vars['dtNascimento'] = NULL;
        }

        if ($vars['cdRegistroFuncional'] == 'NULL' || empty($vars['cdRegistroFuncional']))
        {
            $vars['cdRegistroFuncional'] = NULL;
        }

        if ($vars['nuConselho'] == 'NULL' || empty($vars['nuConselho']))
        {
            $vars['nuConselho'] = NULL;
        }

        if ($vars['miniCurriculum'] == 'NULL' || empty($vars['miniCurriculum']))
        {
            $vars['miniCurriculum'] = NULL;
        }

        if ($vars['tipoFuncionarios'] == 'NULL' || empty($vars['tipoFuncionarios']))
        {
            $vars['tipoFuncionarios'] = NULL;
            return NULL;
        }

        if ($vars['idContato'] == 'NULL' || empty($vars['idContato']))
        {
            $vars['idContato'] = NULL;
        }

        // Se o funcionário já existe, retorna seu ID, não o cadastrando novamente. Variável em forma de array para seguir o padrão do Controller.
        $funcionario[0] = $this->select_existe_funcionario($vars);

        if ($funcionario[0] !== FALSE)
        {
            return $funcionario;
        }
        // fim validação da existência do funcionário.
        

        $colunas = array(
            "nmFuncionario",
            "sexo",
            "dtNascimento",
            "cdRegistroFuncional",
            "nuConselho",
            "miniCurriculum",
            "idTipoFuncionario",
            "idContato",
        );

        $values = array(
            $vars['nmFuncionario'],
            $vars['sexo'],
            $vars['dtNascimento'],
            $vars['cdRegistroFuncional'],
            $vars['nuConselho'],
            $vars['miniCurriculum'],
            $vars['tipoFuncionarios'],
            $vars['idContato'],
        );

        $returned_id = DB::insert("CSM_Funcionario", $colunas)
                ->values($values)
                ->execute($this->_db)
        ;
        return $returned_id;
    }

    public function update_ativar_funcionario($vars)
    {
        $funcionario = DB::select('CSM_Funcionario.ativo')
                ->from('CSM_Funcionario')
                ->where('idFuncionario', '=', $vars['idFuncionario'])
                ->execute($this->_db)
        ;

        if ($funcionario[0]['ativo'] == 'S')
        {
            $pairs = array(
                'ativo' => 'N',
            );
        }
        else
        {
            $pairs = array(
                'ativo' => 'S',
            );
        }

        $state = DB::update('CSM_Funcionario')
                ->set($pairs)
                ->where('idFuncionario', '=', $vars['idFuncionario'])
                ->execute($this->_db)
        ;
        return $state;
    }

    public function delete_funcionario($vars)
    {
        $state = DB::delete('CSM_Funcionario')
                ->where('idFuncionario', '=', $vars['idFuncionario'])
                ->execute($this->_db)
        ;

        return $state;
    }
    
    public function update_funcionario_contato($vars)
    {
        // Formatação dos valores para inserir no banco.
        // Coloca campos vazios como nulo.
        foreach ($vars as $key => $var)
        {
            if (empty($var))
            {
                $vars[$key] = NULL;
            }
        }
        // Fim da formatação.

        $pairs = array(
            'idContato' => $vars['novoIdContato'],
        );

        $state = DB::update('CSM_Funcionario')
                ->set($pairs)
                ->where('idContato', '=', $vars['idContato'])
                ->execute($this->_db)
        ;
        return $state;
    }

     public function update_funcionario($vars)
    {
        
   $pairs = array(
       'nmFuncionario'=> $vars['nmFuncionario'],
       'idTipoFuncionario'=>$vars['tipoFuncionarios'],
       'sexo'=>$vars['sexo'],
       'dtNascimento'=>$vars['dtNascimento'],
       'cdRegistroFuncional'=>$vars['cdRegistroFuncional'],
       'nuConselho'=>$vars['nuConselho'],
       'miniCurriculum'=>$vars['miniCurriculum'],
       );
           
  
        $state = DB::update('CSM_Funcionario')
                ->set($pairs)
                ->where('idFuncionario', '=', $vars['idFuncionario'])
                ->execute($this->_db)
        ;

        return $state;
    }
    
}

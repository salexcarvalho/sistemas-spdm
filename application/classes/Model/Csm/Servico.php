<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Csm_Servico extends Model {

    /**
     * Construtor da classe, já seta a conexao com o banco de dados
     *
     * @param string $con_type
     */
    public function __construct($db = NULL) {
        if ($db != NULL) {
            // Set the instance or name
            $this->_db = $db;
        } else {
            // Use the default name
            $this->_db = Database::$default;
        }

        if (is_string($this->_db)) {
            // Load the database
            $this->_db = Database::instance($this->_db);
        }
    }

    public function select_servicos() {
        $servicos = DB::select('CSM_Servico.idServico', 'CSM_Especialidade.nmEspecialidade', 'CSM_Servico.nmServico', 'CSM_Servico.dsServico',
                'CSM_Servico.dsDoenca', 'CSM_Servico.dsTratamento', 'CSM_Servico.formaAcesso', 'CSM_Servico.dtCadastro', 'CSM_Servico.idContato',
                'CSM_Servico.idEndereco', 'CSM_Servico.idFuncionarioChefe', 'CSM_Servico.ativo', 'CSM_Servico.usuarioCadastro',
                array('CSM_Funcionario.nmFuncionario', 'nmFuncionarioChefe'), 'CSM_Servico.nuCentroCusto')
                ->from('CSM_Servico')
                ->join('CSM_Funcionario', 'LEFT')
                ->on('CSM_Funcionario.idFuncionario', '=', 'CSM_Servico.idFuncionarioChefe')
                ->join('CSM_EspecialidadeTemServico', 'LEFT')
                ->on('CSM_Servico.idServico', '=', 'CSM_EspecialidadeTemServico.idServico')
                ->join('CSM_Especialidade', 'LEFT')
                ->on('CSM_EspecialidadeTemServico.idEspecialidade', '=', 'CSM_Especialidade.idEspecialidade')
                ->order_by('CSM_Servico.nmServico', 'ASC')
                ->execute($this->_db)
        ;

        return $servicos;
    }

    public function select_servico($idServico) {
        $servico = DB::select('CSM_Servico.idServico', 'CSM_Servico.nmServico', 'CSM_Servico.dsServico', 'CSM_Servico.dsDoenca', 'CSM_Servico.dsTratamento',
                'CSM_Servico.formaAcesso', 'CSM_Servico.dtCadastro', 'CSM_Servico.idContato', 'CSM_Servico.idEndereco', 'CSM_Servico.idFuncionarioChefe',
                'CSM_Servico.ativo', 'CSM_Servico.usuarioCadastro', array('CSM_Funcionario.nmFuncionario', 'nmFuncionarioChefe'), 'idFuncionarioChefe',
                'CSM_Servico.nuCentroCusto', 'CSM_Funcionario.dtNascimento', 'CSM_Funcionario.cdRegistroFuncional', 'CSM_Funcionario.nuConselho',
                'CSM_Funcionario.miniCurriculum', 'CSM_Funcionario.idTipoFuncionario', 'CSM_Funcionario.sexo', 'CSM_Endereco.endLogradouro',
                'CSM_Endereco.nuLogradouro', 'CSM_Endereco.complemento', 'CSM_Endereco.CEP', 'CSM_Endereco.bairro', 'CSM_Endereco.cidade',
                'CSM_Endereco.estado', 'CSM_Contato.mostrarContatoNoSite', 'CSM_Contato.telefone', 'CSM_Contato.nuVOIP', 'CSM_Contato.email',
                'CSM_Contato.site', 'CSM_Contato.facebook', 'CSM_Contato.twitter', 'CSM_Contato.googleplus', 'CSM_Contato.linkedIn')
                ->from('CSM_Servico')
                ->join('CSM_Funcionario', 'LEFT')
                ->on('CSM_Funcionario.idFuncionario', '=', 'CSM_Servico.idFuncionarioChefe')
                ->join('CSM_Endereco', 'LEFT')
                ->on('CSM_Endereco.idEndereco', '=', 'CSM_Servico.idEndereco')
                ->join('CSM_Contato', 'LEFT')
                ->on('CSM_Contato.idContato', '=', 'CSM_Servico.idContato')
                ->where('CSM_Servico.idServico', '=', $idServico)
                ->execute($this->_db)
        ;

        return $servico;
    }

    public function select_servicos_mais_procurados($limit) {
        $servico = DB::select('CSM_Servico.idServico', 'CSM_Servico.nmServico')
                ->from('CSM_Servico')
                ->order_by('CSM_Servico.nuAcessos', 'DESC')
                ->limit($limit)
                ->execute($this->_db)
        ;

        return $servico;
    }

    public function select_quantidade_servicos() {
        return $this->select_servicos()->count();
    }

    public function insert_servico($nmServico, $dsServico, $dsDoenca, $dsTratamento, $formaAcesso, $idContato, $idEndereco, $idFuncionarioChefe, $usuarioCadastro, $nuCentroCusto) {
        if ($nmServico == 'NULL' || empty($nmServico)) {
            $nmServico = NULL;
        }
        if ($dsServico == 'NULL' || empty($dsServico)) {
            $dsServico = NULL;
        }
        if ($dsDoenca == 'NULL' || empty($dsDoenca)) {
            $dsDoenca = NULL;
        }
        if ($dsTratamento == 'NULL' || empty($dsTratamento)) {
            $dsTratamento = NULL;
        }
        if ($formaAcesso == 'NULL' || empty($formaAcesso)) {
            $formaAcesso = NULL;
        }
        if ($idEndereco == 'NULL' || empty($idEndereco)) {
            $idEndereco = NULL;
        }
        if ($idContato == 'NULL' || empty($idContato)) {
            $idContato = NULL;
        }
        if ($idFuncionarioChefe == 'NULL' || empty($idFuncionarioChefe)) {
            $idFuncionarioChefe = NULL;
        }
        if ($usuarioCadastro == 'NULL' || empty($usuarioCadastro)) {
            $usuarioCadastro = NULL;
        }
        if ($nuCentroCusto == 'NULL' || empty($nuCentroCusto)) {
            $nuCentroCusto = NULL;
        }

        $colunas = array(
            "nmServico",
            "dsServico",
            "dsDoenca",
            "dsTratamento",
            "formaAcesso",
            "dtCadastro",
            "idContato",
            "idEndereco",
            "idFuncionarioChefe",
            "usuarioCadastro",
            "nuCentroCusto",
        );

        $values = array(
            $nmServico,
            $dsServico,
            $dsDoenca,
            $dsTratamento,
            $formaAcesso,
            DB::expr('NOW()'),
            $idContato,
            $idEndereco,
            $idFuncionarioChefe,
            $usuarioCadastro,
            $nuCentroCusto,
        );

        $returned_id = DB::insert("CSM_Servico", $colunas)
                ->values($values)
                ->execute($this->_db)
        ;
        return $returned_id;
    }

    public function update_ativar_servico($idServico) {
        $servico = DB::select('CSM_Servico.ativo')
                ->from('CSM_Servico')
                ->where('idServico', '=', $idServico)
                ->execute($this->_db)
        ;

        if ($servico[0]['ativo'] == 'S') {
            $pairs = array(
                'ativo' => 'N',
            );
        } else {
            $pairs = array(
                'ativo' => 'S',
            );
        }

        $state = DB::update('CSM_Servico')
                ->set($pairs)
                ->where('idServico', '=', $idServico)
                ->execute($this->_db)
        ;
        return $state;
    }

    public function delete_servico($idServico) {
        $state = DB::delete('CSM_Servico')
                ->where('idServico', '=', $idServico)
                ->execute($this->_db)
        ;

        return $state;
    }

    public function delete_servico_funcionario($vars) {
        $state = DB::delete('CSM_ServicoTemFuncionario')
                ->where('idFuncionario', '=', $vars['idFuncionario'])
                ->execute($this->_db)
        ;

        return $state;
    }
    public function update_servico($vars)
    {
        foreach ($vars as $key => $var) {
            if (empty($var)) {
                $vars[$key] = NULL;
            }
        }
        $pairsServ = array(
            'nmServico' => $vars['nmServico'],
            'dsServico' => $vars['dsServico'],
            'dsDoenca' => $vars['dsDoenca'],
            'dsTratamento' => $vars['dsTratamento'],
            'formaAcesso' => $vars['formaAcesso'],
            'nuCentroCusto' => $vars['nuCentroCusto'],
            'idFuncionarioChefe' => $vars['chefeDep']
        );

        $state = DB::update('CSM_Servico')
                ->set($pairsServ)
                ->where('idServico', '=', $vars['idServico'])
                ->execute($this->_db)
        ;
        return $state;
    }
        
        public function update_servico_funcionario($vars)
    {
        // Formatação dos valores para inserir no banco.
        // Coloca campos vazios como nulo.
        foreach ($vars as $key => $var)
        {
            if (empty($var))
            {
                $vars[$key] = NULL;
            }
        }
        // Fim da formatação.

        $pairs = array(
            'idFuncionarioChefe' => $vars['novoIdFuncionario'],
        );

        $state = DB::update('CSM_Servico')
                ->set($pairs)
                ->where('idFuncionarioChefe', '=', $vars['idFuncionario'])
                ->execute($this->_db)
        ;
        return $state;
    }
    


}

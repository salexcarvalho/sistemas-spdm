<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Csm_EspecialidadeTemFuncionario extends Model
{

    /**
     * Construtor da classe, já seta a conexao com o banco de dados
     *
     * @param string $db Opção de conexão do banco de dados, exemplo : test, default
     */
    public function __construct($db = NULL)
    {
        if ($db != NULL)
        {
            // Set the instance or name
            $this->_db = $db;
        }
        elseif (!$this->_db)
        {
            // Use the default name
            $this->_db = Database::$default;
        }

        if (is_string($this->_db))
        {
            // Load the database
            $this->_db = Database::instance($this->_db);
        }
    }

    public function insert_especialidade_funcionario($idEspecialidade, $idFuncionario)
    {
        if ($idEspecialidade == 'NULL' || empty($idEspecialidade))
        {
            return NULL;
        }
        if ($idFuncionario == 'NULL' || empty($idFuncionario))
        {
            return NULL;
        }

        $colunas = array(
            "idEspecialidade",
            "idFuncionario",
        );

        $values = array(
            $idEspecialidade,
            $idFuncionario,
        );

        DB::insert("CSM_EspecialidadeTemFuncionario", $colunas)
                ->values($values)
                ->execute($this->_db)
        ;
    }
    
    public function select_especialidades_funcionarios()
    {
        $especialidades_funcionarios = DB::select()
                ->from('CSM_EspecialidadeTemFuncionario')
                ->join('CSM_Especialidade', 'INNER')
                ->on('CSM_Especialidade.idEspecialidade', '=', 'CSM_EspecialidadeTemFuncionario.idEspecialidade')
                ->join('CSM_Funcionario', 'INNER')
                ->on('CSM_Funcionario.idFuncionario', '=', 'CSM_EspecialidadeTemFuncionario.idFuncionario')
                ->execute($this->_db)
        ;

        return $especialidades_funcionarios;
    }
    
    public function select_quantidade_especialidades_funcionarios()
    {
        return $this->select_especialidades_funcionarios()->count();
    }
    
    public function delete_especialidade_funcionario($vars)
    {
        $state = DB::delete('CSM_EspecialidadeTemFuncionario')
                ->where('idFuncionario', '=', $vars['idFuncionario'])
                ->execute($this->_db)
        ;

        return $state;
    }
    
    public function delete_espe_funcionario($vars)
    {
        $state = DB::delete('CSM_EspecialidadeTemFuncionario')
                ->where('idFuncionario', '=', $vars['idFuncionario'])
                ->where('idEspecialidade', '=', $vars['idEspecialidade'])
                ->execute($this->_db)
        ;

        return $state;
    }

}

<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Csm_Busca extends Model
{

    /**
     * Construtor da classe, já seta a conexao com o banco de dados
     *
     * @param string $con_type
     */
    public function __construct($db = NULL)
    {
        if ($db != NULL)
        {
            // Set the instance or name
            $this->_db = $db;
        }
        elseif (!$this->_db)
        {
            // Use the default name
            $this->_db = Database::$default;
        }

        if (is_string($this->_db))
        {
            // Load the database
            $this->_db = Database::instance($this->_db);
        }
    }

    public function select_servico($idServico)
    {
        /*
        $query = DB::select()->from('CSM_Servico');

        //only search for these fields
        $form_inputs = array('first_name', 'last_name', 'email');
        foreach ($form_inputs as $name)
        {
            $value = Arr::get($_GET, $name, FALSE);
            if ($value !== FALSE AND $value != '')
            {
                $query->where($name, 'like', '%' . $value . '%');
            }
        }

        $pagination_query = clone $query;
        $count = $pagination_query->select('COUNT("*") AS mycount')->execute()->get('mycount');

        //pass the total item count to Pagination
        $config = Kohana::config('pagination');
        $pagination = Pagination::factory(array(
                    'total_items' => $count,
                    'current_page' => array('source' => 'route', 'key' => 'page'),
                    'items_per_page' => 20,
                    'view' => 'pagination/pretty',
                    'auto_hide' => TRUE,
        ));
        $page_links = $pagination->render();

        //search for results starting at the offset calculated by the Pagination class
        $query->order_by('last_name', 'asc')
                ->order_by('first_name', 'asc')
                ->limit($pagination->items_per_page)
                ->offset($pagination->offset);
        $results = $query->execute()->as_array();

        return $servico;*/
    }
}

<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Csm_Servicos extends Model
{

    /**
     * Construtor da classe, já seta a conexao com o banco de dados
     *
     * @param string $con_type
     */
    public function __construct($db = NULL)
    {
        if ($db != NULL)
        {
            // Set the instance or name
            $this->_db = $db;
        }
        elseif (!$this->_db)
        {
            // Use the default name
            $this->_db = Database::$default;
        }

        if (is_string($this->_db))
        {
            // Load the database
            $this->_db = Database::instance($this->_db);
        }
    }

    /**
     * Retorna os tipos de serviços
     *
     * @return array
     */
    public function get_orgao_classe()
    {
        $dados = array();

        $sql = "SELECT * FROM CSM_OrgaoClasse";

        $results = $this->_db->query(Database::SELECT, $sql);

        return $results;
    }

    public function get_especialidades($limit = null)
    {
        $dados = array();

        $sql = ""
                . "SELECT e.* FROM CSM_Especialidade AS e "
                . "WHERE e.sitEspecialidade = 1 "
                . "ORDER BY e.nuAcessos DESC "
        ;
        if ($limit != null)
        {
            $sql .= "LIMIT 0, {$limit} ";
        }

        $results = $this->_db->query(Database::SELECT, $sql);        

        return $results;
    }

    

    public function acs_acesso($idespecialidade)
    {
        $sql = "UPDATE CSM_Especialidade SET nuAcessos = nuAcessos +1 WHERE idEspecialidade = {$idespecialidade}";

        $results = $this->_db->query(Database::UPDATE, $sql);
    }

}

    <?php

defined('SYSPATH') or die('No direct script access.');

class Model_Csm_HorarioAtendimento extends Model
{

    /**
     * Construtor da classe, já seta a conexao com o banco de dados
     *
     * @param string $con_type
     */
    public function __construct($db = NULL)
    {
        if ($db != NULL)
        {
            // Set the instance or name
            $this->_db = $db;
        }
        elseif (!$this->_db)
        {
            // Use the default name
            $this->_db = Database::$default;
        }

        if (is_string($this->_db))
        {
            // Load the database
            $this->_db = Database::instance($this->_db);
        }
    }

    public function insert_horario_atendimento($diaSemana, $h_inicio, $h_fim, $h_24hs, $idServico, $idEspecialidade,$origemHorario)
    {
        if ($diaSemana == 'NULL' || empty($diaSemana))
        {
            $diaSemana = NULL;
        }
        if ($h_inicio == 'NULL' || empty($h_inicio))
        {
            $h_inicio = NULL;
        }
        if ($h_fim == 'NULL' || empty($h_fim))
        {
            $h_fim = NULL;
        }
        if ($h_24hs == 'NULL' || empty($h_24hs))
        {
            $h_24hs = NULL;
        }
        if ($idServico == 'NULL' || empty($idServico))
        {
            $idServico = NULL;
        }
        if ($idEspecialidade == 'NULL' || empty($idEspecialidade))
        {
            $idEspecialidade = NULL;
        }
        if ($origemHorario == 'NULL' || empty($origemHorario))
        {
            $origemHorario = NULL;
        }

        $colunas = array(
            "diaSemana",
            "horarioInicio",
            "horarioFim",
            "horario24Horas",
            "idServico",
            "idEspecialidade",
            "origemHorario",
        );

        $values = array(
            $diaSemana,
            $h_inicio,
            $h_fim,
            $h_24hs,
            $idServico,
            $idEspecialidade,
            $origemHorario,
        );
        

        $returned_id = DB::insert("CSM_HorarioAtendimento", $colunas)
                ->values($values)
                ->execute($this->_db)
        ;
        return $returned_id;
    }

    public function delete_horario_atendimento($idHorarioAtendimento)
    {
        $state = DB::delete('CSM_HorarioAtendimento')
                ->where('idHorarioAtendimento', '=', $idHorarioAtendimento)
                ->execute($this->_db)
        ;

        return $state;
    }

    public function delete_horarioatendimento_por_servico($vars)
    {
        $state = DB::delete('CSM_HorarioAtendimento')
                ->where('idServico', '=', $vars['idServico'])
                ->execute($this->_db)
        ;

        return $state;
    }
    
    public function delete_horarioatendimento_por_especialidade($vars)
    {
        $state = DB::delete('CSM_HorarioAtendimento')
                ->where('idEspecialidade', '=', $vars['idEspecialidade'])
                ->execute($this->_db)
        ;

        return $state;
    }

    public function select_horarios_atendimentos()
    {
        $especialidades_servicos = DB::select()
                ->from('CSM_EspecialidadeTemServico')
                ->execute($this->_db)
        ;

        return $especialidades_servicos;
    }

    public function select_horario_atendimento($vars)
    {
        

    
        switch ($vars['tipoContato']){
        
            case 'Serviço' :
                $campo = 'idServico';
                $valor = $vars['idServico'];
            break;
     
            case 'Especialidade' :
                $campo = 'idEspecialidade';
                $valor = $vars['idEspecialidade'];
            break;
        
            case 'Funcionario' :
                $campo = 'idFuncionario';
                $valor = $vars['idFuncionario'];
            break;

            case 'Departamento' :
                $campo = 'idDepartamento';
                $valor = $vars['idDepartamento'];
            break;
        }

        $horarios = DB::select()
                ->from('CSM_HorarioAtendimento')
                ->where($campo, '=', $valor)
                ->execute($this->_db)
        ;

        return $horarios;
    }
    

    public function select_quantidade_horarios_atendimentos()
    {
        return $this->select_horarios_atendimentos()->count();
    }

    
    public function cadastra_horarioatd($tipoEntidade, $idEntidade, $horas24Dom, $horarioin1, $horariofi1, $horas24Seg, $horarioin2, $horariofi2, $horas24Ter, $horarioin3, $horariofi3, $horas24Qua, $horarioin4, $horariofi4, $horas24Qui, $horarioin5, $horariofi5, $horas24Sex, $horarioin6, $horariofi6, $horas24Sab, $horarioin7, $horariofi7)
    {
        $res_idHorarioAtendimento = NULL;
        if ($tipoEntidade == 'Serviço')
        {
                            
            if ($horas24Dom == "S")
            {
                $res_idHorarioAtendimento[] = $this->insert_horario_atendimento('DOM', NULL, NULL, $horas24Dom, $idEntidade, NULL,$tipoEntidade);
            }
            elseif (!empty($horarioin1) && !empty($horariofi1))
            {
                $res_idHorarioAtendimento[] = $this->insert_horario_atendimento('DOM', $horarioin1, $horariofi1, 'N', $idEntidade, NULL,$tipoEntidade);
            }

            if ($horas24Seg == 'S')
            {
                $res_idHorarioAtendimento[] = $this->insert_horario_atendimento('SEG', NULL, NULL, $horas24Seg, $idEntidade, NULL,$tipoEntidade);
            }
            elseif (!empty($horarioin2) && !empty($horariofi2))
            {
                $res_idHorarioAtendimento[] = $this->insert_horario_atendimento('SEG', $horarioin2, $horariofi2, 'N', $idEntidade, NULL,$tipoEntidade);
            }

            if ($horas24Ter == 'S')
            {
                $res_idHorarioAtendimento[] = $this->insert_horario_atendimento('TER', NULL, NULL, $horas24Ter, $idEntidade, NULL,$tipoEntidade);
            }
            elseif (!empty($horarioin3) && !empty($horariofi3))
            {
                $res_idHorarioAtendimento[] = $this->insert_horario_atendimento('TER', $horarioin3, $horariofi3, 'N', $idEntidade, NULL, $tipoEntidade);
            }

            if ($horas24Qua == 'S')
            {
                $res_idHorarioAtendimento[] = $this->insert_horario_atendimento('QUA', NULL, NULL, $horas24Qua, $idEntidade, NULL,$tipoEntidade);
            }
            elseif (!empty($horarioin4) && !empty($horariofi4))
            {
                $res_idHorarioAtendimento[] = $this->insert_horario_atendimento('QUA', $horarioin4, $horariofi4, 'N', $idEntidade, NULL, $tipoEntidade);
            }

            if ($horas24Qui == 'S')
            {
                $res_idHorarioAtendimento[] = $this->insert_horario_atendimento('QUI', NULL, NULL, $horas24Qui, $idEntidade, NULL, $tipoEntidade);
            }
            elseif (!empty($horarioin5) && !empty($horariofi5))
            {
                $res_idHorarioAtendimento[] = $this->insert_horario_atendimento('QUI', $horarioin5, $horariofi5, 'N', $idEntidade, NULL,$tipoEntidade);
            }

            if ($horas24Sex == 'S')
            {
                $res_idHorarioAtendimento[] = $this->insert_horario_atendimento('SEX', NULL, NULL, $horas24Sex, $idEntidade, NULL, $tipoEntidade);
            }
            elseif (!empty($horarioin6) && !empty($horariofi6))
            {
                $res_idHorarioAtendimento[] = $this->insert_horario_atendimento('SEX', $horarioin6, $horariofi6, 'N', $idEntidade, NULL, $tipoEntidade);
            }

            if ($horas24Sab == 'S')
            {
                $res_idHorarioAtendimento[] = $this->insert_horario_atendimento('SAB', NULL, NULL, $horas24Sab, $idEntidade, NULL, $tipoEntidade);
            }
            elseif (!empty($horarioin7) && !empty($horariofi7))
            {
                $res_idHorarioAtendimento[] = $this->insert_horario_atendimento('SAB', $horarioin7, $horariofi7, 'N', $idEntidade, NULL, $tipoEntidade);
            }
        }
        elseif ($tipoEntidade == 'Especialidade')
        {
            if ($horas24Dom == 'S')
            {
                $res_idHorarioAtendimento[] = $this->insert_horario_atendimento('DOM', NULL, NULL, $horas24Dom, NULL, $idEntidade, $tipoEntidade);
            }
            elseif (!empty($horarioin1) && !empty($horariofi1))
            {
                $res_idHorarioAtendimento[] = $this->insert_horario_atendimento('DOM', $horarioin1, $horariofi1, 'N', NULL, $idEntidade, $tipoEntidade);
            }

            if ($horas24Seg == 'S')
            {
                $res_idHorarioAtendimento[] = $this->insert_horario_atendimento('SEG', NULL, NULL, $horas24Seg, NULL, $idEntidade, $tipoEntidade);
            }
            elseif (!empty($horarioin2) && !empty($horariofi2))
            {
                $res_idHorarioAtendimento[] = $this->insert_horario_atendimento('SEG', $horarioin2, $horariofi2, 'N', NULL, $idEntidade, $tipoEntidade);
            }

            if ($horas24Ter == 'S')
            {
                $res_idHorarioAtendimento[] = $this->insert_horario_atendimento('TER', NULL, NULL, $horas24Ter, NULL, $idEntidade, $tipoEntidade);
            }
            elseif (!empty($horarioin3) && !empty($horariofi3))
            {
                $res_idHorarioAtendimento[] = $this->insert_horario_atendimento('TER', $horarioin3, $horariofi3, 'N', NULL, $idEntidade, $tipoEntidade);
            }

            if ($horas24Qua == 'S')
            {
                $res_idHorarioAtendimento[] = $this->insert_horario_atendimento('QUA', NULL, NULL, $horas24Qua, NULL, $idEntidade, $tipoEntidade);
            }
            elseif (!empty($horarioin4) && !empty($horariofi4))
            {
                $res_idHorarioAtendimento[] = $this->insert_horario_atendimento('QUA', $horarioin4, $horariofi4, 'N', NULL, $idEntidade, NULL, $tipoEntidade);
            }

            if ($horas24Qui == 'S')
            {
                $res_idHorarioAtendimento[] = $this->insert_horario_atendimento('QUI', NULL, NULL, $horas24Qui, NULL, $idEntidade, NULL, $tipoEntidade);
            }
            elseif (!empty($horarioin5) && !empty($horariofi5))
            {
                $res_idHorarioAtendimento[] = $this->insert_horario_atendimento('QUI', $horarioin5, $horariofi5, 'N', NULL, $idEntidade, NULL, $tipoEntidade);
            }

            if ($horas24Sex == 'S')
            {
                $res_idHorarioAtendimento[] = $this->insert_horario_atendimento('SEX', NULL, NULL, $horas24Sex, NULL, $idEntidade, NULL, $tipoEntidade);
            }
            elseif (!empty($horarioin6) && !empty($horariofi6))
            {
                $res_idHorarioAtendimento[] = $this->insert_horario_atendimento('SEX', $horarioin6, $horariofi6, 'N', NULL, $idEntidade, NULL, $tipoEntidade);
            }

            if ($horas24Sab == 'S')
            {
                $res_idHorarioAtendimento[] = $this->insert_horario_atendimento('SAB', NULL, NULL, $horas24Sab, NULL, $idEntidade, NULL, $tipoEntidade);
            }
            elseif (!empty($horarioin7) && !empty($horariofi7))
            {
                $res_idHorarioAtendimento[] = $this->insert_horario_atendimento('SAB', $horarioin7, $horariofi7, 'N', NULL, $idEntidade, NULL, $tipoEntidade);
            }
        }
        return $res_idHorarioAtendimento;
    }

    public function update_horario_atendimento($vars)
    {
        
        $tipoContato = $vars['tipoContato'];
    
        if ($tipoContato == 'Especialidade')
        {
            $idEspecialidade = $vars['idEspecialidade'] ;
            $idServico = NULL;
        }
        elseif ($tipoContato == 'Serviço')
        {
            $idEspecialidade = NULL ;
            $idServico = $vars['idServico'];  
        }

        //busca todos os horários para este determinado id
        $Horarios = $this->select_horario_atendimento($vars);
        

        //verifica se já existem horários cadastrados e sinaliza para atualizar registro existente ou inserir um novo
        if (count($Horarios) == 0)
        {
            $atl = 0;
        }
        else
        {
           $atl = 1; 
        }

        //Este for verifica se exitem os indices 24horas1 à 24horas7 - caso não, cria o indice no array e inicializa com 'N'
        for ($i = 1; $i <= 7; $i++)
        {
            if (!isset($vars['24horas'. $i]))
            {
                $vars['24horas' . $i] = 'N';
            }
        }
        
        $dias = array(1=>'DOM', 2=>'SEG', 3=>'TER', 4=>'QUA', 5=>'QUI', 6=>'SEX', 7=>'SAB');

       //Flag $atl == 0 indica que neste bloco serão executadas as inserçoes de todos os horários existentes, pois não existe nenhum horário cadastrado
        if ($atl == 0)
        {
            for ($i = 1; $i <= 7; $i++)
            {
                if (($vars['24horas' . $i] = 'S') || (($vars['horarioin' . $i] != "00:00") && ($vars['horariofi' . $i] != "00:00")))
                {
                    $diaSemana = $dias[$i];
                    $h_inicio = $vars['horarioin' . $i];
                    $h_fim = $vars['horariofi' . $i];
                    $h_24hs = $vars['24horas' . $i];
                    $state = $this->insert_horario_atendimento($diaSemana, $h_inicio, $h_fim, $h_24hs, $idServico, $idEspecialidade, $tipoContato);
                }
              
            }
        }
        elseif ($alt = 1) //Flag $atl == 1 consta que já existem horários cadastrados, valida o horário ou checkbox 24 horas e atualiza o dia da semana correspondente.
        {
            for ($h = 1; $h <= 7; $h++)
            {
                foreach ($Horarios as $chave => $valor)
                {
                    if ($valor['diaSemana'] == $dias[$h])
                    {
                        if ((($vars['24horas' . $h] == 'S') && ($vars['horarioin' . $h] == "00:00") && ($vars['horariofi' . $h] == "00:00")) || (($vars['24horas' . $h] == 'N') && ($vars['horarioin' . $h] != "00:00") && ($vars['horariofi' . $h] != "00:00")))
                        {
                            $idHorarioAtendimento = $vars[$dias[$h]];
                            $pairs = array(
                            "horarioInicio"=>$vars['horarioin' . $h],
                            "horarioFim"=>$vars['horariofi' . $h],
                            "horario24Horas"=>$vars['24horas' . $h],
                            "idServico"=>$idServico,
                            "idEspecialidade"=>$idEspecialidade,
                            "origemHorario"=>$tipoContato);
                                    
                            $state = DB::update('CSM_HorarioAtendimento')
                            ->set($pairs)
                            ->where('idHorarioAtendimento', '=', $idHorarioAtendimento)
                            ->execute($this->_db);
                        }

                    }                    
                }
            }
        }

    }
}


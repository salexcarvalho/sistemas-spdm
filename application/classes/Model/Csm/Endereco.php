<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Csm_Endereco extends Model
{

    /**
     * Construtor da classe, já seta a conexao com o banco de dados
     *
     * @param string $con_type
     */
    public function __construct($db = NULL)
    {
        if ($db != NULL)
        {
            // Set the instance or name
            $this->_db = $db;
        }
        elseif (!$this->_db)
        {
            // Use the default name
            $this->_db = Database::$default;
        }

        if (is_string($this->_db))
        {
            // Load the database
            $this->_db = Database::instance($this->_db);
        }
    }

    public function select_enderecos()
    {
        $enderecos = DB::select('CSM_Endereco.idEndereco', 'CSM_Endereco.endLogradouro', 'CSM_Endereco.nuLogradouro', 'CSM_Endereco.complemento', 
                'CSM_Endereco.CEP', 'CSM_Endereco.bairro', 'CSM_Endereco.cidade', 'CSM_Endereco.estado', 'CSM_Endereco.nuLatitude', 
                'CSM_Endereco.nuLongitude', 'CSM_Endereco.ativo')
                ->from('CSM_Endereco')
                ->execute($this->_db)
        ;

        return $enderecos;
    }

    public function select_endereco_por_endereco($vars)
    {
        $endereco = DB::select('CSM_Endereco.idEndereco', 'CSM_Endereco.endLogradouro', 'CSM_Endereco.nuLogradouro', 'CSM_Endereco.complemento',
                'CSM_Endereco.CEP', 'CSM_Endereco.bairro', 'CSM_Endereco.cidade', 'CSM_Endereco.estado', 'CSM_Endereco.nuLatitude',
                'CSM_Endereco.nuLongitude', 'CSM_Endereco.ativo')
                ->from('CSM_Endereco')
                ->where('CSM_Endereco.idEndereco', '=', $vars['idEndereco'])
                ->execute($this->_db)
        ;

        return $endereco;
    }

    public function update_endereco($vars)
    {
        $pairs = array(
            'endLogradouro' => $vars['endLogradouro'],
            'nuLogradouro' => $vars['nuLogradouro'],
            'complemento' => $vars['complemento'],
            'CEP' => $vars['CEP'],
            'bairro' => $vars['bairro'],
            'cidade' => $vars['cidade'],
            'estado' => $vars['estado'],
        );

        $state = DB::update('CSM_Endereco')
                ->set($pairs)
                ->where('idEndereco', '=', $vars['idEndereco'])
                ->execute($this->_db)
        ;
        return $state;
    }

    public function select_quantidade_enderecos()
    {
        return $this->select_enderecos()->count();
    }

    public function insert_endereco($vars)
            
            
    {
        if (empty($vars['endLogradouro']))
        {
            $vars['endLogradouro'] = NULL;
        }
        if (empty($vars['nuLogradouro']))
        {
            $vars['nuLogradouro'] = NULL;
        }
        if (empty($vars['complemento']))
        {
            $vars['complemento'] = NULL;
        }
        if (empty($vars['CEP']))
        {
            $vars['CEP'] = NULL;
        }
        if (empty($vars['bairro']))
        {
            $vars['bairro'] = NULL;
        }
        if (empty($vars['cidade']))
        {
            $vars['cidade'] = NULL;
        }
        if (empty($vars['estado']))
        {
            $vars['estado'] = NULL;
        }
        if ($vars['endLogradouro'] == NULL && $vars['nuLogradouro'] == NULL && $vars['complemento'] == NULL && $vars['CEP'] == NULL && $vars['bairro'] == NULL && $vars['cidade'] == NULL && $vars['estado'] == NULL)
        {
            return NULL;
        }

        $colunas = array(
            "endLogradouro",
            "nuLogradouro",
            "complemento",
            "CEP",
            "bairro",
            "cidade",
            "estado",
        );

        $values = array(
            $vars['endLogradouro'],
            $vars['nuLogradouro'],
            $vars['complemento'],
            $vars['CEP'],
            $vars['bairro'],
            $vars['cidade'],
            $vars['estado'],
        );

        $returned_id = DB::insert("CSM_Endereco", $colunas)
                ->values($values)
                ->execute($this->_db)
        ;
        return $returned_id;
    }

    public function update_ativar_endereco($idEndereco)
    {
        $endereco = DB::select('CSM_Endereco.ativo')
                ->from('CSM_Endereco')
                ->where('idEndereco', '=', $idEndereco)
                ->execute($this->_db)
        ;

        if ($endereco[0]['ativo'] == 'S')
        {
            $pairs = array(
                'ativo' => 'N',
            );
        }
        else
        {
            $pairs = array(
                'ativo' => 'S',
            );
        }

        $state = DB::update('CSM_Endereco')
                ->set($pairs)
                ->where('idEndereco', '=', $idEndereco)
                ->execute($this->_db)
        ;
        return $state;
    }

    public function delete_endereco($idEndereco)
    {
        $state = DB::delete('CSM_Endereco')
                ->where('idEndereco', '=', $idEndereco)
                ->execute($this->_db)
        ;

        return $state;
    }

}

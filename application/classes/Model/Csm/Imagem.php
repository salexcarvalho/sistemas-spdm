<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Csm_Imagem extends Model
{

    /**
     * Construtor da classe, já seta a conexao com o banco de dados
     *
     * @param string $con_type
     */
    public function __construct($db = NULL)
    {
        if ($db != NULL)
        {
            // Set the instance or name
            $this->_db = $db;
        }
        elseif (!$this->_db)
        {
            // Use the default name
            $this->_db = Database::$default;
        }

        if (is_string($this->_db))
        {
            // Load the database
            $this->_db = Database::instance($this->_db);
        }
    }

    private function insert_imagem($caminhoImagem, $nmImagem, $dsImagem, $nmImagemAntigo, $tamanhoImagem, $tipoImagem, $idServico, $idEspecialidade, $idFuncionario, $tipoOrigem)
    {
        if ($caminhoImagem == 'NULL' || empty($caminhoImagem))
        {
            $caminhoImagem = NULL;
            return FALSE;
        }
        if ($nmImagem == 'NULL' || empty($nmImagem))
        {
            $nmImagem = NULL;
            return FALSE;
        }
        if ($tipoOrigem == 'NULL' || empty($tipoOrigem))
        {
            $tipoOrigem = NULL;
            return FALSE;
        }
        if ($dsImagem == 'NULL' || empty($dsImagem))
        {
            $dsImagem = NULL;
        }
        if ($nmImagemAntigo == 'NULL' || empty($nmImagemAntigo))
        {
            $nmImagemAntigo = NULL;
        }
        if ($tamanhoImagem == 'NULL' || empty($tamanhoImagem))
        {
            $tamanhoImagem = NULL;
        }
        if ($tipoImagem == 'NULL' || empty($tipoImagem))
        {
            $tipoImagem = NULL;
        }
        if ($idServico == 'NULL' || empty($idServico))
        {
            $idServico = NULL;
        }
        if ($idEspecialidade == 'NULL' || empty($idEspecialidade))
        {
            $idEspecialidade = NULL;
        }
        if ($idFuncionario == 'NULL' || empty($idFuncionario))
        {
            $idFuncionario = NULL;
        }

        $colunas = array(
            "caminhoImagem",
            "nmImagem",
            "dsImagem",
            "nmImagemAntigo",
            "tamanhoImagem",
            "tipoImagem",
            "idServico",
            "idEspecialidade",
            "idFuncionario",
            "tipoOrigem",
        );

        $values = array(
            $caminhoImagem,
            $nmImagem,
            $dsImagem,
            $nmImagemAntigo,
            $tamanhoImagem,
            $tipoImagem,
            $idServico,
            $idEspecialidade,
            $idFuncionario,
            $tipoOrigem,
        );

        $returned_id = DB::insert("CSM_Imagem", $colunas)
                ->values($values)
                ->execute($this->_db)
        ;
        return $returned_id;
    }

    public function select_imagem_servico($idServico)
    {
        if(!$idServico)
        {
            return array();
        }
        $imagem_servicos = DB::select()
                ->from('CSM_Imagem')
                ->where('idServico', '=', $idServico)
                ->execute($this->_db)
        ;

        return $imagem_servicos;
    }
    
    public function select_imagem_por_funcionario($vars)
    {
        $imagens = DB::select()
                ->from('CSM_Imagem')
                ->where('idFuncionario', '=', $vars['idFuncionario'])
                ->execute($this->_db)
        ;

        return $imagens;
    }
    
    public function select_imagem_especialidade($idEspecialidade)
    {
        if(!$idEspecialidade)
        {
            return array();
        }
        $imagem_especialidades = DB::select()
                ->from('CSM_Imagem')
                ->where('idEspecialidade', '=', $idEspecialidade)
                ->execute($this->_db)
        ;

        return $imagem_especialidades;
    }
    /**
     * Faz o upload de imagens.
     * @param string $tipoEntidade Nome da entidade, também é utilizado como nome da pasta de upload. Só irá fazer o upload do arquivo se a pasta informada existir.
     * @param type $idEntidade ID do tipo de entidade, exemplos : $res_idFuncionario[0](17), $res_idServico[0](48).
     * @param type $nameUpload Valor do atributo name do formulário.
     * @param type $dsImagem Valor do textarea de descrição da imagem do formulário.
     * @return type
     */
    public function upload_imagem($tipoEntidade, $idEntidade, $nameUpload, $dsImagem)
    {
        $allowedExts = array("gif", "jpeg", "jpg", "png");
        $temp = explode(".", $_FILES[$nameUpload]["name"]);
        $extension = end($temp);
       
        // Novo nome da imagem = 'id_' + id do funcionário + '_' + img( ft1, ft2...)
        // exemplo, id_13_ft1
        $newimgname = 'id_' . $idEntidade . '_' . $nameUpload;
        //
        if ((($_FILES[$nameUpload]["type"] == "image/gif") || ($_FILES[$nameUpload]["type"] == "image/jpeg") || ($_FILES[$nameUpload]["type"] == "image/jpg") || ($_FILES[$nameUpload]["type"] == "image/pjpeg") || ($_FILES[$nameUpload]["type"] == "image/x-png") || ($_FILES[$nameUpload]["type"] == "image/png")) && in_array($extension, $allowedExts))
        {
            if (!$_FILES[$nameUpload]["error"] > 0)
            {
                if (!file_exists($_SERVER['DOCUMENT_ROOT'] . URL::base() . 'upload/img/' . $tipoEntidade . '/' . $newimgname . '.' . $extension))
                {
                    $upload = move_uploaded_file($_FILES[$nameUpload]["tmp_name"], $_SERVER['DOCUMENT_ROOT'] . URL::base() . 'upload/img/' . $tipoEntidade . '/' . $newimgname . '.' . $extension);
                    if ($tipoEntidade == 'funcionario')
                    {
                        $this->insert_imagem( 'upload/img/' . $tipoEntidade . '/', $newimgname . '.' . $extension, $dsImagem, $_FILES[$nameUpload]["name"], $_FILES[$nameUpload]["size"] / 1024 . ' kB', $_FILES[$nameUpload]["type"], NULL, NULL,$idEntidade, 'Funcionário');
                        return $upload;
                    }
                    elseif ($tipoEntidade == 'servico')
                    {
                        $this->insert_imagem('upload/img/' . $tipoEntidade . '/', $newimgname . '.' . $extension, $dsImagem, $_FILES[$nameUpload]["name"], $_FILES[$nameUpload]["size"] / 1024 . ' kB', $_FILES[$nameUpload]["type"], $idEntidade, NULL, NULL, 'Serviço');
                        return $upload;
                    }
                    elseif ($tipoEntidade == 'especialidade')
                    {
                        $this->insert_imagem('upload/img/' . $tipoEntidade . '/', $newimgname . '.' . $extension, $dsImagem, $_FILES[$nameUpload]["name"], $_FILES[$nameUpload]["size"] / 1024 . ' kB', $_FILES[$nameUpload]["type"], NULL, $idEntidade, NULL, NULL, 'Especialidade');
                        return $upload;
                    }
                     elseif ($tipoEntidade == 'certificados')
                    {
                        $this->insert_imagem('upload/img/' . $tipoEntidade . '/', $newimgname . '.' . $extension, $dsImagem, $_FILES[$nameUpload]["name"], $_FILES[$nameUpload]["size"] / 1024 . ' kB', $_FILES[$nameUpload]["type"], NULL, $idEntidade, NULL, NULL, 'Certificados');
                        return $upload;
                    }
                    else
                    {
                        $this->insert_imagem('upload/img/' . $tipoEntidade . '/', $newimgname . '.' . $extension, $dsImagem, $_FILES[$nameUpload]["name"], $_FILES[$nameUpload]["size"] / 1024 . ' kB', $_FILES[$nameUpload]["type"], NULL, NULL, NULL, NULL, 'Outro');
                        return $upload;
                    }
                }
            }
        }
        else
        {
            return $error_upload[] = 'Arquivo inválido';
        }
    }
    
        /**
     * Faz o upload de imagens.
     * @param string $tipoEntidade Nome da entidade, também é utilizado como nome da pasta de upload. Só irá fazer o upload do arquivo se a pasta informada existir.
     * @param type $idEntidade ID do tipo de entidade, exemplos : $res_idFuncionario[0](17), $res_idServico[0](48).
     * @param type $nameUpload Valor do atributo name do formulário.
     * @param type $dsImagem Valor do textarea de descrição da imagem do formulário.
     * @return type
     */
    public function edita_upload_imagem($tipoEntidade, $idEntidade, $nameUpload, $dsImagem)
    {
        $allowedExts = array("gif", "jpeg", "jpg", "png");
        $temp = explode(".", $_FILES[$nameUpload]["name"]);
        $extension = end($temp);
      
        
        // Novo nome da imagem = 'id_' + id do funcionário + '_' + img( ft1, ft2...)
        // exemplo, id_13_ft1
        $newimgname ='';
        $newimgname = 'id_update_' . $idEntidade . '_' . $nameUpload;
        
        if ((($_FILES[$nameUpload]["type"] == "image/gif") || ($_FILES[$nameUpload]["type"] == "image/jpeg") || ($_FILES[$nameUpload]["type"] == "image/jpg") || ($_FILES[$nameUpload]["type"] == "image/pjpeg") || ($_FILES[$nameUpload]["type"] == "image/x-png") || ($_FILES[$nameUpload]["type"] == "image/png")) && in_array($extension, $allowedExts))
        {
            if (!$_FILES[$nameUpload]["error"] > 0)
            { 
                    $upload = move_uploaded_file($_FILES[$nameUpload]["tmp_name"], $_SERVER['DOCUMENT_ROOT'] . URL::base() . 'upload/img/' . $tipoEntidade . '/' . $newimgname . '.' . $extension);
                    if ($tipoEntidade == 'funcionario')
                    {
                        $this->update_imagem('upload/img/' . $tipoEntidade . '/', $newimgname . '.' . $extension, $dsImagem, $_FILES[$nameUpload]["name"], $_FILES[$nameUpload]["size"] / 1024 . ' kB', $_FILES[$nameUpload]["type"], $idEntidade, 'Funcionário');
                        return $upload;
                    }
                    elseif ($tipoEntidade == 'servico')
                    {   
                        $this->update_imagem('upload/img/' . $tipoEntidade . '/', $newimgname . '.' . $extension, $dsImagem, $_FILES[$nameUpload]["name"], $_FILES[$nameUpload]["size"] / 1024 . ' kB', $_FILES[$nameUpload]["type"], $idEntidade, 'Serviço');
                        return $upload;
                    }
                    elseif ($tipoEntidade == 'especialidade')
                    {
                        $this->update_imagem('upload/img/' . $tipoEntidade . '/', $newimgname . '.' . $extension, $dsImagem, $_FILES[$nameUpload]["name"], $_FILES[$nameUpload]["size"] / 1024 . ' kB', $_FILES[$nameUpload]["type"], $idEntidade, 'Especialidade');
                        return $upload;
                    }
                    else
                    {
                        $this->update_imagem('upload/img/' . $tipoEntidade . '/', $newimgname . '.' . $extension, $dsImagem, $_FILES[$nameUpload]["name"], $_FILES[$nameUpload]["size"] / 1024 . ' kB', $_FILES[$nameUpload]["type"], $idEntidade, 'Outro');
                        return $upload;
                    }
            }
        }
        else
        {
            return $error_upload[] = 'Arquivo inválido';
        }
    }

    
    private function update_imagem($caminhoImagem, $nmImagem, $dsImagem, $nmImagemAntigo, $tamanhoImagem, $tipoImagem, $idImagem, $tipoOrigem)
    {
        $pairs = array(
            'caminhoImagem' => $caminhoImagem,
            'nmImagem' => $nmImagem,
            'dsImagem' => $dsImagem,
            'nmImagemAntigo' => $nmImagemAntigo,
            'tamanhoImagem' => $tamanhoImagem,
            'tipoImagem' => $tipoImagem,
            'tipoOrigem' => $tipoOrigem
        );
        
        $state = DB::update('CSM_Imagem')
                ->set($pairs)
                ->where('idImagem', '=', $idImagem)
                ->execute($this->_db)
        ;
        return $state;
    }
    
    public function delete_imagem_por_servico($vars)
    {
        $state = DB::delete('CSM_Imagem')
                ->where('idServico', '=', $vars['idServico'])
                ->execute($this->_db)
        ;
        return $state;
    }

    public function delete_imagem_por_funcionario($vars)
    {
        $state = DB::delete('CSM_Imagem')
                ->where('idFuncionario', '=', $vars['idFuncionario'])
                ->execute($this->_db)
        ;
        return $state;
    }
}

<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Csm_Artigo extends Model
{

    /**
     * Construtor da classe, já seta a conexao com o banco de dados
     *
     * @param string $con_type
     */
    public function __construct($db = NULL)
    {
        if ($db != NULL)
        {
            // Set the instance or name
            $this->_db = $db;
        }
        elseif (!$this->_db)
        {
            // Use the default name
            $this->_db = Database::$default;
        }

        if (is_string($this->_db))
        {
            // Load the database
            $this->_db = Database::instance($this->_db);
        }
    }

    public function select_artigos_conheca()
    {
        return $this->select_artigos(2);
    }

    public function select_artigos_apresentacao()
    {
        return $this->select_artigos(3);
    }

    public function select_artigos_profissionais()
    {
        return $this->select_artigos(4);
    }
    
    public function select_artigos($categoria)
    {
        $artigos = DB::select('CSM_Artigo.tituloArtigo', 'CSM_Artigo.aliasArtigo', 'CSM_Artigo.textoIntro', 'CSM_Artigo.textoCompleto', 'CSM_Artigo.publicado',
                'CSM_Artigo.destaque', 'CSM_Artigo.idImagem', 'CSM_Imagem.caminhoImagem', 'CSM_Imagem.nmImagem')
                ->from('CSM_Artigo')
                ->join('CSM_Imagem', 'LEFT')
                ->on('CSM_Artigo.idImagem', '=', 'CSM_Imagem.idImagem')
                ->where('CSM_Artigo.idCategoria', '=', $categoria)
                ->execute($this->_db)
        ;

        return $artigos;
    }

}

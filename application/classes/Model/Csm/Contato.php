<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Csm_Contato extends Model
{

    /**
     * Construtor da classe, já seta a conexao com o banco de dados
     *
     * @param string $con_type
     */
    public function __construct($db = NULL)
    {
        if ($db != NULL)
        {
            // Set the instance or name
            $this->_db = $db;
        }
        elseif (!$this->_db)
        {
            // Use the default name
            $this->_db = Database::$default;
        }

        if (is_string($this->_db))
        {
            // Load the database
            $this->_db = Database::instance($this->_db);
        }
    }

    public function select_contatos_dep()
    {
        $contatos = DB::select('CSM_Contato.idContato', 'CSM_Contato.mostrarContatoNoSite', 'CSM_Contato.telefone', 'CSM_Contato.nuVOIP', 'CSM_Contato.email',
                'CSM_Contato.site', 'CSM_Contato.facebook', 'CSM_Contato.twitter', 'CSM_Contato.googleplus', 'CSM_Contato.linkedIn', 'CSM_Contato.tipoContato')
                ->from('CSM_Contato')
                ->where('CSM_Contato.tipoContato', '=', 'D')
                ->execute($this->_db)
        ;

        return $contatos;
    }

    public function select_contatos()
    {
        $contatos = DB::select('CSM_Contato.idContato', 'CSM_Contato.mostrarContatoNoSite', 'CSM_Contato.telefone', 'CSM_Contato.nuVOIP', 'CSM_Contato.email',
                'CSM_Contato.site', 'CSM_Contato.facebook', 'CSM_Contato.twitter', 'CSM_Contato.googleplus', 'CSM_Contato.linkedIn', 'CSM_Contato.tipoContato',
                'CSM_Contato.ativo', 'CSM_Servico.nmServico', 'CSM_Funcionario.nmFuncionario', 'CSM_Especialidade.nmEspecialidade')
                ->from('CSM_Contato')
                ->join('CSM_Servico', 'LEFT')
                ->on('CSM_Contato.idContato', '=', 'CSM_Servico.idContato')
                ->join('CSM_Funcionario', 'LEFT')
                ->on('CSM_Contato.idContato', '=', 'CSM_Funcionario.idContato')
                ->join('CSM_Especialidade', 'LEFT')
                ->on('CSM_Contato.idContato', '=', 'CSM_Especialidade.idContato')
                ->execute($this->_db)
        ;

        return $contatos;
    }

    public function select_contato($idContato)
    {
        $contatos = DB::select('CSM_Contato.idContato', 'CSM_Contato.mostrarContatoNoSite', 'CSM_Contato.telefone', 'CSM_Contato.nuVOIP', 'CSM_Contato.email',
                'CSM_Contato.site', 'CSM_Contato.facebook', 'CSM_Contato.twitter', 'CSM_Contato.googleplus', 'CSM_Contato.linkedIn', 'CSM_Contato.tipoContato',
                'CSM_Contato.ativo')
                ->from('CSM_Contato')
                ->where('CSM_Contato.idContato', '=', $idContato)
                ->execute($this->_db)
        ;

        return $contatos;
    }

    public function select_quantidade_contatos()
    {
        return $this->select_contatos()->count();
    }

    public function insert_contato($vars)
    {
        foreach ($vars as $key => $var)
        {
            if (empty($var))
            {
                $vars[$key] = NULL;
            }
        }
        
        $usuario = Auth::instance()->get_user();
        
        $colunas = array(
            "mostrarContatoNoSite",
            "telefone",
            "nuVOIP",
            "email",
            "site",
            "facebook",
            "twitter",
            "googleplus",
            "linkedIn",
            "tipoContato",
            "usuarioCadastro",
        );

        $values = array(
            $vars['mostrarContatoNoSite'],
            $vars['telefone'],
            $vars['nuVOIP'],
            $vars['email'],
            $vars['site'],
            $vars['facebook'],
            $vars['twitter'],
            $vars['googleplus'],
            $vars['linkedIn'],
            $vars['tipoContato'],
            $usuario
        );

        $returned_id = DB::insert("CSM_Contato", $colunas)
                ->values($values)
                ->execute($this->_db)
        ;
        return $returned_id;
    }

    public function update_contato($vars)
    {
        // Formatação dos valores para inserir no banco.
        // Coloca campos vazios como nulo.
        foreach ($vars as $key => $var)
        {
            if (empty($var))
            {
                $vars[$key] = NULL;
            }
        }
        // Fim da formatação.

        if ($vars['mostrarContatoNoSite'] != 'S')
        {
            $vars['mostrarContatoNoSite'] = 'N';
        }

        $pairs = array(
            'mostrarContatoNoSite' => $vars['mostrarContatoNoSite'],
            'telefone' => $vars['telefone'],
            'nuVOIP' => $vars['nuVOIP'],
            'email' => $vars['email'],
            'site' => $vars['site'],
            'facebook' => $vars['facebook'],
            'twitter' => $vars['twitter'],
            'googleplus' => $vars['googleplus'],
            'linkedIn' => $vars['linkedIn'],
            'tipoContato' => $vars['tipoContato'],
        );

        $state = DB::update('CSM_Contato')
                ->set($pairs)
                ->where('idContato', '=', $vars['idContato'])
                ->execute($this->_db)
        ;
        return $state;
    }

    public function update_ativar_contato($idContato)
    {
        $contato = DB::select('CSM_Contato.ativo')
                ->from('CSM_Contato')
                ->where('idContato', '=', $idContato)
                ->execute($this->_db)
        ;

        if ($contato[0]['ativo'] == 'S')
        {
            $pairs = array(
                'ativo' => 'N',
            );
        }
        else
        {
            $pairs = array(
                'ativo' => 'S',
            );
        }

        $state = DB::update('CSM_Contato')
                ->set($pairs)
                ->where('idContato', '=', $idContato)
                ->execute($this->_db)
        ;
        return $state;
    }

    public function update_mostrar_contato($idContato)
    {
        $contato = DB::select('CSM_Contato.mostrarContatoNoSite')
                ->from('CSM_Contato')
                ->where('idContato', '=', $idContato)
                ->execute($this->_db)
        ;

        if ($contato[0]['mostrarContatoNoSite'] == 'S')
        {
            $pairs = array(
                'mostrarContatoNoSite' => 'N',
            );
        }
        else
        {
            $pairs = array(
                'mostrarContatoNoSite' => 'S',
            );
        }

        $state = DB::update('CSM_Contato')
                ->set($pairs)
                ->where('idContato', '=', $idContato)
                ->execute($this->_db)
        ;
        return $state;
    }

    public function delete_contato($vars)
    {
        
        $state = DB::delete('CSM_Contato')
                ->where('idContato', '=', $vars['idContato'])
                ->execute($this->_db)
        ;

        return $state;
    }

}

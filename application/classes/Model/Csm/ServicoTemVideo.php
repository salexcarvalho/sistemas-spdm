<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Csm_ServicoTemVideo extends Model
{

    /**
     * Construtor da classe, já seta a conexao com o banco de dados
     *
     * @param string $con_type
     */
    public function __construct($db = NULL)
    {
        if ($db != NULL)
        {
            // Set the instance or name
            $this->_db = $db;
        }
        elseif (!$this->_db)
        {
            // Use the default name
            $this->_db = Database::$default;
        }

        if (is_string($this->_db))
        {
            // Load the database
            $this->_db = Database::instance($this->_db);
        }
    }

    public function delete_servico_video_por_servico($vars)
    {
        $state = DB::delete('CSM_ServicoTemVideo')
                ->where('idServico', '=', $vars['idServico'])
                ->execute($this->_db)
        ;

        return $state;
    }

}

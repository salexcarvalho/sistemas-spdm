<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Csm_Especialidade extends Model
{

    /**
     * Construtor da classe, já seta a conexao com o banco de dados
     *
     * @param string $db Opção de conexão do banco de dados, exemplo : test, default
     */
    public function __construct($db = NULL)
    {
        if ($db != NULL)
        {
            // Set the instance or name
            $this->_db = $db;
        }
        elseif (!$this->_db)
        {
            // Use the default name
            $this->_db = Database::$default;
        }

        if (is_string($this->_db))
        {
            // Load the database
            $this->_db = Database::instance($this->_db);
        }
    }
    
    public function select_contato_especialidade($idContato)
    {
        $contatos = DB::select('CSM_Contato.ativo', 'CSM_Contato.idContato', 'CSM_Contato.mostrarContatoNoSite', 'CSM_Contato.telefone', 'CSM_Contato.nuVOIP',
                'CSM_Contato.email', 'CSM_Contato.site', 'CSM_Contato.facebook', 'CSM_Contato.twitter', 'CSM_Contato.googleplus', 'CSM_Contato.linkedIn',
                'CSM_Contato.tipoContato', 'CSM_Especialidade.nmEspecialidade')
                ->from('CSM_Especialidade')
                ->join('CSM_Contato', 'INNER')
                ->on('CSM_Contato.idContato', '=', 'CSM_Especialidade.idContato')
                ->where('CSM_Contato.idContato', '=', $idContato)
                ->execute($this->_db)
        ;

        return $contatos;
    }
    
    public function select_contatos_especialidades()
    {
        $contatos = DB::select('CSM_Contato.ativo', 'CSM_Contato.idContato', 'CSM_Contato.mostrarContatoNoSite', 'CSM_Contato.telefone', 'CSM_Contato.nuVOIP',
                'CSM_Contato.email', 'CSM_Contato.site', 'CSM_Contato.facebook', 'CSM_Contato.twitter', 'CSM_Contato.googleplus', 'CSM_Contato.linkedIn',
                'CSM_Contato.tipoContato', 'CSM_Especialidade.nmEspecialidade')
                ->from('CSM_Especialidade')
                ->join('CSM_Contato', 'INNER')
                ->on('CSM_Contato.idContato', '=', 'CSM_Especialidade.idContato')
                ->execute($this->_db)
        ;

        return $contatos;
    }
    
    public function select_especialidades_sem_contato()
    {
        $contatos = DB::select()
                ->from('CSM_Especialidade')
                ->where('CSM_Especialidade.idContato', 'IS', NULL)
                ->execute($this->_db)
        ;

        return $contatos;
    }

    public function insert_especialidade($nmEspecialidade, $dsEspecialidade, $nuCentroCusto, $idEndereco, $idFuncionarioChefe, $idContato)
    {
        if ($nmEspecialidade == 'NULL' || empty($nmEspecialidade))
        {
            $nmEspecialidade = NULL;
        }
        if ($dsEspecialidade == 'NULL' || empty($dsEspecialidade))
        {
            $dsEspecialidade = NULL;
        }
        if ($nuCentroCusto == 'NULL' || empty($nuCentroCusto))
        {
            $nuCentroCusto = NULL;
        }
        if ($idEndereco == 'NULL' || empty($idEndereco))
        {
            $idEndereco = NULL;
        }
        if ($idContato == 'NULL' || empty($idContato))
        {
            $idContato = NULL;
        }
        if ($idFuncionarioChefe == 'NULL' || empty($idFuncionarioChefe))
        {
            $idFuncionarioChefe = NULL;
        }

        $colunas = array(
            "nmEspecialidade",
            "dsEspecialidade",
            "nuCentroCusto",
            "dtCadastro",
            "idEndereco",
            "idFuncionarioChefe",
            "idContato",
        );

        $values = array(
            $nmEspecialidade,
            $dsEspecialidade,
            $nuCentroCusto,
            DB::expr('NOW()'),
            $idEndereco,
            $idFuncionarioChefe,
            $idContato
        );

        $returned_id = DB::insert("CSM_Especialidade", $colunas)
                ->values($values)
                ->execute($this->_db)
        ;
        return $returned_id;
    }
    
    public function select_especialidades_contatos()
    {
        $contatos = DB::select()
                ->from('CSM_Especialidade')
                ->join('CSM_Contato', 'INNER')
                ->on('CSM_Contato.idContato', '=', 'CSM_Especialidade.idContato')
                ->execute($this->_db)
        ;

        return $contatos;
    }
    
    public function select_quantidade_especialidades_contatos()
    {
        return $this->select_especialidades_contatos()->count();
    }

    public function select_especialidades()
    {
        $especialidades = DB::select('CSM_Especialidade.idEspecialidade', 'CSM_Especialidade.nmEspecialidade', 'CSM_Especialidade.dsEspecialidade',
                'CSM_Especialidade.nuCentroCusto', 'CSM_Especialidade.dtCadastro', 'CSM_Especialidade.idEndereco', 'CSM_Especialidade.idFuncionarioChefe',
                'CSM_Especialidade.idContato', 'CSM_Especialidade.ativo', array('CSM_Funcionario.nmFuncionario', 'nmFuncionarioChefe'), 
                'CSM_Especialidade.idEndereco')
                ->from('CSM_Especialidade')
                ->join('CSM_Funcionario', 'LEFT')
                ->on('CSM_Funcionario.idFuncionario', '=', 'CSM_Especialidade.idFuncionarioChefe')
                ->order_by('CSM_Especialidade.nmEspecialidade', 'ASC')
                ->execute($this->_db)
        ;

        return $especialidades;
    }
    
        public function select_especialidade($idEspecialidade)
    {
        $especialidade = DB::select('CSM_Especialidade.idEspecialidade', 'CSM_Especialidade.nmEspecialidade', 'CSM_Especialidade.dsEspecialidade', 
                'CSM_Especialidade.nuCentroCusto', 'CSM_Especialidade.dtCadastro', 'CSM_Endereco.endLogradouro','CSM_Endereco.nuLogradouro',
                'CSM_Endereco.complemento', 'CSM_Endereco.CEP','CSM_Endereco.bairro','CSM_Endereco.cidade','CSM_Endereco.estado',
                'CSM_Especialidade.idFuncionarioChefe', 'CSM_Especialidade.idContato', 'CSM_Especialidade.ativo', 
                array('CSM_Funcionario.nmFuncionario', 'nmFuncionarioChefe'),'CSM_Especialidade.idEndereco')
                ->from('CSM_Especialidade')
                ->join('CSM_Funcionario', 'LEFT')
                ->on('CSM_Funcionario.idFuncionario', '=', 'CSM_Especialidade.idFuncionarioChefe')
                 ->join('CSM_Endereco', 'LEFT')
                ->on('CSM_Especialidade.idEndereco', '=', 'CSM_Endereco.idEndereco')
                ->where('idEspecialidade', '=', $idEspecialidade)
                ->order_by('CSM_Especialidade.nmEspecialidade', 'ASC')
                ->execute($this->_db)
        ;

        return $especialidade;
    }

    public function select_quantidade_especialidades()
    {
        return $this->select_especialidades()->count();
    }

    public function update_ativar_especialidade($idEspecialidade)
    {
        $especialidade = DB::select('CSM_Especialidade.ativo')
                ->from('CSM_Especialidade')
                ->where('idEspecialidade', '=', $idEspecialidade)
                ->execute($this->_db)
        ;

        if ($especialidade[0]['ativo'] == 'S')
        {
            $pairs = array(
                'ativo' => 'N',
            );
        }
        else
        {
            $pairs = array(
                'ativo' => 'S',
            );
        }

        $state = DB::update('CSM_Especialidade')
                ->set($pairs)
                ->where('idEspecialidade', '=', $idEspecialidade)
                ->execute($this->_db)
        ;
        return $state;
    }

    public function update_especialidade_contato($vars)
    {
        // Formatação dos valores para inserir no banco.
        // Coloca campos vazios como nulo.
        foreach ($vars as $key => $var)
        {
            if (empty($var))
            {
                $vars[$key] = NULL;
            }
        }
        // Fim da formatação.

        $pairs = array(
            'idContato' => $vars['novoIdContato'],
        );

        $state = DB::update('CSM_Especialidade')
                ->set($pairs)
                ->where('idContato', '=', $vars['idContato'])
                ->execute($this->_db)
        ;
        return $state;
    }
    
   public function update_especialidade_funcionario($vars)
    {
        // Formatação dos valores para inserir no banco.
        // Coloca campos vazios como nulo.
        foreach ($vars as $key => $var)
        {
            if (empty($var))
            {
                $vars[$key] = NULL;
            }
        }
        // Fim da formatação.

        $pairs = array(
            'idFuncionarioChefe' => $vars['novoIdFuncionario'],
        );

        $state = DB::update('CSM_Especialidade')
                ->set($pairs)
                ->where('idFuncionarioChefe', '=', $vars['idFuncionario'])
                ->execute($this->_db)
        ;
        return $state;
    }

    public function update_especialidade($vars)
    {
        // Formatação dos valores para inserir no banco.
        // Coloca campos vazios como nulo.
        foreach ($vars as $key => $var)
        {
            if (empty($var))
            {
                $vars[$key] = NULL;
            }
        }
        // Fim da formatação.

        $pairs = array(
            'nmEspecialidade' => $vars['nmEspecialidade'],
            'dsEspecialidade' => $vars['dsEspecialidade'],
            'nuCentroCusto' => $vars['nuCentroCusto'],
            'idEndereco' => $vars['idEndereco'],
            'idFuncionarioChefe' => $vars['chefeDep'],
            'idContato' => $vars['idContato']
        );

        $state = DB::update('CSM_Especialidade')
                ->set($pairs)
                ->where('idEspecialidade', '=', $vars['idEspecialidade'])
                ->execute($this->_db)
        ;
        return $state;
    }

     public function delete_especialidade($idEspecialidade)
    {
        $state = DB::delete('CSM_Especialidade')
                ->where('idEspecialidade', '=', $idEspecialidade)
                ->execute($this->_db)
        ;

        return $state;
    }
    
}

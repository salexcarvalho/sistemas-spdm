<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Csm_EspecialidadeTemServico extends Model
{

    /**
     * Construtor da classe, já seta a conexao com o banco de dados
     *
     * @param string $con_type
     */
    public function __construct($db = NULL)
    {
        if ($db != NULL)
        {
            // Set the instance or name
            $this->_db = $db;
        }
        elseif (!$this->_db)
        {
            // Use the default name
            $this->_db = Database::$default;
        }

        if (is_string($this->_db))
        {
            // Load the database
            $this->_db = Database::instance($this->_db);
        }
    }

    public function insert_especialidade_servico($idEspecialidade, $idServico)
    {
        if ($idEspecialidade == 'NULL' || empty($idEspecialidade))
        {
            return FALSE;
        }
        if ($idServico == 'NULL' || empty($idServico))
        {
            return FALSE;
        }

        $colunas = array(
            "idEspecialidade",
            "idServico",
        );

        $values = array(
            $idEspecialidade,
            $idServico,
        );

        $returned_id = DB::insert("CSM_EspecialidadeTemServico", $colunas)
                ->values($values)
                ->execute($this->_db)
        ;
        return $returned_id;
    }

    public function delete_especialidade_servico($espe, $idServico)
    {
        $state = DB::delete('CSM_EspecialidadeTemServico')
                ->where('idEspecialidade', '=', $espe)
                ->and_where('idServico', '=', $idServico)
                ->execute($this->_db)
        ;

        return $state;
    }

    public function delete_especialidade_servico_por_servico($vars)
    {
        
        $state = DB::delete('CSM_EspecialidadeTemServico')
                ->where('idServico', '=', $vars['idServico'])
                ->execute($this->_db)
        ;

        return $state;
    }

    public function select_especialidades_servicos()
    {
        $especialidades_servicos = DB::select()
                ->from('CSM_EspecialidadeTemServico')
                ->join('CSM_Especialidade', 'INNER')
                ->on('CSM_Especialidade.idEspecialidade', '=', 'CSM_EspecialidadeTemServico.idEspecialidade')
                ->join('CSM_Servico', 'INNER')
                ->on('CSM_Servico.idServico', '=', 'CSM_EspecialidadeTemServico.idServico')
                ->execute($this->_db)
        ;

        return $especialidades_servicos;
    }

    public function select_especialidades_por_servico($idServico)
    {
        $especialidades_servicos = DB::select()
                ->from('CSM_EspecialidadeTemServico')
                ->where('idServico', '=', $idServico)
                ->execute($this->_db)
        ;

        return $especialidades_servicos;
    }

    public function select_quantidade_especialidades_servicos()
    {
        return $this->select_especialidades_servicos()->count();
    }
    
    public function update_especialidade_servico($idEspecialidade,$idServico)
    {
                $state = DB::update('CSM_EspecialidadeTemServico')
                ->set(array('idEspecialidade' => $idEspecialidade) )
                ->where('idServico', '=', $idServico)
                ->execute($this->_db)
        ;
        return $state;
    }

}

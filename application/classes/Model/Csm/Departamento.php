<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Csm_Departamento extends Model
{

    /**
     * Construtor da classe, já seta a conexao com o banco de dados
     *
     * @param string $con_type
     */
    public function __construct($db = NULL)
    {
        if ($db != NULL)
        {
            // Set the instance or name
            $this->_db = $db;
        }
        elseif (!$this->_db)
        {
            // Use the default name
            $this->_db = Database::$default;
        }

        if (is_string($this->_db))
        {
            // Load the database
            $this->_db = Database::instance($this->_db);
        }
    }

    public function insert_departamento($nmDepartamento, $dsDepartamento, $idEndereco, $idContato, $idFuncionarioChefe)
    {
        if (empty($nmDepartamento))
        {
            $nmDepartamento = NULL;
        }
        if (empty($dsDepartamento))
        {
            $dsDepartamento = NULL;
        }
        if ($idEndereco == 'NULL' || empty($idEndereco))
        {
            $idEndereco = NULL;
        }
        if ($idContato == 'NULL' || empty($idContato))
        {
            $idContato = NULL;
        }
        if ($idFuncionarioChefe == 'NULL' || empty($idFuncionarioChefe))
        {
            $idFuncionarioChefe = NULL;
        }
        $returned_id = DB::insert("CSM_Departamento", array("nmDepartamento", "dsDepartamento", "idEndereco", "idContato", "idFuncionarioChefe"))
                ->values(array($nmDepartamento, $dsDepartamento, $idEndereco, $idContato, $idFuncionarioChefe))
                ->execute($this->_db)
        ;
        return $returned_id;
    }

    public function select_departamentos()
    {
        $departamentos = DB::select('CSM_Departamento.idDepartamento', 'CSM_Departamento.nmDepartamento', 'CSM_Departamento.dsDepartamento', 
                'CSM_Departamento.idEndereco', 'CSM_Departamento.idContato', 'CSM_Departamento.idFuncionarioChefe', 
                array('CSM_Funcionario.nmFuncionario', 'nmFuncionarioChefe'), 'CSM_Departamento.ativo', 'CSM_Contato.telefone', 'CSM_Endereco.endLogradouro', 
                'CSM_Endereco.nuLogradouro')
                ->from('CSM_Departamento')
                ->join('CSM_Funcionario', 'LEFT')
                ->on('CSM_Funcionario.idFuncionario', '=', 'CSM_Departamento.idFuncionarioChefe')
                ->join('CSM_Endereco', 'LEFT')
                ->on('CSM_Endereco.idEndereco', '=', 'CSM_Departamento.idEndereco')
                ->join('CSM_Contato', 'LEFT')
                ->on('CSM_Contato.idContato', '=', 'CSM_Departamento.idContato')
                ->execute($this->_db)
        ;

        return $departamentos;
    }
    
    public function select_departamento($idDepartamento)
    {
        $departamentos = DB::select('CSM_Departamento.idDepartamento', 'CSM_Departamento.nmDepartamento', 'CSM_Departamento.dsDepartamento',
                'CSM_Departamento.idEndereco', 'CSM_Departamento.idContato', 'CSM_Departamento.idFuncionarioChefe', 
                array('CSM_Funcionario.nmFuncionario', 'nmFuncionarioChefe'), 'CSM_Departamento.ativo', 'CSM_Contato.telefone', 'CSM_Endereco.endLogradouro',
                'CSM_Endereco.nuLogradouro', 'CSM_Endereco.complemento', 'CSM_Endereco.CEP', 'CSM_Endereco.bairro', 'CSM_Endereco.cidade',
                'CSM_Endereco.estado', 'CSM_Contato.mostrarContatoNoSite', 'CSM_Contato.nuVOIP', 'CSM_Contato.email', 'CSM_Contato.site',
                'CSM_Contato.facebook', 'CSM_Contato.twitter', 'CSM_Contato.googleplus', 'CSM_Contato.linkedIn')
                ->from('CSM_Departamento')
                ->join('CSM_Funcionario', 'LEFT')
                ->on('CSM_Funcionario.idFuncionario', '=', 'CSM_Departamento.idFuncionarioChefe')
                ->join('CSM_Endereco', 'LEFT')
                ->on('CSM_Endereco.idEndereco', '=', 'CSM_Departamento.idEndereco')
                ->join('CSM_Contato', 'LEFT')
                ->on('CSM_Contato.idContato', '=', 'CSM_Departamento.idContato')
                ->where('CSM_Departamento.idDepartamento', '=', $idDepartamento)
                ->execute($this->_db)
        ;

        return $departamentos;
    }
    
    public function select_quantidade_departamentos()
    {
        return $this->select_departamentos()->count();
    }
    
    public function update_departamento_contato($vars)
    {
        // Formatação dos valores para inserir no banco.
        // Coloca campos vazios como nulo.
        foreach ($vars as $key => $var)
        {
            if (empty($var))
            {
                $vars[$key] = NULL;
            }
        }
        // Fim da formatação.

        $pairs = array(
            'idContato' => $vars['novoIdContato'],
        );

        $state = DB::update('CSM_Departamento')
                ->set($pairs)
                ->where('idContato', '=', $vars['idContato'])
                ->execute($this->_db)
        ;
        return $state;
    }
    
    public function update_departamento_funcionario($vars)
    {
        // Formatação dos valores para inserir no banco.
        // Coloca campos vazios como nulo.
        foreach ($vars as $key => $var)
        {
            if (empty($var))
            {
                $vars[$key] = NULL;
            }
        }
        // Fim da formatação.

        $pairs = array(
            'idFuncionarioChefe' => $vars['novoIdFuncionario'],
        );

        $state = DB::update('CSM_Departamento')
                ->set($pairs)
                ->where('idFuncionarioChefe', '=', $vars['idFuncionario'])
                ->execute($this->_db)
        ;
        return $state;
    }
    
    public function update_ativar_departamento($idDepartamento)
    {
        $departamento = DB::select('CSM_Departamento.ativo')
                ->from('CSM_Departamento')
                ->where('idDepartamento', '=', $idDepartamento)
                ->execute($this->_db)
        ;

        if ($departamento[0]['ativo'] == 'S')
        {
            $pairs = array(
                'ativo' => 'N',
            );
        }
        else
        {
            $pairs = array(
                'ativo' => 'S',
            );
        }

        $state = DB::update('CSM_Departamento')
                ->set($pairs)
                ->where('idDepartamento', '=', $idDepartamento)
                ->execute($this->_db)
        ;
        return $state;
    }

public function update_departamento($vars)
    {
        $pairs = array(
            'nmDepartamento' => $vars['nmDepartamento'],
            'dsDepartamento' => $vars['dsDepartamento'],
            'idFuncionarioChefe' => $vars['chefeDep'],
            'idEndereco' => $vars['idEndereco'],
            'idContato' => $vars['idContato']            
       );

        $state = DB::update('CSM_Departamento')
                ->set($pairs)
                ->where('idDepartamento', '=', $vars['idDepartamento'])
                ->execute($this->_db)
        ;
        
        return $state;
        
    }   
    
    public function delete_departamento($idDepartamento){
              $state = DB::delete('CSM_Departamento')
                ->where('idDepartamento', '=', $idDepartamento)
                ->execute($this->_db)
        ;
        return $state;
    }
    
}

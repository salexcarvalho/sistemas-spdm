<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Csm_ServicoTemFuncionario extends Model
{

    /**
     * Construtor da classe, já seta a conexao com o banco de dados
     *
     * @param string $con_type
     */
    public function __construct($db = NULL)
    {
        if ($db != NULL)
        {
            // Set the instance or name
            $this->_db = $db;
        }
        elseif (!$this->_db)
        {
            // Use the default name
            $this->_db = Database::$default;
        }

        if (is_string($this->_db))
        {
            // Load the database
            $this->_db = Database::instance($this->_db);
        }
    }

    public function insert_servico_funcionario($idServico, $idFuncionario)
    {
        if ($idServico == 'NULL' || empty($idServico))
        {
            return NULL;
        }
        if ($idFuncionario == 'NULL' || empty($idFuncionario))
        {
            return NULL;
        }

        $colunas = array(
            "idServico",
            "idFuncionario",
        );

        $values = array(
            $idServico,
            $idFuncionario,
        );

        DB::insert("CSM_ServicoTemFuncionario", $colunas)
                ->values($values)
                ->execute($this->_db)
        ;
    }
    
    public function select_servicos_funcionarios()
    {
        $servicos_funcionarios = DB::select()
                ->from('CSM_ServicoTemFuncionario')
                ->join('CSM_Servico', 'INNER')
                ->on('CSM_Servico.idServico', '=', 'CSM_ServicoTemFuncionario.idServico')
                ->join('CSM_Funcionario', 'INNER')
                ->on('CSM_Funcionario.idFuncionario', '=', 'CSM_ServicoTemFuncionario.idFuncionario')
                ->execute($this->_db)
        ;

        return $servicos_funcionarios;
    }
    
    public function select_quantidade_servicos_funcionarios()
    {
        return $this->select_servicos_funcionarios()->count();
    }
    
    public function delete_servico_funcionario($vars)
    {
        $state = DB::delete('CSM_ServicoTemFuncionario')
                ->where('idFuncionario', '=', $vars['idFuncionario'])
                ->execute($this->_db)
        ;

        return $state;
    }
    
    public function delete_servico_funcionario_por_servico($vars)
    {
        $state = DB::delete('CSM_ServicoTemFuncionario')
                ->where('idServico', '=', $vars['idServico'])
                ->execute($this->_db)
        ;

        return $state;
    }

}

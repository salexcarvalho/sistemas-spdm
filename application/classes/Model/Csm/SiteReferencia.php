<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Csm_SiteReferencia extends Model
{

    /**
     * Construtor da classe, já seta a conexao com o banco de dados
     *
     * @param string $con_type
     */
    public function __construct($db = NULL)
    {
        if ($db != NULL)
        {
            // Set the instance or name
            $this->_db = $db;
        }
        elseif (!$this->_db)
        {
            // Use the default name
            $this->_db = Database::$default;
        }

        if (is_string($this->_db))
        {
            // Load the database
            $this->_db = Database::instance($this->_db);
        }
    }

    public function insert_site_referencia($nmEmpresa, $urlSite, $idServico)
    {
        if ($nmEmpresa == 'NULL' || empty($nmEmpresa))
        {
            $nmEmpresa = NULL;
        }
        if ($urlSite == 'NULL' || empty($urlSite))
        {
            $urlSite = NULL;
            return FALSE;
        }
        if ($idServico == 'NULL' || empty($idServico))
        {
            $idServico = NULL;
        }

        $colunas = array(
            "nmEmpresa",
            "urlSite",
            "idServico",
        );

        $values = array(
            $nmEmpresa,
            $urlSite,
            $idServico,
        );

        $returned_id = DB::insert("CSM_SiteReferencia", $colunas)
                ->values($values)
                ->execute($this->_db)
        ;
        return $returned_id;
    }

    public function select_site_por_servico($idServico)
    {
        $servicos = DB::select()
                ->from('CSM_SiteReferencia')
                ->where('CSM_SiteReferencia.idServico', '=', $idServico)
                ->execute($this->_db)
        ;

        return $servicos;
    }

    public function delete_sitereferencia($idSite)
    {
        $state = DB::delete('CSM_SiteReferencia')
                ->where('idSiteReferencia', '=', $idSite)
                ->execute($this->_db)
        ;

        return $state;
    }

    public function update_site($idSite,$Empresa,$Url)
    {        
        $pairs = array(
            'nmEmpresa' => $Empresa,
            'urlSite' => $Url
        );

        $state = DB::update('CSM_SiteReferencia')
                ->set($pairs)
                ->where('idSiteReferencia', '=', $idSite)
                ->execute($this->_db)
        ;
        return $state;
    }
    
    
   public function delete_sitereferencia_por_servico($vars){
       $state = DB::delete('CSM_SiteReferencia')
                ->where('idServico', '=', $vars['idServico'])
                ->execute($this->_db)
        ;

        return $state;
   }

}

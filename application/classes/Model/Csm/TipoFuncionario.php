<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Csm_TipoFuncionario extends Model
{

    /**
     * Construtor da classe, já seta a conexao com o banco de dados
     *
     * @param string $con_type
     */
    public function __construct($db = NULL)
    {
        if ($db != NULL)
        {
            // Set the instance or name
            $this->_db = $db;
        }
        elseif (!$this->_db)
        {
            // Use the default name
            $this->_db = Database::$default;
        }

        if (is_string($this->_db))
        {
            // Load the database
            $this->_db = Database::instance($this->_db);
        }
    }

    public function select_tipo_funcionarios()
    {
        $tipoFuncionarios = DB::select('CSM_TipoFuncionario.idTipoFuncionario', 'CSM_TipoFuncionario.nmTipoFuncionario')
                ->from('CSM_TipoFuncionario')
                ->execute($this->_db)
        ;

        return $tipoFuncionarios;
    }

}

<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Scc_Dsei extends Model {

    /**
     * Construtor da classe, já seta a conexao com o banco de dados
     *
     * @param string $con_type
     */
    public function __construct($db = NULL) {
        if ($db != NULL) {
            // Set the instance or name
            $this->_db = $db;
        } else {
            // Use the default name
            $this->_db = Database::$default;
        }

        if (is_string($this->_db)) {
            // Load the database
            $this->_db = Database::instance($this->_db);
        }
    }

    public function select_dsei_id($idDsei) {
        $dsei = DB::select("idDsei", "Nome_Dsei", "Sigla_Dsei", "Status", "idUsuario", "dtCadastro", "dtAlteracao")
                ->from('SCC_Dsei')
                ->where('SCC_Dsei.idDsei', '=', $idDsei)                
                ->execute($this->_db)
        ;
        return $dsei;
    }

    public function select_dsei_alias($Sigla_Dsei) {

        $dsei = DB::select("idDsei", "Nome_Dsei", "Sigla_Dsei", "Status", "idUsuario", "dtCadastro", "dtAlteracao")
                ->from('SCC_Dsei')
                ->where('SCC_Dsei.Sigla_Dsei', '=', $Sigla_Dsei)
                ->execute($this->_db)
        ;

        return $dsei;
    }

    public function select_dsei() {
        $dsei = DB::select("idDsei", "Nome_Dsei", "Sigla_Dsei", "Status", "idUsuario", "dtCadastro", "dtAlteracao")
                ->from('SCC_Dsei')
                ->order_by('SCC_Dsei.idDsei', 'DESC')
                ->execute($this->_db)
        ;
        return $dsei;
    }
    
    public function select_dsei_ativas($Status) {
        $dsei = DB::select("idDsei", "Nome_Dsei", "Sigla_Dsei", "Status", "idUsuario", "dtCadastro", "dtAlteracao")
                ->from('SCC_Dsei')
                ->where('SCC_Dsei.Status', '=', $Status)
                ->order_by('SCC_Dsei.Nome_Dsei', 'ASC')
                ->execute($this->_db)
        ;
        return $dsei;
    }

    public function select_quantidade() {
        return $this->select_dsei()->count();
    }

    public function insert_dsei($Nome_Dsei, $Sigla_Dsei, $status) {
        $colunas = array(
            'Nome_Dsei',
            'Sigla_Dsei',
            'dtCadastro',
            'Status',
            'idUsuario'
        );
        $values = array(
            $Nome_Dsei,
            $Sigla_Dsei,
            date('yy-m-d'),
            $status,
            $_SESSION['idUsuario']
        );
        $returned_id = DB::insert("SCC_Dsei", $colunas)
                ->values($values)
                ->execute($this->_db)
        ;
        return $returned_id;
    }

    public function update_dsei($idDsei, $Nome_Dsei, $Sigla_Dsei, $Status) {
        $state = DB::update('SCC_Dsei')
                ->set(array('Nome_Dsei' => $Nome_Dsei))
                ->set(array('Sigla_Dsei' => $Sigla_Dsei))
                ->set(array('Status' => $Status))
                ->set(array('idUsuario' => $_SESSION['idUsuario']))
                ->set(array('dtAlteracao' => date('yy-m-d')))
                ->where('idDsei', '=', $idDsei)
                ->execute($this->_db)
        ;
        return $state;
    }

    public function delete_dsei($idDsei) {
        $returned_id = DB::delete('SCC_Dsei')
                ->where('idDsei', '=', $idDsei)
                ->execute($this->_db);
        return $returned_id;
    }

    public function update_ativar_dsei2($idDsei) {

        $dsei = DB::select('SCC_Dsei.Status')
                ->from('SCC_Dsei')
                ->where('idDsei', '=', $idDsei)
                ->execute($this->_db)
        ;

        if ($dsei[0]['Status'] == '1') {
            $pairs = array('Status' => '0',);
        } else {
            $pairs = array('Status' => '1',);
        }

        $state = DB::update('SCC_Dsei')
                ->set($pairs)
                ->where('idDsei', '=', $idDsei)
                ->execute($this->_db)
        ;
        return $state;
    }
    
    public function update_ativar_dsei($idDsei) {

        $tipos = DB::select('SCC_Dsei.Status')
                ->from('SCC_Dsei')
                ->where('idDsei', '=', $idDsei)
                ->execute($this->_db)
        ;
        
        if ($tipos[0]['Status'] == '1') {
            $pairs = array('Status' => '0',);
        } else {
            $pairs = array('Status' => '1',);
        }
        
        $state = DB::update('SCC_Dsei')
                ->set($pairs)
                ->where('idDsei', '=', $idDsei)
                ->execute($this->_db)
        ;
        return $state;
    }

}

<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Scc_Unidades extends Model {

    /**
     * Construtor da classe, já seta a conexao com o banco de dados
     *
     * @param string $con_type
     */
    public function __construct($db = NULL) {
        if ($db != NULL) {
            // Set the instance or name
            $this->_db = $db;
        } else {
            // Use the default name
            $this->_db = Database::$default;
        }

        if (is_string($this->_db)) {
            // Load the database
            $this->_db = Database::instance($this->_db);
        }
    }

    public function select_unidade($idUnidade) {
        $unidade = DB::select("SCC_Unidade.idUnidade", "SCC_Unidade.Nome_Unidade", "SCC_Unidade.idVinculo", "SCC_Unidade.Sigla_Unidade", "SCC_Unidade.Status",
                "SCC_Unidade.idUsuario", "SCC_Unidade.dtCadastro", "SCC_Unidade.dtAlteracao", array('SCC_Vinculo.Nome_Vinculo', 'Nome_Vinculo'))
                ->from('SCC_Unidade')
                ->where('SCC_Unidade.idUnidade', '=', $idUnidade)   
                ->join('SCC_Vinculo', 'LEFT')
                ->on('SCC_Vinculo.idVinculo', '=', 'SCC_Unidade.idVinculo')
                ->execute($this->_db)
        ;
        return $unidade;
    }

    public function select_unidade_alias($Sigla_Unidade) {

        $unidade = DB::select("idUnidade", "Nome_Unidade", "Sigla_Unidade", "Status", "idUsuario", "dtCadastro", "dtAlteracao")
                ->from('SCC_Unidade')
                ->where('SCC_Unidade.Sigla_Unidade', '=', $Sigla_Unidade)
                ->execute($this->_db)
        ;

        return $unidade;
    }

    public function select_unidades() {
        $unidade = DB::select("SCC_Unidade.idUnidade", "SCC_Unidade.Nome_Unidade", "SCC_Unidade.idVinculo", "SCC_Unidade.Sigla_Unidade", "SCC_Unidade.Status",
                "SCC_Unidade.idUsuario", "SCC_Unidade.dtCadastro", "SCC_Unidade.dtAlteracao", array('SCC_Vinculo.Nome_Vinculo', 'Nome_Vinculo'))
                ->from('SCC_Unidade')
                ->join('SCC_Vinculo', 'LEFT')
                ->on('SCC_Vinculo.idVinculo', '=', 'SCC_Unidade.idVinculo')
                ->order_by('SCC_Unidade.idUnidade', 'DESC')
                ->execute($this->_db)
        ;
        return $unidade;
    }
    
    public function select_unidades_ativas($idVinculo, $Status) {
        $unidade = DB::select("idUnidade", "Nome_Unidade", "Sigla_Unidade", "idVinculo", "Status", "idUsuario", "dtCadastro", "dtAlteracao")
                ->from('SCC_Unidade')
                ->where('SCC_Unidade.Status', '=', $Status);
                if ($idVinculo == '12') {
                    $unidade->and_where('SCC_Unidade.idVinculo', '=', $idVinculo);
                } else if ($idVinculo == '9') {
                    $unidade->and_where('SCC_Unidade.idVinculo', '=', $idVinculo);                    
                } else {
                    $unidade->and_where('SCC_Unidade.idVinculo', 'in', array($idVinculo, 0));
                }
                return $unidade->order_by('SCC_Unidade.Nome_Unidade', 'ASC')->execute($this->_db);
                
    }

    public function select_quantidade() {
        return $this->select_unidades()->count();
    }

    public function insert_unidade($Nome_unidade, $Sigla_unidade, $idVinculo, $status) {
        $colunas = array(
            'Nome_Unidade',
            'Sigla_Unidade',
            'idVinculo',
            'dtCadastro',
            'Status',
            'idUsuario'
        );
        $values = array(
            $Nome_unidade,
            $Sigla_unidade,
            $idVinculo,
            date('yy-m-d'),
            $status,
            $_SESSION['idUsuario']
        );
        $returned_id = DB::insert("SCC_Unidade", $colunas)
                ->values($values)
                ->execute($this->_db)
        ;
        return $returned_id;
    }

    public function update_unidade($idUnidade, $Nome_unidade, $Sigla_unidade, $idVinculo, $Status) {
        $state = DB::update('SCC_Unidade')
                ->set(array('Nome_Unidade' => $Nome_unidade))
                ->set(array('Sigla_Unidade' => $Sigla_unidade))
                ->set(array('idVinculo' => $idVinculo))
                ->set(array('Status' => $Status))
                ->set(array('idUsuario' => $_SESSION['idUsuario']))
                ->set(array('dtAlteracao' => date('yy-m-d')))
                ->where('idUnidade', '=', $idUnidade)
                ->execute($this->_db)
        ;
        return $state;
    }

    public function delete_unidade($idUnidade) {
        $returned_id = DB::delete('SCC_Unidade')
                ->where('idUnidade', '=', $idUnidade)
                ->execute($this->_db);
        return $returned_id;
    }

    public function update_ativar_unidade($idUnidade) {

        $unidades = DB::select('SCC_Unidade.Status')
                ->from('SCC_Unidade')
                ->where('idUnidade', '=', $idUnidade)
                ->execute($this->_db)
        ;

        if ($unidades[0]['Status'] == '1') {
            $pairs = array('Status' => '0',);
        } else {
            $pairs = array('Status' => '1',);
        }

        $state = DB::update('SCC_Unidade')
                ->set($pairs)
                ->where('idUnidade', '=', $idUnidade)
                ->execute($this->_db)
        ;
        return $state;
    }

}

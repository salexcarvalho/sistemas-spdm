<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Scc_Tipos extends Model {

    /**
     * Construtor da classe, já seta a conexao com o banco de dados
     *
     * @param string $con_type
     */
    public function __construct($db = NULL) {
        if ($db != NULL) {
            // Set the instance or name
            $this->_db = $db;
        } else {
            // Use the default name
            $this->_db = Database::$default;
        }

        if (is_string($this->_db)) {
            // Load the database
            $this->_db = Database::instance($this->_db);
        }
    }

    public function select_tipo($idTipoRelato) {
        $tipo = DB::select("idTipoRelato", "Nome_TipoRelato", "Sigla_TipoRelato", "Status", "idUsuario", "dtCadastro", "dtAlteracao")
                ->from('SCC_Tipo_Relato')
                ->where('SCC_Tipo_Relato.idTipoRelato', '=', $idTipoRelato)                
                ->execute($this->_db)
        ;
        return $tipo;
    }
    
    public function select_tipo_alias($Sigla_TipoRelato) {

        $tipo = DB::select("idTipoRelato", "Nome_TipoRelato", "Sigla_TipoRelato", "Status", "idUsuario", "dtCadastro", "dtAlteracao")
                ->from('SCC_Tipo_Relato')
                ->where('SCC_Tipo_Relato.Sigla_TipoRelato', '=', $Sigla_TipoRelato)
                ->execute($this->_db)
        ;

        return $tipo;
    }

    public function select_tipos() {
        $tipo = DB::select("idTipoRelato", "Nome_TipoRelato", "Sigla_TipoRelato", "Status", "idUsuario", "dtCadastro", "dtAlteracao")
                ->from('SCC_Tipo_Relato')
                ->order_by('SCC_Tipo_Relato.idTipoRelato', 'DESC')
                ->execute($this->_db)
        ;
        return $tipo;
    }
    
    public function select_tipos_ativos($Status) {
        $tipo = DB::select("idTipoRelato", "Nome_TipoRelato", "Sigla_TipoRelato", "Status", "idUsuario", "dtCadastro", "dtAlteracao")
                ->from('SCC_Tipo_Relato')
                ->where('SCC_Tipo_Relato.Status', '=', $Status)
                ->order_by('SCC_Tipo_Relato.Nome_TipoRelato', 'ASC')
                ->execute($this->_db)
        ;
        return $tipo;
    }

    public function select_quantidade() {
        return $this->select_tipos()->count();
    }

    public function insert_tipo($Nome_tiporelato, $Sigla_tiporelato, $status) {
        $colunas = array(
            'Nome_TipoRelato',
            'Sigla_TipoRelato',
            'dtCadastro',
            'Status',
            'idUsuario'
        );
        $values = array(
            $Nome_tiporelato,
            $Sigla_tiporelato,
            date('yy-m-d'),
            $status,
            $_SESSION['idUsuario']
        );
        $returned_id = DB::insert("SCC_Tipo_Relato", $colunas)
                ->values($values)
                ->execute($this->_db)
        ;
        return $returned_id;
    }

    public function update_tipo($idTipoRelato, $Nome_tiporelato, $Sigla_tiporelato, $Status) {
        $state = DB::update('SCC_Tipo_Relato')
                ->set(array('Nome_TipoRelato' => $Nome_tiporelato))
                ->set(array('Sigla_TipoRelato' => $Sigla_tiporelato))
                ->set(array('Status' => $Status))
                ->set(array('idUsuario' => $_SESSION['idUsuario']))
                ->set(array('dtAlteracao' => date('yy-m-d')))
                ->where('idTipoRelato', '=', $idTipoRelato)
                ->execute($this->_db)
        ;
        return $state;
    }

    public function delete_tipo($idTipoRelato) {
        $returned_id = DB::delete('SCC_Tipo_Relato')
                ->where('idTipoRelato', '=', $idTipoRelato)
                ->execute($this->_db);
        return $returned_id;
    }

    public function update_ativar_tipo($idTipoRelato) {

        $tipos = DB::select('SCC_Tipo_Relato.Status')
                ->from('SCC_Tipo_Relato')
                ->where('idTipoRelato', '=', $idTipoRelato)
                ->execute($this->_db)
        ;

        if ($tipos[0]['Status'] == '1') {
            $pairs = array('Status' => '0',);
        } else {
            $pairs = array('Status' => '1',);
        }

        $state = DB::update('SCC_Tipo_Relato')
                ->set($pairs)
                ->where('idTipoRelato', '=', $idTipoRelato)
                ->execute($this->_db)
        ;
        return $state;
    }

}

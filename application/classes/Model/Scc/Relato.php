<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Scc_Relato extends Model {

    /**
     * Construtor da classe, já seta a conexao com o banco de dados
     *
     * @param string $con_type
     */
    public function __construct($db = NULL) {
        if ($db != NULL) {
            // Set the instance or name
            $this->_db = $db;
        } else {
            // Use the default name
            $this->_db = Database::$default;
        }

        if (is_string($this->_db)) {
            // Load the database
            $this->_db = Database::instance($this->_db);
        }
    }

    public function select_relato($idRelato) {
        $relato = DB::select(
                "SCC_Relato.idRelato",
                "SCC_Relato.idTipoRelato",
                "SCC_Relato.idUnidade",
                "SCC_Relato.idDsei",
                "SCC_Relato.dtQuando",
                "SCC_Relato.Onde",
                "SCC_Relato.Nome_Envolvidos",
                "SCC_Relato.Descricao_Relato",
                "SCC_Relato.Numero_Protocolo_Anterior",
                "SCC_Relato.dtCadastro",
                "SCC_Relato.Status",
                array('SCC_Vinculo.Nome_Vinculo', 'Nome_Vinculo'),
                array('SCC_Vinculo.idVinculo', 'idVinculo'),
                array('SCC_Solicitante.idSolicitante', 'idSolicitante'),
                array('SCC_Solicitante.Nome_Solicitante', 'Nome_Solicitante'),
                array('SCC_Solicitante.Email_Solicitante', 'Email_Solicitante'),
                array('SCC_Solicitante.Cargo_Solicitante', 'Cargo_Solicitante'),
                array('SCC_Solicitante.Tel_Solicitante', 'Tel_Solicitante'),
                array('SCC_Solicitante.Cel_Solicitante', 'Cel_Solicitante'),
                array('SCC_Unidade.Nome_Unidade', 'Nome_Unidade'),
                array('SCC_Protocolo.idProtocolo', 'idProtocolo'),
                array('SCC_Protocolo.Status_Atendimento', 'Atendimento'),
                array('SCC_Dsei.Nome_Dsei', 'Nome_Dsei'))
                ->from('SCC_Relato')
                ->where('SCC_Relato.idRelato', '=', $idRelato)
                ->join('SCC_Protocolo', 'LEFT')
                ->on('SCC_Protocolo.idRelato', '=', 'SCC_Relato.idRelato')
                ->join('SCC_Solicitante', 'LEFT')
                ->on('SCC_Solicitante.idSolicitante', '=', 'SCC_Protocolo.idSolicitante')
                ->join('SCC_Vinculo', 'LEFT')
                ->on('SCC_Vinculo.idVinculo', '=', 'SCC_Solicitante.idVinculo')
                ->join('SCC_Unidade', 'LEFT')
                ->on('SCC_Unidade.idUnidade', '=', 'SCC_Relato.idUnidade')
                ->join('SCC_Dsei', 'LEFT')
                ->on('SCC_Dsei.idDsei', '=', 'SCC_Relato.idDsei')
                ->execute($this->_db)
        ;
        return $relato;
    }
    
    public function select_relatos_encerrar() {
        $relatos = DB::select(
                "SCC_Relato.idRelato",
                array('SCC_Protocolo.idProtocolo', 'idProtocolo'),
                array('SCC_Protocolo.Status_Atendimento', 'Atendimento'))
                ->from('SCC_Relato')
                ->where('SCC_Relato.dtCadastro', '<=', date('Y-m-d', strtotime('- 5 days', strtotime(date('yy-m-d')))))
                ->and_where('SCC_Protocolo.Status_Atendimento', '=', '3')
                ->join('SCC_Protocolo', 'LEFT')
                ->on('SCC_Protocolo.idRelato', '=', 'SCC_Relato.idRelato')
                ->execute($this->_db)
        ;
        return $relatos;
    }
    
    public function select_protocolo($idProtocolo) {
        $relato = DB::select("SCC_Protocolo.idRelato")
                ->from('SCC_Protocolo')
                ->where('SCC_Protocolo.idProtocolo', '=', $idProtocolo)
                ->execute($this->_db)
        ;
        return $relato;
    }

    public function select_relatos($StatusAtendimento = NULL) {
        $relato = DB::select("SCC_Relato.idRelato", "SCC_Relato.idTipoRelato", "SCC_Relato.idUnidade", "SCC_Relato.dtQuando", "SCC_Relato.dtCadastro", "SCC_Relato.Status",
                array('SCC_Vinculo.Nome_Vinculo', 'Nome_Vinculo'), array('SCC_Solicitante.Nome_Solicitante', 'Nome_Solicitante'), array('SCC_Protocolo.idProtocolo', 'idProtocolo'), 
                array('SCC_Protocolo.Status_Atendimento', 'Atendimento'), array('SCC_Unidade.Nome_Unidade', 'Nome_Unidade'))
                ->from('SCC_Relato')
                ->join('SCC_Protocolo', 'LEFT')
                ->on('SCC_Protocolo.idRelato', '=', 'SCC_Relato.idRelato')
                ->join('SCC_Solicitante', 'LEFT')
                ->on('SCC_Solicitante.idSolicitante', '=', 'SCC_Protocolo.idSolicitante')
                ->join('SCC_Vinculo', 'LEFT')
                ->on('SCC_Vinculo.idVinculo', '=', 'SCC_Solicitante.idVinculo')
                ->join('SCC_Unidade', 'LEFT')
                ->on('SCC_Unidade.idUnidade', '=', 'SCC_Relato.idUnidade');
                if (($_SESSION['Perfil'] !== 'Administrador') && ($_SESSION['AcLiberaAdmRHM'] == false)) {
                    $relato->where('SCC_Vinculo.idTipoSetor', '=', $_SESSION['idSetor']);
                }
                if ($StatusAtendimento !== NULL) {
                    $relato->and_where('SCC_Protocolo.Status_Atendimento', '=', $StatusAtendimento);
                }
                return $relato->order_by('SCC_Relato.idRelato', 'DESC')->execute($this->_db);
    }
    
    public function select_relato_protocolo($idProtocolo) {
        $relato = DB::select(
                "SCC_Relato.idRelato",
                "SCC_Relato.idTipoRelato",
                "SCC_Relato.idUnidade",
                "SCC_Relato.idDsei",
                "SCC_Relato.dtQuando",
                "SCC_Relato.Onde",
                "SCC_Relato.Nome_Envolvidos",
                "SCC_Relato.Descricao_Relato",
                "SCC_Relato.Numero_Protocolo_Anterior",
                "SCC_Relato.dtCadastro",
                "SCC_Relato.Status",
                array('SCC_Vinculo.Nome_Vinculo', 'Nome_Vinculo'),
                array('SCC_Vinculo.idVinculo', 'idVinculo'),
                array('SCC_Solicitante.idSolicitante', 'idSolicitante'),
                array('SCC_Solicitante.Nome_Solicitante', 'Nome_Solicitante'),
                array('SCC_Solicitante.Email_Solicitante', 'Email_Solicitante'),
                array('SCC_Solicitante.Cargo_Solicitante', 'Cargo_Solicitante'),
                array('SCC_Solicitante.Tel_Solicitante', 'Tel_Solicitante'),
                array('SCC_Solicitante.Cel_Solicitante', 'Cel_Solicitante'),
                array('SCC_Unidade.Nome_Unidade', 'Nome_Unidade'),
                array('SCC_Protocolo.idProtocolo', 'idProtocolo'),
                array('SCC_Protocolo.Status_Atendimento', 'Atendimento'),
                array('SCC_Dsei.Nome_Dsei', 'Nome_Dsei'))
                ->from('SCC_Relato')
                ->join('SCC_Protocolo', 'LEFT')
                ->on('SCC_Protocolo.idRelato', '=', 'SCC_Relato.idRelato')
                ->join('SCC_Solicitante', 'LEFT')
                ->on('SCC_Solicitante.idSolicitante', '=', 'SCC_Protocolo.idSolicitante')
                ->join('SCC_Vinculo', 'LEFT')
                ->on('SCC_Vinculo.idVinculo', '=', 'SCC_Solicitante.idVinculo')
                ->join('SCC_Unidade', 'LEFT')
                ->on('SCC_Unidade.idUnidade', '=', 'SCC_Relato.idUnidade')
                ->join('SCC_Dsei', 'LEFT')
                ->on('SCC_Dsei.idDsei', '=', 'SCC_Relato.idDsei')
                ->where('SCC_Protocolo.idProtocolo', '=', $idProtocolo)
                ->execute($this->_db)
        ;
        return $relato;
    }
    
    public function select_quantidade_relatos() {
        return $this->select_relatos()->count();
    }
    
    public function select_protocolo_abertos() {
        $relato = DB::select("SCC_Protocolo.idRelato")
                ->from('SCC_Protocolo')
                ->where('SCC_Protocolo.Status_Atendimento', '=', '1')
                ->join('SCC_Solicitante', 'LEFT')
                ->on('SCC_Solicitante.idSolicitante', '=', 'SCC_Protocolo.idSolicitante')
                ->join('SCC_Vinculo', 'LEFT')
                ->on('SCC_Solicitante.idVinculo', '=', 'SCC_Vinculo.idVinculo');
                if (($_SESSION['Perfil'] !== 'Administrador') && ($_SESSION['AcLiberaAdmRHM'] == false)) {
                    $relato->and_where('SCC_Vinculo.idTipoSetor', '=', $_SESSION['idSetor']);
                }
                return $relato->execute($this->_db);
    }
    
    public function select_quantidade_abertos() {
        return $this->select_protocolo_abertos()->count();
    }
    
    public function select_protocolo_encerrados() {
        $relato = DB::select("SCC_Protocolo.idRelato")
                ->from('SCC_Protocolo')
                ->where('SCC_Protocolo.Status_Atendimento', '=', '4')
                ->join('SCC_Solicitante', 'LEFT')
                ->on('SCC_Solicitante.idSolicitante', '=', 'SCC_Protocolo.idSolicitante')
                ->join('SCC_Vinculo', 'LEFT')
                ->on('SCC_Solicitante.idVinculo', '=', 'SCC_Vinculo.idVinculo');
                if (($_SESSION['Perfil'] !== 'Administrador') && ($_SESSION['AcLiberaAdmRHM'] == false)) {
                    $relato->and_where('SCC_Vinculo.idTipoSetor', '=', $_SESSION['idSetor']);
                }
                return $relato->execute($this->_db);
    }
    
    public function select_quantidade_encerrados() {
        return $this->select_protocolo_encerrados()->count();
    }
    
    public function select_protocolo_concluidos() {
        $relato = DB::select("SCC_Protocolo.idRelato")
                ->from('SCC_Protocolo')
                ->where('SCC_Protocolo.Status_Atendimento', '=', '3')
                ->join('SCC_Solicitante', 'LEFT')
                ->on('SCC_Solicitante.idSolicitante', '=', 'SCC_Protocolo.idSolicitante')
                ->join('SCC_Vinculo', 'LEFT')
                ->on('SCC_Solicitante.idVinculo', '=', 'SCC_Vinculo.idVinculo');
                if (($_SESSION['Perfil'] !== 'Administrador') && ($_SESSION['AcLiberaAdmRHM'] == false)) {
                    $relato->and_where('SCC_Vinculo.idTipoSetor', '=', $_SESSION['idSetor']);
                }
                return $relato->execute($this->_db);
    }
    
    public function select_quantidade_concluidos() {
        return $this->select_protocolo_concluidos()->count();
    }
    
    public function select_protocolo_processando() {
        $relato = DB::select("SCC_Protocolo.idRelato")
                ->from('SCC_Protocolo')
                ->where('SCC_Protocolo.Status_Atendimento', '=', '2')
                ->join('SCC_Solicitante', 'LEFT')
                ->on('SCC_Solicitante.idSolicitante', '=', 'SCC_Protocolo.idSolicitante')
                ->join('SCC_Vinculo', 'LEFT')
                ->on('SCC_Solicitante.idVinculo', '=', 'SCC_Vinculo.idVinculo');
                if (($_SESSION['Perfil'] !== 'Administrador') && ($_SESSION['AcLiberaAdmRHM'] == false)) {
                    $relato->and_where('SCC_Vinculo.idTipoSetor', '=', $_SESSION['idSetor']);
                }
                return $relato->execute($this->_db);
    }
    
    public function select_quantidade_processando() {
        return $this->select_protocolo_processando()->count();
    }
    
    public function select_anexos($idProtocolo, $idRespota = null) {
        $anexos = DB::select(
                "SCC_Anexo.idAnexo",
                "SCC_Anexo.Caminho",
                "SCC_Anexo.idResposta")
                ->from('SCC_Anexo')
                ->where('SCC_Anexo.idProtocolo', '=', $idProtocolo)
                ->and_where('SCC_Anexo.idResposta', '=', $idRespota)
                ->execute($this->_db)
        ;
        return $anexos;
    }
    
    public function select_resposta($idProtocolo) {
        $resposta = DB::select(
                "SCC_Resposta.idResposta",
                "SCC_Resposta.Resposta",
                "SCC_Resposta.dtResposta",
                "SCC_Resposta.Envio",
                "SCC_Resposta.idUsuario",
                array(PREFIX.TABLELOGIN.'LOGIN_Usuario.nmNome', 'Nome_Usuario'))
                ->from('SCC_Resposta')
                ->join(PREFIX.TABLELOGIN.'LOGIN_Usuario', 'LEFT')
                ->on(PREFIX.TABLELOGIN.'LOGIN_Usuario.idUsuario', '=', 'SCC_Resposta.idUsuario')
                ->where('SCC_Resposta.idProtocolo', '=', $idProtocolo)
                ->execute($this->_db)
        ;
        return $resposta;
    }
    
    public function update_protocolo($var, $Status) {
        $state = DB::update('SCC_Protocolo')
                ->set(array('Status_Atendimento' => $Status))
                ->where('idProtocolo', '=', $var['idProtocolo'])
                ->execute($this->_db)
        ;
        return $state;
    }
    
    public function update_solicitante($var) {
        $state = DB::update('SCC_Solicitante')
                ->set(array('idVinculo' => $var['vinculo_solicitante']))
                ->where('idSolicitante', '=', $var['idSolicitante'])
                ->execute($this->_db)
        ;
        return $state;
    }
    
    public function update_cron($var, $Status) {
        $state = DB::update('SCC_Protocolo')
                ->set(array('Status_Atendimento' => $Status))
                ->where('idProtocolo', '=', $var)
                ->execute($this->_db)
        ;
        return $state;
    }

    public function insert_resposta($var, $resposta) {
        $colunas = array(
            'idProtocolo',
            'Resposta',
            'dtResposta',
            'envio',
            'idSolicitante',
            'idUsuario',
            'dtCadastro',
            'Status'
        );
        $values = array(
            $var['idProtocolo'],
            $resposta,
            date('yy-m-d'),
            $var['envio'],
            $var['idSolicitante'],
            $_SESSION['idUsuario'],
            date('yy-m-d'),
            '1'
        );
        $returned_id = DB::insert("SCC_Resposta", $colunas)
                ->values($values)
                ->execute($this->_db)
        ;
        return $returned_id;
    }
    
    public function insert_resposta_usuario($var, $resposta) {
        $colunas = array(
            'idProtocolo',
            'Resposta',
            'dtResposta',
            'envio',
            'idSolicitante',
            'dtCadastro',
            'Status'
        );
        $values = array(
            $var['idProtocolo'],
            $resposta,
            date('yy-m-d'),
            'resposta_usuario',
            $var['idSolicitante'],
            date('yy-m-d'),
            '1'
        );
        $returned_id = DB::insert("SCC_Resposta", $colunas)
                ->values($values)
                ->execute($this->_db)
        ;
        return $returned_id;
    }


}

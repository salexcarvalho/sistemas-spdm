<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Scc_Vinculos extends Model {

    /**
     * Construtor da classe, já seta a conexao com o banco de dados
     *
     * @param string $con_type
     */
    public function __construct($db = NULL) {
        if ($db != NULL) {
            // Set the instance or name
            $this->_db = $db;
        } else {
            // Use the default name
            $this->_db = Database::$default;
        }

        if (is_string($this->_db)) {
            // Load the database
            $this->_db = Database::instance($this->_db);
        }
    }

    public function select_vinculo($idVinculo) {
        $vinculo = DB::select("idVinculo", "Nome_Vinculo", "Sigla_Vinculo", "Status", "idUsuario", "dtCadastro", "dtAlteracao")
                ->from('SCC_Vinculo')
                ->where('SCC_Vinculo.idVinculo', '=', $idVinculo)                
                ->execute($this->_db)
        ;
        return $vinculo;
    }

    public function select_vinculo_alias($Sigla_Vinculo) {

        $vinculo = DB::select("idVinculo", "Nome_Vinculo", "Sigla_Vinculo", "Status", "idUsuario", "dtCadastro", "dtAlteracao")
                ->from('SCC_Vinculo')
                ->where('SCC_Vinculo.Sigla_Vinculo', '=', $Sigla_Vinculo)
                ->execute($this->_db)
        ;

        return $vinculo;
    }

    public function select_vinculos() {
        $vinculo = DB::select("idVinculo", "Nome_Vinculo", "Sigla_Vinculo", "Status", "idUsuario", "dtCadastro", "dtAlteracao")
                ->from('SCC_Vinculo')
                ->order_by('SCC_Vinculo.idVinculo', 'DESC')
                ->execute($this->_db)
        ;
        return $vinculo;
    }
    
    public function select_vinculos_ativos($Status) {
        $vinculo = DB::select("idVinculo", "Nome_Vinculo", "Sigla_Vinculo", "Status", "idUsuario", "dtCadastro", "dtAlteracao")
                ->from('SCC_Vinculo')
                ->where('SCC_Vinculo.Status', '=', $Status)
                ->order_by('SCC_Vinculo.idVinculo', 'DESC')
                ->execute($this->_db)
        ;
        return $vinculo;
    }

    public function select_quantidade() {
        return $this->select_vinculos()->count();
    }

    public function insert_vinculo($Nome_vinculo, $Sigla_vinculo, $status) {
        $colunas = array(
            'Nome_Vinculo',
            'Sigla_Vinculo',
            'dtCadastro',
            'Status',
            'idUsuario'
        );
        $values = array(
            $Nome_vinculo,
            $Sigla_vinculo,
            date('yy-m-d'),
            $status,
            $_SESSION['idUsuario']
        );
        $returned_id = DB::insert("SCC_Vinculo", $colunas)
                ->values($values)
                ->execute($this->_db)
        ;
        return $returned_id;
    }

    public function update_vinculo($idVinculo, $Nome_vinculo, $Sigla_vinculo, $Status) {
        $state = DB::update('SCC_Vinculo')
                ->set(array('Nome_Vinculo' => $Nome_vinculo))
                ->set(array('Sigla_Vinculo' => $Sigla_vinculo))
                ->set(array('Status' => $Status))
                ->set(array('idUsuario' => $_SESSION['idUsuario']))
                ->set(array('dtAlteracao' => date('yy-m-d')))
                ->where('idVinculo', '=', $idVinculo)
                ->execute($this->_db)
        ;
        return $state;
    }

    public function delete_vinculo($idVinculo) {
        $returned_id = DB::delete('SCC_Vinculo')
                ->where('idVinculo', '=', $idVinculo)
                ->execute($this->_db);
        return $returned_id;
    }

    public function update_ativar_vinculo($idVinculo) {

        $vinculos = DB::select('SCC_Vinculo.Status')
                ->from('SCC_Vinculo')
                ->where('idVinculo', '=', $idVinculo)
                ->execute($this->_db)
        ;

        if ($vinculos[0]['Status'] == '1') {
            $pairs = array('Status' => '0',);
        } else {
            $pairs = array('Status' => '1',);
        }

        $state = DB::update('SCC_Vinculo')
                ->set($pairs)
                ->where('idVinculo', '=', $idVinculo)
                ->execute($this->_db)
        ;
        return $state;
    }

}

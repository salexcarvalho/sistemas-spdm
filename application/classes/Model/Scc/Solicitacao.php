<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Scc_Solicitacao extends Model {

    /**
     * Construtor da classe, já seta a conexao com o banco de dados
     *
     * @param string $con_type
     */
    public function __construct($db = NULL) {
        if ($db != NULL) {
            // Set the instance or name
            $this->_db = $db;
        } else {
            // Use the default name
            $this->_db = Database::$default;
        }

        if (is_string($this->_db)) {
            // Load the database
            $this->_db = Database::instance($this->_db);
        }
    }

    // Insert para solicitante
    public function insert_solicitante($vars) {
        $colunas = array(
            'Nome_Solicitante',
            'Email_Solicitante',
            'Cargo_Solicitante',
            'idVinculo',
            'Tel_Solicitante',
            'Cel_Solicitante',
            'dtCadastro'
        );
        $values = array(
            $vars['nome_solicitante'],
            $vars['email_solicitante'],
            $vars['cargo_solicitante'],
            $vars['vinculo_solicitante'],
            $vars['telefone_solicitante'],
            $vars['celular_solicitante'],
            date('Y-m-d')
        );
        $solicitante = DB::insert("SCC_Solicitante", $colunas)
                ->values($values)
                ->execute($this->_db)
        ;
        return $solicitante;
    }
    
    // Insert para relato
    public function insert_relato($vars, $iDsei) {
        $colunas = array(
            'idTipoRelato',
            'idUnidade',
            'idDsei',
            'dtQuando',
            'Onde',
            'Nome_Envolvidos',
            'Descricao_Relato',
            'Numero_Protocolo_Anterior',
            'dtCadastro'
        );
        $values = array(
            $vars['tipo_relato'],
            $vars['unidade_relato'],
            $iDsei,
            $vars['quando_relato'],
            $vars['onde_relato'],
            $vars['nome_envolvidos'],
            $vars['descricao_da_situacao'],
            $vars['numero_do_protocolo_anterior'],
            date('Y-m-d')
        );
        $relato = DB::insert("SCC_Relato", $colunas)
                ->values($values)
                ->execute($this->_db)
        ;
        return $relato;
    }
    
    // Insert para protocolo
    public function insert_protocolo($Protocolo, $idSolicitante, $idRelato) {
        $colunas = array(
            'idProtocolo',
            'idSolicitante',
            'idRelato',
            'Status_Atendimento'
        );
        $values = array(
            $Protocolo,
            $idSolicitante,
            $idRelato,
            '1'
        );
        $NProtocolo = DB::insert("SCC_Protocolo", $colunas)
                ->values($values)
                ->execute($this->_db)
        ;
        return $NProtocolo;
    }
    
    // Insert para anexo
    public function insert_anexo($Protocolo, $newname, $idResposta = null) {
        $colunas = array(
            'idProtocolo',
            'idResposta',
            'Caminho',
            'dtCadastro'
        );
        $values = array(
            $Protocolo,
            $idResposta,
            $newname,
            date('Y-m-d')
        );
        $anexo = DB::insert("SCC_Anexo", $colunas)
                ->values($values)
                ->execute($this->_db)
        ;
        return $anexo;
    }

}

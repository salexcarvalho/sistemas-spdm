<?php

defined('SYSPATH') or die('No direct script access.');

class Model_Scc_Solicitantes extends Model {

    /**
     * Construtor da classe, já seta a conexao com o banco de dados
     *
     * @param string $con_type
     */
    public function __construct($db = NULL) {
        if ($db != NULL) {
            // Set the instance or name
            $this->_db = $db;
        } else {
            // Use the default name
            $this->_db = Database::$default;
        }

        if (is_string($this->_db)) {
            // Load the database
            $this->_db = Database::instance($this->_db);
        }
    }

    public function select_solicitante($idSolicitante) {
        $solicitante = DB::select("SCC_Solicitante.idSolicitante", "SCC_Solicitante.Nome_Solicitante", "SCC_Solicitante.Email_Solicitante", "SCC_Solicitante.Cargo_Solicitante",
                "SCC_Solicitante.idVinculo", "SCC_Solicitante.Tel_Solicitante", "SCC_Solicitante.Cel_Solicitante", "SCC_Solicitante.Status", "SCC_Solicitante.dtCadastro", array('SCC_Vinculo.Nome_Vinculo', 'Nome_Vinculo'),
                array('SCC_Protocolo.idProtocolo', 'idProtocolo'))
                ->from('SCC_Solicitante')
                ->where('SCC_Solicitante.idSolicitante', '=', $idSolicitante)
                ->join('SCC_Vinculo', 'LEFT')
                ->on('SCC_Vinculo.idVinculo', '=', 'SCC_Solicitante.idVinculo')
                ->join('SCC_Protocolo', 'LEFT')
                ->on('SCC_Protocolo.idSolicitante', '=', 'SCC_Solicitante.idSolicitante')
                ->execute($this->_db)
        ;
        return $solicitante;
    }

    public function select_solicitantes() {
        $solicitante = DB::select("SCC_Solicitante.idSolicitante", "SCC_Solicitante.Nome_Solicitante", "SCC_Solicitante.Email_Solicitante", "SCC_Solicitante.Cargo_Solicitante",
                "SCC_Solicitante.idVinculo", "SCC_Solicitante.Tel_Solicitante", "SCC_Solicitante.Cel_Solicitante", "SCC_Solicitante.Status", "SCC_Solicitante.dtCadastro",
                array('SCC_Vinculo.Nome_Vinculo', 'Nome_Vinculo'), array('SCC_Protocolo.idProtocolo', 'idProtocolo'), array('SCC_Protocolo.idRelato', 'idRelato'), 
                array('SCC_Protocolo.Status_Atendimento', 'Atendimento'))
                ->from('SCC_Solicitante')
                ->join('SCC_Vinculo', 'LEFT')
                ->on('SCC_Vinculo.idVinculo', '=', 'SCC_Solicitante.idVinculo')
                ->join('SCC_Protocolo', 'LEFT')
                ->on('SCC_Protocolo.idSolicitante', '=', 'SCC_Solicitante.idSolicitante');
                if (($_SESSION['Perfil'] !== 'Administrador') && ($_SESSION['AcLiberaAdmRHM'] == false)) {
                    $relato->where('SCC_Vinculo.idTipoSetor', '=', $_SESSION['idSetor']);
                }
                return $solicitante->order_by('SCC_Solicitante.idSolicitante', 'DESC')->execute($this->_db);
    }

    public function select_quantidade() {
        return $this->select_solicitantes()->count();
    }

    public function insert_solicitante($Nome_solicitante, $Sigla_solicitante, $status) {
        $colunas = array(
            'Nome_Solicitante',
            'Sigla_Solicitante',
            'dtCadastro',
            'Status',
            'idUsuario'
        );
        $values = array(
            $Nome_solicitante,
            $Sigla_solicitante,
            date('yy-m-d'),
            $status,
            $_SESSION['idUsuario']
        );
        $returned_id = DB::insert("SCC_Solicitante", $colunas)
                ->values($values)
                ->execute($this->_db)
        ;
        return $returned_id;
    }

    public function update_solicitante($idSolicitante, $Nome_solicitante, $Sigla_solicitante, $Status) {
        $state = DB::update('SCC_Solicitante')
                ->set(array('Nome_Solicitante' => $Nome_solicitante))
                ->set(array('Sigla_Solicitante' => $Sigla_solicitante))
                ->set(array('Status' => $Status))
                ->set(array('idUsuario' => $_SESSION['idUsuario']))
                ->set(array('dtAlteracao' => date('yy-m-d')))
                ->where('idSolicitante', '=', $idSolicitante)
                ->execute($this->_db)
        ;
        return $state;
    }

    public function delete_solicitante($idSolicitante) {
        $returned_id = DB::delete('SCC_Solicitante')
                ->where('idSolicitante', '=', $idSolicitante)
                ->execute($this->_db);
        return $returned_id;
    }

    public function update_ativar_solicitante($idSolicitante) {

        $solicitantes = DB::select('SCC_Solicitante.Status')
                ->from('SCC_Solicitante')
                ->where('idSolicitante', '=', $idSolicitante)
                ->execute($this->_db)
        ;

        if ($solicitantes[0]['Status'] == '1') {
            $pairs = array('Status' => '0',);
        } else {
            $pairs = array('Status' => '1',);
        }

        $state = DB::update('SCC_Solicitante')
                ->set($pairs)
                ->where('idSolicitante', '=', $idSolicitante)
                ->execute($this->_db)
        ;
        return $state;
    }

}

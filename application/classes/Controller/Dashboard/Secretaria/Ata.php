<?php
defined('SYSPATH') or die('No direct script access.');

class Controller_Dashboard_Secretaria_Ata extends Controller {

    public function action_index() {
        $this->redirect('dashboard/secretaria_ata');
    }

    public function action_home() {
	Funcoes::verifica_login();

        $page = $this->request->query('page');
        $Pesquisa = ($this->request->query('Busca')) ? $this->request->query('Busca') : '';
        $option = '&Busca=' . $Pesquisa;

        $model_ata = new Model_Secretaria_Ata('default');
        //$model_tipodocumentos = new Model_Secretaria_Tipodocumentos('default');
        $dados = $model_ata->select_atas($Pesquisa);
        //$TipoDocs = $model_tipodocumentos->select_tipos('');
        
        $paginator = Paginator::factory($dados);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home', 'page', $option);

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($dados);

        $view = View::factory('adm/secretaria/ata')
                //->set('TipoDocs', $TipoDocs)
                ->set('Pesquisa', $Pesquisa)
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total);

        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }
    
    public function action_ata() {
	Funcoes::verifica_login();
        
        $idAta = $this->request->query('idAta');
        $page = $this->request->query('page');
        $idModulo = '1';

        $model_Ata = new Model_Secretaria_Ata('default');
        $model_Anexo= new Model_Secretaria_Anexo('default');
        $model_Setor= new Model_Login_Setor('user');  
        $model_tipodocumentos = new Model_Secretaria_Tipodocumentos('default');
                
        $Ata = $model_Ata->select_ata($idAta);
        $Anexo = $model_Anexo->select_anexo_ata($idAta);
        $Setor = $model_Setor->select_tipo(); 
        $TipoDocs = $model_tipodocumentos->select_tipos_modulo($idModulo);
        
        $view = View::factory('adm/secretaria/set_ata')
                ->set('idAta', $idAta)
                ->set('TipoDocs', $TipoDocs)
                ->set('page', $page)
                ->set('Ata', $Ata)
                ->set('Anexo', $Anexo)
                ->set('Setor', $Setor);

        $this->response->body($view);
    }
    
    public function action_visualizar_ata() {
	Funcoes::verifica_login();
        
        $idAta = $this->request->query('idAta');

        $model_Ata = new Model_Secretaria_Ata('default');
        $model_Anexo= new Model_Secretaria_Anexo('default');
        $model_Setor= new Model_Login_Setor('user');     
                
        $Ata = $model_Ata->select_ata($idAta);
        $Anexo = $model_Anexo->select_anexo_ata($idAta);
        $Setor = $model_Setor->select_tipo();        
        
        $view = View::factory('adm/secretaria/visualizar_ata')
                ->set('idAta', $idAta)
                ->set('Ata', $Ata)
                ->set('Anexo', $Anexo)
                ->set('Setor', $Setor);

        $this->response->body($view);
    }
    
    public function action_insert_ata() {
	Funcoes::verifica_login();
        
        $vars = $this->request->post();
        $page = $this->request->post('page');
        $Pesquisa = ($this->request->query('Busca')) ? $this->request->query('Busca') : '';
        $option = '&Busca=' . $Pesquisa;
        
        $DataReuniao = Funcoes::convertData($this->request->post('dtReuniao'));
        $dtReuniao = $DataReuniao . " " . $vars['horaReuniao'];
        
        $model_Ata = new Model_Secretaria_Ata('default');
        $model_Anexo= new Model_Secretaria_Anexo('default');
                
        $Ata = $model_Ata->insert_ata($vars, $dtReuniao);
        
        // * Inserindo Anexo *
        $AtaName = date('Ymd', time()) . Function_Certificados::coderand(8);
        $anexos = $_FILES['anexo'];
        //Funcoes::dump($anexos);
        $img_desc = Funcoes::reArrayFiles($anexos);
        $i = 1;
        foreach ($img_desc as $val) {
            $ext = pathinfo($val['name'], PATHINFO_EXTENSION);
            $newname = $AtaName . '_Anexo_' . $i . '.' . $ext;
            $envia = Upload::save($val, $newname, 'upload/sep/ata');
            if ($envia != false) {
                $anexar = $model_Anexo->insert_anexo_ata($Ata[0], $newname);
            }
            $i++;
        }
        
        $warning = "Ata cadastrada com sucesso";
        $dados = $model_Ata->select_atas($Pesquisa);
        $paginator = Paginator::factory($dados);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home', 'page', $option);

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($dados);

        $view = View::factory('adm/secretaria/ata')
                ->set('warning', $warning)
                ->set('Pesquisa', $Pesquisa)
                ->set('dados', $dados)
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total);

        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }
    
    public function action_update_ata() {
	Funcoes::verifica_login();
        
        $idAta = $this->request->post('idAta');
        $vars = $this->request->post();
        $page = $this->request->post('page');
        $Pesquisa = ($this->request->query('Busca')) ? $this->request->query('Busca') : '';
        $option = '&Busca=' . $Pesquisa;
        
        $DataReuniao = Funcoes::convertData($this->request->post('dtReuniao'));
        $dtReuniao = $DataReuniao . " " . $vars['horaReuniao'];

        $model_Ata = new Model_Secretaria_Ata('default');
        $model_Anexo = new Model_Secretaria_Anexo('default');
                
        $Ata = $model_Ata->update_ata($idAta, $vars, $dtReuniao);
        
        // * Inserindo Anexo *
        $AtaName = date('Ymd', time()) . Function_Certificados::coderand(8);
        $anexos = $_FILES['anexo'];
        $img_desc = Funcoes::reArrayFiles($anexos);
        $i = 1;
        foreach ($img_desc as $val) {
            $ext = pathinfo($val['name'], PATHINFO_EXTENSION);
            $newname = $AtaName . '_Anexo_' . $i . '.' . $ext;
            $envia = Upload::save($val, $newname, 'upload/sep/ata');
            if ($envia != false) {
                $anexar = $model_Anexo->insert_anexo_ata($idAta, $newname);
            }
            $i++;
        }
        
        $warning = "Ata atualizada com sucesso";
        $dados = $model_Ata->select_atas($Pesquisa);
        $paginator = Paginator::factory($dados);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home', 'page', $option);

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($dados);

        $view = View::factory('adm/secretaria/ata')
                ->set('warning', $warning)
                ->set('Pesquisa', $Pesquisa)
                ->set('dados', $dados)
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total);

        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }

    public function action_delete_anexo() {
	Funcoes::verifica_login();
        $idAnexo = $this->request->query('idAnexo');
        $idAta = $this->request->query('idAta');
        $model_Anexo = new Model_Secretaria_Anexo('default');
        $Anexo = $model_Anexo->select_anexo_ata($idAta);  
        foreach ($Anexo as $filename) {
             if ($filename['idAnexo'] == $idAnexo) {
                unlink('upload/sep/ata/'.$filename['Caminho']);
            }
        }
        $model_Anexo->delete_anexo($idAnexo);
        $this->redirect('dashboard/secretaria_ata/home');
    }

    public function action_delete_ata() {
        Funcoes::verifica_login();
        $idAta = $this->request->post('idAta');
        $model_Ata = new Model_Secretaria_Ata('default');
        $model_Anexo = new Model_Secretaria_Anexo('default');
        $Anexo = $model_Anexo->select_anexo_ata($idAta);  
        foreach ($Anexo as $filename) {
           unlink('upload/sep/ata/'.$filename['Caminho']);
        }
        $del_idAnexo  = $model_Anexo->delete_anexo_ata($idAta);
        $del_idAta  = $model_Ata->delete_ata($idAta);
        return $del_idAta;
    }
    
    public function action_update_ativar_ata() {
	Funcoes::verifica_login();
        
        $idAta = $this->request->query('idAta');

        $model_ata = new Model_Secretaria_Ata('default');
        $model_ata->update_ativar_ata($idAta);
        $atas = $model_ata->select_ata($idAta);

        if ($atas[0]['Status'] != "1") {//se retornar algum resultado
            $view = '0';
        } else {
            $view = '1';
        }
        $this->response->body($view);
    }
    
 }
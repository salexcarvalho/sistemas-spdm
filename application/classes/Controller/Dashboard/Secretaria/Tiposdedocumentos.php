<?php
defined('SYSPATH') or die('No direct script access.');

class Controller_Dashboard_Secretaria_Tiposdedocumentos extends Controller {

    public function action_index() {
        $this->redirect('dashboard/secretaria_tipodocumentos/home');
    }

    public function action_home() {
	Funcoes::verifica_login();

        $page = $this->request->query('page');
        $Pesquisa = ($this->request->query('Busca')) ? $this->request->query('Busca') : '';
        $option = '&Busca=' . $Pesquisa;

        $model_tipodocumentos = new Model_Secretaria_Tipodocumentos('default');
        $dados = $model_tipodocumentos->select_tipos($Pesquisa);
        $paginator = Paginator::factory($dados);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home', 'page', $option);

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($dados);

        $view = View::factory('adm/secretaria/tipodocumentos')
                ->set('Pesquisa', $Pesquisa)
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total);

        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }
    
    public function action_cadastro_tipodocumentos() {
	Funcoes::verifica_login();
        $page = $this->request->query('page');
        
        $model_modulos = new Model_Secretaria_Modulos('default');
        $modulos = $model_modulos->select_modulos();
        
        $view = View::factory('adm/secretaria/cadastro_tipodocumentos')
                ->set('modulos', $modulos)
                ->set('page', $page);
        $this->response->body($view);
    }

    public function action_insert_tipodocumentos() {
	Funcoes::verifica_login();
        Session::instance();
        $idModulo = $this->request->post('idModulo');
        $Nome = $this->request->post('Nome');
        $Alias = $this->request->post('Alias');
        $Status = $this->request->post('status');

        $model_tipo = new Model_Secretaria_Tipodocumentos('default');
        $model_tipo->insert_tipo($idModulo, $Nome, $Alias, $Status);

        /* Mostra a resposta da ação */
        $warning = "Tipo de Despesa cadastrada com sucesso!";
        
        $page = $this->request->query('page');
        $Pesquisa = ($this->request->query('Busca')) ? $this->request->query('Busca') : '';
        $option = '&Busca=' . $Pesquisa;
        
        $dados = $model_tipo->select_tipos($Pesquisa);
        $paginator = Paginator::factory($dados);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home', 'page', $option);

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($dados);
        
        $view = View::factory('adm/secretaria/tipodocumentos')
                ->set('Pesquisa', $Pesquisa)
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
                ->set('warning', $warning)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }

    public function action_editar_tipodocumentos() {
	Funcoes::verifica_login();

        $page = $this->request->query('page');
        $idTipoDoc = $this->request->query('idTipoDoc');
        
        $model_modulos = new Model_Secretaria_Modulos('default');
        $model_tipos = new Model_Secretaria_Tipodocumentos('default');
        $modulos = $model_modulos->select_modulos();
        $tipos = $model_tipos->select_tipo($idTipoDoc);

        $view = View::factory('adm/secretaria/editar_tipodocumentos')
                ->set('modulos', $modulos)
                ->set('tipos', $tipos)
                ->set('page', $page)
        ;
        $this->response->body($view);
    }

    public function action_update_tipodocumentos() {
        Session::instance();
        $idTipoDoc = $this->request->post('idTipoDoc');
        $idModulo = $this->request->post('idModulo');
        $Nome = $this->request->post('Nome');
        $Alias = $this->request->post('Alias');
        $Status = $this->request->post('status');

        $model_tipodocumentos = new Model_Secretaria_Tipodocumentos('default');
        $model_tipodocumentos->update_tipo($idTipoDoc, $idModulo, $Nome, $Alias, $Status);

        /* Mostra a resposta da ação */
        $warning = "Tipo de Despesa atualizada com sucesso!";
        
        $page = $this->request->post('page');
        $Pesquisa = ($this->request->query('Busca')) ? $this->request->query('Busca') : '';
        $option = '&Busca=' . $Pesquisa;
        
        $dados = $model_tipodocumentos->select_tipos($Pesquisa);
        $paginator = Paginator::factory($dados);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home', 'page', $option);

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($dados);
        
        $view = View::factory('adm/secretaria/tipodocumentos')
                ->set('Pesquisa', $Pesquisa)
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
                ->set('warning', $warning)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }

    public function action_delete_tipodocumentos() {
	Funcoes::verifica_login();

        $idTipoDoc = $this->request->post('idTipoDoc');

        $model_tipodocumentos = new Model_Secretaria_Tipodocumentos('default');
        $model_tipodocumentos->delete_tipo($idTipoDoc);

        $this->redirect('dashboard/secretaria_tiposdedocumentos/home');
    }

    public function action_update_ativar_tipodocumentos() {
	Funcoes::verifica_login();
        
        $idTipoDoc = $this->request->query('idTipoDoc');

        $model_tipodocumentos = new Model_Secretaria_Tipodocumentos('default');
        $model_tipodocumentos->update_ativar_tipo($idTipoDoc);
        $tipos = $model_tipodocumentos->select_tipo($idTipoDoc);

        if ($tipos[0]['Status'] != "1") {//se retornar algum resultado
            $view = '0';
        } else {
            $view = '1';
        }
        $this->response->body($view);
    }
    
 }
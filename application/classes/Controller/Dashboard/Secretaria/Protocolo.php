<?php
defined('SYSPATH') or die('No direct script access.');

class Controller_Dashboard_Secretaria_Protocolo extends Controller {

    public function action_index() {
        $this->redirect('dashboard/secretaria_protocolo');
    }

    public function action_home() {
	Funcoes::verifica_login();

        $page = $this->request->query('page');
        $Pesquisa = ($this->request->query('Busca')) ? $this->request->query('Busca') : '';
        $option = '&Busca=' . $Pesquisa;

        $model_protocolo = new Model_Secretaria_Protocolo('default');
        $dados = $model_protocolo->select_protocolos($Pesquisa);
        $paginator = Paginator::factory($dados);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home', 'page', $option);

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($dados);

        $view = View::factory('adm/secretaria/home')
                ->set('Pesquisa', $Pesquisa)
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total);

        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }
    
    public function action_protocolo() {
	Funcoes::verifica_login();
        
        $idProtocolo = $this->request->query('idProtocolo');
        $page = $this->request->query('page');

        $model_Protocolo = new Model_Secretaria_Protocolo('default');
        $model_TipoProtocolo = new Model_Secretaria_Tipodocumentos('default');
        $model_Anexo= new Model_Secretaria_Anexo('default');
        $model_Setor= new Model_Login_Setor('user');     
                
        $Protocolo = $model_Protocolo->select_protocolo($idProtocolo);
        $TipoDocumento = $model_TipoProtocolo->select_tipos('');
        $Anexo = $model_Anexo->select_anexo_protocolo($idProtocolo);
        $Setor = $model_Setor->select_tipo();        
        
        $view = View::factory('adm/secretaria/set_protocolo')
                ->set('idProtocolo', $idProtocolo)
                ->set('page', $page)
                ->set('Protocolo', $Protocolo)
                ->set('TipoDocumento', $TipoDocumento)
                ->set('Anexo', $Anexo)
                ->set('Setor', $Setor);

        $this->response->body($view);
    }
    
    public function action_visualizar_protocolo() {
	Funcoes::verifica_login();
        
        $idProtocolo = $this->request->query('idProtocolo');

        $model_Protocolo = new Model_Secretaria_Protocolo('default');
        $model_TipoProtocolo = new Model_Secretaria_Tipodocumentos('default');
        $model_Anexo= new Model_Secretaria_Anexo('default');
        $model_Setor= new Model_Login_Setor('user');     
                
        $Protocolo = $model_Protocolo->select_protocolo($idProtocolo);
        $TipoDocumento = $model_TipoProtocolo->select_tipos('');
        $Anexo = $model_Anexo->select_anexo_protocolo($idProtocolo);
        $Setor = $model_Setor->select_tipo();        
        
        $view = View::factory('adm/secretaria/visualizar_protocolo')
                ->set('idProtocolo', $idProtocolo)
                ->set('Protocolo', $Protocolo)
                ->set('TipoDocumento', $TipoDocumento)
                ->set('Anexo', $Anexo)
                ->set('Setor', $Setor);

        $this->response->body($view);
    }
    
    public function action_insert_protocolo() {
	Funcoes::verifica_login();
        
        $vars = $this->request->post();
        $page = $this->request->post('page');
        $Pesquisa = ($this->request->query('Busca')) ? $this->request->query('Busca') : '';
        $option = '&Busca=' . $Pesquisa;
        
        $dtEntrada = Funcoes::convertData($this->request->post('dtEntrada'));
        $dtSaida = Funcoes::convertData($this->request->post('dtSaida'));
        $Entrada = $dtEntrada . " " . $vars['horaEntrada'];
        $Saida = $dtSaida . " " . $vars['horaSaida'];

        $model_Protocolo = new Model_Secretaria_Protocolo('default');
        $model_Anexo= new Model_Secretaria_Anexo('default');
                
        $Protocolo = $model_Protocolo->insert_protocolo($vars, $Entrada, $Saida);
        
        // * Inserindo Anexo *
        $ProtocoloName = date('Ymd', time()) . Function_Certificados::coderand(8);
        $anexos = $_FILES['anexo'];
        //Funcoes::dump($anexos);
        $img_desc = Funcoes::reArrayFiles($anexos);
        $i = 1;
        foreach ($img_desc as $val) {
            $ext = pathinfo($val['name'], PATHINFO_EXTENSION);
            $newname = $ProtocoloName . '_Anexo_' . $i . '.' . $ext;
            $envia = Upload::save($val, $newname, 'upload/sep/protocolo');
            if ($envia != false) {
                $anexar = $model_Anexo->insert_anexo_protocolo($Protocolo[0], $newname);
            }
            $i++;
        }
        
        $warning = "Protocolo inserido com sucesso";
        $dados = $model_Protocolo->select_protocolos($Pesquisa);
        $paginator = Paginator::factory($dados);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home', 'page', $option);

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($dados);

        $view = View::factory('adm/secretaria/home')
                ->set('warning', $warning)
                ->set('Pesquisa', $Pesquisa)
                ->set('dados', $dados)
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total);

        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }
    
    public function action_update_protocolo() {
	Funcoes::verifica_login();
        
        $idProtocolo = $this->request->post('idProtocolo');
        $vars = $this->request->post();
        $page = $this->request->post('page');
        $Pesquisa = ($this->request->query('Busca')) ? $this->request->query('Busca') : '';
        $option = '&Busca=' . $Pesquisa;
        
        $dtEntrada = Funcoes::convertData($this->request->post('dtEntrada'));
        $dtSaida = Funcoes::convertData($this->request->post('dtSaida'));
        $Entrada = $dtEntrada . " " . $vars['horaEntrada'];
        $Saida = $dtSaida . " " . $vars['horaSaida'];

        $model_Protocolo = new Model_Secretaria_Protocolo('default');
        $model_Anexo = new Model_Secretaria_Anexo('default');
                
        $Protocolo = $model_Protocolo->update_protocolo($idProtocolo, $vars, $Entrada, $Saida);
        
        // * Inserindo Anexo *
        $ProtocoloName = date('Ymd', time()) . Function_Certificados::coderand(8);
        $anexos = $_FILES['anexo'];
        $img_desc = Funcoes::reArrayFiles($anexos);
        $i = 1;
        foreach ($img_desc as $val) {
            $ext = pathinfo($val['name'], PATHINFO_EXTENSION);
            $newname = $ProtocoloName . '_Anexo_' . $i . '.' . $ext;
            $envia = Upload::save($val, $newname, 'upload/sep/protocolo');
            if ($envia != false) {
                $anexar = $model_Anexo->insert_anexo_protocolo($idProtocolo, $newname);
            }
            $i++;
        }
        
        $warning = "Protocolo atualizado com sucesso";
        $dados = $model_Protocolo->select_protocolos($Pesquisa);
        $paginator = Paginator::factory($dados);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home', 'page', $option);

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($dados);

        $view = View::factory('adm/secretaria/home')
                ->set('warning', $warning)
                ->set('Pesquisa', $Pesquisa)
                ->set('dados', $dados)
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total);

        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }

    public function action_delete_anexo() {
	Funcoes::verifica_login();
        $idAnexo = $this->request->query('idAnexo');
        $idProtocolo = $this->request->query('idProtocolo');
        $model_Anexo = new Model_Secretaria_Anexo('default');
        $Anexo = $model_Anexo->select_anexo_protocolo($idProtocolo);  
        foreach ($Anexo as $filename) {
             if ($filename['idAnexo'] == $idAnexo) {
                unlink('upload/sep/protocolo/'.$filename['Caminho']);
            }
        }
        $model_Anexo->delete_anexo($idAnexo);
        $this->redirect('dashboard/secretaria_protocolo/home');
    }

    public function action_delete_protocolo() {
        Funcoes::verifica_login();
        $idProtocolo = $this->request->post('idProtocolo');
        $model_Protocolo = new Model_Secretaria_Protocolo('default');
        $model_Anexo = new Model_Secretaria_Anexo('default');
        $Anexo = $model_Anexo->select_anexo_protocolo($idProtocolo);  
        foreach ($Anexo as $filename) {
           unlink('upload/sep/protocolo/'.$filename['Caminho']);
        }
        $del_idAnexo  = $model_Anexo->delete_anexo_protocolo($idProtocolo);
        $del_idProtocolo  = $model_Protocolo->delete_protocolo($idProtocolo);
        return $del_idProtocolo;
    }
    
    public function action_update_ativar_protocolo() {
	Funcoes::verifica_login();
        
        $idProtocolo = $this->request->query('idProtocolo');

        $model_protocolo = new Model_Secretaria_Protocolo('default');
        $model_protocolo->update_ativar_protocolo($idProtocolo);
        $protocolos = $model_protocolo->select_protocolo($idProtocolo);

        if ($protocolos[0]['Status'] != "1") {//se retornar algum resultado
            $view = '0';
        } else {
            $view = '1';
        }
        $this->response->body($view);
    }
    
 }
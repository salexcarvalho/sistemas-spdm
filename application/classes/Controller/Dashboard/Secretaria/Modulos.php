<?php
defined('SYSPATH') or die('No direct script access.');

class Controller_Dashboard_Secretaria_Modulos extends Controller {

    public function action_index() {
        $this->redirect('dashboard/secretaria_modulos/home');
    }

    public function action_home() {
	Funcoes::verifica_login();

        $page = $this->request->query('page');

        $model_modulo = new Model_Secretaria_Modulos('default');
        $dados = $model_modulo->select_modulos();
        $paginator = Paginator::factory($dados);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home', 'page');

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($dados);

        $view = View::factory('adm/secretaria/modulos')
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total);

        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }
    
    public function action_cadastro_modulo() {
	Funcoes::verifica_login();
        $page = $this->request->query('page');
        
        $view = View::factory('adm/secretaria/cadastro_modulos')
                ->set('page', $page);
        $this->response->body($view);
    }

    public function action_insert_modulo() {
	Funcoes::verifica_login();
        Session::instance();
        $Nome = $this->request->post('Nome');
        $Alias = $this->request->post('Alias');
        $Status = $this->request->post('status');

        $model_modulo = new Model_Secretaria_Modulos('default');
        $model_modulo->insert_modulo($Nome, $Alias, $Status);

        /* Mostra a resposta da ação */
        $warning = "Módulo cadastrado com sucesso!";
        $page = $this->request->query('page');
        $dados = $model_modulo->select_modulos();
        $paginator = Paginator::factory($dados);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home', 'page');

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($dados);
        
        $view = View::factory('adm/secretaria/modulos')
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
                ->set('warning', $warning)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }

    public function action_editar_modulo() {
	Funcoes::verifica_login();

        $page = $this->request->query('page');
        $idModulo = $this->request->query('idModulo');

        $model_modulos = new Model_Secretaria_Modulos('default');
        $modulos = $model_modulos->select_modulo($idModulo);

        $view = View::factory('adm/secretaria/editar_modulos')
                ->set('modulos', $modulos)
                ->set('page', $page)
        ;
        $this->response->body($view);
    }

    public function action_update_modulo() {
        Session::instance();
        $idModulo = $this->request->post('idModulo');
        $Nome = $this->request->post('Nome');
        $Alias = $this->request->post('Alias');
        $Status = $this->request->post('status');

        $model_modulos = new Model_Secretaria_Modulos('default');
        $model_modulos->update_modulo($idModulo, $Nome, $Alias, $Status);

        /* Mostra a resposta da ação */
        $warning = "Módulo atualizado com sucesso!";
        $page = $this->request->post('page');
        $dados = $model_modulos->select_modulos();
        $paginator = Paginator::factory($dados);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home', 'page');

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($dados);
        
        $view = View::factory('adm/secretaria/modulos')
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
                ->set('warning', $warning)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }

    public function action_delete_modulo() {
	Funcoes::verifica_login();

        $idModulo = $this->request->post('idModulo');

        $model_modulos = new Model_Secretaria_Modulos('default');
        $model_modulos->delete_modulo($idModulo);

        $this->redirect('dashboard/secretaria_modulos/home');
    }

    public function action_update_ativar_modulo() {
	Funcoes::verifica_login();
        
        $idModulo = $this->request->query('idModulo');

        $model_modulos = new Model_Secretaria_Modulos('default');
        $model_modulos->update_ativar_modulo($idModulo);
        $modulos = $model_modulos->select_modulo($idModulo);

        if ($modulos[0]['Status'] != "1") {//se retornar algum resultado
            $view = '0';
        } else {
            $view = '1';
        }
        $this->response->body($view);
    }
    
 }
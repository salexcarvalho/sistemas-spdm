<?php
defined('SYSPATH') or die('No direct script access.');

class Controller_Dashboard_Secretaria_Documentos extends Controller {

    public function action_index() {
        $this->redirect('dashboard/secretaria_documentos');
    }

    public function action_home() {
	Funcoes::verifica_login();

        $page = $this->request->query('page');
        $Pesquisa = ($this->request->query('Busca')) ? $this->request->query('Busca') : '';
        $option = '&Busca=' . $Pesquisa;

        $model_documento = new Model_Secretaria_Documentos('default');
        //$model_tipodocumentos = new Model_Secretaria_Tipodocumentos('default');
        $dados = $model_documento->select_documentos($Pesquisa);
        //$TipoDocs = $model_tipodocumentos->select_tipos('');
        
        $paginator = Paginator::factory($dados);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home', 'page', $option);

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($dados);

        $view = View::factory('adm/secretaria/documentos')
                ->set('Pesquisa', $Pesquisa)
                //->set('TipoDocs', $TipoDocs)
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total);

        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }
    
    public function action_documentos() {
	Funcoes::verifica_login();
        
        $idDocumentos = $this->request->query('idDocumentos');
        $page = $this->request->query('page');
        $idModulo = '6';

        $model_Documentos = new Model_Secretaria_Documentos('default');
        $model_Anexo= new Model_Secretaria_Anexo('default');
        $model_Setor= new Model_Login_Setor('user');
        $model_tipodocumentos = new Model_Secretaria_Tipodocumentos('default');
        
        $Documentos = $model_Documentos->select_documento($idDocumentos);
        $Anexo = $model_Anexo->select_anexo_documento($idDocumentos);
        $Setor = $model_Setor->select_tipo();  
        $TipoDocs = $model_tipodocumentos->select_tipos_modulo($idModulo);
        
        $view = View::factory('adm/secretaria/set_documentos')
                ->set('idDocumentos', $idDocumentos)
                ->set('TipoDocs', $TipoDocs)
                ->set('page', $page)
                ->set('Documentos', $Documentos)
                ->set('Anexo', $Anexo)
                ->set('Setor', $Setor);

        $this->response->body($view);
    }
    
    public function action_visualizar_documento() {
	Funcoes::verifica_login();
        
        $idDocumentos = $this->request->query('idDocumentos');

        $model_Documentos = new Model_Secretaria_Documentos('default');
        $model_Anexo= new Model_Secretaria_Anexo('default');
        $model_Setor= new Model_Login_Setor('user');     
                
        $Documentos = $model_Documentos->select_documento($idDocumentos);
        $Anexo = $model_Anexo->select_anexo_documento($idDocumentos);
        $Setor = $model_Setor->select_tipo();        
        
        $view = View::factory('adm/secretaria/visualizar_documentos')
                ->set('idDocumentos', $idDocumentos)
                ->set('Documentos', $Documentos)
                ->set('Anexo', $Anexo)
                ->set('Setor', $Setor);

        $this->response->body($view);
    }
    
    public function action_insert_documento() {
	Funcoes::verifica_login();
        
        $vars = $this->request->post();
        $page = $this->request->post('page');
        $Pesquisa = ($this->request->query('Busca')) ? $this->request->query('Busca') : '';
        $option = '&Busca=' . $Pesquisa;
        
        $dtArquivo = Funcoes::convertData($this->request->post('dtArquivo'));
        $dtArquivo = $dtArquivo . " " . $vars['horaArquivo'];
        
        $model_Documentos = new Model_Secretaria_Documentos('default');
        $model_Anexo= new Model_Secretaria_Anexo('default');
                
        $Documentos = $model_Documentos->insert_documento($vars, $dtArquivo);
        
        // * Inserindo Anexo *
        $DocumentosName = date('Ymd', time()) . Function_Certificados::coderand(8);
        $anexos = $_FILES['anexo'];
        //Funcoes::dump($anexos);
        $img_desc = Funcoes::reArrayFiles($anexos);
        $i = 1;
        foreach ($img_desc as $val) {
            $ext = pathinfo($val['name'], PATHINFO_EXTENSION);
            $newname = $DocumentosName . '_Anexo_' . $i . '.' . $ext;
            $envia = Upload::save($val, $newname, 'upload/sep/documento');
            if ($envia != false) {
                $anexar = $model_Anexo->insert_anexo_documento($Documentos[0], $newname);
            }
            $i++;
        }
        
        $warning = "Documentos cadastrado com sucesso";
        $dados = $model_Documentos->select_documentos($Pesquisa);
        $paginator = Paginator::factory($dados);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home', 'page', $option);

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($dados);

        $view = View::factory('adm/secretaria/documentos')
                ->set('warning', $warning)
                ->set('Pesquisa', $Pesquisa)
                ->set('dados', $dados)
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total);

        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }
    
    public function action_update_documento() {
	Funcoes::verifica_login();
        
        $idDocumentos = $this->request->post('idDocumentos');
        $vars = $this->request->post();
        $page = $this->request->post('page');
        $Pesquisa = ($this->request->query('Busca')) ? $this->request->query('Busca') : '';
        $option = '&Busca=' . $Pesquisa;
        
        $dtArquivo = Funcoes::convertData($this->request->post('dtArquivo'));
        $dtArquivo = $dtArquivo . " " . $vars['horaArquivo'];

        $model_Documentos = new Model_Secretaria_Documentos('default');
        $model_Anexo = new Model_Secretaria_Anexo('default');
                
        $Documentos = $model_Documentos->update_documento($idDocumentos, $vars, $dtArquivo);
        
        // * Inserindo Anexo *
        $DocumentosName = date('Ymd', time()) . Function_Certificados::coderand(8);
        $anexos = $_FILES['anexo'];
        $img_desc = Funcoes::reArrayFiles($anexos);
        $i = 1;
        foreach ($img_desc as $val) {
            $ext = pathinfo($val['name'], PATHINFO_EXTENSION);
            $newname = $DocumentosName . '_Anexo_' . $i . '.' . $ext;
            $envia = Upload::save($val, $newname, 'upload/sep/documento');
            if ($envia != false) {
                $anexar = $model_Anexo->insert_anexo_documento($idDocumentos, $newname);
            }
            $i++;
        }
        
        $warning = "Documentos atualizado com sucesso";
        $dados = $model_Documentos->select_documentos($Pesquisa);
        $paginator = Paginator::factory($dados);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home', 'page', $option);

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($dados);

        $view = View::factory('adm/secretaria/documentos')
                ->set('warning', $warning)
                ->set('Pesquisa', $Pesquisa)
                ->set('dados', $dados)
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total);

        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }

    public function action_delete_anexo() {
	Funcoes::verifica_login();
        $idAnexo = $this->request->query('idAnexo');
        $idDocumentos = $this->request->query('idDocumentos');
        $model_Anexo = new Model_Secretaria_Anexo('default');
        $Anexo = $model_Anexo->select_anexo_documento($idDocumentos);  
        foreach ($Anexo as $filename) {
             if ($filename['idAnexo'] == $idAnexo) {
                unlink('upload/sep/documento/'.$filename['Caminho']);
            }
        }
        $model_Anexo->delete_anexo($idAnexo);
        $this->redirect('dashboard/secretaria_documentos/home');
    }

    public function action_delete_documento() {
        Funcoes::verifica_login();
        $idDocumentos = $this->request->post('idDocumentos');
        $model_Documentos = new Model_Secretaria_Documentos('default');
        $model_Anexo = new Model_Secretaria_Anexo('default');
        $Anexo = $model_Anexo->select_anexo_documento($idDocumentos);  
        foreach ($Anexo as $filename) {
           unlink('upload/sep/documento/'.$filename['Caminho']);
        }
        $del_idAnexo  = $model_Anexo->delete_anexo_documento($idDocumentos);
        $del_idDocumentos  = $model_Documentos->delete_documento($idDocumentos);
        return $del_idDocumentos;
    }
    
    public function action_update_ativar_documento() {
	Funcoes::verifica_login();
        
        $idDocumentos = $this->request->query('idDocumentos');

        $model_documento = new Model_Secretaria_Documentos('default');
        $model_documento->update_ativar_documento($idDocumentos);
        $documentos = $model_documento->select_documento($idDocumentos);

        if ($documentos[0]['Status'] != "1") {//se retornar algum resultado
            $view = '0';
        } else {
            $view = '1';
        }
        $this->response->body($view);
    }
    
 }
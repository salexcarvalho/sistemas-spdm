<?php
defined('SYSPATH') or die('No direct script access.');

class Controller_Dashboard_Secretaria_Contrato extends Controller {

    public function action_index() {
        $this->redirect('dashboard/secretaria_contrato');
    }

    public function action_home() {
	Funcoes::verifica_login();

        $page = $this->request->query('page');
        $Pesquisa = ($this->request->query('Busca')) ? $this->request->query('Busca') : '';
        $option = '&Busca=' . $Pesquisa;

        $model_contrato = new Model_Secretaria_Contrato('default');
        //$model_tipodocumentos = new Model_Secretaria_Tipodocumentos('default');
        $dados = $model_contrato->select_contratos($Pesquisa);
        //$TipoDocs = $model_tipodocumentos->select_tipos('');
        
        $paginator = Paginator::factory($dados);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home', 'page', $option);

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($dados);

        $view = View::factory('adm/secretaria/contrato')
                ->set('Pesquisa', $Pesquisa)
                //->set('TipoDocs', $TipoDocs)
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total);

        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }
    
    public function action_contrato() {
	Funcoes::verifica_login();
        
        $idContrato = $this->request->query('idContrato');
        $page = $this->request->query('page');
        $idModulo = '2';

        $model_Contrato = new Model_Secretaria_Contrato('default');
        $model_Anexo= new Model_Secretaria_Anexo('default');
        $model_Setor= new Model_Login_Setor('user');
        $model_tipodocumentos = new Model_Secretaria_Tipodocumentos('default');
        
        $Contrato = $model_Contrato->select_contrato($idContrato);
        $Anexo = $model_Anexo->select_anexo_contrato($idContrato);
        $Setor = $model_Setor->select_tipo();  
        $TipoDocs = $model_tipodocumentos->select_tipos_modulo($idModulo);
        
        $view = View::factory('adm/secretaria/set_contrato')
                ->set('idContrato', $idContrato)
                ->set('TipoDocs', $TipoDocs)
                ->set('page', $page)
                ->set('Contrato', $Contrato)
                ->set('Anexo', $Anexo)
                ->set('Setor', $Setor);

        $this->response->body($view);
    }
    
    public function action_visualizar_contrato() {
	Funcoes::verifica_login();
        
        $idContrato = $this->request->query('idContrato');

        $model_Contrato = new Model_Secretaria_Contrato('default');
        $model_Anexo= new Model_Secretaria_Anexo('default');
        $model_Setor= new Model_Login_Setor('user');     
                
        $Contrato = $model_Contrato->select_contrato($idContrato);
        $Anexo = $model_Anexo->select_anexo_contrato($idContrato);
        $Setor = $model_Setor->select_tipo();        
        
        $view = View::factory('adm/secretaria/visualizar_contrato')
                ->set('idContrato', $idContrato)
                ->set('Contrato', $Contrato)
                ->set('Anexo', $Anexo)
                ->set('Setor', $Setor);

        $this->response->body($view);
    }
    
    public function action_insert_contrato() {
	Funcoes::verifica_login();
        
        $vars = $this->request->post();
        $page = $this->request->post('page');
        $Pesquisa = ($this->request->query('Busca')) ? $this->request->query('Busca') : '';
        $option = '&Busca=' . $Pesquisa;
        
        $dtAssinatura = Funcoes::convertData($this->request->post('dtAssinatura'));
        $dtAssinatura = $dtAssinatura . " " . $vars['horaAssinatura'];
        
        $model_Contrato = new Model_Secretaria_Contrato('default');
        $model_Anexo= new Model_Secretaria_Anexo('default');
                
        $Contrato = $model_Contrato->insert_contrato($vars, $dtAssinatura);
        
        // * Inserindo Anexo *
        $ContratoName = date('Ymd', time()) . Function_Certificados::coderand(8);
        $anexos = $_FILES['anexo'];
        //Funcoes::dump($anexos);
        $img_desc = Funcoes::reArrayFiles($anexos);
        $i = 1;
        foreach ($img_desc as $val) {
            $ext = pathinfo($val['name'], PATHINFO_EXTENSION);
            $newname = $ContratoName . '_Anexo_' . $i . '.' . $ext;
            $envia = Upload::save($val, $newname, 'upload/sep/contrato');
            if ($envia != false) {
                $anexar = $model_Anexo->insert_anexo_contrato($Contrato[0], $newname);
            }
            $i++;
        }
        
        $warning = "Contrato cadastrado com sucesso";
        $dados = $model_Contrato->select_contratos($Pesquisa);
        $paginator = Paginator::factory($dados);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home', 'page', $option);

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($dados);

        $view = View::factory('adm/secretaria/contrato')
                ->set('warning', $warning)
                ->set('Pesquisa', $Pesquisa)
                ->set('dados', $dados)
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total);

        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }
    
    public function action_update_contrato() {
	Funcoes::verifica_login();
        
        $idContrato = $this->request->post('idContrato');
        $vars = $this->request->post();
        $page = $this->request->post('page');
        $Pesquisa = ($this->request->query('Busca')) ? $this->request->query('Busca') : '';
        $option = '&Busca=' . $Pesquisa;
        
        $dtAssinatura = Funcoes::convertData($this->request->post('dtAssinatura'));
        $dtAssinatura = $dtAssinatura . " " . $vars['horaAssinatura'];

        $model_Contrato = new Model_Secretaria_Contrato('default');
        $model_Anexo = new Model_Secretaria_Anexo('default');
                
        $Contrato = $model_Contrato->update_contrato($idContrato, $vars, $dtAssinatura);
        
        // * Inserindo Anexo *
        $ContratoName = date('Ymd', time()) . Function_Certificados::coderand(8);
        $anexos = $_FILES['anexo'];
        $img_desc = Funcoes::reArrayFiles($anexos);
        $i = 1;
        foreach ($img_desc as $val) {
            $ext = pathinfo($val['name'], PATHINFO_EXTENSION);
            $newname = $ContratoName . '_Anexo_' . $i . '.' . $ext;
            $envia = Upload::save($val, $newname, 'upload/sep/contrato');
            if ($envia != false) {
                $anexar = $model_Anexo->insert_anexo_contrato($idContrato, $newname);
            }
            $i++;
        }
        
        $warning = "Contrato atualizado com sucesso";
        $dados = $model_Contrato->select_contratos($Pesquisa);
        $paginator = Paginator::factory($dados);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home', 'page', $option);

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($dados);

        $view = View::factory('adm/secretaria/contrato')
                ->set('warning', $warning)
                ->set('Pesquisa', $Pesquisa)
                ->set('dados', $dados)
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total);

        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }

    public function action_delete_anexo() {
	Funcoes::verifica_login();
        $idAnexo = $this->request->query('idAnexo');
        $idContrato = $this->request->query('idContrato');
        $model_Anexo = new Model_Secretaria_Anexo('default');
        $Anexo = $model_Anexo->select_anexo_contrato($idContrato);  
        foreach ($Anexo as $filename) {
             if ($filename['idAnexo'] == $idAnexo) {
                unlink('upload/sep/contrato/'.$filename['Caminho']);
            }
        }
        $model_Anexo->delete_anexo($idAnexo);
        $this->redirect('dashboard/secretaria_contrato/home');
    }

    public function action_delete_contrato() {
        Funcoes::verifica_login();
        $idContrato = $this->request->post('idContrato');
        $model_Contrato = new Model_Secretaria_Contrato('default');
        $model_Anexo = new Model_Secretaria_Anexo('default');
        $Anexo = $model_Anexo->select_anexo_contrato($idContrato);  
        foreach ($Anexo as $filename) {
           unlink('upload/sep/contrato/'.$filename['Caminho']);
        }
        $del_idAnexo  = $model_Anexo->delete_anexo_contrato($idContrato);
        $del_idContrato  = $model_Contrato->delete_contrato($idContrato);
        return $del_idContrato;
    }
    
    public function action_update_ativar_contrato() {
	Funcoes::verifica_login();
        
        $idContrato = $this->request->query('idContrato');

        $model_contrato = new Model_Secretaria_Contrato('default');
        $model_contrato->update_ativar_contrato($idContrato);
        $contratos = $model_contrato->select_contrato($idContrato);

        if ($contratos[0]['Status'] != "1") {//se retornar algum resultado
            $view = '0';
        } else {
            $view = '1';
        }
        $this->response->body($view);
    }
    
 }
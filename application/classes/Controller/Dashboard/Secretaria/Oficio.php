<?php
defined('SYSPATH') or die('No direct script access.');

class Controller_Dashboard_Secretaria_Oficio extends Controller {

    public function action_index() {
        $this->redirect('dashboard/secretaria_oficio');
    }

    public function action_home() {
	Funcoes::verifica_login();

        $page = $this->request->query('page');
        $Pesquisa = ($this->request->query('Busca')) ? $this->request->query('Busca') : '';
        $option = '&Busca=' . $Pesquisa;

        $model_oficio = new Model_Secretaria_Oficio('default');
        $dados = $model_oficio->select_oficios($Pesquisa);
        $paginator = Paginator::factory($dados);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home', 'page', $option);

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($dados);

        $view = View::factory('adm/secretaria/oficios')
                ->set('Pesquisa', $Pesquisa)
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total);

        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }
    
    public function action_oficio() {
	Funcoes::verifica_login();
        
        $idOficio = $this->request->query('idOficio');
        $page = $this->request->query('page');

        $model_Oficio = new Model_Secretaria_Oficio('default');
        $model_Anexo= new Model_Secretaria_Anexo('default');
        $model_Setor= new Model_Login_Setor('user');   
        
        if ($idOficio == '') {
            $NOficio = $model_Oficio->select_oficios_ano(date('Y'));
        } else {
            $NOficio = '';
        }
                
        $Oficio = $model_Oficio->select_oficio($idOficio);
        $Anexo = $model_Anexo->select_anexo_oficio($idOficio);
        $Setor = $model_Setor->select_tipo();        
        
        $view = View::factory('adm/secretaria/set_oficio')
                ->set('idOficio', $idOficio)
                ->set('NOficio', $NOficio)
                //->set('AnoOficio', $AnoOficio)
                ->set('page', $page)
                ->set('Oficio', $Oficio)
                ->set('Anexo', $Anexo)
                ->set('Setor', $Setor);

        $this->response->body($view);
    }
    
    public function action_visualizar_oficio() {
	Funcoes::verifica_login();
        
        $idOficio = $this->request->query('idOficio');

        $model_Oficio = new Model_Secretaria_Oficio('default');
        //$model_TipoOficio = new Model_Secretaria_Tipodocumentos('default');
        $model_Anexo = new Model_Secretaria_Anexo('default');
        $model_Setor = new Model_Login_Setor('user');   
        
        
        $Oficio = $model_Oficio->select_oficio($idOficio);
        //$TipoDocumento = $model_TipoOficio->select_tipos();
        $Anexo = $model_Anexo->select_anexo_oficio($idOficio);
        $Setor = $model_Setor->select_tipo();        
        
        $view = View::factory('adm/secretaria/visualizar_oficio')
                ->set('idOficio', $idOficio)
                ->set('Oficio', $Oficio)
                //->set('TipoDocumento', $TipoDocumento)
                ->set('Anexo', $Anexo)
                ->set('Setor', $Setor);

        $this->response->body($view);
    }
    
    public function action_insert_oficio() {
	Funcoes::verifica_login();
        
        $vars = $this->request->post();
        $page = $this->request->post('page');
        $Pesquisa = ($this->request->query('Busca')) ? $this->request->query('Busca') : '';
        $option = '&Busca=' . $Pesquisa;
        
        $dtEmissao = Funcoes::convertData($this->request->post('dtEmissao'));
        $Emissao = $dtEmissao . " " . $vars['horaEmissao'];

        $model_Oficio = new Model_Secretaria_Oficio('default');
        $model_Anexo= new Model_Secretaria_Anexo('default');
                
        $Oficio = $model_Oficio->insert_oficio($vars, $Emissao);
        
        // * Inserindo Anexo *
        $OficioName = date('Ymd', time()) . Function_Certificados::coderand(8);
        $anexos = $_FILES['anexo'];
        $img_desc = Funcoes::reArrayFiles($anexos);
        $i = 1;
        foreach ($img_desc as $val) {
            $ext = pathinfo($val['name'], PATHINFO_EXTENSION);
            $newname = $OficioName . '_Anexo_' . $i . '.' . $ext;
            $envia = Upload::save($val, $newname, 'upload/sep/oficio');
            if ($envia != false) {
                $anexar = $model_Anexo->insert_anexo_oficio($Oficio[0], $newname);
            }
            $i++;
        }
        
        $warning = "Oficio inserido com sucesso";
        $dados = $model_Oficio->select_oficios($Pesquisa);
        $paginator = Paginator::factory($dados);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home', 'page', $option);

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($dados);

        $view = View::factory('adm/secretaria/oficios')
                ->set('warning', $warning)
                ->set('Pesquisa', $Pesquisa)
                ->set('dados', $dados)
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total);

        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }
    
    public function action_update_oficio() {
	Funcoes::verifica_login();
        
        $idOficio = $this->request->post('idOficio');
        $vars = $this->request->post();
        $page = $this->request->post('page');
        $Pesquisa = ($this->request->query('Busca')) ? $this->request->query('Busca') : '';
        $option = '&Busca=' . $Pesquisa;
        
        $dtEmissao = Funcoes::convertData($this->request->post('dtEmissao'));
        $Emissao = $dtEmissao . " " . $vars['horaEmissao'];

        $model_Oficio = new Model_Secretaria_Oficio('default');
        $model_Anexo= new Model_Secretaria_Anexo('default');
                
        $Oficio = $model_Oficio->update_oficio($idOficio, $vars, $Emissao);
        
        // * Inserindo Anexo *
        $OficioName = date('Ymd', time()) . Function_Certificados::coderand(8);
        $anexos = $_FILES['anexo'];
        $img_desc = Funcoes::reArrayFiles($anexos);
        $i = 1;
        foreach ($img_desc as $val) {
            $ext = pathinfo($val['name'], PATHINFO_EXTENSION);
            $newname = $OficioName . '_Anexo_' . $i . '.' . $ext;
            $envia = Upload::save($val, $newname, 'upload/sep/oficio');
            if ($envia != false) {
                $anexar = $model_Anexo->insert_anexo_oficio($idOficio, $newname);
            }
            $i++;
        }
        
        $warning = "Oficio atualizado com sucesso";
        $dados = $model_Oficio->select_oficios($Pesquisa);
        $paginator = Paginator::factory($dados);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home', 'page', $option);

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($dados);

        $view = View::factory('adm/secretaria/oficios')
                ->set('warning', $warning)
                ->set('Pesquisa', $Pesquisa)
                ->set('dados', $dados)
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total);

        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }

    public function action_delete_anexo() {
	Funcoes::verifica_login();
        $idAnexo = $this->request->query('idAnexo');
        $idOficio = $this->request->query('idOficio');
        $model_Anexo = new Model_Secretaria_Anexo('default');
        $Anexo = $model_Anexo->select_anexo_oficio($idOficio);  
        foreach ($Anexo as $filename) {
            if ($filename['idAnexo'] == $idAnexo) {
                unlink('upload/sep/oficio/'.$filename['Caminho']);
            }
        }
        $model_Anexo->delete_anexo($idAnexo);
        $this->redirect('dashboard/secretaria_oficio/home');
    }

    public function action_delete_oficio() {
        Funcoes::verifica_login();
        $idOficio = $this->request->post('idOficio');
        $model_Oficio = new Model_Secretaria_Oficio('default');
        $model_Anexo = new Model_Secretaria_Anexo('default');
        $Anexo = $model_Anexo->select_anexo_oficio($idOficio);  
        foreach ($Anexo as $filename) {
           unlink('upload/sep/oficio/'.$filename['Caminho']);
        }
        $del_idAnexo  = $model_Anexo->delete_anexo_oficio($idOficio);
        $del_idOficio  = $model_Oficio->delete_oficio($idOficio);
        return $del_idOficio;
    }
    
    public function action_update_ativar_oficio() {
	Funcoes::verifica_login();
        
        $idOficio = $this->request->query('idOficio');

        $model_oficio = new Model_Secretaria_Oficio('default');
        $model_oficio->update_ativar_oficio($idOficio);
        $oficios = $model_oficio->select_oficio($idOficio);

        if ($oficios[0]['Status'] != "1") {//se retornar algum resultado
            $view = '0';
        } else {
            $view = '1';
        }
        $this->response->body($view);
    }
    
 }
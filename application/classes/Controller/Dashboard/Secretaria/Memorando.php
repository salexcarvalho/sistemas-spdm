<?php
defined('SYSPATH') or die('No direct script access.');

class Controller_Dashboard_Secretaria_Memorando extends Controller {

    public function action_index() {
        $this->redirect('dashboard/secretaria_memorando');
    }

    public function action_home() {
	Funcoes::verifica_login();

        $page = $this->request->query('page');
        $Pesquisa = ($this->request->query('Busca')) ? $this->request->query('Busca') : '';
        $option = '&Busca=' . $Pesquisa;

        $model_memorando = new Model_Secretaria_Memorando('default');
        $dados = $model_memorando->select_memorandos($Pesquisa);
        $paginator = Paginator::factory($dados);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home', 'page', $option);

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($dados);

        $view = View::factory('adm/secretaria/memorandos')
                ->set('Pesquisa', $Pesquisa)
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total);

        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }
    
    public function action_memorando() {
	Funcoes::verifica_login();
        
        $idMemorando = $this->request->query('idMemorando');
        $page = $this->request->query('page');

        $model_Memorando = new Model_Secretaria_Memorando('default');
        $model_Anexo= new Model_Secretaria_Anexo('default');
        $model_Setor= new Model_Login_Setor('user');   
        
        if ($idMemorando == '') {
            $NMemorando = $model_Memorando->select_memorandos_ano(date('Y'));
        } else {
            $NMemorando = '';
        }
                
        $Memorando = $model_Memorando->select_memorando($idMemorando);
        $Anexo = $model_Anexo->select_anexo_memorando($idMemorando);
        $Setor = $model_Setor->select_tipo();        
        
        $view = View::factory('adm/secretaria/set_memorando')
                ->set('idMemorando', $idMemorando)
                ->set('NMemorando', $NMemorando)
                //->set('AnoMemorando', $AnoMemorando)
                ->set('page', $page)
                ->set('Memorando', $Memorando)
                ->set('Anexo', $Anexo)
                ->set('Setor', $Setor);

        $this->response->body($view);
    }
    
    public function action_visualizar_memorando() {
	Funcoes::verifica_login();
        
        $idMemorando = $this->request->query('idMemorando');

        $model_Memorando = new Model_Secretaria_Memorando('default');
        //$model_TipoMemorando = new Model_Secretaria_Tipodocumentos('default');
        $model_Anexo = new Model_Secretaria_Anexo('default');
        $model_Setor = new Model_Login_Setor('user');   
                
        $Memorando = $model_Memorando->select_memorando($idMemorando);
        //$TipoDocumento = $model_TipoMemorando->select_tipos('');
        $Anexo = $model_Anexo->select_anexo_memorando($idMemorando);
        $Setor = $model_Setor->select_tipo();        
        
        $view = View::factory('adm/secretaria/visualizar_memorando')
                ->set('idMemorando', $idMemorando)
                ->set('Memorando', $Memorando)
                //->set('TipoDocumento', $TipoDocumento)
                ->set('Anexo', $Anexo)
                ->set('Setor', $Setor);

        $this->response->body($view);
    }
    
    public function action_insert_memorando() {
	Funcoes::verifica_login();
        
        $vars = $this->request->post();
        $page = $this->request->post('page');
        $Pesquisa = ($this->request->query('Busca')) ? $this->request->query('Busca') : '';
        $option = '&Busca=' . $Pesquisa;
        
        $dtEmissao = Funcoes::convertData($this->request->post('dtEmissao'));
        $Emissao = $dtEmissao . " " . $vars['horaEmissao'];

        $model_Memorando = new Model_Secretaria_Memorando('default');
        $model_Anexo= new Model_Secretaria_Anexo('default');
                
        $Memorando = $model_Memorando->insert_memorando($vars, $Emissao);
        
        // * Inserindo Anexo *
        $MemorandoName = date('Ymd', time()) . Function_Certificados::coderand(8);
        $anexos = $_FILES['anexo'];
        $img_desc = Funcoes::reArrayFiles($anexos);
        $i = 1;
        foreach ($img_desc as $val) {
            $ext = pathinfo($val['name'], PATHINFO_EXTENSION);
            $newname = $MemorandoName . '_Anexo_' . $i . '.' . $ext;
            $envia = Upload::save($val, $newname, 'upload/sep/memorando');
            if ($envia != false) {
                $anexar = $model_Anexo->insert_anexo_memorando($Memorando[0], $newname);
            }
            $i++;
        }
        
        $warning = "Memorando inserido com sucesso";
        $dados = $model_Memorando->select_memorandos($Pesquisa);
        $paginator = Paginator::factory($dados);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home', 'page', $option);

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($dados);

        $view = View::factory('adm/secretaria/memorandos')
                ->set('warning', $warning)
                ->set('Pesquisa', $Pesquisa)
                ->set('dados', $dados)
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total);

        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }
    
    public function action_update_memorando() {
	Funcoes::verifica_login();
        
        $idMemorando = $this->request->post('idMemorando');
        $vars = $this->request->post();
        $page = $this->request->post('page');
        $Pesquisa = ($this->request->query('Busca')) ? $this->request->query('Busca') : '';
        $option = '&Busca=' . $Pesquisa;
        
        $dtEmissao = Funcoes::convertData($this->request->post('dtEmissao'));
        $Emissao = $dtEmissao . " " . $vars['horaEmissao'];

        $model_Memorando = new Model_Secretaria_Memorando('default');
        $model_Anexo= new Model_Secretaria_Anexo('default');
                
        $Memorando = $model_Memorando->update_memorando($idMemorando, $vars, $Emissao);
        
        // * Inserindo Anexo *
        $MemorandoName = date('Ymd', time()) . Function_Certificados::coderand(8);
        $anexos = $_FILES['anexo'];
        $img_desc = Funcoes::reArrayFiles($anexos);
        $i = 1;
        foreach ($img_desc as $val) {
            $ext = pathinfo($val['name'], PATHINFO_EXTENSION);
            $newname = $MemorandoName . '_Anexo_' . $i . '.' . $ext;
            $envia = Upload::save($val, $newname, 'upload/sep/memorando');
            if ($envia != false) {
                $anexar = $model_Anexo->insert_anexo_memorando($idMemorando, $newname);
            }
            $i++;
        }
        
        $warning = "Memorando atualizado com sucesso";
        $dados = $model_Memorando->select_memorandos($Pesquisa);
        $paginator = Paginator::factory($dados);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home', 'page', $option);

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($dados);

        $view = View::factory('adm/secretaria/memorandos')
                ->set('warning', $warning)
                ->set('Pesquisa', $Pesquisa)
                ->set('dados', $dados)
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total);

        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }

    public function action_delete_anexo() {
	Funcoes::verifica_login();
        $idAnexo = $this->request->query('idAnexo');
        $idMemorando = $this->request->query('idMemorando');
        $model_Anexo = new Model_Secretaria_Anexo('default');
        $Anexo = $model_Anexo->select_anexo_memorando($idMemorando);  
        foreach ($Anexo as $filename) {
            if ($filename['idAnexo'] == $idAnexo) {
                unlink('upload/sep/memorando/'.$filename['Caminho']);
            }
        }
        $model_Anexo->delete_anexo($idAnexo);
        $this->redirect('dashboard/secretaria_memorando/home');
    }

    public function action_delete_memorando() {
        Funcoes::verifica_login();
        $idMemorando = $this->request->post('idMemorando');
        $model_Memorando = new Model_Secretaria_Memorando('default');
        $model_Anexo = new Model_Secretaria_Anexo('default');
        $Anexo = $model_Anexo->select_anexo_memorando($idMemorando);  
        foreach ($Anexo as $filename) {
           unlink('upload/sep/memorando/'.$filename['Caminho']);
        }
        $del_idAnexo  = $model_Anexo->delete_anexo_memorando($idMemorando);
        $del_idMemorando  = $model_Memorando->delete_memorando($idMemorando);
        return $del_idMemorando;
    }
    
    public function action_update_ativar_memorando() {
	Funcoes::verifica_login();
        
        $idMemorando = $this->request->query('idMemorando');

        $model_memorando = new Model_Secretaria_Memorando('default');
        $model_memorando->update_ativar_memorando($idMemorando);
        $memorandos = $model_memorando->select_memorando($idMemorando);

        if ($memorandos[0]['Status'] != "1") {//se retornar algum resultado
            $view = '0';
        } else {
            $view = '1';
        }
        $this->response->body($view);
    }
    
 }
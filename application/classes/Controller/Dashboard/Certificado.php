<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Dashboard_Certificado extends Controller {

    public function action_index() {
        $this->redirect('certificado/home');
    }

    public function action_home() {
        /* $model_servico = new Model_Servico('default');
          $servicos = $model_servico->select_servicos_mais_procurados(10); */
        
        $view = View::factory('certificado/home')
        /* ->set('servicos', $servicos) */
        ;
        $this->response->body($view);
    }

    public function action_gerapdf() {
        $autenticacao = $this->request->query('autenticacao');
        $idApoio = $this->request->query('idApoio');
        $idCertificado = $this->request->query('idCertificado');
        $model_apoio = new Model_Sec_Apoio('default');
        $model_certificado = new Model_Sec_Certificado('default');
        $model_participante = new Model_Sec_Participantes('default');
        $model_tipo = new Model_Sec_Tipos('default');
        $model_assinatura = new Model_Sec_Assinatura('default');
        $model_tipoDoc = new Model_Sec_TipoDoc('default');
        $verificar = $model_certificado->verificar_status($autenticacao);

        if ($verificar[0]['status'] == 0):
            $autenticacao = "";
        endif;

        if (isset($idApoio)):
            $apoio = $model_apoio->select_apoio_ceritificado($idApoio, $autenticacao);
        else:
            $apoio = $model_apoio->select_apoio_ceritificado_auth($autenticacao);
        endif;

        if (isset($idCertificado)):
            $certificado = $model_certificado->select_certificado($idCertificado);
        else:
            $certificado = $model_certificado->select_certificado($apoio[0]['idCertificado']);
        endif;

        $participante = $model_participante->select_participante($apoio[0]['idParticipante']);
        $tipo = $model_tipo->select_tipo($apoio[0]['idTipo']);        
        $dados01 = $model_assinatura->select_assinatura($certificado[0]['fk_idAssinatura_1']);
        $dados02 = $model_assinatura->select_assinatura($certificado[0]['fk_idAssinatura_2']);
        $dados03 = $model_assinatura->select_assinatura($certificado[0]['fk_idAssinatura_3']);
        $dados04 = $model_assinatura->select_assinatura($certificado[0]['fk_idAssinatura_4']);
        $doc = $model_tipoDoc->select_tipoDoc($participante[0]['TipoDoc']);
        $config = array(
            'author' => 'TI Presidência da SPDM',
            'title' => 'Certificado SPDM',
            'name' => 'Certificado_' . $participante[0]["titulacao"] . "_" . $participante[0]["Nome"] . '_' . $tipo[0]["Nome"] . '_N_' . $apoio[0]["idApoio"] . '.pdf', // name file pdf
            'tpage' => 'L',
            'dest' => 'I',
        );
      
       $view = Pdf::factory('certificado/geracertificado', $config)
       // $view = View::factory('certificado/geracertificado')
                ->set('apoio', $apoio)
                ->set('certificado', $certificado)
                ->set('participante', $participante)
                ->set('doc', $doc[0]['Alias'])
                ->set('tipo', $tipo)
                ->set('dados01', $dados01) 
                ->set('dados02', $dados02)     
                ->set('dados03', $dados03)     
                ->set('dados04', $dados04);     
                             
        $view->render();
      //  $this->response->body($view);
    }

    public function action_baixapdf() {
        $autenticacao = $this->request->query('autenticacao');

        // Retira em 30 dias - 02/10/2014
        if ($autenticacao == "") {
            $autenticacao = ($this->request->post('autenticao')) ? $this->request->post('autenticao') : str_replace(' ', '', $this->request->query('autenticao'));
        }

        $model_apoio = new Model_Sec_Apoio('default');
        $model_certificado = new Model_Sec_Certificado('default');
        $model_participante = new Model_Sec_Participantes('default');
        $model_tipo = new Model_Sec_Tipos('default');
        $model_assinatura = new Model_Sec_Assinatura('default');
        $model_tipoDoc = new Model_Sec_TipoDoc('default');
        
        $apoio = $model_apoio->select_apoio_ceritificado_auth($autenticacao);
        $participante = $model_participante->select_participante($apoio[0]['idParticipante']);
        $certificado = $model_certificado->select_certificado($apoio[0]['idCertificado']);
        $tipo = $model_tipo->select_tipo($apoio[0]['idTipo']);
        $dados01 = $model_assinatura->select_assinatura($certificado[0]['fk_idAssinatura_1']);
        $dados02 = $model_assinatura->select_assinatura($certificado[0]['fk_idAssinatura_2']);
        $dados03 = $model_assinatura->select_assinatura($certificado[0]['fk_idAssinatura_3']);
        $dados04 = $model_assinatura->select_assinatura($certificado[0]['fk_idAssinatura_4']);
        $doc = $model_tipoDoc->select_tipoDoc($participante[0]['TipoDoc']);
        /* echo "<pre>";
          var_dump($dados02); echo "</pre>"; exit; */

        $idCertificado = $apoio[0]['idCertificado'];
        $model_certificado->update_imprime_certificado($idCertificado);

        $config = array(
            'author' => 'TI Presidência da SPDM',
            'title' => 'Certificado SPDM',
            'name' => 'Certificado_' . $participante[0]["titulacao"] . "_" . $participante[0]["Nome"] . '_' . $tipo[0]["Nome"] . '_N_' . $apoio[0]["idApoio"] . '.pdf', // name file pdf
            'tpage' => 'L',
            'dest' => 'D',
        );

        $view = Pdf::factory('certificado/geracertificado', $config)
                // $view = View::factory('certificado/geracertificado')
                ->set('apoio', $apoio)
                ->set('certificado', $certificado)
                ->set('participante', $participante)
                ->set('doc', $doc[0]['Alias'])
                ->set('tipo', $tipo)
                ->set('dados01', $dados01)
                ->set('dados02', $dados02)
                ->set('dados03', $dados03)
                ->set('dados04', $dados04)
                //    ;
                ->render();
        //$this->response->body($view);
    }

    public function action_baixarLote() {
        $idCertificado = $this->request->query('idCertificado');

        $model_apoio = new Model_Sec_Apoio('default');
        $model_certificado = new Model_Sec_Certificado('default');
        $model_participante = new Model_Sec_Participantes('default');
        $model_tipo = new Model_Sec_Tipos('default');
        $model_assinatura = new Model_Sec_Assinatura('default');
        $model_tipoDoc = new Model_Sec_TipoDoc('default');
        
        $nome = $model_certificado->select_certificado($idCertificado);
        $ap = $model_apoio->select_apoio_ceritificado_lote($idCertificado);
        
        foreach ($ap as $lote):
            $autenticacao = $lote['Autenticacao'];
            $base = $model_apoio->select_apoio_ceritificado_auth($autenticacao);
            $apoio[] = $model_apoio->select_apoio_ceritificado_auth($autenticacao);
            $participante[] = $model_participante->select_participante($base[0]['idParticipante']);
            $certificado = $model_certificado->select_certificado($idCertificado);
            $tipo[] = $model_tipo->select_tipo($base[0]['idTipo']);
            $dados01 = $model_assinatura->select_assinatura($certificado[0]['fk_idAssinatura_1']);
            $dados02 = $model_assinatura->select_assinatura($certificado[0]['fk_idAssinatura_2']);
            $dados03 = $model_assinatura->select_assinatura($certificado[0]['fk_idAssinatura_3']);
            $dados04 = $model_assinatura->select_assinatura($certificado[0]['fk_idAssinatura_4']);
            $doc[] = $model_tipoDoc->select_tipoDoc($lote['idParticipante']);
            $model_certificado->update_imprime_certificado($idCertificado);
        endforeach;

        $config = array(
            'author' => 'TI Presidência da SPDM',
            'title' => 'Certificado SPDM',
            'name' => 'Certificado_' . $nome[0]['eventoNome'] . '.pdf', // name file pdf
            'tpage' => 'L',
            'dest' => 'D',
        );
        
        //$view = View::factory('certificado/geracertificadoLote')
        Pdf::factory('certificado/geracertificadoLote', $config)
                ->set('base', $base)
                ->set('apoio', $apoio)
                ->set('certificado', $certificado)
                ->set('participante', $participante)
                ->set('tipo', $tipo)
                ->set('doc', $doc)
                ->set('dados01', $dados01)
                ->set('dados02', $dados02)
                ->set('dados03', $dados03)
                ->set('dados04', $dados04)//;
                ->render();
        //$this->response->body($view);
    }

}

// End Home

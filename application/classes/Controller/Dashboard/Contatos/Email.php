<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Dashboard_Contatos_Email extends Controller {

    public function action_index() {
        $this->redirect('dashboard_adm/home');
    }

    public function action_home() {
	Funcoes::verifica_login();

        $page = $this->request->query('page');

        $model_tipos = new Model_Contatos_Email('default');
        $tipos = $model_tipos->select_tipo();

        $paginator = Paginator::factory($tipos);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home');

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($tipos);

        $view = View::factory('adm/contatos/tipos_email')
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }
   
    
    public function action_cadastro_tipo() {
	Funcoes::verifica_login();
        $view = View::factory('adm/contatos/cadastro_tipo_email');
        $this->response->body($view);
    }
    
    
    public function action_insert_tipo() {
	Funcoes::verifica_login();
        
        Session::instance();
        $Descricao = $this->request->post('Descricao');
        $status = $this->request->post('status');

        $model_tipos = new Model_Contatos_Email('default');
        $model_tipos->insert_tipo($Descricao, $status);

        /* Mostra a resposta da ação */
        $warning = "Tipo de email cadastrado com sucesso!";
        $page = $this->request->query('page');
        $tipos = $model_tipos->select_tipo();
        $paginator = Paginator::factory($tipos);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home');

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($tipos);

        $view = View::factory('adm/contatos/tipos_email')
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
                ->set('warning', $warning)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }
    
    
    public function action_editar_tipo() {
	Funcoes::verifica_login();

        $page = $this->request->query('page');
        $idTipoEmail = $this->request->query('idTipoEmail');

        $model_tipos = new Model_Contatos_Email('default');
        $tipos = $model_tipos->select_tipo_id($idTipoEmail);

        $view = View::factory('adm/contatos/editar_tipo_email')
                ->set('tipos', $tipos)
                ->set('page', $page)
        ;
        $this->response->body($view);
    }

    
    public function action_update_tipo() {
        Session::instance();
        $idTipoEmail = $this->request->post('idTipoEmail');
        $Descricao = $this->request->post('Descricao');
        $Status = $this->request->post('status');

        $model_tipos = new Model_Contatos_Email('default');
        $model_tipos->update_tipo($idTipoEmail, $Descricao, $Status);

        //$this->redirect('dashboard_sec_tiposparticipantes/home');
        /* Mostra a resposta da ação */

        $warning = "Tipo de email atualizado com sucesso!";
        $page = $this->request->post('page');
        $tipos = $model_tipos->select_tipo();
        $paginator = Paginator::factory($tipos);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home');

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($tipos);

        $view = View::factory('adm/contatos/tipos_email')
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
                ->set('warning', $warning)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }

    
    public function action_delete_tipo() {
	Funcoes::verifica_login();

        $idTipoEmail = $this->request->post('idTipoEmail');

        $model_tipos = new Model_Contatos_Email('default');
        $model_tipos->delete_tipo($idTipoEmail);

        $this->redirect('dashboard_contatos_email/home');
    }
    
    public function action_update_ativar_tipo() {
	Funcoes::verifica_login();
        
        $idTipoEmail = $this->request->query('idTipoEmail');

        $model_tipos = new Model_Contatos_Email('default');
        $model_tipos->update_ativar_tipo($idTipoEmail);
        $tipos = $model_tipos->select_tipo($idTipoEmail);

        if ($tipos[0]['Status'] != "1") {//se retornar algum resultado
            $view = '0';
        } else {
            $view = '1';
        }
        $this->response->body($view);
    }

}

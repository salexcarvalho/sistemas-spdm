<?php 

defined('SYSPATH') or die('No direct script access.');

class Controller_Dashboard_Contatos_Home extends Controller {

    public function action_index() {
        $this->redirect('dashboard_adm/home');
    }

    public function action_home() {
	Funcoes::verifica_login();

        $page = $this->request->query('page');
        $Pesquisa = ($this->request->query('Busca')) ? $this->request->query('Busca') : '';

        $model_contatos = new Model_Contatos_Home('default');
        $contatos = $model_contatos->select_contato($Pesquisa);
        
        $model_emails = new Model_Contatos_Email('default');
        $model_tels = new Model_Contatos_Telefone('default');
        
        $emails = $model_emails->select_tipo();
        $tels = $model_tels->select_tipo();

        $paginator = Paginator::factory($contatos);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home');

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($contatos);

        $view = View::factory('adm/contatos/home')
                ->set('emails', $emails)
                ->set('tels', $tels)
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }
   
    public function action_cadastro_contato() {
	Funcoes::verifica_login();
        Session::instance();
        $model_tratamento = new Model_Contatos_Tratamento('default');
        $model_setores = new Model_Login_Setor('user');
        $model_grupos = new Model_Contatos_Grupo('default');
        $model_emails = new Model_Contatos_Email('default');
        $model_tels = new Model_Contatos_Telefone('default');
        
        $tratamentos = $model_tratamento->select_tipo();
        $setores = $model_setores->select_tipo();
        $grupos = $model_grupos->select_tipo();
        $emails = $model_emails->select_tipo();
        $tels = $model_tels->select_tipo();
        
        $view = View::factory('adm/contatos/cadastro_contato')
                ->set('tratamentos', $tratamentos)
                ->set('setores', $setores)
                ->set('grupos', $grupos)
                ->set('emails', $emails)
                ->set('tels', $tels)
                ;
        $this->response->body($view);
    }
    
    public function action_insert_contato() {
	Funcoes::verifica_login();
        
        $vars = $this->request->post();
        $Pesquisa = '';
        $model_contatos = new Model_Contatos_Home('default');
        $model_emails = new Model_Contatos_Email('default');
        $model_tels = new Model_Contatos_Telefone('default');
        $model_enderecos = new Model_Contatos_Endereco('default');
        
        $model_contatos->insert_contato($vars);
        $contato = $model_contatos->select_contato($Pesquisa);
        $idContato = $contato[0]['idContato'];
        
        /*if (!empty($vars['Email_Add'])) {
        $model_emails->insert_emails_adicionais($vars, $idContato);
        }
        if (!empty($vars['idTipoTel'])) {
        $model_tels->insert_telefones_adicionais($vars, $idContato);
        }
        if (!empty($vars['Endereco_Logradouro'])) {
        $model_enderecos->insert_enderecos_adicionais($vars, $idContato);
        }*/
        
        $emails = $model_emails->select_tipo();
        $tels = $model_tels->select_tipo();
        
        /* Mostra a resposta da ação */
        $warning = "Contato cadastrado com sucesso!";
        $page = $this->request->query('page');
        $contatos = $model_contatos->select_contato($Pesquisa);
        $paginator = Paginator::factory($contatos);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home');

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($contatos);

        $view = View::factory('adm/contatos/home')
                ->set('emails', $emails)
                ->set('tels', $tels)
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
                ->set('warning', $warning)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }
    
    public function action_visualiza_contato() {
	Funcoes::verifica_login();

        $idContato = $this->request->post('idContato');
        
        $model_contatos = new Model_Contatos_Home('default');
        $model_tratamento = new Model_Contatos_Tratamento('default');
        $model_setores = new Model_Login_Setor('user');
        $model_grupos = new Model_Contatos_Grupo('default');
        $model_emails = new Model_Contatos_Email('default');
        $model_tels = new Model_Contatos_Telefone('default');
        $model_enderecos = new Model_Contatos_Endereco('default');
        
        $tratamentos = $model_tratamento->select_tipo();
        $setores = $model_setores->select_tipo();
        $grupos = $model_grupos->select_tipo();
        $emails = $model_emails->select_tipo();
        $tels = $model_tels->select_tipo();
        
        $contato = $model_contatos->select_contato_id($idContato);
        //Funcoes::dump($contato);
        $emailscontatos = $model_emails->select_email_id($idContato);
        $telcontatos = $model_tels->select_telefone_id($idContato);
        $enderecos = $model_enderecos->select_endereco_id($idContato);

        $view = View::factory('adm/contatos/visualiza_contato')
                ->set('tratamentos', $tratamentos)
                ->set('setores', $setores)
                ->set('grupos', $grupos)
                ->set('emails', $emails)
                ->set('tels', $tels)
                ->set('contato', $contato)
                ->set('emailscontatos', $emailscontatos)
                ->set('telcontatos', $telcontatos)
                ->set('enderecos', $enderecos)
        ;

        $this->response->body($view);
    }
    
    public function action_editar_contato() {
	Funcoes::verifica_login();
        Session::instance();
        $page = $this->request->post('page');
        $idContato = $this->request->post('idContato');
        
        $model_contatos = new Model_Contatos_Home('default');
        $model_tratamento = new Model_Contatos_Tratamento('default');
        $model_setores = new Model_Login_Setor('user');
        $model_grupos = new Model_Contatos_Grupo('default');
        $model_emails = new Model_Contatos_Email('default');
        $model_tels = new Model_Contatos_Telefone('default');
        $model_enderecos = new Model_Contatos_Endereco('default');
        
        $tratamentos = $model_tratamento->select_tipo();
        $setores = $model_setores->select_tipo();
        $grupos = $model_grupos->select_tipo();
        $emails = $model_emails->select_tipo();
        $tels = $model_tels->select_tipo();
        
        $contato = $model_contatos->select_contato_id($idContato);
        $emailscontatos = $model_emails->select_email_id($idContato);
        $telcontatos = $model_tels->select_telefone_id($idContato);
        $enderecos = $model_enderecos->select_endereco_id($idContato);

        $view = View::factory('adm/contatos/editar_contato')
                ->set('tratamentos', $tratamentos)
                ->set('setores', $setores)
                ->set('grupos', $grupos)
                ->set('emails', $emails)
                ->set('tels', $tels)
                ->set('contato', $contato)
                ->set('emailscontatos', $emailscontatos)
                ->set('telcontatos', $telcontatos)
                ->set('enderecos', $enderecos)
                ->set('page', $page)
        ;

        $this->response->body($view);
    }

    public function action_update_contato() {
	Funcoes::verifica_login();
        
        Session::instance();
        $vars = $this->request->post();
                        
        $model_contatos = new Model_Contatos_Home('default');
        $model_emails = new Model_Contatos_Email('default');
        $model_tels = new Model_Contatos_Telefone('default');
        $model_enderecos = new Model_Contatos_Endereco('default');
        
        $model_contatos->update_contato($vars);
        (isset($vars['Email_Add'])) ? $vars['Email_Add'] : $vars['Email_Add'] = '';
        if ($vars['Email_Add'] !== "") {  
            $model_emails->update_emails($vars);
        }
        (isset($vars['idTipoTel'])) ? $vars['idTipoTel'] : $vars['idTipoTel'] = '';
        if ($vars['idTipoTel'] !== "") {
            $model_tels->update_telefones($vars);
        }
        (isset($vars['Endereco_Logradouro'])) ? $vars['Endereco_Logradouro'] : $vars['Endereco_Logradouro'] = '';
        if ($vars['Endereco_Logradouro'] !== "") {
            $model_enderecos->update_endereco($vars);
        }
        
        $emails = $model_emails->select_tipo();
        $tels = $model_tels->select_tipo();

        $warning = "Contato atualizado com sucesso!";
        $page = $this->request->post('page');
        $Pesquisa = ($this->request->query('Busca')) ? $this->request->query('Busca') : '';
        $contatos = $model_contatos->select_contato($Pesquisa);
        $paginator = Paginator::factory($contatos);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home');

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($contatos);

        $view = View::factory('adm/contatos/home')
                ->set('emails', $emails)
                ->set('tels', $tels)
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
                ->set('warning', $warning)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
        //$this->redirect('dashboard_contatos_home/home');
    }

    public function action_delete_contato() {
	Funcoes::verifica_login();

        $idContato = $this->request->post('idContato');

        $model_contatos = new Model_Contatos_Home('default');
        $model_email = new Model_Contatos_Email('default');
        $model_telefone = new Model_Contatos_Telefone('default');
        $model_endereco = new Model_Contatos_Endereco('default');
        
        $model_email->delete_email($idContato);
        $model_telefone->delete_telefone($idContato);
        $model_endereco->delete_endereco($idContato);
        $model_contatos->delete_contato($idContato);

        $this->redirect('dashboard_contatos_home/home');
    }
 
    public function action_update_ativar_contato() {
	Funcoes::verifica_login();
        
        $idContato = $this->request->query('idContato');

        $model_contatos = new Model_Contatos_Home('default');
        $model_email = new Model_Contatos_Email('default');
        $model_telefone = new Model_Contatos_Telefone('default');
        $model_endereco = new Model_Contatos_Endereco('default');
        
        $model_contatos->update_ativar_contato($idContato);
        $model_email->update_ativar_email($idContato);
        $model_telefone->update_ativar_telefone($idContato);
        $model_endereco->update_ativar_endereco($idContato);
        
        $contatos = $model_contatos->select_contato_id($idContato);

        if ($contatos[0]['Status'] != "1") {//se retornar algum resultado
            $view = '0';
        } else {
            $view = '1';
        }
        $this->response->body($view);
    }
    
    public function action_insert_emails() {
	Funcoes::verifica_login();
        
        Session::instance();
        $vars = $this->request->post();
        
        $model_contatos = new Model_Contatos_Home('default');
        $model_emails = new Model_Contatos_Email('default');
        $model_tels = new Model_Contatos_Telefone('default');
        $model_emails = new Model_Contatos_Email('default');
        
        $idContato = $this->request->post('idContatoEmail');        
        $model_emails->insert_emails_adicionais($vars, $idContato);
        
        $emails = $model_emails->select_tipo();
        $tels = $model_tels->select_tipo();
        
        /* Mostra a resposta da ação */
        $warning = "Emails cadastrados com sucesso!";
        $page = $this->request->query('page');
        $Pesquisa = ($this->request->query('Busca')) ? $this->request->query('Busca') : '';
        $contatos = $model_contatos->select_contato($Pesquisa);
        $paginator = Paginator::factory($contatos);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home');

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($contatos);

        $view = View::factory('adm/contatos/home')
                ->set('emails', $emails)
                ->set('tels', $tels)
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
                ->set('warning', $warning)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }
    
    public function action_insert_telefones() {
	Funcoes::verifica_login();
        
        Session::instance();
        $vars = $this->request->post();
        
        $model_contatos = new Model_Contatos_Home('default');
        $model_emails = new Model_Contatos_Email('default');
        $model_tels = new Model_Contatos_Telefone('default');
        
        $idContato = $this->request->post('idContatoTelefone');        
        $model_tels->insert_telefones_adicionais($vars, $idContato);
        
        $emails = $model_emails->select_tipo();
        $tels = $model_tels->select_tipo();
        
        /* Mostra a resposta da ação */
        $warning = "Telefones cadastrados com sucesso!";
        $Pesquisa = ($this->request->query('Busca')) ? $this->request->query('Busca') : '';
        $contatos = $model_contatos->select_contato($Pesquisa);
        $paginator = Paginator::factory($contatos);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home');

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($contatos);

        $view = View::factory('adm/contatos/home')
                ->set('emails', $emails)
                ->set('tels', $tels)
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
                ->set('warning', $warning)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }
    
    public function action_insert_enderecos() {
	Funcoes::verifica_login();
        
        Session::instance();
        $vars = $this->request->post();
        
        $model_contatos = new Model_Contatos_Home('default');
        $model_emails = new Model_Contatos_Email('default');
        $model_tels = new Model_Contatos_Telefone('default');
        $model_enderecos = new Model_Contatos_Endereco('default');
        
        $idContato = $this->request->post('idContatoEndereco');        
        $model_enderecos->insert_enderecos_adicionais($vars, $idContato);
        
        $emails = $model_emails->select_tipo();
        $tels = $model_tels->select_tipo();
        
        /* Mostra a resposta da ação */
        $warning = "Emails cadastrados com sucesso!";
        $page = $this->request->query('page');
        $Pesquisa = ($this->request->query('Busca')) ? $this->request->query('Busca') : '';
        $contatos = $model_contatos->select_contato($Pesquisa);
        $paginator = Paginator::factory($contatos);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home');

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($contatos);

        $view = View::factory('adm/contatos/home')
                ->set('emails', $emails)
                ->set('tels', $tels)
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
                ->set('warning', $warning)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }
    
    public function action_delete_email() {
	Funcoes::verifica_login();

        $idEmail = $this->request->query('idEmail');

        $model_tipos = new Model_Contatos_Email('default');
        $model_tipos->delete_email_individual($idEmail);
        
        $view = '1';
        $this->response->body($view);
    }
    
    public function action_delete_telefone() {
	Funcoes::verifica_login();

        $idTelefone = $this->request->query('idTelefone');

        $model_tipos = new Model_Contatos_Telefone('default');
        $model_tipos->delete_telefone_individual($idTelefone);
        
        $view = '1';
        $this->response->body($view);
    }
    
    public function action_delete_endereco() {
	Funcoes::verifica_login();

        $idEndereco = $this->request->query('idEndereco');

        $model_tipos = new Model_Contatos_Endereco('default');
        $model_tipos->delete_endereco_individual($idEndereco);
        
        $view = '1';
        $this->response->body($view);
    }

}

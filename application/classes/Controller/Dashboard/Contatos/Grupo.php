<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Dashboard_Contatos_Grupo extends Controller {

    public function action_index() {
        $this->redirect('dashboard_adm/home');
    }

    public function action_home() {
	Funcoes::verifica_login();

        $page = $this->request->query('page');

        $model_tipos = new Model_Contatos_Grupo('default');
        $tipos = $model_tipos->select_tipo();

        $paginator = Paginator::factory($tipos);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home');

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($tipos);

        $view = View::factory('adm/contatos/tipos_grupo')
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }
   
    
    public function action_cadastro_tipo() {
	Funcoes::verifica_login();
        $view = View::factory('adm/contatos/cadastro_tipo_grupo');
        $this->response->body($view);
    }
    
    
    public function action_insert_tipo() {
	Funcoes::verifica_login();
        
        Session::instance();
        $Grupo = $this->request->post('Grupo');
        $Descricao = $this->request->post('Descricao');
        $status = $this->request->post('status');

        $model_tipos = new Model_Contatos_Grupo('default');
        $model_tipos->insert_tipo($Grupo, $Descricao, $status);

        /* Mostra a resposta da ação */
        $warning = "Tipo de grupo cadastrado com sucesso!";
        $page = $this->request->query('page');
        $tipos = $model_tipos->select_tipo();
        $paginator = Paginator::factory($tipos);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home');

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($tipos);

        $view = View::factory('adm/contatos/tipos_grupo')
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
                ->set('warning', $warning)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }
    
    
    public function action_editar_tipo() {
	Funcoes::verifica_login();

        $page = $this->request->query('page');
        $idTipoGrupo = $this->request->query('idTipoGrupo');

        $model_tipos = new Model_Contatos_Grupo('default');
        $tipos = $model_tipos->select_tipo_id($idTipoGrupo);

        $view = View::factory('adm/contatos/editar_tipo_grupo')
                ->set('tipos', $tipos)
                ->set('page', $page)
        ;

        $this->response->body($view);
    }

    
    public function action_update_tipo() {
        Session::instance();
        $idTipoGrupo = $this->request->post('idTipoGrupo');
        $Grupo = $this->request->post('Grupo');
        $Descricao = $this->request->post('Descricao');
        $Status = $this->request->post('status');

        $model_tipos = new Model_Contatos_Grupo('default');
        $model_tipos->update_tipo($idTipoGrupo, $Grupo, $Descricao, $Status);

        //$this->redirect('dashboard_sec_tiposparticipantes/home');
        /* Mostra a resposta da ação */

        $warning = "Tipo de grupo atualizado com sucesso!";
        $page = $this->request->post('page');
        $tipos = $model_tipos->select_tipo();
        $paginator = Paginator::factory($tipos);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home');

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($tipos);

        $view = View::factory('adm/contatos/tipos_grupo')
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
                ->set('warning', $warning)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }

    
    public function action_delete_tipo() {
	Funcoes::verifica_login();

        $idTipoGrupo = $this->request->post('idTipoGrupo');

        $model_tipos = new Model_Contatos_Grupo('default');
        $model_tipos->delete_tipo($idTipoGrupo);

        $this->redirect('dashboard_contatos_grupo/home');
    }

    
    public function action_update_ativar_tipo() {
	Funcoes::verifica_login();
        
        $idTipoGrupo = $this->request->query('idTipoGrupo');

        $model_tipos = new Model_Contatos_Grupo('default');
        $model_tipos->update_ativar_tipo($idTipoGrupo);
        $tipos = $model_tipos->select_tipo($idTipoGrupo);

        if ($tipos[0]['Status'] != "1") {//se retornar algum resultado
            $view = '0';
        } else {
            $view = '1';
        }
        $this->response->body($view);
    }

}

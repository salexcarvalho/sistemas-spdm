<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Dashboard_Integracoes_Home extends Controller {

    public function action_home() {
	Funcoes::verifica_login();

        $page = $this->request->query('page');

        $model_integracoes = new Model_Integracoes_Consulta('joomla');
        $integracoes = $model_integracoes->select_videos_suasaude();

        $paginator = Paginator::factory($integracoes);
        $paginator->set_current_page_number($page);

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($integracoes);

        $view = View::factory('adm/integracoes/home')
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();
        $this->response->body($view);
    }
    
    public function action_boaspraticas() {
	Funcoes::verifica_login();

        $page = $this->request->query('page');

        $model_integracoes = new Model_Integracoes_Consulta('joomla');
        $integracoes = $model_integracoes->select_videos_boaspraticas();

        $paginator = Paginator::factory($integracoes);
        $paginator->set_current_page_number($page);

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($integracoes);

        $view = View::factory('adm/integracoes/boas_praticas')
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();
        $this->response->body($view);
    }
   
    public function action_dicacultural() {
	Funcoes::verifica_login();

        $page = $this->request->query('page');

        $model_integracoes = new Model_Integracoes_Consulta('joomla');
        $integracoes = $model_integracoes->select_videos_dica();

        $paginator = Paginator::factory($integracoes);
        $paginator->set_current_page_number($page);

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($integracoes);

        $view = View::factory('adm/integracoes/dica_cultural')
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();
        $this->response->body($view);
    }
    
    public function action_memoriaspdm() {
	Funcoes::verifica_login();

        $page = $this->request->query('page');

        $model_integracoes = new Model_Integracoes_Consulta('joomla');
        $integracoes = $model_integracoes->select_videos_memoria();

        $paginator = Paginator::factory($integracoes);
        $paginator->set_current_page_number($page);

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($integracoes);

        $view = View::factory('adm/integracoes/memoria_spdm')
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();
        $this->response->body($view);
    }

    public function action_galeria() {
        $page = $this->request->query('page');

        $model_integracoes = new Model_Integracoes_Consulta('joomla');
        $integracoes = $model_integracoes->select_videos_suasaude();

        $paginator = Paginator::factory($integracoes);
        $paginator->set_current_page_number($page);
        $paginator->set_item_count_per_page(9);
        $paginator->set_default_page_range(5);

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($integracoes);

        $view = View::factory('adm/integracoes/integracao')
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();
        $this->response->body($view);
    }
    
    public function action_galeria_memoria() {
        $page = $this->request->query('page');

        $model_integracoes = new Model_Integracoes_Consulta('joomla');
        $integracoes = $model_integracoes->select_videos_memoria();

        $paginator = Paginator::factory($integracoes);
        $paginator->set_current_page_number($page);
        $paginator->set_item_count_per_page(9);
        $paginator->set_default_page_range(5);

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($integracoes);

        $view = View::factory('adm/integracoes/integracao_memoria')
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();
        $this->response->body($view);
    }
    
    public function action_vervideo() { 
        
        $id = $this->request->query('v');  
        
        $model_integracoes = new Model_Integracoes_Consulta('joomla');
        $video = $model_integracoes->select_video($id);

        $view = View::factory('adm/integracoes/visualiza')
                ->set('video', $video)
        ;
        $this->response->body($view);
    }
    
    public function action_rss()
    {
        
        $this->auto_render = FALSE;
        $info = array(
            'title' => 'SPDM - Associação Paulista para o Desenvolvimento da Medicina',
            'description' => 'A SPDM - Associação Paulista para o Desenvolvimento da Medicina é a maior entidade filantrópica de prestação de serviços de saúde do Brasil - com cerca de 44 mil colaboradores, atuando em nove estados e 28 municípios para melhoria da qualidade de vida da população.',
            'language' => 'pt-br',
            'link' => 'https://www.spdm.org.br',
            'generator' => 'Comunicação - SPDM',          
        );
        
        $items = array();
        $model_integracoes = new Model_Integracoes_Consulta('joomla');
        $posts = $model_integracoes->select_conteudo();
               
        
        foreach ($posts as $post) {
        
            if(!empty($post['video'])){
                $destaque = '<div class="Video"><iframe width="560" height="315" src="https://www.youtube.com/embed/'. preg_replace(array('/YouTube/', '|/|', '|{|', '|}|'), '', $post['video']).'"></iframe></div>';  
            } else {
                $destaque = '';
            }
            
            $conteudo = '<![CDATA[<article>';
            $conteudo .= '<header>';
            $conteudo .= '<figure data-mode=aspect-fit><img src="https://www.spdm.org.br/media/k2/items/cache/'.md5('Image' . $post['id']).'_XL.jpg" /></figure>';
            $conteudo .= '<h1>'.$post['title'].'</h1>';
            $conteudo .= '<address>Comunicação SPDM</address>';
            $conteudo .= '<time class="op-published">'.date("D, d M Y H:i:s T", strtotime($post['created'])).'</time>';
            $conteudo .= '<time class="op-modified">'.date("D, d M Y H:i:s T", strtotime($post['modified'])).'</time>';
            $conteudo .= '</header>';
            $conteudo .= $destaque.''.str_replace('&nbsp;', ' ', $post['introtext']);
            $conteudo .= '<footer><small>© SPDM - Associação Paulista para o Desenvolvimento da Medicina</small></footer>';
            $conteudo .= '</article>]]>';
           
            $items[] = array(
                'title' => $post['title'],
                'link' => 'https://www.spdm.org.br/imprensa/noticias/item/' . $post['id'].'-'.$post['alias'],
                'guid' => md5('Image' . $post['id']),
                'pubDate' => date("D, d M Y H:i:s T", strtotime($post['created'])), 
                'author' => 'Comunicação SPDM',
                'category' => $post['nmCategoria'],
                'description' => $conteudo,  
            );
        }
        
                
        $this->response->headers('Content-Type', 'text/xml');
        $this->response->body(Feed::create($info, $items));
    }

}

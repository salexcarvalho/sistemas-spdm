<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Dashboard_Sec_Tipos extends Controller {

    public function action_index() {
        $this->redirect('dashboard_sec_participantes/home');
    }

    public function action_home() {
	Funcoes::verifica_login();

        $page = $this->request->query('page');

        $model_tipos = new Model_Sec_Tipos('default');
        $tipos = $model_tipos->select_tipos();

        $paginator = Paginator::factory($tipos);
        $paginator->set_current_page_number($page);

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($tipos);

        $view = View::factory('adm/certificados/tipos')
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }
    
    public function action_modal_tipos(){
        Funcoes::verifica_login();
        
          $idTipos = $this->request->query('idTipos');
          $page = $this->request->query('page');
         
        if(isset($idTipos)){
            $model_tipos = new Model_Sec_Tipos('default');
            $tipos = $model_tipos->select_tipo($idTipos);
            $view = View::factory('adm/certificados/modal_tipo_origem')
                ->set('dados', $tipos)
                ->set('page', $page)
                ->set('valor', 'Tipo');
        }else{
          $view = View::factory('adm/certificados/modal_tipo_origem')                
                ->set('page', $page)
                ->set('valor', 'Tipo');   
        }
        $this->response->body($view);
    }

    public function action_cadastro_tipo() {
	Funcoes::verifica_login();
        $view = View::factory('adm/certificados/cadastro_tipo');
        $this->response->body($view);
    }

    public function action_insert_tipo() {
	Funcoes::verifica_login();
        Session::instance();
        $nome = $this->request->post('nome');
        $alias = $this->request->post('alias');
        $status = $this->request->post('status');

        $model_tipo = new Model_Sec_Tipos('default');
        $model_tipo->insert_tipo($nome, $alias, $status);

        /* Mostra a resposta da ação */
        $warning = "Tipo de participante cadastrado com sucesso!";
        $page = $this->request->query('page');
        $tipos = $model_tipo->select_tipos();
        $paginator = Paginator::factory($tipos);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home');

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($tipos);

        $view = View::factory('adm/certificados/tipos')
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
                ->set('warning', $warning)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }

    public function action_update_tipo() {
        Session::instance();
        $idTipos = $this->request->post('idTipos');
        $nmTipo = $this->request->post('nome');
        $alias = $this->request->post('alias');
        $Status = $this->request->post('status');

        $model_tipo = new Model_Sec_Tipos('default');
        $model_tipo->update_tipo($idTipos, $nmTipo, $alias, $Status);

        //$this->redirect('dashboard_sec_tiposparticipantes/home');
        /* Mostra a resposta da ação */

        $warning = "Tipo de participante cadastrado com sucesso!";
        $page = $this->request->post('page');
        $tipos = $model_tipo->select_tipos();
        $paginator = Paginator::factory($tipos);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home');

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($tipos);

        $view = View::factory('adm/certificados/tipos')
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
                ->set('warning', $warning)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }

    public function action_delete_tipo() {
	Funcoes::verifica_login();

        $idTipos = $this->request->post('idTipos');

        $model_tipo = new Model_Sec_Tipos('default');
        $model_tipo->delete_tipo($idTipos);

        $this->redirect('dashboard_sec_tipos/home');
    }

    public function action_update_ativar_tipo() {
	Funcoes::verifica_login();
        $idTipos = $this->request->query('idTipos');

        $model_tipo = new Model_Sec_Tipos('default');
        $model_tipo->update_ativar_tipo($idTipos);
        $tipos = $model_tipo->select_tipo($idTipos);

        if ($tipos[0]['Status'] != "1") {//se retornar algum resultado
            $view = '0';
        } else {
            $view = '1';
        }
        $this->response->body($view);
    }

}

<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Dashboard_Sec_Participantes extends Controller {

    public function action_index() {
        $this->redirect('dashboard_sec_participantes/home');
    }

    public function action_home() {
        Funcoes::verifica_login();
        $ids = array();
        $page = $this->request->query('page');

        $VerificaEvento = ($this->request->query('eventoB')) ? $this->request->query('eventoB') : false;
        $Pesquisa = ($this->request->query('Busca')) ? $this->request->query('Busca') : false;

        $option = 'eventoB=' . $VerificaEvento . '&Busca=' . $Pesquisa;

        $model_sec_participantes = new Model_Sec_Participantes('default');
        $model_sec_certificados = new Model_Sec_Certificado('default');
        $model_sec_apoio = new Model_Sec_Apoio('default');

        if ($VerificaEvento != false) {
            $apoio = $model_sec_apoio->select_apoio_participante_evento($VerificaEvento);
            if ($Pesquisa != false && is_numeric($Pesquisa)) {
                $ids[] = $Pesquisa;
                $Pesquisa = false;
            } else {
                foreach ($apoio as $id):
                    $ids[] = $id['idParticipante'];
                endforeach;
            }
            if (count($ids) > 0) {

                $participantes = $model_sec_participantes->select_participantes_in($ids, $Pesquisa);
            } else {

                $participantes = $model_sec_participantes->select_participantes_pesquisa(0);
            }
        } else {
            if ($Pesquisa == false) {
                $participantes = $model_sec_participantes->select_participantes();
            } else {
                $participantes = $model_sec_participantes->select_participantes_pesquisa($Pesquisa);
            }
        }

        $eventos = $model_sec_certificados->select_certificados();

        $paginator = Paginator::factory($participantes);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home', 'page', $option);

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($participantes);

        $view = View::factory('adm/certificados/home')
                ->set('VerificaEvento', $VerificaEvento)
                ->set('Busca', $Pesquisa)
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }

    public function action_update_ativar_participante() {
        Funcoes::verifica_login();

        $idParticipante = $this->request->query('idParticipante');

        $model_sec_participantes = new Model_Sec_Participantes('default');

        $model_sec_participantes->update_ativar_participante($idParticipante);
        $dadosParticipante = $model_sec_participantes->select_participante($idParticipante);

        if ($dadosParticipante[0]['Status'] != "1") {//se retornar algum resultado
            $view = '0';
        } else {
            $view = '1';
        }
        $this->response->body($view);
    }

    public function action_gerar_certificado() {
        Funcoes::verifica_login();
        //Dados de Formulario
        $idApoio = $this->request->post('idApoio');
        $idCertificado = $this->request->post('idCertificados');

        // Gera a auteticaÃ§Ã£o do certificado

        $Autenticacao = Certificados::coderand($size = '30', $type = 4);
        $model_sec_certificado = new Model_Sec_Certificado('default');
        $model_sec_certificado->gerar_certificado($idApoio, $idCertificado, $Autenticacao);
        $this->action_index();
    }

    public function action_excluir_certificado() {
        Funcoes::verifica_login();

        $idApoio = $this->request->post('idApoio');

        $model_sec_certificado = new Model_Sec_Certificado('default');
        $model_sec_certificado->excluir_certificado($idApoio);

        $this->action_index();
        /* Mostra a resposta da aÃ§Ã£o */
        $warning = "Tipo de participante alterado com sucesso!";
        $this->response->body(View::factory('dashboard_sec_tiposorigem/home')
                        ->bind('warning', $warning))
        ;
    }

    public function action_insert_participante() {
        Funcoes::verifica_login();

        $VerificaEvento = ($this->request->query('idCertificados')) ? $this->request->query('idCertificados') : '';
        $model_participante = new Model_Sec_Participantes('default');
        $titulacao = $this->request->post('titulacao');
        $nmParticipante = $this->request->post('nmParticipante');
        $nasc = ($this->request->post('nasc')!="")?Funcoes::convertData($this->request->post('nasc')):null;
        $nacionalidade = $this->request->post('nacionalidade');
        $Doc = $this->request->post('documento');
        $TDoc = $this->request->post('idtipoDoc');
        $email = $this->request->post('email');


        $res_idParticipante = $model_participante->insert_participante($titulacao, $nmParticipante, $nasc, $nacionalidade, $Doc, $TDoc, $email);

        /* Mostra a resposta da aÃ§Ã£o */
        $page = $this->request->query('page');
        $warning = "Participante/Aluno cadastrado com sucesso!";

        $model_sec_participantes = new Model_Sec_Participantes('default');
        $participantes = $model_sec_participantes->select_participantes();

        $paginator = Paginator::factory($participantes);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home');

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($participantes);
        
        $view = View::factory('adm/certificados/home')
                ->bind('PaginaAtual', $PaginaAtual)
                ->bind('VerificaEvento', $VerificaEvento)
                ->bind('NtotalPagina', $NtotalPagina)
                ->bind('total', $total)
                ->bind('warning', $warning)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }

    public function action_cadastro_tipo_participante() {
        Funcoes::verifica_login();

        $view = View::factory('adm/certificados/cadastro_tipo_participante');
        $this->response->body($view);
    }

    public function action_cadastro_participante() {
        Funcoes::verifica_login();

        $model_tipo = new Model_Sec_Tipos('default');
        $tipos = $model_tipo->select_tipos();
        $model_Origem = new Model_Sec_Origem('default');
        $origem = $model_Origem->select_origens();

        $view = View::factory('adm/certificados/cadastro_participante')
                ->set('tipos', $tipos)
                ->set('origem', $origem)
        ;
        $this->response->body($view);
    }

    public function action_import_participante() {
        Funcoes::verifica_login();
        $view = View::factory('adm/certificados/import_participante');
        $this->response->body($view);
    }

    public function action_import_participante_open() {
        Funcoes::verifica_login();
        $part = $_FILES['participantes'];
        $part_nome = date('dmy') . '_' . $part['name'];
        $arquivo = Upload::save($part, $part_nome, 'upload/csv');
        $csv = CSV::factory($arquivo)->parse();
        $titulos = $csv->titles();
        $linhas = $csv->rows();
      
        $colunas = array("titulacao", "nome", "nacionalidade", "nascimento", "documento", "tipoDoc", "email", "eventoNome", "modulo", "tema", "data", "livre");
        $view = View::factory('adm/certificados/import_participante_open')
                ->set('titulos', $titulos)
                ->set('colunas', $colunas)
                ->set('linhas', $linhas);
        $this->response->body($view);
    }

    public function action_delete_participante() {
        
        Funcoes::verifica_login();
        
        $idParticipante = $this->request->post('idParticipante');
        
        $model_participante = new Model_Sec_Participantes('default');
        $del_idParticipante = $model_participante->delete_participante($idParticipante);
        
        $page = $this->request->query('page');
        $warning = "Participante removido com sucesso!";

        $model_sec_participantes = new Model_Sec_Participantes('default');
        $participantes = $model_sec_participantes->select_participantes();
        $VerificaEvento = ($this->request->query('eventoB')) ? $this->request->query('eventoB') : false;

        $paginator = Paginator::factory($participantes);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home');

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($participantes);

        $view = View::factory('adm/certificados/home')
                ->set('VerificaEvento', $VerificaEvento)
                ->set('PaginaAtual', $PaginaAtual)          
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
                ->set('warning', $warning)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }

    public function action_editar_participante() {
        Funcoes::verifica_login();

        $page = $this->request->query('page');
        $idParticipante = $this->request->query('idParticipante');
        $model_Participantes = new Model_Sec_Participantes('default');
        $dadosParticipante = $model_Participantes->select_participante($idParticipante);

        $view = View::factory('adm/certificados/editar_participante')
                ->set('dadosParticipante', $dadosParticipante)
                ->set('page', $page)
        ;

        $this->response->body($view);
    }

    public function action_adicionar_complemento() {
        Funcoes::verifica_login();

        $page = $this->request->query('page');
        $idParticipante = $this->request->query('idParticipante');
        $model_Participantes = new Model_Sec_Participantes('default');
        $dadosParticipante = $model_Participantes->select_participante($idParticipante);

        $view = View::factory('adm/certificados/adicionar_complemento')
                ->set('dadosParticipante', $dadosParticipante)
                ->set('page', $page);

        $this->response->body($view);
    }

    public function action_editar_complemento() {
        Funcoes::verifica_login();

        $page = $this->request->query('page');
        $idParticipante = $this->request->query('idParticipante');
        $idApoio = $this->request->query('idApoio');
        $model_apoio = new Model_Sec_Apoio('default');
        $dados = $model_apoio->select_apoio($idApoio);

        $view = View::factory('adm/certificados/editar_complemento')
                ->set('dados', $dados)
                ->set('page', $page)
        ;

        $this->response->body($view);
    }

    public function action_visualizar_complemento() {
        Funcoes::verifica_login();

        $page = $this->request->query('page');
        $idApoio = $this->request->query('idApoio');
        $model_apoio = new Model_Sec_Apoio('default');
        $dados = $model_apoio->select_apoio($idApoio);

        $view = View::factory('adm/certificados/visualizar_complemento')
                ->set('dados', $dados)
                ->set('page', $page)
        ;

        $this->response->body($view);
    }

    public function action_deleta_complemento() {
        Funcoes::verifica_login();

        $idApoio = $this->request->post('idApoio');
        $model_apoio = new Model_Sec_Apoio('default');
        //Funcoes::dump($idApoio);
        $atualizarDeletar = $model_apoio->delete_apoio($idApoio);

        return $atualizarDeletar;
    }

    public function action_update_participante() {
        Funcoes::verifica_login();

        $idParticipante = $this->request->post('idParticipante');
        $titulacao = $this->request->post('titulacao');
        $Nome = $this->request->post('nmParticipante');
        $nacionalidade = $this->request->post('nacionalidade');
        $nasc = ($this->request->post('nasc')!="")?Funcoes::convertData($this->request->post('nasc')):null;
        $Doc = $this->request->post('documento');
        $TDoc = $this->request->post('idtipoDoc');
        $Email = $this->request->post('email');

        $model_Participantes = new Model_Sec_Participantes('default');
        $atualizaParticipante = $model_Participantes->update_participante($idParticipante, $titulacao, $Nome, $nasc, $nacionalidade, $Doc, $TDoc, $Email);

        /* Mostra a resposta da aÃ§Ã£o */
        $page = $this->request->post('page');
        $warning = "Participante/Aluno alterado com sucesso!";
        $VerificaEvento = ($this->request->query('idCertificado')) ? $this->request->query('idCertificado') : '';

        $model_sec_participantes = new Model_Sec_Participantes('default');
        $participantes = $model_sec_participantes->select_participantes();

        $paginator = Paginator::factory($participantes);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home');

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($participantes);

        $view = View::factory('adm/certificados/home')
                ->bind('VerificaEvento', $VerificaEvento)
                ->bind('page', $page)
                ->bind('PaginaAtual', $PaginaAtual)
                ->bind('NtotalPagina', $NtotalPagina)
                ->bind('total', $total)
                ->bind('warning', $warning)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }

    public function action_update_complemento() {
        Funcoes::verifica_login();

        $idApoio = $this->request->post('idApoio');
        $idParticipante = $this->request->post('idParticipante');
        $idCertificado = $this->request->post('idCertificado');
        $idOrigem = $this->request->post('idOrigem');
        $idTipo = $this->request->post('idTipo');
        if ($this->request->post('data') != "") {
            $data = Funcoes::convertData($this->request->post('data'));
        } else {
            $data = null;
        }
        $modulo = $this->request->post('modulo');
        $tema = $this->request->post('tema');
        $livre = $this->request->post('livre');

        $model_apoio = new Model_Sec_Apoio('default');
        $atualizarComplemento = $model_apoio->update_apoio($idApoio, $idParticipante, $idCertificado, $idTipo, $idOrigem, $modulo, $tema, $data, $livre);

        /* Mostra a resposta da aÃ§Ã£o */
        $page = $this->request->post('page');
        $warning = "Complemento alterado com sucesso!";
        $VerificaEvento = ($this->request->query('idCertificado')) ? $this->request->query('idCertificado') : '';

        $model_sec_participantes = new Model_Sec_Participantes('default');
        $participantes = $model_sec_participantes->select_participantes();

        $paginator = Paginator::factory($participantes);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home');

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($participantes);

        $view = View::factory('adm/certificados/home')
                ->set('VerificaEvento', $VerificaEvento)
                ->set('page', $page)
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
                ->set('warning', $warning)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }

    public function action_visualizar_participante() {
        Funcoes::verifica_login();

        $page = $this->request->query('page');
        $idParticipante = $this->request->query('idParticipante');

        $model_participantes = new Model_Sec_Participantes('default');
        $dadosParticipante = $model_participantes->select_participante($idParticipante);

        $view = View::factory('adm/certificados/visualizar_participante')
                ->set('dadosParticipante', $dadosParticipante)
                ->set('page', $page)
        ;

        $this->response->body($view);
    }

    public function action_validar_email() {
        $buscar = $this->request->post('email');
        $participante = new Model_Sec_Participantes('default');
        $email = $participante->validar_email($buscar);
        return $email;
    }

    public function action_insert_complemento() {
        Funcoes::verifica_login();


        $model_participante = new Model_Sec_Participantes('default');
        $model_apoio = new Model_Sec_Apoio('default');

        $idParticipante =(int) $this->request->post('idParticipante');
        $idOrigem =(int) $this->request->post('idOrigem');
        $idTipo = (int)$this->request->post('idTipo');
        $modulo = ($this->request->post('modulo')=="")? null:$this->request->post('modulo');
        $tema = ($this->request->post('modulo')=="")? null:$this->request->post('modulo');
        if ($this->request->post('data') != "") {
            $data = Funcoes::convertData($this->request->post('data'));
        } else {
            $data = null;
        }
        $livre = $this->request->post('livre');
        $idCertificado = ($this->request->query('idCertificado')) ? $this->request->query('idCertificado') : null;
        $VerificaEvento = ($this->request->query('idCertificado')) ? $this->request->query('idCertificado') : "";
        $res_idParticipante = $model_apoio->insert_apoio($idParticipante, $idCertificado, $idTipo, $idOrigem, $modulo, $tema, $data, $livre);

        /* Mostra a resposta da adição */
        $page = $this->request->query('page');
        $warning = "Complemento cadastrado com sucesso!";

        $model_sec_participantes = new Model_Sec_Participantes('default');
        $participantes = $model_sec_participantes->select_participantes();

        $paginator = Paginator::factory($participantes);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home');

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($participantes);

        $view = View::factory('adm/certificados/home')
                ->set('PaginaAtual', $PaginaAtual)
                ->set('VerificaEvento', $VerificaEvento)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
                ->set('warning', $warning)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }

    public function action_import_insert() {
        Funcoes::verifica_login();
        
        $campos = $this->request->post();        
        $reg = count($campos);
       // Funcoes::dump($campos);
        $model_participantes = new Model_Sec_Participantes('default');
        $model_tipos = new Model_Sec_Tipos('default');
        $model_origem = new Model_Sec_Origem('default');
        $model_modelo = new Model_Sec_Certificado('default');
        $model_apoio = new Model_Sec_Apoio('default');
        $model_tipoDoc = new Model_Sec_TipoDoc('default');

        for($i=1;$i<=$reg;$i++):
            $titulacao = (isset($campos[$i]['titulacao']))?$campos[$i]['titulacao']:NULL;
            $nome = (isset($campos[$i]['nome']))?$campos[$i]['nome']:NULL;
            $nasc = (isset($campos[$i]['nascimento']))?$campos[$i]['nascimento']:NULL;
            $nacionalidade = (isset($campos[$i]['nacionalidade']))?$campos[$i]['nacionalidade']:NULL;
            $email = (isset($campos[$i]['email']))?$campos[$i]['email']:NULL;
            $Doc = (isset($campos[$i]['documento']))?$campos[$i]['documento']:NULL;            
            $aDoc = (isset($campos[$i]['tipoDoc']))?$campos[$i]['tipoDoc']:NULL;            
            $tipo = (isset($campos[$i]['idTipo']))?$campos[$i]['idTipo']:NULL;
            $origem = (isset($campos[$i]['idOrigem']))?$campos[$i]['idOrigem']:NULL;
            $evento = (isset($campos[$i]['eventoNome']))?$campos[$i]['eventoNome']:NULL;
            $modulo = (isset($campos[$i]['modulo']))?$campos[$i]['modulo']:NULL;
            $tema = (isset($campos[$i]['tema']))?$campos[$i]['tema']:NULL;
            $data = (isset($campos[$i]['data']))?$campos[$i]['data']:NULL;
            $livre = (isset($campos[$i]['livre']))?$campos[$i]['livre']:NULL;         
            $idSetor = (isset($campos[$i]['idSetor']))?$campos[$i]['idSetor']:$_SESSION['idSetor'];           
                    
            if($email==''):
                $participante = $model_participantes->select_participante_nome($nome);
            else:
                $participante = $model_participantes->select_participante_nome_email($nome, $email); 
            endif;
            
            if($aDoc!=NULL){
                $aux = $model_tipoDoc->select_tipoDoc_alias($aDoc);
                $TDoc = $aux[0]['idTipoDoc'];
            }else{
                $TDoc = NULL;
            }
           
            //if (!isset($participante[0]['idParticipante'])):
                $pp = $model_participantes->insert_participante($titulacao, $nome, $nasc, $nacionalidade, $Doc, $TDoc, $email, $idSetor);
                $participante_id = $pp[0];
            /*else:
                $participante_id = $participante[0]['idParticipante'];
                $model_participantes->update_participante($participante_id, $titulacao, $nome, $nasc, $nacionalidade, $Doc, $TDoc, $email, $idSetor);
            endif;*/

            if($data[$i]!=''):
                    $data_alt = Funcoes::convertData($data);
                else:
                   $data_alt = NULL; 
                endif;
            
            $apoio = $model_apoio->insert_apoio($participante_id, NULL, $tipo, $origem, $modulo, $tema, $data_alt, $livre);            
            $id_apoio = $apoio[0];
            

            $modelo = $model_modelo->select_certificado_auth(md5($evento));

            if (isset($modelo[0]['idCertificados'])):
                $id_modelo = $modelo[0]['idCertificados'];
                $ap = $model_apoio->update_apoio_certificado($id_apoio, $id_modelo);
                $auth = $model_apoio->select_apoio($id_apoio);
                if ($auth[0]['Autenticacao'] == null) {
                    $Autenticacao = Certificados::coderand($size = '30', $type = 4);
                    $model_apoio->update_apoio_autenticacao($id_apoio, $Autenticacao);
                }
            endif;

        endfor;
        return NULL;
    }

    function action_baixarModelo() {
        Funcoes::verifica_login();
        $arquivo = "modelocsv.xls";
        Download::factory()
                ->setDirectory(URL::base() . "upload/modelocsv/")
                ->setFileName("modelocsv.xls")
                ->setFileRealName("modelocsv.xls")
                ->setContentType('application/vnd.ms-excel')
                ->execute();
    }

    function action_existe_cert() {
        Funcoes::verifica_login();
        $idCertificado = $this->request->post('idCertificado');
        $model_apoio = new Model_Sec_Apoio('default');
        $ap = $model_apoio->select_apoio_ceritificado_lote($idCertificado);

        if (count($ap) > 0) {
            echo 1;
        } else {
            echo 0;
        }
    }

    function action_modalEmail() {
        Funcoes::verifica_login();

        $autenticacao = $this->request->query('autenticacao');
        $idCertificados = $this->request->query('idCertificados');
        $modal_apoio = new Model_Sec_Apoio();

        if ($autenticacao != '') {
            $tipo = 'individual';
            $dados = $modal_apoio->select_apoio_ceritificado_auth($autenticacao);
        }

        if ($idCertificados != '') {
            $tipo = 'grupo';
            $dados = $modal_apoio->select_apoio_ceritificado_lote_full($idCertificados);
        }
        $view = View::factory('adm/certificados/modal_enviarCertificado')
                ->set('tipo', $tipo)
                ->set('dados', $dados);
        $this->response->body($view);
    }
    
    function action_modal_remover() {
        Funcoes::verifica_login();

        $url = $this->request->query('url');
        $id = $this->request->query('id');
        $page = $this->request->query('page');
        $nmParticipante = $this->request->query('nmParticipante');
        
        $view = View::factory('adm/certificados/modal_excluir')
                ->set('url', $url)
                ->set('id', $id)
                ->set('page', $page)
                ->set('nmParticipante', $nmParticipante);
        $this->response->body($view);
    }
    

    function action_enviarCertificado() {
        Funcoes::verifica_login();
        $idApoio = $this->request->post('idApoio');        
        $modal_apoio = new Model_Sec_Apoio();
        $apoio = $modal_apoio->select_apoio_email($idApoio);

        $viewemail = View::factory('adm/certificados/emailCertificado')
                ->set('apoio', $apoio)
                ->render();

        $email_subject = 'Certificado ' . $apoio[0]['eventoNome'];
        $email_body = $viewemail;
        $is_html = true;
        $message = Email::factory($email_subject, $email_body, $is_html)
                ->from('naoresponder@spdm.org.br', 'Sistemas SPDM')
                ->to($apoio[0]['Email'], $apoio[0]['Nome'])
                ->bcc('sergio.carvalho@spdm.org.br', 'Sérgio Alexandre');
        $result = $message->send();
		
        $this->redirect('dashboard_sec_participantes/home');
    }

    function action_enviarCertificados() {
        Funcoes::verifica_login();
        $idApoios = $this->request->post('idApoio');        
        $modal_apoio = new Model_Sec_Apoio();
        //Funcoes::dump($idApoios); exit;
        foreach ($idApoios as $idApoio):
            $apoio = $modal_apoio->select_apoio_email($idApoio);
            $viewemail = View::factory('adm/certificados/emailCertificado')
                    ->set('apoio', $apoio)
                    ->render();
            $email_subject = 'Certificado ' . $apoio[0]['eventoNome'];
            $email_body = $viewemail;
            $is_html = true;
            $message = Email::factory($email_subject, $email_body, $is_html)
                    ->from('naoresponder@spdm.org.br', 'Sistemas SPDM')
                    ->to($apoio[0]['Email'], $apoio[0]['Nome'])
                    ->bcc('sergio.carvalho@spdm.org.br', 'Sérgio Alexandre');
            $message->send();
        endforeach;
       $this->redirect('dashboard_sec_participantes/home');
    }
    
    function action_valida(){
        $nome = $this->request->post('nome');
        $email = $this->request->post('email');
        $model_participante = new Model_Sec_Participantes();
        
        if($email!='')
            $participante = $model_participante->select_participante_nome_email($nome, $email);
        else
            $participante = $model_participante->select_participante_nome($nome);
        
        if($participante[0]['idParticipante']!='')
            echo json_encode ($participante[0]);
        echo '';
        }
    
}

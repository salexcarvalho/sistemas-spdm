<?php
defined('SYSPATH') or die('No direct script access.');

class Controller_Dashboard_Sec_Certificados extends Controller {

    public function action_index() {
        $this->redirect('dashboard_sec_participantes/home');
    }

    public function action_home() {
	Funcoes::verifica_login();

        $page = $this->request->query('page');

        $model_apoio = new Model_Sec_Apoio('default');
        $apoio = $model_apoio->select_apoio_certificados();

        $paginator = Paginator::factory($apoio);
        $paginator->set_current_page_number($page);

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($apoio);

        $view = View::factory('adm/certificados/certificados')
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();
        $this->response->body($view);
    }

    public function action_visualizar_certificado() {
	Funcoes::verifica_login();

        $idCertificado = $this->request->query('idCertificado');
        $model_certificado = new Model_Sec_Certificado('default');
        $certificado = $model_certificado->select_certificado($idCertificado);

        $view = View::factory('adm/certificados/visualizar_certificado')
                ->set('certificado', $certificado)
        ;

        $this->response->body($view);
    }

    // Funcoes para criação e manipulacao dos modelos  
    public function action_modelos() {
	Funcoes::verifica_login();

        $page = $this->request->query('page');

        $model_certificados = new Model_Sec_Certificado('default');
        $certificados = $model_certificados->select_certificados();

        $paginator = Paginator::factory($certificados);
        $paginator->set_current_page_number($page);

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($certificados);

        $view = View::factory('adm/certificados/modelos')
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();
        $this->response->body($view);
    }

    public function action_visualizar_modelo() {
	Funcoes::verifica_login();

        $page = $this->request->query('page');
        $idModelo = $this->request->query('idCertificados');

        $model_modelo = new Model_Sec_Certificado('default');
        $modelos = $model_modelo->select_certificado($idModelo);

        $view = View::factory('adm/certificados/visualizar_modelo')
                ->set('modelos', $modelos)
                ->set('page', $page)
        ;

        $this->response->body($view);
    }

    public function action_update_ativar_modelo() {
	Funcoes::verifica_login();

        $idCertificado = $this->request->query('idCertificado');

        $model_sec_modelos = new Model_Sec_Certificado('default');
        $model_sec_modelos->update_ativar_modelo($idCertificado);
        $modelo = $model_sec_modelos->select_modelo($idCertificado);

        if ($modelo[0]['Status'] != "1") {//se retornar algum resultado
            $view = '0';
        } else {
            $view = '1';
        }
        $this->response->body($view);
    }

    public function action_cadastro_modelo() {
	Funcoes::verifica_login();
        $model_assinatura = new Model_Sec_Assinatura('default');
        $assinaturas = $model_assinatura->select_assinaturas();
        $model_origem = new Model_Sec_Origem('default');
        $origens = $model_origem->select_origens();


        $view = View::factory('adm/certificados/cadastro_modelo')
                ->set('assinaturas', $assinaturas)
                ->set('origens', $origens)
        ;
        $this->response->body($view);
    }

    public function action_insert_modelo() {
	Funcoes::verifica_login();

        $nomeEvento = $this->request->post('nmEvento');
        $local = $this->request->post('Local');
        $data = $this->request->post('Data');
        $hora = $this->request->post('Hora');
        $c_horaria = $this->request->post('c_Horaria');
        $msg_cert = $this->request->post('msg_Cert');
        $idAssinatura1 = ($this->request->post('idAssinaturas1')=="")?null:$this->request->post('idAssinaturas1');
        $idAssinatura2 = ($this->request->post('idAssinaturas2')=="")?null:$this->request->post('idAssinaturas2');
        $idAssinatura3 = ($this->request->post('idAssinaturas3')=="")?null:$this->request->post('idAssinaturas3');
        $idAssinatura4 = ($this->request->post('idAssinaturas4')=="")?null:$this->request->post('idAssinaturas4');
        $idOrigem = $this->request->post('idOrigem');
        $img_Cert = $_FILES['img_Cert'];

        if ($img_Cert['name'] !== '') {
            $img_Cert_name = date('dmy') . '_' . $img_Cert['name'];
        } else {
            $img_Cert_name = $img_Cert['name'];
        }


        // upload
        $img_Cert = Upload::save($_FILES['img_Cert'], $img_Cert_name, 'upload/img/certificados');

        $model_certificado = new Model_Sec_Certificado('default');

        $R_idCertificado = $model_certificado->insert_modelo($idOrigem, $idAssinatura1, $idAssinatura2, $idAssinatura3, $idAssinatura4, $nomeEvento, $local, $data, $hora, $c_horaria, $msg_cert, $img_Cert);

        //$this->action_index(); 

        /* Mostra a resposta da ação */
        $warning = "Modelo de certificado cadastrado com sucesso!";
        $page = $this->request->query('page');
        $modelos = $model_certificado->select_certificados();

        $paginator = Paginator::factory($modelos);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home');

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($modelos);

        $view = View::factory('adm/certificados/modelos')
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
                ->set('warning', $warning)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }

    public function action_editar_modelo() {
	Funcoes::verifica_login();

        $page = $this->request->post('page');
        $idCertificado = $this->request->post('idCertificados');

        $model_sec_modelo = new Model_Sec_Certificado('default');
        $modelo = $model_sec_modelo->select_certificado($idCertificado);

        $model_sec_origem = new Model_Sec_Origem('default');
        $origem = $model_sec_origem->select_origens();

        $model_sec_assinatura = new Model_Sec_Assinatura('default');
        $assinatura = $model_sec_assinatura->select_assinaturas();

        $view = View::factory('adm/certificados/editar_modelo')
                ->set('modelo', $modelo)
                ->set('assinaturas', $assinatura)
                ->set('origens', $origem)
                ->set('page', $page)
        ;
        $this->response->body($view);
    }

    public function action_update_modelo() {
	Funcoes::verifica_login();
        //echo "<pre>";var_dump($this->request->post());exit;
        $idCertificados = $this->request->post('idCertificados');
        $nmEvento = $this->request->post('nmEvento');
        $dtEvento = $this->request->post('dtEvento');
        $Hora = $this->request->post('Hora');
        $Local = $this->request->post('Local');
        $c_horaria = $this->request->post('c_Horaria');
        $idOrigem = $this->request->post('idOrigem');
        $idAssinatura1 = ($this->request->post('idAssinaturas1')=="")?null:$this->request->post('idAssinaturas1');
        $idAssinatura2 = ($this->request->post('idAssinaturas2')=="")?null:$this->request->post('idAssinaturas2');
        $idAssinatura3 = ($this->request->post('idAssinaturas3')=="")?null:$this->request->post('idAssinaturas3');
        $idAssinatura4 = ($this->request->post('idAssinaturas4')=="")?null:$this->request->post('idAssinaturas4');
        $msg_cert = $this->request->post('msg_Cert');
        $img_Cert_hidenn = $this->request->post('img_Cert_hidenn');
        $img_Cert = $_FILES['img_Cert'];

        if ($img_Cert['name'] !== '') {
            $img_Cert_name = date('dmy') . '_' . $img_Cert['name'];
        } else {
            $img_Cert_name = $img_Cert['name'];
        }
        // upload
        $img_Cert = Upload::save($_FILES['img_Cert'], $img_Cert_name, 'upload/img/certificados');


        $model_sec_modelos = new Model_Sec_Certificado('default');
        $model_sec_modelos->update_modelo($idCertificados, $idOrigem, $idAssinatura1, $idAssinatura2, $idAssinatura3, $idAssinatura4, $nmEvento, $Local, $dtEvento, $Hora, $c_horaria, $msg_cert, $img_Cert_hidenn, $img_Cert_name);

        /* Mostra a resposta da ação */
        $warning = "Modelo de certificado alterado com sucesso!";
        $page = $this->request->post('page');
        $modelos = $model_sec_modelos->select_certificados();

        $paginator = Paginator::factory($modelos);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('modelos');

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($modelos);

        $view = View::factory('adm/certificados/modelos')
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
                ->set('warning', $warning)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }

    public function action_delete_modelo() {
	Funcoes::verifica_login();

        $idCertificados = $this->request->post();

        $model_sec_modelos = new Model_Sec_Certificado('default');
        $model_sec_modelos->delete_modelo($idCertificados);
    }

}

<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Dashboard_Sec_Origem extends Controller {

    public function action_index() {
        $this->redirect('dashboard_sec_origem/home');
    }

    public function action_home() {
	Funcoes::verifica_login();
        $page = $this->request->query('page');
        $model_origens = new Model_Sec_Origem('default');
        $origens = $model_origens->select_origens();

        $paginator = Paginator::factory($origens);
        $paginator->set_current_page_number($page);

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($origens);

        $view = View::factory('adm/certificados/origem')
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }

    public function action_cadastro_origem() {
	Funcoes::verifica_login();
        $view = View::factory('adm/certificados/cadastro_origem')
        ;
        $this->response->body($view);
    }

    public function action_insert_origem() {
	Funcoes::verifica_login();
        $nmOrigem = $this->request->post('nome');
        $aliasOrigem = $this->request->post('alias');
        $status = $this->request->post('status');

        $model_origens = new Model_Sec_Origem('default');
        $model_origens->insert_origem($nmOrigem, $aliasOrigem, $status);

        /* Mostra a resposta da ação */
        $warning = "Tipo de origem cadastrado com sucesso!";
        $page = $this->request->query('page');
        $origens = $model_origens->select_origens();

        $paginator = Paginator::factory($origens);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home');

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($origens);

        $view = View::factory('adm/certificados/origem')
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
                ->set('warning', $warning)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }
    public function action_modal_origem(){
        Funcoes::verifica_login();
        
        $page = $this->request->query('page');
        $idOrigem = $this->request->query('idOrigem');      
        if(isset($idOrigem)){
            $model_Tipo_Origem = new Model_Sec_Origem('default');
            $tpOrigem = $model_Tipo_Origem->select_origem($idOrigem);
            $view = View::factory('adm/certificados/modal_tipo_origem')
                ->set('dados', $tpOrigem)
                ->set('page', $page)
                ->set('valor', 'Origem');
        }else{
          $view = View::factory('adm/certificados/modal_tipo_origem')                
                ->set('page', $page)
                ->set('valor', 'Origem');  
        }
        $this->response->body($view);
    }
 

    public function action_update_ativar_origem() {
	Funcoes::verifica_login();
        $idOrigem = $this->request->query('idOrigem');

        $model_Origem = new Model_Sec_Origem('default');
        $model_Origem->update_ativarOrigem($idOrigem);
        $origens = $model_Origem->select_origem($idOrigem);

        if ($origens[0]['Status'] != "1") {//se retornar algum resultado
            $view = '0';
        } else {
            $view = '1';
        }
        $this->response->body($view);
    }

    public function action_update_origem() {
	Funcoes::verifica_login();
        $idOrigem = $this->request->post('idOrigem');
        $nmOrigem = $this->request->post('nome');
        $status = $this->request->post('status');
        $aliasOrigem = $this->request->post('alias');

        $model_Origem = new Model_Sec_Origem('default');
        $model_Origem->update_origem($idOrigem, $nmOrigem, $status, $aliasOrigem);

        /* Mostra a resposta da ação */
        $warning = "Tipo de origem alterado com sucesso!";
        $page = $this->request->post('page');
        $origens = $model_Origem->select_origens();

        $paginator = Paginator::factory($origens);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home');

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($origens);

        $view = View::factory('adm/certificados/origem')
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
                ->set('warning', $warning)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }

    public function action_delete_origem() {
	Funcoes::verifica_login();

        $idOrigem = $this->request->post('idOrigem');

        $model_Tipo_Origem = new Model_Sec_Origem('default');
        $model_Tipo_Origem->delete_origem($idOrigem);

        $this->redirect('dashboard_sec_origem/home');
    }

}

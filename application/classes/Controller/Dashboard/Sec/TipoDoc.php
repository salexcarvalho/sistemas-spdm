<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Dashboard_Sec_TipoDoc extends Controller {

    public function action_index() {
        $this->redirect('dashboard_sec_participantes/home');
    }

    public function action_home() {
	Funcoes::verifica_login();

        $page = $this->request->query('page');

        $model_tipoDoc = new Model_Sec_TipoDoc('default');
        $tipoDoc = $model_tipoDoc->select_tipoDocs();

        $paginator = Paginator::factory($tipoDoc);
        $paginator->set_current_page_number($page);

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($tipoDoc);

        $view = View::factory('adm/certificados/tipoDoc')
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }
    
    public function action_modal_tipoDoc(){
        Funcoes::verifica_login();
        
          $idTipoDoc = $this->request->query('idTipoDoc');
          $page = $this->request->query('page');
         
        if(isset($idTipoDoc)){
            $model_tipoDoc = new Model_Sec_TipoDoc('default');
            $tipoDoc = $model_tipoDoc->select_tipoDoc($idTipoDoc);
            $view = View::factory('adm/certificados/modal_tipoDoc')
                ->set('dados', $tipoDoc)
                ->set('page', $page);
        }else{
          $view = View::factory('adm/certificados/modal_tipoDoc')                
                ->set('page', $page);
        }
        $this->response->body($view);
    }

    public function action_cadastro_tipo() {
	Funcoes::verifica_login();
        $view = View::factory('adm/certificados/cadastro_tipoDoc');
        $this->response->body($view);
    }

    public function action_insert_tipoDoc() {
	Funcoes::verifica_login();
        Session::instance();
        $nome = $this->request->post('nome');
        $alias = $this->request->post('alias');
        $status = $this->request->post('status');

        $model_tipoDoc = new Model_Sec_TipoDoc('default');
        $model_tipoDoc->insert_tipoDoc($nome, $alias, $status);

        /* Mostra a resposta da ação */
        $warning = "Tipo de participante cadastrado com sucesso!";
        $page = $this->request->query('page');
        $tipoDoc = $model_tipoDoc->select_tipoDocs();
        $paginator = Paginator::factory($tipoDoc);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home');

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($tipoDoc);

        $view = View::factory('adm/certificados/tipoDoc')
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
                ->set('warning', $warning)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }

    public function action_update_tipo() {
        Session::instance();
        $idTipos = $this->request->post('idTipos');
        $nmTipo = $this->request->post('nome');
        $alias = $this->request->post('alias');
        $Status = $this->request->post('status');

        $model_tipoDoc = new Model_Sec_TipoDoc('default');
        $model_tipoDoc->update_tipoDoc($idTipos, $nmTipo, $alias, $Status);

        //$this->redirect('dashboard_sec_tiposparticipantes/home');
        /* Mostra a resposta da ação */

        $warning = "Tipo de participante cadastrado com sucesso!";
        $page = $this->request->post('page');
        $tipoDoc = $model_tipoDoc->select_tipoDocs();
        $paginator = Paginator::factory($tipoDoc);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home');

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($tipoDoc);

        $view = View::factory('adm/certificados/tipoDoc')
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
                ->set('warning', $warning)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }

    public function action_delete_tipo() {
	Funcoes::verifica_login();

        $idTipoDoc = $this->request->post('idTipoDoc');

        $model_tipoDoc = new Model_Sec_TipoDoc('default');
        $model_tipoDoc->delete_tipoDoc($idTipoDoc);

        $this->redirect('dashboard_sec_tipoDoc/home');
    }

    public function action_update_ativar_tipo() {
	Funcoes::verifica_login();
        $idTipoDoc = $this->request->query('idTipoDoc');

        $model_tipoDoc = new Model_Sec_TipoDoc('default');
        $model_tipoDoc->update_ativar_tipoDoc($idTipoDoc);
        $tipoDoc = $model_tipoDoc->select_tipoDoc($idTipoDoc);

        if ($tipoDoc[0]['Status'] != "1") {//se retornar algum resultado
            $view = '0';
        } else {
            $view = '1';
        }
        $this->response->body($view);
    }

}

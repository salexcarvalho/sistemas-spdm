<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Dashboard_Sec_Assinaturas extends Controller {

    public function action_index() {
        $this->redirect('dashboard_sec_participantes/home');
    }

    public function action_home() {
	Funcoes::verifica_login();

        $page = $this->request->query('page');

        $model_assinaturas = new Model_Sec_Assinatura('default');
        $dados = $model_assinaturas->select_assinaturas();
        $paginator = Paginator::factory($dados);
        $paginator->set_current_page_number($page);

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($dados);

        $view = View::factory('adm/certificados/assinaturas')
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total);

        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }

    public function action_modal_assinatura() {
	Funcoes::verifica_login();

        $page = $this->request->query('page');        
        $idAssinaturas = $this->request->query('idAssinatura');
        $editar = $this->request->query('editar');
        
        if($idAssinaturas!=""){
        $model_assinaturas = new Model_Sec_Assinatura('default');
        $dados = $model_assinaturas->select_assinatura($idAssinaturas);
        
        $view = View::factory('adm/certificados/modal_assinatura')
                ->set('dados', $dados)
                ->set('editar', $editar)
                ->set('page', $page);
        }else{
            $view = View::factory('adm/certificados/modal_assinatura')
            ->set('page', $page); 
        }
        $this->response->body($view);
    }

    
    
    public function action_editar_assinatura() {
	Funcoes::verifica_login();

        $page = $this->request->post('page');
        $idAssinaturas = $this->request->post('idAssinaturas');

        $model_assinaturas = new Model_Sec_Assinatura('default');
        $dados = $model_assinaturas->select_assinatura($idAssinaturas);

        $view = View::factory('adm/certificados/editar_assinatura')
                ->set('dados', $dados)
                ->set('page', $page);

        $this->response->body($view);
    }

    public function action_cadastro_assinatura() {
	Funcoes::verifica_login();
        $view = View::factory('adm/certificados/cadastro_assinatura');
        $this->response->body($view);
    }

    public function action_insert_assinatura() {
	Funcoes::verifica_login();
        $titulacao = $this->request->post('titulacao');
        $nome = $this->request->post('nome');
        $cargo = $this->request->post('cargo');
        $status = $this->request->post('status');
        $PngAssinatura = $_FILES['PngAssinatura'];
        $PngAssinatura_name = date('dmy') . '_' . $PngAssinatura['name'];
        //echo getimagesize($_FILES['PngAssinatura']);
        //exit;
        // upload
        $PngAssinatura = Upload::save($_FILES['PngAssinatura'], $PngAssinatura_name, 'upload/img/certificados');

        $model_Assinatura = new Model_Sec_Assinatura('default');
        $model_Assinatura->insert_assinatura($titulacao, $nome, $cargo, $PngAssinatura_name, $status);

        /* Mostra a resposta da ação */
        $page = $this->request->query('page');
        $warning = "Assinatura cadastrada com sucesso!";
        $dados = $model_Assinatura->select_assinaturas();

        $paginator = Paginator::factory($dados);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home');

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($dados);

        $view = View::factory('adm/certificados/assinaturas')
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
                ->set('warning', $warning)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }

    public function action_delete_assinatura() {
	Funcoes::verifica_login();

        $idAssinaturas = $this->request->post('idAssinaturas');

        $model_Assinatura = new Model_Sec_Assinatura('default');
        $model_Assinatura->delete_Assinatura($idAssinaturas);
    }

    public function action_update_assinatura() {
	Funcoes::verifica_login();
        $titulacao = $this->request->post('titulacao');
        $idAssinaturas = $this->request->post('idAssinaturas');
        $nome = $this->request->post('nome');
        $cargo = $this->request->post('cargo');
        $status = $this->request->post('status');
        $PngAssinatura_hidenn = $this->request->post('PngAssinatura_hidenn');
        $PngAssinatura = $_FILES['PngAssinatura'];
        $PngAssinatura_name = date('dmY') . '_' . $PngAssinatura['name'];
      
        $PngAssinatura = Upload::save($_FILES['PngAssinatura'], $PngAssinatura_name, 'upload/img/certificados');

        $model_Assinatura = new Model_Sec_Assinatura('default');
        $model_Assinatura->update_assinatura($idAssinaturas, $titulacao, $nome, $cargo, $PngAssinatura_hidenn, $PngAssinatura_name, $status);

        /* Mostra a resposta da ação */
        $page = $this->request->post('page');
        $warning = "Assinatura alterada com sucesso!";
        $dados = $model_Assinatura->select_assinaturas();

        $paginator = Paginator::factory($dados);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home');

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($dados);

        $view = View::factory('adm/certificados/assinaturas')
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
                ->set('warning', $warning)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }

    public function action_update_ativar_assinatura() {
	Funcoes::verifica_login();
        $idAssinaturas = $this->request->post('idAssinaturas');

        $model_Assinatura = new Model_Sec_Assinatura('default');
        $model_Assinatura->update_ativar_assinatura($idAssinaturas);

        $this->redirect('dashboard_sec_assinaturas/home');
    }

}

<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Dashboard_Acoes extends Controller {

    /** Chama a paginas relacionados a ação* */
    public function action_index() {
        $this->redirect('dashboard_adm/home');
    }

    public function action_lista_acoes() {
	Funcoes::verifica_login();

        $page = $this->request->query('page');

        $model_acao = new Model_Login_Acoes('user');
        $acoes = $model_acao->select_acoes();
       
        $paginator = Paginator::factory($acoes);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('lista_acoes');

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($acoes);

        $view = View::factory('adm/login/lista_acoes')
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }

    public function action_add_acao() {
	Funcoes::verifica_login();

        $model_acao = new Model_Login_Acoes('user');
        $controller = $model_acao->select_controller();

        $view = View::factory('adm/login/add_acao')
                ->set('controller', $controller)
        ;
        $this->response->body($view);
    }

    public function action_add_controller() {
	Funcoes::verifica_login();

        $view = View::factory('adm/login/add_controller');

        $this->response->body($view);
    }

    public function action_editar_dados_acao() {
	Funcoes::verifica_login();

        $idAcao = $this->request->query('idAcao');
        $page = $this->request->query('page');

        $model_acao = new Model_Login_Acoes('user');
        $acao = $model_acao->select_acao($idAcao);
        $controller = $model_acao->select_controller();

        $view = View::factory('adm/login/editar_acao')
                ->set('acao', $acao)
                ->set('page', $page)
                ->set('controller', $controller)
        ;
        $this->response->body($view);
    }
    
    public function action_verifica_idcurto() {
	Funcoes::verifica_login();

        $idCurto = $this->request->query('idCurto');

        $model_acoes = new Model_Login_Acoes('user');
        $nmcurto = $model_acoes->select_acao_idcurto($idCurto);

        if ($nmcurto[0]['idCurto'] != "") {//se retornar algum resultado
            $view = 'ok';
        } else {
            $view = '';
        }
        $this->response->body($view);
    }

    /** Executa ações relacionados a usuarios **/
    public function action_insert_acao() {
	Funcoes::verifica_login();
        
        $nmAcao = $this->request->post('nmAcao');
        $dsAcao = $this->request->post('dsAcao');
        $idCurto = $this->request->post('idCurto');
        $nmController = $this->request->post('nmController');

        $model_acao = new Model_Login_Acoes('user');
        $model_perfil = new Model_Login_Perfis('user');        
        $acoes = $model_acao->insert_acao($nmAcao, $dsAcao, $idCurto, $nmController);
        
        $idPerfil = $model_perfil->select_perfis();
        
        foreach ($idPerfil as $listaperfis) {
            $model_perfil->insert_perfil_acao($listaperfis['idPerfil'], $acoes[0], '');
        }        
   
        /* Mostra a resposta da ação */
        $warning = "Ação cadastrada com sucesso!";
        $page = $this->request->query('page');
        $acoes = $model_acao->select_acoes();
        $paginator = Paginator::factory($acoes);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('lista_acoes');

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($acoes);

        $view = View::factory('adm/login/lista_acoes')
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
                ->set('warning', $warning)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();
        
        $this->response->body($view);
    }

    public function action_insert_controller() {
	Funcoes::verifica_login();

        $nmController = $this->request->post('nmController');
        $dsController = $this->request->post('dsController');

        $model_acao = new Model_Login_Acoes('user');
        $Controller = $model_acao->select_controller_nm($nmController);

        if (count($Controller) == 0) {
                $model_acao->insert_controller($nmController, $dsController);

                /* Mostra a resposta da ação de exclusão */
                $warning = "Controller cadastrado com sucesso!";
                $page = $this->request->query('page');
                $acoes = $model_acao->select_acoes();
                $paginator = Paginator::factory($acoes);
                $paginator->set_current_page_number($page);
                $paginator->set_option_queries('lista_acoes');

                $PaginaAtual = $paginator->get_current_page_number();
                $NtotalPagina = $paginator->count();
                $total = $paginator->get_item_count($acoes);

                $view = View::factory('adm/login/lista_acoes')
                        ->set('PaginaAtual', $PaginaAtual)
                        ->set('NtotalPagina', $NtotalPagina)
                        ->set('total', $total)
                        ->set('warning', $warning)
                ;
                $view->data = $paginator;
                $view->pagination = $paginator->render();
                $this->response->body($view);
            } else {
                /* Mostra a resposta da ação de exclusão */
                $warning = "Este Controller já existe!";
                $page = $this->request->query('page');
                $acoes = $model_acao->select_acoes();
                $paginator = Paginator::factory($acoes);
                $paginator->set_current_page_number($page);
                $paginator->set_option_queries('lista_acoes');

                $PaginaAtual = $paginator->get_current_page_number();
                $NtotalPagina = $paginator->count();
                $total = $paginator->get_item_count($acoes);

                $view = View::factory('adm/login/lista_acoes')
                        ->set('PaginaAtual', $PaginaAtual)
                        ->set('NtotalPagina', $NtotalPagina)
                        ->set('total', $total)
                        ->set('warning', $warning)
                ;
                $view->data = $paginator;
                $view->pagination = $paginator->render();
                $this->response->body($view);
        }
    }

    public function action_editar_acao() {
	Funcoes::verifica_login();

        $idAcao = $this->request->post('idAcao');
        $nmAcao = $this->request->post('nmAcao');
        $dsAcao = $this->request->post('dsAcao');
        $idCurto = $this->request->post('idCurto');
        $nmController = $this->request->post('nmController');

        $model_acao = new Model_Login_Acoes('user');
        $model_acao->update_acao($idAcao, $nmAcao, $dsAcao, $idCurto, $nmController);
        
        /* Mostra a resposta da ação */
        $warning = "Ação atualizada com sucesso!";
        $page = $this->request->post('page');
        $acoes = $model_acao->select_acoes();
        $paginator = Paginator::factory($acoes);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('lista_acoes');

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($acoes);

        $view = View::factory('adm/login/lista_acoes')
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
                ->set('warning', $warning)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();    
        $this->response->body($view);
    }

    public function action_excluir_acao() {
	Funcoes::verifica_login();

        $idAcao = $this->request->query('idAcao');

        $model_acao = new Model_Login_Acoes('user');
        $model_acao->delete_perfil_acao($idAcao);
        $model_acao->delete_acao($idAcao);

        /* Mostra a resposta da ação */
        $warning = "Ação excluída com sucesso!";
        $page = $this->request->query('page');
        $acoes = $model_acao->select_acoes();
        $paginator = Paginator::factory($acoes);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('lista_acoes');

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($acoes);

        $view = View::factory('adm/login/lista_acoes')
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
                ->set('warning', $warning)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render(); 
        $this->response->body($view);
    }

    public function action_update_ativar_acao() {
	Funcoes::verifica_login();

        $idAcao = $this->request->query('idAcao');

        $model_acao = new Model_Login_Acoes('user');
        $model_acao->update_ativar_acao($idAcao);
        $status = $model_acao->select_acao($idAcao);

        if ($status[0]['ativo'] != "S") {//se retornar algum resultado
            $view = '0';
        } else {
            $view = '1';
        }
        $this->response->body($view);
    }

}

<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Dashboard_Perfis extends Controller {

    /** Chama a paginas relacionados a ação* */
    public function action_index() {
        $this->redirect('dashboard_adm/home');
    }

    public function action_lista_perfis() {
	Funcoes::verifica_login();
        
        $page = $this->request->query('page');

        $model_perfil = new Model_Login_Perfis('user');
        $perfis = $model_perfil->select_perfis();
       
        $paginator = Paginator::factory($perfis);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('lista_perfis');

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($perfis);

        $view = View::factory('adm/login/lista_perfis')
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }

    public function action_add_perfil() {
	Funcoes::verifica_login();

        $model_acao = new Model_Login_Acoes('user');
        $model_perfil = new Model_Login_Perfis('user');
        $acoes = $model_acao->select_acoes();
        $PerfilTemAcaoMenu = $model_acao->select_acoes();
        $controllers = $model_perfil->select_controller();
        $controllersMenu = $model_perfil->select_controller_menu();

        $view = View::factory('adm/login/add_perfil')
                ->set('acoes', $acoes)
                ->set('controllers', $controllers)
                ->set('PerfilTemAcaoMenu', $PerfilTemAcaoMenu)
                ->set('controllersMenu', $controllersMenu)
        ;
        $this->response->body($view);
    }
    
    public function action_editar_dados_perfil() {
	Funcoes::verifica_login();

        $idPerfil = $this->request->query('idPerfil');
        $page = $this->request->query('page');

        $model_perfil = new Model_Login_Perfis('user');
        $Perfil = $model_perfil->select_perfil($idPerfil);        
        $PerfilTemAcao = $model_perfil->select_perfil_acao($idPerfil);
        $controllers = $model_perfil->select_controller();
        $PerfilTemAcaoMenu = $model_perfil->select_perfil_acao($idPerfil);
        $controllersMenu = $model_perfil->select_controller_menu();

        $view = View::factory('adm/login/editar_perfil')
                ->set('Perfil', $Perfil)
                ->set('PerfilTemAcao', $PerfilTemAcao)
                ->set('controllers', $controllers)
                ->set('PerfilTemAcaoMenu', $PerfilTemAcaoMenu)
                ->set('controllersMenu', $controllersMenu)
                ->set('page', $page)
        ;
        $this->response->body($view);
    }

    /** Executa ações relacionados a usuarios **/
    public function action_insert_perfil() {
	Funcoes::verifica_login();

        $nmPerfil = $this->request->post('nmPerfil');
        $dsPerfil = $this->request->post('dsPerfil');
        $status = $this->request->post('status');
        
        $model_perfil = new Model_Login_Perfis('user');
        $idPerfil = $model_perfil->insert_perfil($nmPerfil, $dsPerfil, $status);
        
        $model_acao = new Model_Login_Acoes('user');
        $acoes = $model_acao->select_acoes();
        
        foreach ($acoes as $listaacoes) {
            $StatusAcao[$listaacoes['idAcao']] = $this->request->post('Acao_'.$listaacoes['idAcao']);
            $idAcao[$listaacoes['idAcao']] = $listaacoes['idAcao'];
            $model_perfil->insert_perfil_acao($idPerfil[0], $idAcao[$listaacoes['idAcao']], $StatusAcao[$listaacoes['idAcao']]);
        }

        /* Mostra a resposta da ação de exclusão */
        $warning = "Perfil cadastrado com sucesso!";
        $page = $this->request->query('page');
        $perfis = $model_perfil->select_perfis();
        $paginator = Paginator::factory($perfis);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('lista_perfis');

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($perfis);

        $view = View::factory('adm/login/lista_perfis')
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
                ->set('warning', $warning)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }

    public function action_edita_perfil() {
	Funcoes::verifica_login();

        $idPerfil = $this->request->post('idPerfil');
        $nmPerfil = $this->request->post('nmPerfil');
        $dsPerfil = $this->request->post('dsPerfil');
        
        $model_perfil = new Model_Login_Perfis('user');
        $model_perfil->update_perfil($idPerfil, $nmPerfil, $dsPerfil);
        
        $model_acao = new Model_Login_Acoes('user');
        $acoes = $model_acao->select_acoes();
        $idPerfilAt = $model_perfil->select_perfil_nm($nmPerfil);
        
        foreach ($acoes as $listaacoes) {
            $StatusAcao[$listaacoes['idAcao']] = $this->request->post('Acao_'.$listaacoes['idAcao']);
            $idAcao[$listaacoes['idAcao']] = $listaacoes['idAcao'];
            $model_perfil->update_perfil_acao($idPerfilAt[0]['idPerfil'], $idAcao[$listaacoes['idAcao']], $StatusAcao[$listaacoes['idAcao']]);
        }

        /* Mostra a resposta da ação de exclusão */
        $warning = "Perfil atualizado com sucesso!";
        $page = $this->request->query('page');
        $perfis = $model_perfil->select_perfis();
        $paginator = Paginator::factory($perfis);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('lista_perfis');

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($perfis);

        $view = View::factory('adm/login/lista_perfis')
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
                ->set('warning', $warning)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }
    
   
    public function action_visualizar_perfil() {
	Funcoes::verifica_login();

        $idPerfil = $this->request->query('idPerfil');

        $model_perfil = new Model_Login_Perfis('user');
        $Perfil = $model_perfil->select_perfil($idPerfil);        
        $PerfilTemAcao = $model_perfil->select_perfil_acao($idPerfil);
        $controllers = $model_perfil->select_controller();
        $PerfilTemAcaoMenu = $model_perfil->select_perfil_acao($idPerfil);
        $controllersMenu = $model_perfil->select_controller_menu();

        $view = View::factory('adm/login/visualizar_perfil')
                ->set('Perfil', $Perfil)
                ->set('PerfilTemAcao', $PerfilTemAcao)
                ->set('controllers', $controllers)
                ->set('PerfilTemAcaoMenu', $PerfilTemAcaoMenu)
                ->set('controllersMenu', $controllersMenu)
        ;
        $this->response->body($view);
    }
    

    public function action_excluir_Perfil() {
	Funcoes::verifica_login();

        $idPerfil = $this->request->query('idPerfil');

        $model_perfil = new Model_Login_Perfis('user');
        $model_perfil->delete_acao_perfil($idPerfil);
        $model_perfil->delete_perfil($idPerfil);

        /* Mostra a resposta da ação de exclusão */
        $warning = "Perfil excluído com sucesso!";
        $page = $this->request->query('page');
        $perfis = $model_perfil->select_perfis();
        $paginator = Paginator::factory($perfis);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('lista_perfis');

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($perfis);

        $view = View::factory('adm/login/lista_perfis')
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
                ->set('warning', $warning)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }

    public function action_update_ativar_perfil() {
	Funcoes::verifica_login();

        $idPerfil = $this->request->query('idPerfil');

        $model_perfil = new Model_Login_Perfis('user');
        $model_perfil->update_ativar_perfil($idPerfil);
        $status = $model_perfil->select_perfil($idPerfil);

        if ($status[0]['ativo'] != "S") {//se retornar algum resultado
            $view = '0';
        } else {
            $view = '1';
        }
        $this->response->body($view);
    }

}

<?php

class Controller_Dashboard_Apoio extends Controller {

    public function action_index() {
        $this->redirect('dashboard_adm/home');
    }

    public function action_busca_cidade() {
	Funcoes::verifica_login();

        $query = $this->request->query('query');

        $model_cidades = new Model_Apoio('default');
        $cidades = $model_cidades->select_busca_cidade($query);

        foreach ($cidades as $cidade) {
            $nmCidade[] = array('Cidade' => $cidade['nome'], 'idEstado' => $cidade['estado']);
        }
        echo json_encode($nmCidade);
    }
    
    public function action_busca_estado() {
	Funcoes::verifica_login();

        $query = $this->request->query('query');

        $model_estados = new Model_Apoio('default');
        $estados = $model_estados->select_busca_estado($query);

        foreach ($estados as $estado) {
            $nmEstado[] = array('Estado' => $estado['nome'], 'UF' => $estado['uf']);
        }
        echo json_encode($nmEstado);
    }

}

<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Dashboard_Scc_Dsei extends Controller {

    public function action_index() {
        $this->redirect('dashboard_scc_dsei/home');
    }

    public function action_home() {
	Funcoes::verifica_login();

        $page = $this->request->query('page');

        $model_dsei = new Model_Scc_Dsei('default');
        $dseis = $model_dsei->select_dsei();

        $paginator = Paginator::factory($dseis);
        $paginator->set_current_page_number($page);

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($dseis);

        $view = View::factory('adm/scc/dsei')
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }
    
    public function action_cadastro_dsei() {
	Funcoes::verifica_login();
        $view = View::factory('adm/scc/cadastro_dsei');
        $this->response->body($view);
    }
    
    public function action_insert_dsei() {
	Funcoes::verifica_login();
        Session::instance();
        $Nome_Dsei = $this->request->post('nome');
        $Sigla_Dsei = $this->request->post('alias');
        $status = $this->request->post('status');

        $model_dsei = new Model_Scc_Dsei('default');
        $model_dsei->insert_dsei($Nome_Dsei, $Sigla_Dsei, $status);

        /* Mostra a resposta da ação */
        $warning = "DSEI cadastrada com sucesso!";
        $page = $this->request->query('page');
        $dseis = $model_dsei->select_dsei();
        $paginator = Paginator::factory($dseis);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home');

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($dseis);
        
        $email_subject = 'Nova DSEI inserida ás '. date('H:i:s');
        $email_body = 'INserção de DSEI';
        $is_html = false;
        $message = Email::factory($email_subject, $email_body, $is_html)
                ->from('naoresponder@spdm.org.br', "Sistema")
                ->to('sergio.carvalho@spdm.org.br', "Sérgio Alexandre");
        $result = $message->send();

        $view = View::factory('adm/scc/dsei')
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
                ->set('warning', $warning)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }
    
    public function action_editar_dsei() {
	Funcoes::verifica_login();

        $page = $this->request->query('page');
        $idDsei = $this->request->query('iddsei');

        $model_dseis = new Model_Scc_Dsei('default');
        $Dsei = $model_dseis->select_dsei($idDsei);

        $view = View::factory('adm/scc/editar_dsei')
                ->set('Dsei', $Dsei)
                ->set('page', $page)
        ;

        $this->response->body($view);
    }

    public function action_update_dsei() {
        Session::instance();
        $idDsei = $this->request->post('iddsei');
        $nome_dsei = $this->request->post('nome_dsei');
        $sigla_dsei = $this->request->post('sigla_dsei');
        $Status = $this->request->post('status');

        $model_dsei = new Model_Scc_Dsei('default');
        $model_dsei->update_dsei($idDsei, $nome_dsei, $sigla_dsei, $Status);

        /* Mostra a resposta da ação */

        $warning = "DSEI atualizada com sucesso!";
        $page = $this->request->post('page');
        $dseis = $model_dsei->select_dsei();
        $paginator = Paginator::factory($dseis);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home');

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($dseis);
        
        $email_subject = 'DSEI atualizada ás '. date('H:i:s');
        $email_body = 'Atualização de DSEI';
        $is_html = false;
        $message = Email::factory($email_subject, $email_body, $is_html)
                ->from('naoresponder@spdm.org.br', "Sistema")
                ->to('sergio.carvalho@spdm.org.br', "Sérgio Alexandre");
        $result = $message->send();

        $view = View::factory('adm/scc/dsei')
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
                ->set('warning', $warning)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }

    public function action_delete_dsei() {
	Funcoes::verifica_login();

        $idDsei = $this->request->post('idDsei');

        $model_dsei = new Model_Scc_Dsei('default');
        $model_dsei->delete_dsei($idDsei);

        $this->redirect('dashboard_scc_dsei/home');
    }

    public function action_update_ativar_dsei() {
	Funcoes::verifica_login();
        $idDsei = $this->request->query('iddsei');

        $model_dsei = new Model_Scc_Dsei('default');
        $model_dsei->update_ativar_dsei($idDsei);
        $dseis = $model_dsei->select_dsei_id($idDsei);

        if ($dseis[0]['Status'] != "1") {//se retornar algum resultado
            $view = '0';
        } else {
            $view = '1';
        }
        $this->response->body($view);
    }
}

<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Dashboard_Scc_Vinculos extends Controller {

    public function action_index() {
        $this->redirect('dashboard_scc_vinculos/home');
    }

    public function action_home() {
	Funcoes::verifica_login();

        $page = $this->request->query('page');

        $model_vinculos = new Model_Scc_Vinculos('default');
        $vinculos = $model_vinculos->select_vinculos();

        $paginator = Paginator::factory($vinculos);
        $paginator->set_current_page_number($page);

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($vinculos);

        $view = View::factory('adm/scc/vinculos')
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }
    
    public function action_cadastro_vinculo() {
	Funcoes::verifica_login();
        $view = View::factory('adm/scc/cadastro_vinculo');
        $this->response->body($view);
    }

    public function action_insert_vinculo() {
	Funcoes::verifica_login();
        Session::instance();
        $Nome_vinculo = $this->request->post('nome_vinculo');
        $Sigla_vinculo = $this->request->post('sigla_vinculo');
        $status = $this->request->post('status');

        $model_vinculo = new Model_Scc_Vinculos('default');
        $model_vinculo->insert_vinculo($Nome_vinculo, $Sigla_vinculo, $status);

        /* Mostra a resposta da ação */
        $warning = "Tipo de Vinculo RH cadastrado com sucesso!";
        $page = $this->request->query('page');
        $vinculos = $model_vinculo->select_vinculos();
        $paginator = Paginator::factory($vinculos);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home');

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($vinculos);
        
        $email_subject = 'Vinculos de RH inserido ás '. date('H:i:s');
        $email_body = 'Vinculos de RH Inserido';
        $is_html = false;
        $message = Email::factory($email_subject, $email_body, $is_html)
                ->from('naoresponder@spdm.org.br', "Sistema")
                ->to('sergio.carvalho@spdm.org.br', "Sérgio Alexandre");
        $result = $message->send();

        $view = View::factory('adm/scc/vinculos')
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
                ->set('warning', $warning)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }

    public function action_editar_vinculo() {
	Funcoes::verifica_login();

        $page = $this->request->query('page');
        $idVinculo = $this->request->query('idVinculo');

        $model_vinculos = new Model_Scc_Vinculos('default');
        $vinculos = $model_vinculos->select_vinculo($idVinculo);

        $view = View::factory('adm/scc/editar_vinculo')
                ->set('vinculos', $vinculos)
                ->set('page', $page)
        ;

        $this->response->body($view);
    }

    public function action_update_vinculo() {
        Session::instance();
        $idVinculo = $this->request->post('idVinculo');
        $Nome_vinculo = $this->request->post('nome_vinculo');
        $Sigla_vinculo = $this->request->post('sigla_vinculo');
        $Status = $this->request->post('status');

        $model_vinculo = new Model_Scc_Vinculos('default');
        $model_vinculo->update_vinculo($idVinculo, $Nome_vinculo, $Sigla_vinculo, $Status);

        /* Mostra a resposta da ação */

        $warning = "Tipo de Vinculo RH atualizado com sucesso!";
        $page = $this->request->post('page');
        $vinculos = $model_vinculo->select_vinculos();
        $paginator = Paginator::factory($vinculos);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home');

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($vinculos);
        
        $email_subject = 'Vinculos de RH atualizado ás '. date('H:i:s');
        $email_body = 'Vinculos de RH Atualizado';
        $is_html = false;
        $message = Email::factory($email_subject, $email_body, $is_html)
                ->from('naoresponder@spdm.org.br', "Sistema")
                ->to('sergio.carvalho@spdm.org.br', "Sérgio Alexandre");
        $result = $message->send();

        $view = View::factory('adm/scc/vinculos')
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
                ->set('warning', $warning)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }

    public function action_delete_vinculo() {
	Funcoes::verifica_login();

        $idVinculo = $this->request->post('idVinculo');

        $model_vinculo = new Model_Scc_Vinculos('default');
        $model_vinculo->delete_vinculo($idVinculo);

        $this->redirect('dashboard_scc_vinculos/home');
    }

    public function action_update_ativar_vinculo() {
	Funcoes::verifica_login();
        $idVinculo = $this->request->query('idVinculo');

        $model_vinculo = new Model_Scc_Vinculos('default');
        $model_vinculo->update_ativar_vinculo($idVinculo);
        $vinculos = $model_vinculo->select_vinculo($idVinculo);

        if ($vinculos[0]['Status'] != "1") {//se retornar algum resultado
            $view = '0';
        } else {
            $view = '1';
        }
        $this->response->body($view);
    }
}

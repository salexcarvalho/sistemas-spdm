<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Dashboard_Scc_Unidades extends Controller {

    public function action_index() {
        $this->redirect('dashboard_scc_unidades/home');
    }

    public function action_home() {
	Funcoes::verifica_login();

        $page = $this->request->query('page');

        $model_unidades = new Model_Scc_Unidades('default');
        $unidades = $model_unidades->select_unidades();

        $paginator = Paginator::factory($unidades);
        $paginator->set_current_page_number($page);

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($unidades);

        $view = View::factory('adm/scc/unidades')
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }
    
    public function action_cadastro_unidade() {
	Funcoes::verifica_login();
        
        $model_vinculos = new Model_Scc_Vinculos('default');
        $vinculos = $model_vinculos->select_vinculos();
        
        $view = View::factory('adm/scc/cadastro_unidade')
                ->set('vinculos', $vinculos);
        $this->response->body($view);
    }

    public function action_insert_unidade() {
	Funcoes::verifica_login();
        Session::instance();
        $Nome_unidade = $this->request->post('nome_unidade');
        $Sigla_unidade = $this->request->post('sigla_unidade');
        $idVinculo = $this->request->post('vinculo_solicitante');
        $status = $this->request->post('status');

        $model_unidade = new Model_Scc_Unidades('default');
        $model_unidade->insert_unidade($Nome_unidade, $Sigla_unidade, $idVinculo, $status);

        /* Mostra a resposta da ação */
        $warning = "Tipo de Unidade RH cadastrada com sucesso!";
        $page = $this->request->query('page');
        $unidades = $model_unidade->select_unidades();
        $paginator = Paginator::factory($unidades);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home');

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($unidades);
        
        $email_subject = 'Unidade de RH inserida ás '. date('H:i:s');
        $email_body = 'Unidade de RH Inserida';
        $is_html = false;
        $message = Email::factory($email_subject, $email_body, $is_html)
                ->from('naoresponder@spdm.org.br', "Sistema")
                ->to('sergio.carvalho@spdm.org.br', "Sérgio Alexandre");
        $result = $message->send();

        $view = View::factory('adm/scc/unidades')
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
                ->set('warning', $warning)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }

    public function action_editar_unidade() {
	Funcoes::verifica_login();

        $page = $this->request->query('page');
        $idUnidade = $this->request->query('idUnidade');

        $model_unidades = new Model_Scc_Unidades('default');
        $unidades = $model_unidades->select_unidade($idUnidade);
        $model_vinculos = new Model_Scc_Vinculos('default');
        $vinculos = $model_vinculos->select_vinculos();

        $view = View::factory('adm/scc/editar_unidade')
                ->set('unidades', $unidades)
                ->set('vinculos', $vinculos)
                ->set('page', $page)
        ;

        $this->response->body($view);
    }

    public function action_update_unidade() {
        Session::instance();
        $idUnidade = $this->request->post('idUnidade');
        $Nome_unidade = $this->request->post('nome_unidade');
        $Sigla_unidade = $this->request->post('sigla_unidade');
        $idVinculo = $this->request->post('vinculo_solicitante');
        $Status = $this->request->post('status');

        $model_unidade = new Model_Scc_Unidades('default');
        $model_unidade->update_unidade($idUnidade, $Nome_unidade, $Sigla_unidade, $idVinculo, $Status);

        /* Mostra a resposta da ação */

        $warning = "Tipo de Unidade RH atualizada com sucesso!";
        $page = $this->request->post('page');
        $unidades = $model_unidade->select_unidades();
        $paginator = Paginator::factory($unidades);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home');

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($unidades);
        
        $email_subject = 'Unidade de RH atualizada ás '. date('H:i:s');
        $email_body = 'Unidade de RH Atualizada';
        $is_html = false;
        $message = Email::factory($email_subject, $email_body, $is_html)
                ->from('naoresponder@spdm.org.br', "Sistema")
                ->to('sergio.carvalho@spdm.org.br', "Sérgio Alexandre");
        $result = $message->send();

        $view = View::factory('adm/scc/unidades')
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
                ->set('warning', $warning)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }

    public function action_delete_unidade() {
	Funcoes::verifica_login();

        $idUnidade = $this->request->post('idUnidade');

        $model_unidade = new Model_Scc_Unidades('default');
        $model_unidade->delete_unidade($idUnidade);

        $this->redirect('dashboard_scc_unidades/home');
    }

    public function action_update_ativar_unidade() {
	Funcoes::verifica_login();
        $idUnidade = $this->request->query('idUnidade');

        $model_unidade = new Model_Scc_Unidades('default');
        $model_unidade->update_ativar_unidade($idUnidade);
        $unidades = $model_unidade->select_unidade($idUnidade);

        if ($unidades[0]['Status'] != "1") {//se retornar algum resultado
            $view = '0';
        } else {
            $view = '1';
        }
        $this->response->body($view);
    }
}

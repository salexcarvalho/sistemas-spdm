<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Dashboard_Scc_Relatos extends Controller {

    public function action_index() {
        $this->redirect('dashboard_scc_relatos/home');
    }

    public function action_home() {
        Funcoes::verifica_login();
        
        $page = $this->request->query('page');
        $StatusAtendimento = $this->request->query('StatusAtendimento');
        
        if (($StatusAtendimento == 'all') or ($StatusAtendimento == '')) {
            $StatusAtendimento =  NULL;
        }
        
        $option = 'StatusAtendimento=' . $StatusAtendimento;

        $model_relatos = new Model_Scc_Relato('default');
        $relatos = $model_relatos->select_relatos($StatusAtendimento);
        
        $paginator = Paginator::factory($relatos);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home', 'page', $option);

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($relatos);

        $view = View::factory('adm/scc/home')
                ->set('StatusAtendimento', $StatusAtendimento)
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }
    
    public function action_visualizar_relato() {
	Funcoes::verifica_login();

        $page = $this->request->post('page');
        //$idRelato = $this->request->query('idRelato');
        
        $idRelato = (($this->request->post('idRelato')))? $this->request->post('idRelato'): $this->request->query('idRelato');
        
        $model_relatos = new Model_Scc_Relato('default');
        $relato = $model_relatos->select_relato($idRelato);
        $anexo = $model_relatos->select_anexos($relato[0]['idProtocolo']);
        $resposta = $model_relatos->select_resposta($relato[0]['idProtocolo']);
        
        //Funcoes::dump($resposta);
        
        $model_vinculos = new Model_Scc_Vinculos('default');
        $vinculos = $model_vinculos->select_vinculos();
                
        $view = View::factory('adm/scc/visualizar_relato')
                ->set('relato', $relato)
                ->set('resposta', $resposta)
                ->set('anexo', $anexo)
                ->set('vinculos', $vinculos)
                ->set('page', $page)
        ;
        
        $this->response->body($view);
    }
    
    public function action_visualizar_relato_protocolo() {
	Funcoes::verifica_login();

        $page = $this->request->post('page');
        $idProtocolo = $this->request->post('idProtocolo');
        
        $model_relatos = new Model_Scc_Relato('default');
        $relato = $model_relatos->select_relato_protocolo($idProtocolo);
        $anexo = $model_relatos->select_anexos($relato[0]['idProtocolo']);
        $resposta = $model_relatos->select_resposta($relato[0]['idProtocolo']);
        
        //Funcoes::dump($resposta);
        
        $model_vinculos = new Model_Scc_Vinculos('default');
        $vinculos = $model_vinculos->select_vinculos();
                
        $view = View::factory('adm/scc/visualizar_relato')
                ->set('relato', $relato)
                ->set('resposta', $resposta)
                ->set('anexo', $anexo)
                ->set('vinculos', $vinculos)
                ->set('page', $page)
        ;
        
        $this->response->body($view);
    }
    
    public function action_abrir_pdf() {
	Funcoes::verifica_login();

        $page = $this->request->post('page');
        $idRelato = $this->request->post('idRelato');
        
        $model_relatos = new Model_Scc_Relato('default');
        $relato = $model_relatos->select_relato($idRelato);
        $anexo = $model_relatos->select_anexos($relato[0]['idProtocolo']);
        $resposta = $model_relatos->select_resposta($relato[0]['idProtocolo']);
        
        //Funcoes::dump($resposta);
        
        $model_vinculos = new Model_Scc_Vinculos('default');
        $vinculos = $model_vinculos->select_vinculos();
        
        $view = View::factory('adm/scc/gerar_relato_pdf')
                ->set('relato', $relato)
                ->set('resposta', $resposta)
                ->set('anexo', $anexo)
                ->set('vinculos', $vinculos)
                ->set('page', $page)
                ->render();
        
        $nome = 'Relatorio_ID_' . $relato[0]['idProtocolo'] . '.pdf';
        
        $conteudo = Request::factory($view);        
          
        $html2pdf = new HTML2PDF('P', 'A4', Kohana::$config->load('html2pdf')->get('langue'),true,'UTF-8',array('5', '5', '5', '5'));        
        $html2pdf->pdf->SetAuthor('TI Presidência da SPDM');
        $html2pdf->pdf->SetTitle('Relato do Canal Confidencial - SPDM');
      
        $content = $conteudo->uri();      
              
        $html2pdf->writeHTML($content,false);
        $html2pdf->Output($nome, 'I');
        
    }
    
    public function action_responder_relato() {
	Funcoes::verifica_login();

        $page = $this->request->post('page');
        $var = $this->request->post();
        $StatusAtendimento = $this->request->query('StatusAtendimento');
        
        if (($StatusAtendimento == 'all') or ($StatusAtendimento == '')) {
            $StatusAtendimento =  NULL;
        }
        
        $option = 'StatusAtendimento=' . $StatusAtendimento;
        
        $model_relatos = new Model_Scc_Relato('default');
        
        if ($var['acaorelato'] == 'responder_relato') {
            
            $resposta = $var['resposta_relato'];
            $resposta = $model_relatos->insert_resposta($var, $resposta);
            $Status = '2';
            $model_relatos->update_protocolo($var, $Status);
            
            // * Inserindo Anexo *
            $anexos = $_FILES['anexo'];
            $img_desc = $this->reArrayFiles($anexos);
            $i = 1; 
            foreach($img_desc as $val)
            {
                $ext = pathinfo($val['name'], PATHINFO_EXTENSION);
                $newname = 'Adm_' .date('Hms',time()) . '_' . $var['idProtocolo'] . '_Anexo_' .$i. '.'. $ext;
                $envia = Upload::save($val, $newname, 'upload/scc');
                if($envia != false){
                    $model_solicitacao = new Model_Scc_Solicitacao('default');
                    $anexar = $model_solicitacao->insert_anexo($var['idProtocolo'], $newname, $resposta[0]);
                }
                $i++;
            }
            
            $warning = "Relato ID: " . $var['idProtocolo'] . " foi respondido com sucesso!";
            
            //Envio da resposta
            if ($var['email_solicitante'] != "") {
                $viewemail = View::factory('scc/corpo_email_respostarh')
                        ->set('Protocolo', $var['idProtocolo'])
                        ->set('var', $var)
                        ->set('warning', $warning)
                    ->render();

                $email_subject = 'Canal Confidencial  - Dados do relato nº'. $var['idProtocolo'];
                $email_body = $viewemail;
                $is_html = true;
                $message = Email::factory($email_subject, $email_body, $is_html)
                        //->from($_SESSION['EmailUsuario'], $_SESSION['NomeUsuario'])
                        ->from('naoresponder@spdm.org.br', 'Canal Confidencial')
                        ->bcc('sergio.carvalho@spdm.org.br', 'Sergio Alexandre')
                        ->to($var['email_solicitante'], $var['nome_solicitante']);
                $result = $message->send();
            }
        } elseif ($var['acaorelato'] == 'concluir_relato') {
            $resposta = $var['concluir_relato'];
            $resposta = $model_relatos->insert_resposta($var, $resposta);
            $Status = '3';
            $model_relatos->update_protocolo($var, $Status);
            
            // * Inserindo Anexo *
            $anexos = $_FILES['anexo'];
            $img_desc = $this->reArrayFiles($anexos);
            $i = 1; 
            foreach($img_desc as $val)
            {
                $ext = pathinfo($val['name'], PATHINFO_EXTENSION);
                $newname = 'Adm_' .date('Hms',time()) . '_' . $var['idProtocolo'] . '_Anexo_' .$i. '.'. $ext;
                $envia = Upload::save($val, $newname, 'upload/scc');
                if($envia != false){
                    $model_solicitacao = new Model_Scc_Solicitacao('default');
                    $anexar = $model_solicitacao->insert_anexo($var['idProtocolo'], $newname, $resposta[0]);
                }
                $i++;
            }
            
            $warning = "Relato ID: " . $var['idProtocolo'] . " foi concluído com sucesso!";
            
            //Envio da resposta
            if ($var['email_solicitante'] != "") {
                $viewemail = View::factory('scc/corpo_email_respostarh')
                        ->set('Protocolo', $var['idProtocolo'])
                        ->set('var', $var)
                        ->set('warning', $warning)
                    ->render();

                $email_subject = 'Canal Confidencial  - Dados do relato nº'. $var['idProtocolo'];
                $email_body = $viewemail;
                $is_html = true;
                $message = Email::factory($email_subject, $email_body, $is_html)
                        //->from($_SESSION['EmailUsuario'], $_SESSION['NomeUsuario'])
                        ->from('naoresponder@spdm.org.br', 'Canal Confidencial')
                        ->bcc('sergio.carvalho@spdm.org.br', 'Sergio Alexandre')
                        ->to($var['email_solicitante'], $var['nome_solicitante']);
                $result = $message->send();
            }
        } elseif ($var['acaorelato'] == 'reencaminhar_relato') {
            
            $model_relatos->update_solicitante($var);
            $Status = '2';
            $model_relatos->update_protocolo($var, $Status);
            $resposta = $var['reencaminhar_relato'];
            $resposta = $model_relatos->insert_resposta($var, $resposta);
                        
            $warning = "Relato ID: " . $var['idProtocolo'] . " foi reencaminhado com sucesso!";
            
            //Envio da resposta reencaminhada
            $idTipoSetor = $var['vinculo_solicitante'];
            $model_usuarios = new Model_Login_Usuario('user');
            $usuarios = $model_usuarios->select_usuarios_setor($idTipoSetor);
            
            foreach ($usuarios as $usuario) {
                $RespostaAcao = "<h3>O Relato ID: " . $var['idProtocolo']. ", foi reencaminhado.</h3> <h4>Por favor acesse o Painel do Sistema para mais informações!</h4>";
                $viewemail = View::factory('scc/corpo_email_reencaminhado')
                        ->set('Protocolo', $var['idProtocolo'])
                        ->set('var', $var)
                        ->set('RespostaAcao', $RespostaAcao)
                    ->render();

                $email_subject = 'Canal Confidencial  - Dados do relato nº'. $var['idProtocolo'];
                $email_body = $viewemail;
                $is_html = true;
                $message = Email::factory($email_subject, $email_body, $is_html)
                        //->from($_SESSION['EmailUsuario'], $_SESSION['NomeUsuario'])
                        ->from('naoresponder@spdm.org.br', 'Canal Confidencial')
                        ->bcc('sergio.carvalho@spdm.org.br', 'Sergio Alexandre')
                        ->to($usuario['email'], $usuario['nmNome']);
                $result = $message->send();
            }
            
        } elseif ($var['acaorelato'] == 'encerrar_relato') {
            
            $Status = '4';
            $model_relatos->update_protocolo($var, $Status);
            
            $warning = "Este relato foi encerrado!";
            
            //Envio da resposta
            if ($var['email_solicitante'] != "") {
                $viewemail = View::factory('scc/corpo_email_encerramento')
                        ->set('Protocolo', $var['idProtocolo'])
                        ->set('var', $var)
                        ->set('warning', $warning)
                    ->render();

                $email_subject = 'Canal Confidencial  - Dados do relato nº'. $var['idProtocolo'];
                $email_body = $viewemail;
                $is_html = true;
                $message = Email::factory($email_subject, $email_body, $is_html)
                        //->from($_SESSION['EmailUsuario'], $_SESSION['NomeUsuario'])
                        ->from('naoresponder@spdm.org.br', 'Canal Confidencial')
                        ->bcc('sergio.carvalho@spdm.org.br', 'Sergio Alexandre')
                        ->to($var['email_solicitante'], $var['nome_solicitante']);
                        //->to('webmaster@spdm.org.br', 'Canal Confidencial');
                $result = $message->send();
            }
            
        }
        $relatos = $model_relatos->select_relatos();
        
        $paginator = Paginator::factory($relatos);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home', 'page', $option);

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($relatos);

        $view = View::factory('adm/scc/home')
                ->set('StatusAtendimento', $StatusAtendimento)
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
                ->set('warning', $warning)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }
    
    public function reArrayFiles($file)
    {
        $file_ary = array();
        $file_count = count($file['name']);
        $file_key = array_keys($file);

        for($i=0;$i<$file_count;$i++)
        {
            foreach($file_key as $val)
            {
                $file_ary[$i][$val] = $file[$val][$i];
            }
        }
        return $file_ary;
    }
    
    public function action_encerrar_chamado() {
	        
        $model_relatos = new Model_Scc_Relato('default');
        $relatos = $model_relatos->select_relatos_encerrar();
        
        if (count($relatos) > 0) {
            foreach ($relatos as $relato) :
                $model_relatos->update_cron($relato['idProtocolo'], 4);
            endforeach;
        }
        
        $email_subject = 'Cron Executado com sucesso ás '. date('H:i:s');
        $email_body = 'Cron executado!';
        $is_html = false;
        $message = Email::factory($email_subject, $email_body, $is_html)
                ->from('naoresponder@spdm.org.br', "Sistema")
                ->to('sergio.carvalho@spdm.org.br', "Sérgio Alexandre");
        $result = $message->send();
        
        return;
        
    }    
}

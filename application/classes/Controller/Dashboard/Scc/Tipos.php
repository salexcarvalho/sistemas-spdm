<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Dashboard_Scc_Tipos extends Controller {

    public function action_index() {
        $this->redirect('dashboard_scc_tipos/home');
    }

    public function action_home() {
	Funcoes::verifica_login();

        $page = $this->request->query('page');

        $model_tipos = new Model_Scc_Tipos('default');
        $tipos = $model_tipos->select_tipos();

        $paginator = Paginator::factory($tipos);
        $paginator->set_current_page_number($page);

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($tipos);

        $view = View::factory('adm/scc/tipos')
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }
    
    public function action_cadastro_tipo() {
	Funcoes::verifica_login();
        $view = View::factory('adm/scc/cadastro_tipo');
        $this->response->body($view);
    }

    public function action_insert_tipo() {
	Funcoes::verifica_login();
        Session::instance();
        $Nome_tiporelato = $this->request->post('nome_tiporelato');
        $Sigla_tiporelato = $this->request->post('sigla_tiporelato');
        $status = $this->request->post('status');

        $model_tipo = new Model_Scc_Tipos('default');
        $model_tipo->insert_tipo($Nome_tiporelato, $Sigla_tiporelato, $status);

        /* Mostra a resposta da ação */
        $warning = "Tipo de Relato cadastrado com sucesso!";
        $page = $this->request->query('page');
        $tipos = $model_tipo->select_tipos();
        $paginator = Paginator::factory($tipos);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home');

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($tipos);
        
        $email_subject = 'Tipos de Relato inserido ás '. date('H:i:s');
        $email_body = 'Tipos de Relato Inserido';
        $is_html = false;
        $message = Email::factory($email_subject, $email_body, $is_html)
                ->from('naoresponder@spdm.org.br', "Sistema")
                ->to('sergio.carvalho@spdm.org.br', "Sérgio Alexandre");
        $result = $message->send();

        $view = View::factory('adm/scc/tipos')
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
                ->set('warning', $warning)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }

    public function action_editar_tipo() {
	Funcoes::verifica_login();

        $page = $this->request->query('page');
        $idTipoRelato = $this->request->query('idTipoRelato');

        $model_tipos = new Model_Scc_Tipos('default');
        $tipos = $model_tipos->select_tipo($idTipoRelato);

        $view = View::factory('adm/scc/editar_tipo')
                ->set('tipos', $tipos)
                ->set('page', $page)
        ;

        $this->response->body($view);
    }

    public function action_update_tipo() {
        Session::instance();
        $idTipoRelato = $this->request->post('idTipoRelato');
        $Nome_tiporelato = $this->request->post('nome_tiporelato');
        $Sigla_tiporelato = $this->request->post('sigla_tiporelato');
        $Status = $this->request->post('status');

        $model_tipo = new Model_Scc_Tipos('default');
        $model_tipo->update_tipo($idTipoRelato, $Nome_tiporelato, $Sigla_tiporelato, $Status);

        /* Mostra a resposta da ação */

        $warning = "Tipo de Relato atualizado com sucesso!";
        $page = $this->request->post('page');
        $tipos = $model_tipo->select_tipos();
        $paginator = Paginator::factory($tipos);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home');

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($tipos);
        
        $email_subject = 'Tipos de Relato atualizado ás '. date('H:i:s');
        $email_body = 'Tipos de Relato Atualizado';
        $is_html = false;
        $message = Email::factory($email_subject, $email_body, $is_html)
                ->from('naoresponder@spdm.org.br', "Sistema")
                ->to('sergio.carvalho@spdm.org.br', "Sérgio Alexandre");
        $result = $message->send();

        $view = View::factory('adm/scc/tipos')
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
                ->set('warning', $warning)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }

    public function action_delete_tipo() {
	Funcoes::verifica_login();

        $idTipoRelato = $this->request->post('idTipoRelato');

        $model_tipo = new Model_Scc_Tipos('default');
        $model_tipo->delete_tipo($idTipoRelato);

        $this->redirect('dashboard_sec_tipos/home');
    }

    public function action_update_ativar_tipo() {
	Funcoes::verifica_login();
        $idTipoRelato = $this->request->query('idTipoRelato');

        $model_tipo = new Model_Scc_Tipos('default');
        $model_tipo->update_ativar_tipo($idTipoRelato);
        $tipos = $model_tipo->select_tipo($idTipoRelato);

        if ($tipos[0]['Status'] != "1") {//se retornar algum resultado
            $view = '0';
        } else {
            $view = '1';
        }
        $this->response->body($view);
    }
}

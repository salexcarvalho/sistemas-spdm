<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Dashboard_Setor extends Controller {

    public function action_index() {
        $this->redirect('dashboard_adm/home');
    }

    public function action_home() {
	Funcoes::verifica_login();

        $page = $this->request->query('page');

        $model_tipos = new Model_Login_Setor('user');
        $tipos = $model_tipos->select_tipo();

        $paginator = Paginator::factory($tipos);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home');

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($tipos);

        $view = View::factory('adm/login/lista_setor')
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }
   
    
    public function action_cadastro_tipo() {
	Funcoes::verifica_login();
        $view = View::factory('adm/login/cadastro_tipo_setor');
        $this->response->body($view);
    }
    
    
    public function action_insert_tipo() {
	Funcoes::verifica_login();
        
        Session::instance();
        $Setor = $this->request->post('Setor');
        $Descricao = $this->request->post('Descricao');
        $status = $this->request->post('status');

        $model_tipos = new Model_Login_Setor('user');
        $model_tipos->insert_tipo($Setor, $Descricao, $status);

        /* Mostra a resposta da ação */
        $warning = "Tipo de setor cadastrado com sucesso!";
        $page = $this->request->query('page');
        $tipos = $model_tipos->select_tipo();
        $paginator = Paginator::factory($tipos);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home');

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($tipos);

        $view = View::factory('adm/login/lista_setor')
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
                ->set('warning', $warning)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }
    
    
    public function action_editar_tipo() {
	Funcoes::verifica_login();

        $page = $this->request->query('page');
        $idTipoSetor = $this->request->query('idTipoSetor');

        $model_tipos = new Model_Login_Setor('user');
        $tipos = $model_tipos->select_tipo_id($idTipoSetor);

        $view = View::factory('adm/login/editar_tipo_setor')
                ->set('tipos', $tipos)
                ->set('page', $page)
        ;

        $this->response->body($view);
    }

    
    public function action_update_tipo() {
        Session::instance();
        $idTipoSetor = $this->request->post('idTipoSetor');
        $Setor = $this->request->post('Setor');
        $Descricao = $this->request->post('Descricao');
        $Status = $this->request->post('status');

        $model_tipos = new Model_Login_Setor('user');
        $model_tipos->update_tipo($idTipoSetor, $Setor, $Descricao, $Status);

        $warning = "Tipo de setor atualizado com sucesso!";
        $page = $this->request->post('page');
        $tipos = $model_tipos->select_tipo();
        $paginator = Paginator::factory($tipos);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home');

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($tipos);

        $view = View::factory('adm/login/lista_setor')
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
                ->set('warning', $warning)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }

    
    public function action_delete_tipo() {
	Funcoes::verifica_login();

        $idTipoSetor = $this->request->post('idTipoSetor');

        $model_tipos = new Model_Login_Setor('user');
        $model_tipos->delete_tipo($idTipoSetor);

        $this->redirect('dashboard_setor/home');
    }

    
    public function action_update_ativar_tipo() {
	Funcoes::verifica_login();
        
        $idTipoSetor = $this->request->query('idTipoSetor');

        $model_tipos = new Model_Login_Setor('user');
        $model_tipos->update_ativar_tipo($idTipoSetor);
        $tipos = $model_tipos->select_tipo_id($idTipoSetor);

        if ($tipos[0]['Status'] != "1") {//se retornar algum resultado
            $view = '0';
        } else {
            $view = '1';
        }
        $this->response->body($view);
    }

}

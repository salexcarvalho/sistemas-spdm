<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Dashboard_Csm_Home extends Controller
{

    public function action_index()
    {
        $this->redirect('dashboard_csm_servico/lista');
    }

    public function action_update_ativar_endereco()
    {
	Funcoes::verifica_login();

        $idEndereco = $this->request->post('idEndereco');

        $model_endereco = new Model_Csm_Endereco('default');
        $model_endereco->update_ativar_endereco($idEndereco);

        $this->action_check_dados();
    }

    public function action_update_ativar_contato()
    {
	Funcoes::verifica_login();

        $idContato = $this->request->post('idContato');

        $model_contato = new Model_Csm_Contato('default');

        $model_contato->update_ativar_contato($idContato);

        $this->action_check_dados();
    }

    

    public function action_update_ativar_funcionario()
    {
	Funcoes::verifica_login();

        $idFuncionario = $this->request->post('idFuncionario');

        $model_funcionario = new Model_Csm_Funcionario('default');

        $model_funcionario->update_ativar_funcionario($vars);

        $this->action_check_dados();
    }

    

    public function action_add_endereco()
    {
	Funcoes::verifica_login();

        $view = View::factory('adm/csm/add_endereco')
        ;
        $this->response->body($view);
    }

    public function action_cadastro_contato()
    {
	Funcoes::verifica_login();

        $view = View::factory('adm/csm/cadastro_contato')
        ;
        $this->response->body($view);
    }

    public function action_add_servico_funcionario()
    {
	Funcoes::verifica_login();

        $model_servico = new Model_Csm_Servico('default');
        $model_funcionario = new Model_Csm_Funcionario('default');

        $servicos = $model_servico->select_servicos();
        $funcionarios = $model_funcionario->select_funcionarios();

        $view = View::factory('adm/csm/add_servico_funcionario')
                ->set('servicos', $servicos)
                ->set('funcionarios', $funcionarios)
        ;
        $this->response->body($view);
    }

    public function action_insert_servico_funcionario()
    {
	Funcoes::verifica_login();

        $idServico = $this->request->post('servicoper');
        $idFuncionarios = $this->request->post('funcionariosper');

        $model_servico = new Model_Csm_Servico('default');
        $model_funcionario = new Model_Csm_Funcionario('default');
        $model_servico_funcionario = new Model_Csm_ServicoTemFuncionario('default');

        $servicos = $model_servico->select_servicos();
        $funcionarios = $model_funcionario->select_funcionarios();

        if (isset($idServico) && $idServico != NULL && isset($idFuncionarios) && $idFuncionarios != NULL)
        {
            foreach ($idFuncionarios as $idFuncionario)
            {
                $model_servico_funcionario->insert_servico_funcionario($idServico, $idFuncionario);
            }

            $warning = 'Formulário enviado com sucesso.';
        }
        else
        {
            $errors = 'Formulário apresenta erros.';
        }
        $this->response->body(View::factory('adm/csm/add_servico_funcionario')
                        ->set('servicos', $servicos)
                        ->set('funcionarios', $funcionarios)
                        ->bind('warning', $warning)
                        ->bind('errors', $errors)
        );
    }

    public function action_add_horarioatd()
    {
	Funcoes::verifica_login();

        $view = View::factory('adm/csm/add_horarioatd')
        ;
        $this->response->body($view);
    }
    
    
}

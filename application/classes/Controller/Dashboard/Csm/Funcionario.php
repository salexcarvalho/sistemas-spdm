<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Dashboard_Csm_Funcionario extends Controller {

    public function action_index() {
        $this->redirect('dashboard_csm_funcionario/lista');
    }

    public function action_lista() {
	Funcoes::verifica_login();

        $model_funcionario = new Model_Csm_Funcionario('default');

        $funcionarios = $model_funcionario->select_funcionarios();

        $view = View::factory('adm/csm/funcionario')
                ->set('funcionarios', $funcionarios)
        ;
        $this->response->body($view);
    }

    public function action_visualizar_funcionario()
    {
	Funcoes::verifica_login();
        
        $vars = $this->request->post();

        $model_funcionario = new Model_Csm_Funcionario('default');
        $model_imagem = new Model_Csm_Imagem('default');
        $model_tipo_funcionario = new Model_Csm_TipoFuncionario('default');

        $funcionario = $model_funcionario->select_funcionario($vars);
        $imagens = $model_imagem->select_imagem_por_funcionario($vars);

        $tipoFuncionarios = $model_tipo_funcionario->select_tipo_funcionarios();

        $view = View::factory('adm/csm/visualizar_funcionario')
                ->set('tipoFuncionarios', $tipoFuncionarios)
                ->set('funcionario', $funcionario)
                ->set('imagens', $imagens)
        ;
        $this->response->body($view);
    }

    public function action_editar_funcionario() {
	Funcoes::verifica_login();

        $vars = $this->request->post();

        $model_funcionario = new Model_Csm_Funcionario('default');
        $model_imagem = new Model_Csm_Imagem('default');
        $model_tipo_funcionario = new Model_Csm_TipoFuncionario('default');
        
        

        $funcionario = $model_funcionario->select_funcionario($vars);
        $imagens = $model_imagem->select_imagem_por_funcionario($vars);
        $tipoFuncionarios = $model_tipo_funcionario->select_tipo_funcionarios();


        
        $ft['ft1'] = NULL;
        $ft['ft2'] = NULL;

        
        foreach ($imagens as $imagem)
        {
            $ex = explode('_', $imagem['nmImagem']);
            $en = end($ex);
            $ex2 = explode('.', $en);

            if ($ex2[0] == 'ft1') {
                $ft['ft1'] = $imagem;
            } else {
                $ft['ft2'] = $imagem;
            }
        }
        
        echo '<pre>';
        var_dump($ft);
        echo '</pre>';
        exit;


        $view = View::factory('adm/csm/editar_funcionario')
                ->set('funcionario', $funcionario)
                ->set('imagens', $imagens)
                ->set('tipoFuncionarios', $tipoFuncionarios)
                ->set('ft', $ft)
        ;
        $this->response->body($view);
    }

    public function action_delete_funcionario()
        {
	Funcoes::verifica_login();
        $vars = $this->request->post();
        

        if (($vars['novoIdFuncionario'] == NULL) || (empty($vars['novoIdFuncionario']))) {
            $vars['novoIdFuncionario'] = "";
        }

        $model_funcionario = new Model_Csm_Funcionario('default');
        $model_departamento = new Model_Csm_Departamento('default');
        $model_especialidade = new Model_Csm_Especialidade('default');
        $model_especialidade_funcionario = new Model_Csm_EspecialidadeTemFuncionario('default');
        $model_imagem = new Model_Csm_Imagem('default');
        $model_servico = new Model_Csm_Servico('default');
        $model_servico_funcionario = new Model_Csm_ServicoTemFuncionario('default');
        $model_contato = new Model_Csm_Contato('default');
        
        $model_servico->update_servico_funcionario($vars);
        $model_departamento->update_departamento_funcionario($vars);
        $model_especialidade->update_especialidade_funcionario($vars);
        $model_especialidade_funcionario->delete_especialidade_funcionario($vars);
        $model_imagem->delete_imagem_por_funcionario($vars);
        $model_servico_funcionario->delete_servico_funcionario($vars);
        $model_funcionario->delete_funcionario($vars);
        $model_contato->delete_contato($vars);
    }

    public function action_cadastro_funcionario() {
	Funcoes::verifica_login();

        $model_tipo_funcionario = new Model_Csm_TipoFuncionario('default');
        $tipoFuncionarios = $model_tipo_funcionario->select_tipo_funcionarios();
        $funcionario = $this->request->post('funcionario');
        $view = View::factory('adm/csm/cadastro_funcionario')
                ->set('tipoFuncionarios', $tipoFuncionarios)
                ->bind('funcionario', $funcionario)
        ;
        $this->response->body($view);
    }

    public function action_update_ativar_funcionario() {
	Funcoes::verifica_login();

        $idFuncionario = $this->request->post('idFuncionario');

        $model_funcionario = new Model_Csm_Funcionario('default');

        $model_funcionario->update_ativar_funcionario($idFuncionario);

    }

    public function action_insert_funcionario() {
	Funcoes::verifica_login();

        $vars = $this->request->post();

        $model_funcionario = new Model_Csm_Funcionario('default');
        $model_tipo_funcionario = new Model_Csm_TipoFuncionario('default');
        $model_contato = new Model_Csm_Contato('default');
        $model_imagem = new Model_Csm_Imagem('default');

        $usuario = Auth::instance()->get_user();

        //validação
        $validation = Validation::factory($this->request->post())
                ->rule('nmFuncionario', 'not_empty');

        $res_idFuncionario = NULL;
        $res_idContato = NULL;
        $delete_return_contato = NULL;

        if ($validation->check())
        {
            try 
            {
                
                $res_idContato = $model_contato->insert_contato($vars);
                $res_idFuncionario = $model_funcionario->insert_funcionario($vars);

                // upload
                $model_imagem->upload_imagem('funcionario', $res_idFuncionario[0], 'ft1', $dsImagem1);
                $model_imagem->upload_imagem('funcionario', $res_idFuncionario[0], 'ft2', $dsImagem2);
                // termino upload
                
                if ($res_idFuncionario) 
                {
                    $warning = 'Funcionário cadastrado com sucesso.';
                }
            } catch (ExCEPtion $ex) 
            {
                Kohana_ExCEPtion::handler($ex);
                $errors = $validation->errors('', 'br');
            }
        }            

            $funcionarios = $model_funcionario->select_funcionarios();
            $this->response->body(View::factory('adm/csm/funcionario')
                            ->set('funcionarios',$funcionarios)
                            ->bind('errors', $errors)
                            ->bind('warning', $warning)
            );
    }

    public function action_update_funcionario() {
	Funcoes::verifica_login();


        $vars = $this->request->post();
        
        foreach ($vars as $key => $var)
        {
            if (empty($var))
            {
                $vars[$key] = NULL;
            }
        }

        $model_contato = new Model_Csm_Contato('default');
        $model_funcionario = new Model_Csm_Funcionario('default');
        $model_imagem = new Model_Csm_Imagem('default');
        $model_tipo_funcionario = new Model_Csm_TipoFuncionario('default');

        $tipoFuncionarios = $model_tipo_funcionario->select_tipo_funcionarios();
        $funcionarios = $model_funcionario->select_funcionarios();
        $funcionario = $model_funcionario->select_funcionario($vars);
        
        $validation = Validation::factory($this->request->post())
                ->rule('nmFuncionario', 'not_empty')
                ->rule('idContato', 'not_empty')
        ;


        if ($validation->check()) {
            try {
                

                    $model_contato->update_contato($vars);
                    $model_funcionario->update_funcionario($vars);


                    // upload
                    if (isset($vars['idImagem1'])) {
                        $model_imagem->edita_upload_imagem('funcionario', $vars['idImagem1'], 'ft1' ,$vars['dsft1']);                 
                    }
                    if (isset($vars['idImagem2'])) {
                        $model_imagem->edita_upload_imagem('funcionario', $vars['idImagem2'], 'ft2' ,$vars['dsft2']);
                   }
                    // termino upload
                   $warning = 'Funcionário atualizado com sucesso.';   
                    
            } catch (ExCEPtion $ex) {
                
                Kohana_ExCEPtion::handler($ex);
            }
            
        $tipoFuncionarios = $model_tipo_funcionario->select_tipo_funcionarios();
        $funcionario = $model_funcionario->select_funcionario($vars);
        $imagens = $model_imagem->select_imagem_por_funcionario($vars);
 
            
        $this->response->body(View::factory('adm/csm/editar_funcionario')
                            ->set('tipoFuncionarios', $tipoFuncionarios)
                            ->set('funcionario', $funcionario)
                            ->set('warning', $warning)
                            ->bind('funcionario', $funcionario)
                            ->set('imagens', $imagens)
            );
        }
        else
        {
            // Validation failed, collect the errors
            $errors = $validation->errors('', 'br');

            

            //fim validação
        }
                    $tipoFuncionarios = $model_tipo_funcionario->select_tipo_funcionarios();
            $funcionario = $model_funcionario->select_funcionario($vars);
            
            $imagens = $model_imagem->select_imagem_por_funcionario($vars);
            // Display the registration form
            $this->response->body(View::factory('adm/csm/editar_funcionario')
                            ->set('tipoFuncionarios', $tipoFuncionarios)
                            ->bind('errors', $errors)
                            ->bind('funcionario', $funcionario)
                            ->set('imagens', $imagens))
                            ;

    }

}

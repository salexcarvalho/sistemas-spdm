<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Dashboard_Csm_Servico extends Controller {

    public function action_home() {
        $this->redirect('dashboard_csm_servico/lista');
    }

    public function action_index() {
        $this->redirect('dashboard_csm_servico/lista');
    }

    public function action_lista() {
	Funcoes::verifica_login();

        $model_servico = new Model_Csm_Servico('default');

        $servicos = $model_servico->select_servicos();

        $view = View::factory('adm/csm/servico')
                ->set('servicos', $servicos)
        ;
        $this->response->body($view);
    }

    public function action_update_ativar_servico() {
	Funcoes::verifica_login();

        $idServico = $this->request->post('idServico');

        $model_servico = new Model_Csm_Servico('default');

        $model_servico->update_ativar_servico($idServico);

        $this->action_check_dados();
    }

    public function action_cadastro_servico() {
	Funcoes::verifica_login();

        $model_funcionario = new Model_Csm_Funcionario('default');
        $model_especialidade = new Model_Csm_Especialidade('default');
        $model_tipo_funcionario = new Model_Csm_TipoFuncionario('default');

        $funcionarios = $model_funcionario->select_funcionarios();
        $especialidades = $model_especialidade->select_especialidades();
        $tipoFuncionarios = $model_tipo_funcionario->select_tipo_funcionarios();

        $view = View::factory('adm/csm/cadastro_servico')
                ->set('funcionarios', $funcionarios)
                ->set('especialidades', $especialidades)
                ->set('tipoFuncionarios', $tipoFuncionarios)
        ;
        $this->response->body($view);
    }

    public function action_visualizar_servico() {
	Funcoes::verifica_login();

        $vars = $this->request->post();
        $idServico = $this->request->post('idServico');

        $model_funcionario = new Model_Csm_Funcionario('default');
        $model_especialidade = new Model_Csm_Especialidade('default');
        $model_tipo_funcionario = new Model_Csm_TipoFuncionario('default');
        $model_servico = new Model_Csm_Servico('default');
        $model_horario_atendimento = new Model_Csm_HorarioAtendimento('default');
        $model_imagem = new Model_Csm_Imagem('default');
        $model_especialidade_servico = new Model_Csm_EspecialidadeTemServico('default');
        $model_site_referencia = new Model_Csm_SiteReferencia('default');

        $funcionarios = $model_funcionario->select_funcionarios();
        $especialidades = $model_especialidade->select_especialidades();
        $tipoFuncionarios = $model_tipo_funcionario->select_tipo_funcionarios();

        
        $servico = $model_servico->select_servico($idServico);
        $horarios_atendimento = $model_horario_atendimento->select_horario_atendimento($vars);
        $imagens = $model_imagem->select_imagem_servico($idServico);
        $especialidades_servico = $model_especialidade_servico->select_especialidades_por_servico($idServico);
        $sites_servico = $model_site_referencia->select_site_por_servico($idServico);

        
        $ft['ft1'] = NULL;
        $ft['ft2'] = NULL;
        $ft['ft3'] = NULL;
        $ft['ft4'] = NULL;

        foreach ($imagens as $imagem) {
            $ex = explode('_', $imagem['nmImagem']);
            $en = end($ex);
            $ex2 = explode('.', $en);

            if ($ex2[0] == 'ft1') {
                $ft['ft1'] = $imagem;
            } elseif ($ex2[0] == 'ft2') {
                $ft['ft2'] = $imagem;
            } elseif ($ex2[0] == 'ft3') {
                $ft['ft3'] = $imagem;
            } elseif ($ex2[0] == 'ft4') {
                $ft['ft4'] = $imagem;
            } 
        }

        $view = View::factory('adm/csm/visualizar_servico')
                ->set('funcionarios', $funcionarios)
                ->set('especialidades', $especialidades)
                ->set('tipoFuncionarios', $tipoFuncionarios)
                ->set('servico', $servico)
                ->set('horarios_atendimento', $horarios_atendimento)
                ->set('imagens', $imagens)
                ->set('especialidades_servico', $especialidades_servico)
                ->set('sites_servico', $sites_servico)
                ->set('ft', $ft)
        ;
        $this->response->body($view);
    }

     public function action_insert_servico() {
	Funcoes::verifica_login();

        $especialidadeper = $this->request->post('especialidadeper');
        $nmServico = $this->request->post('nmServico');
        $nuCentroCusto = $this->request->post('nuCentroCusto');
        $dsServico = $this->request->post('dsServico');
        $dsDoenca = $this->request->post('dsDoenca');
        $dsTratamento = $this->request->post('dsTratamento');
        $formaAcesso = $this->request->post('formaAcesso');

        $horas24Dom = $this->request->post('24horas1');
        $horarioin1 = $this->request->post('horarioin1');
        $horariofi1 = $this->request->post('horariofi1');

        $horas24Seg = $this->request->post('24horas2');
        $horarioin2 = $this->request->post('horarioin2');
        $horariofi2 = $this->request->post('horariofi2');

        $horas24Ter = $this->request->post('24horas3');
        $horarioin3 = $this->request->post('horarioin3');
        $horariofi3 = $this->request->post('horariofi3');

        $horas24Qua = $this->request->post('24horas4');
        $horarioin4 = $this->request->post('horarioin4');
        $horariofi4 = $this->request->post('horariofi4');

        $horas24Qui = $this->request->post('24horas5');
        $horarioin5 = $this->request->post('horarioin5');
        $horariofi5 = $this->request->post('horariofi5');

        $horas24Sex = $this->request->post('24horas6');
        $horarioin6 = $this->request->post('horarioin6');
        $horariofi6 = $this->request->post('horariofi6');

        $horas24Sab = $this->request->post('24horas7');
        $horarioin7 = $this->request->post('horarioin7');
        $horariofi7 = $this->request->post('horariofi7');

        $chefeDep = $this->request->post('chefeDep');
        $endLogradouro = $this->request->post('endLogradouro');
        $nuLogradouro = $this->request->post('nuLogradouro');
        $complemento = $this->request->post('complemento');
        $CEP = $this->request->post('CEP');
        $bairro = $this->request->post('bairro');
        $cidade = $this->request->post('cidade');
        $estado = $this->request->post('estado');

        $mostrarContatoNoSite = $this->request->post('mostrarContatoNoSite');
        $telefone = $this->request->post('telefone');
        $nuVOIP = $this->request->post('nuVOIP');
        $email = $this->request->post('email');
        $site = $this->request->post('site');
        $facebook = $this->request->post('facebook');
        $twitter = $this->request->post('twitter');
        $googleplus = $this->request->post('googleplus');
        $linkedin = $this->request->post('linkedin');

        $nmEmpresa1 = $this->request->post('nmEmpresa1');
        $urlSite1 = $this->request->post('urlSite1');

        $nmEmpresa2 = $this->request->post('nmEmpresa2');
        $urlSite2 = $this->request->post('urlSite2');

        $nmEmpresa3 = $this->request->post('nmEmpresa3');
        $urlSite3 = $this->request->post('urlSite3');

        $nmEmpresa4 = $this->request->post('nmEmpresa4');
        $urlSite4 = $this->request->post('urlSite4');

        $nmEmpresa5 = $this->request->post('nmEmpresa5');
        $urlSite5 = $this->request->post('urlSite5');

        $dsImagem1 = $this->request->post('dsImagem1');
        $dsImagem2 = $this->request->post('dsImagem2');
        $dsImagem3 = $this->request->post('dsImagem3');
        $dsImagem4 = $this->request->post('dsImagem4');

        $nmFuncionario = $this->request->post('nmFuncionario');
        $tipoFuncionarioPer = $this->request->post('tipoFuncionarios');
        $sexo = $this->request->post('sexo');
        $dtNascimento = $this->request->post('dtNascimento');
        $cdRegistroFuncional = $this->request->post('cdRegistroFuncional');
        $nuConselho = $this->request->post('nuConselho');
        $miniCurriculum = $this->request->post('miniCurriculum');

        $model_servico = new Model_Csm_Servico('default');
        $model_endereco = new Model_Csm_Endereco('default');
        $model_contato = new Model_Csm_Contato('default');
        $model_funcionario = new Model_Csm_Funcionario('default');
        $model_especialidade = new Model_Csm_Especialidade('default');
        $model_especialidade_servico = new Model_Csm_EspecialidadeTemServico('default');
        $model_horario_atendimento = new Model_Csm_HorarioAtendimento('default');
        $model_site_referencia = new Model_Csm_SiteReferencia('default');
        $model_tipo_funcionario = new Model_Csm_TipoFuncionario('default');
        $model_imagem = new Model_Csm_Imagem('default');

        $funcionarios = $model_funcionario->select_funcionarios();
        $especialidades = $model_especialidade->select_especialidades();
        $tipoFuncionarios = $model_tipo_funcionario->select_tipo_funcionarios();
        

        $usuario = Auth::instance()->get_user();
        //validação
        $validation = Validation::factory($this->request->post())
                ->rule('nmServico', 'not_empty')
        ;

        if ($validation->check()) 
        {
            $tipoContato = 'Serviço';
            $res_idEndereco = NULL;
            $res_idContato = NULL;

            DB::query(NULL, 'START TRANSACTION')->execute('default');

            try {
               
                DB::query(NULL, 'SET foreign_key_checks = 0')->execute('default');


                $res_idEndereco = $model_endereco->insert_endereco($vars);
                
                    if ($chefeDep == 'NULL')
                    {
                        $res_idFuncionario = $model_funcionario->insert_funcionario($nmFuncionario, $sexo, $dtNascimento, $cdRegistroFuncional, $nuConselho, $miniCurriculum, $tipoFuncionarioPer, NULL);
                        $chefeDep = $res_idFuncionario[0];
                    }
                
                    $res_idContato = $model_contato->insert_contato($vars);
                    $res_idServico = $model_servico->insert_servico($nmServico, $dsServico, $dsDoenca, $dsTratamento, $formaAcesso, $res_idContato[0], $res_idEndereco[0], $chefeDep, $usuario, $nuCentroCusto);
                    $model_horario_atendimento->cadastra_horarioatd('Serviço', $res_idServico[0], $horas24Dom, $horarioin1, $horariofi1, $horas24Seg, $horarioin2, $horariofi2, $horas24Ter, $horarioin3, $horariofi3, $horas24Qua, $horarioin4, $horariofi4, $horas24Qui, $horarioin5, $horariofi5, $horas24Sex, $horarioin6, $horariofi6, $horas24Sab, $horarioin7, $horariofi7);
                

                    $model_site_referencia->insert_site_referencia($nmEmpresa1, $urlSite1, $res_idServico[0]);
                    $model_site_referencia->insert_site_referencia($nmEmpresa2, $urlSite2, $res_idServico[0]);
                    $model_site_referencia->insert_site_referencia($nmEmpresa3, $urlSite3, $res_idServico[0]);
                    $model_site_referencia->insert_site_referencia($nmEmpresa4, $urlSite4, $res_idServico[0]);
                    $model_site_referencia->insert_site_referencia($nmEmpresa5, $urlSite5, $res_idServico[0]);

                
                    // upload
                        $model_imagem->upload_imagem('servico', $res_idServico[0], 'ft1', $dsImagem1);
                        $model_imagem->upload_imagem('servico', $res_idServico[0], 'ft2', $dsImagem2);
                        $model_imagem->upload_imagem('servico', $res_idServico[0], 'ft3', $dsImagem3);
                        $model_imagem->upload_imagem('servico', $res_idServico[0], 'ft4', $dsImagem4);
                    // termino de upload

                    if ($especialidadeper[0] != 'NULL')
                     {
                        foreach ($especialidadeper as $espe)
                        {
                            $model_especialidade_servico->insert_especialidade_servico($espe, $res_idServico[0]);
                        }
                    }
                    
                    DB::query(NULL, 'COMMIT')->execute('default');
                    DB::query(NULL, 'SET foreign_key_checks = 1')->execute('default');
                    $warning = 'Serviço cadastrado com sucesso.';
            }
            catch (Database_Exception $ex)
            {
                
                    DB::query(NULL, 'ROLLBACK')->execute('default');
                    DB::query(NULL, 'SET foreign_key_checks = 1')->execute('default');
                    Kohana_Exception::handler($ex);
                    $errors = $validation->errors('', 'br');
            }
        }
            $servicos = $model_servico->select_servicos();

            $this->response->body(View::factory('adm/csm/servico')
                            ->set('servicos', $servicos)
                            ->bind('warning', $warning)
                            ->bind('errors', $errors));        
    }

    public function action_delete_servico() {
	Funcoes::verifica_login();

        $vars = $this->request->post();
        

            $model_especialidade_servico = new Model_Csm_EspecialidadeTemServico('default');
            $model_servico_palavra = new Model_Csm_ServicoTemPalavraChave('default');
            $model_sitereferencia = new Model_Csm_SiteReferencia('default');
            $model_servico_video = new Model_Csm_ServicoTemVideo('default');
            $model_imagem = new Model_Csm_Imagem('default');
            $model_horarioatendimento = new Model_Csm_HorarioAtendimento('default');
            $model_servico_funcionario = new Model_Csm_ServicoTemFuncionario('default');
            $model_servico = new Model_Csm_Servico('default');
            $model_contato = new Model_Csm_Contato('default');
            
            
            $model_especialidade_servico->delete_especialidade_servico_por_servico($vars);
            $model_servico_palavra->delete_servico_palavrachave_por_servico($vars);
            $model_imagem->delete_imagem_por_servico($vars);
            $model_horarioatendimento->delete_horarioatendimento_por_servico($vars);
            $model_servico_funcionario->delete_servico_funcionario_por_servico($vars);
            $model_servico_video->delete_servico_video_por_servico($vars);
            $model_sitereferencia->delete_sitereferencia_por_servico($vars);
            $model_contato->delete_contato($vars);
            $model_servico->delete_servico($vars['idServico']);
    }

    public function action_update_servico() {
	Funcoes::verifica_login();
        $vars = $this->request->post();
        
        /*echo '<pre>';
        var_dump($vars);
        echo '<pre>';
        exit;*/
        
        $model_servicos = new Model_Csm_Servico('default');
        $model_especialidade_servico = new Model_Csm_EspecialidadeTemServico('default');
        $model_endereco = new Model_Csm_Endereco('default');
        $model_contato = new Model_Csm_Contato('default');
        $model_site = new Model_Csm_SiteReferencia('default');
        $model_horario_atendimento = new Model_Csm_HorarioAtendimento('default');
        $model_imagem = new Model_Csm_Imagem('default');
        $model_funcionario = new Model_Csm_Funcionario('default');
        $especialidades_servico = $model_especialidade_servico->select_especialidades_por_servico($vars['idServico']);
        
        $validation = Validation::factory($this->request->post())
        ->rule('nmServico', 'not_empty');
        
        if ($validation->check())
            {
            try 
                {
                    $model_servicos->update_servico($vars);
                    
                    if ($vars['idEspecialidade'] == "NULL")
                    {
                        $model_especialidade_servico->delete_especialidade_servico_por_servico($vars);
                    }
                    else
                    {
                        if (count($especialidades_servico) == 0)
                        {
                            $model_especialidade_servico->insert_especialidade_servico($vars['idEspecialidade'], $vars['idServico']);
                        }
                        else
                        {
                            $model_especialidade_servico->update_especialidade_servico($vars['idEspecialidade'], $vars['idServico']);
                        }
                    }
                    $model_endereco->update_endereco($vars);
                    $model_contato->update_contato($vars);

                    foreach ($vars as $chave => $var)
                    {
                        if (($chave == 'site1') || ($chave == 'site2') || ($chave == 'site3'))
                        {
                            switch ($chave)
                            {
                            case 'site1':
                                if (($vars['site1'] == NULL) || ($vars['site1'] == ""))
                                {
                                    $model_site->delete_sitereferencia($vars['idSite1']);
                                }
                                else
                                {
                                    $model_site->update_site($vars['idSite1'], $vars['Empresa1'], $vars['site1']);
                                }
                                break;
                                
                            case 'site2':
                                if (($vars['site2'] == NULL) || ($vars['site2'] == ""))
                                {
                                    $model_site->delete_sitereferencia($vars['idSite2']);
                                }
                                else
                                {
                                    $model_site->update_site($vars['idSite2'], $vars['Empresa2'], $vars['site2']);
                                }
                                break;
                                
                            case 'site3':
                                if (($vars['site3'] == NULL) || ($vars['site3'] == ""))
                                {
                                    $model_site->delete_sitereferencia($vars['idSite3']);
                                }
                                else
                                {
                                    $model_site->update_site($vars['idSite3'], $vars['Empresa3'], $vars['site3']);
                                }
                                break;                            }
                        }
                    }
                    
                    $warning = 'Serviço atualizado com sucesso!';
                            
                }
            catch (Exception $ex)
                {
                    Kohana_Exception::handler($ex);
                    
                    $errors = $validation->errors('', 'br');
                }
                
                $servicos = $model_servicos->select_servicos();                    
                    
                    $view = View::factory('adm/csm/servico')
                                        ->set('servicos', $servicos)
                                        ->bind('errors', $errors)
                                        ->bind('warning', $warning)
                            ;
            }
            $this->response->body($view);
}

   public function action_editar_servico() {
	Funcoes::verifica_login();

        $vars = $this->request->post();
        $idServico = $this->request->post('idServico');
        
        $model_funcionario = new Model_Csm_Funcionario('default');
        $model_servicos = new Model_Csm_Servico('default');
        $model_especialidade = new Model_Csm_Especialidade('default');
        $model_especialidade_servico = new Model_Csm_EspecialidadeTemServico('default');
        $model_horario_atendimento = new Model_Csm_HorarioAtendimento('default');
        $model_imagem = new Model_Csm_Imagem('default');
        $model_site = new Model_Csm_SiteReferencia('default');        
        
        $nmfuncionarios = $model_funcionario->select_nmfuncionarios();
        $servico = $model_servicos->select_servico($idServico);
        $especialidades = $model_especialidade->select_especialidades();
        $especialidades_servico = $model_especialidade_servico->select_especialidades_por_servico($idServico);
        $horarios_atendimento = $model_horario_atendimento->select_horario_atendimento($vars);
        $imagem_servico = $model_imagem->select_imagem_servico($idServico);
        $sites = $model_site->select_site_por_servico($idServico);

        $view = View::factory('adm/csm/editar_servico')
                ->set('nmfuncionarios', $nmfuncionarios)
                ->set('especialidades', $especialidades)
                ->set('especialidades_servico', $especialidades_servico)
                ->set('servico', $servico)
                ->set('horarios_atendimento', $horarios_atendimento)
                ->set('imagem_servico', $imagem_servico)
                ->set('sites', $sites)
        ;
        $this->response->body($view);
    }

}

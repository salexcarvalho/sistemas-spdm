<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Dashboard_Csm_Departamento extends Controller
{

    public function action_index()
    {
        $this->redirect('dashboard_csm_departamento/lista');
    }
    
    public function action_lista()
    {
	Funcoes::verifica_login();

        $model_departamento = new Model_Csm_Departamento('default');

        $departamentos = $model_departamento->select_departamentos();

        $view = View::factory('adm/csm/lista_departamento')
                ->set('departamentos', $departamentos)
        ;
        $this->response->body($view);
    } 

    public function action_cadastro_departamento()
    {
	Funcoes::verifica_login();

        $model_funcionario = new Model_Csm_Funcionario('default');
        $model_endereco = new Model_Csm_Endereco('default');
        $model_contato = new Model_Csm_Contato('default');

        $funcionarios = $model_funcionario->select_funcionarios();
        $enderecos = $model_endereco->select_enderecos();
        $contatos = $model_contato->select_contatos_dep();

        $view = View::factory('adm/csm/cadastro_departamento')
                ->set('funcionarios', $funcionarios)
                ->set('enderecos', $enderecos)
                ->set('contatos', $contatos)
        ;
        $this->response->body($view);
    }

    public function action_insert_departamento()
    {
	Funcoes::verifica_login();

        $nmDepartamento = $this->request->post('nmDepartamento');
        $dsDepartamento = $this->request->post('dsDepartamento');
        $chefeDep = $this->request->post('chefeDep');
        $endLogradouro = $this->request->post('endLogradouro');
        $nuLogradouro = $this->request->post('nuLogradouro');
        $complemento = $this->request->post('complemento');
        $CEP = $this->request->post('CEP');
        $bairro = $this->request->post('bairro');
        $cidade = $this->request->post('cidade');
        $estado = $this->request->post('estado');
        $mostrarContatoNoSite = $this->request->post('mostrarContatoNoSite');
        $telefone = $this->request->post('telefone');
        $nuVOIP = $this->request->post('nuVOIP');
        $email = $this->request->post('email');
        $site = $this->request->post('site');
        $facebook = $this->request->post('facebook');
        $twitter = $this->request->post('twitter');
        $googleplus = $this->request->post('googleplus');
        $linkedin = $this->request->post('linkedin');
        $tipoContato = 'Departamento';

        $model_departamento = new Model_Csm_Departamento('default');
        $departamentos = $model_departamento->select_departamentos();

        $model_endereco = new Model_Csm_Endereco('default');
        $model_contato = new Model_Csm_Contato('default');

        $model_funcionario = new Model_Csm_Funcionario('default');

        $funcionarios = $model_funcionario->select_funcionarios();

        $usuario = Auth::instance()->get_user();

        //validação
        $validation = Validation::factory($this->request->post())
                ->rule('nmDepartamento', 'not_empty')

        ;

        if ($validation->check())
        {
           
            
            try
            {
            $res_idEndereco = $model_endereco->insert_endereco($vars);
            $res_idContato = $model_contato->insert_contato($vars);
            $model_departamento->insert_departamento($nmDepartamento, $dsDepartamento, $res_idEndereco[0], $res_idContato[0], $chefeDep);
            $warning = 'Departamento cadastrado com sucesso.';
                
            } catch (Exception $ex) {
                Kohana_Exception::handler($ex);
                $errors = $validation->errors('', 'br');
            }


            $departamentos = $model_departamento->select_departamentos();
            
            // Always redirect after a successful POST to prevent refresh warnings
            $this->response->body(View::factory('adm/csm/lista_departamento')
                            ->set('departamentos', $departamentos)
                            ->bind('warning',$warning)
                            ->bind('errors',$errors)
            );
        }

    }
    
    public function action_visualizar_departamento()
    {
	Funcoes::verifica_login();
        
        $idDepartamento = $this->request->query('idDepartamento');

        $model_funcionario = new Model_Csm_Funcionario('default');
        $model_departamento = new Model_Csm_Departamento('default');
        $model_tipo_funcionario = new Model_Csm_TipoFuncionario('default');

        $funcionarios = $model_funcionario->select_funcionarios();
        $tipoFuncionarios = $model_tipo_funcionario->select_tipo_funcionarios();
        
        $departamento = $model_departamento->select_departamento($idDepartamento);

        $view = View::factory('adm/csm/visualizar_departamento')
                ->set('funcionarios', $funcionarios)
                ->set('tipoFuncionarios', $tipoFuncionarios)
                ->set('departamento', $departamento)
        ;

        $this->response->body($view);
    }

    public function action_update_ativar_departamento()
    {
	Funcoes::verifica_login();

        $idDepartamento = $this->request->post('idDepartamento');

        $model_departamento = new Model_Csm_Departamento('default');

        $model_departamento->update_ativar_departamento($idDepartamento);

        $this->action_index();
    }


    public function action_editar_departamento() {
	Funcoes::verifica_login();

        $idDepartamento = $this->request->post('idDepartamento');
        $model_funcionarios = new Model_Csm_Funcionario('default');
        

        $model_departamento = new Model_Csm_Departamento('default');

        $departamento = $model_departamento->select_departamento($idDepartamento);
        
        
        $funcionarios = $model_funcionarios->select_funcionarios();


        $view = View::factory('adm/csm/editar_departamento')
                ->set('departamento', $departamento)
                ->set('funcionarios', $funcionarios)
        ;
        $this->response->body($view);
    }
    
    
        public function action_update_departamento() {
	Funcoes::verifica_login();

        $vars = $this->request->post();
        
        $tipoContato = 'Departamento';
        $vars['tipoContato'] = $tipoContato;
  

        foreach ($vars as $key => $var)
        {
            if (empty($var))
            {
                $vars[$key] = NULL;
            }
        }
        
        $model_endereco = new Model_Csm_Endereco('default');
        $model_contato = new Model_Csm_Contato('default');
        $model_departamento = new Model_Csm_Departamento('default');
        $model_funcionarios = new Model_Csm_Funcionario('default');
                
        $validation = Validation::factory($this->request->post())
                ->rule('nmDepartamento', 'not_empty');


        if ($validation->check())
        {
            try
            {
                

                if (!empty($vars['idContato']))
                {
                    $model_contato->update_contato($vars);
                }
                
                    else //Caso não exista insere o contato (desde que exista ao menos UM CAMPO PREENCHIDO)
                {
                    $mostrarContatoNoSite = $vars['mostrarContatoNoSite'];
                    $telefone = $vars['telefone'];
                    $nuVOIP = $vars['nuVOIP'];
                    $email = $vars['email'];
                    $site = $vars['site'];
                    $facebook = $vars['facebook'];
                    $twitter = $vars['twitter'];
                    $googleplus = $vars['googleplus'];
                    $linkedIn = $vars['linkedIn'];
                    $usuario = Auth::instance()->get_user();
                    
                    if  (!((empty($mostrarContatoNoSite)) && (empty($telefone)) && (empty($nuVOIP)) && (empty($email)) && (empty($site))
                        && (empty($facebook)) && (empty($twitter)) && (empty($googleplus)) && (empty($linkedIn)) && (empty($tipoContato)))){
                        $CON = $model_contato->insert_contato($vars);
                        $vars['idContato'] = $CON[0];
                        
                    }
                } //Fim do  bloco contato


                if (!empty($vars['idEndereco']))
                {
                    $model_endereco->update_endereco($vars);
                }
                
                else //Caso não exista insere o Endereço (desde que exista ao menos UM CAMPO PREENCHIDO)
                {
                    $endLogradouro = $vars['endLogradouro'];
                    $nuLogradouro = $vars['nuLogradouro'];
                    $complemento = $vars['complemento'];
                    $CEP = $vars['CEP'];
                    $bairro = $vars['bairro'];
                    $cidade = $vars['cidade'];
                    $estado = $vars['estado'];
                    
                    if  (!((empty($endLogradouro)) && (empty($nuLogradouro)) && (empty($complemento)) && (empty($CEP)) && (empty($bairro))
                        && (empty($cidade)) && (empty($estado)))){
                        $END = $model_endereco->insert_endereco($vars);
                        $vars['idEndereco'] = $END[0];
                    }
                } //Fim do  bloco Endereço

                $model_departamento->update_departamento($vars);
                $warning = 'Departamento atualizado com sucesso';
                
            } catch (Exception $ex)                
            {
                Kohana_Exception::handler($ex);
                $errors = $validation->errors('', 'br');
            }
        }
        
            $departamentos = $model_departamento->select_departamentos();
            
            $view = View::factory('adm/csm/lista_departamento')
                            ->set('departamentos', $departamentos)
                            ->bind('warning', $warning)
                            ->bind('errors', $errors)
                    ;
        
        
        
        $this->response->body($view);

    }
    
        public function action_delete_departamento() {
	Funcoes::verifica_login();
        $idDepartamento = $this->request->post('idDepartamento');
        $model_departamento = new Model_Csm_Departamento('default');
        $model_departamento->delete_departamento($idDepartamento);
        
    }
}

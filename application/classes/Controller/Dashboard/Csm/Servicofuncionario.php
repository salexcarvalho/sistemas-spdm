<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Dashboard_Csm_Servicofuncionario extends Controller
{

    public function action_index()
    {
        $this->redirect('dashboard_csm_servicofuncionario/lista');
    }

    public function action_lista()
    {
	Funcoes::verifica_login();

        $model_servico_funcionario = new Model_Csm_ServicoTemFuncionario('default');

        $servicos_funcionarios = $model_servico_funcionario->select_servicos_funcionarios();

        $view = View::factory('adm/csm/servico_funcionario')
                ->set('servicos_funcionarios', $servicos_funcionarios)
        ;
        $this->response->body($view);
    }
    
    public function action_add_servico_funcionario()
    {
	Funcoes::verifica_login();

        $model_servico = new Model_Csm_Servico('default');
        $model_funcionario = new Model_Csm_Funcionario('default');

        $servicos = $model_servico->select_servicos();
        $funcionarios = $model_funcionario->select_funcionarios();

        $view = View::factory('adm/csm/add_servico_funcionario')
                ->set('servicos', $servicos)
                ->set('funcionarios', $funcionarios)
        ;
        $this->response->body($view);
    }

    public function action_insert_servico_funcionario()
    {
	Funcoes::verifica_login();

        $idServico = $this->request->post('servicoper');
        $idFuncionarios = $this->request->post('funcionariosper');

        $model_servico = new Model_Csm_Servico('default');
        $model_funcionario = new Model_Csm_Funcionario('default');
        $model_servico_funcionario = new Model_Csm_ServicoTemFuncionario('default');

        $servicos = $model_servico->select_servicos();
        $funcionarios = $model_funcionario->select_funcionarios();

        if (isset($idServico) && $idServico != NULL && isset($idFuncionarios) && $idFuncionarios != NULL)
        {
            foreach ($idFuncionarios as $idFuncionario)
            {
                $model_servico_funcionario->insert_servico_funcionario($idServico, $idFuncionario);
            }

            $warning = 'Formulário enviado com sucesso.';
        }
        else
        {
            $warning = 'Formulário apresenta erros.';
        }
        $this->response->body(View::factory('adm/csm/add_servico_funcionario')
                        ->set('servicos', $servicos)
                        ->set('funcionarios', $funcionarios)
                        ->bind('warning', $warning)
        );
    }

    public function action_delete_servico_funcionario()
    {
	Funcoes::verifica_login();

        $idServico = $this->request->post('idServico');
        $idFuncionario = $this->request->post('idFuncionario');

        $vars['idServico'] = $idServico;
        $vars['idFuncionario'] = $idFuncionario;

        $model_servico_funcionario = new Model_Csm_ServicoTemFuncionario('default');

        $model_servico_funcionario->delete_servico_funcionario($vars);
    }

}

<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Dashboard_Csm_Especialidadefuncionario extends Controller
{

    public function action_index()
    {
        $this->redirect('dashboard_csm_especialidadefuncionario/lista');
    }

    public function action_lista()
    {
	Funcoes::verifica_login();

        $model_especialidade_funcionario = new Model_Csm_EspecialidadeTemFuncionario('default');

        $especialidades_funcionarios = $model_especialidade_funcionario->select_especialidades_funcionarios();

        $view = View::factory('adm/csm/especialidade_funcionario')
                ->set('especialidades_funcionarios', $especialidades_funcionarios)
        ;
        $this->response->body($view);
    }
    
    public function action_add_especialidade_funcionario()
    {
	Funcoes::verifica_login();

        $model_especialidade = new Model_Csm_Especialidade('default');
        $model_funcionario = new Model_Csm_Funcionario('default');

        $especialidades = $model_especialidade->select_especialidades();
        $funcionarios = $model_funcionario->select_funcionarios();

        $view = View::factory('adm/csm/add_especialidade_funcionario')
                ->set('especialidades', $especialidades)
                ->set('funcionarios', $funcionarios)
        ;
        $this->response->body($view);
    }

    public function action_insert_especialidade_funcionario()
    {
	Funcoes::verifica_login();

        $idEspecialidade = $this->request->post('especialidadeper');
        $idFuncionarios = $this->request->post('funcionariosper');

        $model_especialidade = new Model_Csm_Especialidade('default');
        $model_funcionario = new Model_Csm_Funcionario('default');
        $model_especialidade_funcionario = new Model_Csm_EspecialidadeTemFuncionario('default');

        $especialidades = $model_especialidade->select_especialidades();
        $funcionarios = $model_funcionario->select_funcionarios();

        if (isset($idEspecialidade) && $idEspecialidade != NULL && isset($idFuncionarios) && $idFuncionarios != NULL)
        {
            foreach ($idFuncionarios as $idFuncionario)
            {
                $model_especialidade_funcionario->insert_especialidade_funcionario($idEspecialidade, $idFuncionario);
            }

            $warning = 'Formulário enviado com sucesso.';
        }
        else
        {
            $warning = 'Formulário apresenta erros.';
        }
        $this->response->body(View::factory('adm/csm/add_especialidade_funcionario')
                        ->set('especialidades', $especialidades)
                        ->set('funcionarios', $funcionarios)
                        ->bind('warning', $warning)
        );
    }

    public function action_delete_especialidade_funcionario()
    {
	Funcoes::verifica_login();

        $idEspecialidade = $this->request->post('idEspecialidade');
        $idFuncionario = $this->request->post('idFuncionario');

        $vars['idEspecialidade'] = $idEspecialidade;
        $vars['idFuncionario'] = $idFuncionario;

        $model_especialidade_funcionario = new Model_Csm_EspecialidadeTemFuncionario('default');

        $model_especialidade_funcionario->delete_espe_funcionario($vars);
    }

}

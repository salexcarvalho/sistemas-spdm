<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Dashboard_Csm_Especialidades extends Controller
{

    public function action_index()
    {
        $this->redirect('dashboard_csm_especialidades/lista');
    }
    
    public function action_lista()
    {
	Funcoes::verifica_login();

        $model_especialidade = new Model_Csm_Especialidade('default');

        $especialidades = $model_especialidade->select_especialidades();

        $view = View::factory('adm/csm/lista_especialidade')
                
                ->set('especialidades', $especialidades)
        ;
        $this->response->body($view);
    }

    public function action_cadastro_especialidade()
    {
	Funcoes::verifica_login();

        $model_funcionario = new Model_Csm_Funcionario('default');
        $model_especialidade = new Model_Csm_Especialidade('default');
        $model_tipo_funcionario = new Model_Csm_TipoFuncionario('default');

        $funcionarios = $model_funcionario->select_funcionarios();
        $especialidades = $model_especialidade->select_especialidades();
        $tipoFuncionarios = $model_tipo_funcionario->select_tipo_funcionarios();

        $view = View::factory('adm/csm/add_especialidade')
                ->set('funcionarios', $funcionarios)
                ->set('especialidades', $especialidades)
                ->set('tipoFuncionarios', $tipoFuncionarios)
        ;
        $this->response->body($view);
    }
    
    public function action_visualizar_especialidade()
    {
	Funcoes::verifica_login();
        
        $vars = $this->request->query();
        
        $idEspecialidade = $this->request->query('idEspecialidade');
        $idContato = $this->request->query('idContato');

        $model_funcionario = new Model_Csm_Funcionario('default');
        $model_especialidade = new Model_Csm_Especialidade('default');
        $model_tipo_funcionario = new Model_Csm_TipoFuncionario('default');
        $model_horario_atendimento = new Model_Csm_HorarioAtendimento('default');

        $funcionarios = $model_funcionario->select_funcionarios();
        $especialidades = $model_especialidade->select_especialidades();
        $tipoFuncionarios = $model_tipo_funcionario->select_tipo_funcionarios();
        $contato = $model_especialidade->select_contato_especialidade($idContato);
        $especialidade = $model_especialidade->select_especialidade($idEspecialidade);
        $horarios_atendimento = $model_horario_atendimento->select_horario_atendimento($vars);
        //$imagens = $model_imagem->select_imagem_especialidade($idEspecialidade);

        $view = View::factory('adm/csm/visualizar_especialidade')
                ->set('funcionarios', $funcionarios)
                ->set('especialidades', $especialidades)
                ->set('tipoFuncionarios', $tipoFuncionarios)
                ->set('especialidade', $especialidade)
                ->set('horarios_atendimento', $horarios_atendimento)
                ->set('contato', $contato)
        ;
        $this->response->body($view);
    }
    
    public function action_add_especialidade()
    {
	Funcoes::verifica_login();

        $model_funcionario = new Model_Csm_Funcionario('default');
        $model_tipo_funcionario = new Model_Csm_TipoFuncionario('default');
        $funcionarios = $model_funcionario->select_funcionarios();
        $tipoFuncionarios = $model_tipo_funcionario->select_tipo_funcionarios();

        $view = View::factory('adm/csm/add_especialidade')
                ->set('funcionarios', $funcionarios)
                ->set('tipoFuncionarios', $tipoFuncionarios)
        ;
        $this->response->body($view);
    }
    
    public function action_update_ativar_especialidade()
    {
	Funcoes::verifica_login();
        $idEspecialidade = $this->request->post('idEspecialidade');
        $model_especialidade = new Model_Csm_Especialidade('default');
        $model_especialidade->update_ativar_especialidade($idEspecialidade);
        $this->action_check_dados();
    }

    public function action_insert_especialidade()
    {
	Funcoes::verifica_login();

        $vars = $this->request->post(); 


        (isset($vars['24horas1'])) ? $vars['24horas1']:$vars['24horas1'] = NULL;
        (isset($vars['24horas2'])) ? $vars['24horas2']:$vars['24horas2'] = NULL;
        (isset($vars['24horas3'])) ? $vars['24horas3']:$vars['24horas3'] = NULL;
        (isset($vars['24horas4'])) ? $vars['24horas4']:$vars['24horas4'] = NULL;
        (isset($vars['24horas5'])) ? $vars['24horas5']:$vars['24horas5'] = NULL;
        (isset($vars['24horas6'])) ? $vars['24horas6']:$vars['24horas6'] = NULL;
        (isset($vars['24horas7'])) ? $vars['24horas7']:$vars['24horas7'] = NULL;
        
        (isset($vars['horarioin1'])) ? $vars['horarioin1']:$vars['horarioin1'] = NULL;
        (isset($vars['horariofi1'])) ? $vars['horariofi1']:$vars['horariofi1'] = NULL;
        (isset($vars['horarioin2'])) ? $vars['horarioin2']:$vars['horarioin2'] = NULL;
        (isset($vars['horariofi2'])) ? $vars['horariofi2']:$vars['horariofi2'] = NULL;
        (isset($vars['horarioin3'])) ? $vars['horarioin3']:$vars['horarioin3'] = NULL;
        (isset($vars['horariofi3'])) ? $vars['horariofi3']:$vars['horariofi3'] = NULL;
        (isset($vars['horarioin4'])) ? $vars['horarioin4']:$vars['horarioin4'] = NULL;
        (isset($vars['horariofi4'])) ? $vars['horariofi4']:$vars['horariofi4'] = NULL;
        (isset($vars['horarioin5'])) ? $vars['horarioin5']:$vars['horarioin5'] = NULL;
        (isset($vars['horariofi5'])) ? $vars['horariofi5']:$vars['horariofi5'] = NULL;
        (isset($vars['horarioin6'])) ? $vars['horarioin6']:$vars['horarioin6'] = NULL;
        (isset($vars['horariofi6'])) ? $vars['horariofi6']:$vars['horariofi6'] = NULL;
        (isset($vars['horarioin7'])) ? $vars['horarioin7']:$vars['horarioin7'] = NULL;
        (isset($vars['horariofi7'])) ? $vars['horariofi7']:$vars['horariofi7'] = NULL;


        $model_funcionario = new Model_Csm_Funcionario('default');
        $model_tipo_funcionario = new Model_Csm_TipoFuncionario('default');
        $model_imagem = new Model_Csm_Imagem('default');

        $funcionarios = $model_funcionario->select_funcionarios();
        $tipoFuncionarios = $model_tipo_funcionario->select_tipo_funcionarios();

        $model_endereco = new Model_Csm_Endereco('default');
        $model_contato = new Model_Csm_Contato('default');
        $model_especialidade = new Model_Csm_Especialidade('default');
        $model_horario_atendimento = new Model_Csm_HorarioAtendimento('default');

        $usuario = Auth::instance()->get_user();

        //validação
        $validation = Validation::factory($this->request->post())
                ->rule('nmEspecialidade', 'not_empty');

        if ($validation->check())
        {
            try
            {
                $tipoContato = 'Especialidade';
                $res_idEndereco = NULL;
                $res_idContato = NULL;

                $res_idEndereco = $model_endereco->insert_endereco($vars);
                $res_idContato = $model_contato->insert_contato($vars);

                if ($vars['chefeDep'] == 'NULL')
                {
                    $res_idFuncionario = $model_funcionario->insert_funcionario($vars['nmFuncionario'], $vars['sexo'], $vars['dtNascimento'], $vars['cdRegistroFuncional'], $vars['nuConselho'], $vars['miniCurriculum'], $vars['tipoFuncionarios'], NULL);
                    $vars['chefeDep'] = $res_idFuncionario[0];
                }

                $res_idEspecialidade = $model_especialidade->insert_especialidade($vars['nmEspecialidade'], $vars['dsEspecialidade'], $vars['nuCentroCusto'], $res_idEndereco[0], $vars['chefeDep'], $res_idContato[0]);

                // upload
                $model_imagem->upload_imagem('especialidade', $res_idEspecialidade[0], 'ft1', $vars['dsImagem1']);
                /*$model_imagem->upload_imagem('especialidade', $res_idEspecialidade[0], 'ft2', $vars['dsImagem2']);
                $model_imagem->upload_imagem('especialidade', $res_idEspecialidade[0], 'ft3', $vars['dsImagem3']);
                $model_imagem->upload_imagem('especialidade', $res_idEspecialidade[0], 'ft4', $vars['dsImagem4']);
                $model_imagem->upload_imagem('especialidade', $res_idEspecialidade[0], 'ft5', $vars['dsImagem5']);*/
                // termino de upload
                // Cadastra Horário de Atendimento
                $model_horario_atendimento->cadastra_horarioatd('Especialidade', $res_idEspecialidade[0], $vars['24horas1'], $vars['horarioin1'], $vars['horariofi1'], $vars['24horas2'], $vars['horarioin2'], $vars['horariofi2'], $vars['24horas3'], $vars['horarioin3'], $vars['horariofi3'], $vars['24horas4'], $vars['horarioin4'], $vars['horariofi4'], $vars['24horas5'], $vars['horarioin5'], $vars['horariofi5'], $vars['24horas6'], $vars['horarioin6'], $vars['horariofi6'], $vars['24horas7'], $vars['horarioin7'], $vars['horariofi7']);
                // Fim cadastro Horário de Atendimento
                $warning = 'Especialidade cadastrada com sucesso.';
            }
            catch (Exception $ex)
            {

                Kohana_Exception::handler($ex);
                $errors = $validation->errors('', 'br');
            }
        }
        $especialidades = $model_especialidade->select_especialidades();
                    $this->response->body(View::factory('adm/csm/lista_especialidade')
                            ->set('especialidades',$especialidades)
                            ->bind('warning', $warning)
                            ->bind('errors', $errors));
    }
    
        public function action_editar_especialidade()
    {
	Funcoes::verifica_login();
        
        $vars = $this->request->post();
 
        $model_funcionario = new Model_Csm_Funcionario('default');
        $model_especialidade = new Model_Csm_Especialidade('default');
        $model_tipo_funcionario = new Model_Csm_TipoFuncionario('default');
        $model_horario_atendimento = new Model_Csm_HorarioAtendimento('default');
        $model_imagem = new Model_Csm_Imagem('default');


        $funcionarios = $model_funcionario->select_funcionarios();
        $especialidades = $model_especialidade->select_especialidades();
        $tipoFuncionarios = $model_tipo_funcionario->select_tipo_funcionarios();
        $contato = $model_especialidade->select_contato_especialidade($vars['idContato']);
        $especialidade = $model_especialidade->select_especialidade($vars['idEspecialidade']);
        $horarios_atendimento = $model_horario_atendimento->select_horario_atendimento($vars);
        $imagens = $model_imagem->select_imagem_especialidade($vars);
 
        $view = View::factory('adm/csm/editar_especialidade')
                ->set('funcionarios', $funcionarios)
                ->set('especialidades', $especialidades)
                ->set('tipoFuncionarios', $tipoFuncionarios)
                ->set('especialidade', $especialidade)
                ->set('horarios_atendimento', $horarios_atendimento)
                ->set('contato', $contato)
                ->set('imagens', $imagens)
            ;
        
        $this->response->body($view);

    }
    
    public function action_update_especialidade()
    {
	Funcoes::verifica_login();

        $vars = $this->request->post();
        $vars['tipoContato'] = 'Especialidade';
        
        foreach ($vars as $key => $var)
        {
            if (empty($var))
            {
                $vars[$key] = NULL;
            }
        }
        
       $model_especialidade = new Model_Csm_Especialidade('default');
        $model_Contato = new Model_Csm_Contato('default');
        $model_horarios_atd = new Model_Csm_HorarioAtendimento('default');
        $model_Endereco = new Model_Csm_Endereco('default');
        $model_imagem = new Model_Csm_Imagem('default');
        
        $validation = Validation::factory($this->request->post())
        ->rule('nmEspecialidade', 'not_empty');
        
        if ($validation->check())
        {
            try
            {
        
                foreach ($vars as $key => $value)
                {
                    if (!(strpos($key, 'ft')=== FALSE)) 
                    {
                    
                        $nameUpload = $_FILES[$key]['name'];
                        echo '<pre>';
                        var_dump($nameUpload);
                        echo '</pre>';
                        exit;    
                        
                        //$model_imagem->upload_imagem('Especialidade', $vars['idEspecialidade'], $nameUpload, $dsImagem);
                    }
                }

                if (!(empty($vars['idContato'])))
                {
                    $model_Contato->update_contato($vars);
                }
                else
                {
                    $mostrarContatoNoSite = $vars['mostrarContatoNoSite'];
                    $telefone = $vars['telefone'];
                    $nuVOIP = $vars['nuVOIP'];
                    $email = $vars['email'];
                    $site = $vars['site'];
                    $facebook = $vars['facebook'];
                    $twitter = $vars['twitter'];
                    $googleplus = $vars['googleplus'];
                    $linkedIn = $vars['linkedIn'];
                    $usuario = Auth::instance()->get_user();
                    $tipoContato = 'Especialidade';
                    $CON = $model_Contato->insert_contato($vars);
                    $vars['idContato'] = $CON[0];
                }

                
                
                if (!(empty($vars['idEndereco'])))
                {
                    $model_Endereco->update_endereco($vars);
                }
                else
                {
                    $endLogradouro = $vars['endLogradouro'];
                    $nuLogradouro = $vars['nuLogradouro'];
                    $complemento = $vars['complemento'];
                    $CEP = $vars['CEP'];
                    $bairro = $vars['bairro'];
                    $cidade = $vars['cidade'];
                    $estado = $vars['estado'];
                    $END = $model_Endereco->insert_endereco($vars);
                    $vars['idContato'] = $CON[0];
                }

                    $model_horarios_atd->update_horario_atendimento($vars);
                    $model_especialidade->update_especialidade($vars);
                    
                    $warning = "Especialidade atualizada com sucesso!";
            }
            catch (Exception $ex)
            {
                Kohana_Exception::handler($ex);
                $errors = $validation->errors('', 'br');    
            }
        }
                $especialidades = $model_especialidade->select_especialidades();
                    
                $view = View::factory('adm/csm/lista_especialidade')
                    ->set('especialidades', $especialidades)
                    ->bind('errors', $errors)
                    ->bind('warning', $warning)
                    ;
                $this->response->body($view);

    }
    
    public function action_delete_especialidade(){
	Funcoes::verifica_login();        
       $vars = $this->request->post();

       
            $model_contato = new Model_Csm_Contato('default');
            $model_horarioatd = new Model_Csm_HorarioAtendimento('default');
            $model_especialidade = new Model_Csm_Especialidade('default');
            $model_endereco = new Model_Csm_Endereco('default');
            
            $status = $model_horarioatd->delete_horarioatendimento_por_especialidade($vars);
            $status = $model_especialidade->delete_especialidade($vars['idEspecialidade']);
            $status = $model_endereco->delete_endereco($vars['idEndereco']);
            $status = $model_contato->delete_contato($vars);
     
            return $status;
    }

}

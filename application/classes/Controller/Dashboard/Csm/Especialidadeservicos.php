<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Dashboard_Csm_Especialidadeservicos extends Controller
{

    public function action_index()
    {
        $this->redirect('dashboard_csm_especialidadeservicos/lista');
    }

    public function action_lista()
    {
	Funcoes::verifica_login();

        $model_especialidade_servico = new Model_Csm_EspecialidadeTemServico('default');

        $especialidades_servicos = $model_especialidade_servico->select_especialidades_servicos();

        $view = View::factory('adm/csm/especialidade_servico')
                ->set('especialidades_servicos', $especialidades_servicos)
        ;
        $this->response->body($view);
    }

    public function action_add_especialidade_servico()
    {
	Funcoes::verifica_login();

        $model_especialidade = new Model_Csm_Especialidade('default');
        $model_servico = new Model_Csm_Servico('default');

        $especialidades = $model_especialidade->select_especialidades();
        $servicos = $model_servico->select_servicos();

        $view = View::factory('adm/csm/add_especialidade_servico')
                ->set('especialidades', $especialidades)
                ->set('servicos', $servicos)
        ;
        $this->response->body($view);
    }

    public function action_insert_especialidade_servico()
    {
	Funcoes::verifica_login();

        $idEspecialidade = $this->request->post('especialidadeper');
        $idServicos = $this->request->post('servicosper');

        $model_especialidade = new Model_Csm_Especialidade('default');
        $model_servico = new Model_Csm_Servico('default');
        $model_especialidade_servico = new Model_Csm_EspecialidadeTemServico('default');

        $especialidades = $model_especialidade->select_especialidades();
        $servicos = $model_servico->select_servicos();

        if (isset($idEspecialidade) && $idEspecialidade != NULL && isset($idServicos) && $idServicos != NULL)
        {
            foreach ($idServicos as $idServico)
            {
                $model_especialidade_servico->insert_especialidade_servico($idEspecialidade, $idServico);
            }

            $warning = 'Formulário enviado com sucesso.';
        }
        else
        {
            $warning = 'Formulário apresenta erros.';
        }
        $this->response->body(View::factory('adm/csm/add_especialidade_servico')
                        ->set('especialidades', $especialidades)
                        ->set('servicos', $servicos)
                        ->bind('warning', $warning)
        );
    }

    public function action_delete_especialidade_servico()
    {
	Funcoes::verifica_login();

        $idEspecialidade = $this->request->post('idEspecialidade');
        $idServico = $this->request->post('idServico');

        $vars['idEspecialidade'] = $idEspecialidade;
        $vars['idServico'] = $idServico;

        $model_especialidade_servico = new Model_Csm_EspecialidadeTemServico('default');

        $model_especialidade_servico->delete_espe_servico($vars);
    }

}

<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Dashboard_Configuracoes extends Controller {

    /** Chama a paginas relacionados a usuarios* */
    public function action_index() {
        $this->redirect('dashboard/home');
    }
    
    public function action_home() {
	Funcoes::verifica_login();
        
        $model_config = new Model_Login_Configuracoes('default');
        $config = $model_config->select_config();
        $view = View::factory('adm/login/config')
                ->set('config', $config)
                ;
        $this->response->body($view);
    }
    
    public function action_atualizar() {
	Funcoes::verifica_login();
        
        $vars = $this->request->post();
        $logo_admin = $_FILES['logo_admin'];
        
        if ($logo_admin['name'] !== '') {
            $logo_admin_name = date('Hms',time()) .'_'.$logo_admin['name'];
            // upload
            $logo_admin = Upload::save($_FILES['logo_admin'], $logo_admin_name, 'upload/config');
        } else {
            $logo_admin_name = 'logo_sistema_adm.png';
        }                 
        
        $model_config = new Model_Login_Configuracoes('default');
        $configs = $model_config->update_config($vars, $logo_admin_name);
        
        $warning = "Alteração efetuada com sucesso!";
        
        $config = $model_config->select_config();
        $view = View::factory('adm/login/config')
                ->set('config', $config)
                ->set('warning', $warning)
                ;
        $this->response->body($view);
    }
}

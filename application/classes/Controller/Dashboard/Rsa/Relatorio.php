<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Dashboard_Rsa_Relatorio extends Controller {

    public function action_index() {
        $this->redirect('rsa_reserva/home');
    }

    public function action_home() {
	Funcoes::verifica_login();
        $model_relatorio = new Model_Rsa_Relatorio('default');
        //Areas
        $areas = $model_relatorio->select_areas();
        $totais['areas'] = $model_relatorio->select_qtde_areas();
        
        //Salas
        $salas = $model_relatorio->select_salas();
        $totais['salas'] = $model_relatorio->select_qtde_salas();
        
        //Reservas       
        $reservas = $model_relatorio->select_reservas();
        $totais['reservas'] = $model_relatorio->select_qtde_reservas();
        
        
        $view = View::factory('adm/rsa/relatorio')
         
                ->set('totais',$totais);
        $this->response->body($view);
    }

    public function action_getReport() {
        $tipo = $this->request->query('tipo');
        $model_area = new Model_Rsa_Area('default');
        $model_sala = new Model_Rsa_Salas('default');
        $model_reserva = new Model_Rsa_Reserva('default');
        $args = array();
        if ($tipo == 'salas') {
            $salas = $model_sala->select_salas();
            foreach ($salas as $sala):
                $rq = $model_reserva->select_reservas_salas_qtde($sala['idSalas']);
                $args[] = array('label' => $sala['nome'], 'value' => $rq);
            endforeach;
        }
        if ($tipo == 'areas') {
            $areas = $model_area->select_areas();
            foreach ($areas as $area):
                $rq = $model_sala->select_reservas_area_qtde($area['idArea']);
                $args[] = array('label' => $area['nome'], 'value' => $rq);
            endforeach;
        }
        if ($tipo == 'mes') {
            $rq = 0;
            for ($i = 1; $i <= 12; $i++):
                switch ($i):
                    case 1:
                         $mes = 'Jan';   
                        break;
                    case 2:
                        $mes = 'Fev';
                        break;
                    case 3:
                        $mes = 'Mar';
                        break;
                    case 4:
                        $mes = 'Abr';
                        break;
                    case 5:
                        $mes = 'Mai';
                        break;
                    case 6:
                        $mes = 'Jun';
                        break;
                    case 7:
                        $mes = 'Jul';
                        break;
                    case 8:
                        $mes = 'Ago';
                        break;
                    case 9:
                        $mes = 'Set';
                        break;
                    case 10:
                        $mes = 'Out';
                        break;
                    case 11:
                        $mes = 'Nov';
                        break;
                    case 12:
                        $mes = 'Dez';
                        break;
                endswitch;
                $rq = $model_reserva->select_reservas_mes_qtde($i); 
                $args[] = array('label' => $mes, 'value' => $rq);
            endfor;
        }

        echo json_encode($args);
    }

}

<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Dashboard_Rsa_Areas extends Controller {

    public function action_index() {
        $this->redirect('rsa_areas/home');
    }

    public function action_home() {
	Funcoes::verifica_login();
        $page = $this->request->query('page');

        $model_area = new Model_Rsa_Area('default');
        $areas = $model_area->select_areas();

        $paginator = Paginator::factory($areas);
        $paginator->set_current_page_number($page);

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($areas);

        $view = View::factory('adm/rsa/area')
                ->set('areas', $areas)
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total);
        $view->data = $paginator;
        $view->pagination = $paginator->render();
        $this->response->body($view);
    }

    public function action_pesquisa() {
	Funcoes::verifica_login();
        $nome = $this->request->query('nome');
        $page = $this->request->query('page');

        $model_area = new Model_Rsa_Area('default');
        $areas = $model_area->select_areas($nome);

        $paginator = Paginator::factory($areas);
        $paginator->set_current_page_number($page);

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($areas);

        $view = View::factory('adm/rsa/area')
                ->set('areas', $areas)
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total);
        $this->response->body($view);
    }

    public function action_cadastro_area() {
	Funcoes::verifica_login();
        $view = View::factory('adm/rsa/cadastro_area');
        $this->response->body($view);
    }

    public function action_insert_area() {
	Funcoes::verifica_login();
        $local = $this->request->post('nome');
        $endereco = $this->request->post('endereco');
        $status = $this->request->post('status');
        $modelo_areas = new Model_Rsa_Area('default');

        $modelo_areas->insert_area($local, $endereco, $status);

        $warning = "Area cadastrada com sucesso!";
        $page = $this->request->query('page');

        $areas = $modelo_areas->select_areas();

        $paginator = Paginator::factory($areas);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home');

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($areas);

        $view = View::factory('adm/rsa/area')
                ->set('areas', $areas)
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
                ->set('warning', $warning);

        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }

    public function action_visualizar_area() {
	Funcoes::verifica_login();
        $cond = ($this->request->query('cd') != 1) ? "disabled='disabled'" : "";
        $btn = ($this->request->query('cd') != 1) ? "style='display:none'" : "";
        $idArea = $this->request->query('idArea');
        $modelo_areas = new Model_Rsa_Area('default');
        $area = $modelo_areas->select_areas_filtro_id($idArea);
        $view = View::factory('adm/rsa/visualizar_area')
                ->set('cond', $cond)
                ->set('btn', $btn)
                ->set('area', $area);
        $this->response->body($view);
    }

    public function action_update_area() {
	Funcoes::verifica_login();
        $idArea = $this->request->post('idArea');
        $local = $this->request->post('nome');
        $endereco = $this->request->post('endereco');
        $status = $this->request->post('status');
        $modelo_areas = new Model_Rsa_Area('default');

        $modelo_areas->update_area($idArea, $local, $endereco, $status);


        $warning = "Area atualizada com sucesso!";
        $page = $this->request->query('page');

        $areas = $modelo_areas->select_areas();

        $paginator = Paginator::factory($areas);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home');

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($areas);

        $view = View::factory('adm/rsa/area')
                ->set('areas', $areas)
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
                ->set('warning', $warning);

        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }

    public function action_update_ativar_area() {
	Funcoes::verifica_login();
        $idArea = $this->request->post('idArea');

        $model_Area = new Model_Rsa_Area('default');
        $view = $model_Area->update_ativar_area($idArea);
        
        $this->response->body($view);
        //$this->redirect('rsa_areas/home');
    }

    public function action_delete_area() {
	Funcoes::verifica_login();

        $idArea = $this->request->post('idArea');
        $model_salas = new Model_Rsa_Salas('default');
        $model_reserva = new Model_Rsa_Reserva('default');
        $model_Area = new Model_Rsa_Area('default');
        $salas = $model_salas->select_salas_ids($idArea);
        if ($salas != null) {
            $reservas = $model_reserva->select_reservas_ids($salas);
        } else {
            $reservas = null;
        }
        if ($reservas != null):
            $model_reserva->excluir_reserva($reservas);
            $model_salas->excluir_salas($salas);
            $model_Area->excluir_area($idArea);
        else:
            if ($salas != NULL):
                $model_salas->delete_sala($salas);
            endif;
            $model_Area->delete_area($idArea);
        endif;
    }

}
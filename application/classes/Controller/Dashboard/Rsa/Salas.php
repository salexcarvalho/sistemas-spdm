<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Dashboard_Rsa_Salas extends Controller {

    public function action_index() {
        $this->redirect('rsa_salas/home');
    }

    public function action_home() {
	Funcoes::verifica_login();
        $page = $this->request->query('page');
        $model_salas = new Model_Rsa_Salas('default');
        $salas = $model_salas->select_salas_reservar();
        $model_areas = new Model_Rsa_Area('default');
        $area = $model_areas->select_quantidade_areas();

        $paginator = Paginator::factory($salas);
        $paginator->set_current_page_number($page);

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($salas);

        $view = View::factory('adm/rsa/salas')
                ->set('area', $area)
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total);
        $view->data = $paginator;
        $view->pagination = $paginator->render();
        $this->response->body($view);
    }

    public function action_pesquisa() {
	Funcoes::verifica_login();
        $nome = $this->request->query('nome');
        $page = $this->request->query('page');

        $model_sala = new Model_Rsa_Salas('default');
        $salas = $model_sala->select_salas_reservar($nome);

        $model_areas = new Model_Rsa_Area('default');
        $area = $model_areas->select_quantidade_areas();

        $paginator = Paginator::factory($salas);
        $paginator->set_current_page_number($page);

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($salas);

        $view = View::factory('adm/rsa/sala')
                ->set('area', $area)
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total);
        $this->response->body($view);
    }

    public function action_cadastro_salas() {
	Funcoes::verifica_login();
        $view = View::factory('adm/rsa/cadastro_salas');
        $this->response->body($view);
    }

    public function action_insert_sala() {
	Funcoes::verifica_login();
        $nome = $this->request->post('nome');
        $capacidade = $this->request->post('capacidade');
        $extra = $this->request->post('extras');
        $rgb = Funcoes::rgb();
        $area_id = $this->request->post('area_id');
        $status = $this->request->post('status');
        $modelo_salas = new Model_Rsa_Salas('default');
        $modelo_salas->insert_sala($nome, $capacidade, $extra, $rgb, $area_id, $status);
        $model_areas = new Model_Rsa_Area('default');
        $area = $model_areas->select_quantidade_areas();

        $warning = "Area cadastrada com sucesso!";
        $page = $this->request->query('page');

        $salas = $modelo_salas->select_salas_reservar();

        $paginator = Paginator::factory($salas);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home');

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($salas);

        $view = View::factory('adm/rsa/salas')
                ->set('area', $area)
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
                ->set('warning', $warning);

        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }

    public function action_visualizar_sala() {
	Funcoes::verifica_login();
        $cond = ($this->request->query('cd') != 1) ? "disabled='disabled'" : "";
        $btn = ($this->request->query('cd') != 1) ? "style='display:none'" : "";
        $idSalas = $this->request->query('idSalas');
        $modelo_salas = new Model_Rsa_Salas('default');
        $salas = $modelo_salas->select_salas_id($idSalas);
        $view = View::factory('adm/rsa/visualizar_salas')
                ->set('cond', $cond)
                ->set('btn', $btn)
                ->set('salas', $salas);
        $this->response->body($view);
    }

    public function action_update_sala() {
	Funcoes::verifica_login();
        $idSalas = $this->request->post('idSalas');
        $nome = $this->request->post('nome');
        $capacidade = $this->request->post('capacidade');
        $extra = $this->request->post('extras');
        $area_id = $this->request->post('area_id');
        $status = $this->request->post('status');
        $model_areas = new Model_Rsa_Area('default');
        $area = $model_areas->select_quantidade_areas();
        $modelo_salas = new Model_Rsa_Salas('default');
        $modelo_salas->update_sala($idSalas, $nome, $capacidade, $extra, $area_id, $status);

        $warning = "Sala atualizada com sucesso!";
        $page = $this->request->query('page');

        $salas = $modelo_salas->select_salas_reservar();

        $paginator = Paginator::factory($salas);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('home');

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($salas);

        $view = View::factory('adm/rsa/salas')
                ->set('area', $area)
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
                ->set('warning', $warning);

        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }

    public function action_update_ativar_sala() {
	Funcoes::verifica_login();
        $idSalas = $this->request->query('idSalas');

        $model_Sala = new Model_Rsa_Salas('default');
        $model_Sala->update_ativar_sala($idSalas);
        $sala = $model_Sala->select_salas_id($idSalas);
        
        if ($sala[0]['status'] != "1") {//se retornar algum resultado
            $view = '0';
        } else {
            $view = '1';
        }
        $this->response->body($view);
    }

    public function action_delete_sala() {
	Funcoes::verifica_login();

        $idSalas[] = $this->request->post('idSalas');
        $model_reserva = new Model_Rsa_Reserva('default');
        $model_salas = new Model_Rsa_Salas('default');
        $reservas = $model_reserva->select_reservas_ids($idSalas);
        if (isset($reservas)) {
            $model_reserva->excluir_reserva($reservas);
            $rs = $model_salas->excluir_salas($idSalas);
        } else {
            $rs = $model_salas->delete_sala($idSalas);
        }
        return $rs;
    }   
    
}

<?php 

defined('SYSPATH') or die('No direct script access.');

class Controller_Dashboard_Rsa_Reserva extends Controller {

    public function action_index() {
        $this->redirect('rsa_reserva/home');
    }

    public function action_home() {
        Funcoes::verifica_login();

        $model_salas = new Model_Rsa_Salas('default');
        $salas = $model_salas->select_salas();
        $salasReserva = $model_salas->select_salas_reservar();
        $model_areas = new Model_Rsa_Area();
        $areas = $model_areas->select_areas();

        $view = View::factory('adm/rsa/reserva')
                ->set('areas', $areas)
                ->set('salas', $salas)
                ->set('salasReserva', $salasReserva);
        $this->response->body($view);
    }

    public function action_incluir_reserva() {
        Funcoes::verifica_login();

        $idReserva = $this->request->query('idReserva');
        
        $model_salas = new Model_Rsa_Salas('default');
        $model_reserva = new Model_Rsa_Reserva('default');
        $salasReserva = $model_salas->select_salas_reservar();
        $dados = $model_reserva->select_reserva($idReserva);

        if ($dados[0]['idSerie'] != '') {
            $serieinicio = $model_reserva->select_reserva_inicio($dados[0]['idSerie']);
            $seriefim = $model_reserva->select_reserva_fim($dados[0]['idSerie']);
        } else {
            $serieinicio = $model_reserva->select_reserva($idReserva);
            $seriefim = $model_reserva->select_reserva($idReserva);
        }
        
        $view = View::factory('adm/rsa/modal_reserva')
                ->set('salasReserva', $salasReserva)
                ->set('dados', $dados)
                ->set('serieinicio', $serieinicio)
                ->set('seriefim', $seriefim)       
                ->set('idReserva', $idReserva);
        $this->response->body($view);
    }
    
    public function action_visualizar_reserva() {
        Funcoes::verifica_login();

        $idReserva = $this->request->query('idReserva');
        
        $model_salas = new Model_Rsa_Salas('default');
        $model_reserva = new Model_Rsa_Reserva('default');
        $salasReserva = $model_salas->select_salas_reservar();
        $dados = $model_reserva->select_reserva($idReserva);

        $view = View::factory('adm/rsa/modal_visualizar')
                ->set('salasReserva', $salasReserva)
                ->set('dados', $dados)     
                ->set('idReserva', $idReserva);
        $this->response->body($view);
    }

    public function action_getSalas() {
        Funcoes::verifica_login();
        $model_salas = new Model_Rsa_Salas('default');
        $salas = $model_salas->select_salas();
        $resource = array();
        foreach ($salas as $sala):
            $resource[] = array(
                "id" => $sala['idSalas'],
                "title" => $sala['nome'],
                "eventColor" => "#" . $sala['rgb']
            );
        endforeach;

        echo json_encode($resource);
    }

    public function action_getReservas() {
        Funcoes::verifica_login();
        $model_reserva = new Model_Rsa_Reserva('default');
        $model_salas = new Model_Rsa_Salas('default');
        $model_usuario = new Model_Login_Usuario('user');
        $reservas = $model_reserva->select_reservas_data();
        $args = array();
        foreach ($reservas as $reserva):
            $salas = $model_salas->select_salas_rgb($reserva['sala_id']);
            $idUsuario = ($reserva['idUsuario_at'] > 0) ? $reserva['idUsuario_at'] : $reserva['idUsuario'];
            $user = $model_usuario->select_usuario($idUsuario);
                $args[] = array(
                    "id" => $reserva['idReserva'],
                    "title" => $reserva['titulo'] . ' - ' . $reserva['NomeSala'],
                    "start" => $reserva['start_data'],
                    "end" => $reserva['end_data'],
                    "responsavel" => $reserva['responsavel'],
                    "resourceId" => $reserva['sala_id'],
                    "sala" => $reserva['sala_id'],
                    "color" => "#" . $salas[0]['rgb'],
                    "owner" => $user[0]['nmNome'],
                    "owner_id" => $idUsuario,
                    "obs" => $reserva['obs']
                );
        endforeach;

        echo json_encode($args);
    }

    public function action_setReservas() {
        Funcoes::verifica_login();
        $model_reserva = new Model_Rsa_Reserva('default');
        $model_salas = new Model_Rsa_Salas('default');

        $titulo = $this->request->post('titulo');
        $sala_id = $this->request->post('sala_id');
        $start_data = Funcoes::convertData($this->request->post('start_data'));
        $end_data = Funcoes::convertData($this->request->post('end_data'));
        $responsavel = $this->request->post('responsavel');
        $start_hora = $this->request->post('start_hora');
        $end_hora = $this->request->post('end_hora');
        $inicioSerie = $this->request->post('inicioSerie');
        $obs = $this->request->post('obs');

        $start = $start_data . " " . $start_hora;
        $end = $end_data . " " . $end_hora;

        $diferenca = strtotime($end_data) - strtotime($start_data);
        $dias = floor($diferenca / (60 * 60 * 24));

        if ($inicioSerie == 1) :
            $dow = implode(',', $this->request->post('dow'));
            $array = explode(',', $dow);
            // Executa a inserção em série
            $serie = Function_Certificados::coderand(8, 2); 
            for ($i = 0; $i <= $dias; $i++) {
                if (in_array(date('w', strtotime('+' . $i . ' days', strtotime($start))), $array) !== false) {
                    $start1 = date('Y-m-d', strtotime('+' . $i . ' days', strtotime($start_data))) . " " . $start_hora;
                    $end1 = date('Y-m-d', strtotime('+' . $i . ' days', strtotime($start_data))) . " " . $end_hora;
                    if (isset($reserva[0])) {
                        $reserva = $model_reserva->insert_reserva($titulo, $start1, $end1, $responsavel, $sala_id, $serie, 0, $dow, $obs);
                    } else {
                        $reserva = $model_reserva->insert_reserva($titulo, $start1, $end1, $responsavel, $sala_id, $serie, 1, $dow, $obs);
                    }
                }
            }
        else :
            $reserva = $model_reserva->insert_reserva($titulo, $start, $end, $responsavel, $sala_id, NULL, 0, NULL, $obs);
        endif;

        $salas = $model_salas->select_salas();
        $salasReserva = $model_salas->select_salas_reservar();

        $warning = "Reserva cadastrada com sucesso!";

        $view = View::factory('adm/rsa/reserva')
                ->set('salas', $salas)
                ->set('salasReserva', $salasReserva)
                ->set('warning', $warning);
        $this->response->body($view);
    }

    public function action_updateReservas() {
        Funcoes::verifica_login();
        $model_reserva = new Model_Rsa_Reserva('default');
        $model_salas = new Model_Rsa_Salas('default');

        $idReserva = $this->request->post('idReserva');
        $titulo = $this->request->post('titulo');
        $sala_id = $this->request->post('sala_id');
        $responsavel = $this->request->post('responsavel');
        $inicioSerie = $this->request->post('inicioSerie');
        $idSerie = $this->request->post('idSerie');
        $Serie = $this->request->post('Serie');
        $obs = $this->request->post('obs');
        $start_data = Funcoes::convertData($this->request->post('start_data'));
        $end_data = Funcoes::convertData($this->request->post('end_data'));
        $start_data_serie = Funcoes::convertData($this->request->post('start_data_serie'));
        $end_data_serie = Funcoes::convertData($this->request->post('end_data_serie'));
        $start_hora = $this->request->post('start_hora');
        $end_hora = $this->request->post('end_hora');
        
        $start = $start_data . " " . $start_hora;
        $startSerie = $start_data_serie . " " . $start_hora;
        $end = $end_data . " " . $end_hora;

        $diferenca = strtotime($end_data_serie) - strtotime($start_data_serie);
        $dias = floor($diferenca / (60 * 60 * 24));
        
        if ($Serie == 'S') :
            $model_reserva->excluir_reserva_serie($idSerie);
            $dow = implode(',', $this->request->post('dow'));
            $array = explode(',', $dow);
            // Executa a atualização em série
            for ($i = 0; $i <= $dias; $i++) {
                if (in_array(date('w', strtotime('+' . $i . ' days', strtotime($startSerie))), $array) !== false) {
                    $start1 = date('Y-m-d', strtotime('+' . $i . ' days', strtotime($start_data_serie))) . " " . $start_hora;
                    $end1 = date('Y-m-d', strtotime('+' . $i . ' days', strtotime($start_data_serie))) . " " . $end_hora;
                    $reserva = $model_reserva->insert_reserva($titulo, $start1, $end1, $responsavel, $sala_id, $idSerie, 1, $dow, $obs);
                   }
            }
        else :
            $reserva = $model_reserva->update_reserva($idReserva, $titulo, $start, $end, $responsavel, $sala_id, NULL, 0, NULL, $obs);
        endif;        

        $salas = $model_salas->select_salas();
        $salasReserva = $model_salas->select_salas_reservar();

        $warning = "Reserva alterada com sucesso!";

        $view = View::factory('adm/rsa/reserva')
                ->set('salas', $salas)
                ->set('salasReserva', $salasReserva)
                ->set('warning', $warning);
        $this->response->body($view);
    }

    public function action_excluirReservas() {
        Funcoes::verifica_login();

        $idReserva = $this->request->post('idReserva');
        $idSerie = $this->request->post('idSerie');
        $Serie = $this->request->post('Serie');
        $model_reserva = new Model_Rsa_Reserva('default');
        
        if ($Serie == 'S') {
            $reserva = $model_reserva->excluir_reserva_serie($idSerie);
        } else {
            $reserva = $model_reserva->excluir_reserva_p($idReserva);
        }  
        return $reserva;
    }

    public function action_imprimirReserva() {
        Funcoes::verifica_login();

        $data = $this->request->query('ini_relat');
        $idArea = base64_decode($this->request->query('idArea'));
        $model_salas = new Model_Rsa_Salas('default');
        $local = $model_salas->select_salas_area($idArea);

        $view = View::factory('adm/rsa/reservaimprimir')
                ->set('data', $data)
                ->set('local', $local)
                ->render();

        $nome = 'Reserva_Sala_' . str_replace('-', '_', $data) . '.pdf';

        $conteudo = Request::factory($view);

        $html2pdf = new HTML2PDF('P', 'A4', Kohana::$config->load('html2pdf')->get('langue'), true, 'UTF-8', array('5', '5', '5', '5'));
        $html2pdf->pdf->SetAuthor('TI Presidência da SPDM');
        $html2pdf->pdf->SetTitle('Reserva de Salas SPDM');

        $content = $conteudo->uri();

        $html2pdf->writeHTML($content, false);
        $html2pdf->Output($nome, 'I');
    }

    public function action_modal_imprimir() {
        Funcoes::verifica_login();

        $model_areas = new Model_Rsa_Area('default');
        $dados = $model_areas->select_areas();
        $view = View::factory('adm/rsa/modal_imprimir')
                ->set('dados', $dados);
        $this->response->body($view);
    }
}
<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Dashboard_Rsa_Agendamento extends Controller {
     public function action_index() {
        $this->redirect('rsa_agendar/home');
    }

    public function action_home() {
	Funcoes::verifica_login();
        $page = $this->request->query('page');
        $model_agendamento = new Model_Rsa_Agendamento('default');
        $agendamentos = $model_agendamento->select_agendamentos();
        
        $paginator = Paginator::factory($agendamentos);
        $paginator->set_current_page_number($page);

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($agendamentos);

        $view = View::factory('adm/rsa/agendamentos')
                ->set('agendamentos', $agendamentos)               
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total);
        $view->data = $paginator;
        $view->pagination = $paginator->render();
        $this->response->body($view);
    }
    
    public function action_visualizar() {
	Funcoes::verifica_login();
        $idAgendamento = $this->request->query('idAgendamento');
        $model_agendamento = new Model_Rsa_Agendamento('default');
        $agendamento = $model_agendamento->select_visualizar($idAgendamento);
        $dados = $agendamento[0]; 
        $view = View::factory('adm/rsa/modal_agendamento')
                ->set('dados', $dados); 
        $this->response->body($view);
    }
    
    public function action_confirmar(){
        
        $idAgendamento = $this->request->query('idAgendamento');
        $model_agendamento = new Model_Rsa_Agendamento('default');
        $agendamento = $model_agendamento->select_agendamento($idAgendamento);       
        $model_reserva = new Model_Rsa_Reserva('default');        
        $titulo = $agendamento[0]['titulo'];
        $start_data = $agendamento[0]['start_data'];
        $end_data = $agendamento[0]['end_data'];
        $responsavel = $agendamento[0]['resp_nome'];
        $sala_id = $agendamento[0]['sala_id'];
        $obs = $agendamento[0]['obs'];
        $reserva = $model_reserva->insert_reserva($titulo, $start_data, $end_data, $responsavel, $sala_id, NULL, 0, $obs);
        $model_agendamento->change_status($idAgendamento, 1);
        
        return 1;
    }
    public function action_conciliar(){
        
        $idSala = $this->request->post('idSala');
        $start_data = $this->request->post('start_data');
        $end_data = $this->request->post('end_data');
        $model_reserva = new Model_Rsa_Reserva();
        $verificar = $model_reserva->disponibilidade($idSala, $start_data, $end_data);
        return $verificar[0]['idReserva'];
        
    }
    public function action_rejeitar(){
        
        $idAgendamento = $this->request->query('idAgendamento');
        $model_agendamento = new Model_Rsa_Agendamento('default');
        $agendamento = $model_agendamento->change_status($idAgendamento, 2);
        
    }
}
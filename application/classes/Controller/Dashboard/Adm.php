<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Dashboard_Adm extends Controller {

    /** Chama a paginas relacionados a usuarios* */
    public function action_index() {
        $this->redirect('dashboard_adm/home');
    }
    
    public function action_home() {
	Funcoes::verifica_login();
        $pag = $this->request->query('pag');
        $view = View::factory('adm/login/home');
        $this->response->body($view);
    }

    public function action_lista_usuario() {
	Funcoes::verifica_login();
        
        $page = $this->request->query('page');

        $model_usuario = new Model_Login_Usuario('user');
        $usuarios = $model_usuario->select_usuarios();
       
        $paginator = Paginator::factory($usuarios);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('lista_usuario');

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($usuarios);

        $view = View::factory('adm/login/lista_usuarios')
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }

    public function action_add_usuario() {
	Funcoes::verifica_login();

        $model_usuario = new Model_Login_Usuario('user');
        $model_setor = new Model_Login_Setor('user');
        $perfil = $model_usuario->select_perfis();
        $setor = $model_setor->select_tipo();

        $view = View::factory('adm/login/add_usuario')
                ->set('perfil', $perfil)
                ->set('setor', $setor)
        ;
        $this->response->body($view);
    }

    public function action_visualizar_usuario() {
	Funcoes::verifica_login();

        $idUsuario = $this->request->query('idUsuario');

        $model_usuario = new Model_Login_Usuario('user');
        $model_setor = new Model_Login_Setor('user');
        $usuario = $model_usuario->select_usuario($idUsuario);
        $perfil = $model_usuario->select_perfis();
        $setor = $model_setor->select_tipo();

        $view = View::factory('adm/login/visualizar_usuario')
                ->set('usuario', $usuario)
                ->set('perfil', $perfil)
                ->set('setor', $setor)
        ;
        $this->response->body($view);
    }
    
    public function action_minha_conta() {
	Funcoes::verifica_login();

        $idUsuario = $this->request->query('idUsuario');

        $model_usuario = new Model_Login_Usuario('user');
        $model_setor = new Model_Login_Setor('user');
        $usuario = $model_usuario->select_usuario($idUsuario);
        $perfil = $model_usuario->select_perfis();
        $setor = $model_setor->select_tipo();

        $view = View::factory('adm/login/minha_conta')
                ->set('usuario', $usuario)
                ->set('perfil', $perfil)
                ->set('setor', $setor)
        ;
        $this->response->body($view);
    }    

    public function action_editar_dados_usuarios() {
	Funcoes::verifica_login();

        $idUsuario = $this->request->query('idUsuario');
        $page = $this->request->query('page');

        $model_usuario = new Model_Login_Usuario('user');
        $model_setor = new Model_Login_Setor('user');
        $usuario = $model_usuario->select_usuario($idUsuario);
        $perfil = $model_usuario->select_perfis();
        $setor = $model_setor->select_tipo();

        $view = View::factory('adm/login/editar_usuario')
                ->set('usuario', $usuario)
                ->set('perfil', $perfil)
                ->set('setor', $setor)
                ->set('page', $page)
        ;
        $this->response->body($view);
    }
     
    public function action_editar_minha_conta() {
	Funcoes::verifica_login();

        $idUsuario = $this->request->query('idUsuario');

        $model_usuario = new Model_Login_Usuario('user');
        $model_setor = new Model_Login_Setor('user');
        $usuario = $model_usuario->select_usuario($idUsuario);
        $perfil = $model_usuario->select_perfis();
        $setor = $model_setor->select_tipo();

        $view = View::factory('adm/login/editar_minha_conta')
                ->set('usuario', $usuario)
                ->set('perfil', $perfil)
                ->set('setor', $setor)
        ;
        $this->response->body($view);
    }

    /** Executa ações relacionados a usuarios * */
    public function action_insert_usuario() {
	Funcoes::verifica_login();

        $nmNome = $this->request->post('txtNome');
        $nmUsuario = $this->request->post('txtUsuario');
        $senha = $this->request->post('txtSenha');
        $email = $this->request->post('txtEmail');
        $idTipoSetor = $this->request->post('idTipoSetor');
        $idPerfil = $this->request->post('idPerfil');
        
        $hash = Auth::instance()->hash($senha);

        $model_usuario = new Model_Login_Usuario('user');
        $model_usuario->insert_usuario($nmNome, $nmUsuario, $idTipoSetor, $hash, $email);

        $idUsuario = $model_usuario->select_usuario_pornome_ins($nmUsuario);
        $model_usuario->insert_usuario_perfil($idUsuario[0]['idUsuario'], $idPerfil);
        
        /* Mostra a resposta da ação de exclusão */
        $page = $this->request->query('page');
        $usuarios = $model_usuario->select_usuarios();
        $warning = "Usuário cadastrado com sucesso!";
        
        $paginator = Paginator::factory($usuarios);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('lista_usuario');

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($usuarios);

        $view = View::factory('adm/login/lista_usuarios')
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
                ->set('warning', $warning)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }

    public function action_editar_usuario() {
	Funcoes::verifica_login();

        $idUsuario = $this->request->post('idUsuario');
        $nmNome = $this->request->post('txtNome');
        $nmUsuario = $this->request->post('txtUsuario');        
        $idTipoSetor = $this->request->post('idTipoSetor');
        $senha = $this->request->post('txtSenha');
        $email = $this->request->post('txtEmail');
        $idPerfil = $this->request->post('idPerfil');

        $model_usuario = new Model_Login_Usuario('user');
        if ($senha != ""):
            $hash = Auth::instance()->hash($senha);
            $model_usuario->update_usuario($idUsuario, $nmNome, $nmUsuario, $idTipoSetor, $hash, $email);
        else:
            $model_usuario->update_usuario_simples($idUsuario, $nmNome, $nmUsuario, $idTipoSetor, $email);
        endif;
        $model_usuario->update_perfil_usuario($idUsuario, $idPerfil);

        /* Mostra a resposta da ação de exclusão */
        $page = $this->request->query('page');
        $usuarios = $model_usuario->select_usuarios();
        $warning = "Usuário atualizado com sucesso!";
        
        $paginator = Paginator::factory($usuarios);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('lista_usuario');

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($usuarios);

        $view = View::factory('adm/login/lista_usuarios')
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
                ->set('warning', $warning)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }
      
    public function action_editar_conta() {
	Funcoes::verifica_login();

        $idUsuario = $this->request->post('idUsuario');
        $nmNome = $this->request->post('txtNome');
        $nmUsuario = $this->request->post('txtUsuario');
        $idTipoSetor = $this->request->post('idTipoSetor');
        $senha = $this->request->post('txtSenha');
        $email = $this->request->post('txtEmail');
        $idPerfil = $this->request->post('idPerfil');
        
        $model_setor = new Model_Login_Setor('user');
        $model_usuario = new Model_Login_Usuario('user');
        
        if ($senha != ""):
            $hash = Auth::instance()->hash($senha);
            $model_usuario->update_usuario($idUsuario, $nmNome, $nmUsuario, $idTipoSetor, $hash, $email);
        else:
            $model_usuario->update_usuario_simples($idUsuario, $nmNome, $nmUsuario, $idTipoSetor, $email);
        endif;

        $model_usuario->update_perfil_usuario($idUsuario, $idPerfil);

        /* Mostra a resposta da ação de exclusão */
        $usuario = $model_usuario->select_usuario($idUsuario);
        $perfil = $model_usuario->select_perfis();   
        $setor = $model_setor->select_tipo();
        $warning = "Usuário atualizado com sucesso!";
        $this->response->body(View::factory('adm/login/minha_conta')
                        ->bind('usuario', $usuario)
                        ->bind('perfil', $perfil)
                        ->bind('setor', $setor)
                        ->bind('warning', $warning))
        ;
    }

    public function action_excluir_usuario() {
	Funcoes::verifica_login();

        $idUsuario = $this->request->query('idUsuario');

        $model_usuario = new Model_Login_Usuario('user');
        $model_usuario->delete_perfil_usuario($idUsuario);
        $model_usuario->delete_usuario($idUsuario);

        /* Mostra a resposta da ação de exclusão */
        $page = $this->request->query('page');
        $usuarios = $model_usuario->select_usuarios();
        $warning = "Usuário excluído com sucesso!";
        
        $paginator = Paginator::factory($usuarios);
        $paginator->set_current_page_number($page);
        $paginator->set_option_queries('lista_usuario');

        $PaginaAtual = $paginator->get_current_page_number();
        $NtotalPagina = $paginator->count();
        $total = $paginator->get_item_count($usuarios);

        $view = View::factory('adm/login/lista_usuarios')
                ->set('PaginaAtual', $PaginaAtual)
                ->set('NtotalPagina', $NtotalPagina)
                ->set('total', $total)
                ->set('warning', $warning)
        ;
        $view->data = $paginator;
        $view->pagination = $paginator->render();

        $this->response->body($view);
    }

    public function action_verifica_usuario() {
	Funcoes::verifica_login();

        $txtUsuario = $this->request->query('txtUsuario');

        $model_usuario = new Model_Login_Usuario('user');
        $usuario = $model_usuario->select_usuario_pornome($txtUsuario);

        if ($usuario[0]['idUsuario'] != "") {//se retornar algum resultado
            $view = 'ok';
        } else {
            $view = '';
        }
        $this->response->body($view);
    }
    
    /** Executa ações relacionados a redefinição de senha * */
    public function action_redefinir_senha() {
        $view = View::factory('adm/login/redefinir_senha');
        $this->response->body($view);
    }
    
    public function action_resetar_senha() {
        $nmUsuarioEmail = $this->request->post('nmUsuarioEmail');
        
        $model_usuario = new Model_Login_Usuario('user');
        $usuario = $model_usuario->select_usuario_redrfinir($nmUsuarioEmail);
        
        if (empty($usuario[0]['idUsuario'])) {//se retornar algum resultado
            $warning = "Não existe Usuário cadastrado ou Ativo";
            $estilo = 'danger';
        } else {
            
            $viewemail = View::factory('adm/login/email_redefinicao_senha')
                    ->set('usuario', $usuario)
                    ->render();
            
            $email_subject = 'Redefinição de Senha com sucesso';
            $email_body = $viewemail;
            $is_html = true;
            $message = Email::factory($email_subject, $email_body, $is_html)
                    ->from('naoresponder@spdm.org.br', 'Sistemas SPDM')
                    ->to($usuario[0]['email'], $usuario[0]['nmNome']);
            $result = $message->send();
            
            $warning = "Solicita efetuada com sucesso, verifique seu email e siga as instruções. <br> Obrigado!";
            $estilo = 'success';
        }
        
        $view = View::factory('adm/login/redefinir_senha')
                ->set('warning', $warning)
                ->set('estilo', $estilo)
                ;
        $this->response->body($view);
    }
    
    public function action_redefinir() {
        $Token = $this->request->query('Token');
        $Email = $this->request->query('Email');
                
        $view = View::factory('adm/login/redefinir')
                ->set('Token', $Token)
                ->set('Email', $Email);
        $this->response->body($view);
    }
    
    public function action_resetar() {
        $Token = $this->request->post('Token');
        $Email = $this->request->post('Email');
        $senha = $this->request->post('txtSenha');
        
        $hash = Auth::instance()->hash($senha);
        
        $model_usuario = new Model_Login_Usuario('user');
        $model_usuario->update_senha($Token, $Email, $hash);
        
        $warning = "Senha alterada com sucesso";
        $estilo = 'success';
                
        $view = View::factory('adm/login/login')
                ->set('warning', $warning)
                ->set('estilo', $estilo);
        $this->response->body($view);
    }

    /** Metodos de Login * */
    public function action_login() {
        $view = View::factory('adm/login/login')
        ;
        $this->response->body($view);
    }

    public function action_logar() {
        
        $post = $this->request->post();
        $nmUsuario = ($this->request->post('nmUsuario')) ? $this->request->post('nmUsuario') : '';
        $url = $this->request->post('url');
        if ($nmUsuario == '') {
            if($url!=""){                
                $this->redirect('dashboard/adm/login?pag='.$url);
            }else{
               $this->redirect('dashboard/adm/login'); 
            }
        }
    
        $model_usuario = new Model_Login_Usuario('user');
        $users = $model_usuario->select_usuario_pornome($nmUsuario);
        
        foreach ($users as $user) {
            $users2[$user['nmUsuario']] = $user['senha'];
        }
        
        if (count($users) > 0) {    
           Auth::instance()->set_users($users2);
        }
        
        $success = Auth::instance()->login($nmUsuario, $post['senha']);

        if ($success) {
            $model_usuario->update_login_usuario($nmUsuario);
            
            $model_acao = new Model_Login_Acoes('user');
            $model_perfil = new Model_Login_Perfis('user');
            $usuario = $model_usuario->select_usuario_perfil($nmUsuario);
            $PerfilTemAcao = $model_perfil->select_perfil_acao_usuario($usuario[0]['perfil']);
            
            $_SESSION['idUsuario'] = $usuario[0]['idUsuario'];
            $_SESSION['Usuario'] = $usuario[0]['nmUsuario'];
            $_SESSION['NomeUsuario'] = $usuario[0]['nmNome'];
            $_SESSION['EmailUsuario'] = $usuario[0]['email'];
            $_SESSION['Perfil'] = $usuario[0]['nmPerfil'];
            $_SESSION['idSetor'] = $usuario[0]['idSetor'];
            $_SESSION['nmSetor'] = $usuario[0]['nmSetor'];
            
            foreach ($PerfilTemAcao as $listaperfis) {
                $_SESSION[$listaperfis['idCurto']] = $listaperfis['Status'];
            }
            if($url==""){ 
                   $this->action_home();
            }else{ 
                $this->redirect(base64_decode($url));
            }
            
        } else {

            $warning = "Usuário ou senha inválidos!";
            $estilo = 'danger';
            $view = View::factory('adm/login/login')
                    ->set('warning', $warning)
                    ->set('url', $url)
                    ->set('estilo', $estilo);
            
            $this->response->body($view);
            
        }
    }

    /** Método para deslogar o usuário */
    public function action_logout() {
        if (Auth::instance()->logged_in() == 1) {
            Auth::instance()->logout();
        }
        
        $warning = "Você se desconectou com sucesso!";
        $estilo = 'success';
        $view = View::factory('adm/login/login')
                ->set('warning', $warning)
                ->set('estilo', $estilo)
        ;
        $this->response->body($view);
    }

    public function action_update_ativar_usuario() {
	Funcoes::verifica_login();

        $idUsuario = $this->request->query('idUsuario');

        $model_usuario = new Model_Login_Usuario('user');
        $model_usuario->update_ativar_usuario($idUsuario);
        $usuario = $model_usuario->select_usuario($idUsuario);

        if ($usuario[0]['ativo'] != "S") {//se retornar algum resultado
            $view = '0';
        } else {
            $view = '1';
        }
        $this->response->body($view);
    }
    
}

<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Canalconfidencial_Home extends Controller
{

    public function action_index()
    {
        $this->redirect('canalconfidencial/home');
    }

    public function action_home() {
                
        $view = View::factory('scc/home');
        $this->response->body($view);
    }

}


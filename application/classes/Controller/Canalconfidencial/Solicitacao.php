<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Canalconfidencial_Solicitacao extends Controller {

    public function action_index() {
        $this->redirect('canalconfidencial/home');
    }

    public function action_home() {

        $view = View::factory('scc/solicitacao');
        $this->response->body($view);
    }

    public function action_nao_concordo() {

        $view = View::factory('scc/nao_concordo');
        $this->response->body($view);
    }

    public function action_abrir_relato() {

        $model_vinculos = new Model_Scc_Vinculos('default');
        $vinculos = $model_vinculos->select_vinculos_ativos('1');

        $view = View::factory('scc/abrir_relato')
                ->set('vinculos', $vinculos)
        ;
        $this->response->body($view);
    }

    public function action_acompanhar_relato() {

        $view = View::factory('scc/acompanhar_relato')
        ;
        $this->response->body($view);
    }

    public function action_visualizar_relato() {

        $idProtocolo = (($this->request->post('idProtocolo'))) ? $this->request->post('idProtocolo') : $this->request->query('idProtocolo');

        $model_relatos = new Model_Scc_Relato('default');
        $protocolo = $model_relatos->select_protocolo($idProtocolo);
        
        if ($protocolo[0]['idRelato'] != "") {
            $relato = $model_relatos->select_relato($protocolo[0]['idRelato']);
            $anexo = $model_relatos->select_anexos($relato[0]['idProtocolo']);
            $resposta = $model_relatos->select_resposta($relato[0]['idProtocolo']);

            $model_vinculos = new Model_Scc_Vinculos('default');
            $vinculos = $model_vinculos->select_vinculos();

            $view = View::factory('scc/visualizar_relato')
                    ->set('idProtocolo', $idProtocolo)
                    ->set('relato', $relato)
                    ->set('resposta', $resposta)
                    ->set('anexo', $anexo)
                    ->set('vinculos', $vinculos)
            ;        
        } else {
            $warning = "O ID do protocolo informado não consta em nosso banco de dados.";
            $view = View::factory('scc/acompanhar_relato')
                    ->set('warning', $warning)
            ;  
        }

        $this->response->body($view);
    }

    public function action_dados_relato() {

        $vars = $this->request->post();

        $model_tipos = new Model_Scc_Tipos('default');
        $model_unidades = new Model_Scc_Unidades('default');
        $model_dsei = new Model_Scc_Dsei('default');
        $dseis = $model_dsei->select_dsei_ativas('1');
        $tipos = $model_tipos->select_tipos_ativos('1');
        $unidades = $model_unidades->select_unidades_ativas($vars['vinculo_solicitante'], '1');

        $view = View::factory('scc/dados_relato')
                ->set('vars', $vars)
                ->set('dseis', $dseis)
                ->set('tipos', $tipos)
                ->set('unidades', $unidades)
        ;
        $this->response->body($view);
    }

    public function action_gravar_relato() {

        $Protocolo = date('Ymd', time()) . Function_Certificados::coderand(8);
        $vars = $this->request->post();

        // * Inserindo Solicitante *
        $model_solicitacao = new Model_Scc_Solicitacao('default');
        $idSolicitante = $model_solicitacao->insert_solicitante($vars);

        // * Inserindo Relato *
        if ($vars["dsei"] == 'null') {
            $iDsei = null;
        } else {
            $iDsei = $vars["dsei"];
        }
            
        $idRelato = $model_solicitacao->insert_relato($vars, $iDsei);

        // * Inserindo Relato *
        $idProtocolo = $model_solicitacao->insert_protocolo($Protocolo, $idSolicitante[0], $idRelato[0]);

        // * Inserindo Anexo *
        $anexos = $_FILES['anexo'];
        $img_desc = $this->reArrayFiles($anexos);
        $i = 1;
        foreach ($img_desc as $val) {
            $ext = pathinfo($val['name'], PATHINFO_EXTENSION);
            $newname = $Protocolo . '_Anexo_' . $i . '.' . $ext;
            $envia = Upload::save($val, $newname, 'upload/scc');
            if ($envia != false) {
                $anexar = $model_solicitacao->insert_anexo($Protocolo, $newname);
            }
            $i++;
        }
        
        $idTipoSetor = $vars['vinculo_solicitante'];
        $model_usuarios = new Model_Login_Usuario('user');
        $usuarios = $model_usuarios->select_usuarios_setor($idTipoSetor);

        //Envio para o RH 
        if ($idTipoSetor != "") {
            foreach ($usuarios as $usuario) {
                $viewemail = View::factory('scc/corpo_email_solicitacao')
                        ->set('Protocolo', $Protocolo)
                        ->set('vars', $vars)
                        ->set('idRelato', $idRelato)
                        ->render();

                $email_subject = 'Canal Confidencial  - Dados do relato nº' . $Protocolo;
                $email_body = $viewemail;
                $is_html = true;
                $message = Email::factory($email_subject, $email_body, $is_html)
                        ->from('naoresponder@spdm.org.br', 'Canal Confidencial')
                        ->to($usuario['email'], $usuario['nmNome']);
                $result = $message->send();
            }
        }
        
        //Envio para o Sergio 
        if ($idTipoSetor != "") {
            $viewemail = View::factory('scc/corpo_email_solicitacao')
                        ->set('Protocolo', $Protocolo)
                        ->set('vars', $vars)
                        ->set('idRelato', $idRelato)
                        ->render();

                $email_subject = 'Canal Confidencial  - Dados do relato nº' . $Protocolo;
                $email_body = $viewemail;
                $is_html = true;
                $message = Email::factory($email_subject, $email_body, $is_html)
                        ->from('naoresponder@spdm.org.br', 'Canal Confidencial')
                        ->to('sergio.carvalho@spdm.org.br', 'Sergio Alexandre');
                $result = $message->send();
        }    
        
        //Envio para o Dr Nacime 
        if ($idTipoSetor != "") {
            if ($idTipoSetor == '10') {
                    $viewemail = View::factory('scc/corpo_email_solicitacao')
                            ->set('Protocolo', $Protocolo)
                            ->set('vars', $vars)
                            ->set('idRelato', $idRelato)
                            ->render();

                    $email_subject = 'Canal Confidencial  - Dados do relato nº' . $Protocolo;
                    $email_body = $viewemail;
                    $is_html = true;
                    $message = Email::factory($email_subject, $email_body, $is_html)
                            ->from('naoresponder@spdm.org.br', 'Canal Confidencial')
                            ->to('nacimemansur@uol.com.br', 'Dr Nacime');
                    $result = $message->send();
            }
        }    

        //Envio para o solicitante
        if ($idTipoSetor != "") {
            if ($vars['email_solicitante'] != "") {
                $viewemails = View::factory('scc/corpo_email_solicitante')
                        ->set('Protocolo', $Protocolo)
                        ->set('vars', $vars)
                        ->set('idRelato', $idRelato)
                        ->render();

                $email_subject_s = 'Canal Confidencial  - Dados do relato nº' . $Protocolo;
                $email_body_s = $viewemails;
                $is_html_s = true;
                $message_s = Email::factory($email_subject_s, $email_body_s, $is_html_s)
                        //->from('combateaoassedio@spdm.org.br', 'Canal Confidencial')
                        ->from('naoresponder@spdm.org.br', 'Canal Confidencial')
                        ->to($vars['email_solicitante'], $vars['nome_solicitante']);
                $result_s = $message_s->send();
            }
        }

        $view = View::factory('scc/relato_gravado')
                ->set('Protocolo', $Protocolo)
        ;
        $this->response->body($view);
    }

    public function action_responder_relato() {
        $var = $this->request->post();
        $model_relatos = new Model_Scc_Relato('default');
        
        $resposta = $var['resposta_usuario'];
        $resposta = $model_relatos->insert_resposta_usuario($var, $resposta);
        $Status = '2';
        $Protocolo = $var['idProtocolo'];
        $model_relatos->update_protocolo($var, $Status);

        $warning = "Relato ID: " . $var['idProtocolo'] . " foi respondido pelo solicitante!";
        //Envio da resposta
        if ($var['email_solicitante'] != "") {
            $viewemail = View::factory('scc/corpo_email_resposta_usuario')
                    ->set('Protocolo', $Protocolo)
                    ->set('var', $var)
                    ->set('warning', $warning)
                    ->render();
            $email_subject = 'Canal Confidencial  - Dados do relato nº' . $var['idProtocolo'];
            $email_body = $viewemail;
            $is_html = true;
            $message = Email::factory($email_subject, $email_body, $is_html)
                    //->from('combateaoassedio@spdm.org.br', 'Canal Confidencial')
                    ->from('naoresponder@spdm.org.br', 'Canal Confidencial')
                    ->to($var['email_solicitante'], $var['nome_solicitante']);
            $result = $message->send();
        }
        
        $idTipoSetor = $var['vinculo_solicitante'];
        $model_usuarios = new Model_Login_Usuario('user');
        $usuarios = $model_usuarios->select_usuarios_setor($idTipoSetor);
        
        //Envio para o RH 
        foreach ($usuarios as $usuario) {
            $viewemail = View::factory('scc/corpo_email_resposta_usuario')
                    ->set('Protocolo', $Protocolo)
                    ->set('var', $var)
                    ->set('warning', $warning)
                    ->render();

            $email_subject = 'Canal Confidencial  - Dados do relato nº' . $Protocolo;
            $email_body = $viewemail;
            $is_html = true;
            $message = Email::factory($email_subject, $email_body, $is_html)
                    //->from('combateaoassedio@spdm.org.br', 'Canal Confidencial')
                    ->from('naoresponder@spdm.org.br', 'Canal Confidencial')
                    ->to($usuario['email'], $usuario['nmNome']);
            $result = $message->send();
        }
        
        //Envio para o Sergio 
        $viewemail = View::factory('scc/corpo_email_resposta_usuario')
                    ->set('Protocolo', $Protocolo)
                    ->set('var', $var)
                    ->set('warning', $warning)
                    ->render();

            $email_subject = 'Canal Confidencial  - Dados do relato nº' . $Protocolo;
            $email_body = $viewemail;
            $is_html = true;
            $message = Email::factory($email_subject, $email_body, $is_html)
                    ->from('naoresponder@spdm.org.br', 'Canal Confidencial')
                    ->to('sergio.carvalho@spdm.org.br', 'Sergio Alexandre');
            $result = $message->send();
        
        //Envio para o Dr Nacime
        if ($idTipoSetor == '10') {
            $viewemail = View::factory('scc/corpo_email_resposta_usuario')
                    ->set('Protocolo', $Protocolo)
                    ->set('vars', $vars)
                    ->set('warning', $warning)
                    ->render();

            $email_subject = 'Canal Confidencial  - Dados do relato nº' . $Protocolo;
            $email_body = $viewemail;
            $is_html = true;
            $message = Email::factory($email_subject, $email_body, $is_html)
                    //->from('combateaoassedio@spdm.org.br', 'Canal Confidencial')
                    ->from('naoresponder@spdm.org.br', 'Canal Confidencial')
                    ->to('nacimemansur@uol.com.br', 'Dr Nacime');
            $result = $message->send();
        }
            
        $protocolo = $model_relatos->select_protocolo($var['idProtocolo']);
        $relato = $model_relatos->select_relato($var['idRelato']);
        $anexo = $model_relatos->select_anexos($var['idProtocolo']);
        $resposta = $model_relatos->select_resposta($var['idProtocolo']);

        $model_vinculos = new Model_Scc_Vinculos('default');
        $vinculos = $model_vinculos->select_vinculos();

        $view = View::factory('scc/visualizar_relato')
                ->set('idProtocolo', $Protocolo)
                ->set('relato', $relato)
                ->set('resposta', $resposta)
                ->set('anexo', $anexo)
                ->set('vinculos', $vinculos)
                ->set('warning', $warning)
        ;

        $this->response->body($view);
    }

    public function reArrayFiles($file) {
        $file_ary = array();
        $file_count = count($file['name']);
        $file_key = array_keys($file);

        for ($i = 0; $i < $file_count; $i++) {
            foreach ($file_key as $val) {
                $file_ary[$i][$val] = $file[$val][$i];
            }
        }
        return $file_ary;
    }

}

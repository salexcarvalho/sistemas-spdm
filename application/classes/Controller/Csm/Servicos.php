<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Csm_Servicos extends Controller
{

    public function action_index()
    {
        $this->redirect('csm_servicos/listar_servicos');
    }

    public function action_echo()
    {
        $message = $this->request->param('id');
        if (!empty($message))
        {
            $this->response->body("ID : $message.");
        }
        else
        {
            $this->response->body("ID : sem mensagem");
        }
    }

    public function action_listar_servicos()
    {

        //$especialidades = $this->action_lista_especialidades();

        $view = View::factory('csm/servicos/servicos')
        //        ->set('especialidades', $especialidades)
        ;
        $this->response->body($view);
    }

    

    public function action_detalhes()
    {
        $view = View::factory('csm/servicos/detalhes')
        ;
        $this->response->body($view);
    }

}

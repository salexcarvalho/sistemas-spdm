<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Csm_Recado extends Controller
{

    public function action_index()
    {
        $this->redirect('csm_recado/contato');
    }

    public function action_contato()
    {
        $view = View::factory('csm/recado/contato')
        ;
        $this->response->body($view);
    }

    public function action_enviar_contato()
    {
        $nome = $this->request->post('txtNome');
        $email = $this->request->post('txtEmail');
        $mensagem = $this->request->post('txtMensagem');

        //$model_recado = new Model_Recado('default');

        //validação
        $validation = Validation::factory($this->request->post())
                ->rule('txtNome', 'not_empty')
                ->rule('txtNome', 'alpha_spaces')
                ->rule('txtNome', 'min_length', array(':value', 3))
                ->rule('txtEmail', 'not_empty')
                ->rule('txtEmail', 'email')
                ->rule('txtEmail', 'min_length', array(':value', 4))
                ->rule('txtMensagem', 'not_empty')
                ->rule('txtMensagem', 'alpha_numbers_dash_spaces')
                ->rule('txtMensagem', 'min_length', array(':value', 10))
        //->rule('txtEmail', 'regex', array(':value', '/^[a-z_. ]++$/iD'))
        ;

        if ($validation->check())
        {
            // Data has been validated, register the user
            $status = $model_recado->insert_contato($nome, $email, $mensagem);

            // Always redirect after a successful POST to prevent refresh warnings
            //$this->redirect('recado/contato', 303);

            $warning = 'Contato enviado com sucesso.';

            //$this->redirect('recado/contato', 303);
            $this->response->body(View::factory('csm/recado/contato')
                            ->bind('warning', $warning))
            ;
        }
        else
        {

            // Validation failed, collect the errors
            $errors = $validation->errors('recado/contato', 'br');

            // Display the registration form
            $postt = $this->request->post();
            $this->response->body(View::factory('recado/contato')
                            ->bind('post', $postt)
                            ->bind('errors', $errors))
            ;

            //fim validação
            //$view = View::factory('recado/contato')
            //        ->set('artigos', $artigos)
            //;
            //$this->response->body($view);
            //$this->response->body(View::factory('recado/contato')
            //->bind('post', $postt)
            //->bind('errors', $errors)
            //)
            //;
        }
    }

}

// End Home

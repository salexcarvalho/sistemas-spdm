<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Csm_Busca extends Controller
{

    public function action_index()
    {
        $this->redirect('csm_busca/simples');
    }

    public function action_simples()
    {
        $view = View::factory('csm/busca/simples')
        ;
        $this->response->body($view);
    }

    public function action_avancada()
    {
        $model_servico = new Model_Csm_Servico('default');

        $servicos = $model_servico->select_servicos_mais_procurados(10);

        $view = View::factory('csm/busca/avancada')
                ->set('servicos', $servicos)
        ;
        $this->response->body($view);
    }

    public function action_resultado()
    {
        $view = View::factory('csm/busca/resultado')
        ;
        $this->response->body($view);
    }

}

// End Home
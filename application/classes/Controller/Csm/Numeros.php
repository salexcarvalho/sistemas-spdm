<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Csm_Numeros extends Controller
{

    public function action_index()
    {
        $this->redirect('csm_numeros/detalhes');
    }

    public function action_detalhes()
    {
        $view = View::factory('csm/numeros/detalhes')
        ;
        $this->response->body($view);
    }

}

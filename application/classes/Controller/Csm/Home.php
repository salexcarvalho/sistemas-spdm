<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Csm_Home extends Controller
{

    public function action_index()
    {
        
        $this->redirect('csm_home/inicio');
    }

    public function action_inicio()         
    {
        $model_servico = new Model_Csm_Servico('default');
        $servicos = $model_servico->select_servicos_mais_procurados(10);

        $view = View::factory('csm/home')
                ->set('servicos', $servicos)
        ;
        $this->response->body($view);
    }

}

// End Home

<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Csm_Imprensa extends Controller
{

    public function action_index()
    {
        $this->redirect('csm_imprensa/detalhes');
    }

    public function action_detalhes()
    {
        $view = View::factory('csm/imprensa/detalhes')
        ;
        $this->response->body($view);
    }

}

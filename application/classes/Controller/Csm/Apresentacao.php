<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Csm_Apresentacao extends Controller
{

    public function action_index()
    {
        $this->redirect('csm_apresentacao/detalhes');
    }

    public function action_detalhes()
    {
        $model_artigo = new Model_Csm_Artigo('default');        
        $model_servico = new Model_Csm_Servico('default');

        $servicos = $model_servico->select_servicos_mais_procurados(10);
        $artigos = $model_artigo->select_artigos_apresentacao();
        
        $view = View::factory('csm/apresentacao/detalhes')
                ->set('artigos', $artigos)
                ->set('servicos', $servicos)
        ;
        $this->response->body($view);
    }

}

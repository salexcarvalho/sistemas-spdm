<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Csm_Profissionais extends Controller
{

    public function action_index()
    {
        $this->redirect('csm_profissionais/detalhes');
    }

    public function action_detalhes()
    {
        $model_artigo = new Model_Csm_Artigo('default');
        $model_servico = new Model_Csm_Servico('default');

        $servicos = $model_servico->select_servicos_mais_procurados(10);
        $artigos = $model_artigo->select_artigos_profissionais();
        
        $view = View::factory('csm/profissionais/detalhes')
                ->set('artigos', $artigos)
                ->set('servicos', $servicos)
        ;
        $this->response->body($view);
    }

}

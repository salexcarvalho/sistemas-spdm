<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Agenda_Home extends Controller {

    public function action_index() {
        $this->redirect('agenda/home/home');
    }

    public function action_home() {
        $model_salas = new Model_Rsa_Salas('default');
        $salas = $model_salas->select_salas();
        $model_areas = new Model_Rsa_Area();
        $areas = $model_areas->select_areas();

        $view = View::factory('agenda/home')
                ->set('areas', $areas)
                ->set('salas', $salas);
        $this->response->body($view);
    }
    
    public function action_getSalas() {
        $model_salas = new Model_Rsa_Salas('default');
        $salas = $model_salas->select_salas();
        $resource = array();
        foreach ($salas as $sala):
            $resource[] = array(
                "id" => $sala['idSalas'],
                "title" => $sala['nome'],
                "eventColor" => "#" . $sala['rgb']
            );
        endforeach;

        echo json_encode($resource);
    }

    public function action_getReservas() {
        $model_reserva = new Model_Rsa_Reserva('default');
        $model_salas = new Model_Rsa_Salas('default');
        $model_usuario = new Model_Login_Usuario('user');
        $reservas = $model_reserva->select_reservas_data();
        $args = array();
        foreach ($reservas as $reserva):
            $salas = $model_salas->select_salas_rgb($reserva['sala_id']);
            $idUsuario = ($reserva['idUsuario_at'] > 0) ? $reserva['idUsuario_at'] : $reserva['idUsuario'];
            $user = $model_usuario->select_usuario($idUsuario);
                $args[] = array(
                    "id" => $reserva['idReserva'],
                    "title" => $reserva['titulo'] . ' - ' . $reserva['NomeSala'],
                    "start" => $reserva['start_data'],
                    "end" => $reserva['end_data'],
                    "responsavel" => $reserva['responsavel'],
                    "resourceId" => $reserva['sala_id'],
                    "sala" => $reserva['sala_id'],
                    "color" => "#" . $salas[0]['rgb'],
                    "owner" => $user[0]['nmNome'],
                    "owner_id" => $idUsuario,
                    "obs" => $reserva['obs']
                );
        endforeach;

        echo json_encode($args);
    }
    
    public function action_visualizar_reserva() {
        $idReserva = $this->request->query('idReserva');
        
        $model_salas = new Model_Rsa_Salas('default');
        $model_reserva = new Model_Rsa_Reserva('default');
        $salasReserva = $model_salas->select_salas_publico();
        $dados = $model_reserva->select_reserva($idReserva);

        $view = View::factory('adm/rsa/modal_visualizar')
                ->set('salasReserva', $salasReserva)
                ->set('dados', $dados)     
                ->set('idReserva', $idReserva);
        $this->response->body($view);
    }

}

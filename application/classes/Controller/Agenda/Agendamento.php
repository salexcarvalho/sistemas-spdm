<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Agenda_Agendamento extends Controller {

    public function action_realizar() {
        $model_areas = New Model_Rsa_Area('default');
        $areas = $model_areas->select_areas();
        $view = View::factory('agenda/realizar_agendamento')
                ->set('areas', $areas);
        $this->response->body($view);
    }

    public function action_agendar() {
        $viewemails = View::factory('agenda/corpo_email_agendamento')
                ->set('Protocolo', $Protocolo)
                ->set('vars', $vars)
                ->set('idRelato', $idRelato)
                ->render();
    }

    public function action_consultar() {
        $model_agendamento = New Model_Rsa_Salas('default');

        $salas = $model_salas->select_salas();
        $view = View::factory('agenda/consultar_agendamento')
                ->set('salas', $salas);
        $this->response->body($view);
    }

    public function action_salas() {
        $idArea = $this->request->query('idArea');
        $model_salas = New Model_Rsa_Salas('default');
        $salas = $model_salas->select_salas_area($idArea);
        echo "<option >Selecione uma opção</option>";
        foreach ($salas as $sala) {
            echo "<option value='" . $sala['idSalas'] . "'>" . $sala['nome'] . "</option>";
        }
    }
 public function action_conciliar(){        
        $idSala = $this->request->post('idSala');
        $start_data = Funcoes::convertData($this->request->post('start_data'));
        $end_data = Funcoes::convertData($this->request->post('end_data'));
        $model_reserva = new Model_Rsa_Reserva();
        $verificar = $model_reserva->disponibilidade($idSala, $start_data, $end_data);
         Funcoes::dump($verificar);
        echo $verificar[0]['idReserva'];        
    }
}

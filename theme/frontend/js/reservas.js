$(document).ready(function () {
    var d = new Date();
    var hora = d.getHours();
    if (hora < 7) {
        hora = 7;
    }
    var min = d.getMinutes();
    if (min < 10) {
        min = '0' + min;
    }
    var dataAtual, dia, mes, ano;
    dia = d.getDate();
    mes = d.getMonth() + 1;
    ano = d.getFullYear();

    if (mes < 10) {
        mes = "0" + mes;
    }
    if (dia < 10) {
        dia = "0" + dia;
    }

    dataAtual = ano + "-" + mes + "-" + dia;
    
    /*------------ initialize the calendar-----*/
    $('#calendar').fullCalendar({
        locale: 'pt-br',
        schedulerLicenseKey: 'CC-Attribution-NonCommercial-NoDerivatives',
        resources: {
            url: 'getSalas',
        },
        events: {
            url: 'getReservas',
        },
        firstDay: 0,
        contentHeight: 795,
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'listMonth,agendaWeek,month'
        },
        viewDisplay: function (view) {
            alert('viewDisplay(' + view + ')');
        },
        defaultDate: dataAtual,
        minTime: "07:00",
        maxTime: "23:00",
        defaultView: 'month',
        themeSystem: 'bootstrap3',
        //businessHours: true, // display business hours

        eventRender: function (event, element, view) {
            var data = event.start.format();
            if (data < dataAtual) {
                element.draggable = false;
                element.editable = false;
                element.eventLimit = false;
                element.droppable = false;
                element.navLinks = false;
                element.selectable = false;
                element.selectHelper = false;
            } else {
                element.draggable = false;
                element.editable = false;
                element.eventLimit = false;
                element.droppable = false;
                element.navLinks = true;
                element.selectable = true;
                element.selectHelper = true;
            }
            if (Array.isArray(event.ranges)) {
                return (event.ranges.filter(function (range) {
                    return (event.start.isBefore(range.end) &&
                            event.end.isAfter(range.start));
                }).length) > 0;
            }
            if ($('#eventByResource').val() > 0) {
                return ['all', event.resourceId].indexOf($('#eventByResource').val()) > 0;
                
            }            
        },

        eventClick: function (event) {
                var idReserva = event.id;
                    var options = {
                        url: "visualizar_reserva?idReserva=" + idReserva,
                        title: 'Visualizar Reserva',
                        size: eModal.size.lg,
                        loadingHtml: true,
                        buttons: [
                            {text: 'FECHAR', style: 'danger', close: true}
                        ]
                    };
                    eModal.ajax(options);
        },       
        
    });

    $('#calendar').fullCalendar();
    
    $('#eventByResource').on('change', function () {
        $('#calendar').fullCalendar('rerenderEvents');
    });
});
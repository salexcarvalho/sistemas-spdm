$(document).ready(function () {
    var d = new Date();
    var hora = d.getHours();
    if (hora < 7) {
        hora = 7;
    }
    var min = d.getMinutes();
    if (min < 10) {
        min = '0' + min;
    }
    var dataAtual, dia, mes, ano;
    dia = d.getDate();
    mes = d.getMonth() + 1;
    ano = d.getFullYear();

    if (mes < 10) {
        mes = "0" + mes;
    }
    if (dia < 10) {
        dia = "0" + dia;
    }
    
    function excluir() {

        var idReserva = $('#idReserva').val();
        var idSerie = $('#idSerie').val();
        var Serie = $('#Serie:checked').val();
        var acao;
        acao = 'excluirReservas';
        $.ajax({
            url: acao,
            data: {
                'idReserva': idReserva,
                'idSerie' : idSerie,
                'Serie' : Serie
            },
            type: 'POST',
            dataType: 'json',
            success: function (data) {
                console.log(data);
            },
            error: function (error) {
                console.log("Error:");
                console.log(data);
            }
        });
       window.location = 'home';
    };

    var user = $("#idUsuario").val();
    var idPerfil = $("#Perfil").val();
    var permissao = $("#Permissao").val();
    //alert(permissao);
    dataAtual = ano + "-" + mes + "-" + dia;
    
    /*------------ initialize the calendar-----*/
    $('#calendar').fullCalendar({
        locale: 'pt-br',
        schedulerLicenseKey: 'CC-Attribution-NonCommercial-NoDerivatives',
        resources: {
            url: 'getSalas',
        },
        events: {
            url: 'getReservas',
        },
        firstDay: 0,
        contentHeight: 795,
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'listMonth,agendaWeek,month'
        },
        viewDisplay: function (view) {
            alert('viewDisplay(' + view + ')');
        },
        defaultDate: dataAtual,
        minTime: "07:00",
        maxTime: "23:00",
        defaultView: 'agendaWeek',
        themeSystem: 'bootstrap3',
        //businessHours: true, // display business hours

        eventRender: function (event, element, view) {
            var data = event.start.format();
            if (data < dataAtual) {
                element.draggable = false;
                element.editable = false;
                element.eventLimit = false;
                element.droppable = false;
                element.navLinks = false;
                element.selectable = false;
                element.selectHelper = false;
            } else {
                element.draggable = false;
                element.editable = false;
                element.eventLimit = false;
                element.droppable = false;
                element.navLinks = true;
                element.selectable = true;
                element.selectHelper = true;
            }
            if (Array.isArray(event.ranges)) {
                return (event.ranges.filter(function (range) {
                    return (event.start.isBefore(range.end) &&
                            event.end.isAfter(range.start));
                }).length) > 0;
            }
            if ($('#eventByResource').val() > 0) {
                return ['all', event.resourceId].indexOf($('#eventByResource').val()) > 0;
                
            }            
        },

        eventClick: function (event) {
            var owner_id = event.owner_id;
            if (owner_id == user || idPerfil == 'Administrador') {
                var comp = moment(event.start).format('YYYY-MM-DD');
                if (comp >= dataAtual) {
                    var idReserva = event.id;
                    var options = {
                        url: "incluir_reserva?idReserva=" + idReserva,
                        title: 'Editar Reserva',
                        size: eModal.size.lg,
                        loadingHtml: true,
                        buttons: [
                            {text: 'ALTERAR', style: 'info', close: false, type: 'submit', form: 'reservaForm'},
                            {text: 'EXCLUIR', style: 'warning', close: true, click: excluir},
                            {text: 'FECHAR', style: 'danger', close: true}
                        ]
                    };
                    eModal.ajax(options);
                }
            } else {
                var idReserva = event.id;
                    var options = {
                        url: "visualizar_reserva?idReserva=" + idReserva,
                        title: 'Visualizar Reserva',
                        size: eModal.size.lg,
                        loadingHtml: true,
                        buttons: [
                            {text: 'FECHAR', style: 'danger', close: true}
                        ]
                    };
                    eModal.ajax(options);
            }
        },
        
        dayClick: function (date, jsEvent, view) {
            var comp = date.format('YYYY-MM-DD');
            if (comp >= dataAtual) {
                var idReserva = '';
                var options = {
                    url: "incluir_reserva?idReserva=" + idReserva,
                    title: 'Cadastrar Reserva',
                    size: eModal.size.lg,
                    buttons: [
                        {text: 'GRAVAR', style: 'primary', close: false, type: 'submit', form: 'reservaForm'},
                        {text: 'FECHAR', style: 'danger', close: true}
                    ]
                };

                eModal.ajax(options);
            }
        },        
    });
    
    $('button[name=btn_novo_reserva]').click(function () {
        var idReserva = '';
        var options = {
            url: "incluir_reserva?idReserva=" + idReserva,
            title: 'Cadastrar Reserva',
            size: eModal.size.lg,
            buttons: [
                {text: 'GRAVAR', style: 'primary', close: false, type: 'submit', form: 'reservaForm'},
                {text: 'FECHAR', style: 'danger', close: true}
            ]
        };
        eModal.ajax(options);
    });

    $("button[name=modal_btn-excluir]").click(function () {
        var idReserva = $('#idReserva').val();
        $.ajax({
            url: 'excluirReservas',
            data: 'idReserva=' + idReserva,
            type: 'POST',
            dataType: 'json'
        });
        $("button[name=modal_btn-excluir]").hide();
        $('#calendar').fullCalendar('refetchEvents');
    });
    
    $('#calendar').fullCalendar();
    
    $('#btn_imprimir').click(function () {    
         var params = {
            url: "modal_imprimir",
            title: 'Imprimir Reserva',
            size: eModal.size.lg,
            buttons: [
                  {text: 'IMPRIMIR', style: 'primary', close: false, type: 'submit', form: 'imprimrirForm'},
                  {text: 'FECHAR', style: 'danger', close: true}
            ]
        };       
        return eModal
                .ajax(params);
       });   
    
    $('#modal_btn-fechar').click(function () {
        $('#ini_relat').val('');     
        $('#modal_imprimir').hide();
    });
    $('#eventByResource').on('change', function () {
        $('#calendar').fullCalendar('rerenderEvents');
    });
});
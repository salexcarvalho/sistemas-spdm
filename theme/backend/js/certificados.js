jQuery(document).ready(function ($)
{
    var idParticipante;
    var Nome;
    var idApoio;
    var page;

    $("#idCertificados").on('change', function () {
        $("#eventoB").val($(this).val());
        $('button[name=btn_filtrar]').attr('idEvento', $(this).val());
    });

    $("body").on("click", 'button[name=btnExcluir]', function ()    {
        Nome = $(this).attr('nmparticipante');
        idParticipante = $(this).attr('idparticipante');
        idApoio = $(this).attr('idapoio');
        if (typeof idApoio == 'undefined') {
            $('#modal_delete_participante').html("Deseja excluir o participante <b>\"" + Nome + "\"</b> ?");
        } else {
            $('#modal_delete_participante').html("Deseja excluir este complemento <b>\"" + Nome + "\"</b> ?");
        }
    });

    $("body").on("click", 'button[name=btn_filtrar]', function ()
    {
        var idEvento = $(this).attr('idEvento');
        var Busca = $(this).attr('Busca');
        window.location.href = "home?Busca=" + Busca + "&eventoB=" + idEvento;

    });

    $('#modal_excluir').on('hidden.bs.modal', function ()
    {
        window.location.replace("home");
    });


    $('#modal_btn-excluir').on("click", function ()
    {
        var btn = $(this);
        var excluir, txt;
        btn.text('loading');
        btn.removeClass('btn-primary');
        btn.removeClass('btn-danger');
        btn.removeClass('btn-success');
        btn.addClass('btn-primary');
        if (idApoio > 0) {
            excluir = 'deleta_complemento';
            txt = 'Complemento excluído com sucesso';
        } else {
            excluir = 'delete_participante';
            txt = 'Participante excluído com sucesso';
        }
        $('#modal_btn-excluir').prop('disabled', true); 
        $.post(excluir,{idParticipante: idParticipante,idApoio: idApoio},
                function(returnedData)
                {
                    $('#modal_delete_participante').html(txt);

                    btn.removeClass('btn-primary');
                    btn.addClass('btn-success');
                })
                .fail(function()
                {
                    $('#modal_delete_participante').html('Erro: ' + data);
                    btn.removeClass('btn-primary');
                    btn.addClass('btn-danger');
                });
    });

    $("body").on("click", "button[name=btnAtivar]", function () {
        idParticipante = $(this).attr('idParticipante');
        $.get('update_ativar_participante?idParticipante=' + idParticipante, function (data) {

            if (data === "1") {
                $('#btnAtivar_' + idParticipante).addClass('btn btn-success btn-xs');
                $('#btnAtivar_' + idParticipante).removeClass('btn-danger');
                $('#activ_' + idParticipante).removeClass('glyphicon-ban-circle');
                $('#activ_' + idParticipante).addClass('glyphicon-ok');
            } else {
                $('#btnAtivar_' + idParticipante).addClass('btn btn-danger btn-xs');
                $('#btnAtivar_' + idParticipante).removeClass('btn-success');
                $('#activ_' + idParticipante).removeClass('glyphicon-ok');
                $('#activ_' + idParticipante).addClass('glyphicon-ban-circle');
            }
        });
    });
    
    $('.dataTables_filter label').addClass('pull-right');

    //Atualiza link paginacao


    // Gera o certificado
    $("body").on("click", "button[name=btnGerarCertificado]", function ()
    {
        var idParticipante = $(this).attr('idParticipante');
        var idCertificados = $('#idCertificados').val();
        var idApoio = $(this).attr('idApoio');
        var CertUsuario = $(this).attr('CertUsuario');

        if (idCertificados === "NULL") {
            $('#AlertaEvento').removeClass('hidden');
            $('#idCertificados').addClass('alert-warning');
        } else {
            var acao = "gerar_certificado";
            $.ajax(acao,
                    {
                        type: "POST",
                        data:
                                {
                                    idParticipante: idParticipante,
                                    idCertificados: idCertificados,
                                    idApoio: idApoio
                                }
                    })
                    .always(function ()
                    {
                        $('#acaoplus_' + idApoio).removeClass('fa-plus-circle');
                        $('#btnGerarCertificado_' + idApoio).removeClass('btn-primary');
                        $('#BaixaCertificado_' + idApoio).removeClass('hidden');
                        $('#MostrarCertificado_' + idApoio).removeClass('hidden');
                        $('#btnGeraPDF_' + idApoio).addClass('disabled');
                        $('#btnBaixaPDF_' + idApoio).addClass('disabled');
                        $('#autenticacao_' + idApoio).val(idParticipante);
                        $('#acaoplus_' + idApoio).addClass('fa-times-circle');
                        $('#btnGerarCertificado_' + idApoio).addClass('btn-danger');
                        $('#CertUsuario_' + idApoio).val(idApoio);
                        //   location.reload();

                    });

        }
    });
    $("body").on("click", "button[name=btnExcluirCertificado]", function ()
    {
        var idParticipante = $(this).attr('idParticipante');
        var idCertificados = $(this).attr('idCertificados');
        var idApoio = $(this).attr('idApoio');
        var acao = "excluir_certificado";
        $.ajax(acao,
                {
                    type: "POST",
                    data:
                            {
                                idParticipante: idParticipante,
                                idCertificados: idCertificados,
                                idApoio: idApoio
                            }
                })
                .always(function ()
                {
                    $('#acaoplus_' + idApoio).removeClass('fa-times-circle')
                    $('#btnGerarCertificado_' + idApoio).removeClass('btn-danger');
                    $('#acaoplus_' + idApoio).addClass('fa-plus-circle');
                    $('#btnGerarCertificado_' + idApoio).addClass('btn-primary');
                    $('#MostrarCertificado_' + idApoio).addClass('hidden');
                    $('#BaixaCertificado_' + idApoio).addClass('hidden');
                    $('#CertUsuario_' + idApoio).val('');
                    location.reload();
                });


    });
   
    $("body").on("click", "button[name=btnGeraPDF]", function () {
        var idApoio = $(this).attr('idApoio');
        var idCertificado = $(this).attr('idCertificado');
        var autenticacao = $(this).attr('autenticacao');
        if ((idApoio != "") && (idCertificado != "") && (autenticacao != "")) {
            window.open("../certificado/gerapdf?idCertificado=" + idCertificado + "&idApoio=" + idApoio + "&autenticacao=" + autenticacao);
        }
    });
    $("body").on("click", "button[name=btnBaixaPDF]", function () {
        var autenticacao = $(this).attr('autenticacao');
        if (autenticacao != "") {
            window.open("../certificado/baixapdf?autenticacao=" + autenticacao);
        }
    });
    $("body").on("click", "button[name=btn_pdf_down]", function () {
        var idCertificados = $('#idCertificados').val();
        if (idCertificados === "NULL") {
            $('#AlertaEvento').removeClass('hidden');
            $('#idCertificados').addClass('alert-warning');
        } else {
            $.ajax('existe_cert',
                    {
                        type: "POST",
                        data: {
                            idCertificado: idCertificados
                        },
                        success: function (result) {
                            if (result == 1) {
                                window.open("../certificado/baixarLote?idCertificado=" + idCertificados);
                            } else {
                                alert("Este curso não possue certificados para baixar");
                            }
                        }
                    });
        }
    });
    
    $("#btn_enviar_email").click(function () {
        var idCertificados = $('#idCertificados').val();
        if (idCertificados === "NULL") {
            $('#AlertaEvento').removeClass('hidden');
            $('#idCertificados').addClass('alert-warning');
        } else {
            var params = {
                url: "modalEmail?idCertificados=" + idCertificados,
                title: 'Enviar Certificados',
                size: eModal.size.lg,
                buttons: [
                    
                    {text: 'ENVIAR', style: 'primary', close: false, type: 'submit', form: 'reservaForm'},
                    {text: 'FECHAR', style: 'danger', close: true}
                ]
            };
            return eModal.ajax(params);
        }
    });
    $("body").on("click", "button[name=EnviarEmailI]",function () {
        var params = {
            url: "modalEmail?autenticacao=" + $(this).attr('autenticacao'),
            title: 'Enviar Certificado',
            size: eModal.size.lg,
            buttons: [                
                {text: 'ENVIAR', style: 'primary', close: false, type: 'submit', form: 'reservaForm'},
                {text: 'FECHAR', style: 'danger', close: true}
            ]
        };
        return eModal.ajax(params);
    });
    $("body").on("click", "button[name=btnAddComp]",function () {
        var idParticipante = $(this).attr('idParticipante');
        var page = $(this).attr('page');
        var params = {
            url: "adicionar_complemento?idParticipante=" + idParticipante + "&page=" + page,
            title: 'Cadastrar Complemento Participante',
            size: eModal.size.lg,
            buttons: [                
                {text: 'GRAVAR', style: 'primary', close: false, type: 'submit', form: 'FormCompAdd'},
                {text: 'FECHAR', style: 'danger', close: true}
            ]
        };
        return eModal.ajax(params);
    });
    $("body").on("click", "button[name=btnEditarComp]",function () {
        var idParticipante = $(this).attr('idParticipante');
        var idApoio = $(this).attr('idApoio');
        var page = $(this).attr('page');
        var params = {
            url: "editar_complemento?idParticipante=" + idParticipante + "&page=" + page + "&idApoio=" + idApoio,
            title: 'Editar Complemento Participante',
            size: eModal.size.lg,
            buttons: [
                {text: 'SALVAR', style: 'primary', close: false, type: 'submit', form: 'FormCompEdit'},
                {text: 'FECHAR', style: 'danger', close: true}

            ]
        };
        return eModal.ajax(params);
    });
    $("body").on("click", "button[name=btnAddPart]",function () {
        var page = $(this).attr('page');
        var params = {
            url: "cadastro_participante?page=" + page,
            title: 'Cadastrar Participante',
            size: eModal.size.lg,
            buttons: [                
                {text: 'GRAVAR', style: 'primary', close: false, type: 'submit', form: 'FormPartAdd'},
                {text: 'FECHAR', style: 'danger', close: true}
            ]
        };
        return eModal.ajax(params);
    });
    $("body").on("click", "button[name=btnEditarPart]",function () {
        var idParticipante = $(this).attr('idParticipante');        
        var page = $(this).attr('page');
        var params = {
            url: "editar_participante?idParticipante=" + idParticipante + "&page=" + page,
            title: 'Editar Participante',
            size: eModal.size.lg,
            buttons: [
                {text: 'SALVAR', style: 'primary', close: false, type: 'submit', form: 'FormPartEdit'},
                {text: 'FECHAR', style: 'danger', close: true}

            ]
        };
        return eModal.ajax(params);
    });
    
});
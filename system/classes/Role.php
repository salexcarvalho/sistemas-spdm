<?php

defined('SYSPATH') OR die('No direct script access.');

/**
 * @author Lucas Henrique <lucashenriquesd@gmail.com>
 */
class Role
{

    /**
     * 
     * @param string $usuario Nome do usuário.
     * @param type $controllerName TODO, ainda não implementado.
     * @param type $actionName
     * @return boolean
     */
    public static function check_role($usuario, $controllerName, $actionName)
    {
        $model_usuario_perfil = new Model_UsuarioTemPerfil('default');
        $model_perfil_acao = new Model_PerfilTemAcao('default');

        // Pega os perfis do usuário.
        $usuario_perfil = $model_usuario_perfil->select_usuario_perfil_por_usuario($usuario);

        // Pega as ações dos perfis do usuário.
        foreach ($usuario_perfil as $u_perfil)
        {
            $perfil_acao[] = $model_perfil_acao->select_acao_perfil_por_perfil($u_perfil['idPerfil']);
        }

        // Pega os nomes das actions relacionadas com os perfis e as formata em um array.

        $temp_acao = array();

        foreach ($perfil_acao as $p_acao)
        {
            foreach ($p_acao as $acao)
            {
                $temp_acao[] = $acao['nmAcao'];
            }
        }

        if (in_array($actionName, $temp_acao) == true)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

}
